

2019-10-04 Rec v30r6
===

This version uses Lbcom v30r6, LHCb v50r6, Gaudi v32r2 and LCG_96b with ROOT 6.18.04

This version is released on `master` branch.

Built relative to Rec v30r5, with the following changes:

### New features

- Add `NONE` and `MAXDOCACHI2CUT` functors, !1708 (@nnolte)   

- Add `NHITS` and `CLOSESTTOBEAMZ` functors for the smog minimum bias line, !1703 (@samarian)   
  See also lhcb/LHCb!2134

- Add zipping of SOA tracks + PV relations demonstrator, !1678 (@olupton)   
  See also lhcb/LHCb!2095. Contains also many other changes:  
  - Implement @graven's suggestions in Rec#80  
  - Add `PrUpstreamFromVelo` algorithm for feeding `SciFiTrackForwarding` with velo tracks  
  - Make logical and/or in functor expressions short-circuit when cuts are vectorised  
  - Teach functor predicates how to filter `LHCb::Pr::Zip` objects  
  - Generalise the functor predicate overload that already covered `LHCb::Pr::{Velo,Fitted::Forward}::Tracks` to one that filters anything accepted by `LHCb::Pr::make_zip()`  
  - Move some helper functions (`all( bool )`, `any( bool )`, `none( bool )`, ...) that are needed when writing generic code into `SelKernel` and out of the functor code  
  - Instantiate `CombineTracks` for track+relations zip input  
  - Instantiate `PrTracks` for track+relations zip input
  - Add `MakePVRelations` algorithm that puts track->vertex relations on the TES  
  - Add `MakeZip` algorithm that puts `LHCb::Pr::make_zip` output on the TES  
  - Replace old `MakeIterableTracks` algorithm with `Unwrap` algorithm, which puts 'unwrapped' iterable tracks on the TES. 
  - Add convenience typedefs like `LHCb::Pr::Fitted::Forward::TracksWithPVs`

- convert trackcompactvertex to recvertex_v1, !1672 (@nnolte)   

- components for full 1-track and 2-track line implementations, !1659 (@olupton)   
  Combined lhcb/LHCb!2065 and lhcb/Moore!223 this implements the `TrackMVALoose` and `TwoTrackMVALoose` lines    
  - Add adapter for applying "combination cuts" as "vertex cuts"
  - Add `MVA` functor and enough implementation to run the Run 2 `MatrixNet` `TwoTrackMVA` classifier
  - Use a new `detail::DataDepWrapper` helper for functors with data dependencies 
    - Fix a bug causing some extraneous copies in these functors  
  - Extend the Python wrapper:  
    - Allow docstrings for template arguments  
    - Map Python `str` onto C++ `std::string`  
    - Custom formatters for more complex functors like `MVA` and `COMB`  
    - Support template arguments properly  
  - Modify the factory to make fewer separate calls to cling and simplify how the functors are created. Check cling's "`-O3`" optimisation works. Don't call cling multiple times for the same functor.  
  - Add new unit tests.  
  - Add dummy `.family() = 42` method to `Pr::Selection<T>`  
  - Add child relations to `TrackCompactVertex`  

- More automated binary input production for Allen, !1639 (@dovombru)   
  - removed debug printf statement in TrackerDumper  
  - provide options script to dump raw banks and MC info at the same time  
  - provide shell script to produce binary input for the various physics samples  
    

### Enhancements

- VeloKalmanHelpers.h - remove std::move on return, that inhibits RVO, !1704 (@jonrob)   

- VeloSIMD small throughput improvement, !1699 (@ahennequ)   
  * Reorder loops to allow unrolling  
  * Add early exit if any hit container size == 0  
  * Remove division from loop over hits

- Update PrCheatedSciFiTracking, !1698 (@decianm)   
  Adapt PrCheatedSciFiTracking to work with Track::v2, and fix truthmatching

- Various updates for new RICH PMT DBs with large flag properly set, !1679 (@jonrob)   
  related to lhcb/LHCb!2097  

- Speed improvements of the seeding, !1676 (@lohenry)   

- Modify Pr::Tracks to know of ancestor relation, add lhcbIDs functions, !1671 (@sstahl)   
  - Adapt pattern recognition to changes in LHCb!2086.  
  - Add converter going from `LHCb::Pr::Fitted::Forward::Tracks` to `LHCb::v2:Track` directly.

- SciFiTrackForwarding, Introduce wrong sign window, update LDA parameters, !1656 (@chasse)   
  - updates the parameters of the linear discriminant analysis used to reject ghosts.        
  - introduce a wrong sign window search including a property which specifies the minimum transverse momentum at which this should be activated.  
  - The linear search is added in a variant that always uses abs.       

- New parabola seed window search in PrHybridSeeding, !1645 (@lohenry)   

- RichSIMDQuarticPhotonReco - Enable truncation of CK theta/phi values, !1643 (@jonrob)   

- Improvements to RICH pixel background algorithm, !1638 (@jonrob)   
  Implements some bug fixes to the background algorithm, and adapts to the changes in lhcb/LHCb!2036.   

- Improved u-v-hit search window calculation for the ForwardTracking, !1633 (@gunther)   

- Reintroduce Kalman Fit in VeloClusterTrackingSIMD, !1628 (@ahennequ)   
  Needs lhcb/LHCb!2038

- Functor improvements, !1625 (@olupton)   
  - Adjust functor code to allow printing via the owning algorithm.  
  - Provide a simple implementation of Warning that doesn't spam  
  - Remove apply_single, simplifying functor definitions slightly. Closes Rec#60.  
  - Support adapters, so you can write e.g. `SUM(PT)` as a `CombinationCut`  
  - Add some functors for acting on composite particles
  - Make the `detail::ComposedFunctor` helper variadic and implement most/all operations and overloads more succinctly using it. Also add some missing operator overloads.  
  - Add generic `detail::FunctorWithDataDeps` helper for functors that have external data dependencies  
  - Make some functors into templates
  - Improvements in the Python wrapper:  
    - Add missing operator overloads `__r*__` for when the LHS is a basic type.  
    - Allow functors with exactly one argument to have it specified as a positional argument.  
  - Allow "prepared" functors to be accessed through the type-erasure layer.  
  - Support variadic functor signatures and add some new functors.  
    - Allows `void` functors for things like `SIZE("Some/TES/Location") > 0`.  
    - Also permits functors that explicitly take two objects, e.g. `DOCA`.  
  - Reduce the verbosity of the functor factory and try to enable cling optimisation.  
  - Move the proxies used to iterate `LHCb::Pr::Velo::Tracks` out of the functor code (closes Rec#72)   
  - Make `Pr::Selection`'s iterators compatible with `std::next()`  
  - In `TracksFTConverter` make all states get a consistent `q/p` error according to the algorithm property  
  - Import commit from @pseyfert simplifying how we can write out `SOA::Container` skin/proxy types.  
  - Import commit from @wouter including new vertex types
  - Add a track combiner algorithm based on these new vertex classes
  - Add a "void filter" algorithm, which wraps a functor that takes no arguments  
  - Add `Phys/SelAlgorithms` and `Phys/SelKernel` packages to hold new algorithms/types/utilities.  
  Goes with LHCb!2029 and Moore!204.

- VeloKalman parameters update, !1624 (@ldufour)   

- Add special (faster!) handling for Line measurements, !1598 (@graven)   


### Bug fixes

- Fix PV type in converter, !1729 (@sstahl)

- Change Pr::Zip to avoid using SIMDWrapper types as class template arguments, !1713 (@olupton)  
  Goes with LHCb!2156   

- RichMCCherenkovResolution - Book histograms for all MC types, not just those 'active', !1706 (@jonrob)   
  Minor MC checking bug fix.

- fix gcc9 error: reinterpret_cast is not constexpr, !1688 (@graven)   

- Add workaround for cling errors when mixing C++ functors/LoKi, !1681 (@olupton)   

- Fix bug in MuonMatchVeloUTSoA which caused q over p written into the wrong place, !1662 (@sstahl)   

- Fixed precision issues in computations of the Kalman filter, !1660 (@sponce)   

- Attempts to fix gcc problem with range-v3..., !1649, !1666 (@graven, @cattanem)   

- Add dependency of CaloTools on libomp.so (clang only), !1631 (@clemenci)   
  needed to be able to load XGBoost.


### Code modernisations and cleanups

- Remove unused dependency on GaudiObjDesc, !1715 (@clemenci)

- Prefer CaloIndex enum over int or string, !1701 (@graven)  
  Needs lhcb/LHCb!2132

- Remove CaloFutureDigitFilterAlg, !1712 (@jmarchan)   
  Needs lhcb/LHCb!2154

- Address #78, !1707 (@cmarinbe)   
  Use `PublicToolHandleArray` instead of `ToolHandleArray` in `ClassifyPhotonElectronAlg` and remove intermediate `m_corrXTypeNames`.   

- Follow changes introduced in lhcb/LHCb!2127, !1705 (@graven)   

- Make public headers compilable, !1702 (@pseyfert)   

- Move {,I}CaloFutureDigitFilterTool from LHCb to Rec, !1700 (@graven)   
  Needs lhcb/LHCb!2131.

- prefer std::is_sorted to ranges::is_sorted to avoid issues with missing operator ==, !1696 (@jonrob)   

- follow changes in ROOT-8935, !1695 (@graven) [ROOT-8935]  

- Streamline upgrade ghost ID, !1694 (@graven)   
  * avoid virtual destructors if not required  
  * prefer front/back over *begin and *end  
  * multiply less  
  * add const  
  * avoid naked new and delete, prefer std::unique_ptr  
  * avoid repeated push_back on container, prefer constructing  
    in one shot  
  * prefer switch over repeated if-else-if

- Fix gcc9 warning: redundant move in return statement, !1693 (@graven)   

- Simplify ranges usage, misc. fixes for v 0.9.x, !1686 (@graven)   

- Remove obsolete (Run 1+2 only) FlagHitsForPatternReco algorithm, !1685 (@cattanem)   
  
- Follow lhcb/LHCb!2099, !1682 (@graven)   
  * rename Calo::Future into LHCb::Calo  
  * follow changes in CaloFutureUtils  
  * prefer string_view where appropriate  
  * avoid declaring uninitialized variables  
  * prefer std::optional over 'special values'  
  * avoid functions with lots of arguments of the same type -- instead  
    define a dedicated struct so the variables can be explicitly named  
  * prefer std::copy, std::transform over explicit loop  

- Avoid generate an anonymous Gaudi::Property, !1677 (@graven)   

- RichFutureRecTrackAlgorithms - Misc. cosmetic cleanups, !1669 (@jonrob)   

- Rich monitoring cosmetic cleanup, !1668 (@jonrob)   

- CaloFuture{ShowerOverlap,MergedPi0}: use functional conditions access to get DeCalorimeter, !1665 (@graven)   

- Changed IP resolution checker to functional, !1661 (@ldufour)   

- Follow renaming in lhcb/LHCb!2057, !1658 (@graven)   

- Rename PrStoreUTHit to LHCb::Pr::StoreUTHit, !1655 (@graven)   

- PrVeloUT: introduce namespace LHCb::Pr, !1652 (@graven)   

- PrPixel: Introduce the LHCb::Pr::Velo namespace, !1651 (@graven)   

- PrStoreUTHit - Adapt to UTDet changes, warn when a null sector ptr is received, !1650 (@jonrob)   
  Adapts to lhcb/LHCb!2052 and adds warning  
    
- various fixes for clang 8 build, !1644 (@jonrob)   

- Follow changes in lhcb/LHCb!2045, !1642 (@graven)   
  * follow renaming of interfaces  
  * follow constification of interface methods  
  * disentangle CaloFutureHypoEstimator and FutureNeutralIDTool usage  
  * remove unused method in CaloFutureElectron  
  * remove unused CaloFutureGetterInit algorithm  
  * remove (use of) CaloFutureGetterTool

- Classify Photon Electron, !1640 (@cmarinbe)   
  Replace `CaloFutureSinglePhotonAlg` and `CaloFutureElectronAlg` by a single `ClassifyPhotonElectronAlg`.   

- Avoid GOD for Node, !1522 (@graven)   


### Monitoring changes

- Put back option for x/y plots at a given z in PrChecker, !1641 (@decianm)   


### Changes to tests

- Add simple test for TrackLike functors, !1709 (@samarian)   

- RichRecTests - Add CPU capabilities check, !1670 (@jonrob)   
  
- BinaryDumpers - Update ref file size for ut boards, !1663 (@jonrob)   
  Adapts to change following lhcb/LHCb!2052  
