###############################################################################
# (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Configurables import LHCb__MDFWriter as MDFWriter
from Configurables import CondDB
from Gaudi.Configuration import ApplicationMgr
from Configurables import LHCbApp, createODIN
from Configurables import GaudiSequencer
from Configurables import LoKiSvc
from Configurables import FTNZSRawBankDecoder
from Configurables import FTDigitMonitor
from GaudiConf import IOHelper
from Configurables import HistogramPersistencySvc

app = LHCbApp()
app.DDDBtag = "dddb-20180815"
app.CondDBtag = "sim-20180530-vc-md100"
app.DataType = "Upgrade"
app.EvtMax = -1
app.Simulation = True
CondDB().Upgrade = True
CondDB().IgnoreHeartBeat = True
LoKiSvc().Welcome = False

#co = createODIN()
files = ["/scratch/scifi/lex/run_680_NZS_COUNTER_FTNZS.mdf"]
HistogramPersistencySvc().OutputFile = files[0][:-4] + ".root"

from GaudiConf import IOHelper
IOHelper("MDF").inputFiles(files)

#######
# Setup SciFi algorithms
from Configurables import FTNZSRawBankDecoder
from Configurables import FTDigitMonitor
monSeq = GaudiSequencer('SciFiMonitorSequence')
monSeq.IgnoreFilterPassed = True

decoder = FTNZSRawBankDecoder("DecodeFT")
decoder.OutputLevel = 1
decoder.DecodingVersion = 0

monitor = FTDigitMonitor()
monitor.OutputLevel = 1

monSeq.Members = [decoder, monitor]
#######

#######
# To look at what it's in the file (add dump to sequence to run it)
from Configurables import Online__RawEventTestDump
dump = Online__RawEventTestDump('Dump')
dump.RawLocation = '/Event/DAQ/RawEvent'
dump.CheckData = 0
dump.DumpData = 1
dump.FullDump = 1
dump.OutputLevel = 1
#######

ApplicationMgr().TopAlg = [monSeq]
ApplicationMgr().HistogramPersistency = "ROOT"
