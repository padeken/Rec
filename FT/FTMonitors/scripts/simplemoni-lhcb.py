###############################################################################
# (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Configurables import LHCb__MDFWriter as MDFWriter
from Configurables import CondDB
from Gaudi.Configuration import ApplicationMgr
from Configurables import LHCbApp, createODIN
from Configurables import GaudiSequencer
from Configurables import LoKiSvc
from GaudiConf import IOHelper
from Configurables import HistogramPersistencySvc

app = LHCbApp()
app.DDDBtag = "dddb-20180815"
app.CondDBtag = "sim-20180530-vc-md100"
app.DataType = "Upgrade"
app.EvtMax = -1
app.Simulation = True
CondDB().Upgrade = True
CondDB().IgnoreHeartBeat = True
LoKiSvc().Welcome = False

#co = createODIN()
directory = "/eos/lhcb/wg/SciFi/FromOlivier/"
#files = ["run_292.mdf"] #Number of clusters per link = seesaw like (from 0 to 10).
files = ["run_320_littleendian_cluster_v4.mdf"]

#files =  ["run_238_SeesawClusters_type64.mdf"]

HistogramPersistencySvc().OutputFile = files[0][:-4] + ".root"

input_files = []
for f in files:
    input_files.append(directory + f)

from GaudiConf import IOHelper
IOHelper("MDF").inputFiles(input_files)

#######
# Setup SciFi algorithms
from Configurables import FTRawBankDecoder
from Configurables import FTClusterMonitor

monSeq = GaudiSequencer('SciFiMonitorSequence')
monSeq.IgnoreFilterPassed = True

decoder = FTRawBankDecoder("DecodeFT")
decoder.OutputLevel = 0
decoder.DecodingVersion = 4

monitor = FTClusterMonitor()
monitor.OutputLevel = 0

monSeq.Members = [decoder, monitor]
#######

ApplicationMgr().TopAlg = [monSeq]
ApplicationMgr().HistogramPersistency = "ROOT"
