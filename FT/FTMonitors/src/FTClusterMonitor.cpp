/*****************************************************************************\
* (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "Event/FTLiteCluster.h"
#include "FTDet/DeFTDetector.h"
#include "GaudiAlg/Consumer.h"
#include "GaudiAlg/GaudiHistoAlg.h"
#include "GaudiKernel/PhysicalConstants.h"
#include "GaudiKernel/SystemOfUnits.h"
#include <DetDesc/ConditionAccessorHolder.h>
#include <GaudiKernel/GaudiException.h>

#include "Kernel/FTChannelID.h"
#include "Kernel/FastClusterContainer.h"

#include <AIDA/IHistogram1D.h>
#include <AIDA/IHistogram2D.h>
#include <optional>

/** @class FTClusterMonitor FTClusterMonitor.cpp
 *
 *
 *  @author Roel Aaij, adapted by Lex Greeven
 *  @date   2018-12-13
 */
using FTLiteClusters = LHCb::FTLiteCluster::FTLiteClusters;
namespace {
  using std::string;
  using std::to_string;
} // namespace

class FTClusterMonitor
    : public Gaudi::Functional::Consumer<void( const FTLiteClusters&, const DeFTDetector& ),
                                         LHCb::DetDesc::usesBaseAndConditions<GaudiHistoAlg, DeFTDetector>> {
public:
  FTClusterMonitor( const std::string& name, ISvcLocator* pSvcLocator );

  StatusCode initialize() override;

  void operator()( const FTLiteClusters&, const DeFTDetector& ) const override;

private:
  AIDA::IHistogram1D* m_lcps    = nullptr;
  AIDA::IHistogram1D* m_lcpm    = nullptr;
  AIDA::IHistogram1D* m_lcpsipm = nullptr;
  AIDA::IHistogram1D* m_lcpc    = nullptr;
  AIDA::IHistogram1D* m_lcppc   = nullptr;
  AIDA::IHistogram1D* m_lcf     = nullptr;
  AIDA::IHistogram1D* m_lcs     = nullptr;
  AIDA::IHistogram1D* m_nc      = nullptr;
  AIDA::IHistogram1D* m_lcisipm = nullptr;
  AIDA::IHistogram1D* m_lciq    = nullptr;

  std::vector<AIDA::IHistogram1D*> m_lcichpm;
  std::vector<AIDA::IHistogram1D*> m_lcipchpq;
  std::vector<AIDA::IHistogram1D*> m_sipmpq;
  std::vector<AIDA::IHistogram1D*> m_fracpq;
  std::vector<AIDA::IHistogram1D*> m_pseusizepq;
  std::vector<AIDA::IHistogram1D*> m_lcpst;
  AIDA::IHistogram2D*              m_lcsf    = nullptr;
  AIDA::IHistogram2D*              m_lcqsipm = nullptr;

  // Property for drawing histograms per station
  Gaudi::Property<bool> m_drawHistsPerStation{this, "DrawHistsPerStation", true,
                                              "Boolean for drawing histrograms per station"};

  // Property for drawing histograms per quarter
  Gaudi::Property<bool> m_drawHistsPerQuarter{this, "DrawHistsPerQuarter", true,
                                              "Boolean for drawing histrograms per quarter"};

  // Property for drawing histograms per module
  Gaudi::Property<bool> m_drawHistsPerModule{this, "DrawHistsPerModule", true,
                                             "Boolean for drawing histograms per module"};

  // Property for drawing histograms per SiPM
  Gaudi::Property<bool> m_drawHistsPerSiPM{this, "DrawHistsPerSiPM", true, "Boolean for drawing histrograms per SiPM"};
};

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( FTClusterMonitor )

//=============================================================================
FTClusterMonitor::FTClusterMonitor( const std::string& name, ISvcLocator* pSvcLocator )
    : Consumer( name, pSvcLocator,
                {KeyValue{"ClusterLocation", LHCb::FTLiteClusterLocation::Default},
                 KeyValue{"FTDetectorLocation", DeFTDetectorLocation::Default}} ) {}

//=============================================================================
StatusCode FTClusterMonitor::initialize() {
  StatusCode sc = Consumer::initialize(); // must be executed first
  if ( sc.isFailure() ) return sc;

  auto bookHisto = [this]( std::string name, std::string title, double low, double high, uint bins ) {
    AIDA::IHistogram1D* h = book1D( name, title, low, high, bins );
    declareInfo( name, h, title );
    return h;
  };

  m_nc = bookHisto( "nClusters", "Number of clusters; Clusters/event; Events", 0., 1000, 100 );

  // Only create hist when property is set to true
  if ( m_drawHistsPerQuarter ) {
    m_lciq = bookHisto( "LiteClustersPerQuarter", "Number of clusters; Quarter; Clusters", 0., 47., 47 );
  }

  m_lcpm = bookHisto( "LiteClustersPerModule", "Lite clusters per module; Module; Clusters", -0.5, 5.5, 6 );

  m_lcps    = bookHisto( "LiteClustersPerStation", "Lite clusters per station; Station; Clusters", 0.5, 3.5, 3 );
  m_lcpsipm = bookHisto( "LiteClustersPerSiPM", "Lite clusters per SiPM; SiPMID; Clusters", 0., 16., 16 );
  m_lcpc    = bookHisto( "LiteClustersPerChannel", "Lite clusters per channel; Channel; Clusters", -0.5, 127.5, 128 );
  m_lcppc   = bookHisto( "LiteClustersPerPseudoChannel",
                       "Lite clusters per pseudo channel;Pseudo channel;Clusters/(64 channels)", 0., 12288., 192 );
  m_lcf     = bookHisto( "LiteClusterFraction", "Lite cluster fraction; Fraction", -0.25, 0.75, 2 );
  m_lcs     = bookHisto( "LiteClusterSize", "Lite cluster size; Size", -0.5, 8.5, 9 );

  m_lcisipm =
      bookHisto( "LiteClustersInSiPM", "Lite clusters per SiPM; Number of clusters; Number of SiPMs", -0.5, 20.5, 21 );

  // Histograms per station/quarter/module
  size_t prev_stationID( 999 );
  size_t prev_quarter( 999 );
  for ( size_t station = 1; station < 4; station++ ) {
    for ( size_t layer = 0; layer < 4; layer++ ) {
      for ( size_t quarter = 0; quarter < 4; quarter++ ) {
        for ( size_t module = 0; module < 6; module++ ) {

          const auto& SLQ   = "_S" + to_string( station ) + "L" + to_string( layer ) + "Q" + to_string( quarter );
          const auto& QSLQ  = " in quarter" + SLQ + "; ";
          const auto& ISLQ  = "InQuarter" + SLQ;
          const auto& SLQM  = SLQ + "M" + to_string( module );
          const auto& MSLQM = " in module" + SLQM + "; ";

          if ( m_drawHistsPerQuarter && ( quarter != prev_quarter ) ) {

            m_fracpq.push_back(
                bookHisto( "Fraction" + ISLQ, "Fraction" + QSLQ + "Fraction; Number of clusters", -0.25, 0.75, 2 ) );
            m_pseusizepq.push_back( bookHisto( "Pseudosize" + ISLQ,
                                               "Pseudosize" + QSLQ + "Pseudosize; Number of clusters", -0.5, 8.5, 9 ) );
            m_sipmpq.push_back( bookHisto( "ClustersPerSiPM" + ISLQ,
                                           "Clusters per SiPM" + QSLQ + "SiPM number; Number of clusters", 0., 96.,
                                           97 ) );
            m_lcipchpq.push_back(
                bookHisto( "ClustersPerPseudochannel" + ISLQ,
                           "Clusters per Pseudochannel" + QSLQ + "Pseudochannel; Number of clusters/8 pseudochannels",
                           0., 12282., 1536 ) );
          }
          prev_quarter = quarter;
          if ( m_drawHistsPerModule ) {
            m_lcichpm.push_back( bookHisto( "LiteClustersPerChannelinModule" + SLQM,
                                            "Lite Clusters Per Channel" + MSLQM + "Channel number; Number of clusters",
                                            0., 2047., 2048 ) );
          }
          if ( m_drawHistsPerStation && ( station != prev_stationID ) ) {
            m_lcpst.push_back( bookHisto(
                "LiteClustersInStation_S" + to_string( station ),
                "Lite Clusters In Station " + to_string( station ) + "; Module ID; Number of clusters", 0., 96., 97 ) );
          }
          prev_stationID = station;
        }
      }
    }
  }

  // 2D histograms
  m_lcsf = book2D( "LiteClusterSizeVsFraction", "Cluster size vs fraction ; Size; Fraction", -0.5, 8.5, 9, -0.25, 0.75,
                   100 );
  declareInfo( "LiteClusterSizeVsFraction", m_lcsf, "Cluster size vs fraction ; Size; Fraction" );

  m_lcqsipm =
      book2D( "QuarterVsSiPMNumber", "Quarter ID vs SiPMNumber ; Quarter ID; SiPM number", 0., 47., 48, 0., 115., 116 );
  declareInfo( "QuarterVsSiPMNumber", m_lcqsipm, "Quarter ID vs SiPM Number ; Quarter ID; SiPM number" );
  return sc;
}

//=============================================================================
void FTClusterMonitor::operator()( const FTLiteClusters& clusters, const DeFTDetector& deFT ) const {

  if ( deFT.version() < 61 ) {
    throw GaudiException( "This version requires FTDet v6.1 or higher", "", StatusCode::FAILURE );
  };

  m_nc->fill( clusters.size() );
  std::optional<uint> prevSiPM, prevModuleID;
  int                 clustersInSiPM = 0;

  // Loop over FTLiteCluster
  for ( const auto& cluster : clusters.range() ) {
    LHCb::FTChannelID chanID = cluster.channelID();
    const DeFTModule* mod    = deFT.findModule( chanID );

    size_t q_ID          = chanID.moniModuleID();
    uint   SiPM_ID       = chanID.moniSiPMID();
    int    pseudoChannel = mod->pseudoChannel( chanID );

    // Draw cluster channel properties
    m_lcps->fill( chanID.station() );
    m_lcpm->fill( chanID.module() );
    m_lcpsipm->fill( chanID.sipmInModule() );
    m_lcpc->fill( chanID.channel() );
    if ( mod != nullptr ) { m_lcppc->fill( pseudoChannel ); }
    m_lcf->fill( cluster.fraction() );
    m_lcs->fill( cluster.pseudoSize() );
    m_lcsf->fill( cluster.pseudoSize(), cluster.fraction() );
    m_lcqsipm->fill( q_ID, SiPM_ID );

    // Check if property was set, if so fill corresponding hists
    if ( m_drawHistsPerQuarter ) {
      m_lciq->fill( q_ID );
      m_fracpq[q_ID]->fill( cluster.fraction() );
      m_pseusizepq[q_ID]->fill( cluster.pseudoSize() );
      m_sipmpq[q_ID]->fill( SiPM_ID );
      m_lcipchpq[q_ID]->fill( pseudoChannel );
    }
    if ( m_drawHistsPerStation ) { m_lcpst[mod->stationID() - 1]->fill( chanID.moniModuleIDstation() ); }
    if ( m_drawHistsPerModule ) { m_lcichpm[mod->moduleID()]->fill( chanID.moniChannelID() ); }

    // Count the number of clusters per SiPM
    uint thisSiPM = chanID.uniqueSiPM();
    if ( prevSiPM && thisSiPM != *prevSiPM && clustersInSiPM != 0 ) {
      m_lcisipm->fill( clustersInSiPM );
      clustersInSiPM = 0;
    }
    prevSiPM     = thisSiPM;
    prevModuleID = chanID.moniModuleID();
    ++clustersInSiPM;
  }                                  // Loop over FTLiteClusters
  m_lcisipm->fill( clustersInSiPM ); // Fill this for the last time
}
