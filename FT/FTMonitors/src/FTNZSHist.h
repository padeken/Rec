/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef FTNZSHIST
#define FTNZSHIST 1

// from Gaudi
#include "GaudiAlg/GaudiHistoAlg.h"

// from AIDA
#include "AIDA/IHistogram1D.h"
#include "AIDA/IHistogram2D.h"

// from boost
#include <optional>

// from ROOT
#include "TH1D.h"

namespace {
  using std::string;
  using std::to_string;
} // namespace

/** @class FTNZSHist FTNZSHist.h
 *
 *
 *  @author Lex Greeven
 *  @date   2020-06-29
 */

class FTNZSHist {
private:
  string m_histName;
  string m_histTitle;
  string m_xAxisTitle;
  string m_yAxisTitle;
  string m_titleString;
  float  m_xMin;
  float  m_xMax;
  int    m_nBins;

  AIDA::IHistogram1D* m_AIDAHistNZS;
  AIDA::IHistogram1D* m_AIDAHistDigits;

public:
  // Constructor
  FTNZSHist( string histName, string histTitle, string xAxis, string yAxis, float xMin, float xMax, int nBins,
             AIDA::IHistogram1D* histNZS, AIDA::IHistogram1D* histDigits );

  // Make class non-copyable but movable
  FTNZSHist& operator=( const FTNZSHist& ) = delete;
  FTNZSHist( const FTNZSHist& )            = delete;
  FTNZSHist( FTNZSHist&& )                 = default;
  FTNZSHist& operator=( FTNZSHist&& ) = default;

  // Getters
  AIDA::IHistogram1D const* getHistNZS() const { return m_AIDAHistNZS; };
  AIDA::IHistogram1D const* getHistDigits() const { return m_AIDAHistDigits; };
  AIDA::IHistogram1D*       getHistNZS() { return m_AIDAHistNZS; };
  AIDA::IHistogram1D*       getHistDigits() { return m_AIDAHistDigits; };
  const std::string&        getTitleString() const { return m_titleString; };
  const float&              getxMin() const { return m_xMin; };
  const float&              getxMax() const { return m_xMax; };
  const int&                getnBins() const { return m_nBins; };

  // Functions to fill histograms
  template <class T>
  void fillNZS( T value ) const {
    m_AIDAHistNZS->fill( value );
  }

  template <class T>
  void fillDigit( T value ) const {
    m_AIDAHistDigits->fill( value );
  }
};

FTNZSHist::FTNZSHist( string histName, string histTitle, string xAxis, string yAxis, float xMin, float xMax, int nBins,
                      AIDA::IHistogram1D* histNZS, AIDA::IHistogram1D* histDigits )
    : m_histName{std::move( histName )}
    , m_histTitle{std::move( histTitle )}
    , m_xAxisTitle{xAxis}
    , m_yAxisTitle{yAxis}
    , m_titleString{m_histTitle + "; " + m_xAxisTitle + "; " + m_yAxisTitle}
    , m_xMin{xMin}
    , m_xMax{xMax}
    , m_nBins{nBins}
    , m_AIDAHistNZS{histNZS}
    , m_AIDAHistDigits{histDigits} {}

#endif // FTNZSHIST