/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// clang-format off
// STD
#include <algorithm>
#include <cmath>
#include <cstdlib>
#include <map>
#include <mutex>
#include <regex>
#include <sstream>
#include <string>
#include <tuple>

// Gaudi
#include "GaudiKernel/ParsersFactory.h"
#include "GaudiKernel/PhysicalConstants.h"
#include "GaudiKernel/StdArrayAsProperty.h"

// base class
#include "RichFutureRecBase/RichRecHistoAlgBase.h"

// Gaudi functional
#include "GaudiAlg/Consumer.h"

// event model
#include "RichFutureRecEvent/RichRecCherenkovAngles.h"
#include "RichFutureRecEvent/RichRecCherenkovPhotons.h"
#include "RichFutureRecEvent/RichRecPhotonPredictedPixelSignals.h"
#include "RichFutureRecEvent/RichRecRelations.h"
#include "RichFutureRecEvent/RichSummaryEventData.h"

// rich utils
#include "RichUtils/RichPDIdentifier.h"
#include "RichUtils/RichTrackSegment.h"
#include "RichUtils/ZipRange.h"
#include "RichFutureUtils/RichSIMDMirrorData.h"

// track selector
#include "TrackInterfaces/ITrackSelector.h"

// RichDet
#include "RichDet/DeRichSystem.h"

// DetDesc
#include "DetDesc/ConditionAccessorHolder.h"

namespace Rich::Future::Rec::Moni {

  // Use the functional framework
  using namespace Gaudi::Functional;
  using namespace Gaudi::Units;

  /** @class SIMDAlignment
   *
   *  For monitoring the RICH1, RICH2 mirror alignment, fills histograms with
   *  delta-theta vs. phi of the reconstructed CHerenkov photons for a
   *  special set of the primary / secondary mirror combinations.
   *
   *  Follows RichSIMDPhotonCherenkovAngles.cpp as a prototype.
   *
   *  @author Anatoly Solomin anatoly.solomin@cern.ch
   *  @date   2020-03-17
   */

  class SIMDAlignment final : public Consumer<void( const LHCb::Track::Selection&,             //
                                                    const Summary::Track::Vector&,             //
                                                    const Relations::PhotonToParents::Vector&, //
                                                    const LHCb::RichTrackSegment::Vector&,     //
                                                    const CherenkovAngles::Vector&,            //
                                                    const SIMDCherenkovPhoton::Vector&,        //
                                                    const SIMDMirrorData::Vector& ),
                                              Traits::BaseClass_t<HistoAlgBase>> {

  public:
    /// Standard constructor
    SIMDAlignment( const std::string& name, ISvcLocator* pSvcLocator )                                  //
        : Consumer( name, pSvcLocator,                                                                  //
                    {KeyValue{"TracksLocation",           LHCb::TrackLocation::Default},                //
                     KeyValue{"SummaryTracksLocation",    Summary::TESLocations::Tracks},               //
                     KeyValue{"PhotonToParentsLocation",  Relations::PhotonToParentsLocation::Default}, //
                     KeyValue{"TrackSegmentsLocation",    LHCb::RichTrackSegmentLocation::Default},     //
                     KeyValue{"CherenkovAnglesLocation",  CherenkovAnglesLocation::Signal},             //
                     KeyValue{"CherenkovPhotonLocation",  SIMDCherenkovPhotonLocation::Default},        //
                     KeyValue{"PhotonMirrorDataLocation", SIMDMirrorDataLocation::Default}} ) {
      // print some stats on the final plots
      setProperty( "HistoPrint", true ).ignore();
    }

    /// Functional operator
    void operator()( const LHCb::Track::Selection&             tracks,        //
                     const Summary::Track::Vector&             sumTracks,     //
                     const Relations::PhotonToParents::Vector& photToSegPix,  //
                     const LHCb::RichTrackSegment::Vector&     segments,      //
                     const CherenkovAngles::Vector&            expTkCKThetas, //
                     const SIMDCherenkovPhoton::Vector&        photons,       //
                     const SIMDMirrorData::Vector&             mirrorData ) const override;

  protected:
    /// Pre-book all histograms
    StatusCode prebookHistograms() override;

  private:

    // dictionary of delta-theta vs. phi histograms
    std::map<std::string, AIDA::IHistogram2D*> h_alignHistoRec2D;
    // some more 1D hitograms
    std::map<std::string, AIDA::IHistogram1D*> h_alignHistoRec1D;

    // properties and tools

    /// minimum beta value for tracks
    Gaudi::Property<RadiatorArray<float>>      m_minBeta           {this, "MinBeta",          {0.9999f, 0.9999f, 0.9999f}}; ///
    /// maximum beta value for tracks
    Gaudi::Property<RadiatorArray<float>>      m_maxBeta           {this, "MaxBeta",          {999.99f, 999.99f, 999.99f}}; ///
    /// minimum momentum value for tracks
    Gaudi::Property<float>                     m_minPtot           {this, "MinPtot",          50.*GeV     }; ///<saturation momenum for Rich2
    Gaudi::Property<unsigned int>              m_nPhiBins          {this, "NPhiBins",         60          }; ///<for Rich2
                           // previously used  m_nPhiBins values:                             25, 20
    Gaudi::Property<unsigned int>              m_nThetaBins        {this, "NThetaBins",       50          }; ///<for Rich2
    /// mirrors of which radiator to align?
    Gaudi::Property<std::vector<std::string>>  m_radName           {this, "RichGases",        {"Rich2Gas"}}; ///<default
    Gaudi::Property<bool>                      m_useMCTruth        {this, "UseMCTruth",       false       }; ///
    Gaudi::Property<int>                       m_histoOutputLevel  {this, "HistoOutputLevel", 3           }; ///
    Gaudi::Property<std::vector<std::string>>  m_prebookHistos     {this, "PrebookHistos"                 }; ///

    Gaudi::Property<std::vector<std::string>>  m_alignmentTasks    {this, "AlignmentTasks",   {"Produce"} }; ///

    // dictionary of tasks
    std::map<std::string, bool> alignmentTasks = {
      {"Produce",   false},
      {"Monitor",   false},
      {"Map",       false},
      {"Optimize",  false},
      {"Explore",   false},
    };

    ToolHandle<const ITrackSelector> m_tkSel{this, "TrackSelector", "TrackSelector"};

     float                    deltaThetaRange = 0.; // round the delta-theta limits of the histograms
     Gaudi::Property<float> m_deltaThetaRange {this, "DeltaThetaRange", 0.004,
                            [&](const auto&) {
                                float roundingOrder = std::round( std::pow( 10., ( -std::floor(std::log10( m_deltaThetaRange ) ) ) ) );
                                deltaThetaRange     = std::round( m_deltaThetaRange * roundingOrder ) / roundingOrder;
                            }, Gaudi::Details::Property::ImmediatelyInvokeHandler{true} } ; ///<for Rich2

    // accumulative dictionary-counter of photons per any mirror combination,
    // that have at least one common photon emitted by a saturated
    // [unambiguous] track;
    // this is only for the "Optimize" task
    mutable std::map<std::string, double> photCountSaturTr4Opt;

    /// mutex lock
    mutable std::mutex m_fill_histogram_lock;
  };

} // namespace Rich::Future::Rec::Moni

using namespace Rich::Future::Rec::Moni;

//=============================================================================
// Initialization
//=============================================================================
//=============================================================================
// Book histograms
//=============================================================================
StatusCode SIMDAlignment::prebookHistograms() {
  using namespace Gaudi::Units;

  const auto radName = m_radName[0];
  // which radiator ?
  const auto rad     = ( radName == "Rich2Gas" ? Rich::Rich2Gas : Rich::Rich1Gas );

  // "un-false" tasks, those in m_alignmentTasks
  for ( auto& task : m_alignmentTasks )
    alignmentTasks.at(task) = true;

  bool ok = true;
  // Book histograms for an optimal subset of mirror combinations determined
  // in advance. Their list is stored in a comma-separated vector of strings,
  // e.g. '0000','0103',..., where the first  two digits is a primary
  // mirror segment number, while the second two digits is a secondary mirror
  // segment number.
  std::string h_id  = "";
  std::string title = "";
  for ( const auto& combin : m_prebookHistos ) {

    const auto& pri = combin.substr( 0, 2 );
    const auto& sec = combin.substr( 2, 2 );

    if ( alignmentTasks.at("Produce") ) {
      std::string h_id;
      std::string title;
      h_id  = std::string("dThetaPhiRecSaturTr"+combin);
      title = std::string("Ch. delta theta vs phi saturated tracks: pri "+pri+" sec "+sec+" "+radName);
      ok &= saveAndCheck( h_alignHistoRec2D[h_id],
                          richHisto2D( HID( h_id, rad ), title,
                                       0., 2.*pi, m_nPhiBins,
                                       -deltaThetaRange, deltaThetaRange, m_nThetaBins ) );
    }
    if ( alignmentTasks.at("Explore") ) {
      h_id  = std::string("dThetaPhiRecSaturTr4Symm"+combin);
      title = std::string("Ch. delta theta vs phi saturated tracks symmetrized: pri "+pri+" sec "+sec+" "+radName);
      ok &= saveAndCheck( h_alignHistoRec2D[h_id],
                          richHisto2D( HID( h_id, rad ), title,
                                       0., 2.*pi, m_nPhiBins,
                                       -deltaThetaRange, deltaThetaRange, m_nThetaBins ) );
      h_id  = std::string("dThetaPhiRecAllTrSymm"+combin);
      title = std::string("Ch. delta theta vs phi all tracks symmetrized: pri "+pri+" sec "+sec+" "+radName);
      ok &= saveAndCheck( h_alignHistoRec2D[h_id],
                          richHisto2D( HID( h_id, rad ), title,
                                       0., 2.*pi, m_nPhiBins,
                                       -deltaThetaRange, deltaThetaRange, m_nThetaBins ) );
    }
    if ( alignmentTasks.at("Map") ) {
      h_id  = std::string("thetaPhiRecSaturTr"+combin);
      title = std::string("Satur. tr. map: pri "+pri+" sec "+sec+" "+radName);
      ok &= saveAndCheck( h_alignHistoRec2D[h_id],
                          richHisto2D( HID( h_id, rad ), title,
                                       -pi, pi,    m_nPhiBins,
                                       0.,  pi/4., m_nThetaBins ) );

      h_id  = std::string("numPhotsRecSaturTr"+combin);
      title = std::string("Satur. tr. Ch. phots: pri "+pri+" sec "+sec+" "+radName);
      ok &= saveAndCheck( h_alignHistoRec1D[h_id],
                          richHisto1D( HID( h_id, rad ), title,
                                       0.,  100., 100 ) );
    }
  }
  return StatusCode{ok};
}

//=============================================================================
// Main execution
//=============================================================================
void SIMDAlignment::operator()( const LHCb::Track::Selection&             tracks,        //
                                const Summary::Track::Vector&             sumTracks,     //
                                const Relations::PhotonToParents::Vector& photToSegPix,  //
                                const LHCb::RichTrackSegment::Vector&     segments,      //
                                const CherenkovAngles::Vector&            expTkCKThetas, //
                                const SIMDCherenkovPhoton::Vector&        photons,       //
                                const SIMDMirrorData::Vector&             mirrorData ) const {

  // the lock
  std::scoped_lock lock{m_fill_histogram_lock};

  using namespace Gaudi::Units;

  auto radName = m_radName[0];
  auto rad     = ( radName == "Rich2Gas" ? Rich::Rich2Gas : Rich::Rich1Gas );

  // loop over the track containers
  for ( const auto&& [tk, sumTk] : Ranges::ConstZip( tracks, sumTracks ) ) {

    // is this track selected ?
    if ( ! m_tkSel.get()->accept( *tk ) ) continue;

    // declare track's phi, theta etc.
    float       trPhiRec   = 0.;
    float       trThetaRec = 0.;
    float       pTot       = 0.;
    std::string h_id       = "";

    // declare counter of useful photons produced by this track (if its momentum
    // is beyond saturation) in the chosen radiator, per mirror combination
    // from the production subset to which its photon groups contribute; there
    // can be more than one such combination; this is for the "Map" task
    std::map<std::string, double> photCountSaturTr4Map;

    // list of mirror combinations only from the production subset, to which
    // current track contributes its photons; this is for the "Map" task
    std::vector<std::string> mirrCombsSaturTr4Map;

    std::string pri;
    std::string sec;

    // loop over photons for this track
    for ( const auto photIn : sumTk.photonIndices() ) {

      const auto& rels          = photToSegPix[photIn];

      // the segment for this photon
      const auto& seg           = segments[rels.segmentIndex()];

      // radiator of this segment
      const auto  segRad        = seg.radiator();

      // we work with only one RICH at a time
      if ( segRad != rad ) continue;

      // photon data
      const auto& phot          = photons[photIn];

      // segment momentum
      pTot                      = seg.bestMomentumMag();

      // The PID type to assume. Just use Pion here.
      const auto  pid           = Rich::Pion;

      // beta
      const auto  beta          = richPartProps()->beta( pTot, pid );

      // selection cuts
      if ( beta < m_minBeta[rad] || m_maxBeta[rad] < beta ) continue;

      // segment angles
      trPhiRec                  = seg.bestMomentum().Phi();
      trThetaRec                = seg.bestMomentum().Theta();

      // mirror data
      const auto& mirrordata    = mirrorData[photIn];

      // get the expected CK theta values for this segment
      const auto& expCKangles   = expTkCKThetas[rels.segmentIndex()];

      // expected CK theta
      const auto  thetaExp      = expCKangles[pid];

      // mirror segment numbers
      const auto& mirrorDataPri = mirrordata.primaryMirrors();
      const auto& mirrorDataSec = mirrordata.secondaryMirrors();

      // loop over scalar entries in SIMD photon
      for ( std::size_t i = 0; i < SIMDCherenkovPhoton::SIMDFP::Size; ++i ) {

        // Select valid entries
        if ( phot.validityMask()[i] ) {

          // reconstructed theta
          const auto thetaRec    = phot.CherenkovTheta()[i];
          // reconstructed phi
          const auto phiRec      = phot.CherenkovPhi()[i];
          // reconstructed delta theta
          const auto dTheta      = thetaRec - thetaExp;

          const auto& mirrorSegmentPri = mirrorDataPri[i]->mirrorNumber();
          const auto& mirrorSegmentSec = mirrorDataSec[i]->mirrorNumber();

          std::ostringstream oss;
          oss <<std::setw(2)<<std::setfill('0')<<mirrorSegmentPri;
          oss <<std::setw(2)<<std::setfill('0')<<mirrorSegmentSec;
          const auto& mirrComb = oss.str();

          pri = mirrComb.substr( 0, 2 ); // in case of RICH1 it is quadrant number

          sec = mirrComb.substr( 2, 2 );

          // check whether this combination of the mirror segments belongs to
          // the predefined subset, i.e. is in the list of chosen combinations
          // of the  mirror segment numbers
          for ( const auto& combin : m_prebookHistos ) {

            if ( mirrComb == combin ) {

              // only Ch photons from tracks with momentum higher than the
              // CHerenkov angle saturation
              if ( pTot > m_minPtot && alignmentTasks.at("Produce") ) {
                // fill the production histograms from the dictionary
                h_id = std::string("dThetaPhiRecSaturTr"+mirrComb );
                h_alignHistoRec2D.at( h_id )->fill( phiRec, dTheta );
              }
              if ( pTot > m_minPtot && alignmentTasks.at("Map") ) {
                // this is a useful photon - count it for this mirror
                // combination from the production subset for the current track
                photCountSaturTr4Map[mirrComb] += 1.;
                // memorize this mirror combination from the production subset
                // for the current track
                if ( std::none_of( mirrCombsSaturTr4Map.begin(), mirrCombsSaturTr4Map.end(), [&](auto s){ return s == mirrComb; } ) )
                  mirrCombsSaturTr4Map.push_back( mirrComb );
              }

              // some on-demand exercises, and only in case of RICH1
              if ( radName == "Rich1Gas" && alignmentTasks.at("Explore") ) {
                // few tricks to make the histograms look intuitively symmetric

                // reconstructed phi
                auto phiRecSymm      = phiRec;
                // reconstructed delta theta
                auto dThetaSymm      = dTheta;
                // first, flip the deltaTheta
                if ( pri == "03"  ||  pri == "02" )
                  dThetaSymm = -dThetaSymm;
                // secondly, change direction and shift for the phi
                if      ( pri == "00" )
                  phiRecSymm += pi;
                else if ( pri == "02" )
                  phiRecSymm  = - phiRecSymm;
                else if ( pri == "03" )
                  phiRecSymm  = - phiRecSymm + pi;
                // finally, restrict phi to 0 - 2pi range
                if      ( phiRecSymm < 0. )
                  phiRecSymm += 2.*pi;
                else if ( phiRecSymm > 2.*pi )
                  phiRecSymm -= 2.*pi;

                // only Chernkov photons from tracks with momentum higher
                // than the CHerenkov angle saturation
                if ( pTot > m_minPtot ) {
                  h_id = std::string("dThetaPhiRecSaturTr4Symm"+mirrComb );
                  h_alignHistoRec2D.at( h_id )->fill( phiRecSymm, dThetaSymm );
                }
                // Chernkov photons from all tracks
                h_id = std::string("dThetaPhiRecAllTrSymm"+mirrComb );
                h_alignHistoRec2D.at( h_id )->fill( phiRecSymm, dThetaSymm );
              } // alignmentTasks.at("Explore")

              break; // combination is found in the list, things done - exit
                     // from the loop
            } // this combination of the mirror segments is in the list
          } // m_prebookHistos loop

          // only Ch photons from tracks with momentum higher than the
          // CHerenkov angle saturation
          if ( pTot > m_minPtot && alignmentTasks.at("Optimize") ) {
            // find all combinations of mirrors that have photons from this track

            // If there is no element with the mirrComb key, it is added and = 1
            // otherwise -- simply incremented.
            // The care is taken about thread-safety.

            photCountSaturTr4Opt[mirrComb] += 1.;

          } // alignmentTasks.at("Optimize")
        } // valid scalars
      } // SIMD loop
    } // loop over photons for this track

    // doing the following two tasks is OK, because we use only one radiator
    // (i.e. one segment) at a time, and therefore, trPhiRec, trThetaRec are
    // unique from the previous loop over photons (and hence, segments) for this
    // track

    // only with momentum higher than the CHerenkov angle saturation
    if ( pTot > m_minPtot && alignmentTasks.at("Map") ) {
      // loop over mirror combinations from the production subset
      // to which current track contributed its photons
      for ( const auto& mirrComb : mirrCombsSaturTr4Map ) {
        // fill control histograms for photon-contributing tracks
        if ( photCountSaturTr4Map.find(mirrComb) != photCountSaturTr4Map.end() ) {
          std::string h_id{};
          // fill the track's angular region
          h_id = std::string("thetaPhiRecSaturTr"+mirrComb );
          h_alignHistoRec2D.at( h_id )->fill( trPhiRec, trThetaRec );
          // fill photon conters per track
          h_id = std::string("numPhotsRecSaturTr"+mirrComb );
          h_alignHistoRec1D.at( h_id )->fill( photCountSaturTr4Map.at( mirrComb ) );
        }
      }
    } // alignmentTasks.at("Map")
  } // loop over the track containers
}
// Declaration of the Algorithm Factory
DECLARE_COMPONENT( SIMDAlignment )
//-----------------------------------------------------------------------------
// clang-format on
