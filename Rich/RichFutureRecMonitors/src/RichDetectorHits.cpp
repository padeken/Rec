/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

// base class
#include "RichFutureRecBase/RichRecHistoAlgBase.h"

// Gaudi Functional
#include "GaudiAlg/Consumer.h"

// Rich Utils
#include "RichFutureUtils/RichDecodedData.h"
#include "RichUtils/RichDAQDefinitions.h"

// STD
#include <mutex>

namespace Rich::Future::Rec::Moni {

  // Use the functional framework
  using namespace Gaudi::Functional;

  /** @class DetectorHits RichDetectorHits.h
   *
   *  Monitors the data sizes in the RICH detectors.
   *
   *  @author Chris Jones
   *  @date   2016-12-06
   */

  class DetectorHits final : public Consumer<void( const DAQ::DecodedData& ), Traits::BaseClass_t<HistoAlgBase>> {

  public:
    /// Standard constructor
    DetectorHits( const std::string& name, ISvcLocator* pSvcLocator )
        : Consumer( name, pSvcLocator, //
                    KeyValue{"DecodedDataLocation", DAQ::DecodedDataLocation::Default} ) {
      // print some stats on the final plots
      setProperty( "HistoPrint", true ).ignore();
    }

  public:
    /// Functional operator
    void operator()( const DAQ::DecodedData& data ) const override {

      // Count active PDs in each RICH
      DetectorArray<unsigned long long> activePDs = {{}};
      // Count hits in each RICH
      DetectorArray<unsigned long long> richHits = {{}};
      // Count active modules
      DetectorArray<unsigned long long> richModules = {{}};

      // the lock
      std::lock_guard lock( m_updateLock );

      // Loop over RICHes
      for ( const auto rich : activeDetectors() ) {

        // data for this RICh
        const auto& rD = data[rich];

        // sides per RICH
        for ( const auto& pD : rD ) {

          // PD modules per side
          richModules[rich] += pD.size();
          for ( const auto& mD : pD ) {

            // PDs per module
            for ( const auto& PD : mD ) {

              // PD ID
              const auto pdID = PD.pdID();
              if ( pdID.isValid() ) {

                // Vector of SmartIDs
                const auto& rawIDs = PD.smartIDs();

                // Do we have any hits
                if ( !rawIDs.empty() ) {

                  // Fill average HPD occ plot
                  fillHisto( h_nTotalPixsPerPD[rich], rawIDs.size() );

                  // count active PDs
                  ++activePDs[rich];

                  // count hits
                  richHits[rich] += rawIDs.size();

                } // PD has hits

              } // not inhibited

            } // PDs

          } // modules

        } // panels

      } // RICHes

      // Loop over RICHes
      for ( const auto rich : activeDetectors() ) {
        // Fill active PD plots
        fillHisto( h_nActivePDs[rich], activePDs[rich] );
        // Fill RICH hits plots
        if ( richHits[rich] > 0 ) {
          fillHisto( h_nTotalPixs[rich], richHits[rich] );
          fillHisto( h_nActiveModules[rich], richModules[rich] );
        }
      }
    }

  protected:
    /// Pre-Book all histograms
    StatusCode prebookHistograms() override {
      bool ok = true;
      for ( const auto rich : activeDetectors() ) {
        ok &= saveAndCheck( h_nTotalPixsPerPD[rich],                                   //
                            richHisto1D( Rich::HistogramID( "nTotalPixsPerPD", rich ), //
                                         "Average overall PD occupancy (nHits>0)", 0.5, 64.5, 65 ) );
        ok &= saveAndCheck( h_nTotalPixs[rich],                                   //
                            richHisto1D( Rich::HistogramID( "nTotalPixs", rich ), //
                                         "Overall occupancy (nHits>0)", 0, m_maxPixels, nBins1D() ) );
        ok &= saveAndCheck( h_nActivePDs[rich],                                   //
                            richHisto1D( Rich::HistogramID( "nActivePDs", rich ), //
                                         "# Active PDs (nHits>0)", -0.5, 2000.5, 2001 ) );
        ok &= saveAndCheck( h_nActiveModules[rich],                                   //
                            richHisto1D( Rich::HistogramID( "nActiveModules", rich ), //
                                         "# Active PD Modules (nHits>0)", -0.5, 150.5, 151 ) );
      }
      return StatusCode{ok};
    }

  private:
    /// Maximum pixels
    Gaudi::Property<unsigned int> m_maxPixels{this, "MaxPixels", 10000u};

  private:
    // data

    /// mutex lock
    mutable std::mutex m_updateLock;

  private:
    // cached histograms

    /// Pixels per PD histograms
    DetectorArray<AIDA::IHistogram1D*> h_nTotalPixsPerPD = {{}};
    /// Pixels per detector histograms
    DetectorArray<AIDA::IHistogram1D*> h_nTotalPixs = {{}};
    /// Number active PDs histograms
    DetectorArray<AIDA::IHistogram1D*> h_nActivePDs = {{}};
    /// Number active module histograms
    DetectorArray<AIDA::IHistogram1D*> h_nActiveModules = {{}};
  };

  // Declaration of the Algorithm Factory
  DECLARE_COMPONENT( DetectorHits )

} // namespace Rich::Future::Rec::Moni
