/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

// STD
#include <mutex>

// Gaudi
#include "GaudiKernel/ParsersFactory.h"
#include "GaudiKernel/PhysicalConstants.h"
#include "GaudiKernel/StdArrayAsProperty.h"

// base class
#include "RichFutureRecBase/RichRecHistoAlgBase.h"

// Gaudi Functional
#include "GaudiAlg/Consumer.h"

// Rich Utils
#include "RichUtils/ZipRange.h"

// Rec Event
#include "RichFutureRecEvent/RichRecGeomEfficiencies.h"

// Utils
#include "RichUtils/RichTrackSegment.h"

namespace Rich::Future::Rec::Moni {

  // Use the functional framework
  using namespace Gaudi::Functional;

  /** @class GeometricalEfficiencies
   *
   *  Monitors the RICH segment geometrical efficiencies
   *
   *  @author Chris Jones
   *  @date   2020-03-30
   */

  class GeometricalEfficiencies final : public Consumer<void( const LHCb::RichTrackSegment::Vector&, //
                                                              const GeomEffs::Vector& ),             //
                                                        Traits::BaseClass_t<HistoAlgBase>> {

  public:
    /// Standard constructor
    GeometricalEfficiencies( const std::string& name, ISvcLocator* pSvcLocator )
        : Consumer( name, pSvcLocator,
                    // input data
                    {KeyValue{"TrackSegmentsLocation", LHCb::RichTrackSegmentLocation::Default},
                     KeyValue{"GeomEffsLocation", GeomEffsLocation::Default}} ) {
      // print some stats on the final plots
      setProperty( "HistoPrint", true ).ignore();
      // debug
      // setProperty( "OutputLevel", MSG::VERBOSE );
    }

  public:
    /// Functional operator
    void operator()( const LHCb::RichTrackSegment::Vector& segments, //
                     const GeomEffs::Vector&               geoms ) const override {

      // the lock
      std::lock_guard lock( m_updateLock );

      // loop over segments and geom. eff. data
      for ( auto&& [segment, geom] : Ranges::ConstZip( segments, geoms ) ) {

        // Radiator info
        const auto rad = segment.radiator();
        if ( !radiatorIsActive( rad ) ) continue;

        // loop over mass hypos
        for ( const auto pid : activeParticlesNoBT() ) {
          // fill histo (if > 0 so above threshold)
          if ( geom[pid] > 0 ) { fillHisto( h_geoms[rad][pid], geom[pid] ); }
        }

      } // segment loop
    }

  protected:
    /// Pre-Book all histograms
    StatusCode prebookHistograms() override {
      bool ok = true;
      // loop over active radiators
      for ( const auto rad : activeRadiators() ) {
        // loop over active mass hypos
        for ( const auto pid : activeParticlesNoBT() ) {
          // book yield histo
          ok &= saveAndCheck( h_geoms[rad][pid],                   //
                              richHisto1D( HID( "eff", rad, pid ), //
                                           "Geom. Eff. (>0)", 0, 1, nBins1D() ) );
        }
      }
      return StatusCode{ok};
    }

  private:
    // data

    /// mutex lock
    mutable std::mutex m_updateLock;
    /// Yield histograms
    RadiatorArray<ParticleArray<AIDA::IHistogram1D*>> h_geoms = {{}};
  };

  // Declaration of the Algorithm Factory
  DECLARE_COMPONENT( GeometricalEfficiencies )

} // namespace Rich::Future::Rec::Moni
