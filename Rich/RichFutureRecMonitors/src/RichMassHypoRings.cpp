/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

// STD
#include <mutex>
#include <numeric>

// Gaudi
#include "GaudiKernel/ParsersFactory.h"
#include "GaudiKernel/PhysicalConstants.h"
#include "GaudiKernel/StdArrayAsProperty.h"

// base class
#include "RichFutureRecBase/RichRecHistoAlgBase.h"

// Gaudi Functional
#include "GaudiAlg/Consumer.h"

// Rich Utils
#include "RichUtils/RichTrackSegment.h"
#include "RichUtils/ZipRange.h"

// Rec Event
#include "RichFutureRecEvent/RichRecCherenkovAngles.h"
#include "RichFutureRecEvent/RichRecMassHypoRings.h"
#include "RichFutureRecEvent/RichRecSpacePoints.h"

// Kernel
#include "LHCbMath/FastMaths.h"

namespace Rich::Future::Rec::Moni {

  // Use the functional framework
  using namespace Gaudi::Functional;

  /** @class MassHypoRings
   *
   *  Monitors the RICH mass hypothesis rings
   *
   *  @author Chris Jones
   *  @date   2020-03-30
   */

  class MassHypoRings final : public Consumer<void( const LHCb::RichTrackSegment::Vector&,  //
                                                    const CherenkovAngles::Vector&,         //
                                                    const SegmentPanelSpacePoints::Vector&, //
                                                    const MassHypoRingsVector& ),           //
                                              Traits::BaseClass_t<HistoAlgBase>> {

  public:
    /// Standard constructor
    MassHypoRings( const std::string& name, ISvcLocator* pSvcLocator )
        : Consumer( name, pSvcLocator,
                    // input data
                    {KeyValue{"TrackSegmentsLocation", LHCb::RichTrackSegmentLocation::Default},
                     KeyValue{"CherenkovAnglesLocation", CherenkovAnglesLocation::Signal},
                     KeyValue{"TrackLocalPointsLocation", SpacePointLocation::SegmentsLocal},
                     KeyValue{"MassHypothesisRingsLocation", MassHypoRingsLocation::Emitted}} ) {
      // print some stats on the final plots
      setProperty( "HistoPrint", true ).ignore();
      // debug
      // setProperty( "OutputLevel", MSG::VERBOSE );
    }

  public:
    /// Functional operator
    void operator()( const LHCb::RichTrackSegment::Vector&  segments,     ///< Track Segments
                     const CherenkovAngles::Vector&         ckAngles,     ///< CK theta angles
                     const SegmentPanelSpacePoints::Vector& trHitPntsLoc, ///< segment hit points
                     const MassHypoRingsVector&             massRings ) const override {

      // the lock
      std::lock_guard lock( m_updateLock );

      // loop over rings
      for ( auto&& [segment, ckT, tkLocPtn, ring] : Ranges::ConstZip( segments, ckAngles, trHitPntsLoc, massRings ) ) {

        // Which rad
        const auto rad = segment.radiator();
        if ( radiatorIsActive( rad ) ) {

          // ring reuse flagds
          ParticleArray<bool> reused{{false}};

          // loop over IDs and deduce the reuse
          for ( const auto id : activeParticlesNoBT() ) {

            // Is the ring reused ?
            if ( ring.massAlias( id ) != id ) {
              // label this hypo and the one it is aliased to as being reused
              reused[id]                   = true;
              reused[ring.massAlias( id )] = true;
            }

            // saved vector of seperations
            std::vector<std::pair<double, double>> seps;
            seps.reserve( ring[id].size() );
            // accumlate sum of seperations for mean determination
            double sumSep = 0.0;

            // loop over points in the ring
            for ( const auto& p : ring[id] ) {
              // ring point position in local (corrected) coords
              const auto& lPosR = p.localPosition();
              // detector side
              const auto side = p.smartID().panel();
              // segment impact point on this side
              const auto& lPosT = tkLocPtn.point( side );
              // track point seperation
              const auto dx  = lPosR.x() - lPosT.x();
              const auto dy  = lPosR.y() - lPosT.y();
              const auto sep = std::sqrt( ( dx * dx ) + ( dy * dy ) );
              seps.emplace_back( LHCb::Math::fast_atan2( dy, dx ), sep );
              sumSep += sep;
            }

            if ( LIKELY( !seps.empty() ) ) {
              // compute average seperation for this ring
              const auto avSep = sumSep / seps.size();
              fillHisto( h_sepVckt[rad], ckT[id], avSep );
              // fill deviations from average into histo
              for ( const auto& [phi, sep] : seps ) {
                const auto sepDiff = sep - avSep;
                fillHisto( h_ringPointSegSep[rad][id], sepDiff );
                fillHisto( h_ringPointSegSepVPhi[rad][id], phi, sepDiff );
              }
            }
          }

          // fill reuse histos
          for ( const auto id : activeParticlesNoBT() ) {
            // only fill if ring not empty (i.e. above threshold)
            if ( !ring[id].empty() ) { fillHisto( h_ringReuse[rad], id, reused[id] ? 100 : 0 ); }
          }
        }
      }
    }

  protected:
    /// Pre-Book all histograms
    StatusCode prebookHistograms() override {
      bool ok = true;
      // loop over active radiators
      for ( const auto rad : activeRadiators() ) {
        // book ring reuse histo
        ok &= saveAndCheck( h_ringReuse[rad],                                                              //
                            richProfile1D( HID( "ringReuse", rad ),                                        //
                                           "Ring Reuse (%) V Mass Hypothesis",                             //
                                           -0.5, Rich::NRealParticleTypes - 0.5, Rich::NRealParticleTypes, //
                                           "Ring Reuse (%)", "Mass Hypothesis",                            //
                                           {"El", "Mu", "Pi", "Ka", "Pr", "De"} ) );
        // Sep versus CKT
        ok &= saveAndCheck( h_sepVckt[rad],                               //
                            richProfile1D( HID( "sepVckt", rad ),         //
                                           "Local Seperation V CK Theta", //
                                           m_ckThetaMin[rad], m_ckThetaMax[rad], nBins1D() ) );
        // loop over active particle types
        for ( const auto id : activeParticlesNoBT() ) {
          // point/segment seperation derivation from average
          const Rich::RadiatorArray<double> sepHistR{100, 20, 50};
          ok &= saveAndCheck( h_ringPointSegSep[rad][id],                                     //
                              richHisto1D( HID( "pntSegSepDev", rad, id ),                    //
                                           "Ring - Segment local point seperation deviation", //
                                           -sepHistR[rad], sepHistR[rad], nBins1D(),          //
                                           "Rich-Segment local point seperation / mm" ) );
          // point/segment seperation derivation from average
          ok &= saveAndCheck( h_ringPointSegSepVPhi[rad][id],                                                  //
                              richProfile1D( HID( "pntSegSepDevVPhi", rad, id ),                               //
                                             "Ring - Segment local point seperation deviation V Ring Azimuth", //
                                             -M_PI, M_PI, nBins1D(),                                           //
                                             "Ring azimuthal angle / rad",                                     //
                                             "Rich-Segment local point seperation / mm" ) );
        }
      }
      return StatusCode{ok};
    }

  private:
    // properties

    /// Min theta limit for histos for each rad
    Gaudi::Property<RadiatorArray<float>> m_ckThetaMin{this, "ChThetaRecHistoLimitMin", {0.150f, 0.010f, 0.010f}};

    /// Max theta limit for histos for each rad
    Gaudi::Property<RadiatorArray<float>> m_ckThetaMax{this, "ChThetaRecHistoLimitMax", {0.325f, 0.056f, 0.033f}};

  private:
    /// mutex lock
    mutable std::mutex m_updateLock;
    /// Ring Reuse histograms
    RadiatorArray<AIDA::IProfile1D*> h_ringReuse = {{}};
    /// Deviations in point - segment ring seperation
    RadiatorArray<ParticleArray<AIDA::IHistogram1D*>> h_ringPointSegSep     = {{}};
    RadiatorArray<ParticleArray<AIDA::IProfile1D*>>   h_ringPointSegSepVPhi = {{}};
    /// Correlation between CHerenkov theta and local segment-point seperation
    RadiatorArray<AIDA::IProfile1D*> h_sepVckt = {{}};
  };

  // Declaration of the Algorithm Factory
  DECLARE_COMPONENT( MassHypoRings )

} // namespace Rich::Future::Rec::Moni
