#!/usr/bin/env python

###############################################################################
# (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

from __future__ import print_function
import glob, os
import subprocess
import multiprocessing
from contextlib import closing


def merge(dir):

    # Find files in sub-jobs
    files = sorted(glob.glob(dir + "/jobs/job*/*.root"))
    print("Found", len(files), "files to merge in", dir)
    if len(files) > 0:

        # group files by base name
        merge_list = {}
        for f in files:
            basef = os.path.basename(f)
            if basef not in merge_list.keys(): merge_list[basef] = []
            merge_list[basef] += [f]

        # loop over different files
        for bf in merge_list.keys():
            # run merge
            subprocess.call(
                ["nice", "-n10", "hadd", "-v", "0", "-ff", dir + "/" + bf] +
                merge_list[bf])
            # delete input files
            subprocess.call(["rm"] + merge_list[bf])

        # tar up jobs directory and remove
        subprocess.call([
            "nice", "-n10", "tar", "-jcf", dir + "/jobs.tar.bz2", dir + "/jobs"
        ])
        subprocess.call(["rm", "-r", dir + "/jobs"])


if __name__ == '__main__':

    # Find jobs to merge
    dirs = sorted(glob.glob("CondorJobs/*"))
    print("Found", len(dirs), "directories to merge")

    # Get the number of processors available
    num_cpu = min(8, multiprocessing.cpu_count())
    print("Using", num_cpu, "processing pools")
    # threads
    threads = []

    # Run jobs in parrallel using process pool
    with closing(multiprocessing.Pool(num_cpu)) as pool:
        pool.map(merge, dirs)
        pool.terminate()
