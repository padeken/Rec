###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

# --------------------------------------------------------------------------------------

from Gaudi.Configuration import *
from GaudiConfig.ControlFlow import seq
from Configurables import CondDB, LHCbApp, GaudiSequencer
import os

# --------------------------------------------------------------------------------------
# General configs

# histograms
#histos = "OfflineFull"
histos = "Expert"

# ROOT persistency
ApplicationMgr().HistogramPersistency = "ROOT"
from Configurables import RootHistCnv__PersSvc
RootHistCnv__PersSvc('RootHistCnv').ForceAlphaIds = True
myBuild = os.environ['User_release_area'].split('/')[-1]
myConf = os.environ['CMTCONFIG']
rootFileBaseName = myBuild + "-" + myConf + "-" + histos
HistogramPersistencySvc().OutputFile = rootFileBaseName + "-Histos.root"

# Event numbers
nEvents = 10000
EventSelector().PrintFreq = 100
#LHCbApp().SkipEvents = 5416

# Just to initialise
CondDB()
LHCbApp()

# Timestamps in messages
#LHCbApp().TimeStamp = True

msgSvc = getConfigurable("MessageSvc")
#msgSvc.setVerbose += [ "DeRichGasRadiator" ]
#msgSvc.setVerbose += [ "DeRichSystem" ]
#msgSvc.setVerbose += [ "DeRichHPD" ]
msgSvc.Format = "% F%40W%S%7W%R%T %0W%M"
from Configurables import SequencerTimerTool
SequencerTimerTool("ToolSvc.SequencerTimerTool").NameSize = 40

# Auditors
#AuditorSvc().Auditors += [ "FPEAuditor" ]
#from Configurables import FPEAuditor
#FPEAuditor().ActivateAt = ["Execute"]
#AuditorSvc().Auditors += [ "NameAuditor" ]

# The overall sequence. Filled below.
all = GaudiSequencer("All", MeasureTime=True)

# Finally set up the application
ApplicationMgr(
    TopAlg=[all],
    EvtMax=nEvents,  # events to be processed
    ExtSvc=['ToolSvc', 'AuditorSvc'],
    AuditAlgorithms=True)

# --------------------------------------------------------------------------------------

# Fetch required data from file
from Configurables import Gaudi__Hive__FetchDataFromFile as FetchDataFromFile
fetcher = FetchDataFromFile('FetchDSTData')
fetcher.DataKeys = ['Trigger/RawEvent', 'Rich/RawEvent']
all.Members += [fetcher]

# First various raw event decodings

from Configurables import createODIN
odinDecode = createODIN("ODINFutureDecode")
all.Members += [odinDecode]

from Configurables import Rich__Future__RawBankDecoder as RichDecoder
richDecode = RichDecoder("RichFutureDecode")
all.Members += [richDecode]

# clustering
from Configurables import Rich__Future__SmartIDClustering as RichClustering
clusters = RichClustering("RichClustering")
all.Members += [clusters]

# Positions
#from Configurables import Rich__Future__Rec__PixelClusterGlobalPositions as RichGlobalPoints
#from Configurables import Rich__Future__Rec__PixelClusterLocalPositions  as RichLocalPoints
#from Configurables import Rich__Future__Rec__PixelClusterPositions  as RichPoints
#gPoints = RichGlobalPoints("RichGloPoints")
#lPoints = RichLocalPoints("RichLocPoints")
#aPoints = RichPoints("RichPoints")
#aPoints.RichPixelGlobalPositionsLocation = gPoints.RichPixelGlobalPositionsLocation+"TMP"
#aPoints.RichPixelLocalPositionsLocation  = lPoints.RichPixelLocalPositionsLocation+"TMP"
#all.Members += [ gPoints, lPoints ]
#all.Members += [ aPoints ]

# Make SIMD pixel summaries
from Configurables import Rich__Future__Rec__SIMDSummaryPixels as RichSIMDPixels
simdPixs = RichSIMDPixels("SIMDPixels")
all.Members += [simdPixs]

# --------------------------------------------------------------------------------------
# Example command lines
# --------------------------------------------------------------------------------------

# Normal running
# gaudirun.py -T ~/LHCbCMake/Feature/Rec/Rich/RichFutureRecSys/examples/{RichDecode.py,data/PMTs/RealTel40Format/MCLDstFiles-NoSpill.py} 2>&1 | tee ${User_release_area##/*/}-${CMTCONFIG}.log

# --------------------------------------------------------------------------------------
