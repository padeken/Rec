/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

// Gaudi
#include "GaudiAlg/Transformer.h"

// Base class
#include "RichFutureRecBase/RichRecAlgBase.h"

// Event Model
#include "RichFutureRecEvent/RichRecSpacePoints.h"

// Utils
#include "RichFutureUtils/RichGeomPhoton.h"
#include "RichFutureUtils/RichRayTracing.h"
#include "RichUtils/RichTrackSegment.h"

// DetDesc
#include "DetDesc/ConditionAccessorHolder.h"

namespace Rich::Future::Rec {

  // Use the functional framework
  using namespace Gaudi::Functional;

  /** @class RayTraceTrackGlobalPoints RichRayTraceTrackGlobalPoints.h
   *
   *  Ray traces the track direction through the RICH system to the
   *  HPD panels and stores the results in global coordinates.
   *
   *  @author Chris Jones
   *  @date   2016-09-30
   */

  class RayTraceTrackGlobalPoints final
      : public Transformer<SegmentPanelSpacePoints::Vector( const LHCb::RichTrackSegment::Vector&, //
                                                            const Utils::RayTracing& ),
                           LHCb::DetDesc::usesBaseAndConditions<AlgBase<>, Utils::RayTracing>> {

  public:
    /// Standard constructor
    RayTraceTrackGlobalPoints( const std::string& name, ISvcLocator* pSvcLocator )
        : Transformer( name, pSvcLocator,
                       // data inputs
                       {KeyValue{"TrackSegmentsLocation", LHCb::RichTrackSegmentLocation::Default},
                        // input conditions data
                        KeyValue{"RichRayTracing", Utils::RayTracing::DefaultConditionKey}},
                       // outputs
                       {KeyValue{"TrackGlobalPointsLocation", SpacePointLocation::SegmentsGlobal}} ) {
      // init
      m_traceMode.fill( LHCb::RichTraceMode( LHCb::RichTraceMode::DetectorPlaneBoundary::IgnorePDAcceptance ) );
      // debugging
      // setProperty( "OutputLevel", MSG::VERBOSE );
    }

    /// Initialization after creation
    StatusCode initialize() override {
      return Transformer::initialize().andThen( [&] {
        // track ray tracing
        for ( const auto rad : Rich::radiators() ) {
          _ri_verbo << "Trace mode " << rad << " : " << m_traceMode[rad] << endmsg;
          m_fTraceMode[rad] = m_traceMode[rad];
          m_fTraceMode[rad].setForcedSide( true );
          m_fTraceMode[rad].setDetPlaneBound( LHCb::RichTraceMode::DetectorPlaneBoundary::IgnorePDAcceptance );
        }
        // Derived condition objects.
        Utils::RayTracing::addConditionDerivation( this );
      } );
    }

  public:
    /// Functional operator
    SegmentPanelSpacePoints::Vector operator()( const LHCb::RichTrackSegment::Vector& segments, //
                                                const Utils::RayTracing&              rayTrace ) const override;

  private:
    /// Cached trace modes for each radiator
    RadiatorArray<LHCb::RichTraceMode> m_traceMode = {{}};

    /// Cached forced trace modes for each radiator
    RadiatorArray<LHCb::RichTraceMode> m_fTraceMode = {{}};
  };

} // namespace Rich::Future::Rec

// All code is in general Rich reconstruction namespace
using namespace Rich::Future::Rec;

//=============================================================================

SegmentPanelSpacePoints::Vector //
RayTraceTrackGlobalPoints::operator()( const LHCb::RichTrackSegment::Vector& segments,
                                       const Utils::RayTracing&              rayTrace ) const {

  // the global points to return
  SegmentPanelSpacePoints::Vector points;
  points.reserve( segments.size() );

  // local photon object to work with
  GeomPhoton photon;

  // Loop over segments
  for ( const auto& trSeg : segments ) {

    // rich and radiator
    const auto rich = trSeg.rich();
    const auto rad  = trSeg.radiator();

    // best start point and direction
    const auto& trackDir = trSeg.bestMomentum();
    const auto& trackPtn = trSeg.bestPoint();

    // ray trace the best point
    auto result = rayTrace.traceToDetector( rich, trackPtn, trackDir, photon, m_traceMode[rad], //
                                            Rich::top );                                        // side not used ...
    if ( m_traceMode[rad].traceWasOK( result ) ) {

      // save point and side for the best tracing
      const auto bestPoint = photon.detectionPoint();
      const auto bestSide  = photon.smartID().panel();

      // closest PD
      const auto closestPD = photon.smartID().pdID();

      // Now do the forced left/right up/down tracings

      // left/top
      result          = rayTrace.traceToDetector( rich, trackPtn, trackDir, photon, m_fTraceMode[rad], Rich::left );
      const auto lPos = ( m_fTraceMode[rad].traceWasOK( result ) ? photon.detectionPoint() : SpacePoint{0, 0, 0} );

      // right/bottom
      result          = rayTrace.traceToDetector( rich, trackPtn, trackDir, photon, m_fTraceMode[rad], Rich::right );
      const auto rPos = ( m_fTraceMode[rad].traceWasOK( result ) ? photon.detectionPoint() : SpacePoint{0, 0, 0} );

      // save the data
      points.emplace_back( bestPoint, lPos, rPos, bestSide, closestPD );
    } else {
      // Save a default object for this segment
      points.emplace_back();
    }

    _ri_verbo << "Segment PD panel point (global) " << points.back() << endmsg;

  } // segment loop

  return points;
}

//=============================================================================

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( RayTraceTrackGlobalPoints )

//=============================================================================
