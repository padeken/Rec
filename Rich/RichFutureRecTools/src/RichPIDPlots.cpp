/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

//-----------------------------------------------------------------------------
/** @file RichPIDPlots.cpp
 *
 *  Implementation file for RICH tool : Rich::Rec::PIDPlots
 *
 *  @author Chris Jones     Christopher.Rob.Jones@cern.ch
 *  @date   2008-04-14
 */
//-----------------------------------------------------------------------------

// local
#include "RichPIDPlots.h"

//-----------------------------------------------------------------------------

using namespace Rich::Future::Rec;

// Declaration of the Tool Factory
DECLARE_COMPONENT( PIDPlots )

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
PIDPlots::PIDPlots( const std::string& type, const std::string& name, const IInterface* parent )
    : HistoToolBase( type, name, parent ) {
  // interface
  declareInterface<Rich::Future::Rec::IPIDPlots>( this );
  // turn off histo printing by default
  setProperty( "HistoPrint", false ).ignore();
  // Default number of bins
  setProperty( "NBins1DHistos", 50 ).ignore();
  setProperty( "NBins2DHistos", 40 ).ignore();
}

//=============================================================================

StatusCode PIDPlots::prebookHistograms() {

  for ( const auto hypo : activeParticles() ) {
    // Momentum spectra
    richHisto1D( HID( "Ptot", hypo ), "Track Momentum", m_minP, m_maxP, nBins1D(), "Track Momentum (MeV/c)" );

    // Acc. Eff v P and Pt plots
    std::string title = "Eff. RICH acceptance V P";
    richProfile1D( HID( "AccEffVP", hypo ), title, m_minP, m_maxP, nBins1D(), "Track Momentum (MeV/c)",
                   "Selection Efficiency (%)" );
    title = "Eff. RICH acceptance Versus Pt";
    richProfile1D( HID( "AccEffVPt", hypo ), title, m_minPt, m_maxPt, nBins1D(), "Track Transverse Momentum (MeV/c)",
                   "Selection Efficiency (%)" );
    title = "Eff. RICH acceptance Versus P,Pt (MeV)";
    richProfile2D( HID( "AccEffVPVPt", hypo ), title, m_minP, m_maxP, nBins2D(), m_minPt, m_maxPt, nBins2D(),
                   "Track Momentum (MeV/c)", "Track Transverse Momentum (MeV/c)", "Selection Efficiency (%)" );
  }

  // Loop over all combinations of PID pairs
  for ( auto i = activeParticles().rbegin(); i != activeParticles().rend(); ++i ) {
    auto j = i;
    ++j;
    for ( ; j != activeParticles().rend(); ++j ) {
      // construct the DLL diff string
      const auto fDLL    = Rich::text( *i );
      const auto sDLL    = Rich::text( *j );
      const auto DllDiff = fDLL + "-" + sDLL;

      // inclusive plot
      auto title = "RichDLL(" + DllDiff + ")";
      auto id    = "All/" + fDLL + sDLL;
      richHisto1D( HID( id ), title, -m_dllRange, m_dllRange, nBins1D() );

      // loop over hypos
      for ( const auto hypo : activeParticles() ) {

        // Dll(X-Y) distributions
        // For given PID type
        title = "RichDLL(" + DllDiff + ")";
        id    = fDLL + sDLL;
        richHisto1D( HID( id, hypo ), title, -m_dllRange, m_dllRange, nBins1D() );
        // 2D plot V P
        title = "RichDLL(" + DllDiff + ")" + " Versus P (MeV)";
        id    = "DLLVP" + fDLL + sDLL;
        richHisto2D( HID( id, hypo ), title, m_minP, m_maxP, nBins2D(), -m_dllRange, m_dllRange, nBins2D() );
        // 1D profile V P
        id = "DLLProfVP" + fDLL + sDLL;
        richProfile1D( HID( id, hypo ), title, m_minP, m_maxP, nBins1D() );

        // Efficiency plots
        title = "Eff. RichDLL(" + DllDiff + ")>" + std::to_string( m_dllCut ) + " Versus P (MeV)";
        id    = "EffVP" + fDLL + sDLL;
        richProfile1D( HID( id, hypo ), title, m_minP, m_maxP, nBins1D(), "Track Momentum (MeV/c)",
                       "PID Efficiency (%)" );
        title = "Eff. RichDLL(" + DllDiff + ")>" + std::to_string( m_dllCut ) + " Versus Pt (MeV)";
        id    = "EffVPt" + fDLL + sDLL;
        richProfile1D( HID( id, hypo ), title, m_minPt, m_maxPt, nBins1D(), "Track Transverse Momentum (MeV/c)",
                       "PID Efficiency (%)" );
        title = "Eff. RichDLL(" + DllDiff + ")>" + std::to_string( m_dllCut ) + " Versus P,Pt (MeV)";
        id    = "EffVPVPt" + fDLL + sDLL;
        richProfile2D( HID( id, hypo ), title, m_minP, m_maxP, nBins2D(), m_minPt, m_maxPt, nBins2D(),
                       "Track Momentum (MeV/c)", "Track Transverse Momentum (MeV/c)", "PID Efficiency (%)" );
      }
    }

  } // hypos

  return StatusCode::SUCCESS;
}

//=============================================================================

void PIDPlots::plots( const LHCb::ProtoParticle* proto, const Rich::ParticleIDType hypo ) const {

  if ( UNLIKELY( !proto ) ) {
    ++m_nullProto;
    return;
  }

  // Get the Track pointer
  const auto track = proto->track();
  if ( UNLIKELY( !track ) ) {
    ++m_nullTrack;
    return;
  }

  // track selection
  if ( !selected( track ) ) return;

  // Fill RichPID plots
  plots( proto->richPID(), hypo );

  // ProtoParticle level plots

  // track momentum
  const auto pTot = trackP( track );
  const auto pT   = trackPt( track );

  // Efficiency to have a RichPID data object associated (~ RICH Acceptance)
  const double accEff( proto->richPID() ? 100.0 : 0.0 );

  // Eff v P and Pt plots
  richProfile1D( HID( "AccEffVP", hypo ) )->fill( pTot, accEff );
  richProfile1D( HID( "AccEffVPt", hypo ) )->fill( pT, accEff );
  richProfile2D( HID( "AccEffVPVPt", hypo ) )->fill( pTot, pT, accEff );
}

void PIDPlots::plots( const LHCb::RichPID* pid, const Rich::ParticleIDType hypo ) const {

  // Only proceed further with valid RichPID data objects
  if ( !pid ) { return; }

  // track selection
  if ( !selected( pid->track() ) ) return;

  // Fill 'track' plots
  plots( pid->track(), hypo );

  // track momentum
  const auto pTot = trackP( pid->track() );
  const auto pT   = trackPt( pid->track() );

  // Loop over all combinations of PID pairs
  for ( auto i = activeParticles().rbegin(); i != activeParticles().rend(); ++i ) {
    auto j = i;
    ++j;
    for ( ; j != activeParticles().rend(); ++j ) {
      // types
      const auto first = *i;
      const auto last  = *j;
      // DLL diff ID string
      const auto DllDiff = Rich::text( first ) + Rich::text( last );

      // Dll(X-Y) distributions
      const auto dll = pid->particleDeltaLL( first ) - pid->particleDeltaLL( last );
      richHisto1D( HID( DllDiff, hypo ) )->fill( dll );
      richHisto1D( HID( "All/" + DllDiff ) )->fill( dll );
      richHisto2D( HID( "DLLVP" + DllDiff, hypo ) )->fill( pTot, dll );
      richProfile1D( HID( "DLLProfVP" + DllDiff, hypo ) )->fill( pTot, dll );

      // Efficiency plots
      const double eff( dll > m_dllCut ? 100.0 : 0.0 );
      richProfile1D( HID( "EffVP" + DllDiff, hypo ) )->fill( pTot, eff );
      richProfile1D( HID( "EffVPt" + DllDiff, hypo ) )->fill( pT, eff );
      richProfile2D( HID( "EffVPVPt" + DllDiff, hypo ) )->fill( pTot, pT, eff );
    }
  }
}

void PIDPlots::plots( const LHCb::Track* track, const Rich::ParticleIDType hypo ) const {

  if ( UNLIKELY( !track ) ) {
    ++m_nullTrack;
    return;
  }

  // track selection
  if ( !selected( track ) ) return;

  // Track momentum in GeV/c
  const auto tkPtot = trackP( track );

  // Momentum spectra
  richHisto1D( HID( "Ptot", hypo ) )->fill( tkPtot );
}
