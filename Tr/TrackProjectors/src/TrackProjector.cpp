/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "TrackProjector.h"
#include "Event/FitNode.h"
#include "Event/Measurement.h"
#include "Event/State.h"
#include "Event/StateVector.h"
#include "GaudiKernel/IMagneticFieldSvc.h"
#include "Kernel/ITrajPoca.h"
#include "TrackKernel/StateZTraj.h"

using namespace ROOT::Math;
using ROOT::Math::SMatrix;

DECLARE_COMPONENT( TrackProjector )

//-----------------------------------------------------------------------------
/// Initialize
//-----------------------------------------------------------------------------
StatusCode TrackProjector::initialize() {
  return GaudiTool::initialize().andThen( [&] {
    m_pIMF = svc<IMagneticFieldSvc>( "MagneticFieldSvc", true );
    m_poca = tool<ITrajPoca>( "TrajPoca" );
  } );
}

// trivial helpers to make code clearer...
namespace {
  typedef Gaudi::Matrix1x3 DualVector;

  DualVector dual( const Gaudi::XYZVector& v ) {
    DualVector d;
    v.GetCoordinates( d.Array() );
    return d;
  }

  // Determine the actual minimum with the Poca tool
  auto minimize_traj( const ITrajPoca* poca, double tolerance ) {
    return [=]( const LHCb::StateZTraj<double>& refTraj, const LHCb::Trajectory<double>& measTraj, double zState ) {
      double sMeas = measTraj.muEstimate( refTraj.position( zState ) ); // Assume state is already close to the minimum
      // Determine the actual minimum with the Poca tool
      Gaudi::XYZVector dist;
      StatusCode       sc = poca->minimize( refTraj, zState, measTraj, sMeas, dist, tolerance );
      if ( sc.isFailure() ) { throw sc; }
      // Set up the vector onto which we project everything. This should
      // actually be parallel to dist.
      auto unitPocaVector = measTraj.direction( sMeas ).Cross( refTraj.direction( zState ) ).Unit();
      return std::tuple{zState, sMeas, unitPocaVector.Dot( dist ), std::move( unitPocaVector )};
    };
  }

  auto minimize_line = []( const LHCb::StateZTraj<double>& refTraj, const LHCb::LineTraj<double>& measTraj,
                           double zState ) {
    // assume minimum is close to zState
    auto       dist   = refTraj.position( zState ) - measTraj.position( 0 );
    const auto d0     = measTraj.direction( 0 );
    const auto d1     = refTraj.direction( zState );
    const auto c1     = refTraj.curvature( zState );
    const auto mat    = std::array{d0.mag2(), -d1.Dot( d0 ), d1.mag2() - dist.Dot( c1 )};
    auto       mu     = std::array{-dist.Dot( d0 ), dist.Dot( d1 )};
    const auto decomp = ROOT::Math::CholeskyDecomp<double, 2>{mat.data()};
    if ( !decomp.Solve( mu ) ) throw std::runtime_error( "singular matrix" );
    zState += mu[1];
    dist += mu[0] * d0 - mu[1] * ( d1 + 0.5 * mu[1] * c1 );
    // Set up the vector onto which we project everything. This should
    // actually be parallel to dist.
    auto unitPoca = d0.Cross( d1 + mu[1] * c1 ).Unit();
    return std::tuple{zState, mu[0], unitPoca.Dot( dist ), std::move( unitPoca )};
  };
} // namespace

//-----------------------------------------------------------------------------
// internal project method, doing the actual work
//-----------------------------------------------------------------------------
TrackProjector::InternalProjectResult TrackProjector::internal_project( const LHCb::StateVector& statevector,
                                                                        const LHCb::Measurement& meas ) const {
  // Project onto the reference. First create the StateTraj with or without BField information.
  Gaudi::XYZVector bfield( 0, 0, 0 );
  if ( m_useBField ) m_pIMF->fieldVector( statevector.position(), bfield ).ignore();
  const LHCb::StateZTraj<double> refTraj( statevector, bfield );
  auto minimize = LHCb::details::Measurement::overloaded{minimize_traj( m_poca, m_tolerance ), minimize_line};
  auto [zState, sMeas, doca, unitPocaVector] = meas.visit(
      [&minimize, &refTraj, z = statevector.z()]( const auto& m ) { return minimize( refTraj, m.trajectory, z ); } );
  // compute the projection matrix from parameter space onto the (signed!) unit
  Gaudi::TrackProjectionMatrix H = dual( unitPocaVector ) * refTraj.derivative( zState );

  // Set the error on the measurement so that it can be used in the fit
  double errMeasure2 = meas.resolution2( refTraj.position( zState ), refTraj.direction( zState ) );
  double errMeasure  = sqrt( errMeasure2 );

  return {sMeas, doca, -doca, errMeasure, std::move( H ), std::move( unitPocaVector )};
}
//-----------------------------------------------------------------------------
/// Project a state onto a measurement
//-----------------------------------------------------------------------------
std::tuple<StatusCode, double, double> TrackProjector::project( const LHCb::State&       state,
                                                                const LHCb::Measurement& meas ) const {
  try {
    // Project onto the reference (prevent the virtual function call)
    auto result = internal_project( LHCb::StateVector( state.stateVector(), state.z() ), meas );
    return {StatusCode{StatusCode::SUCCESS}, result.residual, result.errMeasure};
    // Calculate the error on the residual
    // m_errResidual = sqrt( m_errMeasure*m_errMeasure + Similarity( m_H, state.covariance() )(0,0) );
  } catch ( StatusCode sc ) { return {sc, 0, 0}; }
}

//-----------------------------------------------------------------------------
/// Project the state vector in this fitnode and update projection matrix and reference residual
//-----------------------------------------------------------------------------
StatusCode TrackProjector::projectReference( LHCb::FitNode& node ) const {
  StatusCode sc = StatusCode::FAILURE;
  if ( node.hasMeasurement() ) {
    try {
      auto result = internal_project( node.refVector(), node.measurement() );
      node.updateProjection( std::move( result.H ), result.residual, result.errMeasure );
      node.setPocaVector( std::move( result.unitPocaVector ) );
      node.setDoca( result.doca );
      sc = StatusCode::SUCCESS;
    } catch ( StatusCode scr ) { sc = scr; }
  }
  return sc;
}

StatusCode TrackProjector::project( const LHCb::StateVector& statevector, LHCb::Measurement& meas,
                                    Gaudi::TrackProjectionMatrix& H, double& residual, double& errMeasure,
                                    Gaudi::XYZVector& unitPocaVector, double& doca, double& sMeas ) const {
  StatusCode sc = StatusCode::FAILURE;
  try {
    // Project onto the reference (prevent the virtual function call)
    auto result    = internal_project( statevector, meas );
    H              = std::move( result.H );
    residual       = result.residual;
    errMeasure     = result.errMeasure;
    unitPocaVector = std::move( result.unitPocaVector );
    doca           = result.doca;
    sMeas          = result.sMeas;
    sc             = StatusCode::SUCCESS;
  } catch ( StatusCode scr ) { sc = scr; }
  return sc;
}

//-----------------------------------------------------------------------------
/// Derivatives wrt.the measurement's alignment...
//-----------------------------------------------------------------------------
TrackProjector::Derivatives TrackProjector::alignmentDerivatives( const LHCb::StateVector& statevector,
                                                                  const LHCb::Measurement& meas,
                                                                  const Gaudi::XYZPoint&   pivot ) const {
  auto       result = internal_project( statevector, meas );
  DualVector unit   = dual( result.unitPocaVector );

  // Calculate the derivative of the poca on measTraj to alignment parameters.
  // Only non-zero elements:
  Gaudi::XYZVector                  arm = meas.trajectory().position( result.sMeas ) - pivot;
  ROOT::Math::SMatrix<double, 3, 6> dPosdAlpha;
  // Derivative to translation
  dPosdAlpha( 0, 0 ) = dPosdAlpha( 1, 1 ) = dPosdAlpha( 2, 2 ) = 1;
  // Derivative to rotation around x-axis
  dPosdAlpha( 1, 3 ) = -arm.z();
  dPosdAlpha( 2, 3 ) = arm.y();
  // Derivative to rotation around y-axis
  dPosdAlpha( 0, 4 ) = arm.z();
  dPosdAlpha( 2, 4 ) = -arm.x();
  // Derivative to rotation around z-axis
  dPosdAlpha( 0, 5 ) = -arm.y();
  dPosdAlpha( 1, 5 ) = arm.x();

  return unit * dPosdAlpha;
  // compute the projection matrix from parameter space onto the (signed!) unit
  // return unit*AlignTraj( meas.trajectory(), pivot ).derivative( m_sMeas );
}
