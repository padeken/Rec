/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files

// local
#include "TrackExtrapolator.h"
#include "Event/TrackParameters.h"
#include "TrackKernel/TrackFunctors.h"

#include <math.h>

#include "LHCbMath/GeomFun.h"
#include "LHCbMath/Line.h"
#include "LHCbMath/Similarity.h"

using namespace LHCb;
using namespace Gaudi;

//=============================================================================
// Propagate a state vector from zOld to zNew
//=============================================================================
StatusCode TrackExtrapolator::propagate( Gaudi::TrackVector& stateVec, double zOld, double zNew,
                                         const LHCb::Tr::PID pid ) const {
  return propagate( stateVec, zOld, zNew, nullptr, pid );
}

//=============================================================================
// Propagate a state vector from zOld to zNew
//=============================================================================
StatusCode TrackExtrapolator::propagate( LHCb::StateVector& state, double z, Gaudi::TrackMatrix* transportmatrix,
                                         const LHCb::Tr::PID pid ) const {
  return propagate( state.parameters(), state.z(), z, transportmatrix, pid ).andThen( [&] { state.setZ( z ); } );
}

//=============================================================================
// Propagate a track to a given z-position
//=============================================================================
StatusCode TrackExtrapolator::propagate( const Track& track, double z, State& state, const LHCb::Tr::PID pid ) const {
  // get state closest to z
  state = closestState( track, z );

  // propagate the closest state
  return propagate( state, z, pid );
}

//=============================================================================
// Propagate a track to a given z-position
//=============================================================================
StatusCode TrackExtrapolator::propagate( const Track& track, double z, StateVector& state,
                                         const LHCb::Tr::PID pid ) const {
  // get state closest to z
  const State& closest = closestState( track, z );
  state                = LHCb::StateVector( closest.stateVector(), closest.z() );

  // propagate the closest state
  return propagate( state, z, 0, pid );
}

//=============================================================================
// Propagate a state to a given z-position
//=============================================================================
StatusCode TrackExtrapolator::propagate( State& state, double z, const LHCb::Tr::PID pid ) const {
  Gaudi::TrackMatrix transMat = ROOT::Math::SMatrixIdentity();
  return propagate( state, z, &transMat, pid );
}

//=============================================================================
// Propagate a state to a given z-position
// Transport matrix is calulated when transMat pointer is not NULL
//=============================================================================
StatusCode TrackExtrapolator::propagate( State& state, double z, Gaudi::TrackMatrix* tm,
                                         const LHCb::Tr::PID pid ) const {
  Gaudi::TrackMatrix transMat = ROOT::Math::SMatrixIdentity();
  return propagate( state.stateVector(), state.z(), z, &transMat, pid ).andThen( [&] {
    state.setZ( z );
    state.setCovariance( LHCb::Math::Similarity( transMat, state.covariance() ) );
    if ( tm ) *tm = transMat;
  } );
}

//=============================================================================
// Propagate a track to the closest point to the specified point
//=============================================================================
StatusCode TrackExtrapolator::propagate( const Track& track, const Gaudi::XYZPoint& point, LHCb::State& state,
                                         const LHCb::Tr::PID pid ) const {
  // get state closest to z of point
  state = closestState( track, point.z() );

  // propagate the closest state
  return propagate( state, point.z(), pid );
}

//=============================================================================
// Propagate a state to the closest point to the specified point
//=============================================================================
StatusCode TrackExtrapolator::propagate( State& state, const Gaudi::XYZPoint& point, const LHCb::Tr::PID ) const {
  ++m_impossible;

  if ( UNLIKELY( msgLevel( MSG::DEBUG ) ) )
    debug() << " can not propagate state at " << state.z() << " to point at z " << point.z() << endmsg;

  return StatusCode::FAILURE;
}

//=============================================================================
// Propagate a track to within tolerance of a plane (default = 10 microns)
//=============================================================================
StatusCode TrackExtrapolator::propagate( const Track& track, const Gaudi::Plane3D& plane, LHCb::State& state,
                                         double tolerance, const LHCb::Tr::PID pid ) const {
  // get state closest to the plane
  state = closestState( track, plane );

  // propagate the closest state
  return propagate( state, plane, tolerance, pid );
}

//=============================================================================
// Propagate a state to within tolerance of a plane (default = 10 microns)
//=============================================================================
StatusCode TrackExtrapolator::propagate( State& state, const Gaudi::Plane3D& plane, double tolerance,
                                         const LHCb::Tr::PID pid ) const {
  StatusCode      sc = StatusCode::FAILURE;
  Gaudi::XYZPoint intersect;
  int             iter;
  double          distance;
  for ( iter = 0; iter < m_maxIter; ++iter ) {
    Gaudi::Math::Line<Gaudi::XYZPoint, Gaudi::XYZVector> line( state.position(), state.slopes() );
    double                                               dz;
    bool success = Gaudi::Math::intersection( line, plane, intersect, dz );
    if ( !success ) {
      ++m_parallel;
      break;
    }
    distance = ( intersect - line.beginPoint() ).R();

    if ( distance < tolerance ) {
      sc = StatusCode::SUCCESS;
      break;
    } else {
      double ztarget = state.z() + dz;
      sc             = propagate( state, ztarget, pid );
      if ( sc.isFailure() ) {
        ++m_propfail;
        if ( UNLIKELY( msgLevel( MSG::DEBUG ) ) ) debug() << "Failed to propagate to z = " << ztarget << endmsg;
        break;
      }
    }
  }

  if ( iter == m_maxIter ) ++m_tolerance;

  return sc;
}

//--------------------------------------------------------------------------
//  ACCESS METHODS
//--------------------------------------------------------------------------

//=============================================================================
// Retrieve the position and momentum vectors and the corresponding
// 6D covariance matrix (pos:1->3,mom:4-6) of a track at a given z-position
//=============================================================================
StatusCode TrackExtrapolator::positionAndMomentum( const Track& track, double z, Gaudi::XYZPoint& pos,
                                                   Gaudi::XYZVector& mom, Gaudi::SymMatrix6x6& cov6D,
                                                   const LHCb::Tr::PID pid ) const {
  State tmpState;
  return propagate( track, z, tmpState, pid ).andThen( [&] { tmpState.positionAndMomentum( pos, mom, cov6D ); } );
}

//=============================================================================
// Retrieve the position and momentum vectors of a track at a given z-position
//=============================================================================
StatusCode TrackExtrapolator::positionAndMomentum( const Track& track, double z, Gaudi::XYZPoint& pos,
                                                   Gaudi::XYZVector& mom, const LHCb::Tr::PID pid ) const {
  State tmpState;
  return propagate( track, z, tmpState, pid ).andThen( [&] {
    pos = tmpState.position();
    mom = tmpState.momentum();
  } );
}

//=============================================================================
// Retrieve the 3D-position vector and error matrix of a track
// at a given z-position
//=============================================================================
StatusCode TrackExtrapolator::position( const Track& track, double z, Gaudi::XYZPoint& pos, Gaudi::SymMatrix3x3& errPos,
                                        const LHCb::Tr::PID pid ) const

{
  State tmpState;
  return propagate( track, z, tmpState, pid ).andThen( [&] {
    pos    = tmpState.position();
    errPos = tmpState.errPosition();
  } );
}

//=============================================================================
// Retrieve the 3D-position vector of a track at a given z-position
//=============================================================================
StatusCode TrackExtrapolator::position( const Track& track, double z, Gaudi::XYZPoint& pos,
                                        const LHCb::Tr::PID pid ) const

{
  State tmpState;
  return propagate( track, z, tmpState, pid ).andThen( [&] { pos = tmpState.position(); } );
}

//=============================================================================
// Retrieve the slopes (dx/dz,dy/dz,1) and error matrix of a
// track at a given z-position
//=============================================================================
StatusCode TrackExtrapolator::slopes( const Track& track, double z, Gaudi::XYZVector& slopes,
                                      Gaudi::SymMatrix3x3& errSlopes, const LHCb::Tr::PID pid ) const {
  State tmpState;
  return propagate( track, z, tmpState, pid ).andThen( [&] {
    slopes    = tmpState.slopes();
    errSlopes = tmpState.errSlopes();
  } );
}

//=============================================================================
// Retrieve the slopes (dx/dz,dy/dz,1) of a track at a given z-position
//=============================================================================
StatusCode TrackExtrapolator::slopes( const Track& track, double z, Gaudi::XYZVector& slopes,
                                      const LHCb::Tr::PID pid ) const {
  State tmpState;
  return propagate( track, z, tmpState, pid ).andThen( [&] { slopes = tmpState.slopes(); } );
}

//=============================================================================
// Retrieve the momentum of a track at a given z-position
//=============================================================================
StatusCode TrackExtrapolator::p( const Track& track, double z, double& p, const LHCb::Tr::PID pid ) const {
  State tmpState;
  return propagate( track, z, tmpState, pid ).andThen( [&] { p = tmpState.p(); } );
}

//=============================================================================
// Retrieve the transverse momentum of a track at a given z-position
//=============================================================================
StatusCode TrackExtrapolator::pt( const Track& track, double z, double& pt, const LHCb::Tr::PID pid ) const {
  State tmpState;
  return propagate( track, z, tmpState, pid ).andThen( [&] { pt = tmpState.pt(); } );
}

//=============================================================================
// Retrieve the momentum vector and error matrix of a
// track at a given z-position
//=============================================================================
StatusCode TrackExtrapolator::momentum( const Track& track, double z, Gaudi::XYZVector& mom,
                                        Gaudi::SymMatrix3x3& errMom, const LHCb::Tr::PID pid ) const {
  State tmpState;
  return propagate( track, z, tmpState, pid ).andThen( [&] {
    mom    = tmpState.momentum();
    errMom = tmpState.errMomentum();
  } );
}

//=============================================================================
// Retrieve the momentum vector of a track at a given z-position
//=============================================================================
StatusCode TrackExtrapolator::momentum( const Track& track, double z, Gaudi::XYZVector& mom,
                                        const LHCb::Tr::PID pid ) const {
  State tmpState;
  return propagate( track, z, tmpState, pid ).andThen( [&] { mom = tmpState.momentum(); } );
}

//=============================================================================
