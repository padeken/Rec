/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "AIDA/IHistogram1D.h"
#include "AIDA/IHistogram2D.h"
#include "AIDA/IProfile1D.h"
#include "AIDA/IProfile2D.h"
#include "Event/STCluster.h"
#include "GaudiKernel/PhysicalConstants.h"
#include "Kernel/ISTChannelIDSelector.h"
#include "Kernel/ITDetectorPlot.h"
#include "Kernel/STLexicalCaster.h"
#include "Kernel/TTDetectorPlot.h"
#include "STDet/DeSTDetector.h"
#include "STDet/DeSTSector.h"
#include "STDet/DeSTSensor.h"
#include "STDet/DeTTSector.h"
#include "TrackInterfaces/IHitExpectation.h"
#include "TrackInterfaces/ISTClusterCollector.h"
#include "TrackMonitorBase.h"
#include <cmath>
#include <iostream>
#include <map>
#include <sstream>
#include <vector>
using namespace std;
using namespace LHCb;
using namespace ST;
using namespace AIDA;
using namespace Gaudi;

//-----------------------------------------------------------------------------
// Implementation file for class : STEfficiency
//
// 2009-06-16 : Johan Luisier
// 2010-07-27 : Frederic Dupertuis
//-----------------------------------------------------------------------------

/** @class STEfficiency STEfficiency.h
 *
 *
 *  @author Johan Luisier, Frederic Dupertuis
 *  @date   2009-06-16, 2010-07-27
 */
class STEfficiency : public TrackMonitorBase {
public:
  /// Standard constructor
  using TrackMonitorBase::TrackMonitorBase;

  StatusCode initialize() override; ///< Algorithm initialization
  StatusCode execute() override;    ///< Algorithm execution
  StatusCode finalize() override;   ///< Algorithm finalization

private:
  unsigned int TThistoBin( const LHCb::STChannelID& chan ) const;
  unsigned int IThistoBin( const LHCb::STChannelID& chan ) const;

  bool foundHitInSector( const ISTClusterCollector::Hits& hits, LHCb::Track const& track, const unsigned int testsector,
                         const double resCut, const bool toplot ) const;

  std::string formatNumber( const double& nbr, const unsigned int& digits = 2u ) const;

  bool hasMinStationPassed( LHCb::Track const& ) const;

  Gaudi::Property<std::string> m_clustercollectorName{this, "ClusterCollector", "ITClusterCollector"};

  ISTClusterCollector* m_clustercollector;

  /**
   * Name of the expected hits tool
   */
  Gaudi::Property<std::string> m_expectedHitsToolName{this, "HitExpectation", "ITHitExpectation"};
  /**
   * Collects all the expeted hits around a track
   */
  IHitExpectation* m_expectedHits;

  /**
   * Cuts applied to get the scan efficiency vs window
   */
  Gaudi::Property<std::vector<double>> m_spacialCuts{this, "Cuts", {300. * Gaudi::Units::um}};
  Gaudi::Property<double>              m_xCut{this, "XLayerCut",
                                 300. * Gaudi::Units::um}; /**< Applied cut to compute X layer efficiencies */
  Gaudi::Property<double>              m_stereoCut{this, "StereoLayerCut",
                                      300. * Gaudi::Units::um}; /**< Applied cut to compute stereo layer efficiencies */

  /**
   * Total number of expected hits
   */
  unsigned int m_totalExpected{0u};
  /**
   * Total number of found hits
   */
  unsigned int m_totalFound{0u};
  /**
   * Indicates what is the applied cut, depending on the layer.
   */
  unsigned int m_whichCut[2];
  /**
   * Size of the m_spacialCut container
   */
  unsigned int m_NbrOfCuts;
  /**
   * Min number of expected hits for the track to be accepted.
   */
  unsigned int m_minHits;

  /**
   * Detector type, can be either IT or TT.
   */
  Gaudi::Property<std::string> m_detType{this, "DetType", "IT"};
  /**
   * Detector element, automatically retrieved.
   */
  DeSTDetector* m_tracker;

  typedef std::map<unsigned int, unsigned> CounterMap;
  std::vector<CounterMap>                  m_foundSector, m_foundLayer;
  CounterMap                               m_expectedSector, m_expectedLayer;
  std::map<unsigned int, DeSTSector*>      m_nameMapSector, m_nameMapLayer;

  void filterNameList( std::vector<unsigned int>& vec );

  /**
   * Cut applied on the cluster charge, in terms of signal to noise.
   */
  Gaudi::Property<double> m_minCharge{this, "MinCharge", 0.};

  /**
   * Prefix added to the collector names used to get the hits. Default
   * value is "". SHould be used to study spill over.
   */
  std::string m_collectorPrefix;

  /**
   * Size of a bin, derived from the spacial cuts given in option
   */
  double m_binSize{10000.};

  /**
   * Number of bins, derived from the spacial cuts given in option
   */
  unsigned int m_binNumber;

  /**
   * Minimum number of expected hits in order to compute an efficiency
   * Default value is 100.
   */
  Gaudi::Property<unsigned int> m_minExpSectors{this, "MinExpectedSectors", 1};

  /**
   *
   */
  Gaudi::Property<int> m_minNbResSectors{this, "MinNbResSectors", -10};

  /**
   *
   */
  Gaudi::Property<int> m_maxNbResSectors{this, "MaxNbResSectors", 10};

  /**
   *
   */
  Gaudi::Property<bool> m_singlehitpersector{this, "SingleHitPerSector", false};

  /**
   * List of wanted track types that will be used to compute efficiency.
   * Default is only Long (type 3)
   */
  std::vector<unsigned int> m_wantedTypes;

  /**
   * Toggles off / on picking only hits from the track.\n
   * \e true => every found hit is taken\n
   * \e false => only hits picked up by the pattern recognition are
   * taken into account.
   */
  Gaudi::Property<bool> m_everyHit{this, "TakeEveryHit", true};

  /**
   * Cut on the active region X
   */
  Gaudi::Property<double> m_minDistToEdgeX{this, "MinDistToEdgeX", -1.};

  /**
   * Cut on the active region Y
   */
  Gaudi::Property<double> m_minDistToEdgeY{this, "MinDistToEdgeY", -1.};

  /**
   * Cut on minimum station that the track must passed through
   */
  Gaudi::Property<unsigned int> m_minStationPassed{this, "MinStationPassed", 1};

  /**
   * Plot efficiency plot
   */
  Gaudi::Property<bool> m_effPlot{this, "EfficiencyPlot", true};

  /**
   * Plot residuals plot
   */
  Gaudi::Property<bool> m_resPlot{this, "ResidualsPlot", false};

  Gaudi::Property<bool> m_SummaryHist{this, "TH1DSummaryHist", false};
};

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( STEfficiency )

//=============================================================================
// Initialization
//=============================================================================
StatusCode STEfficiency::initialize() {
  StatusCode sc( TrackMonitorBase::initialize() ); // must be executed first
  if ( sc.isFailure() ) return sc;                 // error printed already by TrackMonitorBase
  if ( msgLevel( MSG::DEBUG ) ) debug() << "==> Initialize" << endmsg;

  m_clustercollectorName = m_detType + "ClusterCollector";
  m_expectedHitsToolName = m_detType + "HitExpectation";

  m_clustercollector = tool<ISTClusterCollector>( "STClusterCollector", m_clustercollectorName );
  m_expectedHits     = tool<IHitExpectation>( m_detType + "HitExpectation", m_expectedHitsToolName );

  m_tracker   = getDet<DeSTDetector>( DeSTDetLocation::location( m_detType ) );
  m_NbrOfCuts = m_spacialCuts.size();
  m_foundSector.resize( m_NbrOfCuts );

  for ( const auto& iterS : m_tracker->sectors() ) {
    STChannelID elemID                      = iterS->elementID();
    m_expectedSector[elemID.uniqueSector()] = 0u;
    m_nameMapSector[elemID.uniqueSector()]  = iterS;

    for ( unsigned int i = 0; i < m_NbrOfCuts; ++i ) { m_foundSector[i][elemID.uniqueSector()] = 0u; }

  } // iterS

  unsigned int iCut;

  for ( iCut = 0; iCut < m_NbrOfCuts; ++iCut ) {
    if ( std::abs( m_xCut - m_spacialCuts[iCut] ) < .005 ) m_whichCut[0] = iCut;
    if ( std::abs( m_stereoCut - m_spacialCuts[iCut] ) < .005 ) m_whichCut[1] = iCut;
  } // loop cuts

  for ( iCut = 1; iCut < m_NbrOfCuts; iCut++ ) {
    m_binSize = std::min( m_binSize, m_spacialCuts[iCut] - m_spacialCuts[iCut - 1] );
  }

  // m_binSize = std::min( m_binSize, .01 );
  // m_binSize = std::max( m_binSize, .01 );

  m_binNumber =
      static_cast<unsigned int>( ( m_spacialCuts.value().back() - m_spacialCuts.value().front() ) / m_binSize );
  m_binNumber++;

  return StatusCode::SUCCESS;
}

//=============================================================================
// Main execution
//=============================================================================
StatusCode STEfficiency::execute() {
  if ( msgLevel( MSG::DEBUG ) ) debug() << "==> Execute" << endmsg;

  auto tracks = inputContainer();

  std::vector<LHCbID>       expectedHits;
  std::vector<unsigned int> expectedSectors;
  std::vector<unsigned int> foundSectors;
  ISTClusterCollector::Hits prefoundHits;
  ISTClusterCollector::Hits foundHits;
  StatusCode                inSensor;
  bool                      inActiveSensor;
  unsigned int              nbFoundHits, nbExpHits, nbExpSectors, nbFoundSectors;
  int                       nbResHits, nbResSectors;
  State                     trackState;
  Gaudi::XYZPoint toleranceOnEdge( -std::max( 0., m_minDistToEdgeX.value() ), -std::max( 0., m_minDistToEdgeY.value() ),
                                   0. );

  DeSTSector*              sector = nullptr;
  std::vector<DeSTSensor*> sensors;

  // Loop on tracks
  for ( const auto& trk : tracks ) {
    // Clearing all the std::vectors !
    expectedHits.clear();
    prefoundHits.clear();
    foundHits.clear();
    expectedSectors.clear();
    foundSectors.clear();

    // ask for tracks crossing enough stations
    if ( !hasMinStationPassed( *trk ) ) continue;

    // collect the list of expected hits
    m_expectedHits->collect( *trk, expectedHits );

    nbExpHits = expectedHits.size();

    // remove tracks with too few expected hits
    if ( nbExpHits < m_minExpSectors ) continue;

    // collect the list of found hits
    m_clustercollector->execute( *trk, prefoundHits ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );

    nbFoundHits = prefoundHits.size();

    nbResHits = (int)( nbFoundHits ) - (int)( nbExpHits );

    if ( fullDetail() ) {
      plot( nbFoundHits, "Control/NbFoundHits", "Number of found hits", -0.5, 24.5, 25 );
      plot( nbExpHits, "Control/NbExpHits", "Number of expected hits", -0.5, 24.5, 25 );
      plot( nbResHits, "Control/NbResHits", "Number of found - expected hits", -5.5, 7.5, 13 );
    }

    // convert the expected hits into unique sectors cutting sensor edges
    for ( const auto& id : expectedHits ) {
      if ( m_minDistToEdgeX > 0. || m_minDistToEdgeY > 0. ) {

        sector = m_tracker->findSector( id.stID() );
        if ( sector == 0 ) {
          info() << "Should not happen !" << endmsg;
          continue;
        }

        sensors        = sector->sensors();
        inActiveSensor = false;

        for ( const auto& sensor : sensors ) {
          inSensor = extrapolator()->propagate( *trk, sensor->plane(), trackState, sensor->thickness() );
          if ( inSensor.isSuccess() ) {
            inActiveSensor = sensor->globalInActive( trackState.position(), toleranceOnEdge );
            if ( inActiveSensor ) break;
          }
        }

        if ( inActiveSensor ) { expectedSectors.push_back( id.stID().uniqueSector() ); }
      }
    }

    nbExpSectors = expectedSectors.size();

    if ( nbExpSectors < m_minExpSectors ) continue;

    for ( const auto& hit : prefoundHits ) {
      if ( find( expectedSectors.begin(), expectedSectors.end(), hit.cluster->channelID().uniqueSector() ) !=
           expectedSectors.end() ) {
        if ( find( foundSectors.begin(), foundSectors.end(), hit.cluster->channelID().uniqueSector() ) ==
             foundSectors.end() ) {
          foundHits.push_back( hit );
          foundSectors.push_back( hit.cluster->channelID().uniqueSector() );
        } else if ( !m_singlehitpersector.value() )
          foundHits.push_back( hit );
      }
    }

    nbExpHits   = nbExpSectors;
    nbFoundHits = foundHits.size();
    nbResHits   = (int)( nbFoundHits ) - (int)( nbExpHits );

    nbFoundSectors = foundSectors.size();
    nbResSectors   = (int)( nbFoundSectors ) - (int)( nbExpSectors );

    // remove track with too much found - expected hits
    if ( nbResHits > m_maxNbResSectors ) continue;

    // remove track with too few found - expected sectors
    if ( nbResSectors < m_minNbResSectors ) continue;

    if ( fullDetail() ) {
      plot( nbFoundHits, "Control/NbFoundHitsFinal", "Number of found hits after full selection", -0.5, 24.5, 25 );
      plot( nbExpHits, "Control/NbExpHitsFinal", "Number of expected hits after full selection", -0.5, 24.5, 25 );
      plot( nbResHits, "Control/NbResHitsFinal", "Number of found - expected hits after full selection", -5.5, 7.5,
            13 );
      plot( nbFoundSectors, "Control/NbFoundSectorsFinal", "Number of found sectors after full selection", -0.5, 24.5,
            25 );
      plot( nbExpSectors, "Control/NbExpSectorsFinal", "Number of expected sectors after full selection", -0.5, 24.5,
            25 );
      plot( nbResSectors, "Control/NbResSectorsFinal", "Number of found - expected sectors after full selection", -5.5,
            7.5, 13 );
    }

    bool foundHit = false;
    for ( const auto& exp : expectedSectors ) {
      ++m_expectedSector[exp];
      ++m_totalExpected;
      foundHit = false;

      for ( unsigned int i = 0; i < m_NbrOfCuts; ++i ) {
        foundHit = foundHitInSector( foundHits, *trk, exp, m_spacialCuts[i],
                                     ( sector->isStereo() && i == m_whichCut[1] ) ||
                                         ( !sector->isStereo() && i == m_whichCut[0] ) );
        if ( foundHit ) {
          if ( sector->isStereo() && i == m_whichCut[1] )
            ++m_totalFound;
          else if ( !sector->isStereo() && i == m_whichCut[0] )
            ++m_totalFound;
          ++m_foundSector[i][exp];
        }
      } // loop cuts
    }   // loop expected
  }     // loop tracks

  return StatusCode::SUCCESS;
}

/**
 * Checks whether there is a hit in the given sector.
 *
 * @param hits list of clusters.
 * @param testsector uniqueID of the wanted sector
 * @param resCut window size
 *
 * @return \e true if yes.
 */
bool STEfficiency::foundHitInSector( const ISTClusterCollector::Hits& hits, Track const& track,
                                     const unsigned int testsector, const double resCut, const bool toplot ) const {
  for ( const auto& aHit : hits ) {
    if ( aHit.cluster->channelID().uniqueSector() == testsector ) {
      if ( fullDetail() && toplot ) {
        plot( aHit.residual, "Control/HitResidual", "Hit residual", -resCut, resCut,
              static_cast<unsigned int>( resCut * 100 + 0.01 ) * 2 );
        plot( aHit.cluster->totalCharge(), "Control/HitCharge", "Hit charge", 0., 200., 200 );
      }
      if ( m_resPlot.value() && toplot ) {
        std::string name( "perSector/Residuals_" );
        name += aHit.cluster->sectorName();
        plot( aHit.residual, name, "Hit residual", -resCut, resCut,
              static_cast<unsigned int>( resCut * 100 + 0.01 ) * 2 );
      }
      if ( fabs( aHit.residual ) < resCut ) {
        if ( !( m_everyHit.value() || track.isOnTrack( LHCbID( aHit.cluster->channelID() ) ) ) ) continue;
        if ( aHit.cluster->totalCharge() < m_minCharge ) continue;
        return true;
      }
    }
  }

  return false;
}

//=============================================================================
//  Finalize
//=============================================================================
StatusCode STEfficiency::finalize() {
  if ( msgLevel( MSG::DEBUG ) ) debug() << "==> Finalize" << endmsg;

  if ( m_totalExpected == 0 ) {
    std::string msg( "I'm sorry, but no track was found, please check that" );
    msg += " you selected the right track type.\nYour selection was : ";
    for ( unsigned int i( 0 ); i < m_wantedTypes.size(); i++ ) msg += std::to_string( m_wantedTypes[i] ) + ' ';
    info() << msg << endmsg;
    return TrackMonitorBase::finalize();
  }

  STChannelID channelID;
  std::string nick, root;

  unsigned int i, presentCut( 0u );

  //  double totExpected = 0; double totFound = 0;
  double nExpected, nFound, err;
  double eff = 999.0;
  double nBins;

  std::unique_ptr<Histo2DProperties> prop;
  std::unique_ptr<Histo2DProperties> propSectorStatus;
  std::unique_ptr<Histo2DProperties> propNbHits;
  std::unique_ptr<Histo2DProperties> propNbFoundHits;

  if ( m_detType == "IT" ) {
    prop             = std::make_unique<ST::ITDetectorPlot>( "EfficiencyPlot", "Efficiency Plot" );
    propSectorStatus = std::make_unique<ST::ITDetectorPlot>( "SectorStatus", "Sector Status Plot" );
    propNbHits       = std::make_unique<ST::ITDetectorPlot>( "NbExpHits", "Number of Expected Hits Plot" );
    propNbFoundHits  = std::make_unique<ST::ITDetectorPlot>( "NbFoundHits", "Number of Found Hits Plot" );
    nBins            = 336;
  } else {
    prop             = std::make_unique<ST::TTDetectorPlot>( "EfficiencyPlot", "Efficiency Plot" );
    propSectorStatus = std::make_unique<ST::TTDetectorPlot>( "SectorStatus", "Sector Status Plot" );
    propNbHits       = std::make_unique<ST::TTDetectorPlot>( "NbExpHits", "Number of Expected Hits Plot" );
    propNbFoundHits  = std::make_unique<ST::TTDetectorPlot>( "NbFoundHits", "Number of Found Hits Plot" );
    nBins            = 312;
  }

  IHistogram2D* histoEff =
      ( m_effPlot.value() ? book2D( prop->name(), prop->title(), prop->minBinX(), prop->maxBinX(), prop->nBinX(),
                                    prop->minBinY(), prop->maxBinY(), prop->nBinY() )
                          : nullptr );

  IProfile2D* histoSectorStatus = bookProfile2D(
      propSectorStatus->name(), propSectorStatus->title(), propSectorStatus->minBinX(), propSectorStatus->maxBinX(),
      propSectorStatus->nBinX(), propSectorStatus->minBinY(), propSectorStatus->maxBinY(), propSectorStatus->nBinY() );

  IHistogram2D* histoNbHits =
      book2D( propNbHits->name(), propNbHits->title(), propNbHits->minBinX(), propNbHits->maxBinX(),
              propNbHits->nBinX(), propNbHits->minBinY(), propNbHits->maxBinY(), propNbHits->nBinY() );

  IHistogram2D* histoNbFoundHits = book2D(
      propNbFoundHits->name(), propNbFoundHits->title(), propNbFoundHits->minBinX(), propNbFoundHits->maxBinX(),
      propNbFoundHits->nBinX(), propNbFoundHits->minBinY(), propNbFoundHits->maxBinY(), propNbFoundHits->nBinY() );

  IHistogram1D* histoEff_1D    = ( m_SummaryHist.value() ? book1D( "SummaryEfficiencyPlot", "Per sector efficiencies",
                                                                -0.5, nBins + 0.5, nBins + 1 )
                                                      : nullptr );
  IHistogram1D* histoNbHits_1D = ( m_SummaryHist.value() ? book1D( "SummaryNbExpectedPlot", "Per sector Nb expected",
                                                                   -0.5, nBins + 0.5, nBins + 1 )
                                                         : nullptr );
  IHistogram1D* histoNbFoundHits_1D =
      ( m_SummaryHist.value() ? book1D( "SummaryNbFoundPlot", "Per sector Nb found", -0.5, nBins + 0.5, nBins + 1 )
                              : nullptr );

  for ( const auto& sector : m_nameMapSector ) {
    channelID  = sector.second->elementID();
    nick       = sector.second->nickname();
    presentCut = m_whichCut[sector.second->isStereo() ? 1 : 0];

    root = "perSector";

    // how many were expected ?
    nExpected = static_cast<double>( m_expectedSector[sector.first] );

    if ( nExpected >= 1 ) {
      if ( fullDetail() )
        plot( -10., root + "/NbFoundHits_" + nick, "Number of Found Hits vs Cut",
              m_spacialCuts.value().front() - m_binSize / 2., m_spacialCuts.value().back() + m_binSize / 2.,
              m_binNumber, nExpected );
      if ( fullDetail() )
        plot( -10., root + "/NbExpHits_" + nick, "Number of Found Hits vs Cut",
              m_spacialCuts.value().front() - m_binSize / 2., m_spacialCuts.value().back() + m_binSize / 2.,
              m_binNumber, nExpected );
      if ( m_effPlot.value() ) {
        if ( fullDetail() )
          profile1D( -10., nExpected, root + "/Eff_" + nick, "Efficiency vs Cut",
                     m_spacialCuts.value().front() - m_binSize / 2., m_spacialCuts.value().back() + m_binSize / 2.,
                     m_binNumber );
      }
      if ( m_detType == "IT" ) {
        ST::ITDetectorPlot::Bins theBins =
            static_cast<ST::ITDetectorPlot*>( propSectorStatus.get() )->toBins( channelID );
        histoSectorStatus->fill( theBins.xBin, theBins.yBin, 1 );
      } else {
        ST::TTDetectorPlot::Bins theBins = static_cast<ST::TTDetectorPlot*>( propSectorStatus.get() )
                                               ->toBins( static_cast<DeTTSector*>( sector.second ) );
        for ( int ybin( theBins.beginBinY ); ybin < theBins.endBinY; ybin++ ) {
          histoSectorStatus->fill( theBins.xBin, ybin, 1 );
        }
      }

      for ( i = 0; i < m_NbrOfCuts; ++i ) {
        nFound = static_cast<double>( m_foundSector[i][sector.first] );
        eff    = 100. * nFound / nExpected;
        err    = sqrt( eff * ( 100. - eff ) / nExpected );

        if ( fullDetail() )
          plot( m_spacialCuts[i], root + "/NbFoundHits_" + nick, "Number of Found Hits vs Cut",
                m_spacialCuts.value().front() - m_binSize / 2., m_spacialCuts.value().back() + m_binSize / 2.,
                m_binNumber, nFound );
        if ( fullDetail() )
          plot( m_spacialCuts[i], root + "/NbExpHits_" + nick, "Number of Exp Hits vs Cut",
                m_spacialCuts.value().front() - m_binSize / 2., m_spacialCuts.value().back() + m_binSize / 2.,
                m_binNumber, nExpected );
        if ( m_effPlot.value() ) {
          if ( fullDetail() )
            profile1D( m_spacialCuts[i], eff, root + "/Eff_" + nick, "Efficiency vs cut",
                       m_spacialCuts.value().front() - m_binSize / 2., m_spacialCuts.value().back() + m_binSize / 2.,
                       m_binNumber );
        }
        if ( i == presentCut ) {
          verbose() << nick << ' ' << eff << " +/- " << err << " (found " << nFound << " for cut " << m_spacialCuts[i]
                    << ")" << endmsg;

          if ( m_detType == "IT" ) {
            ST::ITDetectorPlot::Bins theBins =
                static_cast<ST::ITDetectorPlot*>( propSectorStatus.get() )->toBins( channelID );
            if ( m_effPlot.value() ) histoEff->fill( theBins.xBin, theBins.yBin, eff );
            histoNbHits->fill( theBins.xBin, theBins.yBin, nExpected );
            histoNbFoundHits->fill( theBins.xBin, theBins.yBin, nFound );
            if ( m_SummaryHist.value() ) {
              histoEff_1D->fill( IThistoBin( channelID ), eff );
              histoNbHits_1D->fill( IThistoBin( channelID ), nExpected );
              histoNbFoundHits_1D->fill( IThistoBin( channelID ), nFound );
            }

          } else {
            ST::TTDetectorPlot::Bins theBins = static_cast<ST::TTDetectorPlot*>( propSectorStatus.get() )
                                                   ->toBins( static_cast<DeTTSector*>( sector.second ) );
            for ( int ybin( theBins.beginBinY ); ybin < theBins.endBinY; ybin++ ) {
              if ( m_effPlot.value() ) histoEff->fill( theBins.xBin, ybin, eff );
              histoNbHits->fill( theBins.xBin, ybin, nExpected );
              histoNbFoundHits->fill( theBins.xBin, ybin, nFound );
            }
            if ( m_SummaryHist.value() ) {
              histoEff_1D->fill( TThistoBin( channelID ), eff );
              histoNbHits_1D->fill( TThistoBin( channelID ), nExpected );
              histoNbFoundHits_1D->fill( TThistoBin( channelID ), nFound );
            }
          }
        }
      } // i
    }   // nExpected
    else {
      if ( m_detType == "IT" ) {
        ST::ITDetectorPlot::Bins theBins =
            static_cast<ST::ITDetectorPlot*>( propSectorStatus.get() )->toBins( channelID );
        if ( m_effPlot.value() ) histoEff->fill( theBins.xBin, theBins.yBin, -1 );
        if ( m_SummaryHist.value() ) {
          histoEff_1D->fill( IThistoBin( channelID ), -1 );
          histoNbHits_1D->fill( IThistoBin( channelID ), 0 );
          histoNbFoundHits_1D->fill( IThistoBin( channelID ), 0 );
        }
        histoSectorStatus->fill( theBins.xBin, theBins.yBin, -1 );
        histoNbHits->fill( theBins.xBin, theBins.yBin, 0 );
        histoNbFoundHits->fill( theBins.xBin, theBins.yBin, 0 );
      } else {
        ST::TTDetectorPlot::Bins theBins = static_cast<ST::TTDetectorPlot*>( propSectorStatus.get() )
                                               ->toBins( static_cast<DeTTSector*>( sector.second ) );
        for ( int ybin( theBins.beginBinY ); ybin < theBins.endBinY; ybin++ ) {
          if ( m_effPlot.value() ) histoEff->fill( theBins.xBin, ybin, -1 );
          histoSectorStatus->fill( theBins.xBin, ybin, -1 );
          histoNbHits->fill( theBins.xBin, ybin, 0 );
          histoNbFoundHits->fill( theBins.xBin, ybin, 0 );
        }
        if ( m_SummaryHist.value() ) {
          histoEff_1D->fill( TThistoBin( channelID ), -1 );
          histoNbHits_1D->fill( TThistoBin( channelID ), 0 );
          histoNbFoundHits_1D->fill( TThistoBin( channelID ), 0 );
        }
      }
    }
  } // sector

  return TrackMonitorBase::finalize(); // must be called after all other actions
}

/**
 * Stupid function that allows to print the number with 2 decimals,
 * since using std::setprecision and std::fixed directly in the info()
 * screws everythings up.
 *
 * @param nbr number (double) to print
 * @param digits number of wanted decimal (2 is the default)
 *
 * @return a string with the correct number of decimals.
 */
std::string STEfficiency::formatNumber( const double& nbr, const unsigned int& digits ) const {
  std::stringstream ss;
  ss.setf( std::ios::fixed, std::ios::floatfield );
  ss << std::setprecision( digits ) << nbr;
  return ss.str();
}

unsigned int STEfficiency::TThistoBin( const LHCb::STChannelID& chan ) const {

  // convert sector to a flat number (312 in total)
  return ( chan.station() - 1 ) * 26 * 3 * 2 + ( chan.layer() - 1 ) * 26 * 3 + ( chan.detRegion() - 1 ) * 26 +
         ( chan.sector() - 1 );
}

unsigned int STEfficiency::IThistoBin( const LHCb::STChannelID& chan ) const {
  // convert sector to a flat number (336 in total)
  return ( chan.station() - 1 ) * 7 * 4 * 4 + ( chan.layer() - 1 ) * 7 * 4 + ( chan.detRegion() - 1 ) * 7 +
         ( chan.sector() - 1 );
}

/**
 *
 */
bool STEfficiency::hasMinStationPassed( const LHCb::Track& aTrack ) const {
  std::set<unsigned int>           stations;
  const std::vector<LHCb::LHCbID>& ids = aTrack.lhcbIDs();

  for ( auto iter = ids.begin(); iter != ids.end(); ++iter ) {
    if ( m_detType == "IT" ) {
      if ( iter->isIT() ) stations.insert( iter->stID().station() );
    } else {
      if ( iter->isTT() ) stations.insert( iter->stID().station() );
    }
  }

  if ( fullDetail() )
    plot( stations.size(), "Control/NbCrossedStation", "Number of stations crossed by the track", -0.5, 3.5, 4 );

  if ( stations.size() >= m_minStationPassed ) return true;

  return false;
}

//=============================================================================
