#ifndef TRACKKERNEL_TRACKVERTEXUTILS_H
#define TRACKKERNEL_TRACKVERTEXUTILS_H

/***************************************************************************** \
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/State.h"
#include "GaudiKernel/Point3DTypes.h"
#include "LHCbMath/MatVec.h"

namespace LHCb {

  namespace TrackVertexUtils {

    enum ReturnStatus { Failure, Success };

    ///////////////////////////////////////////////////////////////////////////
    /// Return the chi2 of a track state with respect to a
    /// vertex. This is also known as the 'IPCHI2'.
    ///////////////////////////////////////////////////////////////////////////
    template <typename STATE>
    auto vertexChi2( const STATE& state, const Gaudi::XYZPoint& vertexpos, const Gaudi::SymMatrix3x3& vertexcov );

    ///////////////////////////////////////////////////////////////////////////
    /// Return the chi2 of the vertex of two track states
    ///////////////////////////////////////////////////////////////////////////
    template <typename STATEA, typename STATEB>
    auto vertexChi2( const STATEA& stateA, const STATEB& stateB );

    ///////////////////////////////////////////////////////////////////////////
    /// Return the doca between two track states
    ///////////////////////////////////////////////////////////////////////////
    template <typename StateVector>
    auto doca( const StateVector& stateA, const StateVector& stateB );

    ///////////////////////////////////////////////////////////////////////////
    /// Return the distance between a track state and a point
    ///////////////////////////////////////////////////////////////////////////
    template <typename StateVector, typename XYZPoint>
    auto doca( const StateVector& stateA, const XYZPoint& pos );

    /////////////////////////////////////////////////////////////////////////
    /// Compute the chi2 and decaylength of a 'particle' with respect
    /// to a vertex. Return 1 if successful.
    /// This should probably go into LHCb math.
    /////////////////////////////////////////////////////////////////////////
    ReturnStatus computeChiSquare( const Gaudi::XYZPoint& pos, const Gaudi::XYZVector& mom,
                                   const Gaudi::SymMatrix6x6& cov6, const Gaudi::XYZPoint& motherpos,
                                   const Gaudi::SymMatrix3x3& mothercov, double& chi2, double& decaylength,
                                   double& decaylengtherr );

    template <typename float_v, typename Vec_t, typename Sym6x6_t>
    ReturnStatus computeChiSquare( Vec_t const& pos, Vec_t const& mom, Sym6x6_t const& cov6,
                                   Gaudi::XYZPoint const& motherpos, Gaudi::SymMatrix3x3 const& mothercov,
                                   float_v& chi2, float_v& decaylength, float_v& decaylengtherr ) {
      // pos:  decay vertex of particle
      // vec:  direction or momentum of particle (does not need to be normalized)
      // cov6: corresponding covariance matrix

      // This calculation is basically a 1-iteration beamspot fit. The
      // constraint is
      //
      //    r = x - lambda p/|p| - xbs
      //
      // where x and p are the position of the decay vertex of the
      // candidate and its momentum, lambda is the decaylength and xbs
      // the position of the beamspot. The covariance in the constraint
      // is
      //
      //    V = Vbs + Vxx - a * Vxp - a Vxp^T + a^2 * Vpp
      //
      // where a=lambda/|p|^2. It needs an initial estimate for the
      // flightlength, for which we simply take the projection of deltaX
      // on the direction. We now minimize  the chisquare contribution
      //
      //     chi^2 = r^T V^{-1} r
      //
      // for lambda.

      Vec_t dx{};
      dx( 0 )    = pos( 0 ) - float( motherpos.X() );
      dx( 1 )    = pos( 1 ) - float( motherpos.Y() );
      dx( 2 )    = pos( 2 ) - float( motherpos.Z() );
      auto p3mag = mom.mag();
      auto dir   = mom * ( 1.f / p3mag ); // rsqrt

      LHCb::LinAlg::resize_t<Sym6x6_t, 3> W{};
      LHCb::Utils::unwind<0, 3>(
          [&]( auto i ) { LHCb::Utils::unwind<0, i + 1>( [&]( auto j ) { W( i, j ) = mothercov( i, j ); } ); } );
      // TODO remove explicit copy of mothercov?

      auto a = dir.dot( dx ) / p3mag;
      for ( size_t row = 0; row < 3; ++row )
        for ( size_t col = 0; col <= row; ++col )
          W( row, col ) +=
              cov6( row, col ) + a * a * cov6( row + 3, col + 3 ) - a * ( cov6( row + 3, col ) + cov6( col + 3, row ) );
      // TODO check if easier to just give 3x3 cov matrices instead of creating 6x6 cov matrix

      W = W.invChol();

      auto halfdChi2dLam2 = similarity( dir, W );
      decaylength         = dir.dot( W * dx ) / halfdChi2dLam2;
      decaylengtherr      = sqrt( 1 / halfdChi2dLam2 ); // put rsqrt here

      auto res = dx - dir * decaylength;

      chi2 = similarity( res, W );

      return Success;
    }

    /////////////////////////////////////////////////////////////////////////
    /// Compute the point of the doca of two track states.  Return 1 if successful.
    /////////////////////////////////////////////////////////////////////////
    template <typename StateVector, typename XYZPoint>
    ReturnStatus poca( const StateVector& stateA, const StateVector& stateB, XYZPoint& vertex );

    /////////////////////////////////////////////////////////////////////////
    /// Add a track to a vertex represented by a position and a cov
    /// matrix. Returns the chi2 increment. This routine calls the
    /// routine below that also has a vertex weight matrix, because
    /// the formalism requires the computation of both. If you add
    /// tracks to vertex one-by-one, then it makes sense to keep the
    /// weight matrix as this is more precise.
    /////////////////////////////////////////////////////////////////////////
    double addToVertex( const LHCb::State& state, Gaudi::XYZPoint& vertexpos, Gaudi::SymMatrix3x3& vertexcov );

    /////////////////////////////////////////////////////////////////////////
    /// Add a track to a vertex represented by a position and a weight
    /// matrix. Both the weight matrix and the covariance matrix are
    /// computed. Returns the chi2
    /////////////////////////////////////////////////////////////////////////
    double addToVertex( const LHCb::State& state, Gaudi::XYZPoint& vertexpos, Gaudi::SymMatrix3x3& vertexweight,
                        Gaudi::SymMatrix3x3& vertexcov );

    /////////////////////////////////////////////////////////////////////////
    /// Computes a vertex from two track states.
    /////////////////////////////////////////////////////////////////////////
    double vertex( const LHCb::State& stateA, const LHCb::State& stateB, Gaudi::XYZPoint& vertexpos,
                   Gaudi::SymMatrix3x3& vertexweight, Gaudi::SymMatrix3x3& vertexcov );

    /////////////////////////////////////////////////////////////////////////
    /// Computes a vertex from two track states.
    /////////////////////////////////////////////////////////////////////////
    double vertex( const LHCb::State& stateA, const LHCb::State& stateB, Gaudi::XYZPoint& vertexpos,
                   Gaudi::SymMatrix3x3& vertexcov );

  } // namespace TrackVertexUtils
} // namespace LHCb

#include "TrackKernel/TrackVertexUtils.icpp"

#endif
