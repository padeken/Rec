###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
################################################################################
# Package: TrackFitEvent
################################################################################
gaudi_subdir(TrackFitEvent v6r6)

gaudi_depends_on_subdirs(Det/FTDet
                         Det/MuonDet
                         Det/STDet
                         Det/UTDet
                         Det/VPDet
                         Det/VeloDet
                         Event/DigiEvent
                         Event/MCEvent
                         Event/TrackEvent
                         Kernel/LHCbKernel
                         Kernel/LHCbMath
                         )

find_package(Boost)
find_package(ROOT)
include_directories(SYSTEM ${Boost_INCLUDE_DIRS} ${ROOT_INCLUDE_DIRS})

gaudi_add_library(TrackFitEvent
                  src/*.cpp
                  PUBLIC_HEADERS Event
                  INCLUDE_DIRS Event/DigiEvent 
                  LINK_LIBRARIES FTDetLib MuonDetLib STDetLib UTDetLib VPDetLib VeloDetLib MCEvent TrackEvent LHCbKernel LHCbMathLib)

