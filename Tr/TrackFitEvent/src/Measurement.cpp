/*****************************************************************************\
* (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/Measurement.h"

#include "FTDet/DeFTMat.h"
#include "MuonDet/DeMuonChamber.h"
#include "UTDet/DeUTSector.h"
#include "VPDet/DeVPSensor.h"

Gaudi::XYZPoint LHCb::Measurement::toLocal( const Gaudi::XYZPoint& globalPoint ) const {
  return visit( [&globalPoint]( const FT& ft ) -> Gaudi::XYZPoint { return ft.mat->toLocal( globalPoint ); },
                [&globalPoint]( const Muon& m ) -> Gaudi::XYZPoint { return m.chamber->toLocal( globalPoint ); },
                []( const UT& ) -> Gaudi::XYZPoint { throw std::string( "UT has no toLocal method" ); },
                [&globalPoint]( const VP& vp ) -> Gaudi::XYZPoint { return vp.sensor->toLocal( globalPoint ); } );
}

Gaudi::XYZVector LHCb::Measurement::toLocal( const Gaudi::XYZVector& globalPoint ) const {
  return visit( [&globalPoint]( const FT& ft ) -> Gaudi::XYZVector { return ft.mat->toLocal( globalPoint ); },
                [&globalPoint]( const Muon& m ) -> Gaudi::XYZVector { return m.chamber->toLocal( globalPoint ); },
                []( const UT& ) -> Gaudi::XYZVector { throw std::string( "UT has no toLocal method" ); },
                [&globalPoint]( const VP& vp ) -> Gaudi::XYZVector { return vp.sensor->toLocal( globalPoint ); } );
}

Gaudi::XYZPoint LHCb::Measurement::toGlobal( const Gaudi::XYZPoint& globalPoint ) const {
  return visit( [&globalPoint]( const FT& ft ) -> Gaudi::XYZPoint { return ft.mat->toGlobal( globalPoint ); },
                [&globalPoint]( const Muon& m ) -> Gaudi::XYZPoint { return m.chamber->toGlobal( globalPoint ); },
                []( const UT& ) -> Gaudi::XYZPoint { throw std::string( "UT has no toGlobal method" ); },
                [&globalPoint]( const VP& vp ) -> Gaudi::XYZPoint { return vp.sensor->toGlobal( globalPoint ); } );
}

Gaudi::XYZVector LHCb::Measurement::toGlobal( const Gaudi::XYZVector& globalPoint ) const {
  return visit( [&globalPoint]( const FT& ft ) -> Gaudi::XYZVector { return ft.mat->toGlobal( globalPoint ); },
                [&globalPoint]( const Muon& m ) -> Gaudi::XYZVector { return m.chamber->toGlobal( globalPoint ); },
                []( const UT& ) -> Gaudi::XYZVector { throw std::string( "UT has no toGlobal method" ); },
                [&globalPoint]( const VP& vp ) -> Gaudi::XYZVector { return vp.sensor->toGlobal( globalPoint ); } );
}

std::string LHCb::Measurement::name() const {
  return visit( []( const FT& ft ) -> std::string { return ft.mat->name(); },
                []( const Muon& m ) -> std::string { return m.chamber->name(); },
                []( const UT& ut ) -> std::string { return ut.sector->name(); },
                []( const VP& vp ) -> std::string { return vp.sensor->name(); } );
}

bool LHCb::Measurement::isSameDetectorElement( const Measurement& other ) const {
  return std::visit(
      details::Measurement::overloaded{[]( const FT& lhs, const FT& rhs ) { return lhs.mat == rhs.mat; },
                                       []( const Muon& lhs, const Muon& rhs ) { return lhs.chamber == rhs.chamber; },
                                       []( const UT& lhs, const UT& rhs ) { return lhs.sector == rhs.sector; },
                                       []( const VP& lhs, const VP& rhs ) { return lhs.sensor == rhs.sensor; },
                                       []( ... ) { return false; }},
      m_sub, other.m_sub );
}
