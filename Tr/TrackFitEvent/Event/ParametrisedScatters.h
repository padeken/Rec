/*****************************************************************************\
* (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
// Rec
#include "Event/PrFitNode.h"
// LHCb
#include "Event/FitNode.h"
#include "Event/TrackTypes.h"

/**
 * This file contains the implementation for a ``simple'' per-node
 * scattering parametrisation, intended to be used in the
 * TrackMasterFitter.
 *
 * Information about its implementation can be found here:
 * https://surfdrive.surf.nl/files/index.php/s/B4pOFnL1rDRFfvO
 *
 * @author Wouter Hulsbergen
 **/
namespace TrackFit {
  namespace param_scatter_impl {
    enum NodeType {
      ClosestToBeam,
      VPHit,
      EndVelo,
      BegRich1,
      EndRich1,
      UTHit,
      AtT,
      FTHit,
      BegRich2,
      EndRich2,
      RefNode,
      HitNode,
      NTypes
    };

    struct ParametrisedScatter {
      float Q{0};        // effective scattering thickness in tx (usually in range 1-5)
      float etaxx{0.5};  // always in [0,1]
      float etaxtx{0.5}; // always in [0,1]
      float eloss{1.0};  // energy loss in MeV
    };

    using ParametrisedScatters = std::array<std::array<ParametrisedScatter, NTypes>, NTypes>;

    NodeType nodetype( Pr::Tracks::FitNode const& node ) {
      switch ( node.type() ) {
      case Pr::Tracks::FitNode::Type::VPHitOnTrack:
        return NodeType::VPHit;
      case Pr::Tracks::FitNode::Type::UTHitOnTrack:
        return NodeType::UTHit;
      case Pr::Tracks::FitNode::Type::FTHitOnTrack:
        return NodeType::FTHit;
      default:
        std::cout << "Unknown node type : " << node.z() << std::endl;
        return NodeType::HitNode;
      }
    }

    NodeType nodetype( const LHCb::FitNode& node ) {
      NodeType rc = NTypes;
      if ( node.hasMeasurement() ) {
        rc = node.measurement().visit(
            []( LHCb::Measurement::FT const& ) { return FTHit; }, []( LHCb::Measurement::VP const& ) { return VPHit; },
            []( LHCb::Measurement::UT const& ) { return UTHit; },
            [&]( ... ) {
              std::cout << "Unknown node type for measurement: " << node.measurement().type() << std::endl;
              return HitNode;
            } );

      } else {
        switch ( node.location() ) {
        case LHCb::State::ClosestToBeam:
          rc = ClosestToBeam;
          break;
        case LHCb::State::EndVelo:
          rc = EndVelo;
          break;
        case LHCb::State::BegRich1:
          rc = BegRich1;
          break;
        case LHCb::State::EndRich1:
          rc = EndRich1;
          break;
        case LHCb::State::BegRich2:
          rc = BegRich2;
          break;
        case LHCb::State::EndRich2:
          rc = EndRich2;
          break;
        case LHCb::State::AtT:
          rc = AtT;
          break;
        default:
          rc = RefNode;
          std::cout << "Unknown reference node type : " << node.z() << std::endl;
        }
      }
      return rc;
    }

    ParametrisedScatters fillParametrisedScatters() {
      ParametrisedScatters rc{};

      // tuned on 10k events of upgrade_DB19_01_MinBiasMD from TestFileDB
      rc[VPHit][ClosestToBeam] = {3.35, 0.796, 0.781, 1.42};
      rc[VPHit][VPHit]         = {1.25, 0.653, 0.529, 0.551};
      rc[EndVelo][VPHit]       = {4.27, 0.299, 0.24, 1.92};
      rc[BegRich1][EndVelo]    = {7.38, 0.495, 0.469, 2.49};
      rc[EndRich1][BegRich1]   = {5.74, 0.575, 0.496, 3.5};
      rc[UTHit][EndRich1]      = {1.29, 0.661, 0.6, 0.975};
      rc[UTHit][UTHit]         = {1, 0.649, 0.504, 0.603};
      rc[AtT][EndRich1]        = {68.1, 0.285, 0.211, 24.3};
      rc[AtT][UTHit]           = {4.8, 0.491, 0.388, 2.79};
      rc[FTHit][AtT]           = {0.676, 0.908, 0.883, 0.601};
      rc[FTHit][FTHit]         = {1.17, 0.62, 0.499, 1.06};
      rc[BegRich2][FTHit]      = {0.566, 0.315, 0.254, 0.507};
      rc[EndRich2][BegRich2]   = {22, 0.698, 0.614, 10.8};

      // needed for the state creation after fit in simple fitter
      rc[BegRich1][VPHit] = {13.2, 0.536, 0.479, 4.84};
      rc[FTHit][EndRich1] = {69.7, 0.336, 0.241, 24.9};
      // simple fitter doesn't use reference states so we need these transitions
      rc[UTHit][VPHit] = {22, 0.431, 0.318, 10.1};
      rc[FTHit][VPHit] = {93.2, 0.32, 0.256, 33.5};
      rc[FTHit][UTHit] = {5.71, 0.6, 0.49, 3.47};

      // fill the inverse table as well. I hope that I have the formulas right
      for ( int i = ClosestToBeam; i < NTypes; ++i )
        for ( int j = ClosestToBeam; j < i; ++j ) {
          const ParametrisedScatter s1 = rc[i][j];
          ParametrisedScatter       s2 = s1;
          s2.etaxtx                    = s1.etaxtx - 1; // take into account sign change in dz
          s2.etaxx                     = std::sqrt( 1 + s2.etaxx * s2.etaxx - 2 * s1.etaxtx );
          rc[j][i]                     = s2;
        }

      return rc;
    }

    inline std::pair<Gaudi::TrackSymMatrix, double>
    computeNoiseAndDeltaE( NodeType thisnodetype, double const x, double const y, double const z, double const tx,
                           double const ty, NodeType prevnodetype, double const prevnode_z, double const pscatter ) {
      static const ParametrisedScatters scatters = fillParametrisedScatters();
      Gaudi::TrackSymMatrix             Q;
      double                            deltaE{0};
      auto const                        dz = z - prevnode_z;

      // this cna be optimized later
      if ( std::abs( dz ) <= 0.5 ) return {Q, deltaE};

      // let's complete the correction for a thin scatterer, then use
      // that to take the known effects out of the noise.
      const auto tx2  = tx * tx;
      const auto ty2  = ty * ty;
      const auto n2   = 1 + tx2 + ty2;
      const auto n    = std::sqrt( n2 );
      const auto invp = 1 / pscatter;
      const auto norm = n2 * invp * invp * n; // I believe that LHCb tools are missing the last factor n

      const ParametrisedScatter scatter = scatters[prevnodetype][thisnodetype];
      // temporary
      if ( scatter.Q == 0 ) {
        std::cout << "Trackfit uses uninitialized scatter: " << prevnodetype << " " << thisnodetype << std::endl;
      }

      auto normCms = norm * scatter.Q;
      // add a bit extra for tracks inside the rf foil. need to find a more efficient way to do this.
      if ( prevnodetype == VPHit && thisnodetype == VPHit ) {
        const auto xprime = y + x;
        const auto yprime = y - x;
        bool infoil = ( yprime > -15 && xprime >= 0 && xprime < 15 ) || ( yprime < 15 && xprime > -15 && xprime <= 0 );
        const float rffoilscatter = 0.6;
        if ( infoil ) normCms += norm * rffoilscatter;
      }
      // else if( prevnodetype == EndVelo && thisnodetype == VPHit ) {
      // 	const auto x = state.x() ;
      // 	const auto y = state.y() ;
      // 	const auto R2 = x*x+y*y ;
      // 	if( R2 >28*28 )  normCms += norm * 9. ;
      // } else if( prevnodetype == BegRich1 && thisnodetype == EndVelo ) {
      // 	//const auto x = state.x() ;
      // 	//const auto y = state.y() ;
      // 	//const auto R2 = x*x+y*y ;
      // 	//if( R2 <40*40 )  normCms += norm * 4. ;
      // 	// I don;t think
      // 	if( t2 < 0.06*0.06 ) normCms += norm * 10 ;
      // }
      else if ( prevnodetype == AtT && thisnodetype == UTHit ) {
        if ( tx2 + ty2 < 0.02 * 0.02 ) normCms += norm * 10;
      }

      Q( 2, 2 ) = ( 1 + tx2 ) * normCms;
      Q( 3, 3 ) = ( 1 + ty2 ) * normCms;
      Q( 3, 2 ) = tx * ty * normCms;

      // x,tx part
      Q( 0, 0 ) = Q( 2, 2 ) * dz * dz * scatter.etaxx * scatter.etaxx;
      Q( 2, 0 ) = Q( 2, 2 ) * dz * scatter.etaxtx;
      // y,ty part
      Q( 1, 1 ) = Q( 3, 3 ) * dz * dz * scatter.etaxx * scatter.etaxx;
      Q( 3, 1 ) = Q( 3, 3 ) * dz * scatter.etaxtx;
      // x,y part
      Q( 1, 0 ) = Q( 3, 2 ) * dz * dz * scatter.etaxx * scatter.etaxx;
      Q( 3, 0 ) = Q( 2, 1 ) = Q( 3, 2 ) * dz * scatter.etaxtx;

      // energyloss part
      deltaE = ( dz < 0 ? 1 : -1 ) * scatter.eloss * n;
      // the landau distribution is wide: assign full Eloss as error in cov matrix
      // (since we add many small contributions, it will be small effect in the end)
      // const auto qop = state.qOverP() ;
      // const auto sigmaQOverP = qop*qop*deltaE ;
      // Q(4,4) += sigmaQOverP*sigmaQOverP ;
      return std::make_pair( Q, deltaE );
    }

    template <typename FitNodeType>
    std::pair<Gaudi::TrackSymMatrix, double> computeNoiseAndDeltaE( FitNodeType& prevnode, FitNodeType& node,
                                                                    double pscatter ) {
      const auto& state = node.refVector();
      return computeNoiseAndDeltaE( nodetype( node ), state.x(), state.y(), node.z(), state.tx(), state.ty(),
                                    nodetype( prevnode ), prevnode.z(), pscatter );
    }
  } // namespace param_scatter_impl
} // namespace TrackFit
