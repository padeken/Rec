/*****************************************************************************\
* (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
// Rec
#include "Event/PrFitNode.h"
// LHCb
#include "Event/ChiSquare.h"
#include "LHCbMath/Similarity.h"
// Gaudi
#include "GaudiKernel/Kernel.h"
// std
#include <cstddef>
#include <functional>
#include <iterator>
#include <type_traits>
#include <vector>

namespace Pr::Tracks {

  template <bool enable = false, typename... Args>
  inline void mydebug( Args&&... args ) {
    if constexpr ( enable ) { ( ( std::cout << std::forward<Args>( args ) << " " ), ... ) << std::endl; }
  }

  namespace KF {
    inline void pre_fit_init( LHCb::span<FitNode> fitnodes, double err_x2, double err_y2, double err_tx2,
                              double err_ty2, double err_qop2 ) {
      auto& seed_cov   = fitnodes.back().predicted_state_cov[FitNode::backward];
      seed_cov( 0, 0 ) = err_x2;
      seed_cov( 1, 1 ) = err_y2;
      seed_cov( 2, 2 ) = err_tx2;
      seed_cov( 3, 3 ) = err_ty2;
      seed_cov( 4, 4 ) = err_qop2;

      fitnodes.front().predicted_state_cov[FitNode::forward] = fitnodes.back().predicted_state_cov[FitNode::backward];
    }

    template <int direction = FitNode::forward>
    void predict( FitNode const& prevnode, FitNode& node ) {
      mydebug( "predict of node at z= ", node.z() );

      if constexpr ( direction == FitNode::forward ) {
        auto const& F = node.transport_matrix;
        mydebug( "transport Matrix F\n", F );
        mydebug( "transport Vector\n", node.transport_vector );
        node.predicted_state_vec[direction] = F * prevnode.filtered_state_vec[direction] + node.transport_vector;
        LHCb::Math::Similarity( F, prevnode.filtered_state_cov[direction], node.predicted_state_cov[direction] );
        node.predicted_state_cov[direction] += node.noise_matrix;
      } else {
        auto const& F_inv = prevnode.transport_matrix_inverse;
        mydebug( "inverse prevnode transport Matrix F\n", F_inv );
        mydebug( "prevnode transport Vector\n", prevnode.transport_vector );
        node.predicted_state_vec[direction] =
            F_inv * ( prevnode.filtered_state_vec[direction] - prevnode.transport_vector );

        LHCb::Math::Similarity( F_inv, prevnode.filtered_state_cov[direction] + prevnode.noise_matrix,
                                node.predicted_state_cov[direction] );
      }

      mydebug( "predict statevec:  ", node.predicted_state_vec[direction] );
      mydebug( "predict statecov:\n", node.predicted_state_cov[direction] );
    }

    template <int direction = FitNode::forward>
    LHCb::ChiSquare filter( FitNode& node ) {
      mydebug( "filter of node at z= ", node.z() );

      node.filtered_state_vec[direction] = node.predicted_state_vec[direction];
      node.filtered_state_cov[direction] = node.predicted_state_cov[direction];

      if ( UNLIKELY( node.type() == FitNode::Type::Outlier ) ) {
        node.delta_chi2[direction] = LHCb::ChiSquare{};
        return node.delta_chi2[direction];
      }

      // EKF res = residual + H * (x_ref - x)
      mydebug( "RefVector:  ", node.ref_vector() );
      mydebug( "residual:  ", node.ref_residual );
      mydebug( "error:  ", node.measurement_error );
      mydebug( "projection:  ", node.projection );
      double chi2 =
          LHCb::Math::Filter( node.filtered_state_vec[direction], node.filtered_state_cov[direction], node.ref_vector(),
                              node.projection, node.ref_residual, node.measurement_error * node.measurement_error );

      node.delta_chi2[direction] = LHCb::ChiSquare( chi2, 1 );

      mydebug( "filter statevec:  ", node.filtered_state_vec[direction] );
      mydebug( "filter statecov:\n", node.filtered_state_cov[direction] );
      mydebug( "delta chi2: ", node.delta_chi2[direction] );
      return node.delta_chi2[direction];
    }

    inline void average_node( FitNode& node ) {
      mydebug( "average of node at z= ", node.z() );

      int const filter_idx =
          ( node.predicted_state_cov[FitNode::backward]( 0, 0 ) > node.predicted_state_cov[FitNode::forward]( 0, 0 ) )
              ? FitNode::backward
              : FitNode::forward;
      int const predict_idx = ( filter_idx + 1 ) % 2;

      LHCb::Math::Average( node.filtered_state_vec[filter_idx], node.filtered_state_cov[filter_idx],
                           node.predicted_state_vec[predict_idx], node.predicted_state_cov[predict_idx],
                           node.final_state_vec, node.final_state_cov );

      mydebug( "average filter_idx:", filter_idx, "predict_idx:", predict_idx );
      mydebug( "average statevec:  ", node.final_state_vec );
      mydebug( "average statecov:\n", node.final_state_cov, "\n" );
    }

    template <int direction = FitNode::forward>
    LHCb::ChiSquare predict_and_filter( FitNode const& prevnode, FitNode& node ) {
      predict<direction>( prevnode, node );
      return filter<direction>( node );
    }

    inline LHCb::ChiSquare fit( LHCb::span<FitNode> fitnodes ) {
      // not checking that first_hit_node_fwd != end or that I can increment the iterator safely.
      // But it should be safe since we know our track is made of a couple of hits
      // and I can't think of a scenario where this could crash.

      fitnodes.front().predicted_state_vec[FitNode::forward] = fitnodes.front().ref_vector();
      auto chi2_fwd = filter<FitNode::forward>( fitnodes.front() ) + LHCb::ChiSquare{0, -5};
      for ( size_t i{1}; i < fitnodes.size(); ++i ) {
        chi2_fwd += predict_and_filter<FitNode::forward>( fitnodes[i - 1], fitnodes[i] );
      }

      // start from forward filter
      fitnodes.back().predicted_state_vec[FitNode::backward] = fitnodes.back().filtered_state_vec[FitNode::forward];
      // start independent from ref_vector
      // fitnodes.back().predicted_state_vec[FitNode::backward] = fitnodes.back().ref_vector();
      auto chi2_bkwd = filter<FitNode::backward>( fitnodes.back() ) + LHCb::ChiSquare{0, -5};
      for ( int i( fitnodes.size() - 2 ); i >= 0; --i ) {
        chi2_bkwd += predict_and_filter<FitNode::backward>( fitnodes[i + 1], fitnodes[i] );
      }

      // use the smaller chi2
      mydebug( chi2_fwd, chi2_bkwd );
      return chi2_fwd.chi2() < chi2_bkwd.chi2() ? chi2_fwd : chi2_bkwd;
    }

    inline void smooth_and_update_ref_vector( LHCb::span<FitNode> fitnodes ) {
      fitnodes.front().ref_vector() = fitnodes.front().filtered_state_vec[FitNode::backward];
      fitnodes.back().ref_vector()  = fitnodes.back().filtered_state_vec[FitNode::forward];

      std::for_each( fitnodes.begin() + 1, fitnodes.end() - 1, []( FitNode& node ) {
        average_node( node );
        node.ref_vector() = node.final_state_vec;
      } );
    }

    inline void smooth_and_update_ref_vector_outlier( LHCb::span<FitNode> fitnodes ) {

      auto forward_iter  = fitnodes.begin();
      auto backward_iter = fitnodes.rbegin();

      // the first hit is never averaged and following ones only if the previous ones aren't all outliers
      do {
        forward_iter->ref_vector()    = forward_iter->filtered_state_vec[FitNode::backward];
        forward_iter->final_state_cov = forward_iter->filtered_state_cov[FitNode::backward];
        ++forward_iter;
      } while ( forward_iter->type() == FitNode::Type::Outlier );

      // same as above from the other end
      do {
        backward_iter->ref_vector()    = backward_iter->filtered_state_vec[FitNode::forward];
        backward_iter->final_state_cov = backward_iter->filtered_state_cov[FitNode::forward];
        ++backward_iter;
      } while ( backward_iter->type() == FitNode::Type::Outlier );

      std::for_each( fitnodes.begin() + std::distance( fitnodes.begin(), forward_iter ),
                     fitnodes.end() - std::distance( fitnodes.rbegin(), backward_iter ), []( FitNode& node ) {
                       average_node( node );
                       node.ref_vector() = node.final_state_vec;
                     } );
    }

  } // namespace KF
} // namespace Pr::Tracks
