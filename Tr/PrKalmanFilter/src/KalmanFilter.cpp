/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "KF.h"
// Rec
#include "Event/ParametrisedScatters.h"
#include "Event/PrFitNode.h"
#include "PrKernel/PrSciFiHits.h"
#include "PrKernel/PrUTHitHandler.h"
#include "TrackInterfaces/ITrackExtrapolator.h"
// LHCb
#include "Event/ChiSquare.h"
#include "Event/PrDownstreamTracks.h"
#include "Event/PrLongTracks.h"
#include "Event/PrSeedTracks.h"
#include "Event/PrUTHits.h"
#include "Event/PrVeloHits.h"
#include "Event/StateParameters.h"
#include "Event/StateVector.h"
#include "Event/Track.h"
#include "Event/TrackTypes.h"
#include "Kernel/STLExtensions.h"
#include "LHCbMath/LHCbMath.h"
#include "LHCbMath/Similarity.h"
// Gaudi
#include "Gaudi/Accumulators.h"
#include "GaudiAlg/Transformer.h"
#include "GaudiKernel/GaudiException.h"
// std
#include <algorithm>
#include <type_traits>

namespace {
  double calc_ctb_z( LHCb::State const& state ) {
    auto const& vec = state.stateVector();
    auto        z   = state.z();
    // check on division by zero (track parallel to beam line!)
    if ( vec[2] != 0 || vec[3] != 0 ) {
      z -= ( vec[0] * vec[2] + vec[1] * vec[3] ) / ( vec[2] * vec[2] + vec[3] * vec[3] );
    }
    return z;
  }

  void make_averaged_state( std::vector<LHCb::State*>& statevec, LHCb::State const& state_up,
                            LHCb::State const& state_down ) {

    auto* new_state = statevec.emplace_back( new LHCb::State{state_up.location()} );
    new_state->setZ( state_up.z() );
    LHCb::Math::Average( state_up.stateVector(), state_up.covariance(), state_down.stateVector(),
                         state_down.covariance(), new_state->stateVector(), new_state->covariance() );
  }
} // namespace
// TrackFit dev namespace to avoid collisions
using namespace Pr::Tracks;

template <typename InputTrackType>
class KalmanFilter
    : public Gaudi::Functional::Transformer<LHCb::Event::Tracks( InputTrackType const&, LHCb::Pr::Velo::Hits const&,
                                                                 LHCb::Pr::UT::HitHandler const&,
                                                                 SciFiHits::PrSciFiHits const& )> {
public:
  using base_t = Gaudi::Functional::Transformer<LHCb::Event::Tracks( InputTrackType const&, LHCb::Pr::Velo::Hits const&,
                                                                     LHCb::Pr::UT::HitHandler const&,
                                                                     SciFiHits::PrSciFiHits const& )>;

  using base_t::info;
  KalmanFilter( std::string const& name, ISvcLocator* pSvcLocator )
      : base_t( name, pSvcLocator,
                {typename base_t::KeyValue{"Input", ""}, typename base_t::KeyValue{"HitsVP", ""},
                 typename base_t::KeyValue{"HitsUT", ""}, typename base_t::KeyValue{"HitsFT", ""}},
                typename base_t::KeyValue{"Output", ""} ) {}

  template <bool input_is_long_tracks>
  StatusCode add_fitted_states( LHCb::Event::Track& newtrack, std::vector<FitNode>& fitnodes,
                                double const scatteringMomentum ) const {

    assert( newtrack.states().size() == 0 && "this should be a new track without states on it" );

    Gaudi::TrackMatrix transMat      = ROOT::Math::SMatrixIdentity();
    auto const extrapolate_and_noise = [this, scatteringMomentum, &transMat]( LHCb::State& state, double z_target,
                                                                              auto node_type, auto prev_node_type ) {
      auto&      vec   = state.stateVector();
      auto const z_old = state.z();
      return m_extrap->propagate( vec, state.z(), z_target, &transMat ).andThen( [&] {
        state.setZ( z_target );
        // transport
        state.covariance() = LHCb::Math::Similarity( transMat, state.covariance() );
        state.covariance() +=
            TrackFit::param_scatter_impl::computeNoiseAndDeltaE( node_type, vec[0], vec[1], state.z(), vec[2], vec[3],
                                                                 prev_node_type, z_old, scatteringMomentum )
                .first;
      } );
    };

    // last state because it's largest z but since we fit from scifi -> velo
    // this will be filled with our first node
    auto& statevec = newtrack.states();

    auto const& first_hit = m_fit_upstream.value() ? fitnodes.back() : fitnodes.front();
    auto const& last_hit  = m_fit_upstream.value() ? fitnodes.front() : fitnodes.back();
    auto const  dir_1     = m_fit_upstream.value() ? FitNode::forward : FitNode::backward;
    auto const  dir_2     = m_fit_upstream.value() ? FitNode::backward : FitNode::forward;

    StatusCode ret = StatusCode::SUCCESS;
    if constexpr ( input_is_long_tracks ) {
      auto state =
          std::make_unique<LHCb::State>( first_hit.filtered_state_vec[dir_1], first_hit.filtered_state_cov[dir_1],
                                         first_hit.z(), LHCb::State::ClosestToBeam );
      auto z = calc_ctb_z( *state );
      // don't go outside the sensible volume
      if ( z > -100 * Gaudi::Units::cm or z < 700 * Gaudi::Units::cm ) {
        ret = extrapolate_and_noise( *state, z, TrackFit::param_scatter_impl::NodeType::ClosestToBeam,
                                     TrackFit::param_scatter_impl::NodeType::VPHit );
        if ( UNLIKELY( ret.isFailure() ) ) { return ret; }
        statevec.emplace_back( state.release() );
      }

      statevec.emplace_back( new LHCb::State{first_hit.filtered_state_vec[dir_1], first_hit.filtered_state_cov[dir_1],
                                             first_hit.z(), LHCb::State::Location::FirstMeasurement} );

      auto const [upstream_hit, downstream_hit] = [&]() {
        if ( m_fit_upstream.value() ) {
          auto const up = std::find_if( fitnodes.begin(), fitnodes.end(),
                                        []( auto const& node ) { return node.z() < StateParameters::ZBegRich1; } );
          assert( up != fitnodes.begin() );
          return std::pair{up, prev( up )};
        } else {
          auto const up = std::find_if( fitnodes.begin(), fitnodes.end(),
                                        []( auto const& node ) { return node.z() > StateParameters::ZBegRich1; } );
          assert( up != fitnodes.begin() );
          return std::pair{prev( up ), up};
        }
      }();

      auto beg_up = LHCb::State{upstream_hit->filtered_state_vec[dir_2], upstream_hit->filtered_state_cov[dir_2],
                                upstream_hit->z(), LHCb::State::Location::BegRich1};

      auto end_down = LHCb::State{downstream_hit->filtered_state_vec[dir_1], downstream_hit->filtered_state_cov[dir_1],
                                  downstream_hit->z(), LHCb::State::Location::LocationUnknown};

      ret = extrapolate_and_noise( beg_up, StateParameters::ZBegRich1, TrackFit::param_scatter_impl::NodeType::BegRich1,
                                   TrackFit::param_scatter_impl::nodetype( *upstream_hit ) );
      if ( UNLIKELY( ret.isFailure() ) ) { return ret; }

      LHCb::State end_up{beg_up};
      ret = extrapolate_and_noise( end_up, StateParameters::ZEndRich1, TrackFit::param_scatter_impl::NodeType::EndRich1,
                                   TrackFit::param_scatter_impl::NodeType::BegRich1 );
      if ( UNLIKELY( ret.isFailure() ) ) { return ret; }
      end_up.setLocation( LHCb::State::Location::EndRich1 );

      ret =
          extrapolate_and_noise( end_down, StateParameters::ZEndRich1, TrackFit::param_scatter_impl::NodeType::EndRich1,
                                 TrackFit::param_scatter_impl::nodetype( *downstream_hit ) );
      if ( UNLIKELY( ret.isFailure() ) ) { return ret; }

      LHCb::State beg_down{end_down};
      ret =
          extrapolate_and_noise( beg_down, StateParameters::ZBegRich1, TrackFit::param_scatter_impl::NodeType::BegRich1,
                                 TrackFit::param_scatter_impl::NodeType::EndRich1 );
      if ( UNLIKELY( ret.isFailure() ) ) { return ret; }

      make_averaged_state( statevec, beg_up, beg_down );
      make_averaged_state( statevec, end_up, end_down );

    } else {
      // for downstream tracks, first measurement is after RICH1
      // thus create state here but first add the RICH states
      auto first_meas =
          std::make_unique<LHCb::State>( first_hit.filtered_state_vec[dir_1], first_hit.filtered_state_cov[dir_1],
                                         first_hit.z(), LHCb::State::Location::FirstMeasurement );

      // RICH1 states are much simpler because they are only an extrapolation
      auto end_rich1 = std::make_unique<LHCb::State>( *first_meas );
      ret            = extrapolate_and_noise( *end_rich1, StateParameters::ZEndRich1,
                                   TrackFit::param_scatter_impl::NodeType::EndRich1,
                                   TrackFit::param_scatter_impl::nodetype( first_hit ) );
      if ( UNLIKELY( ret.isFailure() ) ) { return ret; }
      end_rich1->setLocation( LHCb::State::Location::EndRich1 );

      // to keep the right order we first insert the BegRICH1 state
      auto* beg_rich1 = statevec.emplace_back( new LHCb::State{*end_rich1} );
      // then the EndRICH1 state
      statevec.emplace_back( end_rich1.release() );

      ret = extrapolate_and_noise( *beg_rich1, StateParameters::ZBegRich1,
                                   TrackFit::param_scatter_impl::NodeType::BegRich1,
                                   TrackFit::param_scatter_impl::NodeType::EndRich1 );
      if ( UNLIKELY( ret.isFailure() ) ) { return ret; }
      beg_rich1->setLocation( LHCb::State::Location::BegRich1 );

      // now add first measurement
      statevec.emplace_back( first_meas.release() );
    }

    statevec.emplace_back( new LHCb::State{last_hit.filtered_state_vec[dir_2], last_hit.filtered_state_cov[dir_2],
                                           last_hit.z(), LHCb::State::Location::LastMeasurement} );

    // begRich2 is copy of last measurement
    // now extrapolate
    auto* beg_rich2 = statevec.emplace_back( new LHCb::State{*statevec.back()} );
    ret =
        extrapolate_and_noise( *beg_rich2, StateParameters::ZBegRich2, TrackFit::param_scatter_impl::NodeType::BegRich2,
                               TrackFit::param_scatter_impl::nodetype( last_hit ) );
    if ( UNLIKELY( ret.isFailure() ) ) {
      info() << "Warning 5" << endmsg;
      return ret;
    }
    beg_rich2->setLocation( LHCb::State::Location::BegRich2 );

    auto* end_rich2 = statevec.emplace_back( new LHCb::State{*statevec.back()} );

    ret =
        extrapolate_and_noise( *end_rich2, StateParameters::ZEndRich2, TrackFit::param_scatter_impl::NodeType::EndRich2,
                               TrackFit::param_scatter_impl::NodeType::BegRich2 );
    if ( UNLIKELY( ret.isFailure() ) ) {
      info() << "Warning 6" << endmsg;
      return ret;
    }
    end_rich2->setLocation( LHCb::State::Location::EndRich2 );

    return ret;
  }

  StatusCode init_nodes( std::vector<FitNode>& fitnodes, double const scatteringMomentum,
                         LHCb::StateVector const& ctb_state_vec, LHCb::StateVector const& scifi_state_vec ) const {

    mydebug( "Start of init_nodes" );
    // FIXME this could use some rethinking
    // Long tracks are currently initialized from CTB and AtT states but
    // we would probably do better using CTB, something in UT and something in the middle of SciFi
    // This however requires more logic than the simple extrapolate to first and last hit and then go step
    // by step that is done below
    // FIXME furhtermore this logic isn't really ideal for downstream where the upstream seed is
    // in the middel of the UT, thus we extrapolate that to the first UT it and then go hit by hit
    // instead of going lef and right from the ut state.
    Gaudi::TrackMatrix F{ROOT::Math::SMatrixIdentity()};

    auto const fit_from_scifi_to_velo = fitnodes.front().z() > fitnodes.back().z();
    // if we start from fitting from scifi nodes -> velo nodes
    // we need want to init the first node based on scifi state, otherwise ctb state
    StatusCode ret;
    {
      auto start_vec = fit_from_scifi_to_velo ? scifi_state_vec : ctb_state_vec;
      ret            = m_extrap->propagate( start_vec, fitnodes.front().z() );
      if ( UNLIKELY( ret.isFailure() ) ) { return ret; }
      // set reference state
      fitnodes.front().lhcb_ref_vector.parameters() = start_vec.parameters();
    }

    {
      auto end_vec = fit_from_scifi_to_velo ? ctb_state_vec : scifi_state_vec;
      ret          = m_extrap->propagate( end_vec, fitnodes.back().z() );
      if ( UNLIKELY( ret.isFailure() ) ) { return ret; }
      fitnodes.back().lhcb_ref_vector.parameters() = end_vec.parameters();
    }

    auto const is_on_other_side_of_mag = [fit_from_scifi_to_velo]( auto const& node ) {
      return fit_from_scifi_to_velo ? node->z() < 4000 : node->z() > 4000;
    };

    for ( auto node = next( fitnodes.rbegin() ); is_on_other_side_of_mag( node ); ++node ) {
      auto state_vec = std::prev( node )->lhcb_ref_vector;
      ret            = m_extrap->propagate( state_vec, node->z() );
      if ( UNLIKELY( ret.isFailure() ) ) { return ret; }
      node->lhcb_ref_vector.parameters() = state_vec.parameters();
    }

    auto lhcb_ref_vector{std::cref( fitnodes.begin()->lhcb_ref_vector )};

    for ( auto node = next( fitnodes.begin() ); node != fitnodes.end(); ++node ) {
      // wanted explicit copy
      auto       state_vec = lhcb_ref_vector.get();
      auto const z         = node->z();
      ret                  = m_extrap->propagate( state_vec, z, &F );
      if ( UNLIKELY( ret.isFailure() ) ) { return ret; }

      if ( not is_on_other_side_of_mag( node ) ) node->lhcb_ref_vector.parameters() = state_vec.parameters();

      std::tie( node->noise_matrix, node->delta_energy ) =
          TrackFit::param_scatter_impl::computeNoiseAndDeltaE( *( node - 1 ), *node, scatteringMomentum );

      if ( std::abs( state_vec.qOverP() ) > LHCb::Math::lowTolerance ) {
        auto charge = state_vec.qOverP() > 0 ? 1. : -1.;
        auto momnew = std::max( 10., std::abs( 1 / state_vec.qOverP() ) + node->delta_energy );
        if ( std::abs( momnew ) > 10 ) state_vec.setQOverP( charge / momnew );
      }

      Gaudi::TrackVector transportvec = state_vec.parameters() - F * lhcb_ref_vector.get().parameters();
      node->set_transport( F, transportvec );
      // update the reference
      lhcb_ref_vector = std::cref( node->lhcb_ref_vector );
    }

    mydebug( "End of init_nodes" );
    return ret;
  };

  StatusCode update_transport_matrix( std::vector<FitNode>& fitnodes ) const {
    mydebug( "Start of update_transport" );
    Gaudi::TrackMatrix F{ROOT::Math::SMatrixIdentity()};
    auto               lhcb_ref_vector{std::cref( fitnodes.front().lhcb_ref_vector )};

    StatusCode ret;
    for ( auto node = next( fitnodes.begin() ); node != fitnodes.end(); ++node ) {
      // wanted explicit copy
      auto       state_vec = lhcb_ref_vector.get();
      auto const z         = node->z();
      ret                  = m_extrap->propagate( state_vec, z, &F );
      if ( ret.isFailure() ) { return ret; }

      auto dE = node->delta_energy;
      if ( std::abs( state_vec.qOverP() ) > LHCb::Math::lowTolerance ) {
        auto charge = state_vec.qOverP() > 0 ? 1. : -1.;
        auto momnew = std::max( 10., std::abs( 1 / state_vec.qOverP() ) + dE );
        if ( std::abs( momnew ) > 10 ) state_vec.setQOverP( charge / momnew );
      }

      Gaudi::TrackVector transportvec = state_vec.parameters() - F * lhcb_ref_vector.get().parameters();
      node->set_transport( F, transportvec );
      // update the reference
      lhcb_ref_vector = std::cref( node->lhcb_ref_vector );
    }
    mydebug( "End of update_transport" );
    return ret;
  }

  template <bool input_is_long_tracks, typename T>
  LHCb::StateVector get_upstream_seed( InputTrackType const& tracks, T const& track ) const {

    if constexpr ( input_is_long_tracks ) {

      auto const vp_tracks = LHCb::Pr::make_zip<SIMDWrapper::InstructionSet::Scalar>( *tracks.getVeloAncestors() );

      auto const vp_tr = track.template get<LHCb::Pr::Long::Tag::trackVP>().cast();
      auto const qop   = track.template get<LHCb::Pr::Long::Tag::StateQoP>().cast();

      auto const vp_track = vp_tracks[vp_tr];
      auto const pos      = vp_track.StatePos( 0 );
      auto const dir      = vp_track.StateDir( 0 );

      return LHCb::StateVector{{pos.x.cast(), pos.y.cast(), dir.x.cast(), dir.y.cast(), qop}, pos.z.cast()};

    } else {

      auto const s   = track.template get<LHCb::Pr::Downstream::Tag::StatePosition>();
      auto const qop = track.template get<LHCb::Pr::Downstream::Tag::StateQoP>().cast();
      return LHCb::StateVector{{s.x().cast(), s.y().cast(), s.tx().cast(), s.ty().cast(), qop}, s.z().cast()};
    }
  }

  template <bool input_is_long_tracks, typename T>
  LHCb::StateVector get_downstream_seed( InputTrackType const& tracks, T const& track ) const {

    if constexpr ( input_is_long_tracks ) {
      namespace tag  = LHCb::Pr::Long::Tag;
      auto const qop = track.template get<tag::StateQoP>().cast();
      auto const x   = track.template get<tag::StatePositions>( 1, 0 ).cast();
      auto const y   = track.template get<tag::StatePositions>( 1, 1 ).cast();
      auto const z   = track.template get<tag::StatePositions>( 1, 2 ).cast();
      auto const tx  = track.template get<tag::StatePositions>( 1, 3 ).cast();
      auto const ty  = track.template get<tag::StatePositions>( 1, 4 ).cast();

      return LHCb::StateVector{{x, y, tx, ty, qop}, z};

    } else {
      auto const seed_tracks = tracks.getFTAncestors()->scalar();
      auto const ft_tr       = track.template get<LHCb::Pr::Downstream::Tag::trackFT>().cast();
      auto const qop         = track.template get<LHCb::Pr::Downstream::Tag::StateQoP>().cast();
      auto const ft_track    = seed_tracks[ft_tr];
      auto const s           = ft_track.template get<LHCb::Pr::Seeding::Tag::StatePositions>( 2 );
      return LHCb::StateVector{{s.x().cast(), s.y().cast(), s.tx().cast(), s.ty().cast(), qop}, s.z().cast()};
    }
  }

  LHCb::Event::Tracks operator()( InputTrackType const& tracks, LHCb::Pr::Velo::Hits const& hits_vp,
                                  LHCb::Pr::UT::HitHandler const& hits_ut,
                                  SciFiHits::PrSciFiHits const&   hits_ft ) const override {

    // info() << "INPUT: " << tracks.size() << endmsg;
    constexpr bool            input_is_long_tracks = std::is_same_v<InputTrackType, LHCb::Pr::Long::Tracks>;
    std::vector<LHCb::LHCbID> id_vec;
    id_vec.reserve( LHCb::Pr::Long::Tracks::MaxLHCbIDs );

    using dType = SIMDWrapper::scalar::types;
    using F     = dType::float_v;

    m_counter_tracks_in += tracks.size();

    auto outlier_iter_buffer{m_counter_outlier_iterations.buffer()};
    auto iter_buffer{m_counter_iterations.buffer()};
    auto transport_failed_buffer{m_counter_transport_failed.buffer()};
    auto states_failed_buffer{m_counter_states_failed.buffer()};
    auto pre_outlier_chi2_cut_buffer{m_counter_pre_outlier_cut.buffer()};
    auto chi2_cut_buffer{m_counter_cut.buffer()};

    LHCb::Tracks output;
    output.reserve( tracks.size() );
    std::vector<FitNode> fitnodes;
    fitnodes.reserve( 50 );

    std::vector<int> sort_order;
    sort_order.reserve( 20 );

    auto const input_tracks = tracks.scalar();

    for ( auto const track : input_tracks ) {
      mydebug( "intput track", track.offset() );

      auto const ctb_state_vec   = get_upstream_seed<input_is_long_tracks>( tracks, track );
      auto const scifi_state_vec = get_downstream_seed<input_is_long_tracks>( tracks, track );

      fitnodes.clear();

      // passing track in to make lambda depend on type of track
      // maybe_unused is only needed for GCC < 10
      // FIXME remove attribute once we don't have GCC 9.x anymore
      [[maybe_unused]] auto const add_vp_nodes = [&]( auto const& in_track ) {
        auto const n_hits = in_track.template get<LHCb::Pr::Long::Tag::nVPHits>().cast();
        for ( int j{0}; j < n_hits; ++j ) {
          // reverse order in case we fit don't upstream
          // note velo is by default ordered from higher to lower z-coordinate
          auto const i = m_fit_upstream.value() ? j : n_hits - 1 - j;

          auto idx = in_track.template get<LHCb::Pr::Long::Tag::vp_indices>( i ).cast();
          auto pos = hits_vp.pos<F>( idx );

          fitnodes.emplace_back( pos.x.cast(), pos.y.cast(), pos.z.cast(), 0, 1, 0, 0.0125,
                                 FitNode::Type::VPHitOnTrack );
          fitnodes.emplace_back( pos.x.cast(), pos.y.cast(), pos.z.cast(), 1, 0, 0, 0.0125,
                                 FitNode::Type::VPHitOnTrack );
        }
      };

      auto const add_ut_nodes = [&]() {
        using nUTHits =
            std::conditional_t<input_is_long_tracks, LHCb::Pr::Long::Tag::nUTHits, LHCb::Pr::Downstream::Tag::nUTHits>;
        using ut_idx = std::conditional_t<input_is_long_tracks, LHCb::Pr::Long::Tag::ut_indices,
                                          LHCb::Pr::Downstream::Tag::ut_indices>;

        auto const n_hits = track.template get<nUTHits>().cast();
        sort_order.resize( n_hits );

        // get the indices into the hit container for all our ut hits
        std::generate( sort_order.begin(), sort_order.end(),
                       [&, cnt = 0]() mutable { return track.template get<ut_idx>( cnt++ ).cast(); } );

        // sort the indices according to the hit z-coordinate
        std::sort( sort_order.begin(), sort_order.end(), [&]( int const i, int const j ) {
          return m_fit_upstream.value() ? hits_ut.hits().zAtYEq0<F>( i ).cast() > hits_ut.hits().zAtYEq0<F>( j ).cast()
                                        : hits_ut.hits().zAtYEq0<F>( i ).cast() < hits_ut.hits().zAtYEq0<F>( j ).cast();
        } );

        for ( auto const idx : sort_order ) {

          auto x0   = hits_ut.hits().xAtYEq0<F>( idx );
          auto z0   = hits_ut.hits().zAtYEq0<F>( idx );
          auto w    = hits_ut.hits().weight<F>( idx );
          auto dxdy = hits_ut.hits().dxDy<F>( idx );

          // in PR weight = 12.0 / ( pitch * pitch );
          // Measurement Provider error: pitch * sqrt( eValue * eValue + m_APE * m_APE );
          // evalue is .22, .14, or .25 based on pseudosize.
          // ???APE is currently set to zero but is a property that could change?
          fitnodes.emplace_back( x0.cast(), 0, z0.cast(), dxdy.cast(), 1, 0, ( 1. / sqrt( w ) ).cast(),
                                 FitNode::Type::UTHitOnTrack );
        }
      };
      auto const add_ft_nodes = [&]() {
        using nFTHits =
            std::conditional_t<input_is_long_tracks, LHCb::Pr::Long::Tag::nFTHits, LHCb::Pr::Downstream::Tag::nFTHits>;
        using ft_idx = std::conditional_t<input_is_long_tracks, LHCb::Pr::Long::Tag::ft_indices,
                                          LHCb::Pr::Downstream::Tag::ft_indices>;

        auto const n_hits = track.template get<nFTHits>().cast();
        sort_order.resize( n_hits );

        // get the indices into the hit container for all our ut hits
        std::generate( sort_order.begin(), sort_order.end(),
                       [&, cnt = 0]() mutable { return track.template get<ft_idx>( cnt++ ).cast(); } );

        // sort the indices according to the hit z-coordinate
        std::sort( sort_order.begin(), sort_order.end(), [&]( int const i, int const j ) {
          return m_fit_upstream.value() ? hits_ft._z0[i] > hits_ft._z0[j] : hits_ft._z0[i] < hits_ft._z0[j];
        } );

        for ( auto const idx : sort_order ) {
          // X and Z are defined at y=0
          auto x    = hits_ft._x[idx];
          auto z    = hits_ft._z0[idx];
          auto w    = hits_ft._w[idx];
          auto dxdy = hits_ft._dxDy[idx];
          auto dzdy = hits_ft._dzDy[idx];

          // FIXME Pr gets weight from an array via clustersize
          // MeasurementProvider does 0.04 + 0.01 * itH->pseudoSize()
          // we don't know the pseudosize anymore at this stage though :(
          fitnodes.emplace_back( x, 0, z, dxdy, 1, dzdy, ( .5 / std::sqrt( w ) ), FitNode::Type::FTHitOnTrack );
        }
      };

      if ( m_fit_upstream.value() ) {
        add_ft_nodes();
        add_ut_nodes();
        if constexpr ( input_is_long_tracks ) add_vp_nodes( track );
      } else {
        if constexpr ( input_is_long_tracks ) add_vp_nodes( track );
        add_ut_nodes();
        add_ft_nodes();
      }

      // set some limits for the momentum used for scattering
      using QoverP =
          std::conditional_t<input_is_long_tracks, LHCb::Pr::Long::Tag::StateQoP, LHCb::Pr::Downstream::Tag::StateQoP>;
      auto scatteringMomentum = std::clamp( std::abs( 1. / track.template get<QoverP>().cast() ), 100., 500000. );

      if ( UNLIKELY( init_nodes( fitnodes, scatteringMomentum, ctb_state_vec, scifi_state_vec ).isFailure() ) ) {
        continue;
      }

      for ( auto& node : fitnodes ) { node.project_reference(); }

      KF::pre_fit_init( fitnodes, m_errorX * m_errorX, m_errorY * m_errorY, m_errorTx * m_errorTx,
                        m_errorTy * m_errorTy, m_errorQoP * m_errorQoP );

      bool has_converged{false};
      // first fit execution
      LHCb::ChiSquare prev_chi2 = KF::fit( fitnodes );
      auto const      tolerance = prev_chi2.nDoF() * 0.01;
      // iterate until max iterations or chi2 change below tolerance
      int  iter{2};
      bool transport_failure{false};

      for ( ; iter <= m_max_fit_iter.value() && !has_converged; ++iter ) {
        mydebug( "Iteration:", iter );
        KF::smooth_and_update_ref_vector( fitnodes );
        for ( auto& node : fitnodes ) node.project_reference();
        if ( UNLIKELY( update_transport_matrix( fitnodes ).isFailure() ) ) {
          transport_failure = true;
          ++transport_failed_buffer;
          break;
        }
        auto const chi2  = KF::fit( fitnodes );
        auto const dchi2 = prev_chi2.chi2() - chi2.chi2();
        prev_chi2        = chi2;
        has_converged    = std::abs( dchi2 ) < tolerance && iter > 1;
      }
      iter_buffer += ( iter - 1 );
      if ( UNLIKELY( transport_failure ) ) { continue; };

      //
      //
      // OUTLIER REMOVAL
      // FIXME this is currently more of an ad-hoc implementation
      // Check if it's faster to first check based on each nodes delta_chi2
      // similar to what is done in TMF.
      // Also given that I now need to smooth preety much every time I also fit
      // it should be checked if It's faster to fuse the backwards and smoothing
      // operations
      //
      //
      //
      if ( prev_chi2.chi2PerDoF() >= m_maxchi2perdof_pre_outlier ) {
        ++pre_outlier_chi2_cut_buffer;
        continue;
      }

      std::array<int, 3>                              num_hits{0, 0, 0};
      std::array<int, 3>                              min_hits{3, 3, 6};
      std::vector<std::pair<FitNode*, FitNode::Type>> type_backup;
      // VP=0, UT=1, FT=2
      for ( auto& node : fitnodes ) ++num_hits[static_cast<int>( node.type() ) - 2];

      int outlier_iter{0};

      for ( ; outlier_iter < m_max_outlier_iter; ++outlier_iter ) {
        KF::smooth_and_update_ref_vector_outlier( fitnodes );

        FitNode* outlier   = nullptr;
        double   worstChi2 = 9;

        for ( auto& node : fitnodes ) {
          if ( UNLIKELY( node.type() == FitNode::Type::Outlier ) ) continue;

          node.project_reference();

          auto type_idx = static_cast<int>( node.type() ) - 2;
          // not allowed to remove this one so skip the chi2 calc
          if ( num_hits[type_idx] <= min_hits[type_idx] ) { continue; }

          double V    = node.measurement_error * node.measurement_error;
          double HCH  = LHCb::Math::Similarity( node.projection, node.final_state_cov );
          double R    = V - HCH;
          double chi2 = ( node.ref_residual * node.ref_residual ) / R;

          if ( chi2 > worstChi2 ) {
            worstChi2 = chi2;
            outlier   = &node;
          }
        }

        // if I don't find an outlier, we are done.
        if ( !outlier ) break;

        --num_hits[static_cast<int>( outlier->type() ) - 2];
        type_backup.emplace_back( outlier, outlier->type() );
        outlier->m_type = FitNode::Type::Outlier;

        prev_chi2 = KF::fit( fitnodes );
      }
      // I need to restore the types such that the parametrised scatters work
      // which are used in the add_fitted_states function
      for ( auto pair : type_backup ) { pair.first->m_type = pair.second; }
      outlier_iter_buffer += outlier_iter;

      if ( prev_chi2.chi2PerDoF() >= m_maxchi2perdof ) {
        chi2_cut_buffer++;
        continue;
      }

      //
      // start creating our output track
      //
      auto new_track =
          std::make_unique<LHCb::Event::Track>( LHCb::Event::Track::History::PrForward, LHCb::Event::Track::Types::Long,
                                                LHCb::Event::Track::PatRecStatus::PatRecIDs );

      if constexpr ( !input_is_long_tracks ) { new_track->setType( LHCb::Event::Track::Types::Downstream ); }

      // Add LHCbIds
      id_vec.clear();
      if constexpr ( input_is_long_tracks ) {
        namespace tag = LHCb::Pr::Long::Tag;
        auto nhits    = track.template get<tag::nVPHits>().cast() + track.template get<tag::nUTHits>().cast() +
                     track.template get<tag::nFTHits>().cast();
        for ( int i{0}; i < nhits; ++i ) { id_vec.emplace_back( track.template get<tag::lhcbIDs>( i ).cast() ); }
      } else {
        namespace tag = LHCb::Pr::Downstream::Tag;
        auto nhits    = track.template get<tag::nUTHits>().cast() + track.template get<tag::nFTHits>().cast();
        for ( int i{0}; i < nhits; ++i ) { id_vec.emplace_back( track.template get<tag::lhcbIDs>( i ).cast() ); }
      }
      new_track->setLhcbIDs( id_vec );

      new_track->setFitStatus( LHCb::Event::Track::FitStatus::Fitted );
      new_track->setNDoF( prev_chi2.nDoF() );
      new_track->setChi2PerDoF( prev_chi2.chi2() / prev_chi2.nDoF() );

      auto velo_chi2     = LHCb::ChiSquare{0, -5};
      auto down_chi2     = LHCb::ChiSquare{0, -5};
      auto upstream_chi2 = LHCb::ChiSquare{};

      for ( auto const& node : fitnodes ) {
        switch ( node.type() ) {
        case FitNode::Type::VPHitOnTrack:
          velo_chi2 += node.delta_chi2[m_fit_upstream ? FitNode::backward : FitNode::backward];
          break;
        case FitNode::Type::UTHitOnTrack:
          upstream_chi2 += node.delta_chi2[m_fit_upstream ? FitNode::backward : FitNode::backward];
          break;
        case FitNode::Type::FTHitOnTrack:
          down_chi2 += node.delta_chi2[m_fit_upstream ? FitNode::forward : FitNode::backward];
          break;
        default:
          break;
        }
      }
      upstream_chi2 += velo_chi2;

      new_track->addInfo( LHCb::Event::v1::Track::AdditionalInfo::FitTChi2, down_chi2.chi2() );
      new_track->addInfo( LHCb::Event::v1::Track::AdditionalInfo::FitTNDoF, down_chi2.nDoF() );

      if constexpr ( input_is_long_tracks ) {
        new_track->addInfo( LHCb::Event::v1::Track::AdditionalInfo::FitVeloChi2, velo_chi2.chi2() );
        new_track->addInfo( LHCb::Event::v1::Track::AdditionalInfo::FitVeloNDoF, velo_chi2.nDoF() );
        new_track->addInfo( LHCb::Event::v1::Track::AdditionalInfo::FitMatchChi2,
                            prev_chi2.chi2() - upstream_chi2.chi2() - down_chi2.chi2() );
      }

      if ( UNLIKELY(
               add_fitted_states<input_is_long_tracks>( *new_track, fitnodes, scatteringMomentum ).isFailure() ) ) {
        ++states_failed_buffer;
        continue;
      }

      output.add( new_track.release() );

    } // loop over tracks
    m_counter_tracks_out += output.size();
    return output;
  }

private:
  ToolHandle<ITrackExtrapolator> m_extrap{this, "ReferenceExtrapolator", "TrackMasterExtrapolator"};

  Gaudi::Property<int>  m_max_outlier_iter{this, "MaxOutlierIterations", 2, "max number of fit iterations to perform"};
  Gaudi::Property<int>  m_max_fit_iter{this, "MaxFitIterations", 10, "max number of fit iterations to perform"};
  Gaudi::Property<bool> m_fit_upstream{this, "FitUpstream", true, "Fit Nodes in order of decreasing z coordinate"};
  Gaudi::Property<double> m_errorX{this, "ErrorX", 20.0 * Gaudi::Units::mm, "Seed error on x"};
  Gaudi::Property<double> m_errorY{this, "ErrorY", 20.0 * Gaudi::Units::mm, "Seed error on y"};
  Gaudi::Property<double> m_errorTx{this, "ErrorTx", 0.1, "Seed error on slope x"};
  Gaudi::Property<double> m_errorTy{this, "ErrorTy", 0.1, "Seed error on slope y"};
  Gaudi::Property<double> m_errorQoP{this, "ErrorQoP", 0.01, "Seed error on QoP"};
  Gaudi::Property<double> m_maxchi2perdof_pre_outlier{this, "MaxChi2PreOutlierRemoval", 9999999,
                                                      "Maximum Chi2 per DoF before outlier removal"};
  Gaudi::Property<double> m_maxchi2perdof{this, "MaxChi2", 9999999, "Maximum Chi2 per DoF"};

  mutable Gaudi::Accumulators::SummingCounter<unsigned int>   m_counter_tracks_in{this, "nTracksInput"};
  mutable Gaudi::Accumulators::SummingCounter<unsigned int>   m_counter_tracks_out{this, "nTracksOutput"};
  mutable Gaudi::Accumulators::SummingCounter<unsigned int>   m_counter_transport_failed{this, "Transport failed"};
  mutable Gaudi::Accumulators::SummingCounter<unsigned int>   m_counter_states_failed{this, "Add states failed"};
  mutable Gaudi::Accumulators::AveragingCounter<unsigned int> m_counter_outlier_iterations{this, "nOutlierIterations"};
  mutable Gaudi::Accumulators::AveragingCounter<unsigned int> m_counter_iterations{this, "nIterations"};
  mutable Gaudi::Accumulators::Counter<>                      m_counter_pre_outlier_cut{this, "Pre outlier chi2 cut"};
  mutable Gaudi::Accumulators::Counter<>                      m_counter_cut{this, "chi2 cut"};
};

DECLARE_COMPONENT_WITH_ID( KalmanFilter<LHCb::Pr::Long::Tracks>, "PrKalmanFilter" )
DECLARE_COMPONENT_WITH_ID( KalmanFilter<LHCb::Pr::Downstream::Tracks>, "PrKalmanFilter_Downstream" )
