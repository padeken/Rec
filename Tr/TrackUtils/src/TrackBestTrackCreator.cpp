/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/ChiSquare.h"
#include "Event/KalmanFitResult.h"
#include "Event/Track.h"
#include "Event/TrackFunctor.h"
#include "Gaudi/Accumulators.h"
#include "GaudiAlg/MergingTransformer.h"
#include "GaudiKernel/ToolHandle.h"
#include "Kernel/HitPattern.h"
#include "Kernel/LHCbID.h"
#include "Kernel/STLExtensions.h"
#include "TrackInterfaces/IGhostProbability.h"
#include "TrackInterfaces/ITrackCloneFinder.h"
#include "TrackInterfaces/ITrackFitter.h"
#include "TrackKernel/TrackCloneData.h"
#include "TrackKernel/TrackFunctors.h"
#include "range/v3/version.hpp"
#include <algorithm>
#include <array>
#include <cassert>
#include <cstddef>
#include <exception>
#include <functional>
#include <memory>
#include <type_traits>
#include <unordered_map>
#if DEBUGHISTOGRAMS
#  include "GaudiAlg/GaudiHistoAlg.h"
using TrackBestTrackCreatorBase = GaudiHistoAlg;
#else
using TrackBestTrackCreatorBase = GaudiAlgorithm;
#endif

namespace {

  /// structure to save some data for each track
  class TrackData : public LHCb::TrackCloneData<> {
  private:
    bool                   m_isAccepted{false};
    double                 m_qOverP{0};
    LHCb::Track::FitStatus m_fitStatus{LHCb::Track::FitStatus::FitStatusUnknown};
    enum { Clone = 1 };

  public:
    /// constructor
    TrackData( std::unique_ptr<LHCb::Track> tr )
        : TrackCloneData<>( std::move( tr ) )
        , m_qOverP( track().firstState().qOverP() )
        , m_fitStatus( track().fitStatus() ) {}
    /// return q/p (or what it was at construction time)
    double                 qOverP() const { return m_qOverP; }
    LHCb::Track::FitStatus previousStatus() const { return m_fitStatus; }
    bool                   cloneFlag() const { return userFlags() & Clone; }
    void                   setCloneFlag() { setUserFlags( userFlags() | Clone ); }
    bool                   isAccepted() const { return m_isAccepted; }
    void                   setAccepted( const bool isAccepted ) { m_isAccepted = isAccepted; }

    std::vector<std::reference_wrapper<TrackData>> clones;
  };

} // namespace

/** @brief kill clones among tracks in input container and Kalman-fit survivors
 *
 * @author Wouter Hulsbergen
 * - initial release
 *
 * @author Manuel Schiller
 * @date 2014-11-18
 * - simplify, C++11 cleanups, use BloomFilter based TrackCloneData
 * @date 2014-12-09
 * - add code to use ancestor information on tracks to deal with obvious clones
 */
class TrackBestTrackCreator final
    : public Gaudi::Functional::MergingTransformer<LHCb::Tracks(
                                                       const Gaudi::Functional::vector_of_const_<LHCb::Tracks>& ),
                                                   Gaudi::Functional::Traits::BaseClass_t<TrackBestTrackCreatorBase>> {
public:
  /// Standard constructor
  TrackBestTrackCreator( const std::string& name, ISvcLocator* pSvcLocator );

  virtual StatusCode initialize() override; ///< Algorithm initialization
  LHCb::Tracks       operator()( const Gaudi::Functional::vector_of_const_<LHCb::Tracks>& ranges ) const override;

  template <typename CounterType>
  class CounterSet {
  public:
    template <typename OWNER>
    CounterSet( OWNER* owner, const std::string& tag ) {
      m_counters.reserve( LHCb::Track::UT * 2 + 2 );
      // setup counters for each type of forward track
      for ( int type = 0; type < LHCb::Track::UT + 1; type++ ) {
        m_counters.emplace_back( owner, Gaudi::Utils::toString( (LHCb::Track::Types)type ) + "." + tag );
      }
      // setup counters for each type of backward track
      for ( int type = 0; type < LHCb::Track::UT + 1; type++ ) {
        m_counters.emplace_back( owner, Gaudi::Utils::toString( (LHCb::Track::Types)type ) + "Backward." + tag );
      }
    }
    CounterType& operator()( bool isBackward, LHCb::Track::Types type ) {
      return m_counters[type + ( isBackward ? LHCb::Track::UT + 1 : 0 )];
    }

  private:
    // the actual counters
    std::vector<CounterType> m_counters;
  };

private:
  ToolHandle<ITrackFitter>      m_fitter{this, "Fitter", "TrackMasterFitter"};
  ToolHandle<IGhostProbability> m_ghostTool{this, "GhostIdTool", "UpgradeGhostId"};

  Gaudi::Property<double>   m_maxOverlapFracVelo{this, "MaxOverlapFracVelo", 0.5};
  Gaudi::Property<double>   m_maxOverlapFracT{this, "MaxOverlapFracT", 0.5};
  Gaudi::Property<double>   m_maxOverlapFracTT{this, "MaxOverlapFracTT", 0.35, "essentially: max 1 common hit"};
  Gaudi::Property<double>   m_minLongLongDeltaQoP{this, "MinLongLongDeltaQoP", -1};
  Gaudi::Property<double>   m_minLongDownstreamDeltaQoP{this, "MinLongDownstreamDeltaQoP", 5e-6};
  Gaudi::Property<double>   m_maxChi2DoF{this, "MaxChi2DoF", 3};
  Gaudi::Property<double>   m_maxChi2DoFVelo{this, "MaxChi2DoFVelo", 999};
  Gaudi::Property<double>   m_maxChi2DoFT{this, "MaxChi2DoFT", 999};
  Gaudi::Property<double>   m_maxChi2DoFMatchAndTT{this, "MaxChi2DoFMatchTT", 999};
  Gaudi::Property<double>   m_maxGhostProb{this, "MaxGhostProb", 99999};
  Gaudi::Property<bool>     m_fitTracks{this, "FitTracks", true, "fit the tracks using the Kalman filter"};
  Gaudi::Property<bool>     m_addGhostProb{this, "AddGhostProb", false, "Add the Ghost Probability to a track"};
  Gaudi::Property<bool>     m_useAncestorInfo{this, "UseAncestorInfo", true,
                                          "use ancestor information to identify obvious clones"};
  Gaudi::Property<bool>     m_doNotRefit{this, "DoNotRefit", false, "Do not refit already fitted tracks"};
  Gaudi::Property<unsigned> m_batchSize{this, "BatchSize", 50, "size of batches (if processing in batches)"};

  mutable Gaudi::Accumulators::Counter<>         m_fittedBeforeCounter{this, "FittedBefore"};
  mutable Gaudi::Accumulators::Counter<>         m_fitFailedBeforeCounter{this, "FitFailedBefore"};
  mutable Gaudi::Accumulators::BinomialCounter<> m_badInputCounter{this, "BadInput"};
  mutable Gaudi::Accumulators::BinomialCounter<> m_fitFailedCounter{this, "FitFailed"};

  mutable CounterSet<Gaudi::Accumulators::AveragingCounter<double>>       m_chisqProbSumCounters{this, "chisqProbSum"};
  mutable CounterSet<Gaudi::Accumulators::AveragingCounter<double>>       m_ghostProbCounters{this, "ghostProbability"};
  mutable CounterSet<Gaudi::Accumulators::AveragingCounter<unsigned int>> m_numOutliersCounters{this, "numOutliers"};
  mutable CounterSet<Gaudi::Accumulators::AveragingCounter<unsigned int>> m_fitFailedCounters{this, "FitFailed"};
  mutable CounterSet<Gaudi::Accumulators::BinomialCounter<>>              m_badChisqCounters{this, "badChisq"};
  mutable CounterSet<Gaudi::Accumulators::BinomialCounter<>>              m_flipChargeCounters{this, "flipCharge"};

  /// Process sequentially or in batches
  bool m_processInBatches{};

protected:
  /// are tracks clones in VeloR and VeloPhi
  bool veloClones( const TrackData&, const TrackData& ) const;
  /// are tracks clones in VeloR or VeloPhi
  bool veloOrClones( const TrackData&, const TrackData& ) const;
  /// are tracks clones in T
  bool TClones( const TrackData&, const TrackData& ) const;
  /// are tracks clones in TT
  bool TTClones( const TrackData&, const TrackData& ) const;

  int fitAndSelect( const std::vector<std::reference_wrapper<TrackData>>::iterator& tracksBegin,
                    const std::vector<std::reference_wrapper<TrackData>>::iterator& tracksEnd ) const;

  void fitAndUpdateCounters( const std::vector<std::reference_wrapper<TrackData>>::iterator& tracksBegin,
                             const std::vector<std::reference_wrapper<TrackData>>::iterator& tracksEnd ) const;

  /// check if tracks pointed to by their TrackData objects are clones
  bool areClones( const TrackData& it, const TrackData& jt ) const;

  /// mapping between original track and the index of its copy
  using CopyMapEntry = std::pair<const LHCb::Track*, size_t>;
  using CopyMap      = std::vector<CopyMapEntry>;
};

DECLARE_COMPONENT( TrackBestTrackCreator )

/// hack to allow for tools with non-const interfaces...
template <typename IFace>
IFace* fixup( const ToolHandle<IFace>& iface ) {
  return &const_cast<IFace&>( *iface );
}

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
TrackBestTrackCreator::TrackBestTrackCreator( const std::string& name, ISvcLocator* pSvcLocator )
    : MergingTransformer( name, pSvcLocator,
                          // NOTE that the algorithm behaviour is weakly dependent on the ordering of the input
                          // containers, because if the track 'quality' is the same the ordering comes from the input
                          // container ordering. The quality is related to the track type and number of LHCbIDs. See
                          // LHCBPS-1757 for more details. One should be careful to set this order deterministically.
                          // NOTE that optimising the order may improve algorithm performance -- to be checked
                          {"TracksInContainers",
                           {LHCb::TrackLocation::Forward, LHCb::TrackLocation::Match, LHCb::TrackLocation::VeloTT,
                            LHCb::TrackLocation::Downstream, LHCb::TrackLocation::Tsa, LHCb::TrackLocation::Velo}},
                          {"TracksOutContainer", LHCb::TrackLocation::Default} ) {}

//=============================================================================
// Initialization
//=============================================================================
StatusCode TrackBestTrackCreator::initialize() {
  return MergingTransformer::initialize().andThen( [&] {
    if ( !m_fitTracks ) m_fitter.disable();
    if ( !m_addGhostProb ) m_ghostTool.disable();

    // Process in batches in case we are not using MasterFitter
    std::string prefix( "TrackMasterFitter" );
    m_processInBatches = m_fitter.typeAndName().compare( 0, prefix.size(), prefix );

    // Print out the user-defined settings
    if ( msgLevel( MSG::DEBUG ) )
      debug() << endmsg << "============ TrackBestTrackCreator Settings ===========" << endmsg
              << "TracksInContainers : " << getProperty( "TracksInContainers" ).toString() << endmsg
              << "TrackOutContainer  : " << getProperty( "TracksOutContainer" ).toString() << endmsg
              << "=======================================================" << endmsg << endmsg;
  } );
}

//=============================================================================
// Main execution
//=============================================================================
LHCb::Tracks TrackBestTrackCreator::
             operator()( const Gaudi::Functional::vector_of_const_<LHCb::Tracks>& ranges ) const {
  if ( msgLevel( MSG::DEBUG ) ) debug() << "==> Execute" << endmsg;
  if ( m_addGhostProb.value() ) {
    //@FIXME: make IGhostTool interface stateless
    fixup( m_ghostTool )->beginEvent().ignore();
  }
  int nGhosts = 0;

  // create pool for TrackData objects for all input tracks
  std::vector<TrackData> trackdatapool;
  // keep a record of which track goes where
  CopyMap copymap;
  // get total number of tracks
  size_t nTracks = std::accumulate( std::begin( ranges ), std::end( ranges ), size_t( 0 ),
                                    []( size_t sz, const LHCb::Tracks& range ) { return sz + range.size(); } );
  // reserve enough space so we don't have to reallocate
  trackdatapool.reserve( nTracks );
  copymap.reserve( nTracks );

  // generate the TrackData objects for the input tracks, initialising the
  // States for use in the Kalman filter on the way
  nTracks = 0;
  for ( auto& range : ranges ) {
    for ( auto& oldtr : range ) {
      // clone track
      std::unique_ptr<LHCb::Track> tr{new LHCb::Track()};
      tr->copy( *oldtr );

      // pre-initialise (if required)
      const bool fitted = m_doNotRefit.value() && ( tr->fitStatus() == LHCb::Track::FitStatus::Fitted ||
                                                    tr->fitStatus() == LHCb::Track::FitStatus::FitFailed );
      if ( fitted && tr->fitStatus() == LHCb::Track::FitStatus::FitFailed ) continue;
      if ( m_useAncestorInfo.value() ) {
        // save mapping between original track and its copy
        copymap.emplace_back( oldtr, nTracks );
      }
      ++nTracks;
      // keep a record where this track came from
      tr->addToAncestors( oldtr );
      trackdatapool.emplace_back( std::move( tr ) );
    }
  }

  // take a vector of "references" which is much easier to sort (because less
  // data is moved around)
  std::vector<std::reference_wrapper<TrackData>> alltracks( trackdatapool.begin(), trackdatapool.end() );

  // sort them by quality
  auto qualitySort = []( const TrackData& t1, const TrackData& t2 ) { return t1 < t2; };
  std::stable_sort( alltracks.begin(), alltracks.end(), qualitySort );

  // Prepare TrackData reference containers
  std::vector<std::reference_wrapper<TrackData>> successful_tracks;
  // Helper function to verify if t is a clone

  // Conditions for being a clone:
  // * cloneFlag is true
  // * It's a clone of any track in v
  auto isClone = [&]( TrackData& t, const std::vector<std::reference_wrapper<TrackData>>& v ) {
    const auto firstClone =
        std::find_if( v.begin(), v.end(), [&]( const TrackData& t2 ) { return areClones( t, t2 ); } );
    if ( firstClone != v.end() ) return true;
    return false;
  };

  if ( !m_processInBatches ) {
    // Sequential treatment
    std::for_each( alltracks.begin(), alltracks.end(), [&]( TrackData& t ) {
      if ( !t.cloneFlag() && !isClone( t, successful_tracks ) ) {
        std::vector<std::reference_wrapper<TrackData>> to_process{t};
        nGhosts += fitAndSelect( to_process.begin(), to_process.end() );
        if ( t.isAccepted() ) { successful_tracks.emplace_back( t ); }
      }
    } );
  } else {
    // isClone function with added functionality
    auto isCloneExtended = [&]( TrackData& t, const std::vector<std::reference_wrapper<TrackData>>& v ) {
      const auto firstClone =
          std::find_if( v.begin(), v.end(), [&]( const TrackData& t2 ) { return areClones( t, t2 ); } );
      if ( firstClone != v.end() ) {
        // Add the clone found to the list of clones of that track
        TrackData& foundT = *firstClone;
        foundT.clones.emplace_back( t );

        return true;
      }
      return false;
    };

    std::vector<std::reference_wrapper<TrackData>> to_process;
    std::vector<std::reference_wrapper<TrackData>> unsuccessful_tracks;

    for ( auto chunk : LHCb::range::chunk{alltracks, m_batchSize.value()} ) {
      // Fill to_process
      std::for_each( chunk.begin(), chunk.end(), [&]( TrackData& t ) {
        if ( !t.cloneFlag() && !isClone( t, successful_tracks ) && !isCloneExtended( t, to_process ) ) {
          to_process.emplace_back( t );
        }
      } );

      // Perform fit
      fitAndSelect( to_process.begin(), to_process.end() );

      // Move to successful
      std::partition_copy( to_process.begin(), to_process.end(), std::back_inserter( successful_tracks ),
                           std::back_inserter( unsuccessful_tracks ),
                           []( const TrackData& t ) { return t.isAccepted(); } );

      // Create new to_process batch
      to_process.clear();
      std::for_each( unsuccessful_tracks.begin(), unsuccessful_tracks.end(), [&]( TrackData& t ) {
        std::copy_if( t.clones.begin(), t.clones.end(), std::back_inserter( to_process ), [&]( TrackData& c ) {
          return !isClone( c, successful_tracks ) && !isCloneExtended( c, to_process );
        } );
      } );

      unsuccessful_tracks.clear();
    }

    // Remaining tracks
    while ( !to_process.empty() ) {
      // Perform fit
      fitAndSelect( to_process.begin(), to_process.end() );

      // Move to successful
      std::partition_copy( to_process.begin(), to_process.end(), std::back_inserter( successful_tracks ),
                           std::back_inserter( unsuccessful_tracks ),
                           []( const TrackData& t ) { return t.isAccepted(); } );

      // Create new to_process batch
      to_process.clear();
      std::for_each( unsuccessful_tracks.begin(), unsuccessful_tracks.end(), [&]( TrackData& t ) {
        std::copy_if( t.clones.begin(), t.clones.end(), std::back_inserter( to_process ), [&]( TrackData& c ) {
          return !isClone( c, successful_tracks ) && !isCloneExtended( c, to_process );
        } );
      } );

      unsuccessful_tracks.clear();
    }
  }

  // create output container, and put selected tracks there
  LHCb::Tracks tracksOutCont;
  // insert selected tracks
  tracksOutCont.reserve( successful_tracks.size() );
  for ( TrackData& tr : successful_tracks ) {
    // make tr release ownership of track
    tracksOutCont.add( std::move( tr ).trackptr().release() );
  }

  if ( msgLevel( MSG::DEBUG ) ) {
    debug() << "Selected " << successful_tracks.size() << " out of " << nTracks << " tracks. Rejected " << nGhosts
            << endmsg;
  }

  return tracksOutCont;
}

int TrackBestTrackCreator::fitAndSelect(
    const std::vector<std::reference_wrapper<TrackData>>::iterator& tracksBegin,
    const std::vector<std::reference_wrapper<TrackData>>::iterator& tracksEnd ) const {

  int nGhosts = 0;
  fitAndUpdateCounters( tracksBegin, tracksEnd );

  unsigned i = 0;
  std::for_each( tracksBegin, tracksEnd, [&]( TrackData& trackData ) {
    LHCb::Track& track = trackData.track();
    trackData.setAccepted( track.fitStatus() == LHCb::Track::FitStatus::Fitted );

    // Impose tighter restrictions to composite tracks
    if ( trackData.isAccepted() &&
         ( track.type() == LHCb::Track::Types::Long || track.type() == LHCb::Track::Types::Upstream ||
           track.type() == LHCb::Track::Types::Downstream ) ) {

      const LHCb::ChiSquare& chi2 = {track.chi2(), track.nDoF()};

      // compatibility for tracks fitted with simple_fitter
      // since they don't have a fitresult
      auto*                  fit   = fitResult( track );
      const LHCb::ChiSquare& chi2T = [fit, &track]() {
        return fit ? fit->chi2Downstream()
                   : LHCb::ChiSquare{track.info( LHCb::Track::AdditionalInfo::FitTChi2, 0 ),
                                     static_cast<int>( track.info( LHCb::Track::AdditionalInfo::FitTNDoF, 0 ) )};
      }();
      const LHCb::ChiSquare& chi2Velo = [fit, &track]() {
        return fit ? fit->chi2Velo()
                   : LHCb::ChiSquare{track.info( LHCb::Track::AdditionalInfo::FitVeloChi2, 0 ),
                                     static_cast<int>( track.info( LHCb::Track::AdditionalInfo::FitVeloNDoF, 0 ) )};
      }();

      // note: this includes TT hit contribution
      const LHCb::ChiSquare chi2MatchAndTT = chi2 - chi2T - chi2Velo;

      if ( msgLevel( MSG::DEBUG ) ) {
        debug() << "Track #" << i << " " << chi2.chi2() << " " << chi2.nDoF() << " ("
                << ( chi2.chi2PerDoF() < m_maxChi2DoF ? "1" : "0" ) << "), " << chi2T.chi2() << " " << chi2T.nDoF()
                << " (" << ( chi2T.chi2PerDoF() < m_maxChi2DoFT ? "1" : "0" ) << "), " << chi2Velo.chi2() << " "
                << chi2Velo.nDoF() << " (" << ( chi2Velo.chi2PerDoF() < m_maxChi2DoFVelo ? "1" : "0" ) << "), "
                << chi2MatchAndTT.chi2() << " " << chi2MatchAndTT.nDoF() << " ("
                << ( chi2MatchAndTT.chi2PerDoF() < m_maxChi2DoFMatchAndTT ? "1" : "0" ) << "), "
                << track.ghostProbability() << " (" << ( track.ghostProbability() < m_maxGhostProb ? "1" : "0" ) << ") "
                << endmsg;
      }

      trackData.setAccepted( chi2.chi2PerDoF() < m_maxChi2DoF && chi2T.chi2PerDoF() < m_maxChi2DoFT &&
                             chi2Velo.chi2PerDoF() < m_maxChi2DoFVelo &&
                             chi2MatchAndTT.chi2PerDoF() < m_maxChi2DoFMatchAndTT &&
                             ( !m_addGhostProb || track.ghostProbability() < m_maxGhostProb ) );

      if ( !trackData.isAccepted() ) { ++nGhosts; }
    }

    if ( msgLevel( MSG::DEBUG ) ) {
      debug() << "Track #" << i++ << ( trackData.isAccepted() ? " accepted" : " not accepted" ) << endmsg;
    }
  } );
  return nGhosts;
}

/**
 * @brief      Invokes the fitter and sets some counters.
 */
void TrackBestTrackCreator::fitAndUpdateCounters(
    const std::vector<std::reference_wrapper<TrackData>>::iterator& tracksBegin,
    const std::vector<std::reference_wrapper<TrackData>>::iterator& tracksEnd ) const {

  if ( m_fitTracks.value() ) {
    std::vector<std::reference_wrapper<LHCb::Track>> fittingTracks;
    std::for_each( tracksBegin, tracksEnd, [&]( TrackData& trackData ) {
      LHCb::Track& track = trackData.track();

      // Conditions for fitting a track
      if ( ( !m_doNotRefit || ( track.fitStatus() != LHCb::Track::FitStatus::Fitted &&
                                track.fitStatus() != LHCb::Track::FitStatus::FitFailed ) ) &&
           ( track.nStates() != 0 && !track.checkFlag( LHCb::Track::Flags::Invalid ) ) ) {
        fittingTracks.emplace_back( track );
      }
    } );
    m_fitter->operator()( fittingTracks ).ignore();
  }

  // Sequential treatment
  std::for_each( tracksBegin, tracksEnd, [&]( TrackData& trackData ) {
    LHCb::Track& track = trackData.track();

    bool badinput   = false;
    bool fitfailed  = false;
    bool isBackward = track.checkFlag( LHCb::Track::Flags::Backward );

    if ( m_doNotRefit && ( trackData.previousStatus() == LHCb::Track::FitStatus::Fitted ||
                           trackData.previousStatus() == LHCb::Track::FitStatus::FitFailed ) ) {
      if ( trackData.previousStatus() == LHCb::Track::FitStatus::Fitted ) {
        ++m_fittedBeforeCounter;
        if ( m_addGhostProb ) m_ghostTool->execute( track ).ignore();
      } else {
        /// fit failed before
        /// This should always be 0 as this type is filtered out when initializing the tracks
        ++m_fitFailedBeforeCounter;
        track.setFlag( LHCb::Track::Flags::Invalid, true );
      }
    } else {
      if ( track.nStates() == 0 || track.checkFlag( LHCb::Track::Flags::Invalid ) ) {
        track.setFlag( LHCb::Track::Flags::Invalid, true );
        badinput = true;
      } else {
        // Note: The track has already been fitted
        if ( track.fitStatus() == LHCb::Track::FitStatus::Fitted ) {
          if ( m_addGhostProb ) m_ghostTool->execute( track ).ignore();
          // Update counters
          if ( track.nDoF() > 0 ) {
            double chisqProb = track.probChi2();
            m_chisqProbSumCounters( isBackward, track.type() ) += chisqProb;
            m_badChisqCounters( isBackward, track.type() ) += bool( chisqProb < 0.01 );
          }
          m_flipChargeCounters( isBackward, track.type() ) +=
              bool( trackData.qOverP() * track.firstState().qOverP() < 0 );
          m_numOutliersCounters( isBackward, track.type() ) += nMeasurementsRemoved( track );
          m_ghostProbCounters( isBackward, track.type() ) += track.ghostProbability();
        } else {
          track.setFlag( LHCb::Track::Flags::Invalid, true );
          fitfailed = true;
          m_fitFailedCounters( isBackward, track.type() ) += int( fitfailed );
        }
      }
    }
    m_badInputCounter += badinput;
    m_fitFailedCounter += fitfailed;
  } );
}

bool TrackBestTrackCreator::veloOrClones( const TrackData& lhs, const TrackData& rhs ) const {
  const double fR = lhs.overlapFraction( rhs, TrackData::VeloR );
#ifndef DEBUGHISTOGRAMS
  if ( fR > m_maxOverlapFracVelo ) return true;
#endif
  const double fPhi = lhs.overlapFraction( rhs, TrackData::VeloPhi );
#ifdef DEBUGHISTOGRAMS
  if ( fR > 0 ) plot1D( fR, "veloROverlapFractionH1", 0, 1 );
  if ( fPhi > 0 ) plot1D( fPhi, "veloPhiOverlapFractionH1", 0, 1 );
  return ( fR > m_maxOverlapFracVelo ) || ( fPhi > m_maxOverlapFracVelo );
#else
  return fPhi > m_maxOverlapFracVelo;
#endif
}

bool TrackBestTrackCreator::TClones( const TrackData& lhs, const TrackData& rhs ) const {
  const double f = lhs.overlapFraction( rhs, TrackData::T );
#ifdef DEBUGHISTOGRAMS
  if ( f > 0 ) plot1D( f, "TOverlapFractionH1", 0, 1 );
#endif
  return f > m_maxOverlapFracT;
}

bool TrackBestTrackCreator::TTClones( const TrackData& lhs, const TrackData& rhs ) const {
  const double f = lhs.overlapFraction( rhs, TrackData::TT );
#ifdef DEBUGHISTOGRAMS
  if ( f > 0 ) plot1D( f, "TTOverlapFractionH1", 0, 1 );
#endif
  return f > m_maxOverlapFracTT;
}

bool TrackBestTrackCreator::areClones( const TrackData& it, const TrackData& jt ) const {
  const LHCb::Track &itrack( it.track() ), &jtrack( jt.track() );
  const int          itype( itrack.type() ), jtype( jtrack.type() );
  const double       dqop   = it.qOverP() - jt.qOverP();
  const int          offset = 256;
  switch ( itype + offset * jtype ) {
  case LHCb::Track::Types::Long + offset* LHCb::Track::Types::Long:
#ifdef DEBUGHISTOGRAMS
    if ( TClones( it, jt ) && veloOrClones( it, jt ) ) {
      plot( dqop, "LLDqopClones", -1e-5, 1e-5 );
    } else if ( TClones( it, jt ) ) {
      plot( dqop, "LLDqopTClones", -1e-5, 1e-5 );
    } else if ( veloOrClones( it, jt ) ) {
      plot( dqop, "LLDqopVeloOrClones", -1e-5, 1e-5 );
    }
#endif
    return TClones( it, jt ) && ( std::abs( dqop ) < m_minLongLongDeltaQoP || veloOrClones( it, jt ) );
  case LHCb::Track::Types::Long + offset*       LHCb::Track::Types::Downstream:
  case LHCb::Track::Types::Downstream + offset* LHCb::Track::Types::Long:
#ifdef DEBUGHISTOGRAMS
    if ( TClones( it, jt ) ) {
      plot( dqop, "DLDqop", -2e-5, 2e-5 );
      if ( TTClones( it, jt ) ) plot( dqop, "DLDqopTTClones", -2e-5, 2e-5 );
    }
#endif
    return TClones( it, jt ) && ( std::abs( dqop ) < m_minLongDownstreamDeltaQoP || TTClones( it, jt ) );
  case LHCb::Track::Types::Downstream + offset* LHCb::Track::Types::Downstream:
    // it seems that there are no down stream tracks that share T hits ...
#ifdef DEBUGHISTOGRAMS
    if ( TClones( it, jt ) ) { plot( dqop, "DDDqop", -1e-4, 1e-4 ); }
#endif
    return TClones( it, jt ) && TTClones( it, jt );
  case LHCb::Track::Types::Long + offset*     LHCb::Track::Types::Upstream:
  case LHCb::Track::Types::Upstream + offset* LHCb::Track::Types::Long:
  case LHCb::Track::Types::Upstream + offset* LHCb::Track::Types::Upstream:
    return veloOrClones( it, jt ) && TTClones( it, jt );
  case LHCb::Track::Types::Long + offset*     LHCb::Track::Types::Velo:
  case LHCb::Track::Types::Velo + offset*     LHCb::Track::Types::Long:
  case LHCb::Track::Types::Upstream + offset* LHCb::Track::Types::Velo:
  case LHCb::Track::Types::Velo + offset*     LHCb::Track::Types::Upstream:
  case LHCb::Track::Types::Velo + offset*     LHCb::Track::Types::Velo:
    return veloOrClones( it, jt );
  case LHCb::Track::Types::Long + offset*       LHCb::Track::Types::Ttrack:
  case LHCb::Track::Types::Ttrack + offset*     LHCb::Track::Types::Long:
  case LHCb::Track::Types::Downstream + offset* LHCb::Track::Types::Ttrack:
  case LHCb::Track::Types::Ttrack + offset*     LHCb::Track::Types::Downstream:
  case LHCb::Track::Types::Ttrack + offset*     LHCb::Track::Types::Ttrack:
    return TClones( it, jt );
  case LHCb::Track::Types::Ttrack + offset*     LHCb::Track::Types::Upstream:
  case LHCb::Track::Types::Upstream + offset*   LHCb::Track::Types::Ttrack:
  case LHCb::Track::Types::Ttrack + offset*     LHCb::Track::Types::Velo:
  case LHCb::Track::Types::Velo + offset*       LHCb::Track::Types::Ttrack:
  case LHCb::Track::Types::Downstream + offset* LHCb::Track::Types::Velo:
  case LHCb::Track::Types::Velo + offset*       LHCb::Track::Types::Downstream:
  case LHCb::Track::Types::Downstream + offset* LHCb::Track::Types::Upstream:
  case LHCb::Track::Types::Upstream + offset*   LHCb::Track::Types::Downstream:
    break;
  default:
    error() << "Don't know how to handle combi: " << itype << " " << jtype << endmsg;
  }
  return false;
}
