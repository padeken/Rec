/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/TrackWithMuonPIDSkin.h"
#include "GaudiAlg/Transformer.h"
#include "SOAContainer/SOAContainer.h"
#include "SOAExtensions/ZipContainer.h"
#include "SOAExtensions/ZipSelection.h"
#include <vector>

namespace {
  template <typename I1, typename I2, typename O>
  using MakeViewBase = Gaudi::Functional::Transformer<O( I1 const&, I2 const& )>;
}

template <typename I1, typename I2, typename O>
struct MakeView final : public MakeViewBase<I1, I2, O> {
  using KeyValue = typename MakeViewBase<I1, I2, O>::KeyValue;
  MakeView( const std::string& name, ISvcLocator* pSvcLocator )
      : MakeViewBase<I1, I2, O>( name, pSvcLocator, {KeyValue{"InputTracks", ""}, KeyValue{"InputMuonIDs", ""}},
                                 KeyValue{"Output", ""} ) {}

  O operator()( I1 const& input1, I2 const& input2 ) const override {
    return Zipping::semantic_zip<LHCb::Event::v2::TrackWithMuonID>( input1, input2 );
  }
};

using TrackContainer  = Zipping::ZipContainer<SOA::contiguous_const_view_from_skin_t<LHCb::Event::v2::TrackSkin>>;
using MuonIDContainer = Zipping::ZipContainer<SOA::Container<std::vector, LHCb::Event::v2::MuonID>>;
using TrackMuonIDView = Zipping::ZipContainer<SOA::contiguous_const_view_from_skin_t<LHCb::Event::v2::TrackWithMuonID>>;
using MakeView__Track_v2__MuonID = MakeView<TrackContainer, MuonIDContainer, TrackMuonIDView>;
DECLARE_COMPONENT_WITH_ID( MakeView__Track_v2__MuonID, "MakeView__Track_v2__MuonID" )
