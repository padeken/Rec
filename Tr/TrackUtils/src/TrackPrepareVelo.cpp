/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

/** @class TrackPrepareVelo TrackPrepareVelo.h
 *
 *  Take a container of tracks and set q/p so that pT=400 MeV and the charge
 *  set to a random but repeatable value.
 *
 */

#include "Event/State.h"
#include "Event/Track.h"
#include "GaudiAlg/GaudiAlgorithm.h"
#include "GaudiKernel/SystemOfUnits.h"
#include <string>

using namespace LHCb;
using namespace Gaudi::Units;
using namespace Gaudi;

class TrackPrepareVelo : public GaudiAlgorithm {

public:
  // Constructors and destructor
  using GaudiAlgorithm::GaudiAlgorithm;

  StatusCode execute() override;

private:
  /// see if the track was used as an ancestor to one in bestCont
  bool used( const LHCb::Track& aTrack, const LHCb::Tracks* bestCont ) const;
  /// set the states on the track with the required q/p
  void prepare( LHCb::Track& aTrack, const int i ) const;

  DataObjectReadHandle<Tracks>  m_inputLocation{this, "inputLocation", TrackLocation::Velo}; ///< input TES location
  DataObjectWriteHandle<Tracks> m_outputLocation{this, "outputLocation",
                                                 "/Event/Rec/Track/PreparedVelo"}; ///< output TES location
  DataObjectReadHandle<Tracks>  m_bestLocation{
      this, "bestLocation", TrackLocation::Default}; ///< location to look for tracks already used and skip here
  Gaudi::Property<double> m_ptVelo{this, "ptVelo", 400. * MeV};          ///< pT to assign to tracks
  Gaudi::Property<bool>   m_reverseCharge{this, "reverseCharge", false}; ///< Reverse the even=- odd=+ assigned charge
};

DECLARE_COMPONENT( TrackPrepareVelo )

StatusCode TrackPrepareVelo::execute() {

  const Tracks* inCont   = m_inputLocation.get();
  const Tracks* bestCont = ( !m_bestLocation.objKey().empty() ? m_bestLocation.get() : nullptr );
  auto          outCont  = std::make_unique<Tracks>();

  for ( auto track : *inCont ) {
    if ( m_bestLocation.objKey().empty() || !used( *track, bestCont ) ) {
      auto        aTrack = std::make_unique<LHCb::Track>( *track );
      const auto& ids    = aTrack->lhcbIDs();
      auto        fid    = std::find_if( ids.begin(), ids.end(), []( const auto& id ) { return id.isVelo(); } );
      if ( fid == ids.end() ) { return Warning( "Setting can not set random q/p for non-velo track" ); }
      int firstStrip = fid->veloID().strip();
      int charge     = ( firstStrip % 2 == 0 ? -1 : 1 );
      if ( m_reverseCharge.value() ) charge *= -1; // flip charge assignment if requested
      prepare( *aTrack, charge );
      outCont->insert( aTrack.release() );
    }
  }

  m_outputLocation.put( std::move( outCont ) );

  return StatusCode::SUCCESS;
}

bool TrackPrepareVelo::used( const Track& aTrack, const Tracks* bestCont ) const {
  // if the track has already been flagged as a clone then do not use it
  if ( aTrack.checkFlag( Track::Flags::Clone ) ) return true;
  // check if velo track is used.
  return std::any_of( bestCont->begin(), bestCont->end(), [&]( const auto& i ) {
    const auto& parents = i->ancestors();
    return std::any_of( parents.begin(), parents.end(), [&]( const auto& ref ) { return ref.target() == &aTrack; } );
  } );
}

void TrackPrepareVelo::prepare( Track& aTrack, const int charge ) const {
  // set q/p and error in all of the existing states
  for ( auto& iState : aTrack.states() ) {
    TrackVector& vec    = iState->stateVector();
    double       slope2 = std::max( vec( 2 ) * vec( 2 ) + vec( 3 ) * vec( 3 ), 1e-20 );
    double       curv   = charge * sqrt( slope2 ) / ( m_ptVelo * sqrt( 1. + slope2 ) );
    iState->setQOverP( curv );
    iState->setErrQOverP2( 1e-6 );
  }
}
