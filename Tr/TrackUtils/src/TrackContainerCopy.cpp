/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

/** @class TrackContainerCopy TrackContainerCopy.h
 *
 *  Copy a container of tracks. By default do not copy tracks that failed the fit
 *
 *  Properties:
 *
 *  - inputLocations: Vector of input locations to copy.
 *  - outputLocation: Output location to copy the tracks to.
 *  - copyFailures: Also copy tracks that are flagged invalid?
 *  - Selector: The selector to select a subsample of tracks to copy (e.g.  TrackSelector )
 *
 *  @author M.Needham
 *  @date   30/05/2006
 */

#include "Event/Track.h"
#include "GaudiAlg/GaudiAlgorithm.h"
#include "GaudiKernel/ToolHandle.h"
#include "TrackInterfaces/ITrackSelector.h"
#include <string>

using namespace LHCb;

class TrackContainerCopy final : public GaudiAlgorithm {

public:
  // Constructors and destructor
  TrackContainerCopy( const std::string& name, ISvcLocator* pSvcLocator );

  StatusCode execute() override;

private:
  Gaudi::Property<std::vector<std::string>> m_inputLocations{this, "inputLocations", {TrackLocation::Velo}};
  Gaudi::Property<std::string>              m_outputLocation{this, "outputLocation", TrackLocation::Default};
  Gaudi::Property<bool>      m_copyFailures{this, "copyFailures", false}; ///< If true, copy also tracks that failed fit
  ToolHandle<ITrackSelector> m_selector{this, "Selector", ""};
};

DECLARE_COMPONENT( TrackContainerCopy )

//=============================================================================
// Constructor
//=============================================================================
TrackContainerCopy::TrackContainerCopy( const std::string& name, ISvcLocator* pSvcLocator )
    : GaudiAlgorithm( name, pSvcLocator ) {
  // constructor
}

//=============================================================================
// Execute
//=============================================================================
StatusCode TrackContainerCopy::execute() {

  Tracks* outCont = getOrCreate<Tracks, Tracks>( m_outputLocation );
  size_t  naccepted( 0 );

  // loop
  for ( const std::string& inputLoc : m_inputLocations ) {

    LHCb::Track::Range inCont = getIfExists<LHCb::Track::Range>( inputLoc );
    if ( inCont.empty() ) continue;

    for ( const LHCb::Track* track : inCont ) {

      if ( ( !track->checkFlag( Track::Flags::Invalid ) || m_copyFailures.value() ) &&
           ( !m_selector.isEnabled() || m_selector->accept( *track ) ) ) {

        Track* aTrack = new LHCb::Track( *track );
        outCont->insert( aTrack );
        ++naccepted;
      }
    }
  }

  return StatusCode::SUCCESS;
}
