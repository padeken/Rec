/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

/** @class TrackContainerSplitter TrackContainerSplitter.h
 *
 *  Split container to two (passed and rejected) wrt to given selection criteria
 *
 *  @author Andrii Usachov
 *  @date   30/04/2020
 */

#include "Event/Track.h"
#include "GaudiAlg/MergingTransformer.h"
#include "GaudiAlg/Transformer.h"
#include "GaudiKernel/ToolHandle.h"
#include "TrackInterfaces/ITrackSelector.h"

namespace {
  /// The output data
  using OutConts = std::tuple<LHCb::Tracks, LHCb::Tracks>;
} // namespace

class TrackContainerSplitter final
    : public Gaudi::Functional::MultiTransformer<OutConts( const LHCb::Tracks& ),
                                                 Gaudi::Functional::Traits::BaseClass_t<GaudiAlgorithm>> {
public:
  TrackContainerSplitter( const std::string& name, ISvcLocator* pSvcLocator )
      : MultiTransformer( name, pSvcLocator, {KeyValue{"TracksInContainer", ""}},
                          {KeyValue{"PassedContainer", ""}, KeyValue{"RejectedContainer", ""}} ) {}

  OutConts operator()( const LHCb::Tracks& input_tracks ) const override {

    OutConts out;
    auto& [passed, rejected] = out;

    m_inputs += input_tracks.size();
    for ( auto& trk : input_tracks ) {
      auto& container = ( ( m_selector.empty() || m_selector->accept( *trk ) ) ? passed : rejected );
      container.insert( new LHCb::Track( *trk ) );
    }
    m_passed += passed.size();

    return out;
  }

private:
  ToolHandle<ITrackSelector>                 m_selector{this, "Selector", ""};
  mutable Gaudi::Accumulators::StatCounter<> m_inputs{this, "#inputs"};
  mutable Gaudi::Accumulators::StatCounter<> m_passed{this, "#passed"};
};

DECLARE_COMPONENT( TrackContainerSplitter )
