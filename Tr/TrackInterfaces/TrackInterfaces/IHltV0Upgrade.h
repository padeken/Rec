/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef TRACKINTERFACES_IHLTV0UPGRADE_H
#define TRACKINTERFACES_IHLTV0UPGRADE_H 1

// Include files
// -------------
// from Gaudi
#include "GaudiKernel/IAlgTool.h"

// forward declarations
namespace LHCb {
  class TwoProngVertex;
}

/** @class IHltV0Upgrade IHltV0Upgrade.h TrackInterfaces/IHltV0Upgrade.h
 *
 *
 *  @author Jaap Panman
 *  @date   2008-03-04
 */
struct IHltV0Upgrade : extend_interfaces<IAlgTool> {

  DeclareInterfaceID( IHltV0Upgrade, 2, 0 );

  // Estimate two prong vertex after refitting tracks using TrackTraj
  virtual StatusCode process( LHCb::TwoProngVertex& vertex ) const = 0;
};
#endif // TRACKINTERFACES_IHLTV0UPGRADE_H
