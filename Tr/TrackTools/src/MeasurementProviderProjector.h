/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
/** @class MeasurementProviderProjector MeasurementProviderProjector.h
 *
 * Implementation of templated MeasurementProviderProjector tool
 * see interface header for description
 *
 *  @author A. Usachov
 *  @date   15/10/2019
 */

#ifndef MEASUREMENTPROVIDERPROJECTOR_H
#define MEASUREMENTPROVIDERPROJECTOR_H 1

#include "Event/Measurement.h"
#include "Event/StateVector.h"
#include "Event/TrackParameters.h"
#include "GaudiAlg/GaudiTool.h"
#include "GaudiKernel/DataObjectHandle.h"
#include "GaudiKernel/ToolHandle.h"
#include "TrackInterfaces/IMeasurementProviderProjector.h"
#include "TrackKernel/ZTrajectory.h"
#include <type_traits>

// from GaudiKernel
#include "GaudiKernel/IMagneticFieldSvc.h"

// from TrackFitEvent
#include "Event/FitNode.h"
#include "Event/State.h"

// from TrackInterfaces
#include "Kernel/ITrajPoca.h"

// from TrackKernel
#include "TrackKernel/StateZTraj.h"

class IMagneticFieldSvc;
struct ITrajPoca;

class MeasurementProviderProjector : public extends<GaudiTool, IMeasurementProviderProjector> {
public:
  using extends::extends;

  StatusCode initialize() override;

  /// Project a state onto a measurement.
  /// returns StatusCode, residual and errMeasure
  virtual std::tuple<StatusCode, double, double> project( const LHCb::State&       state,
                                                          const LHCb::Measurement& meas ) const;

  /// Project the state vector in this fitnode and update projection matrix and reference residual
  virtual StatusCode projectReference( LHCb::FitNode& node ) const override;

  /// Retrieve the derivative of the residual wrt. the alignment parameters
  /// of the measurement. The details of the alignment transformation are
  /// defined in AlignTraj.
  virtual Derivatives alignmentDerivatives( const LHCb::StateVector& state, const LHCb::Measurement& meas,
                                            const Gaudi::XYZPoint& pivot ) const;

  StatusCode project( const LHCb::StateVector& statevector, LHCb::Measurement& meas, Gaudi::TrackProjectionMatrix& H,
                      double& residual, double& errMeasure, Gaudi::XYZVector& unitPocaVector, double& doca,
                      double& sMeas ) const;

  virtual bool useBField() const { return m_useBField; }

protected:
  /**
   * Helper struct storing the result of calls to internal_project
   */
  struct InternalProjectResult final {
    double                       sMeas;
    double                       doca;
    double                       residual;
    double                       errMeasure;
    Gaudi::TrackProjectionMatrix H;
    Gaudi::XYZVector             unitPocaVector;
  };

  virtual InternalProjectResult internal_project( const LHCb::StateVector& statevector,
                                                  const LHCb::Measurement& meas ) const;

  Gaudi::Property<bool> m_useBField{this, "UseBField", false}; /// Create StateTraj with or without BField information.
  Gaudi::Property<double> m_tolerance{this, "Tolerance",
                                      0.0005 * Gaudi::Units::mm}; ///< Required accuracy of the projection
  IMagneticFieldSvc*      m_pIMF = nullptr;                       ///< Pointer to the magn. field service
  ITrajPoca*              m_poca = nullptr;                       ///< Pointer to the ITrajPoca interface
};

#endif //  MEASUREMENTPROVIDERPROJECTOR_H
