/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/Track.h"
#include "Event/TwoProngVertex.h"
#include "GaudiAlg/GaudiTool.h"
#include "GaudiKernel/IMagneticFieldSvc.h"
#include "Kernel/ITrajPoca.h"
#include "TrackInterfaces/IHltV0Upgrade.h"
#include "TrackInterfaces/ITrackFitter.h"
#include "TrackKernel/TrackTraj.h"

/** @file HltV0Upgrade.cpp
 *
 *  @author Jaap Panman
 *  @date   2008-03-04
 *  based on example made by Wouter Hulsbergen
 */

/** @class HltV0Upgrade HltV0Upgrade.h
 *
 *  @author Jaap Panman
 *  @date   2008-03-04
 *  based on example made by Wouter Hulsbergen
 */

class HltV0Upgrade : public extends<GaudiTool, IHltV0Upgrade> {
public:
  /// Standard construct
  using extends::extends;

  StatusCode initialize() override;

  StatusCode process( LHCb::TwoProngVertex& vertex ) const override;

private:
  StatusCode fittrack( LHCb::Track& track ) const;

  ITrackFitter*      m_trackfitter = nullptr;
  ITrajPoca*         m_pocatool    = nullptr;
  IMagneticFieldSvc* m_magfieldsvc = nullptr;
};

DECLARE_COMPONENT( HltV0Upgrade )

//=============================================================================
// Initialization
//=============================================================================
StatusCode HltV0Upgrade::initialize() {
  return GaudiTool::initialize().andThen( [&] {
    m_magfieldsvc = svc<IMagneticFieldSvc>( "MagneticFieldSvc", true );
    m_pocatool    = tool<ITrajPoca>( "TrajPoca" );
    m_trackfitter = tool<ITrackFitter>( "TrackMasterFitter", "TrackFitter", this );
  } );
}

StatusCode HltV0Upgrade::fittrack( LHCb::Track& track ) const {
  StatusCode sc = StatusCode::SUCCESS;
  if ( track.fitStatus() == LHCb::Track::FitStatus::FitFailed ) {
    sc = StatusCode::FAILURE;
  } else if ( track.fitStatus() == LHCb::Track::FitStatus::FitStatusUnknown ) {
    m_trackfitter->operator()( track ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
  }
  return sc;
}

StatusCode HltV0Upgrade::process( LHCb::TwoProngVertex& vertex ) const {

  // extract the two tracks (it doesn't really matter which one is the positive one)
  LHCb::Track* trackA = const_cast<LHCb::Track*>( vertex.trackA() );
  LHCb::Track* trackB = const_cast<LHCb::Track*>( vertex.trackB() );

  // fit the tracks if necessary
  return StatusCode{fittrack( *trackA ) && fittrack( *trackB )}.andThen( [&] {
    // only do the rest if both tracks fitted successfully

    // turn the tracks into trajectories. it will be slightly faster here to
    // leave the fieldsvc pointer away.
    LHCb::TrackTraj trajA( *trackA, m_magfieldsvc );
    LHCb::TrackTraj trajB( *trackB, m_magfieldsvc );

    // mu is the expansion parameter along the tracktraj. seed the
    // expansion with the z position of the vertex:
    double muA, muB;
    muA = muB = vertex.position().z();

    // calls pocatool. last argument is required precision.
    Gaudi::XYZVector deltaX;
    return m_pocatool->minimize( trajA, muA, trajB, muB, deltaX, 0.001 * Gaudi::Units::mm ).andThen( [&] {
      // if successful, update the vertex
      LHCb::StateVector stateA = trajA.stateVector( muA );
      LHCb::StateVector stateB = trajB.stateVector( muB );

      // update the momentum of the first track
      vertex.momA()( 0 ) = stateA.tx();
      vertex.momA()( 1 ) = stateA.ty();
      vertex.momA()( 2 ) = stateA.qOverP();

      // update the momentum of the second track
      vertex.momB()( 0 ) = stateB.tx();
      vertex.momB()( 1 ) = stateB.ty();
      vertex.momB()( 2 ) = stateB.qOverP();

      // update the position of the vertex
      Gaudi::XYZPoint position( 0.5 * ( stateA.x() + stateB.x() ), 0.5 * ( stateA.y() + stateB.y() ),
                                0.5 * ( stateA.z() + stateB.z() ) );
      vertex.setPosition( position );

      // need to fill the distance somewhere. let's put it in the chsiquare
      vertex.setChi2( deltaX.R() );
    } );
  } );
}

//=============================================================================
