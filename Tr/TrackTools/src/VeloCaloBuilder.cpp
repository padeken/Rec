/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "CaloInterfaces/ICaloGetterTool.h"
#include "Event/RecVertex.h"
#include "Event/Track.h"
#include "Event/TrackFitResult.h"
#include "GaudiAlg/GaudiTupleAlg.h"
#include "Kernel/CaloCellID.h"
#include "Kernel/ILHCbMagnetSvc.h"
#include "Kernel/LHCbID.h"
#include <cmath>
#include <cstdio>
#include <cstdlib>

//-----------------------------------------------------------------------------
// Implementation file for class : VeloCaloBuilder
//
// 2010-09-16 : Paul Seyfert
//-----------------------------------------------------------------------------

/** @class VeloCaloBuilder VeloCaloBuilder.h
 *
 * \brief  Make a ValoCaloTrack: Get calo clusters and match them to velo tracks. no trackfit done.
 *
 * Parameters:
 * - zcut: z position of the first velo state. for Ks reco reduces background
 * - IPcut: minimum pseudo IP.
 * - quali: minimum quality of the match (recommend 0.5 .. 1)
 * - VeloLoc, ClusterLoc, OutputTracks: TES locations of the input containers and the outputcontainer
 * - PtkickConstant: parameter for magnetic field
 * - zKick: z position of the magnet's bending plane (in cm !!!!)
 * - eRes0, eRes1: resolution of the calorimeter (energetic resolution)
 * - eCorrect: scal measured energy
 *
 *  @author Paul Seyfert
 *  @date   2010-09-16
 */

class VeloCaloBuilder : public GaudiTupleAlg {

public:
  /// Standard constructor
  using GaudiTupleAlg::GaudiTupleAlg;
  /// initialization
  StatusCode initialize() override;
  /// execution
  StatusCode execute() override;

private:
  // -- Methods
  float matchchi( LHCb::Track const& velo, LHCb::Track const& Calo ); ///< calculate matching chi^2

  /// add track to output
  /// only 3 lines but there is more to be done in an elaborate study
  void TESpush( LHCb::Tracks* container, LHCb::Track* track, LHCb::Track* ancestor, LHCb::Track* calotrack ) const;

  // -- tools
  ILHCbMagnetSvc*  m_magFieldSvc = nullptr;
  ICaloGetterTool* m_getter      = nullptr;

  // -- properties
  Gaudi::Property<std::string> m_veloTracksName{this, "VeloLoc", LHCb::TrackLocation::Velo};
  Gaudi::Property<std::string> m_outputTracksName{this, "OutputTracks", "Rec/Track/VeloCalo"};
  Gaudi::Property<std::string> m_clusterlocation{this, "ClusterLoc", LHCb::CaloClusterLocation::Ecal};

  Gaudi::Property<float> m_ipcut{this, "IPcut", 0};      // min pseudo IP cut
  Gaudi::Property<float> m_zcut{this, "zcut", 100};      // whare the first Velo State should be
  Gaudi::Property<float> m_qualimodi{this, "quali", 10}; // min matching chi^2

  // -- variables from HltVeloEcalMatch
  // -- those now taken from N.Zwahlen's code. m_zKick also used for momentum
  Gaudi::Property<float> m_ptkickConstant{this, "PtkickConstant", 1.263f * (float)Gaudi::Units::GeV};
  Gaudi::Property<float> m_zKick{this, "zKick", 525.0};
  Gaudi::Property<float> m_eres[2] = {{this, "eRes0", 0.60f}, {this, "eRes1", 0.70f}};
  Gaudi::Property<float> m_eCorrect{this, "eCorrect", 1.2f};
};

DECLARE_COMPONENT( VeloCaloBuilder )

//=============================================================================
// Initialization
//=============================================================================
StatusCode VeloCaloBuilder::initialize() {
  StatusCode sc = GaudiTupleAlg::initialize();
  if ( sc.isFailure() ) { return sc; }

  m_magFieldSvc = svc<ILHCbMagnetSvc>( "MagneticFieldSvc", true );
  m_getter      = tool<ICaloGetterTool>( "CaloGetterTool" );

  return StatusCode::SUCCESS;
}

using namespace LHCb;
//=============================================================================
// Main execution
//=============================================================================
StatusCode VeloCaloBuilder::execute() {
  float field = -(float)( m_magFieldSvc->signedRelativeCurrent() ); // -- get field polarity
  if ( true ) {                                                     // debuging purpose
    LHCb::Tracks* velotracks = get<LHCb::Tracks>( m_veloTracksName );

    auto caloTracks = new LHCb::Tracks();
    caloTracks->reserve( 100 );

    put( caloTracks, m_outputTracksName );

    if ( true ) {           // debuging purpose
      double mass = 139.57; // pi or k mass
      m_getter->update();

      LHCb::CaloClusters* cluster = m_getter->clusters( m_clusterlocation );
      // -- if there are no clusters, there's nothing to do
      if ( !cluster ) return StatusCode::SUCCESS;
      LHCb::CaloClusters::const_iterator clusterIter;
      int                                matches   = 0;
      int                                nomatches = 0;
      // -- loop over clusters
      for ( clusterIter = cluster->begin(); clusterIter != cluster->end(); ++clusterIter ) {

        LHCb::CaloCluster* thecluster = *clusterIter;

        if ( thecluster->e() < mass ) continue;
        LHCb::Tracks::const_iterator veloIt;
        int                          locmatches = 0;
        // -- loop over velo tracks
        for ( veloIt = velotracks->begin(); velotracks->end() != veloIt; ++veloIt ) {
          // -- apply IP, z, type cuts
          {
            float x, y, tx, ty, z, norm, IP;
            x    = (float)( *veloIt )->firstState().x();
            y    = (float)( *veloIt )->firstState().y();
            tx   = (float)( *veloIt )->firstState().tx();
            ty   = (float)( *veloIt )->firstState().ty();
            z    = (float)( *veloIt )->firstState().z();
            norm = sqrt( tx * tx + ty * ty );
            IP   = ( fabs( x * ty - y * tx ) / norm );
            if ( IP < m_ipcut ) continue;
            if ( z < m_zcut ) continue;
            if ( ( *veloIt )->type() == LHCb::Track::Types::VeloR ) continue;
          }

          LHCb::Track* mytrack = ( *veloIt );

          // -- now make the calo stub
          auto calotrack = std::make_unique<LHCb::Track>( thecluster->key() );
          calotrack->setType( LHCb::Track::Types::Calo );
          const std::vector<LHCb::CaloClusterEntry> caloentries = thecluster->entries();
          // unsigned int caloarea; // not needed in the current version.
          for ( std::vector<LHCb::CaloClusterEntry>::const_iterator itter = caloentries.begin();
                itter != caloentries.end(); ++itter ) {
            LHCb::CaloClusterEntry    theentry = *itter;
            SmartRef<LHCb::CaloDigit> reffer   = theentry.digit();
            LHCb::CaloDigit           diggy    = *reffer;
            LHCbID                    iddy     = LHCbID( diggy.cellID() );
            // caloarea = diggy.cellID().area();
            calotrack->addToLhcbIDs( iddy );
          }

          Gaudi::TrackVector vector;
          vector[0] = thecluster->position().x();
          vector[1] = thecluster->position().y();
          vector[2] = thecluster->position().z();
          vector[3] = vector[0] / thecluster->position().z();
          vector[4] = vector[1] / thecluster->position().z();
          Gaudi::TrackSymMatrix matrix;
          matrix( 0, 0 ) = ( thecluster->position().spread() )( 0, 0 );
          matrix( 1, 1 ) = ( thecluster->position().spread() )( 1, 1 );
          matrix( 2, 2 ) = vector[0] / thecluster->position().z() * vector[0] / thecluster->position().z();
          matrix( 3, 3 ) = vector[3] * vector[3] * 0.05 * 0.05;
          matrix( 4, 4 ) = vector[4] * vector[4] * 0.08 * 0.08;
          LHCb::State calostate =
              LHCb::State( vector, matrix, thecluster->position().z(), LHCb::State::Location::BegECal );
          calotrack->addToStates( ( calostate ) );

          float quality = matchchi( *mytrack, *calotrack );
          // -- at this point the HltVeloEcalMatch tool was used
          // m_matchtool->function(*mytrack,*calotrack);

          // -- if the chi^2 cut is passed ...
          if ( quality < m_qualimodi ) {
            // ... the momentum is calculated
            float kick =
                float( mytrack->firstState().x() +
                       mytrack->firstState().tx() * ( calotrack->firstState().z() - mytrack->firstState().z() ) -
                       calotrack->firstState().x() );
            float qp     = float( -kick / 9950000. * field ); // field corrects up/down field configuration
            float corrtx = float( calotrack->firstState().x() - mytrack->firstState().x() -
                                  mytrack->firstState().tx() * ( 10 * m_zKick - mytrack->firstState().z() ) );
            corrtx       = corrtx / ( float( calotrack->firstState().z() ) - m_zKick * 10 );

            // -- using upstream instead of velo tracks, a momentum cut can purify the VeloCalo tracks here
            if ( true ) {
              matches++;
              locmatches++;
              // -- the final track
              LHCb::Track* aCopy = new LHCb::Track();
              aCopy->addToAncestors( *mytrack );
              aCopy->addToAncestors( *calotrack );
              for ( auto anc2 = mytrack->ancestors().begin(); anc2 != mytrack->ancestors().end(); ++anc2 ) {
                aCopy->addToAncestors( *( *anc2 ) );
              }
              std::vector<LHCb::State*> velostates = mytrack->states();
              std::vector<LHCb::State*> copiedstates;
              for ( const auto* s : velostates ) {
                copiedstates.push_back( new LHCb::State{*s} );
                // calculate momentum
                Gaudi::TrackSymMatrix cov = copiedstates.back()->covariance();
                copiedstates.back()->setQOverP( qp );
                cov( 4, 4 ) = qp * qp * 0.07 * 0.07;
                // this is only a fake covariance matrix, but the offdiagonal entries should at least reflekt the
                // pt-kick calculation
                copiedstates.back()->setCovariance( cov );
              }
              aCopy->addToStates( copiedstates );
              aCopy->addToLhcbIDs( mytrack->lhcbIDs() );

              velostates.clear();
              copiedstates.clear();
              velostates = calotrack->states();

              for ( const auto* s : velostates ) {
                copiedstates.push_back( new LHCb::State{*s} );
                // -- calculated momentum
                copiedstates.back()->setQOverP( qp );
                copiedstates.back()->setTx( corrtx );
              }
              aCopy->addToStates( copiedstates );
              aCopy->addToLhcbIDs( calotrack->lhcbIDs() );

              LHCb::TrackFitResult* result = new LHCb::TrackFitResult();
              result->setPScatter( 1 / qp );

              aCopy->setFitResult( result );

              aCopy->setType( LHCb::Track::Types::Long );
              aCopy->setChi2AndDoF( quality * 60, 60 );
              TESpush( caloTracks, aCopy, mytrack, calotrack.release() );
            } else {
              nomatches++;
            }
          } else {
            nomatches++;
          }
        } // -- looped over velo tracks
      }   // -- looped over clusters
    }     // if true
  }       // if true
  return StatusCode::SUCCESS;
}

//=============================================================================
//  Add tracks to output container and write an additional info
//=============================================================================
void VeloCaloBuilder::TESpush( LHCb::Tracks* container, LHCb::Track* track, LHCb::Track* ancestor,
                               LHCb::Track* calotrack ) const {
  track->addInfo( 2001, ancestor->key() );
  track->addInfo( 2002, calotrack->key() );
  container->add( track );
}

//=============================================================================
//  calculate matching chi^2. mainly taken from N. Zwahlen's HltVeloEcalMatch
//=============================================================================
float VeloCaloBuilder::matchchi( LHCb::Track const& velo, LHCb::Track const& calo ) {
  static const double cellSizeECal[3] = {121.7, 60.85, 40.56};
  int                 ids_ecal        = 0;
  double              siz_ecal        = 0.;
  for ( auto itid = calo.lhcbIDs().begin(); itid != calo.lhcbIDs().end(); ++itid ) {
    if ( !itid->isCalo() ) continue;
    LHCb::CaloCellID cid = itid->caloID();
    if ( cid.calo() != 2 ) continue;
    int area = cid.area();
    if ( area < 0 || area > 2 ) continue;
    ids_ecal++;
    siz_ecal += cellSizeECal[area];
  }

  double cell_size = cellSizeECal[0];
  if ( ids_ecal > 0 ) { cell_size = siz_ecal / ids_ecal; }

  const LHCb::State& state = calo.firstState();
  float              x     = (float)( state.x() / Gaudi::Units::cm );
  float              y     = (float)( state.y() / Gaudi::Units::cm );
  float              z     = (float)( state.z() / Gaudi::Units::cm ); // Shower Max
  float              ex    = (float)( 2. * cell_size / Gaudi::Units::cm );
  float              ey    = ex;
  float              e     = (float)( calo.pt() / Gaudi::Units::GeV );

  e *= m_eCorrect;

  // -- get track slopes
  double trackDxDz = velo.firstState().tx();
  double trackDyDz = velo.firstState().ty();

  // -- Absolute energy uncertainty:
  double de = e * ( sqrt( m_eres[0] * m_eres[0] + m_eres[1] * m_eres[1] / e ) );

  double deltaZ = z - m_zKick;
  double xkick  = deltaZ * ( m_ptkickConstant / Gaudi::Units::GeV ) / e;
  double exkick = fabs( xkick / e ) * de;

  // -- Calculate the slopes and their uncertainties:
  double edxdz = sqrt( ex * ex + exkick * exkick ) / z;
  double dydz  = y / z;
  double edydz = ey / z;

  double uncorrdxdz = ( x / z );
  double dxdz;
  // -- apply the kick in the "right" direction
  if ( uncorrdxdz - trackDxDz < 0 )
    dxdz = ( x + xkick ) / z;
  else
    dxdz = ( x - xkick ) / z;

  // -- calculate chi2
  double deltaX = ( dxdz - trackDxDz ) / edxdz;
  double deltaY = ( dydz / fabs( dydz ) ) * ( dydz - trackDyDz ) / edydz;
  float  chi2   = (float)( deltaX * deltaX + deltaY * deltaY );

  return chi2;
}
