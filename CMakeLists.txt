###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
CMAKE_MINIMUM_REQUIRED(VERSION 2.8.5)

#---------------------------------------------------------------
# Load macros and functions for Gaudi-based projects
find_package(GaudiProject)
#---------------------------------------------------------------

set(USE_DD4HEP "OFF" CACHE BOOL "Controls whether DD4hep is used for the subdetectors supporting it")
if (USE_DD4HEP)
  add_compile_definitions(USE_DD4HEP)
endif()


set( CMAKE_EXPORT_COMPILE_COMMANDS ON )
# Declare project name and version
gaudi_project(Rec v31r2
              USE Lbcom v31r2
              DATA TCK/HltTCK
                   ChargedProtoANNPIDParam VERSION v1r*
                   TMVAWeights)
