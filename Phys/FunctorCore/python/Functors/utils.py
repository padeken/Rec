###############################################################################
# (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
def pack_dict(input_dict, wrap=None):
    """Given a string-keyed BoundFunctor-valued dictionary, pack that into a
    dictionary-of-vectors property added by the with_functor_maps C++ mixin.

    This is basically a workaround for missing support for
    Gaudi::Property<std::map<std::string, ThOr::FunctorProxy>>

    The optional `wrap` argument is an extra functor adapter that will be used
    to wrap all of the functors in the dictionary. The canonical usage of this
    feature is to add a wrapping `POD` functor to ensure plain, scalar data
    types are returned.
    """
    if input_dict is None: return {}
    if wrap is not None:
        input_dict = {k: wrap(v) for k, v in input_dict.items()}
    return {
        k: [v.code(), v.code_repr()] + v.headers()
        for k, v in input_dict.items()
    }
