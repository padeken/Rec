###############################################################################
# (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import os
import Configurables
import GaudiKernel.Configurable
from Gaudi.Configuration import (ApplicationMgr, DEBUG)
from Functors import (ALL, FILTER, POD)
from Functors.utils import pack_dict
from Functors.tests.categories import functors_for_class
from Configurables import (EvtStoreSvc, ProducePrFittedForwardTracks,
                           FunctorFactory, CondDB, DDDBConf)

# This lets us use this set of options to generate a functor cache that excludes the functors that we want to use cling
inside_cache_generation = ('CURRENTLY_GENERATING_FUNCTOR_CACHE' in os.environ)

produce = ProducePrFittedForwardTracks(Output='Fake/Tracks')
ApplicationMgr().EvtMax = 0 if inside_cache_generation else 10
ApplicationMgr().EvtSel = 'NONE'
ApplicationMgr().ExtSvc = [EvtStoreSvc("EventDataSvc")]
ApplicationMgr().TopAlg = [produce]
# Need valid DB tags for particle name -> mass translation to work
DDDBConf(DataType='Upgrade', Simulation=True)
CondDB(LatestGlobalTagByDataType='Upgrade', Upgrade=True)
# Workaround for gaudi/Gaudi#117 that monkey patches gaudirun.py
GaudiKernel.Configurable.expandvars = lambda x: x

if not inside_cache_generation:
    FunctorFactory().OutputLevel = DEBUG


def add_algorithm(type_suffix, eval_or_init, functors, producer=None):
    algo_type = getattr(Configurables,
                        'TestThOrFunctor' + eval_or_init + '__' + type_suffix)
    functors = {f.code_repr(): f for f in functors}
    algo_instance = algo_type(
        Functors=pack_dict(functors), PODFunctors=pack_dict(functors, POD))
    if producer is not None: algo_instance.Input = producer
    if hasattr(algo_instance, 'Cut'):
        algo_instance.Cut = pack_dict({'': FILTER(ALL)})
    if not inside_cache_generation:
        algo_instance.OutputLevel = DEBUG
    ApplicationMgr().TopAlg.append(algo_instance)


# which inputs we can provide at runtime in this test
inputs = {
    'Container': produce.Output,
    'Children': [produce.Output],
}


def setup_test(type_suffix,
               class_name,
               producer=None,
               exclusions=[],
               cannot_execute=[]):
    # get the functors corresponding to this class
    functors, functors_init_only = functors_for_class(
        class_name,
        inputs,
        exclusions=exclusions,
        cannot_execute=cannot_execute)
    if producer is None:
        # in principle `functors` are executable, but we don't have any input
        # to feed them -- shuffle these functors into `functors_init_only`
        functors_init_only += functors
    elif len(functors):
        # `functors` are executable and we have a producer to feed them with
        add_algorithm(type_suffix, 'Evaluation', functors, producer=producer)
    if len(functors_init_only):
        add_algorithm(type_suffix, 'Initialisation', functors_init_only)


# somewhat-special case of void functors
setup_test('void', 'Void')
# LHCb::v2::Composites
# @todo setup a producer of v2::Composites from 'produce' and add execution tests
setup_test(
    'Composites',
    'Composite',
    exclusions=['CHARGE', 'ETA', 'P', 'PHI', 'MINIP', 'MINIPCUT'])
# LHCb::Pr::Fitted::Forward::Tracks
PrFittedForwardTracks_derived_execute_exclusions = [
    # This is because the fake LHCb::Pr::Fitted::Forward::Tracks that we use
    # for the execute tests do not have valid long track ancestors.
    'NHITS',
]
setup_test(
    'PrFittedForwardTracks',
    'Track',
    producer=produce.Output,
    cannot_execute=PrFittedForwardTracks_derived_execute_exclusions)
# LHCb::Pr::Fitted::Forward::TracksWithPVs
setup_test(
    'PrFittedForwardTracksWithPVs',
    'Track',
    cannot_execute=PrFittedForwardTracks_derived_execute_exclusions)
# LHCb::Pr::Fitted::Forward::TracksWithMuonID
setup_test(
    'PrFittedForwardTracksWithMuonID',
    'TrackWithMuonID',
    cannot_execute=PrFittedForwardTracks_derived_execute_exclusions)
