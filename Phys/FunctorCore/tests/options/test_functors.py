#!/usr/bin/env python
###############################################################################
# (c) Copyright 2019-20 CERN for the benefit of the LHCb Collaboration        #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# =============================================================================
## @file Small script to test functors instantiation
#  @author Saverio Mariani
##
# =============================================================================
from builtins import str
from builtins import range
import Configurables
from Configurables import (ApplicationMgr, LHCbApp)
from Functors import *
from Functors.utils import pack_dict

generic_functors = [
    ALL,
    NONE,
]

all_new_eventmodel_track_functors = [
    NHITS,
]

# these rely on forward->velo ancestor links. vectorised following of these is
# not currently implemented, so we exclude them from the test (compilation
# would fail with a static_assert)
all_tracks_except_unfitted_prforward_avx = [
    ETA,
    PHI,
    MINIP(''),
    CLOSESTTOBEAM(X),
    CLOSESTTOBEAM(Y),
    CLOSESTTOBEAM(Z),
    CLOSESTTOBEAM(TX),
    CLOSESTTOBEAM(TY),
    MINIPCUT(IPCut=0.0, Vertices=''),
]

# TODO this is no longer true
# The MatrixNet implementation is currently not generalised to support running
# with vector types (OL doesn't see a major barrier to this being done though!)
scalar_track_functors = [
    MVA(MVAType='MatrixNet',
        Config={'MatrixnetFile': "${PARAMFILESROOT}/data/Hlt1TwoTrackMVA.mx"},
        Inputs={
            'chi2': ETA,
            'fdchi2': PHI,
            'sumpt': ETA,
            'nlt16': PHI,
        }),
]

only_velo_track_functors = []

only_long_track_functors = [
    P,
]

only_long_track_functors_except_track_v2 = [
    QOVERP,
]

# see all_tracks_except_unfitted_prforward_avx above -- PT requires ETA and P,
# ETA requires navigating back to the state in the VELO
long_tracks_except_unfitted_prforward_avx = [
    PT,
]

fitted_track_functors = [
    CLOSESTTOBEAM(QOVERP),
]
for row in range(5):
    for col in range(row, 5):
        fitted_track_functors.append(CLOSESTTOBEAM(COV(Row=row, Col=col)))

fitted_track_or_composite_functors = [
    NDOF,
    CHI2DOF,
    MINIPCHI2(''),
    BPVIPCHI2(''),
    MINIPCHI2CUT(IPChi2Cut=0.0, Vertices=''),
]

# this was removed from v2::Track, hopefully there will be some new ghostprob
# event model object soon...
only_v1_track_functors = [
    NDOF,  # TODO can this move?
    GHOSTPROB,
]

only_combination_functors = [
    DOCA(1, 2),
    DOCACHI2(1, 2), MAXDOCA, MAXDOCACHI2,
    MAXDOCACUT(10.),
    MAXDOCACHI2CUT(10.),
    SUM(PT),
    MIN(PT),
    MAX(PT), CHARGE
]

only_composite_functors = [
    MASS(Masses=[497., 497.]),  # assume we'll test with a 2-body vertex...
    MASS(Masses=[137., 'pi+']),
    MASS(Masses=["mu+", "mu-"]),
    BPVETA(''),
    BPVCORRM(Mass=497., Vertices=''),
    BPVDIRA(''),
    BPVFDCHI2(''),
    COMB(
        Functor=SUM(PT),
        # C++ version of this functor isn't well-formed with zero containers...
        ChildContainers=[''],
        ChildContainerTypes=[('LHCb::Pr::Fitted::Forward::Tracks',
                              'Event/PrFittedForwardTracks.h')]),
]


def test_functors(alg_name_suffix, functors_to_test, SkipCut=False):
    algo = getattr(Configurables, 'InstantiateFunctors__' + alg_name_suffix)
    test = algo('Test' + alg_name_suffix)
    test.Functions = pack_dict(
        {functor.code_repr(): functor
         for functor in functors_to_test})
    if not SkipCut: test.Cut = FILTER(ALL)
    ApplicationMgr().TopAlg.append(test)


def test_pr(prname, functors, only_unwrapped_functors=[]):
    test_functors(prname, functors)


app = LHCbApp()
app.EvtMax = 0

# Explicitly disable use of the functor cache. If the variable
# THOR_BUILD_TEST_FUNCTOR_CACHE is set then a cache will be generated from
# these options, so if we *didn't* disable the cache then the test that cling
# can handle all of these functors would be bypassed.
from Configurables import FunctorFactory
FunctorFactory().UseCache = False

# Simple instantiation test: are the templates working?
#
# See InstantiateFunctors.cpp for the explicit type names that are being used
# here, and if you want to add new instantiations.
test_functors(
    'vector__TrackCompactVertex__2_double', generic_functors +
    fitted_track_or_composite_functors + only_composite_functors)
test_pr(
    'PrVeloTracks',
    generic_functors + only_velo_track_functors +
    all_tracks_except_unfitted_prforward_avx +
    all_new_eventmodel_track_functors,
    only_unwrapped_functors=scalar_track_functors)
forward_functors = generic_functors + only_long_track_functors
test_pr(
    'PrLongTracks',
    forward_functors + all_new_eventmodel_track_functors +
    only_long_track_functors_except_track_v2,
    only_unwrapped_functors=scalar_track_functors +
    all_tracks_except_unfitted_prforward_avx +
    long_tracks_except_unfitted_prforward_avx)
test_functors('Track_v1', only_v1_track_functors)
test_functors(
    'Track_v2', forward_functors + fitted_track_functors +
    fitted_track_or_composite_functors +
    all_tracks_except_unfitted_prforward_avx +
    long_tracks_except_unfitted_prforward_avx)
test_functors('vector__ParticleCombination__FittedWithMuonID__2',
              generic_functors + only_combination_functors)
