/*****************************************************************************\
* (c) Copyright 2019-20 CERN for the benefit of the LHCb Collaboration        *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MODULE utestSelection
#include "Event/GeneratePrFittedForwardTracks.h"
#include "Event/Particle_v2.h"
#include "Event/PrVeloTracks.h"
#include "Functors/Adapters.h"
#include "Functors/Filter.h"
#include "Functors/MVA.h"
#include "Functors/TrackLike.h"
#include "PrKernel/PrSelection.h"
#include "SelTools/MatrixNet.h"

#include <boost/test/unit_test.hpp>

#include <cmath>
#include <random>
#include <vector>

// Define some special functors that have execution counters so that we can
// test logical operator short-circuiting. Note that when constructing
// composite expressions the functors are copied, so having a mutable member
// variable and accessor does not work, as it is difficult to find the actual
// PT functor deep inside the composite functor object.
struct PT_Count : public Functors::Function {
  std::size_t& m_counter;
  PT_Count( std::size_t& counter ) : m_counter( counter ) {}
  template <typename TL>
  auto operator()( TL const& track ) const {
    m_counter++;
    return track.pt();
  }
};

struct ETA_Count : public Functors::Function {
  std::size_t& m_counter;
  ETA_Count( std::size_t& counter ) : m_counter( counter ) {}
  template <typename TL>
  auto operator()( TL const& track ) const {
    m_counter++;
    return track.pseudoRapidity();
  }
};

// Check that functors can be instantiated
BOOST_AUTO_TEST_CASE( test_creating_functors ) {
  [[maybe_unused]] Functors::AcceptAll ALL;
  [[maybe_unused]] auto                ip_gt_10 = Functors::Track::MinimumImpactParameterCut( 10.f, "Path/To/PVs" );
}

struct DummyState {
  ROOT::Math::XYZVector m_position, m_slopes;
  DummyState( ROOT::Math::XYZVector position, ROOT::Math::XYZVector slopes )
      : m_position( std::move( position ) ), m_slopes( std::move( slopes ) ) {}
  ROOT::Math::XYZVector const& position() const { return m_position; }
  ROOT::Math::XYZVector const& slopes() const { return m_slopes; }
};

// dummy track type
struct DummyTrack {
  float                 m_pt{0.f}, m_eta{0.f};
  bool                  m_ismuon{false};
  float                 pt() const { return m_pt; }
  bool                  loop_mask() const { return true; }
  bool                  IsMuon() const { return m_ismuon; }
  float                 pseudoRapidity() const { return m_eta; }
  float                 chi2PerDoF() const { return 1.f; }
  float                 ghostProbability() const { return 0.5f; }
  LHCb::State const*    stateAt( LHCb::State::Location ) const { return nullptr; }
  ROOT::Math::XYZVector closestToBeamStatePos() const { return {0.0, 0.0, 0.0}; }
  ROOT::Math::XYZVector closestToBeamStateDir() const { return {0.0, 0.0, 0.0}; }
  LHCb::State           closestToBeamState() const { return {}; }
  DummyTrack( float pt, float eta, bool ismuon ) : m_pt( pt ), m_eta( eta ), m_ismuon( ismuon ) {}
};

using Tracks       = std::vector<DummyTrack>;
using TrackFunctor = Functors::Functor<Tracks( Tracks const& )>;
Tracks make_tracks() {
  Tracks tracks;
  tracks.emplace_back( 0.f, 3.f, true );
  tracks.emplace_back( 20.f, 1.f, false );
  tracks.emplace_back( 40.f, 5.f, false );
  return tracks;
}

using Functors::Filter;

// check we can apply an IsMuon cut to some tracks
BOOST_AUTO_TEST_CASE( test_ptcut ) {
  TrackFunctor ISMUON{Filter{Functors::Track::IsMuon{}}};
  auto         tracks          = make_tracks();
  auto         filtered_tracks = ISMUON( tracks );
  BOOST_CHECK( tracks.size() == 3 );
  BOOST_CHECK( filtered_tracks.size() == 1 );
}

// check that it also works for Pr::Selection
using SelOfDummy = Pr::Selection<DummyTrack>;
BOOST_AUTO_TEST_CASE( test_ptcut_selection ) {
  Functors::Functor<SelOfDummy( SelOfDummy const& )> ISMUON{Filter{Functors::Track::IsMuon{}}};
  auto                                               tracks = make_tracks();
  Pr::Selection                                      track_sel{tracks};
  auto                                               filtered_track_sel = ISMUON( track_sel );
  BOOST_CHECK( track_sel.size() == 3 );
  BOOST_CHECK( filtered_track_sel.size() == 1 );
}

// check that composition gives something sensible
BOOST_AUTO_TEST_CASE( test_functor_composition ) {
  Functors::Track::PseudoRapidity     ETA;
  Functors::Track::TransverseMomentum PT;
  auto                                tracks = make_tracks();
  TrackFunctor                        high_pT_any_eta{Filter{( PT > 10.f ) & ( ETA > 0 )}};
  BOOST_CHECK( high_pT_any_eta( tracks ).size() == 2 );
  TrackFunctor high_pT_or_eta{Filter{( PT > 10.f ) | ( ETA > 2 )}};
  BOOST_CHECK( high_pT_or_eta( tracks ).size() == 3 );
  TrackFunctor low_pT{Filter{~( PT > 10.f )}};
  BOOST_CHECK( low_pT( tracks ).size() == 1 );
  TrackFunctor low_eta_or_high_pT{Filter{~( ETA > 2 ) | ( PT > 30.f )}};
  BOOST_CHECK( low_eta_or_high_pT( tracks ).size() == 2 );
}

// check that predicates constructed from functors work
BOOST_AUTO_TEST_CASE( test_predicate_from_function ) {
  Functors::Track::TransverseMomentum PT;
  auto                                tracks       = make_tracks();
  TrackFunctor                        pt_gt_10     = Filter{PT > 10};
  TrackFunctor                        pt_lt_10     = Filter{PT < 10};
  TrackFunctor                        pt_gt_10_alt = Filter{10 < PT};
  TrackFunctor                        pt_lt_10_alt = Filter{10 > PT};
  BOOST_CHECK( pt_gt_10( tracks ).size() == 2 );
  BOOST_CHECK( pt_lt_10( tracks ).size() == 1 );
  BOOST_CHECK( pt_gt_10( tracks ).size() == pt_gt_10_alt( tracks ).size() );
  BOOST_CHECK( pt_lt_10( tracks ).size() == pt_lt_10_alt( tracks ).size() );
}

auto one_track_functor() {
  // This is the full Run2 Hlt1TrackMVA expression, but we can't actually test using it because it has data dependencies
  Functors::Track::TransverseMomentum PT;
  Functors::Track::GhostProbability   GHOSTPROB;
  Functors::Track::Chi2PerDoF         CHI2PERDOF;

  auto         MinPT     = 1.f;
  auto         MaxPT     = 25.f;
  auto         MinIPChi2 = 7.4f;
  auto         PV_loc    = "Rec/Vertex/Primary";
  TrackFunctor one_track =
      Filter{( CHI2PERDOF < 2.f ) & ( GHOSTPROB < 0.3f ) &
             ( ( ( PT > MaxPT ) & Functors::Track::MinimumImpactParameterChi2Cut( MinIPChi2, PV_loc ) ) |
               ( Functors::math::in_range( MinPT, PT, MaxPT ) &
                 ( Functors::math::log( Functors::Track::MinimumImpactParameterChi2( PV_loc ) ) >
                   ( 1.0 / Functors::math::pow( PT - 1.0, 2 ) + ( 1.0 / MaxPT ) * ( MaxPT - PT ) +
                     std::log( MinIPChi2 ) ) ) ) )};
  // If we actually call one_track_functor() this will crash, but we can at least make sure the template can be
  // instantiated
  one_track( make_tracks() );
}

BOOST_AUTO_TEST_CASE( test_1trackmva_functor ) {
  Functors::Track::TransverseMomentum PT;
  Functors::Track::PseudoRapidity     ETA;
  Functors::Track::GhostProbability   GHOSTPROB;
  Functors::Track::Chi2PerDoF         CHI2PERDOF;
  auto                                MinPT          = 1.f;
  auto                                MaxPT          = 25.f;
  auto                                MinIPChi2      = 7.4f;
  TrackFunctor                        pt_squared_cut = Filter{Functors::math::pow( PT, 2 ) > 2.f};
  // This has had ETA put in place of MINIPCHI2 so it should actually be able to run...
  TrackFunctor one_track_simple =
      Filter{( CHI2PERDOF < 2.f ) & ( GHOSTPROB < 0.3f ) &
             ( ( ( PT > MaxPT ) & ( ETA > MinIPChi2 ) ) |
               ( Functors::math::in_range( MinPT, PT, MaxPT ) &
                 ( Functors::math::log( ETA ) > ( 1.0 / Functors::math::pow( PT - 1.0, 2 ) +
                                                  ( 1.0 / MaxPT ) * ( MaxPT - PT ) + std::log( MinIPChi2 ) ) ) ) )};
  auto tracks = make_tracks();
  auto test_1 = pt_squared_cut( tracks );
  auto test_2 = one_track_simple( tracks );
}

BOOST_AUTO_TEST_CASE( test_short_circuiting ) {
  std::size_t  pT_counter{0}, eta_counter{0};
  PT_Count     counting_PT( pT_counter );
  ETA_Count    counting_ETA( eta_counter );
  TrackFunctor high_pT_and_eta = Filter{( counting_PT > 10.f ) & ( counting_ETA > 3.f )};
  auto         cut_tracks      = high_pT_and_eta( make_tracks() );
  BOOST_CHECK( cut_tracks.size() == 1 );
  BOOST_CHECK( pT_counter == 3 );  // should be called for every track
  BOOST_CHECK( eta_counter == 2 ); // should only be called for high pT tracks

  pT_counter = eta_counter    = 0; // reset counters for clarity
  TrackFunctor high_pT_or_eta = Filter{( counting_PT > 10.f ) | ( counting_ETA > 2.f )};
  auto         cut_tracks2    = high_pT_or_eta( make_tracks() );
  BOOST_CHECK( cut_tracks2.size() == 3 );
  BOOST_CHECK( pT_counter == 3 );  // should be called for every track
  BOOST_CHECK( eta_counter == 1 ); // should only be called for low pT tracks
}

BOOST_AUTO_TEST_CASE( test_scalar_functors ) {
  BOOST_CHECK_THROW(
      TrackFunctor{Filter{Functors::Track::MinimumImpactParameterCut( 0.1, "Path/To/PVs" )}}( make_tracks() ),
      GaudiException );
}

BOOST_AUTO_TEST_CASE( test_vector_functors ) {
  using VT = LHCb::Pr::Velo::Tracks;
  BOOST_CHECK_THROW( Functors::Functor<VT( VT const& )>{Filter{
                         Functors::Track::MinimumImpactParameterCut( 0.1, "Path/To/PVs" )}}( VT{} ),
                     GaudiException );
}

BOOST_AUTO_TEST_CASE( test_combination_cut_and_function ) {
  using Combination         = Sel::ParticleCombination<DummyTrack, DummyTrack>;
  using CombinationCut      = Functors::Functor<bool( Combination const& )>;
  using CombinationFunction = Functors::Functor<float( Combination const& )>;
  DummyTrack     t1{5.f, 0.f, false}, t2{10.f, 0.f, false};
  Combination    combination{t1, t2}; // the scalar sum of pTs is 15
  auto const     SUMPT = Functors::Adapters::Accumulate{Functors::Track::TransverseMomentum{}};
  CombinationCut comb_cut{SUMPT > 20.f}, comb_cut_2{SUMPT > 10.f};
  BOOST_CHECK( comb_cut( combination ) == false );
  BOOST_CHECK( comb_cut_2( combination ) == true );

  CombinationFunction sumpt_fn{SUMPT};
  BOOST_CHECK( sumpt_fn( combination ) == t1.pt() + t2.pt() );
}

using TrackFunction  = Functors::Functor<float( DummyTrack const& )>;
using TrackPredicate = Functors::Functor<bool( DummyTrack const& )>;
BOOST_AUTO_TEST_CASE( test_single_object_functions ) {
  using namespace Functors::Track;
  DummyTrack     track{5.f, 1.f, false}; // pT, eta, IsMuon
  TrackPredicate ismuon{IsMuon{}};
  TrackFunction  pT{TransverseMomentum{}}, eta{PseudoRapidity{}};
  BOOST_CHECK( pT( track ) == track.pt() );
  BOOST_CHECK( eta( track ) == track.pseudoRapidity() );
  BOOST_CHECK( ismuon( track ) == track.IsMuon() );
}

struct InfoStruct {
  std::ostream& stream() { return std::cout; }
};

struct Algorithm {
  InfoStruct info() { return {}; }
};

BOOST_AUTO_TEST_CASE( test_mva_functor ) {
  using namespace Functors;
  auto mva =
      MVA<Sel::MatrixNet>( {{"MatrixnetFile", std::string{"${PARAMFILESROOT}/data/Hlt1TwoTrackMVA.mx"}}},
                           MVAInput( "chi2", Track::Chi2PerDoF{} ), MVAInput( "fdchi2", Track::PseudoRapidity{} ),
                           MVAInput( "sumpt", Adapters::Accumulate{Track::TransverseMomentum{}} ),
                           MVAInput( "nlt16", Track::PseudoRapidity{} ) );
  TopLevelInfo top_level;
  mva.bind( top_level );
}

struct ChildRelation {
  ChildRelation( std::size_t index, Zipping::ZipFamilyNumber family ) : m_index{index}, m_family{family} {}
  std::size_t              index() const { return m_index; }
  Zipping::ZipFamilyNumber zipIdentifier() const { return m_family; }

private:
  std::size_t              m_index{std::numeric_limits<std::size_t>::max()};
  Zipping::ZipFamilyNumber m_family{Zipping::generateZipIdentifier()};
};

template <std::size_t N>
struct Vertex {
  template <typename... T>
  Vertex( T&&... args ) : m_children{std::forward<T>( args )...} {}
  static auto        numChildren() { return num_children; }
  [[nodiscard]] auto childRelationIndex( std::size_t n_child ) const { return m_children[n_child].index(); }
  [[nodiscard]] auto childRelationFamily( std::size_t n_child ) const { return m_children[n_child].zipIdentifier(); }

  static constexpr std::size_t num_children = N;

private:
  std::array<ChildRelation, N> m_children;
};

BOOST_AUTO_TEST_CASE( test_combination_adapter ) {
  using ChildTracks                  = LHCb::Pr::Fitted::Forward::Tracks;
  ChildTracks const tracks1          = LHCb::Pr::Fitted::Forward::generate_tracks( 3 /* nTracks */, 1 /* seed */ );
  ChildTracks const tracks2          = LHCb::Pr::Fitted::Forward::generate_tracks( 3 /* nTracks */, 2 /* seed */ );
  auto const        iterable_tracks1 = LHCb::Pr::make_zip<SIMDWrapper::Scalar>( tracks1 );
  auto const        iterable_tracks2 = LHCb::Pr::make_zip<SIMDWrapper::Scalar>( tracks2 );

  std::size_t tracks1_index{0}, tracks2_index{2};
  Vertex<2>   parent{ChildRelation{tracks1_index, tracks1.zipIdentifier()},
                   ChildRelation{tracks2_index, tracks2.zipIdentifier()}};

  // Get the tracks by hand, so we can check the functor works properly
  auto const& t1 = iterable_tracks1[tracks1_index];
  auto const& t2 = iterable_tracks2[tracks2_index];

  // Functor we want to apply to the combination -- scalar sum of pT
  Functors::Adapters::Accumulate cf{Functors::Track::TransverseMomentum{}};

  // Set up the functor that should recover the combination of these tracks
  // from 'parent'...but this includes some magic to get the ChildTracks
  // from the given TES locations, so we can't easily run it here. For now
  // we just check that creating it works.
  auto pf = Functors::Adapters::CombinationFromComposite<ChildTracks, ChildTracks>( cf, "Path/To/Tracks1",
                                                                                    "Path/To/Tracks2" );

  // Extract the guts of the functor from the wrapper that handles the data
  // dependencies, so we can inject them by hand and avoid messing around with
  // Gaudi
  auto const& wrapped = pf.wrapped_functor();

  // Prepare it (in this case we know it provides .prepare() so we don't need
  // to use the detail::prepare() helper)
  EventContext const           evtCtx;
  Functors::TopLevelInfo const top_level;
  auto                         prepared = wrapped.prepare( evtCtx, top_level );

  // Actually call the converter, making sure to add the data dependencies that
  // the wrapper would have injected.
  auto cf_result = prepared( true /* mask */, tracks1, tracks2, parent );

  // Check that the result is correct...
  BOOST_CHECK( all( cf_result == t1.pt() + t2.pt() ) );
}

struct Prepare_Helper : public Functors::Function {
  std::size_t& m_executions;
  std::size_t& m_preparations;
  Prepare_Helper( std::size_t& executions, std::size_t& preparations )
      : m_executions{executions}, m_preparations{preparations} {}

  auto prepare() const {
    m_preparations++;
    return [this]() { this->m_executions++; };
  }
};

BOOST_AUTO_TEST_CASE( test_preparation ) {
  std::size_t               execs{0}, preps{0};
  Functors::Functor<void()> func{Prepare_Helper{execs, preps}};
  BOOST_CHECK( execs == 0 );
  BOOST_CHECK( preps == 0 );
  auto prepped = func.prepare();
  BOOST_CHECK( preps == 1 );
  BOOST_CHECK( execs == 0 );
  prepped();
  BOOST_CHECK( preps == 1 );
  BOOST_CHECK( execs == 1 );
  prepped();
  BOOST_CHECK( preps == 1 );
  BOOST_CHECK( execs == 2 );
  func();
  BOOST_CHECK( preps == 2 );
  BOOST_CHECK( execs == 3 );
}

struct PrepareWithTopLevel : public Functors::Function {
  auto prepare( Functors::TopLevelInfo const& top_level ) const {
    return [ptr = top_level.algorithm()]() { return ptr; };
  }
};

BOOST_AUTO_TEST_CASE( test_toplevelinfo_prepare ) {
  auto                                         parent = reinterpret_cast<Gaudi::Algorithm*>( 0xdeadbeef );
  Functors::Functor<Gaudi::Algorithm const*()> func{PrepareWithTopLevel{}};
  func.bind( parent );
  BOOST_CHECK( func() == parent );
}

// Define some basic particle-like types
struct Charged {
  int   nHits() const { return 5; }
  float pt() const { return 7.f; }
};

struct Composite {
  float pt() const { return 8.f; }
};

// And a generic version
using Particle = std::variant<Charged, Composite>;

BOOST_AUTO_TEST_CASE( test_variants ) {
  // Make some different kinds of particles
  Particle p_charged   = Charged{};
  Particle p_composite = Composite{};

  // Use std::any so we can check what the type is without conversions
  Functors::Functor<std::any( Particle const& )> func{Functors::Track::TransverseMomentum{}};

  // Exercise calling functors as visitors to variants
  auto charged_res   = func( p_charged );
  auto composite_res = func( p_composite );

  // The return type should be the common type to the different variant
  // members' pt() accessor return types, i.e. float
  BOOST_CHECK( func.rtype() == typeid( float ) );
  BOOST_CHECK( charged_res.type() == typeid( float ) );
  BOOST_CHECK( composite_res.type() == typeid( float ) );

  // Check the hardcoded values too
  BOOST_CHECK( std::any_cast<float>( charged_res ) == Charged{}.pt() );
  BOOST_CHECK( std::any_cast<float>( composite_res ) == Composite{}.pt() );
}

namespace detail {
  /** This test and these adapters are examples of how things could be done.
   *  If this functionality is useful, the adapters could be published in, for
   *  example, Adapters.h
   */
  template <typename F, typename... AllowedTypes>
  struct Adapter : public std::conditional_t<Functors::detail::is_functor_predicate_v<F>, Functors::Predicate,
                                             Functors::Function> {
    Adapter( F f ) : m_f{std::move( f )} {}

    /** Forward to the contained functor
     */
    void bind( Functors::TopLevelInfo& top_level ) { Functors::detail::bind( m_f, top_level ); }

    /** Prepare the contained functor and bake it into a new lambda.
     */
    auto prepare( EventContext const& evtCtx, Functors::TopLevelInfo const& top_level ) const {
      // Figure out what the common return type is for invoking the prepared
      // functor with the different permitted types (AllowedTypes...)
      using ret_t = std::common_type_t<std::invoke_result_t<Functors::detail::prepared<F>, AllowedTypes>...>;
      return [this, f = Functors::detail::prepare( m_f, evtCtx, top_level )]( auto const& part ) -> ret_t {
        // If a functor is called with a variant type then the functor is used
        // as a visitor on that type. If a particular functor is only valid for
        // a subset of the types then -- without an adapter -- this would lead
        // to compilation errors. This helper will be invoked for *all* members
        // of the variant, but it only forwards to the contained functors for a
        // subset of those members.
        using arg_t = std::decay_t<decltype( part )>;
        if constexpr ( ( std::is_same_v<arg_t, AllowedTypes> || ... ) ) {
          return std::invoke( f, part );
        } else {
          auto const our_name = Functors::detail::get_name( *this );
          throw GaudiException{our_name + " blocked call with type " + System::typeinfoName( typeid( arg_t ) ),
                               our_name, StatusCode::FAILURE};
        }
      };
    }

  private:
    F m_f;
  };

  template <typename F_allowed, typename F_default, typename... AllowedTypes>
  struct DefaultAdapter : public std::conditional_t<Functors::detail::is_functor_predicate_v<F_allowed>,
                                                    Functors::Predicate, Functors::Function> {
    static_assert( Functors::detail::is_functor_predicate_v<F_allowed> ==
                   Functors::detail::is_functor_predicate_v<F_default> );
    DefaultAdapter( F_allowed f_allowed, F_default f_default )
        : m_f_allowed{std::move( f_allowed )}, m_f_default{std::move( f_default )} {}

    /** Forward to the contained functors
     */
    void bind( Functors::TopLevelInfo& top_level ) {
      Functors::detail::bind( m_f_allowed, top_level );
      Functors::detail::bind( m_f_default, top_level );
    }

    /** Prepare the contained functors and bake them into a new lambda.
     */
    auto prepare( EventContext const& evtCtx, Functors::TopLevelInfo const& top_level ) const {
      return [f_allowed = Functors::detail::prepare( m_f_allowed, evtCtx, top_level ),
              f_default = Functors::detail::prepare( m_f_default, evtCtx, top_level )]( auto const& part ) {
        if constexpr ( ( std::is_same_v<std::decay_t<decltype( part )>, AllowedTypes> || ... ) ) {
          return std::invoke( f_allowed, part );
        } else {
          return std::invoke( f_default, part );
        }
      };
    }

  private:
    F_allowed m_f_allowed;
    F_default m_f_default;
  };
} // namespace detail

template <typename... AllowedTypes, typename F>
auto Adapter( F&& f ) {
  return detail::Adapter<F, AllowedTypes...>( std::forward<F>( f ) );
}

template <typename... AllowedTypes, typename F_allowed, typename F_default>
auto DefaultAdapter( F_allowed&& f_allowed, F_default&& f_default ) {
  return detail::DefaultAdapter<F_allowed, F_default, AllowedTypes...>( std::forward<F_allowed>( f_allowed ),
                                                                        std::forward<F_default>( f_default ) );
}

BOOST_AUTO_TEST_CASE( test_variant_adapters ) {
  // Prepare inputs
  Particle p_charged   = Charged{};
  Particle p_composite = Composite{};

  // This:
  //  Functors::Functor<int( Particle const& )> func{Functors::Track::nHits{}};
  // wouldn't compile because one member of the variant (Composite) doesn't
  // have the nHits() accessor demanded by the functor.

  // Use an adapter to specify which members of the variant are OK
  Functors::Functor<int( Particle const& )> func{Adapter<Charged>( Functors::Track::nHits{} )};

  // Use the adapter on a charged Particle, this should work:
  BOOST_CHECK( func( p_charged ) == Charged{}.nHits() );

  // Using the Charged adapter on a Composite Particle should produce a runtime error
  try {
    func( p_composite );
    BOOST_CHECK( false ); // should never get here
  } catch ( GaudiException& e ) {
    BOOST_CHECK( e.message() == "detail::Adapter<Functors::Track::nHits,Charged> blocked call with type Composite" );
  }

  // Use an adapter that has a fallback instead of throwing a runtime error
  auto                                      default_nhits = 0;
  Functors::Functor<int( Particle const& )> func_default{
      DefaultAdapter<Charged>( Functors::Track::nHits{}, Functors::detail::promote( default_nhits ) )};

  BOOST_CHECK( func_default( p_charged ) == Charged{}.nHits() );
  BOOST_CHECK( func_default( p_composite ) == default_nhits );

  // Try with a cut instead
  Functors::Functor<bool( Particle const& )> cut{
      DefaultAdapter<Charged>( Functors::Track::nHits{} > 3, Functors::AcceptNone{} )};
  BOOST_CHECK( cut( p_charged ) );
  BOOST_CHECK( !cut( p_composite ) );

  // And check that the adapters don't choke on non-variant input, even though
  // in this case the adapters are pointless overhead that should be dropped
  // from the configuration
  BOOST_CHECK( func( Charged{} ) == Charged{}.nHits() );
  BOOST_CHECK_THROW( func( Composite{} ), GaudiException );
  BOOST_CHECK( func_default( Charged{} ) == Charged{}.nHits() );
  BOOST_CHECK( func_default( Composite{} ) == default_nhits );
  BOOST_CHECK( cut( Charged{} ) );
  BOOST_CHECK( !cut( Composite{} ) );
}

BOOST_AUTO_TEST_CASE( test_bitwise_and_or ) {
  using VoidPredicate  = Functors::Functor<bool()>;
  using BitwiseFunctor = Functors::Functor<int()>;

  // 'Reference' value which we'll check for set bits
  auto           ref = Functors::detail::promote( 0b1010 );
  VoidPredicate  f_pass{Functors::math::test_bit( ref, 0b0010 )};
  VoidPredicate  f_fail{Functors::math::test_bit( ref, 0b0100 )};
  BitwiseFunctor f_and{Functors::bitwise_and( ref, 0b0101 )};
  BitwiseFunctor f_or{Functors::bitwise_or( ref, 0b0101 )};

  BOOST_CHECK( f_pass() == true );
  BOOST_CHECK( f_fail() == false );
  BOOST_CHECK( f_and() == 0b0000 );
  BOOST_CHECK( f_or() == 0b1111 );
}

namespace {
  template <typename T>
  constexpr auto approx_equal( T const& a, T const& b, float tolerance = std::numeric_limits<float>::epsilon() ) {
    using std::abs;
    using std::max;
    auto const diff = abs( a - b );
    return ( diff < tolerance ) || ( diff < max( abs( a ), abs( b ) ) );
  }
} // namespace

BOOST_AUTO_TEST_CASE( test_v2_particle_functors ) {
  constexpr auto ncomposites = 18;
  constexpr auto ntracks     = ncomposites + 2;
  // Produce the children
  auto const input1 = LHCb::Pr::Fitted::Forward::generate_tracks( ntracks, 0 );
  auto const input2 = LHCb::Pr::Fitted::Forward::generate_tracks( ntracks, 1 );
  // Iterable versions for convenience
  auto const iterin1 = LHCb::Pr::make_zip( input1 );
  auto const iterin2 = LHCb::Pr::make_zip( input2 );
  // Get the plain integers for convenience
  auto const input1_family_id = int{input1.zipIdentifier()};
  auto const input2_family_id = int{input2.zipIdentifier()};
  // Generate some composites
  using int_v   = SIMDWrapper::scalar::int_v;
  using float_v = SIMDWrapper::scalar::float_v;
  LHCb::v2::Composites threebody{3};

  for ( auto i = 0; i < ncomposites; ++i ) {
    std::array<int_v, 3> child_indices{i, i + 1, 0},
        child_zip_ids{input1_family_id, input2_family_id, input1_family_id};
    threebody.emplace_back<float_v, int_v>(
        {} /* pos */, {i * 1.f, i * 2.f, 0.f, 0.f} /* p4 */, 42 /* pid */, 3.f /* chi2 */, 2 /* ndof */,
        {} /* pos_cov */, {0.f, 0.f, 0.f, 0.f, 0.f, 0.f, 0.f, 0.f, 0.f, 0.f} /* p4_cov */,
        {0.f, 0.f, 0.f, 0.f, 0.f, 0.f, 0.f, 0.f, 0.f, 0.f, 0.f, 0.f} /* mom_pos_cov */, child_indices, child_zip_ids );
  }
  BOOST_CHECK( threebody.size() == ncomposites );

  // Functor we want to apply to the combination -- scalar sum of pT
  auto const SUMPT = Functors::Adapters::Accumulate{Functors::Track::TransverseMomentum{}};

  // Set up the functor that should recover the combination of these tracks
  // from 'parent'...but this includes some magic to get the child containers
  // from the given TES locations, so we can't easily run it here. For now
  // we just check that creating it works.
  auto const pf = Functors::Adapters::CombinationFromComposite<LHCb::Pr::Fitted::Forward::Tracks,
                                                               LHCb::Pr::Fitted::Forward::Tracks>(
      SUMPT, "Path/To/Tracks1", "Path/To/Tracks2" );

  // Extract the guts of the functor from the wrapper that handles the data
  // dependencies, so we can inject them by hand and avoid messing around with
  // Gaudi
  auto const& wrapped = pf.wrapped_functor();

  // Prepare it (in this case we know it provides .prepare() so we don't need
  // to use the detail::prepare() helper)
  EventContext const           evtCtx;
  Functors::TopLevelInfo const top_level;
  auto                         prepared = wrapped.prepare( evtCtx, top_level );

  for ( auto composite : threebody.simd() ) {
    // Actually call the converter, making sure to add the data dependencies that
    // the wrapper would have injected.
    auto const cf_result = prepared( composite.loop_mask(), input1, input2, composite );

    // Get the tracks by hand, so we can check the functor works properly
    auto const& t1 = iterin1[composite.offset()];
    auto const& t2 = iterin2[composite.offset() + 1];
    auto const& t3 = iterin1.gather( 0 ); // broadcasting

    // Check that the result is correct...
    auto const mask = approx_equal( cf_result, decltype( cf_result )( t1.pt() + t2.pt() + t3.pt() ) );
    BOOST_CHECK( none( composite.loop_mask() && !mask ) );
  }
}

bool random_bool() {
  static std::default_random_engine engine{42};
  return std::uniform_int_distribution<int>{0, 1}( engine );
}

namespace Functors {
  template <typename Operator>
  struct Op_Count : public Predicate {
    float        m_threshold;
    std::size_t &m_counter, &m_scalar_counter;
    Op_Count( std::size_t& counter, std::size_t& scalar_counter, float threshold )
        : m_threshold{threshold}, m_counter{counter}, m_scalar_counter{scalar_counter} {}
    template <typename Data>
    auto operator()( bool mask, Data const& x ) const {
      m_scalar_counter += mask;
      if ( mask ) {
        return Operator{}( x, m_threshold );
      } else {
        // Return random results in slots that are invalid according to 'mask'
        return random_bool();
      }
    }

    template <typename mask_v, typename float_v>
    auto operator()( mask_v const& mask, float_v const& x ) const {
      m_counter += popcount( mask );
      auto const                         result = Operator{}( x, m_threshold );
      std::array<float, float_v::size()> data_to_generate_mask{};
      for ( auto i = 0ul; i < float_v::size(); ++i ) {
        data_to_generate_mask[i] = testbit( mask, i ) ? testbit( result, i ) : random_bool();
      }
      // 'result' where 'mask' was true, random results where 'mask' was false
      return float_v{data_to_generate_mask.data()} > float_v{0.5f};
    }
    constexpr static bool requires_explicit_mask = true;
  };
} // namespace Functors

BOOST_AUTO_TEST_CASE( test_mask_logic ) {
  // Test the short-circuiting behaviour of functors that use explicit masks by
  // generating a large number of combinations of input data and checking that
  // scalar (assumed correct...) and vector backends return the same results
  // and invoke the "calculations" with the same total number of positive mask
  // bits
  using simd_t = SIMDWrapper::sse::types; // vector width 4 is enough...
  std::size_t x1_counter{}, x1_counter_scalar{}, x2_counter{}, x2_counter_scalar{}, x4_counter{}, x4_counter_scalar{},
      x7_counter{}, x7_counter_scalar{};
  Functors::Op_Count<std::less<>>     X1{x1_counter, x1_counter_scalar, 0.5f};
  Functors::Op_Count<std::equal_to<>> X2{x2_counter, x2_counter_scalar, 2.f};
  auto const                          X3 = X1 | X2;
  Functors::Op_Count<std::greater<>>  X4{x4_counter, x4_counter_scalar, 2.5f};
  auto const                          X5 = X1 | X2 | X4;
  auto const                          X6 = X1 | ( ~X2 & X4 );
  Functors::Op_Count<std::greater<>>  X7{x7_counter, x7_counter_scalar, 0.5f};
  auto const                          X8 = X2 & ( X7 | X1 );
  EventContext const                  evtCtx;
  Functors::TopLevelInfo const        top_level;
  auto const                          X3_prepared  = X3.prepare( evtCtx, top_level );
  auto const                          X5_prepared  = X5.prepare( evtCtx, top_level );
  auto const                          X6_prepared  = X6.prepare( evtCtx, top_level );
  auto const                          X8_prepared  = X8.prepare( evtCtx, top_level );
  auto const                          check_counts = [&]() {
    BOOST_CHECK_EQUAL( x1_counter, x1_counter_scalar );
    BOOST_CHECK_EQUAL( x2_counter, x2_counter_scalar );
    BOOST_CHECK_EQUAL( x4_counter, x4_counter_scalar );
    BOOST_CHECK_EQUAL( x7_counter, x7_counter_scalar );
  };

  // Generate all possible masks for simd_t
  std::array<float, simd_t::size> data_to_generate_mask{}, data_to_generate_float{};
  for ( auto i = 0; i < std::pow( 2, simd_t::size ); ++i ) {
    for ( auto j = 0ul; j < simd_t::size; ++j ) { data_to_generate_mask[simd_t::size - 1 - j] = ( i & ( 0x1 << j ) ); }
    auto const mask = simd_t::float_v{data_to_generate_mask.data()} > 0.5f;

    // Generate some float_v vectors we can use as input, based on the
    // thresholds hardcoded in the test functors (X{1,2,...} above) then
    // [0, 1, 2, 3] are the only relevant "float" values -- generate all
    // possible vectors of simd_t::size (4) floats with the elements taking
    // these values
    for ( auto j = 0; j < std::pow( 2ul, 2 * simd_t::size ); ++j ) {
      for ( auto l = 0ul; l < simd_t::size; ++l ) { data_to_generate_float[l] = ( j >> 2 * l ) & 0x3; }
      auto const data = simd_t::float_v{data_to_generate_float.data()};
      // Do some checks using 'mask' and 'data', vector-wise and scalar
      auto const do_tests = [&]( auto const& obj ) {
        auto const vector_res = obj( mask, data );
        for ( auto l = 0ul; l < simd_t::size; ++l ) {
          // Only compare parts of the result that were 'valid' in 'mask'
          // If the relevant part of 'mask' was zero, then the functor was free
          // to return any value there.
          if ( testbit( mask, l ) ) {
            auto const scalar_res = obj( true, data_to_generate_float[l] );
            BOOST_CHECK_EQUAL( testbit( vector_res, l ), scalar_res );
          }
        }
      };
      do_tests( X1 );
      do_tests( X2 );
      check_counts();
      do_tests( X3_prepared );
      check_counts();
      do_tests( X4 );
      check_counts();
      do_tests( X5_prepared );
      check_counts();
      do_tests( X6_prepared );
      check_counts();
      do_tests( X7 );
      check_counts();
      do_tests( X8_prepared );
      check_counts();
    }
  }
  check_counts();
}
