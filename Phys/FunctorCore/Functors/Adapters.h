/*****************************************************************************\
* (c) Copyright 2019-20 CERN for the benefit of the LHCb Collaboration        *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include "Functors/Function.h"
#include "Functors/Utilities.h"
#include "SelKernel/ParticleCombination.h"

#include <boost/mp11/algorithm.hpp>

/** @file  Adapters.h
 *  @brief Functors that adapt object-wise functors (e.g. PT) into
 *         collection-wise functors (e.g. scalar-sum-of-PT in CombinationCut)
 */
namespace Functors::Adapters {
  template <typename F>
  struct Accumulate : public Function {
    /** Flag that this adapter expects to be invoked with an explicit mask as
     *  its first argument.
     */
    constexpr static bool requires_explicit_mask = true;
    static_assert( detail::is_functor_function_v<F>, "Functors::Adapters::Accumulate must wrap a functor" );

    Accumulate( F f ) : m_f{std::move( f )} {}

    /* Improve error messages. */
    constexpr auto name() const { return "Accumulate( " + detail::get_name( m_f ) + " )"; }

    void bind( TopLevelInfo& top_level ) { detail::bind( m_f, top_level ); }

    auto prepare( EventContext const& evtCtx, TopLevelInfo const& top_level ) const {
      return [f = detail::prepare( m_f, evtCtx, top_level )]( auto const& mask, auto const& collection ) {
        return Sel::transform_reduce(
            collection, [&f, &mask]( auto const& p ) { return std::invoke( f, mask_arg, mask, p ); }, std::plus<>{} );
      };
    }

  private:
    F m_f;
  };

  template <typename F>
  struct Minimum : public Function {
    /** Flag that this adapter expects to be invoked with an explicit mask as
     *  its first argument.
     */
    constexpr static bool requires_explicit_mask = true;

    static_assert( detail::is_functor_function_v<F>, "Functors::Adapters::Minimum must wrap a functor" );

    Minimum( F f ) : m_f{std::move( f )} {}

    /* Improve error messages. */
    constexpr auto name() const { return "Minimum( " + detail::get_name( m_f ) + " )"; }

    void bind( TopLevelInfo& top_level ) { detail::bind( m_f, top_level ); }

    auto prepare( EventContext const& evtCtx, TopLevelInfo const& top_level ) const {
      return [f = detail::prepare( m_f, evtCtx, top_level )]( auto const& mask, auto const& collection ) {
        return Sel::transform_reduce(
            collection, [&f, &mask]( auto const& p ) { return std::invoke( f, mask_arg, mask, p ); },
            []( auto const& a, auto const& b ) {
              using std::min;
              return min( a, b );
            } );
      };
    }

  private:
    F m_f;
  };

  template <typename F>
  struct Maximum : public Function {
    /** Flag that this adapter expects to be invoked with an explicit mask as
     *  its first argument.
     */
    constexpr static bool requires_explicit_mask = true;

    static_assert( detail::is_functor_function_v<F>, "Functors::Adapters::Maximum must wrap a functor" );

    Maximum( F f ) : m_f{std::move( f )} {}

    /* Improve error messages. */
    constexpr auto name() const { return "Maximum( " + detail::get_name( m_f ) + " )"; }

    void bind( TopLevelInfo& top_level ) { detail::bind( m_f, top_level ); }

    auto prepare( EventContext const& evtCtx, TopLevelInfo const& top_level ) const {
      return [f = detail::prepare( m_f, evtCtx, top_level )]( auto const& mask, auto const& collection ) {
        return Sel::transform_reduce(
            collection, [&f, &mask]( auto const& p ) { return std::invoke( f, mask_arg, mask, p ); },
            []( auto const& a, auto const& b ) {
              using std::max;
              return max( a, b );
            } );
      };
    }

  private:
    F m_f;
  };
} // namespace Functors::Adapters

namespace Functors::detail {
  inline int zipFamilyNumber_to_int( Zipping::ZipFamilyNumber zip_id ) { return int{zip_id}; }
  template <typename T>
  inline T zipFamilyNumber_to_int( T zip_id ) {
    return zip_id;
  }

  template <typename T>
  using simd_or_ = std::integral_constant<SIMDWrapper::InstructionSet, T::simd>;

  template <typename T>
  inline constexpr auto simd_or_scalar_v =
      Gaudi::cpp17::detected_or_t<std::integral_constant<SIMDWrapper::InstructionSet, SIMDWrapper::Scalar>, simd_or_,
                                  T>::value;

  template <typename mask_t, typename proxy_t, typename... child_containers_t>
  struct child_iterator {
    static_assert( ( LHCb::Pr::is_proxy_v<typename child_containers_t::value_type> && ... ),
                   "Functors::detail::child_iterator doesn't support non-proxy child types yet." );
    using child_proxy_t =
        boost::mp11::mp_unique<Sel::Utils::variant<typename child_containers_t::gather_value_type...>>;
    child_iterator( mask_t const& mask, proxy_t const& proxy, unsigned int child,
                    std::tuple<child_containers_t...> const& child_containers )
        : m_mask{mask}, m_proxy{proxy}, m_child_containers{child_containers}, m_child{child} {
      using Sel::Utils::none; // in case m_mask is bool
      if ( none( m_mask ) ) {
        throw GaudiException{"Received empty mask", "Functors::detail::child_iterator", StatusCode::FAILURE};
      }
    }
    child_iterator( child_iterator const& ) = default;
    child_iterator& operator                =( child_iterator const& rhs ) {
      if ( UNLIKELY( ( &m_mask != &rhs.m_mask ) || ( &m_proxy != &rhs.m_proxy ) ||
                     ( &m_child_containers != &rhs.m_child_containers ) ) ) {
        throw GaudiException{"Attempt to assign iterator with an iterator from a different combination",
                             "Functors::detail::child_iterator", StatusCode::FAILURE};
      }
      m_child = rhs.m_child;
      return *this;
    }
    friend bool operator==( child_iterator const& lhs, child_iterator const& rhs ) {
      // This comparison is important for iterating over a span of children,
      // where both operands should be referring to the same underlying
      // containers and pointer comparison of the referred-to objects is
      // appropriate.
      return ( lhs.m_child == rhs.m_child ) && ( &lhs.m_mask == &rhs.m_mask ) && ( &lhs.m_proxy == &rhs.m_proxy ) &&
             ( &lhs.m_child_containers == &rhs.m_child_containers );
    }
    friend bool     operator!=( child_iterator const& lhs, child_iterator const& rhs ) { return !( lhs == rhs ); }
    child_iterator& operator++() {
      ++m_child;
      return *this;
    }
    child_proxy_t operator*() const {
      // Get the [chunk of] family IDs that we need to match up with the
      // containers listed in m_child_containers
      // {id0, id1, ..., idN-1} for SIMD width N
      auto const family_ids = zipFamilyNumber_to_int( m_proxy.childRelationFamily( m_child ) );
      // Get the family IDs of the containers we know about
      // {id0, id1, ..., idM-1} for M containers
      auto const container_ids = std::apply(
          []( auto const&... child_containers ) {
            return std::array{zipFamilyNumber_to_int( child_containers.zipIdentifier() )...};
          },
          m_child_containers );
      // In case we're dealing with plain bool
      using Sel::Utils::all;
      using Sel::Utils::hmax;
      using Sel::Utils::hmin;
      using Sel::Utils::select;
      // Get the indices in m_child_containers corresponding to each family_ids
      // {ind0, ind1, ..., indN-1}
      mask_t                               found_mask = !m_mask; // start with ones in the invalid entries
      std::decay_t<decltype( family_ids )> container_inds{};
      for ( auto i = 0; i < num_child_containers; ++i ) {
        auto const mask_i = ( family_ids == container_ids[i] );
        found_mask        = found_mask || mask_i;
        container_inds    = select( mask_i, i, container_inds );
      }
      // Check that we found an index for every child
      if ( !all( found_mask ) ) {
        using GaudiUtils:: operator<<; // Enable pretty-printing of std::array
        std::ostringstream oss;
        oss << "Couldn't find valid child container: m_child = " << m_child << ", mask = " << m_mask
            << ", family_ids = " << family_ids << " (" << System::typeinfoName( typeid( family_ids ) )
            << "), container_ids = " << container_ids;
        throw GaudiException{oss.str(), "Functors::detail::child_iterator", StatusCode::FAILURE};
      }
      // Get the indices of the children
      // {off0, off1, ..., offN-1}
      auto const indices = m_proxy.childRelationIndex( m_child );
      // Now construct some object x so that x.pt() expands to
      // {std::get< ind0 >( m_child_containers )[ off0 ].pt(),
      //  ...,
      //  std::get<indN-1>( m_child_containers )[offN-1].pt()}
      // Note that in general these can be heterogeneous types -- that should
      // be supported in principle, but it's not going to be the normal
      // use-case. We can normally arrange for m_proxy to represent something
      // like the D0 in D0 -> KS0 K- pi+, and for the KS0 to always be child 0,
      // which translates here to family_ids being filled with a single value,
      // and consequently container_ids being filled with the same value. This
      // is good, because it means that we can do something like
      //   std::get<ind>( m_child_containers ).gather([off0, ..., offN-1]).pt()
      // which is "just" a gather on "the pt column" in a single container
      auto min_ind = hmin( container_inds, m_mask );
      auto max_ind = hmax( container_inds, m_mask );
      if ( min_ind != max_ind ) {
        // In principle we could support this...but it's not a priority
        throw GaudiException{"Having Nth children of heterogeneous types is not yet supported.",
                             "Functors::detail::child_iterator", StatusCode::FAILURE};
      }
      // This should be guaranteed by the loop (with 'found_mask') above
      assert( min_ind >= 0 && min_ind < int( sizeof...( child_containers_t ) ) );
      return get_proxy( min_ind, select( m_mask, indices, hmin( indices, m_mask ) /* avoid out-of-bounds access */ ),
                        std::index_sequence_for<child_containers_t...>{} );
    }

  private:
    template <typename int_v, std::size_t N, std::size_t... Ns>
    child_proxy_t get_proxy( int index_of_container, int_v const& indices_in_container,
                             std::index_sequence<N, Ns...> ) const {
      if ( index_of_container == N ) {
        return std::get<N>( m_child_containers ).template gather<simd_or_scalar_v<proxy_t>>( indices_in_container );
      }
      if constexpr ( sizeof...( Ns ) == 0 ) {
        // Need a throw/return statement to avoid control reaching the end of
        // a non-void function. This should never happen...
        throw GaudiException{"Requested container index " + std::to_string( index_of_container ) + " was out of range",
                             "Functors::detail::child_iterator", StatusCode::FAILURE};
      } else {
        return get_proxy( index_of_container, indices_in_container, std::index_sequence<Ns...>{} );
      }
    }

    mask_t const&                            m_mask;
    proxy_t const&                           m_proxy;
    std::tuple<child_containers_t...> const& m_child_containers;
    unsigned int                             m_child;
    constexpr static int                     num_child_containers{sizeof...( child_containers_t )};
  };

  template <typename F, typename... child_containers_t>
  struct CombinationFromComposite : public std::conditional_t<is_functor_predicate_v<F>, Predicate, Function> {
    /** Flag that this adapter expects to be invoked with an explicit mask as
     *  its first argument.
     */
    static constexpr bool requires_explicit_mask = true;

    CombinationFromComposite( F f ) : m_f{std::move( f )} {}

    /* Improve error messages. */
    constexpr auto name() const { return "CombinationFromComposite( " + get_name( m_f ) + " )"; }

    void bind( TopLevelInfo& top_level ) { detail::bind( m_f, top_level ); }

    auto prepare( EventContext const& evtCtx, TopLevelInfo const& top_level ) const {
      // The DataDepWrapper we are owned by injects the data dependencies that
      // ChildContainers contains the types of as the first arguments (after
      // the mask) when calling the lambda that we return here.
      return [f = detail::prepare( m_f, evtCtx, top_level )](
                 auto const& mask, child_containers_t const&... child_containers, auto const& composite ) {
        // This adapter doesn't really make sense with input a variadic
        // parameter pack, so we hardcode that it's a single item
        // (i.e. 'composite' is not a variadic pack).

        // First, reconstruct some kind of combination object from the input
        using composite_t = std::decay_t<decltype( composite )>;
        // Make sure all the child containers are iterable, and that these
        // iterable versions have sufficiently long lifetimes. The
        // transient combination object will hold a reference to this tuple,
        // so its important that it stays valid until the evaluation of 'f'
        // has completed. Also make sure that we are using the same vector
        // stride for the composite and the child containers.
        std::tuple iterable_child_containers{LHCb::Pr::make_zip<simd_or_scalar_v<composite_t>>( child_containers )...};
        // Create some kind of combination object representing a collection of
        // composite.numChildren() particles, which are handled as if they are a
        // variant of all the value types in 'child_containers'
        // The number of children is a runtime value, so we cannot return
        // array<variant<...>, N>. and vector<variant<...>> is ugly...
        // TODO: change to use some lightweight sentinel type for
        //       'end_child_iter' instead of the relatively heavy
        //       child_iterator
        child_iterator child_iter{mask, composite, 0, iterable_child_containers},
            end_child_iter{mask, composite, static_cast<unsigned>( composite.numChildren() ),
                           iterable_child_containers};
        return std::invoke( f, mask_arg, mask, Sel::ParticleCombinationSpan{child_iter, end_child_iter} );
      };
    }

  private:
    F m_f;
  };
} // namespace Functors::detail

namespace Functors::Adapters {
  /** Adapter to re-assemble the combination of children that a composite is
   *  formed from. Using the language of 'CombineParticles', that means that
   *  applying ADAPTER(COMBINATION_CUT, ...) as a 'MotherCut', where
   *  COMBINATION_CUT would have been a valid 'CombinationCut'. Because the
   *  composite particle only knows a (family, index) pair about its own
   *  children, the '...' must include a input container for each family. This
   *  also means that you will need to tell the functor the explicit type of
   *  this input container.
   *
   *  The helper function allows us to avoid specifying any more types than is
   *  strictly necessary.
   */
  template <typename... ChildContainers, typename F, typename... CArgs>
  auto CombinationFromComposite( F f, CArgs&&... args ) {
    // 'F'    is the wrapped functor (COMBINATION_CUT)
    // 'args' is the list of data locations where child data can be found
    // The composite itself will be passed as the argument to the adapter's
    // function call operator.
    return detail::add_data_deps<std::conditional_t<detail::is_functor_predicate_v<F>, Predicate, Function>,
                                 ChildContainers...>(
        detail::CombinationFromComposite<F, ChildContainers...>{std::move( f )}, std::forward<CArgs>( args )... );
  }

  /** @class ConvertToPOD
   *  @brief Simple adapter that calls .cast() on its argument.
   *  This is useful when outputing the result of a calculation that uses a
   *  some kind of numeric wrapper (e.g. SIMDWrapper, but it doesn't have to
   *  be) to some destination that only understands plain types (e.g. a ROOT
   *  TTree). Clearly this only makes sense if it has been arranged elsewhere
   *  that the functor return value is conceptually a scalar arithmetic value.
   */
  template <typename F>
  struct ConvertToPOD : public Function {
    /** Flag that this adapter expects to be invoked with an explicit mask as
     *  its first argument.
     */
    static constexpr bool requires_explicit_mask = true;

    static_assert( detail::is_functor_function_v<F>, "Functors::Adapters::ConvertToPOD must wrap a functor" );

    ConvertToPOD( F f ) : m_f{std::move( f )} {}

    void bind( TopLevelInfo& top_level ) { detail::bind( m_f, top_level ); }

    auto prepare( EventContext const& evtCtx, TopLevelInfo const& top_level ) const {
      return [f = detail::prepare( m_f, evtCtx, top_level )]( auto const& mask, auto&&... args ) {
        return Sel::Utils::as_arithmetic( std::invoke( f, mask_arg, mask, std::forward<decltype( args )>( args )... ) );
      };
    }

  private:
    F m_f;
  };
} // namespace Functors::Adapters
