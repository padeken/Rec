/*****************************************************************************\
* (c) Copyright 2019-20 CERN for the benefit of the LHCb Collaboration        *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include "Functors/Core.h"
#include "Functors/FunctorDesc.h"
#include "GaudiKernel/System.h"
#include "GaudiKernel/extend_interfaces.h"

namespace Gaudi {
  class Algorithm;
} // namespace Gaudi

namespace Functors {
  /** @file  IFactory.h
   *  @brief Interface to the service that JIT-compiles functors or loads them from the cache.
   *
   *  This defines the Functors::IFactory interface.
   */

  /** @class IFactory
   *  @brief Interface for turning strings into Functor<Out(In)> instances.
   */
  struct IFactory : extend_interfaces<IInterface> {
  protected:
    enum CompilationBehaviourBit { TryCache = 0x1, TryJIT = 0x2, ExceptionOnFailure = 0x4 };

  public:
    /** Enum to flag whether the requested functor should be obtained from the
     *  functor cache, by JIT compilation, or either (the default). Also
     *  specifies whether or not failure should result in an exception or a
     *  null functor object being returned. Note that if all backends are
     *  disabled then a null functor object will always be returned and no
     *  exception will be raised.
     */
    enum CompilationBehaviour {
      CacheOrJIT     = TryCache | TryJIT | ExceptionOnFailure,
      CacheOnly      = TryCache | ExceptionOnFailure,
      JITOnly        = TryJIT | ExceptionOnFailure,
      QuietCacheOnly = TryCache,
      QuietJITOnly   = TryJIT
    };

    /** Default combination behaviour
     */
    static constexpr CompilationBehaviour DefaultCompilationBehaviour = CacheOrJIT;

  protected:
    using functor_base_t                     = std::unique_ptr<Functors::AnyFunctor>;
    constexpr static auto functor_base_t_str = "std::unique_ptr<Functors::AnyFunctor>";
    /** Implementation method that gets an input/output-type-agnostic std::unique_ptr<AnyFunctor>
     *  object from either cling or the cache.
     */
    virtual functor_base_t get_impl( Gaudi::Algorithm* owner, std::string_view functor_type,
                                     ThOr::FunctorDesc const& desc, CompilationBehaviour ) = 0;

  public:
    DeclareInterfaceID( IFactory, 1, 0 );

    /** Factory method to get a C++ functor object from a string, either from
     *  the cache or using JIT compilation.
     *
     * @param  owner   The algorithm that owns the functor, this is needed to
     *                 set up the functor's data dependencies correctly.
     * @param  desc    ThOr::FunctorDesc object holding the functor code, list
     *                 of headers required to compile it and "pretty"
     *                 representation.
     * @param  compile CompilationBehaviour enum value specifying what should
     *                 be tried when compiling this functor. By default the
     *                 functor cache will be tried first, and the factory will
     *                 fall back on JIT compilation. This does not override the
     *                 global settings of the factory.
     * @tparam FType   Functor<Out(In)> type that will be returned. This
     *                 specifies precisely how the functor is instantiated.
     * @return         Functor<Out(In)> object of the given type, may be empty.
     */
    template <typename FType>
    FType get( Gaudi::Algorithm* owner, ThOr::FunctorDesc const& desc,
               CompilationBehaviour compile = DefaultCompilationBehaviour ) {
      auto any_functor = get_impl( owner, System::typeinfoName( typeid( FType ) ), desc, compile );
      if ( any_functor ) {                                          // check the unique_ptr<AnyFunctor> isn't empty
        auto ftype_ptr = dynamic_cast<FType*>( any_functor.get() ); // cast AnyFunctor* -> FType* (base -> derived)
        if ( ftype_ptr ) {                                          // check the AnyFunctor -> Functor conversion was OK
          return std::move( *ftype_ptr ); // move the contents into the FType we return by value
        } else {
          // This should only happen if you have a bug (e.g. you used a
          // SIMDWrapper type that has a different meaning depending on the
          // compilation flags in the stack/cling). We can't fix that at
          // runtime so let's just fail hard.
          throw GaudiException{"Failed to cast factory return type (" +
                                   System::typeinfoName( typeid( decltype( *any_functor.get() ) ) ) +
                                   ") to desired type (" + System::typeinfoName( typeid( FType ) ) + "), rtype is (" +
                                   System::typeinfoName( any_functor->rtype() ) + ") and it " +
                                   ( any_functor->wasJITCompiled() ? "was" : "was not" ) + " JIT compiled",
                               "Functors::IFactory::get<FType>( owner, desc, compile )", StatusCode::FAILURE};
        }
      }
      // Return an empty FType object. This can happen if e.g. you disabled
      // both cling and the cache, as is done during cache generation, so we
      // should not abort the application...
      return {};
    }
  };
} // namespace Functors
