/*****************************************************************************\
* (c) Copyright 2019-20 CERN for the benefit of the LHCb Collaboration        *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "CombineTracksSIMD.h"

#include "SelKernel/TrackZips.h"

using namespace SelAlgorithms::CombineTracksSIMD;

// Aliases are needed because there are multiple template arguments and the
// commas separating them confuse the DECLARE_COMPONENT_WITH_ID macro.
// Note that the input type is always the same; the convention is that the type
// stored on the TES is a default/best zip, and if (as here) an algorithm is to
// instantiated with different backends then it still takes a default/best zip
// and changes the setting internally

template <std::size_t N>
using CombineTracksSIMD__NBody__PrFittedForwardTracksWithPVs =
    CombineTracksSIMD<LHCb::Pr::Fitted::Forward::TracksWithPVs, N>;

template <std::size_t N>
using CombineTracksSIMD__NBody__PrFittedForwardTracksWithPVs_AVX2 =
    CombineTracksSIMD<LHCb::Pr::Fitted::Forward::TracksWithPVs, N, SIMDWrapper::AVX2>;

template <std::size_t N>
using CombineTracksSIMD__NBody__PrFittedForwardTracksWithPVs_SSE =
    CombineTracksSIMD<LHCb::Pr::Fitted::Forward::TracksWithPVs, N, SIMDWrapper::SSE>;

template <std::size_t N>
using CombineTracksSIMD__NBody__PrFittedForwardTracksWithPVs_Scalar =
    CombineTracksSIMD<LHCb::Pr::Fitted::Forward::TracksWithPVs, N, SIMDWrapper::Scalar>;

DECLARE_COMPONENT_WITH_ID( CombineTracksSIMD__NBody__PrFittedForwardTracksWithPVs<2>,
                           "CombineTracksSIMD__2Body__PrFittedForwardTracksWithPVs" )
DECLARE_COMPONENT_WITH_ID( CombineTracksSIMD__NBody__PrFittedForwardTracksWithPVs<3>,
                           "CombineTracksSIMD__3Body__PrFittedForwardTracksWithPVs" )
DECLARE_COMPONENT_WITH_ID( CombineTracksSIMD__NBody__PrFittedForwardTracksWithPVs_AVX2<2>,
                           "CombineTracksSIMD__2Body__PrFittedForwardTracksWithPVs_AVX2" )
DECLARE_COMPONENT_WITH_ID( CombineTracksSIMD__NBody__PrFittedForwardTracksWithPVs_AVX2<3>,
                           "CombineTracksSIMD__3Body__PrFittedForwardTracksWithPVs_AVX2" )
DECLARE_COMPONENT_WITH_ID( CombineTracksSIMD__NBody__PrFittedForwardTracksWithPVs_SSE<2>,
                           "CombineTracksSIMD__2Body__PrFittedForwardTracksWithPVs_SSE" )
DECLARE_COMPONENT_WITH_ID( CombineTracksSIMD__NBody__PrFittedForwardTracksWithPVs_SSE<3>,
                           "CombineTracksSIMD__3Body__PrFittedForwardTracksWithPVs_SSE" )
DECLARE_COMPONENT_WITH_ID( CombineTracksSIMD__NBody__PrFittedForwardTracksWithPVs_Scalar<2>,
                           "CombineTracksSIMD__2Body__PrFittedForwardTracksWithPVs_Scalar" )
DECLARE_COMPONENT_WITH_ID( CombineTracksSIMD__NBody__PrFittedForwardTracksWithPVs_Scalar<3>,
                           "CombineTracksSIMD__3Body__PrFittedForwardTracksWithPVs_Scalar" )
