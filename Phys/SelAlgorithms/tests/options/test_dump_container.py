###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Gaudi.Configuration import ApplicationMgr
from Functors import (PT, ETA, PHI, POD)
from Functors.utils import pack_dict
from Configurables import (EvtStoreSvc, ProducePrFittedForwardTracks,
                           DumpContainer__PrFittedForwardTracks)

produce = ProducePrFittedForwardTracks(Output='Fake/Tracks')
consume = DumpContainer__PrFittedForwardTracks(
    Input=produce.Output,
    Branches=pack_dict({
        'PT': PT,
        'ETA': ETA,
        'PHI': PHI
    }, wrap=POD),
    VoidBranches={},
    DumpFileName='DumpContainer.root',
    DumpTreeName='DumpTree')

ApplicationMgr().EvtMax = 10
ApplicationMgr().EvtSel = 'NONE'
ApplicationMgr().ExtSvc = [EvtStoreSvc("EventDataSvc")]
ApplicationMgr().TopAlg = [produce, consume]
