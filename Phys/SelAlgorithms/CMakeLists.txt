###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
################################################################################
# Package: SelAlgorithms
################################################################################
gaudi_subdir(SelAlgorithms v1r0)

gaudi_depends_on_subdirs(Phys/FunctorCore
                         Phys/SelKernel)

find_package(Boost)
find_package(ROOT COMPONENTS RIO Tree)
include_directories(SYSTEM ${Boost_INCLUDE_DIRS} ${ROOT_INCLUDE_DIRS})

gaudi_add_module(SelAlgorithms
                 src/*.cpp
                 INCLUDE_DIRS Phys/SelKernel
                 LINK_LIBRARIES ROOT FunctorCoreLib)

gaudi_install_headers(SelAlgorithms)

gaudi_add_test(QMTest QMTEST)
