/*****************************************************************************\
* (c) Copyright 2019-20 CERN for the benefit of the LHCb Collaboration        *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include "Event/State.h"
#include "Event/Zip.h"
#include "Kernel/Variant.h"
#include "LHCbMath/MatVec.h"
#include "SelKernel/ParticleAccessors.h"
#include "SelKernel/ParticleTraits.h"
#include "SelKernel/VectorTraits.h"
#include "TrackKernel/TrackVertexUtils.h"

#include "GaudiKernel/GenericMatrixTypes.h"
#include "GaudiKernel/detected.h"

#include <cassert>

/** @file  Utilities.h
 *  @brief Helper types and functions for selections.
 */

namespace LHCb {
  class Particle;
  class VertexBase;
} // namespace LHCb

namespace Sel::Utils {
  /** Backwards compatibility -- these moved to Kernel/Variant.h in LHCb
   */
  template <typename... Ts>
  using variant = LHCb::variant<Ts...>;

  template <typename T>
  using is_variant = LHCb::is_variant<T>;

  template <typename T>
  inline constexpr bool is_variant_v = LHCb::is_variant_v<T>;

  template <typename... Variants>
  inline constexpr bool are_all_variants_v = LHCb::are_all_variants_v<Variants...>;

  template <typename... Args>
  decltype( auto ) invoke_or_visit( Args&&... args ) {
    return LHCb::invoke_or_visit( std::forward<Args>( args )... );
  }

  /** Backwards compatibility -- this moved to LHCbMath/Utils.h in LHCb
   */
  template <typename Data>
  inline constexpr auto as_arithmetic( Data&& x ) {
    return LHCb::Utils::as_arithmetic( std::forward<Data>( x ) );
  }

  /** Helper to determine if the given type has a static mask_true() method. */
  template <typename T>
  using has_static_mask_true_ = decltype( T::mask_true() );

  template <typename T>
  inline constexpr bool has_static_mask_true_v = Gaudi::cpp17::is_detected_v<has_static_mask_true_, T>;

  /** Define plain bool versions that mirror the functions defined for
   *  SIMDWrapper's mask_v types. These are useful when writing generic
   *  functor code that works both for scalar and vector types.
   */
  constexpr bool all( bool x ) { return x; }
  constexpr bool any( bool x ) { return x; }
  constexpr bool none( bool x ) { return !x; }
  constexpr int  popcount( bool x ) { return x; }
  template <typename T>
  constexpr T select( bool x, T a, T b ) {
    return x ? a : b;
  }
  template <typename T, std::enable_if_t<std::is_arithmetic_v<T>, int> = 0>
  constexpr T hmin( T x, bool ) {
    return x;
  }
  template <typename T, std::enable_if_t<std::is_arithmetic_v<T>, int> = 0>
  constexpr T hmax( T x, bool ) {
    return x;
  }

  template <typename T, typename M>
  constexpr auto hmin( T const& x, M const& m ) {
    return x.hmin( m );
  }
  template <typename T, typename M>
  constexpr auto hmax( T const& x, M const& m ) {
    return x.hmax( m );
  }

  template <typename T>
  auto&& deref_if_ptr( T&& x ) {
    if constexpr ( std::is_pointer_v<std::remove_reference_t<T>> ) {
      assert( x ); // turn on DEBUG flags if you want to check this
      return *x;
    } else {
      return std::forward<T>( x );
    }
  }

  /** Helper to determine if the given type has a bestPV() method. */
  template <typename T>
  using has_bestPV_ = decltype( std::declval<T>().bestPV() );

  template <typename T>
  inline constexpr bool has_bestPV_v = Gaudi::cpp17::is_detected_v<has_bestPV_, T>;

  /** Helper to determine if the given type has a referencePoint() method. */
  template <typename T>
  using has_referencePoint_ = decltype( std::declval<T>().referencePoint() );

  template <typename T>
  inline constexpr bool has_referencePoint_v = Gaudi::cpp17::is_detected_v<has_referencePoint_, T>;

  /** Helper to determine if the given type has a size() method, to determine if one deals with a int_v or int. */
  template <typename T>
  using has_size_ = decltype( std::declval<T>().size() );

  template <typename T>
  inline constexpr bool has_size_v = Gaudi::cpp17::is_detected_v<has_size_, T>;

  /** Helper to determine if the given type has a closestToBeamState() method.
   */
  template <typename T>
  using has_closestToBeamState_ = decltype( std::declval<T>().closestToBeamState() );

  template <typename T>
  inline constexpr bool has_closestToBeamState_v = Gaudi::cpp17::is_detected_v<has_closestToBeamState_, T>;

  /** Helpers to determine if a given type is "state like". */
  template <typename T>
  using has_x_ = decltype( std::declval<T>().x() );
  template <typename T>
  using has_y_ = decltype( std::declval<T>().y() );
  template <typename T>
  using has_tx_ = decltype( std::declval<T>().tx() );
  template <typename T>
  using has_ty_ = decltype( std::declval<T>().ty() );
  template <typename T>
  using has_covariance_ = decltype( std::declval<T>().covariance() );
  template <typename T>
  inline constexpr bool has_x_v = Gaudi::cpp17::is_detected_v<has_x_, T>;
  template <typename T>
  inline constexpr bool has_y_v = Gaudi::cpp17::is_detected_v<has_y_, T>;
  template <typename T>
  inline constexpr bool has_tx_v = Gaudi::cpp17::is_detected_v<has_tx_, T>;
  template <typename T>
  inline constexpr bool has_ty_v = Gaudi::cpp17::is_detected_v<has_ty_, T>;
  template <typename T>
  inline constexpr bool has_covariance_v = Gaudi::cpp17::is_detected_v<has_covariance_, T>;
  template <typename T>
  inline constexpr bool is_state_like =
      has_x_v<T>&& has_y_v<T>&& type_traits::has_z_v<T>&& has_tx_v<T>&& has_ty_v<T>&& has_covariance_v<T>;

  template <typename T>
  constexpr bool is_legacy_particle = std::is_same_v<LHCb::Particle, T>;

  template <typename T>
  constexpr bool is_lhcb_vertexbase = std::is_base_of_v<LHCb::VertexBase, T>;

  inline constexpr auto get_track_from_particle = []( auto const& _p ) {
    auto const& p  = deref_if_ptr( _p );
    auto const* pp = p.proto();
    return pp ? pp->track() : nullptr;
  };

  inline constexpr auto get_track_property_from_particle = []( auto const& p, auto&& accessor, auto&& invalid ) {
    auto const* track = get_track_from_particle( p );
    return track ? accessor( track ) : invalid;
  };

  /* when we have different accessor names, we use these indirection layers */
  template <typename T>
  auto threeMomentum( T const& item ) {
    if constexpr ( is_legacy_particle<T> ) {
      return item.momentum().Vect();
    } else {
      return item.threeMomentum();
    }
  }

  template <typename T>
  auto endVertexPos( T const& item ) {
    if constexpr ( is_legacy_particle<T> ) {
      assert( item.endVertex() );
      return item.endVertex()->position();
    } else {
      return item.endVertex();
    }
  }

  template <typename T>
  auto threeMomCovMatrix( T const& item ) {
    if constexpr ( is_legacy_particle<T> ) {
      return item.momCovMatrix().template Sub<Gaudi::SymMatrix3x3>( 0, 0 );
    } else {
      return item.threeMomCovMatrix();
    }
  }

  template <typename T>
  auto threeMomPosCovMatrix( T const& item ) {
    if constexpr ( is_legacy_particle<T> ) {
      return item.posMomCovMatrix().template Sub<Gaudi::Matrix3x3>( 0, 0 );
    } else {
      return item.threeMomPosCovMatrix();
    }
  }

  template <typename T>
  auto posCovMatrix( T const& item ) {
    if constexpr ( is_lhcb_vertexbase<T> ) {
      return item.covMatrix();
    } else {
      return item.posCovMatrix();
    }
  }

  /** Helpers for dispatching to the right fdchi2 calculation. */
  template <typename Vertex1, typename Vertex2>
  auto flightDistanceChi2( Vertex1 const& v1, Vertex2 const& v2 ) {
    // Lifted from LoKi::DistanceCalculator
    // we really need better `if constexpr` conditions
    if constexpr ( LHCb::LinAlg::is_sym_mat_v<decltype( posCovMatrix( v1 ) )> ) {
      auto nItems = popcount( v1.loop_mask() );

      auto cov = posCovMatrix( v1 );
      // should be cov += v2.posCovMatrix, but they have different types RN
      LHCb::Utils::unwind<0, 3>( [&]( auto i ) {
        LHCb::Utils::unwind<0, i + 1>( [&]( auto j ) {
          std::array<float, Vertex1::dType::size> elements{};
          // FIXME this is super ugly/slow and needs to be fixed with zippable vertices
          for ( int k = 0; k < nItems; ++k ) elements[k] = posCovMatrix( *v2[k] )( i, j );
          cov( i, j ) += elements.data();
        } );
      } );
      cov           = cov.invChol(); // what if it fails?
      auto const p1 = endVertexPos( v1 );

      // SoAify the second position
      // ugly af
      std::decay_t<decltype( p1 )> p2;
      {
        std::array<float, Vertex1::dType::size> X{}, Y{}, Z{};
        for ( int j = 0; j < nItems; ++j ) {
          X[j] = v2[j]->position().X();
          Y[j] = v2[j]->position().Y();
          Z[j] = v2[j]->position().Z();
        }
        p2( 0 ) = X.data();
        p2( 1 ) = Y.data();
        p2( 2 ) = Z.data();
      }
      return LHCb::LinAlg::similarity( p1 - p2, cov );
    } else {
      Gaudi::SymMatrix3x3 cov{posCovMatrix( v1 ) + posCovMatrix( v2 )};
      if ( !cov.Invert() ) { return std::numeric_limits<double>::max(); }
      auto const&    p1 = v1.position();
      auto const&    p2 = v2.position();
      Gaudi::Vector3 delta{p1.X() - p2.X(), p1.Y() - p2.Y(), p1.Z() - p2.Z()};
      return ROOT::Math::Similarity( delta, cov );
    }
  }

  /** @fn    impactParameterChi2
   *  @brief Helper for dispatching to the correct ipchi2 calculation.
   *
   * The input be particle-like, with an (x, y, z) position and 3/4-momentum, or
   * it could be state-like, with a (x, y, tx, ty[, q/p]) vector and covariance
   * matrix.
   *
   * LHCb::TrackVertexUtils::vertexChi2() has the ipchi2 calculation for a
   * state and [primary] vertex position/covariance.
   *
   * @todo Add a [template] version of LHCb::TrackVertexUtils::vertexChi2()
   *       that takes a particle-like? It only uses the 4x4 upper corner of
   *       the state-like covariance matrix, so we might be able to save
   *       something?
   */
  template <typename TrackOrParticle, typename Vertex>
  auto impactParameterChi2( TrackOrParticle const& _obj, Vertex const& vertex ) {
    auto const& obj = deref_if_ptr( _obj );
    if constexpr ( has_closestToBeamState_v<TrackOrParticle> ) {
      return LHCb::TrackVertexUtils::vertexChi2( obj.closestToBeamState(), vertex.position(), vertex.covMatrix() );
    } else if constexpr ( is_state_like<TrackOrParticle> ) {
      return LHCb::TrackVertexUtils::vertexChi2( obj, vertex.position(), vertex.covMatrix() );
    } else if constexpr ( LHCb::LinAlg::is_sym_mat_v<decltype( obj.posCovMatrix() )> ) {
      LHCb::LinAlg::resize_t<decltype( obj.posCovMatrix() ), 6> cov6{};

      // TODO consider not putting everything in a 6x6 matrix. is there a reason?
      cov6 = cov6.template place_at<0, 0>( obj.posCovMatrix() );
      cov6 = cov6.template place_at<3, 0>( obj.threeMomPosCovMatrix() );
      cov6 = cov6.template place_at<3, 3>( obj.threeMomCovMatrix() );

      decltype( obj.x() ) chi2{std::numeric_limits<float>::max()}, decaylength{std::numeric_limits<float>::max()},
          decaylength_err{std::numeric_limits<float>::max()};
      /* status = */ LHCb::TrackVertexUtils::computeChiSquare( endVertexPos( obj ), obj.threeMomentum(), cov6,
                                                               vertex.position(), vertex.covMatrix(), chi2, decaylength,
                                                               decaylength_err );
      // chi2 = decaylength = decaylength_err = std::numeric_limits<float>::max();
      return chi2;
    } else {
      Gaudi::Matrix6x6 cov6tmp;
      cov6tmp.Place_at( obj.posCovMatrix(), 0, 0 );          // 3x3 position
      cov6tmp.Place_at( threeMomCovMatrix( obj ), 3, 3 );    // 3x3 momentum
      cov6tmp.Place_at( threeMomPosCovMatrix( obj ), 3, 0 ); // 3x3 position x momentum
      Gaudi::SymMatrix6x6 cov6{cov6tmp.LowerBlock()};
      /** @todo add an implementation for ipchi2-of-composite that just returns
       *        it without calculating the decay length [error] too. OL tried
       *        this in https://gitlab.cern.ch/snippets/894, but without a
       *        better study of which implementation copes better when the
       *        matrix inversion fails (which it inevitably does sometimes)
       *        lets not switch to using it.
       */
      double chi2{std::numeric_limits<double>::max()}, decaylength{std::numeric_limits<double>::max()},
          decaylength_err{std::numeric_limits<double>::max()};
      if ( LHCb::TrackVertexUtils::computeChiSquare( obj.referencePoint(), threeMomentum( obj ), cov6,
                                                     vertex.position(), vertex.covMatrix(), chi2, decaylength,
                                                     decaylength_err ) != LHCb::TrackVertexUtils::Success ) {
        chi2 = decaylength = decaylength_err = std::numeric_limits<double>::max();
      }
      return chi2;
    }
  }
} // namespace Sel::Utils
