/*****************************************************************************\
* (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include "GaudiKernel/detected.h"

namespace Sel::type_traits {
  namespace detail {
    template <typename T>
    using has_eta = decltype( std::declval<T>().eta() );

    template <typename T>
    using has_perp = decltype( std::declval<T>().perp() );

    template <typename T>
    using has_x = decltype( std::declval<T>().x() );

    template <typename T>
    using has_y = decltype( std::declval<T>().y() );

    template <typename T>
    using has_z = decltype( std::declval<T>().z() );
  } // namespace detail

  template <typename T>
  inline constexpr bool has_eta_v = Gaudi::cpp17::is_detected_v<detail::has_eta, T>;

  template <typename T>
  inline constexpr bool has_perp_v = Gaudi::cpp17::is_detected_v<detail::has_perp, T>;

  template <typename T>
  inline constexpr bool has_x_v = Gaudi::cpp17::is_detected_v<detail::has_x, T>;

  template <typename T>
  inline constexpr bool has_y_v = Gaudi::cpp17::is_detected_v<detail::has_y, T>;

  template <typename T>
  inline constexpr bool has_z_v = Gaudi::cpp17::is_detected_v<detail::has_z, T>;
} // namespace Sel::type_traits