/*****************************************************************************\
* (c) Copyright 2019-20 CERN for the benefit of the LHCb Collaboration        *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include "Event/Zip.h"
#include "SelKernel/VertexRelation.h"

namespace Sel::VertexRelations {
  /** Proxy type for iterating over BestVertexRelations objects.
   *  @todo Implement using SOACollection.
   */
  DECLARE_PROXY( Proxy ) {
    PROXY_METHODS( Proxy, dType, BestVertexRelations, m_rels );
    auto bestPVIPChi2() const { return this->load_vector( m_rels->ipchi2s().data() ); }
    auto bestPVIndex() const { return this->load_vector( m_rels->indices().data() ); }
    auto bestPV() const { return BestVertexRelation{bestPVIndex(), bestPVIPChi2()}; }
  };
} // namespace Sel::VertexRelations

REGISTER_PROXY( BestVertexRelations, Sel::VertexRelations::Proxy );
REGISTER_HEADER( BestVertexRelations, "SelKernel/IterableVertexRelations.h" );
