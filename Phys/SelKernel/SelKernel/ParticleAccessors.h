/*****************************************************************************\
* (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include "LHCbMath/MatVec.h"
#include "LHCbMath/Vec3.h"
#include "SelKernel/ParticleTraits.h"
#include "SelKernel/VectorOps.h"
/** @namespace Sel::get
 *  @brief     Free-standing helper functions for extracting physical variables
 *
 *  This namespace collects helper functions for operations such like "get the
 *  energy of this object" that can perform some compile-time type
 *  introspection and support a range of different argument types.
 */
namespace Sel::get {
  template <typename T>
  auto chi2PerDoF( T const& x ) {
    if constexpr ( type_traits::has_chi2PerDoF_v<T> ) {
      return x.chi2PerDoF();
    } else {
      return x.chi2() / x.nDoF();
    }
  }

  /** Extract the energy of the given particle-like object, trying to
   *  generically prefer an efficient option. If doing this generically turns
   *  out to be hard in some specific cases, overloads could be provided for
   *  specific types.
   */
  template <typename T>
  auto energy( T const& x ) {
    if constexpr ( type_traits::has_e_v<T> ) {
      // Prefer a direct accessor if it exists
      return x.e();
    } else {
      // Otherwise calculate it
      auto const mass2 = [&]() {
        if constexpr ( type_traits::has_mass2_v<T> ) {
          return x.mass2();
        } else {
          static_assert( type_traits::has_mass_v<T> );
          auto const mass = x.mass();
          return mass * mass;
        }
      }();
      auto const momentum2 = [&]() {
        // TODO check for an accessor returning the squared magnitude of the
        //      3-momentum? This could be an efficient thing to return when the
        //      storage is (px, py, pz)
        auto const momentum = x.p();
        return momentum * momentum;
      }();
      using std::sqrt;
      return sqrt( mass2 + momentum2 );
    }
  }
  template <typename T>
  auto energy( LHCb::LinAlg::Vec<T, 4> const& mom ) {
    return mom( 4 );
  }

  template <typename T>
  auto x( LHCb::LinAlg::Vec<T, 3> const& vec ) {
    return vec( 0 );
  }
  template <typename T>
  auto x( T const& x ) -> decltype( x.x() ) {
    return x.x();
  }
  template <typename T>
  auto y( LHCb::LinAlg::Vec<T, 3> const& vec ) {
    return vec( 1 );
  }
  template <typename T>
  auto y( T const& x ) -> decltype( x.y() ) {
    return x.y();
  }
  template <typename T>
  auto z( LHCb::LinAlg::Vec<T, 3> const& vec ) {
    return vec( 2 );
  }
  template <typename T>
  auto z( T const& x ) -> decltype( x.z() ) {
    return x.z();
  }
  /** Extract the 4x4 position-slope covariance of a state.
   */
  template <typename T>
  decltype( auto ) posSlopeCovariance( T const& x ) {
    if constexpr ( type_traits::has_posSlopeCovariance_v<T> ) {
      return x.posSlopeCovariance();
    } else {
      return vector::sub_sym<0, 4>{}( x.covariance() );
    }
  }
  /** Extract the x component of the momentum of the given object
   */
  template <typename T, int N>
  auto px( LHCb::LinAlg::Vec<T, N> const& x ) {
    static_assert( N == 3 || N == 4 );
    // 3-momentum is {px, py, pz}, 4-momentum is {px, py, pz, energy}
    return x( 0 );
  }
  template <typename T>
  auto px( Vec3<T> const& x ) {
    return x.x;
  }
  template <typename T>
  auto px( T const& x ) -> decltype( x.px() ) {
    return x.px();
  }
  template <typename T>
  auto px( T const& x ) -> decltype( x.X() ) {
    return x.X();
  }
  template <typename T>
  auto px( T const& x ) -> decltype( x.Px() ) {
    return x.Px();
  }
  /** Extract the y component of the momentum of the given object
   */
  template <typename T, int N>
  auto py( LHCb::LinAlg::Vec<T, N> const& x ) {
    static_assert( N == 3 || N == 4 );
    // 3-momentum is {px, py, pz}, 4-momentum is {px, py, pz, energy}
    return x( 1 );
  }
  template <typename T>
  auto py( Vec3<T> const& x ) {
    return x.y;
  }
  template <typename T>
  auto py( T const& x ) -> decltype( x.py() ) {
    return x.py();
  }
  template <typename T>
  auto py( T const& x ) -> decltype( x.Y() ) {
    return x.Y();
  }
  template <typename T>
  auto py( T const& x ) -> decltype( x.Py() ) {
    return x.Py();
  }
  /** Extract the z component of the momentum of the given object
   */
  template <typename T, int N>
  auto pz( LHCb::LinAlg::Vec<T, N> const& x ) {
    static_assert( N == 3 || N == 4 );
    // 3-momentum is {px, py, pz}, 4-momentum is {px, py, pz, energy}
    return x( 2 );
  }
  template <typename T>
  auto pz( Vec3<T> const& x ) {
    return x.z;
  }
  template <typename T>
  auto pz( T const& x ) -> decltype( x.pz() ) {
    return x.pz();
  }
  template <typename T>
  auto pz( T const& x ) -> decltype( x.Z() ) {
    return x.Z();
  }
  template <typename T>
  auto pz( T const& x ) -> decltype( x.Pz() ) {
    return x.Pz();
  }
  /** Get the square of the transverse part of a 3- or 4-momentum vector.
   */
  template <typename T>
  auto pt2( T const& x ) {
    auto const p_x = px( x );
    auto const p_y = py( x );
    return p_x * p_x + p_y * p_y;
  }
  /** Get the transverse part of a 3- or 4-momentum vector.
   */
  template <typename T>
  auto pt( T const& x ) {
    using std::sqrt;
    return sqrt( pt2( x ) );
  }
  /** Get the mass from a 4-momentum.
   */
  template <typename T>
  auto mass( LHCb::LinAlg::Vec<T, 4> const& mom ) {
    using std::sqrt;
    return sqrt( mom( 3 ) * mom( 3 ) - mom( 0 ) * mom( 0 ) - mom( 1 ) * mom( 1 ) - mom( 2 ) * mom( 2 ) );
  }
  /** Extract the 3-momentum of a particle.
   */
  template <typename T>
  decltype( auto ) threeMomentum( T const& x ) {
    return x.threeMomentum();
  }

  /** Extract the 4-momentum of a particle.
   *  Declared out of order because it uses p{x,y,z}.
   */
  template <typename T>
  decltype( auto ) momentum( T const& x ) {
    if constexpr ( type_traits::has_momentum_v<T> ) {
      return x.momentum();
    } else {
      auto const mom3 = x.threeMomentum();
      return LHCb::LinAlg::Vec{px( mom3 ), py( mom3 ), pz( mom3 ), energy( x )};
    }
  }
} // namespace Sel::get