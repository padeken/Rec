/*****************************************************************************\
* (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include "GaudiKernel/detected.h"

/** @namespace Sel::type_traits
 *  @brief     Type traits for particle-like types.
 *
 *  This namespace holds a collection of compile-time tests for different
 *  accessors. They are typically used in `if constexpr` expressions inside
 *  the free-standing helper functions in the Sel::get namespace.
 */
namespace Sel::type_traits {
  namespace detail {
    template <typename T>
    using has_chi2PerDoF = decltype( std::declval<T>().chi2PerDoF() );

    template <typename T>
    using has_closestToBeamState = decltype( std::declval<T>().closestToBeamState() );

    template <typename T>
    using has_e = decltype( std::declval<T>().e() );

    template <typename T>
    using has_endVertex = decltype( std::declval<T>().endVertex() );

    template <typename T>
    using has_mass = decltype( std::declval<T>().mass() );

    template <typename T>
    using has_mass2 = decltype( std::declval<T>().mass2() );

    template <typename T>
    using has_momentum = decltype( std::declval<T>().momentum() );

    template <typename T>
    using has_momCovMatrix = decltype( std::declval<T>().momCovMatrix() );

    template <typename T>
    using has_momPosCovMatrix = decltype( std::declval<T>().momPosCovMatrix() );

    template <typename T>
    using has_posCovMatrix = decltype( std::declval<T>().posCovMatrix() );

    template <typename T>
    using has_posSlopeCovariance = decltype( std::declval<T>().posSlopeCovariance() );

    template <typename T>
    using has_px = decltype( std::declval<T>().px() );

    template <typename T>
    using has_py = decltype( std::declval<T>().py() );

    template <typename T>
    using has_pz = decltype( std::declval<T>().pz() );

    template <typename T>
    using has_threeMomentum = decltype( std::declval<T>().threeMomentum() );

    template <typename T>
    using has_threeMomCovMatrix = decltype( std::declval<T>().threeMomCovMatrix() );

    template <typename T>
    using has_threeMomPosCovMatrix = decltype( std::declval<T>().threeMomPosCovMatrix() );
  } // namespace detail

  template <typename T>
  inline constexpr bool has_chi2PerDoF_v = Gaudi::cpp17::is_detected_v<detail::has_chi2PerDoF, T>;

  template <typename T>
  inline constexpr bool has_closestToBeamState_v = Gaudi::cpp17::is_detected_v<detail::has_closestToBeamState, T>;

  template <typename T>
  inline constexpr bool has_e_v = Gaudi::cpp17::is_detected_v<detail::has_e, T>;

  template <typename T>
  inline constexpr bool has_endVertex_v = Gaudi::cpp17::is_detected_v<detail::has_endVertex, T>;

  template <typename T>
  inline constexpr bool has_mass_v = Gaudi::cpp17::is_detected_v<detail::has_mass, T>;

  template <typename T>
  inline constexpr bool has_mass2_v = Gaudi::cpp17::is_detected_v<detail::has_mass2, T>;

  /** This should mean 4-momentum.
   */
  template <typename T>
  inline constexpr bool has_momentum_v = Gaudi::cpp17::is_detected_v<detail::has_momentum, T>;

  template <typename T>
  inline constexpr bool has_momCovMatrix_v = Gaudi::cpp17::is_detected_v<detail::has_momCovMatrix, T>;

  template <typename T>
  inline constexpr bool has_momPosCovMatrix_v = Gaudi::cpp17::is_detected_v<detail::has_momPosCovMatrix, T>;

  template <typename T>
  inline constexpr bool has_posCovMatrix_v = Gaudi::cpp17::is_detected_v<detail::has_posCovMatrix, T>;

  template <typename T>
  inline constexpr bool has_posSlopeCovariance_v = Gaudi::cpp17::is_detected_v<detail::has_posSlopeCovariance, T>;

  template <typename T>
  inline constexpr bool has_px_v = Gaudi::cpp17::is_detected_v<detail::has_px, T>;

  template <typename T>
  inline constexpr bool has_py_v = Gaudi::cpp17::is_detected_v<detail::has_py, T>;

  template <typename T>
  inline constexpr bool has_pz_v = Gaudi::cpp17::is_detected_v<detail::has_pz, T>;

  template <typename T>
  inline constexpr bool has_threeMomentum_v = Gaudi::cpp17::is_detected_v<detail::has_threeMomentum, T>;

  template <typename T>
  inline constexpr bool has_threeMomCovMatrix_v = Gaudi::cpp17::is_detected_v<detail::has_threeMomCovMatrix, T>;

  template <typename T>
  inline constexpr bool has_threeMomPosCovMatrix_v = Gaudi::cpp17::is_detected_v<detail::has_threeMomPosCovMatrix, T>;
} // namespace Sel::type_traits