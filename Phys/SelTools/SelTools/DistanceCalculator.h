/*****************************************************************************\
* (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include "SelKernel/State4.h"
#include "SelTools/Utilities.h"

#include "LHCbMath/MatVec.h"

#include "Gaudi/Algorithm.h"

namespace Sel {
  /** @class DistanceCalculator
   *  @brief Collection of functions for calculating quantities like IP,
   *         IPCHI2, DOCA, DOCACHI2, ...
   */
  struct DistanceCalculator {
    DistanceCalculator( Gaudi::Algorithm* ) {}

  private:
    template <typename Particle>
    static auto getClosestToBeamState( Particle const& p ) {
      if constexpr ( Sel::type_traits::has_closestToBeamState_v<Particle> ) {
        return p.closestToBeamState();
      } else {
        // Make a state vector from a particle (position + 4-momentum)
        return stateVectorFromComposite( p );
      }
    }

  public:
    /** @fn    stateDOCA
     *  @brief Calculate the distance of closest approach between two states.
     *
     *  This is copied from LHCb::TrackVertexUtils::doca so it can be templated
     *  on the state type, allowing the two states to be of different types. In
     *  the TrackVertexUtils version this would lead to a collision with the doca
     *  function taking a state and a point.
     */
    template <typename StateA, typename StateB>
    auto stateDOCA( StateA const& stateA, StateB const& stateB ) const {
      using std::sqrt; // allows sqrt() below to work with basic types + ADL
      // first compute the cross product of the directions.
      auto const txA = stateA.tx();
      auto const tyA = stateA.ty();
      auto const txB = stateB.tx();
      auto const tyB = stateB.ty();
      auto const nx  = tyA - tyB;             //   y1 * z2 - y2 * z1
      auto const ny  = txB - txA;             // - x1 * z2 + x2 * z1
      auto const nz  = txA * tyB - tyA * txB; //   x1 * y2 - x2 * y1
      auto const n   = sqrt( nx * nx + ny * ny + nz * nz );
      // compute the doca
      auto const dx    = stateA.x() - stateB.x();
      auto const dy    = stateA.y() - stateB.y();
      auto const dz    = stateA.z() - stateB.z();
      auto const ndoca = dx * nx + dy * ny + dz * nz;
      return ndoca / n;
    }

    /** @fn    stateDOCAChi2
     *  @brief Significance of DOCA between two states.
     */
    template <typename StateA, typename StateB>
    auto stateDOCAChi2( StateA const& sA, StateB const& sB ) const {
      // first compute the cross product of the directions. we'll need this in any case
      using float_v     = std::decay_t<decltype( sA.tx() )>;
      float_v const txA = sA.tx();
      float_v const tyA = sA.ty();
      float_v const txB = sB.tx();
      float_v const tyB = sB.ty();
      float_v const nx  = tyA - tyB;             //   y1 * z2 - y2 * z1
      float_v const ny  = txB - txA;             // - x1 * z2 + x2 * z1
      float_v const nz  = txA * tyB - tyA * txB; //   x1 * y2 - x2 * y1

      // compute doca. we don't divide by the normalization to save time. we call it 'ndoca'
      float_v const dx    = sA.x() - sB.x();
      float_v const dy    = sA.y() - sB.y();
      float_v const dz    = sA.z() - sB.z();
      float_v const ndoca = dx * nx + dy * ny + dz * nz;

      // the hard part: compute the jacobians :-)
      using Vector4 = LHCb::LinAlg::Vec<float_v, 4>;
      Vector4 jacA, jacB;
      jacA( 0 ) = nx;
      jacA( 1 ) = ny;
      jacA( 2 ) = -dy + dz * tyB;
      jacA( 3 ) = dx - dz * txB;
      jacB( 0 ) = -nx;
      jacB( 1 ) = -ny;
      jacB( 2 ) = dy - dz * tyA;
      jacB( 3 ) = -dx + dz * txA;

      // compute the variance on ndoca
      float_v const varndoca = vector::similarity{}( jacA, get::posSlopeCovariance( sA ) ) +
                               vector::similarity{}( jacB, get::posSlopeCovariance( sB ) );

      // return the chi2
      return ndoca * ndoca / varndoca;
    }

    /** @fn    particleDOCA
     *  @brief Calculate the distance of closest approach between two particles.
     *  @todo  This needs to do something sensible when the particle is a
     *         downstream track.
     */
    template <typename ParticleA, typename ParticleB>
    auto particleDOCA( ParticleA const& pA, ParticleB const& pB ) const {
      return stateDOCA( getClosestToBeamState( pA ), getClosestToBeamState( pB ) );
    }

    /** @fn    particleDOCAChi2
     *  @brief Significance of DOCA between two particles.
     */
    template <typename ParticleA, typename ParticleB>
    auto particleDOCAChi2( ParticleA const& pA, ParticleB const& pB ) const {
      return stateDOCAChi2( getClosestToBeamState( pA ), getClosestToBeamState( pB ) );
    }
  };
} // namespace Sel