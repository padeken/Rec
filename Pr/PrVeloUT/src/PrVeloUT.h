/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

// Include files
//
#include <numeric>

// from Gaudi
#include "GaudiAlg/ISequencerTimerTool.h"
#include "GaudiAlg/Transformer.h"

// from TrackInterfaces
#include "Event/RecVertex_v2.h"
#include "Event/Track_v2.h"

#include "PrKernel/PrUTHitHandler.h"

#include "DetDesc/Condition.h"
#include "DetDesc/ConditionAccessorHolder.h"
#include "GaudiKernel/DataObject.h"
#include "GaudiKernel/ObjectContainerBase.h"
#include "GaudiKernel/Range.h"
#include "Kernel/ILHCbMagnetSvc.h"
#include "PrKernel/PrMutUTHits.h"
#include "PrKernel/PrVeloUTTrack.h"
#include "PrKernel/UTHitInfo.h"
#include "PrUTMagnetTool.h"
#include "TfKernel/IndexedHitContainer.h"
#include "TfKernel/MultiIndexedHitContainer.h"
#include "UTDAQ/UTDAQHelper.h"
#include "UTDAQ/UTInfo.h"
#include "vdt/log.h"
#include "vdt/sqrt.h"

#include "Event/PrUpstreamTracks.h"
#include "Event/PrVeloTracks.h"

/** @class PrVeloUT PrVeloUT.h
 *
 *  PrVeloUT algorithm. This is just a wrapper,
 *  the actual pattern recognition is done in the 'PrVeloUTTool'.
 *
 *  - InputTracksName: Input location for Velo tracks
 *  - OutputTracksName: Output location for VeloTT tracks
 *  - TimingMeasurement: Do a timing measurement?
 *
 *  @author Mariusz Witek
 *  @date   2007-05-08
 *  @update for A-Team framework 2007-08-20 SHM
 *
 *  2017-03-01: Christoph Hasse (adapt to future framework)
 *  2019-04-26: Arthur Hennequin (change data Input/Output)
 *  2020-08-26: Peilian Li (change data Input/Output to SOACollection)
 */

namespace LHCb::Pr {

  constexpr static int batchSize     = align_size( 48 );
  constexpr static int maxNumCols    = 3;                       // if needed, algo can be templated with this
  constexpr static int maxNumRows    = 3;                       // if needed, algo can be templated with this
  constexpr static int maxNumSectors = maxNumCols * maxNumRows; // if needed, algo can be templated with this

  using simd   = SIMDWrapper::best::types;
  using scalar = SIMDWrapper::scalar::types;

  struct MiniStatesArray final {

    constexpr static int              max_tracks = align_size( 1024 );
    std::array<float, 3 * max_tracks> poss;
    std::array<float, 2 * max_tracks> dirs;
    std::array<float, 3 * max_tracks> covs;
    std::array<int, max_tracks>       indexs;
    std::size_t                       size{0};

    SOA_ACCESSOR( x, &( poss[0] ) )
    SOA_ACCESSOR( y, &( poss[max_tracks] ) )
    SOA_ACCESSOR( z, &( poss[2 * max_tracks] ) )
    SOA_ACCESSOR( tx, &( dirs[0] ) )
    SOA_ACCESSOR( ty, &( dirs[max_tracks] ) )
    SOA_ACCESSOR( covx, &( covs[0] ) )
    SOA_ACCESSOR( covy, &( covs[max_tracks] ) )
    SOA_ACCESSOR( covz, &( covs[2 * max_tracks] ) )
    SOA_ACCESSOR( index, indexs.data() )
    VEC3_SOA_ACCESSOR( pos, (float*)&( poss[0] ), (float*)&( poss[max_tracks] ), (float*)&( poss[2 * max_tracks] ) )
    VEC3_XY_SOA_ACCESSOR( dir, (float*)&( dirs[0] ), (float*)&( dirs[max_tracks] ), 1.0f )
    VEC3_SOA_ACCESSOR( cov, (float*)&( covs[0] ), (float*)&( covs[max_tracks] ), (float*)&( covs[2 * max_tracks] ) )

    // -- Copy back the entries, but with a filtering mask
    template <typename dType>
    void copyBack( std::size_t at, typename dType::mask_v mask ) {

      using F = typename dType::float_v;
      using I = typename dType::int_v;

      F( &poss[at] ).compressstore( mask, &poss[size] );
      F( &poss[at + max_tracks] ).compressstore( mask, &poss[size + max_tracks] );
      F( &poss[at + 2 * max_tracks] ).compressstore( mask, &poss[size + 2 * max_tracks] );
      F( &dirs[at] ).compressstore( mask, &dirs[size] );
      F( &dirs[at + max_tracks] ).compressstore( mask, &dirs[size + max_tracks] );
      F( &covs[at] ).compressstore( mask, &covs[size] );
      F( &covs[at + max_tracks] ).compressstore( mask, &covs[size + max_tracks] );
      F( &covs[at + 2 * max_tracks] ).compressstore( mask, &covs[size + 2 * max_tracks] );
      I( &indexs[at] ).compressstore( mask, &indexs[size] );
      size += dType::popcount( mask );
    }
  };

  struct ExtrapolatedStates final {

    constexpr static int max_tracks = align_size( 1024 );

    std::array<float, max_tracks> xLayers;
    std::array<float, max_tracks> yLayers;
    std::array<float, max_tracks> xTols;
    std::array<float, max_tracks> txs;

    std::size_t size{0};
    SOA_ACCESSOR( xLayer, xLayers.data() )
    SOA_ACCESSOR( yLayer, yLayers.data() )
    SOA_ACCESSOR( xTol, xTols.data() )
    SOA_ACCESSOR( tx, txs.data() )
  };

  struct Boundaries final {

    constexpr static int max_tracks = align_size( 1024 );

    std::array<int, 9 * max_tracks> sects{};
    std::array<float, max_tracks>   xTols;
    std::array<int, max_tracks>     nPoss{};

    std::size_t size{0};
    SOA_ACCESSOR_VAR( sect, &( sects[pos * max_tracks] ), int pos )
    SOA_ACCESSOR( xTol, xTols.data() )
    SOA_ACCESSOR( nPos, nPoss.data() )
  };

  struct ProtoTracks final {

    std::array<float, simd::size> wbs;
    std::array<float, simd::size> xMidFields;
    std::array<float, simd::size> invKinkVeloDists;

    // -- this is for the hits
    // -- this does _not_ include overlap hits, so only 4 per track
    std::array<float, 4 * batchSize> xs;
    std::array<float, 4 * batchSize> zs;
    std::array<float, 4 * batchSize> weightss{}; // this needs to be zero-initialized
    std::array<float, 4 * batchSize> sins;
    std::array<int, 4 * batchSize>   ids;
    std::array<int, 4 * batchSize>   hitIndexs{-1};

    // -- this is the output of the fit
    std::array<float, batchSize> qps;
    std::array<float, batchSize> chi2TTs;
    std::array<float, batchSize> xTTs;
    std::array<float, batchSize> xSlopeTTs;
    std::array<float, batchSize> ys;

    // -- and this the original state (in the Velo)
    std::array<float, 3 * batchSize> statePoss;
    std::array<float, 2 * batchSize> stateDirs;
    std::array<int, batchSize>       indexs;

    // -- and this an index to find the hit containers
    std::array<int, batchSize> hitContIndexs;

    std::size_t size{0};
    SOA_ACCESSOR_VAR( x, &( xs[pos * batchSize] ), int pos )
    SOA_ACCESSOR_VAR( z, &( zs[pos * batchSize] ), int pos )
    SOA_ACCESSOR_VAR( weight, &( weightss[pos * batchSize] ), int pos )
    SOA_ACCESSOR_VAR( sin, &( sins[pos * batchSize] ), int pos )
    SOA_ACCESSOR_VAR( id, &( ids[pos * batchSize] ), int pos )
    SOA_ACCESSOR_VAR( hitIndex, &( hitIndexs[pos * batchSize] ), int pos )

    SOA_ACCESSOR( qp, qps.data() )
    SOA_ACCESSOR( chi2TT, chi2TTs.data() )
    SOA_ACCESSOR( xTT, xTTs.data() )
    SOA_ACCESSOR( xSlopeTT, xSlopeTTs.data() )
    SOA_ACCESSOR( y, ys.data() )

    SOA_ACCESSOR( index, indexs.data() )
    SOA_ACCESSOR( hitContIndex, hitContIndexs.data() )
    VEC3_SOA_ACCESSOR( pos, (float*)&( statePoss[0] ), (float*)&( statePoss[batchSize] ),
                       (float*)&( statePoss[2 * batchSize] ) )
    VEC3_XY_SOA_ACCESSOR( dir, (float*)&( stateDirs[0] ), (float*)&( stateDirs[batchSize] ), 1.0f )

    SOA_ACCESSOR( wb, wbs.data() )
    SOA_ACCESSOR( xMidField, xMidFields.data() )
    SOA_ACCESSOR( invKinkVeloDist, invKinkVeloDists.data() )

    template <typename dType>
    void fillHelperParams( Vec3<typename dType::float_v> pos, Vec3<typename dType::float_v> dir, const float zKink,
                           const float sigmaVeloSlope ) {

      using F = typename dType::float_v;

      F( pos.x + dir.x * ( zKink - pos.z ) ).store( xMidFields.data() );
      F a = sigmaVeloSlope * ( zKink - pos.z );
      F( 1.0f / ( a * a ) ).store( wbs.data() );
      F( 1.0f / ( zKink - pos.z ) ).store( invKinkVeloDists.data() );
    }
  };

  class VeloUT : public Gaudi::Functional::Transformer<Upstream::Tracks( const EventContext&, const Velo::Tracks&,
                                                                         const LHCb::Pr::UT::HitHandler&,
                                                                         const UTDAQ::GeomCache& ),
                                                       LHCb::DetDesc::usesConditions<UTDAQ::GeomCache>> {

  public:
    /// Standard constructor
    VeloUT( const std::string& name, ISvcLocator* pSvcLocator );

    StatusCode initialize() override;

    Upstream::Tracks operator()( const EventContext&, const Velo::Tracks&, const LHCb::Pr::UT::HitHandler&,
                                 const UTDAQ::GeomCache& ) const override final;

  private:
    Gaudi::Property<float> m_minMomentum{this, "minMomentum", 1.5 * Gaudi::Units::GeV};
    Gaudi::Property<float> m_minPT{this, "minPT", 0.3 * Gaudi::Units::GeV};
    Gaudi::Property<float> m_minMomentumFinal{this, "minMomentumFinal", 2.5 * Gaudi::Units::GeV};
    Gaudi::Property<float> m_minPTFinal{this, "minPTFinal", 0.425 * Gaudi::Units::GeV};
    Gaudi::Property<float> m_maxPseudoChi2{this, "maxPseudoChi2", 1280.};
    Gaudi::Property<float> m_yTol{this, "YTolerance", 0.5 * Gaudi::Units::mm}; // 0.8
    Gaudi::Property<float> m_yTolSlope{this, "YTolSlope", 0.08};               // 0.2
    Gaudi::Property<float> m_hitTol{this, "HitTol", 0.8 * Gaudi::Units::mm};   // 0.8
    Gaudi::Property<float> m_deltaTx{this, "DeltaTx", 0.018};                  // 0.02
    Gaudi::Property<float> m_maxXSlope{this, "MaxXSlope", 0.350};
    Gaudi::Property<float> m_maxYSlope{this, "MaxYSlope", 0.300};
    Gaudi::Property<float> m_centralHoleSize{this, "centralHoleSize", 33. * Gaudi::Units::mm};
    Gaudi::Property<float> m_intraLayerDist{this, "IntraLayerDist", 15.0 * Gaudi::Units::mm};
    Gaudi::Property<float> m_overlapTol{this, "OverlapTol", 0.5 * Gaudi::Units::mm};
    Gaudi::Property<float> m_passHoleSize{this, "PassHoleSize", 40. * Gaudi::Units::mm};
    Gaudi::Property<float> m_LD3Hits{this, "LD3HitsMin", -0.5};
    Gaudi::Property<float> m_LD4Hits{this, "LD4HitsMin", -0.5};

    Gaudi::Property<bool> m_printVariables{this, "PrintVariables", false};
    Gaudi::Property<bool> m_passTracks{this, "PassTracks", false};
    Gaudi::Property<bool> m_doTiming{this, "TimingMeasurement", false};
    Gaudi::Property<bool> m_finalFit{this, "FinalFit", true};
    Gaudi::Property<bool> m_fiducialCuts{this, "FiducialCuts", true};

    mutable Gaudi::Accumulators::SummingCounter<unsigned int> m_seedsCounter{this, "#seeds"};
    mutable Gaudi::Accumulators::SummingCounter<unsigned int> m_tracksCounter{this, "#tracks"};

    StatusCode recomputeGeometry();

    MiniStatesArray getStates( const Velo::Tracks& inputTracks, Upstream::Tracks& outputTracks ) const;

    std::array<ExtrapolatedStates, static_cast<int>( UTInfo::DetectorNumbers::TotalLayers )>
    extrapStates( const MiniStatesArray& filteredStates, const UTDAQ::GeomCache& geom ) const;

    std::array<Boundaries, static_cast<int>( UTInfo::DetectorNumbers::TotalLayers )> findAllSectors(
        const std::array<ExtrapolatedStates, static_cast<int>( UTInfo::DetectorNumbers::TotalLayers )>& eStatesArray,
        MiniStatesArray& filteredStates, const UTDAQ::GeomCache& geom ) const;

    bool getHitsScalar( const LHCb::Pr::UT::HitHandler& hh, const MiniStatesArray& filteredStates,
                        const std::array<Boundaries, 4>& compBoundsArray, LHCb::Pr::UT::Mut::Hits& hitsInLayers,
                        const std::size_t t ) const;

    inline void findHits( const LHCb::Pr::UT::HitHandler& hh, const simd::float_v& yProto, const simd::float_v& ty,
                          const simd::float_v& tx, const simd::float_v& xOnTrackProto, const simd::float_v& tolProto,
                          const simd::float_v& xTolNormFact, LHCb::Pr::UT::Mut::Hits& mutHits,
                          const simd::float_v& yTol, const int firstIndex, const int lastIndex ) const;

    template <bool forward>
    bool formClusters( const LHCb::Pr::UT::Mut::Hits& hitsInLayers, ProtoTracks& pTracks, const int trackIndex ) const;

    template <typename BdlTable>
    void prepareOutputTrackSIMD( const ProtoTracks&                                    protoTracks,
                                 const std::array<LHCb::Pr::UT::Mut::Hits, batchSize>& hitsInLayers,
                                 Upstream::Tracks& outputTracks, const Velo::Tracks& inputTracks,
                                 const BdlTable& bdlTable ) const;

    DeUTDetector* m_utDet = nullptr;

    /// Multipupose tool for Bdl and deflection
    ToolHandle<UTMagnetTool>      m_PrUTMagnetTool{this, "PrUTMagnetTool", "PrUTMagnetTool"};
    ServiceHandle<ILHCbMagnetSvc> m_magFieldSvc{this, "MagneticField", "MagneticFieldSvc"};
    /// timing tool
    mutable ToolHandle<ISequencerTimerTool> m_timerTool{this, "SequencerTimerTool", "SequencerTimerTool"}; // FIXME
    mutable Gaudi::Accumulators::MsgCounter<MSG::ERROR> m_too_much_in_filtered{
        this, "Reached the maximum number of tracks in filteredStates!!"};
    mutable Gaudi::Accumulators::MsgCounter<MSG::ERROR> m_too_much_in_boundaries{
        this, "Reached the maximum number of tracks in Boundaries!!"};

    ///< Counter for timing tool
    int m_veloUTTime{0};

    float m_zMidUT;
    float m_distToMomentum;

    constexpr static float c_zKink{1780.0};
    constexpr static float c_sigmaVeloSlope{0.10 * Gaudi::Units::mrad};
    constexpr static float c_invSigmaVeloSlope{10.0 / Gaudi::Units::mrad};
  };
} // namespace LHCb::Pr
