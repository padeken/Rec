/***************************************************************************** \
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files
#include "Kernel/STLExtensions.h"
#include "LHCbMath/bit_cast.h"
#include "UTDet/DeUTSector.h"
#include <boost/container/small_vector.hpp>

// local
#include "LHCbMath/GeomFun.h"
#include "PrVeloUT.h"

//-----------------------------------------------------------------------------
// Implementation file for class : PrVeloUT
//
// 2007-05-08: Mariusz Witek
// 2017-03-01: Christoph Hasse (adapt to future framework)
// 2019-04-26: Arthur Hennequin (change data Input/Output)
//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_COMPONENT_WITH_ID( LHCb::Pr::VeloUT, "PrVeloUT" )

namespace LHCb::Pr {
  namespace {

    simd::mask_v CholeskyDecomposition3( const std::array<simd::float_v, 6>& mat, std::array<simd::float_v, 3>& rhs ) {
      // -- copied from Root::Math::CholeskyDecomp
      // -- first decompose
      std::array<simd::float_v, 6> dst;
      simd::mask_v                 mask = mat[0] < simd::float_v{0.0f};
      dst[0]                            = max( 1e-6f, dst[0] ); // that's only needed if you care about FPE
      // dst[0]                            = rsqrt( mat[0] ); // this seems to give precision issues
      dst[0] = 1.0f / sqrt( mat[0] );
      dst[1] = mat[1] * dst[0];
      dst[2] = mat[2] - dst[1] * dst[1];
      mask   = mask || ( dst[2] < simd::float_v{0.0f} );
      dst[2] = max( 1e-6f, dst[2] ); // that's only needed if you care about FPE
      // dst[2]                            = rsqrt( dst[2] ); // this seems to give precision issues
      dst[2] = 1.0f / sqrt( dst[2] );
      dst[3] = mat[3] * dst[0];
      dst[4] = ( mat[4] - dst[1] * dst[3] ) * dst[2];
      dst[5] = mat[5] - ( dst[3] * dst[3] + dst[4] * dst[4] );
      mask   = mask || ( dst[5] < simd::float_v{0.0f} );
      dst[5] = max( 1e-6f, dst[5] ); // that's only needed if you care about FPE
      // dst[5]                            = rsqrt( dst[5] ); // this seems to give precision issues
      dst[5] = 1.0f / sqrt( dst[5] );

      // -- then solve
      // -- solve Ly = rhs
      const simd::float_v y0 = rhs[0] * dst[0];
      const simd::float_v y1 = ( rhs[1] - dst[1] * y0 ) * dst[2];
      const simd::float_v y2 = ( rhs[2] - ( dst[3] * y0 + dst[4] * y1 ) ) * dst[5];
      // solve L^Tx = y, and put x into rhs
      rhs[2] = (y2)*dst[5];
      rhs[1] = ( y1 - ( dst[4] * rhs[2] ) ) * dst[2];
      rhs[0] = ( y0 - ( dst[3] * rhs[2] + dst[1] * rhs[1] ) ) * dst[0];

      return mask;
    }

    // -- parameters that describe the z position of the kink point as a function of ty in a 4th order polynomial (even
    // terms only)
    constexpr auto magFieldParams = std::array{2010.0f, -2240.0f, -71330.f};

    // perform a fit using trackhelper's best hits with y correction, improve qop estimate
    simd::float_v fastfitterSIMD( std::array<simd::float_v, 4>& improvedParams, const ProtoTracks& protoTracks,
                                  const float zMidUT, const simd::float_v qpxz2p, const int t,
                                  simd::mask_v& goodFitMask ) {

      const Vec3<simd::float_v> pos = protoTracks.pos<simd::float_v>( t );
      const Vec3<simd::float_v> dir = protoTracks.dir<simd::float_v>( t );

      const simd::float_v x  = pos.x;
      const simd::float_v y  = pos.y;
      const simd::float_v z  = pos.z;
      const simd::float_v tx = dir.x;
      const simd::float_v ty = dir.y;
      const simd::float_v zKink =
          magFieldParams[0] - ty * ty * magFieldParams[1] - ty * ty * ty * ty * magFieldParams[2];
      const simd::float_v xMidField = x + tx * ( zKink - z );

      const simd::float_v zDiff = 0.001f * ( zKink - zMidUT );

      // -- This is to avoid division by zero...
      const simd::float_v pHelper = max( abs( protoTracks.qp<simd::float_v>( t ) * qpxz2p ), 1e-9f );
      const simd::float_v invP    = pHelper * rsqrt( 1.0f + ty * ty );

      // these resolution are semi-empirical, could be tuned and might not be correct for low momentum.
      const simd::float_v error1 =
          0.14f + 10000.0f * invP; // this is the resolution due to multiple scattering between Velo and UT
      const simd::float_v error2 = 0.12f + 3000.0f * invP; // this is the resolution due to the finite Velo resolution
      const simd::float_v error  = error1 * error1 + error2 * error2;
      const simd::float_v weight = 1.0f / error;

      std::array<simd::float_v, 6> mat = {weight, weight * zDiff, weight * zDiff * zDiff, 0.0f, 0.0f, 0.0f};
      std::array<simd::float_v, 3> rhs = {weight * xMidField, weight * xMidField * zDiff, 0.0f};

      for ( int i = 0; i < 4; ++i ) {

        // -- there are 3-hit candidates, but we'll
        // -- just treat them like 4-hit candidates
        // -- with 0 weight for the last hit
        const simd::float_v ui = protoTracks.x<simd::float_v>( t, i );
        const simd::float_v dz = 0.001f * ( protoTracks.z<simd::float_v>( t, i ) - zMidUT );
        const simd::float_v w  = protoTracks.weight<simd::float_v>( t, i );
        const simd::float_v ta = protoTracks.sin<simd::float_v>( t, i );
        mat[0] += w;
        mat[1] += w * dz;
        mat[2] += w * dz * dz;
        mat[3] += w * ta;
        mat[4] += w * dz * ta;
        mat[5] += w * ta * ta;
        rhs[0] += w * ui;
        rhs[1] += w * ui * dz;
        rhs[2] += w * ui * ta;
      }

      goodFitMask = !CholeskyDecomposition3( mat, rhs );

      const simd::float_v xUTFit      = rhs[0];
      const simd::float_v xSlopeUTFit = 0.001f * rhs[1];
      const simd::float_v offsetY     = rhs[2];

      const simd::float_v distX = ( xMidField - xUTFit - xSlopeUTFit * ( zKink - zMidUT ) );
      // -- This takes into account that the distance between a point and track is smaller than the distance on the
      // x-axis
      const simd::float_v distCorrectionX2 = 1.0f / ( 1 + xSlopeUTFit * xSlopeUTFit );
      simd::float_v       chi2 = weight * ( distX * distX * distCorrectionX2 + offsetY * offsetY / ( 1.0f + ty * ty ) );

      for ( int i = 0; i < 4; ++i ) {

        const simd::float_v dz   = protoTracks.z<simd::float_v>( t, i ) - zMidUT;
        const simd::float_v w    = protoTracks.weight<simd::float_v>( t, i );
        const simd::float_v dist = ( protoTracks.x<simd::float_v>( t, i ) - xUTFit - xSlopeUTFit * dz -
                                     offsetY * protoTracks.sin<simd::float_v>( t, i ) );

        chi2 += w * dist * dist * distCorrectionX2;
      }

      // new VELO slope x
      const simd::float_v xb =
          0.5f * ( ( xUTFit + xSlopeUTFit * ( zKink - zMidUT ) ) + xMidField ); // the 0.5 is empirical
      const simd::float_v xSlopeVeloFit = ( xb - x ) / ( zKink - z );

      improvedParams = {xUTFit, xSlopeUTFit, y + ty * ( zMidUT - z ) + offsetY, chi2};

      // calculate q/p
      const simd::float_v sinInX  = xSlopeVeloFit * rsqrt( 1.0f + xSlopeVeloFit * xSlopeVeloFit + ty * ty );
      const simd::float_v sinOutX = xSlopeUTFit * rsqrt( 1.0f + xSlopeUTFit * xSlopeUTFit + ty * ty );
      return ( sinInX - sinOutX );
    }

    // -- Evaluate the linear discriminant
    // -- Coefficients derived with LD method for p, pT and chi2 with TMVA
    template <int nHits>
    simd::float_v evaluateLinearDiscriminantSIMD( const std::array<simd::float_v, 3>& inputValues ) {

      constexpr auto coeffs =
          ( nHits == 3 ? std::array{0.162880166064f, -0.107081172665f, 0.134153123662f, -0.137764853657f}
                       : std::array{0.235010729187f, -0.0938323617311f, 0.110823681145f, -0.170467109599f} );

      assert( coeffs.size() == inputValues.size() + 1 );

      return simd::float_v{coeffs[0]} + coeffs[1] * log<simd::float_v>( inputValues[0] ) +
             coeffs[2] * log<simd::float_v>( inputValues[1] ) + coeffs[3] * log<simd::float_v>( inputValues[2] );
    }

    /*
    simd::float_v calcXTol( const simd::float_v minMom, const simd::float_v ty ) {
      return ( 38000.0f / minMom + 0.25f ) * ( 1.0f + ty * ty * 0.8f );
    }
    */
    // --------------------------------------------------------------------
    // -- Helper function to calculate the planeCode: 0 - 1 - 2 - 3
    // --------------------------------------------------------------------
    int planeCode( unsigned int id ) {
      const int station = ( (unsigned int)id & static_cast<unsigned int>( UTInfo::MasksBits::StationMask ) ) >>
                          static_cast<int>( UTInfo::MasksBits::StationBits );
      const int layer = ( (unsigned int)id & static_cast<unsigned int>( UTInfo::MasksBits::LayerMask ) ) >>
                        static_cast<int>( UTInfo::MasksBits::LayerBits );
      return 2 * ( station - 1 ) + ( layer - 1 );
    }

    // --------------------------------------------------------------------
    // -- Helper function to find duplicates in hits in the output
    // --------------------------------------------------------------------
    [[maybe_unused]] bool findDuplicates( const Upstream::Tracks& outputTracks ) {
      auto const uttracks = LHCb::Pr::make_zip<SIMDWrapper::InstructionSet::Scalar>( outputTracks );
      for ( auto const& track : uttracks ) {
        std::vector<int> IDs;
        IDs.reserve( track.nUTHits().cast() );
        for ( int h = track.nVPHits().cast(); h < track.nHits().cast(); h++ ) {
          const int id = track.lhcbID( h ).cast();
          IDs.push_back( id );
        }
        std::sort( IDs.begin(), IDs.end() );
        if ( std::adjacent_find( IDs.begin(), IDs.end() ) != IDs.end() ) return false;
      }
      return true;
    }
    // --------------------------------------------------------------------

    // -- bubble sort is slow, but we never have more than 9 elements (horizontally)
    // -- and can act on 8 elements at once vertically (with AVX)
    void bubbleSortSIMD(
        const int maxColsMaxRows,
        std::array<simd::int_v, maxNumSectors* static_cast<int>( UTInfo::DetectorNumbers::TotalLayers )>& helper,
        const int                                                                                         start ) {
      for ( int i = 0; i < maxColsMaxRows - 1; i++ ) {
        for ( int j = 0; j < maxColsMaxRows - i - 1; j++ ) {
          swap( helper[start + j] > helper[start + j + 1], helper[start + j], helper[start + j + 1] );
        }
      }
    }

    // -- not sure that is the smartest solution
    // -- but I could not come up with anything better
    // -- inspired by: https://lemire.me/blog/2017/04/10/removing-duplicates-from-lists-quickly/
    simd::int_v makeUniqueSIMD(
        std::array<simd::int_v, maxNumSectors* static_cast<int>( UTInfo::DetectorNumbers::TotalLayers )>& out,
        int start, size_t len ) {
      simd::int_v pos  = start + 1;
      simd::int_v oldv = out[start];
      for ( size_t j = start + 1; j < start + len; ++j ) {
        simd::int_v  newv      = out[j];
        simd::mask_v blendMask = ( newv == oldv );
        for ( size_t k = j + 1; k < start + len; ++k ) { out[k - 1] = select( blendMask, out[k], out[k - 1] ); }
        oldv = newv;
        pos  = pos + select( blendMask, simd::int_v{0}, simd::int_v{1} );
      }
      return pos;
    }

    // -- These things are all hardcopied from the PrTableForFunction
    // -- and PrUTMagnetTool
    // -- If the granularity or whatever changes, this will give wrong results
    simd::int_v masterIndexSIMD( const simd::int_v index1, const simd::int_v index2, const simd::int_v index3 ) {
      return ( index3 * 11 + index2 ) * 31 + index1;
    }

    constexpr auto minValsBdl = std::array{-0.3f, -250.0f, 0.0f};
    constexpr auto maxValsBdl = std::array{0.3f, 250.0f, 800.0f};
    constexpr auto deltaBdl   = std::array{0.02f, 50.0f, 80.0f};
    // constexpr auto dxDyHelper = std::array{0.0f, 1.0f, -1.0f, 0.0f};
    // ===========================================================================================
    // -- 2 helper functions for fit
    // -- Pseudo chi2 fit, templated for 3 or 4 hits
    // ===========================================================================================
    void addHit( span<float, 3> mat, span<float, 2> rhs, const LHCb::Pr::UT::Mut::Hits& hits, int index,
                 float zMidUT ) {
      const float ui = hits.xs[index];
      const float ci = hits.coss[index];
      const float dz = 0.001f * ( hits.zs[index] - zMidUT );
      const float wi = hits.weights[index];
      mat[0] += wi * ci;
      mat[1] += wi * ci * dz;
      mat[2] += wi * ci * dz * dz;
      rhs[0] += wi * ui;
      rhs[1] += wi * ui * dz;
    }
    template <std::size_t N>
    void simpleFit( const std::array<int, N>& indices, const LHCb::Pr::UT::Mut::Hits& hits, ProtoTracks& pTracks,
                    const int trackIndex, float zMidUT, float zKink, float invSigmaVeloSlope ) {
      static_assert( N == 3 || N == 4 );

      // -- Scale the z-component, to not run into numerical problems
      // -- with floats
      const float wb              = pTracks.wb<scalar::float_v>( 0 ).cast();
      const float xMidField       = pTracks.xMidField<scalar::float_v>( 0 ).cast();
      const float invKinkVeloDist = pTracks.invKinkVeloDist<scalar::float_v>( 0 ).cast();
      const float stateX          = pTracks.pos<scalar::float_v>( trackIndex ).x.cast();
      const float stateTx         = pTracks.dir<scalar::float_v>( trackIndex ).x.cast();

      const float zDiff = 0.001f * ( zKink - zMidUT );
      auto        mat   = std::array{wb, wb * zDiff, wb * zDiff * zDiff};
      auto        rhs   = std::array{wb * xMidField, wb * xMidField * zDiff};

      std::for_each( indices.begin(), indices.end(),
                     [&]( const auto index ) { addHit( mat, rhs, hits, index, zMidUT ); } );

      ROOT::Math::CholeskyDecomp<float, 2> decomp( mat.data() );
      if ( UNLIKELY( !decomp ) ) return;

      decomp.Solve( rhs );

      const float xSlopeTTFit = 0.001f * rhs[1];
      const float xTTFit      = rhs[0];

      // new VELO slope x
      const float xb            = xTTFit + xSlopeTTFit * ( zKink - zMidUT );
      const float xSlopeVeloFit = ( xb - stateX ) * invKinkVeloDist;
      const float chi2VeloSlope = ( stateTx - xSlopeVeloFit ) * invSigmaVeloSlope;

      const float chi2TT =
          std::accumulate( indices.begin(), indices.end(), chi2VeloSlope * chi2VeloSlope,
                           [&]( float chi2, const int index ) {
                             const float du = ( xTTFit + xSlopeTTFit * ( hits.zs[index] - zMidUT ) ) - hits.xs[index];
                             return chi2 + hits.weights[index] * ( du * du );
                           } ) /
          ( N + 1 - 2 );

      if ( chi2TT < pTracks.chi2TT<scalar::float_v>( trackIndex ).cast() ) {

        // calculate q/p
        const float sinInX  = xSlopeVeloFit * vdt::fast_isqrtf( 1.0f + xSlopeVeloFit * xSlopeVeloFit );
        const float sinOutX = xSlopeTTFit * vdt::fast_isqrtf( 1.0f + xSlopeTTFit * xSlopeTTFit );
        const float qp      = ( sinInX - sinOutX );

        pTracks.store_chi2TT<scalar::float_v>( trackIndex, chi2TT );
        pTracks.store_qp<scalar::float_v>( trackIndex, qp );
        pTracks.store_xTT<scalar::float_v>( trackIndex, xTTFit );
        pTracks.store_xSlopeTT<scalar::float_v>( trackIndex, xSlopeTTFit );

        for ( std::size_t i = 0; i < N; i++ ) { pTracks.store_hitIndex<scalar::int_v>( trackIndex, i, indices[i] ); }
        if constexpr ( N == 3 ) { pTracks.store_hitIndex<scalar::int_v>( trackIndex, 3, -1 ); }
      }
    }
  } // namespace

  //=============================================================================
  // Standard constructor, initializes variables
  //=============================================================================
  VeloUT::VeloUT( const std::string& name, ISvcLocator* pSvcLocator )
      : Transformer( name, pSvcLocator,
                     {KeyValue{"InputTracksName", "Rec/Track/Velo"}, KeyValue{"UTHits", ::UT::Info::HitLocation},
                      KeyValue{"GeometryInfo", "AlgorithmSpecific-" + name + "-UTGeometryInfo"}},
                     KeyValue{"OutputTracksName", "Rec/Track/UT"} ) {}

  /// Initialization
  StatusCode VeloUT::initialize() {

    return Transformer::initialize().andThen( [&] { return m_PrUTMagnetTool.retrieve(); } ).andThen( [&] {
      // m_zMidUT is a position of normalization plane which should to be close to z middle of UT ( +- 5 cm ).
      // Cached once in VeloUTTool at initialization. No need to update with small UT movement.
      m_zMidUT = m_PrUTMagnetTool->zMidUT();
      // zMidField and distToMomentum is properly recalculated in PrUTMagnetTool when B field changes
      m_distToMomentum = m_PrUTMagnetTool->averageDist2mom();

      if ( m_doTiming ) {
        m_timerTool->increaseIndent();
        m_veloUTTime = m_timerTool->addTimer( "Internal VeloUT Tracking" );
        m_timerTool->decreaseIndent();
      }
      addConditionDerivation<UTDAQ::GeomCache( const DeUTDetector& )>( {DeUTDetLocation::UT},
                                                                       inputLocation<UTDAQ::GeomCache>() );
    } );
  }
  //=============================================================================
  // Main execution
  //=============================================================================
  Upstream::Tracks VeloUT::operator()( const EventContext& evtCtx, const Velo::Tracks& inputTracks,
                                       const LHCb::Pr::UT::HitHandler& hh, const UTDAQ::GeomCache& geometry ) const {

    if ( m_doTiming ) m_timerTool->start( m_veloUTTime );
    Upstream::Tracks outputTracks{&inputTracks, Zipping::generateZipIdentifier(), LHCb::getMemResource( evtCtx )};
    outputTracks.reserve( inputTracks.size() );
    m_seedsCounter += inputTracks.size();

    // const auto& fudgeFactors = m_PrUTMagnetTool->DxLayTable();
    const auto& bdlTable = m_PrUTMagnetTool->BdlTable();

    MiniStatesArray filteredStates = getStates( inputTracks, outputTracks );

    std::array<ExtrapolatedStates, 4> extrapStatesArray = extrapStates( filteredStates, geometry );

    auto compBoundsArray = findAllSectors( extrapStatesArray, filteredStates, geometry );

    std::array<LHCb::Pr::UT::Mut::Hits, batchSize> hitsInLayers;

    ProtoTracks pTracks;

    // -- We cannot put all found hits in an array, as otherwise the stack overflows
    // -- so we just do the whole thing in batches
    const std::size_t filteredStatesSize = filteredStates.size;

    for ( std::size_t t = 0; t < filteredStatesSize; t += batchSize ) {

      // -- This is scalar, as the hits are found in a scalar way
      filteredStates.size = 0;
      for ( std::size_t t2 = 0; t2 < batchSize && t2 + t < filteredStatesSize; ++t2 ) {
        for ( auto& it : hitsInLayers[filteredStates.size].layerIndices ) it = -1;
        hitsInLayers[filteredStates.size].size = 0;
        const bool foundHits =
            getHitsScalar( hh, filteredStates, compBoundsArray, hitsInLayers[filteredStates.size], t + t2 );
        filteredStates.copyBack<scalar>( t + t2, foundHits );
      }

      pTracks.size = 0;
      for ( std::size_t tEff = 0; tEff < filteredStates.size; tEff++ ) {

        Vec3<scalar::float_v> pos = filteredStates.pos<scalar::float_v>( tEff );
        Vec3<scalar::float_v> dir = filteredStates.dir<scalar::float_v>( tEff );

        int trackIndex = pTracks.size;
        pTracks.fillHelperParams<scalar>( pos, dir, c_zKink, c_sigmaVeloSlope );
        pTracks.store_pos<scalar::float_v>( trackIndex, pos );
        pTracks.store_dir<scalar::float_v>( trackIndex, dir );
        pTracks.store_chi2TT<scalar::float_v>( trackIndex, m_maxPseudoChi2.value() );

        pTracks.store_hitIndex<scalar::int_v>( trackIndex, 0, -1 );
        if ( !formClusters<true>( hitsInLayers[tEff], pTracks, trackIndex ) ) {
          formClusters<false>( hitsInLayers[tEff], pTracks, trackIndex );
        }
        if ( pTracks.hitIndex<scalar::int_v>( trackIndex, 0 ).cast() == -1 ) continue;

        scalar::int_v ancestorIndex = filteredStates.index<scalar::int_v>( tEff );
        pTracks.store_index<scalar::int_v>( trackIndex, ancestorIndex );
        pTracks.store_hitContIndex<scalar::int_v>( trackIndex, tEff );

        // -- this runs over all 4 layers, even if no hit was found
        // -- but it fills a weight of 0
        // -- Note: These are not "physical" layers, as the hits are ordered such that only
        // -- the last one can be not filled.
        for ( int i = 0; i < static_cast<int>( UTInfo::DetectorNumbers::TotalLayers ); ++i ) {
          int             hitI   = pTracks.hitIndex<scalar::int_v>( trackIndex, i ).cast();
          scalar::float_v weight = ( hitI == -1 ) ? 0.0f : hitsInLayers[tEff].weights[hitI];
          pTracks.store_weight<scalar::float_v>( trackIndex, i, weight );

          hitI = std::max( 0, hitI ); // prevent index out of bound

          pTracks.store_x<scalar::float_v>( trackIndex, i, hitsInLayers[tEff].xs[hitI] );
          pTracks.store_z<scalar::float_v>( trackIndex, i, hitsInLayers[tEff].zs[hitI] );
          pTracks.store_sin<scalar::float_v>( trackIndex, i, hitsInLayers[tEff].sins[hitI] );

          LHCb::LHCbID id( LHCb::UTChannelID( hitsInLayers[tEff].channelIDs[hitI] ) );
          pTracks.store_id<scalar::int_v>( trackIndex, i, id.lhcbID() ); // not sure if correct
          pTracks.store_hitIndex<scalar::int_v>( trackIndex, i,
                                                 hitsInLayers[tEff].indexs[hitI] ); // not sure if correct
        }
        pTracks.size++;
      }

      // padding to avoid FPEs
      if ( ( pTracks.size + simd::size ) < batchSize ) {
        pTracks.store_pos<simd::float_v>( pTracks.size, Vec3<simd::float_v>( 1.f, 1.f, 1.f ) );
        pTracks.store_dir<simd::float_v>( pTracks.size, Vec3<simd::float_v>( 1.f, 1.f, 1.f ) );
      }
      prepareOutputTrackSIMD( pTracks, hitsInLayers, outputTracks, inputTracks, bdlTable );
    }

    // -- The algorithm should not store duplicated hits...
    assert( findDuplicates( outputTracks ) && "Hit duplicates found" );

    m_tracksCounter += outputTracks.size();
    if ( m_doTiming ) m_timerTool->stop( m_veloUTTime );
    return outputTracks;
  }
  //=============================================================================
  // Get the state, do some cuts
  //=============================================================================
  MiniStatesArray VeloUT::getStates( const Velo::Tracks& inputTracks, Upstream::Tracks& outputTracks ) const {

    const int          EndVelo    = 1;
    const simd::mask_v passTracks = m_passTracks.value() ? simd::mask_true() : simd::mask_false();

    MiniStatesArray filteredStates;

    auto const velozipped = LHCb::Pr::make_zip( inputTracks );
    for ( auto const& velotrack : velozipped ) {
      auto const loopMask = velotrack.loop_mask();
      auto const trackVP  = velotrack.indices();
      auto       pos      = velotrack.StatePos( EndVelo );
      auto       dir      = velotrack.StateDir( EndVelo );
      auto       covX     = velotrack.StateCovX( EndVelo );

      simd::float_v xMidUT = pos.x + dir.x * ( m_zMidUT - pos.z );
      simd::float_v yMidUT = pos.y + dir.y * ( m_zMidUT - pos.z );

      simd::mask_v centralHoleMask = xMidUT * xMidUT + yMidUT * yMidUT < simd::float_v{m_centralHoleSize.value()} *
                                                                             simd::float_v{m_centralHoleSize.value()};
      simd::mask_v slopesMask   = ( ( abs( dir.x ) > m_maxXSlope.value() ) || ( abs( dir.y ) > m_maxYSlope.value() ) );
      simd::mask_v passHoleMask = abs( xMidUT ) < m_passHoleSize.value() && abs( yMidUT ) < m_passHoleSize.value();
      simd::mask_v mask         = centralHoleMask || slopesMask;
      simd::mask_v csMask       = loopMask && !mask && ( !passTracks || !passHoleMask );

      int index = filteredStates.size;
      filteredStates.compressstore_pos<simd::float_v>( index, csMask, pos );
      filteredStates.compressstore_dir<simd::float_v>( index, csMask, dir );
      filteredStates.compressstore_cov<simd::float_v>( index, csMask, covX );
      filteredStates.compressstore_index<simd::int_v>( index, csMask, trackVP );
      filteredStates.size += simd::popcount( csMask );

      if ( m_passTracks ) {

        auto outMask = loopMask && passHoleMask; // not sure if correct...
        auto i       = outputTracks.size();
        outputTracks.resize( i + simd::popcount( outMask ) );
        outputTracks.store<TracksTag::trackVP>( i, velotrack.indices(), outMask );
        outputTracks.store<TracksTag::StateQoP>( i, simd::float_v{0.f}, outMask );
        outputTracks.store<TracksTag::nUTHits>( i, simd::int_v{0}, outMask );
        outputTracks.store<TracksTag::nVPHits>( i, velotrack.nHits(), outMask );
        outputTracks.store_StatePosDir( i, pos, dir, outMask );
        outputTracks.store_StateCovX( i, covX, outMask );
        for ( int idx = 0; idx < velotrack.nHits().hmax( outMask ); ++idx ) {
          outputTracks.store_vp_index( i, idx, velotrack.vp_index( idx ), outMask );
        }
      }
    }
    return filteredStates;
  }
  //=============================================================================
  // Extrapolate the states
  //=============================================================================
  std::array<ExtrapolatedStates, static_cast<int>( UTInfo::DetectorNumbers::TotalLayers )>
  VeloUT::extrapStates( const MiniStatesArray& filteredStates, const UTDAQ::GeomCache& geom ) const {

    std::array<ExtrapolatedStates, static_cast<int>( UTInfo::DetectorNumbers::TotalLayers )> eStatesArray;

    // -- Used for the calculation of the size of the search windows
    constexpr const std::array<float, static_cast<int>( UTInfo::DetectorNumbers::TotalLayers )> normFact{0.95f, 1.0f,
                                                                                                         1.36f, 1.41f};

    for ( std::size_t t = 0; t < filteredStates.size; t += simd::size ) {

      simd::float_v x  = filteredStates.x<simd::float_v>( t );
      simd::float_v y  = filteredStates.y<simd::float_v>( t );
      simd::float_v z  = filteredStates.z<simd::float_v>( t );
      simd::float_v tx = filteredStates.tx<simd::float_v>( t );
      simd::float_v ty = filteredStates.ty<simd::float_v>( t );

      // -- this 500 seems a little odd...
      const simd::float_v invTheta = min( 500.0f, 1.0f * rsqrt( tx * tx + ty * ty ) );
      const simd::float_v minMom   = max( m_minPT.value() * invTheta, m_minMomentum.value() );

      for ( int layerIndex = 0; layerIndex < static_cast<int>( UTInfo::DetectorNumbers::TotalLayers ); ++layerIndex ) {

        const simd::float_v xTol = abs( 1.0f / ( m_distToMomentum * minMom ) ) * normFact[layerIndex];
        const simd::float_v yTol = m_yTol.value() + m_yTolSlope.value() * xTol;

        const simd::float_v zGeo{geom.layers[layerIndex].z};
        const simd::float_v dxDy{geom.layers[layerIndex].dxDy};

        const simd::float_v yAtZ   = y + ty * ( zGeo - z );
        const simd::float_v xLayer = x + tx * ( zGeo - z );
        const simd::float_v yLayer = yAtZ + yTol * dxDy;

        eStatesArray[layerIndex].store_xLayer<simd::float_v>( t, xLayer );
        eStatesArray[layerIndex].store_yLayer<simd::float_v>( t, yLayer );
        eStatesArray[layerIndex].store_xTol<simd::float_v>( t, xTol );
        eStatesArray[layerIndex].store_tx<simd::float_v>( t, tx );
      }
    }

    eStatesArray[0].size = filteredStates.size;
    eStatesArray[1].size = filteredStates.size;
    eStatesArray[2].size = filteredStates.size;
    eStatesArray[3].size = filteredStates.size;

    return eStatesArray;
  }
  //=============================================================================
  // -- find the sectors
  //=============================================================================
  std::array<Boundaries, static_cast<int>( UTInfo::DetectorNumbers::TotalLayers )> VeloUT::findAllSectors(
      const std::array<ExtrapolatedStates, static_cast<int>( UTInfo::DetectorNumbers::TotalLayers )>& eStatesArray,
      MiniStatesArray& filteredStates, const UTDAQ::GeomCache& geom ) const {

    std::array<Boundaries, static_cast<int>( UTInfo::DetectorNumbers::TotalLayers )> compBoundsArray;
    int                                                                              contSize = filteredStates.size;
    filteredStates.size                                                                       = 0;

    std::array<simd::int_v, static_cast<int>( UTInfo::DetectorNumbers::TotalLayers )> posArray{};
    std::array<simd::int_v, maxNumSectors* static_cast<int>( UTInfo::DetectorNumbers::TotalLayers )>
                                                                              helperArray{}; // 4 layers x maximum 9 sectors
    std::array<int, static_cast<int>( UTInfo::DetectorNumbers::TotalLayers )> maxColsRows;

    // -- This now works with up to 9 sectors
    for ( int t = 0; t < contSize; t += simd::size ) {
      auto loopMask = simd::loop_mask( t, contSize );

      simd::int_v nLayers{0};

      for ( int layerIndex = 0; layerIndex < static_cast<int>( UTInfo::DetectorNumbers::TotalLayers ); ++layerIndex ) {

        const simd::int_v regionBoundary1 = ( 2 * geom.layers[layerIndex].nColsPerSide + 3 );
        const simd::int_v regionBoundary2 = ( 2 * geom.layers[layerIndex].nColsPerSide - 5 );

        simd::int_v subcolmin{0};
        simd::int_v subcolmax{0};
        simd::int_v subrowmin{0};
        simd::int_v subrowmax{0};

        simd::float_v xLayer = eStatesArray[layerIndex].xLayer<simd::float_v>( t );
        simd::float_v yLayer = eStatesArray[layerIndex].yLayer<simd::float_v>( t );
        simd::float_v xTol   = eStatesArray[layerIndex].xTol<simd::float_v>( t );
        simd::float_v tx     = eStatesArray[layerIndex].tx<simd::float_v>( t );

        simd::mask_v mask = UTDAQ::findSectors( layerIndex, xLayer, yLayer, xTol - abs( tx ) * m_intraLayerDist.value(),
                                                m_yTol.value() + m_yTolSlope.value() * abs( xTol ),
                                                geom.layers[layerIndex], subcolmin, subcolmax, subrowmin, subrowmax );

        const simd::mask_v gathermask = loopMask && mask;

        // -- Determine the maximum number of rows and columns we have to take into account
        // -- maximum 3, minimum 0
        // -- The 'clamp' is needed to prevent large negative values from 'hmax' when gathermask has no true entries
        const int maxCols = std::clamp( ( subcolmax - subcolmin ).hmax( gathermask ) + 1, 0, maxNumCols );
        const int maxRows = std::clamp( ( subrowmax - subrowmin ).hmax( gathermask ) + 1, 0, maxNumRows );

        maxColsRows[layerIndex] = maxCols * maxRows;

        int counter = 0;
        for ( int sc = 0; sc < maxCols; sc++ ) {

          simd::int_v realSC = min( subcolmax, subcolmin + sc );
          // -- Gives the region (actually region - 1): left 0, center 1, right 2
          simd::int_v region = select( realSC > regionBoundary1, simd::int_v{1}, simd::int_v{0} ) +
                               select( realSC > regionBoundary2, simd::int_v{1}, simd::int_v{0} );

          for ( int sr = 0; sr < maxRows; sr++ ) {

            simd::int_v realSR = min( subrowmax, subrowmin + sr );
            simd::int_v sectorIndex =
                realSR + static_cast<int>( UTInfo::SectorNumbers::EffectiveSectorsPerColumn ) * realSC;

            // -- only gather when we are not outside the acceptance
            // -- if we are outside, fill 1 which is the lowest possible sector number
            // -- We need to fill a valid number, as one can have 3 layers with a correct sector
            // -- and one without a correct sector, in which case the track will not be masked off.
            // -- However, these cases should happen very rarely
            simd::int_v sect = ( layerIndex < 2 )
                                   ? geom.sectorLUT.maskgather_station1<simd::int_v>( sectorIndex, gathermask, 1 )
                                   : geom.sectorLUT.maskgather_station2<simd::int_v>( sectorIndex, gathermask, 1 );

            // -- ID is: sectorIndex (from LUT) + (layerIndex * 3 + region - 1 ) * 98
            // -- The regions are already calculated with a -1
            helperArray[maxNumSectors * layerIndex + counter] =
                sect +
                ( layerIndex * static_cast<int>( UTInfo::DetectorNumbers::Regions ) + region ) *
                    static_cast<int>( UTInfo::SectorNumbers::MaxSectorsPerRegion ) -
                1;
            counter++;
          }
        }

        // -- This is sorting
        bubbleSortSIMD( maxCols * maxRows, helperArray, maxNumSectors * layerIndex );
        // -- This is uniquifying
        posArray[layerIndex] = makeUniqueSIMD( helperArray, maxNumSectors * layerIndex, maxCols * maxRows );
        // -- count the number of layers which are 'valid'
        nLayers += select( mask, simd::int_v{1}, simd::int_v{0} );
      }

      // -- We need at least three layers
      const simd::mask_v compressMask = ( nLayers > 2 ) && loopMask;

      for ( int iLayer = 0; iLayer < static_cast<int>( UTInfo::DetectorNumbers::TotalLayers ); ++iLayer ) {
        int index = compBoundsArray[iLayer].size;
        if ( index + simd::popcount( compressMask ) > LHCb::Pr::Boundaries::max_tracks ) {
          error() << "Reach the maximum number of tracks in Boundaries!!" << endmsg;
          ++m_too_much_in_boundaries;
          break;
        }
        for ( int iSector = 0; iSector < maxColsRows[iLayer]; ++iSector ) {
          compBoundsArray[iLayer].compressstore_sect<simd::int_v>( index, iSector, compressMask,
                                                                   helperArray[maxNumSectors * iLayer + iSector] );
        }
        simd::float_v xTol = eStatesArray[iLayer].xTol<simd::float_v>( t );
        compBoundsArray[iLayer].compressstore_xTol<simd::float_v>( index, compressMask, xTol );
        compBoundsArray[iLayer].compressstore_nPos<simd::int_v>( index, compressMask,
                                                                 posArray[iLayer] - maxNumSectors * iLayer );
        compBoundsArray[iLayer].size += simd::popcount( compressMask );
      }

      // -- Now need to compress the filtered states, such that they are
      // -- in sync with the sectors
      if ( filteredStates.size + simd::popcount( compressMask ) > LHCb::Pr::MiniStatesArray::max_tracks ) {
        ++m_too_much_in_filtered;
        break;
      }
      filteredStates.copyBack<simd>( t, compressMask );
    }

    return compBoundsArray;
  }
  //=============================================================================
  // Find the hits
  //=============================================================================
  bool VeloUT::getHitsScalar(
      const LHCb::Pr::UT::HitHandler& hh, const MiniStatesArray& filteredStates,
      const std::array<Boundaries, static_cast<int>( UTInfo::DetectorNumbers::TotalLayers )>& compBoundsArray,
      LHCb::Pr::UT::Mut::Hits& hitsInLayers, const std::size_t t ) const {

    // -- This is for some sanity checks later
    constexpr const int maxSectorsPerRegion = static_cast<int>( UTInfo::SectorNumbers::MaxSectorsPerRegion );
    constexpr const int maxLayer            = static_cast<int>( UTInfo::DetectorNumbers::TotalLayers );
    constexpr const int maxRegion           = static_cast<int>( UTInfo::DetectorNumbers::Regions );
    [[maybe_unused]] constexpr const int maxSectorNumber =
        maxSectorsPerRegion + ( ( maxLayer - 1 ) * maxRegion + ( maxRegion - 1 ) ) * maxSectorsPerRegion;

    const simd::float_v yTolSlope{m_yTolSlope.value()};

    const float xState  = filteredStates.x<scalar::float_v>( t ).cast();
    const float yState  = filteredStates.y<scalar::float_v>( t ).cast();
    const float zState  = filteredStates.z<scalar::float_v>( t ).cast();
    const float txState = filteredStates.tx<scalar::float_v>( t ).cast();
    const float tyState = filteredStates.ty<scalar::float_v>( t ).cast();

    std::size_t nSize   = 0;
    std::size_t nLayers = 0;

    // -- the protos could be precomputed
    const simd::float_v yProto{yState - tyState * zState};
    const simd::float_v xOnTrackProto{xState - txState * zState};
    const simd::float_v ty{tyState};
    const simd::float_v tx{txState};

    for ( int layerIndex = 0; layerIndex < static_cast<int>( UTInfo::DetectorNumbers::TotalLayers ); ++layerIndex ) {

      if ( ( layerIndex == 2 && nLayers == 0 ) || ( layerIndex == 3 && nLayers < 2 ) ) return false;

      const float         xTolS = compBoundsArray[layerIndex].xTol<scalar::float_v>( t ).cast();
      const int           nPos  = compBoundsArray[layerIndex].nPos<scalar::int_v>( t ).cast();
      const simd::float_v yTol  = m_yTol.value() + m_yTolSlope.value() * xTolS;

      assert( nPos < maxNumSectors && "nPos out of bound" );

      const simd::float_v tolProto{m_yTol.value()};
      const simd::float_v xTol{xTolS};

      std::array<int, maxNumSectors + 1> sectors{0};

      for ( int i = 0; i < nPos; ++i ) {
        sectors[i] = std::min( maxSectorNumber - 1,
                               std::max( compBoundsArray[layerIndex].sect<scalar::int_v>( t, i ).cast(), 0 ) );
      }
      for ( int j = 0; j < nPos; j++ ) {

        assert( ( sectors[j] > -1 ) && ( sectors[j] < maxSectorNumber ) && "sector number out of bound" );
        if ( ( sectors[j] <= -1 ) || ( sectors[j] > maxSectorNumber ) ) {
          error() << "Sector number out of bound, something wrong!" << endmsg;
        }

        // -- let's try to make it branchless
        const std::pair<int, int>& temp       = hh.indices( sectors[j] );
        const std::pair<int, int>& temp2      = hh.indices( sectors[j + 1] );
        const int                  firstIndex = temp.first;
        // const int                  lastIndex = temp.second;
        const int shift =
            ( temp2.first == temp.second || ( temp.first == temp2.first && temp.second == temp2.second ) );
        if ( temp2.first == 0 && temp2.second == 0 && temp2.first != temp.second ) j += 1;
        const int lastIndex = ( shift == 1 && ( j + 1 ) < nPos ) ? temp2.second : temp.second;
        j += shift;
        findHits( hh, yProto, ty, tx, xOnTrackProto, tolProto, xTol, hitsInLayers, yTol, firstIndex, lastIndex );
      }

      nLayers += int( nSize != hitsInLayers.size );
      hitsInLayers.layerIndices[layerIndex] = nSize;
      nSize                                 = hitsInLayers.size;
    }
    // -- only use these hits, if we have at least 3 layers
    return nLayers > 2;
  }
  // ==============================================================================
  // -- Method that finds the hits in a given layer within a certain range
  // ==============================================================================
  inline void VeloUT::findHits( const LHCb::Pr::UT::HitHandler& hh, const simd::float_v& yProto,
                                const simd::float_v& ty, const simd::float_v& tx, const simd::float_v& xOnTrackProto,
                                const simd::float_v& tolProto, const simd::float_v& xTolNormFact,
                                LHCb::Pr::UT::Mut::Hits& mutHits, const simd::float_v& yTol, const int firstIndex,
                                const int lastIndex ) const {

    const LHCb::Pr::UT::Hits& myHits = hh.hits();

    for ( int i = firstIndex; i < lastIndex; i += simd::size ) {
      // -- Calculate distance between straight line extrapolation from Velo and hit position
      const simd::float_v yy       = yProto + ty * myHits.zAtYEq0<simd::float_v>( i );
      const simd::float_v xx       = myHits.xAtYEq0<simd::float_v>( i ) + yy * myHits.dxDy<simd::float_v>( i );
      const simd::float_v xOnTrack = xOnTrackProto + tx * myHits.zAtYEq0<simd::float_v>( i );
      const simd::float_v absdx    = abs( xx - xOnTrack );

      if ( none( absdx < xTolNormFact ) ) continue;
      auto loopMask = simd::loop_mask( i, lastIndex );

      // is there anything like minmax?
      const simd::float_v yMin = min( myHits.yBegin<simd::float_v>( i ), myHits.yEnd<simd::float_v>( i ) );
      const simd::float_v yMax = max( myHits.yBegin<simd::float_v>( i ), myHits.yEnd<simd::float_v>( i ) );

      const simd::float_v tol  = yTol + absdx * tolProto;
      auto                mask = ( yMin - tol < yy && yy < yMax + tol ) && ( absdx < xTolNormFact ) && loopMask;

      if ( none( mask ) ) continue;

      auto index = mutHits.size;

      if ( ( index + simd::size ) >= LHCb::Pr::UT::Mut::Hits::max_hits ) {
        error() << "Reached maximum number of hits. This is a temporary limitation and needs to be fixed" << endmsg;
        break;
      }
      mutHits.compressstore_x( index, mask, xx );
      mutHits.compressstore_z( index, mask, myHits.zAtYEq0<simd::float_v>( i ) );
      mutHits.compressstore_cos( index, mask, myHits.cos<simd::float_v>( i ) );
      mutHits.compressstore_sin( index, mask,
                                 myHits.cos<simd::float_v>( i ) * -1.0f * myHits.dxDy<simd::float_v>( i ) );
      mutHits.compressstore_weight( index, mask, myHits.weight<simd::float_v>( i ) );
      mutHits.compressstore_channelID( index, mask, myHits.channelID<simd::int_v>( i ) );
      mutHits.compressstore_index( index, mask, simd::indices( i ) ); // fill the index in the original hit container
      mutHits.size += simd::popcount( mask );
    }
  }
  //=========================================================================
  // Form clusters
  //=========================================================================
  template <bool forward>
  bool VeloUT::formClusters( const LHCb::Pr::UT::Mut::Hits& hitsInLayers, ProtoTracks& pTracks,
                             const int trackIndex ) const {

    const int begin0 = forward ? hitsInLayers.layerIndices[0] : hitsInLayers.layerIndices[3];
    const int end0   = forward ? hitsInLayers.layerIndices[1] : hitsInLayers.size;

    const int begin1 = forward ? hitsInLayers.layerIndices[1] : hitsInLayers.layerIndices[2];
    const int end1   = forward ? hitsInLayers.layerIndices[2] : hitsInLayers.layerIndices[3];

    const int begin2 = forward ? hitsInLayers.layerIndices[2] : hitsInLayers.layerIndices[1];
    const int end2   = forward ? hitsInLayers.layerIndices[3] : hitsInLayers.layerIndices[2];

    const int begin3 = forward ? hitsInLayers.layerIndices[3] : hitsInLayers.layerIndices[0];
    const int end3   = forward ? hitsInLayers.size : hitsInLayers.layerIndices[1];

    bool fourLayerSolution = false;

    const float stateTx = pTracks.dir<scalar::float_v>( trackIndex ).x.cast();

    // -- this is scalar for the moment
    for ( int i0 = begin0; i0 < end0; ++i0 ) {

      const float xhitLayer0 = hitsInLayers.xs[i0];
      const float zhitLayer0 = hitsInLayers.zs[i0];

      // Loop over Second Layer
      for ( int i2 = begin2; i2 < end2; ++i2 ) {

        const float xhitLayer2 = hitsInLayers.xs[i2];
        const float zhitLayer2 = hitsInLayers.zs[i2];

        const float tx = ( xhitLayer2 - xhitLayer0 ) / ( zhitLayer2 - zhitLayer0 );

        if ( std::abs( tx - stateTx ) > m_deltaTx ) continue;

        int   bestHit1Index = -1;
        float hitTol        = m_hitTol;

        for ( int i1 = begin1; i1 < end1; ++i1 ) {

          const float xhitLayer1 = hitsInLayers.xs[i1];
          const float zhitLayer1 = hitsInLayers.zs[i1];

          const float xextrapLayer1 = xhitLayer0 + tx * ( zhitLayer1 - zhitLayer0 );
          if ( std::abs( xhitLayer1 - xextrapLayer1 ) < hitTol ) {
            hitTol        = std::abs( xhitLayer1 - xextrapLayer1 );
            bestHit1Index = i1;
          }
        }

        if ( fourLayerSolution && bestHit1Index == -1 ) continue;

        int bestHit3Index = -1;
        hitTol            = m_hitTol;
        for ( int i3 = begin3; i3 < end3; ++i3 ) {

          const float xhitLayer3 = hitsInLayers.xs[i3];
          const float zhitLayer3 = hitsInLayers.zs[i3];

          const float xextrapLayer3 = xhitLayer2 + tx * ( zhitLayer3 - zhitLayer2 );

          if ( std::abs( xhitLayer3 - xextrapLayer3 ) < hitTol ) {
            hitTol        = std::abs( xhitLayer3 - xextrapLayer3 );
            bestHit3Index = i3;
          }
        }

        // -- All hits found
        if ( bestHit1Index != -1 && bestHit3Index != -1 ) {
          simpleFit( std::array{i0, bestHit1Index, i2, bestHit3Index}, hitsInLayers, pTracks, trackIndex, m_zMidUT,
                     c_zKink, c_invSigmaVeloSlope );

          if ( !fourLayerSolution && pTracks.hitIndex<scalar::int_v>( trackIndex, 0 ).cast() != -1 ) {
            fourLayerSolution = true;
          }
          continue;
        }

        // -- Nothing found in layer 3
        if ( !fourLayerSolution && bestHit1Index != -1 ) {
          simpleFit( std::array{i0, bestHit1Index, i2}, hitsInLayers, pTracks, trackIndex, m_zMidUT, c_zKink,
                     c_invSigmaVeloSlope );
          continue;
        }
        // -- Noting found in layer 1
        if ( !fourLayerSolution && bestHit3Index != -1 ) {
          simpleFit( std::array{i0, bestHit3Index, i2}, hitsInLayers, pTracks, trackIndex, m_zMidUT, c_zKink,
                     c_invSigmaVeloSlope );
          continue;
        }
      }
    }
    return fourLayerSolution;
  }
  //=========================================================================
  // Create the Velo-UT tracks
  //=========================================================================
  template <typename BdlTable>
  void VeloUT::prepareOutputTrackSIMD( const ProtoTracks&                                    protoTracks,
                                       const std::array<LHCb::Pr::UT::Mut::Hits, batchSize>& hitsInLayers,
                                       Upstream::Tracks& outputTracks, const Velo::Tracks& inputTracks,
                                       const BdlTable& bdlTable ) const {
    auto const velozipped = LHCb::Pr::make_zip( inputTracks );
    for ( std::size_t t = 0; t < protoTracks.size; t += simd::size ) {

      //== Handle states. copy Velo one, add TT.
      const simd::float_v zOrigin =
          select( protoTracks.dir<simd::float_v>( t ).y > 0.001f,
                  protoTracks.pos<simd::float_v>( t ).z -
                      protoTracks.pos<simd::float_v>( t ).y / protoTracks.dir<simd::float_v>( t ).y,
                  protoTracks.pos<simd::float_v>( t ).z -
                      protoTracks.pos<simd::float_v>( t ).x / protoTracks.dir<simd::float_v>( t ).x );

      auto loopMask = simd::loop_mask( t, protoTracks.size );
      // -- this is to filter tracks where the fit had a too large chi2
      simd::mask_v fourHitTrack = protoTracks.weight<simd::float_v>( t, 3 ) > 0.0001f;

      // const float bdl1    = m_PrUTMagnetTool->bdlIntegral(helper.state.ty,zOrigin,helper.state.z);

      // -- These are calculations, copied and simplified from PrTableForFunction
      // -- FIXME: these rely on the internal details of PrTableForFunction!!!
      //           and should at least be put back in there, and used from here
      //           to make sure everything _stays_ consistent...
      auto var = std::array{protoTracks.dir<simd::float_v>( t ).y, zOrigin, protoTracks.pos<simd::float_v>( t ).z};

      simd::int_v index1 = min( max( simd::int_v{( var[0] + 0.3f ) / 0.6f * 30}, 0 ), 30 );
      simd::int_v index2 = min( max( simd::int_v{( var[1] + 250 ) / 500 * 10}, 0 ), 10 );
      simd::int_v index3 = min( max( simd::int_v{var[2] / 800 * 10}, 0 ), 10 );

      simd::float_v bdl = gather( bdlTable.table().data(), masterIndexSIMD( index1, index2, index3 ) );

      // -- TODO: check if we can go outside this table...
      const std::array<simd::float_v, 3> bdls =
          std::array{gather( bdlTable.table().data(), masterIndexSIMD( index1 + 1, index2, index3 ) ),
                     gather( bdlTable.table().data(), masterIndexSIMD( index1, index2 + 1, index3 ) ),
                     gather( bdlTable.table().data(), masterIndexSIMD( index1, index2, index3 + 1 ) )};

      const std::array<simd::float_v, 3> boundaries = {-0.3f + simd::float_v{index1} * deltaBdl[0],
                                                       -250.0f + simd::float_v{index2} * deltaBdl[1],
                                                       0.0f + simd::float_v{index3} * deltaBdl[2]};

      // -- This is an interpolation, to get a bit more precision
      simd::float_v addBdlVal{0.0f};
      for ( int i = 0; i < 3; ++i ) {

        // -- this should make sure that values outside the range add nothing to the sum
        var[i] = select( minValsBdl[i] > var[i], boundaries[i], var[i] );
        var[i] = select( maxValsBdl[i] < var[i], boundaries[i], var[i] );

        const simd::float_v dTab_dVar = ( bdls[i] - bdl ) / deltaBdl[i];
        const simd::float_v dVar      = ( var[i] - boundaries[i] );
        addBdlVal += dTab_dVar * dVar;
      }
      bdl += addBdlVal;
      // ----

      // -- order is: x, tx, y, chi2
      std::array<simd::float_v, 4> finalParams = {
          protoTracks.xTT<simd::float_v>( t ), protoTracks.xSlopeTT<simd::float_v>( t ),
          protoTracks.pos<simd::float_v>( t ).y +
              protoTracks.dir<simd::float_v>( t ).y * ( m_zMidUT - protoTracks.pos<simd::float_v>( t ).z ),
          protoTracks.chi2TT<simd::float_v>( t )};

      const simd::float_v qpxz2p  = -1.0f / bdl * 3.3356f / Gaudi::Units::GeV;
      simd::mask_v        fitMask = simd::mask_true();
      simd::float_v       qp = m_finalFit ? fastfitterSIMD( finalParams, protoTracks, m_zMidUT, qpxz2p, t, fitMask )
                                    : protoTracks.qp<simd::float_v>( t ) *
                                          rsqrt( 1.0f + protoTracks.dir<simd::float_v>( t ).y *
                                                            protoTracks.dir<simd::float_v>( t ).y ); // is this correct?

      qp                      = select( fitMask, qp, protoTracks.qp<simd::float_v>( t ) );
      const simd::float_v qop = select( abs( bdl ) < 1.e-8f, simd::float_v{1000.0f}, qp * qpxz2p );

      // -- Don't make tracks that have grossly too low momentum
      // -- Beware of the momentum resolution!
      const simd::float_v p = abs( 1.0f / qop );
      const simd::float_v pt =
          p * sqrt( protoTracks.dir<simd::float_v>( t ).x * protoTracks.dir<simd::float_v>( t ).x +
                    protoTracks.dir<simd::float_v>( t ).y * protoTracks.dir<simd::float_v>( t ).y );
      const simd::mask_v pPTMask = ( p > m_minMomentumFinal.value() && pt > m_minPTFinal.value() );

      const simd::float_v xUT  = finalParams[0];
      const simd::float_v txUT = finalParams[1];
      const simd::float_v yUT  = finalParams[2];

      // -- apply some fiducial cuts
      // -- they are optimised for high pT tracks (> 500 MeV)
      simd::mask_v fiducialMask = simd::mask_false();

      if ( m_fiducialCuts ) {
        const float magSign = m_magFieldSvc->signedRelativeCurrent();

        fiducialMask = ( magSign * qop < 0.0f && xUT > -48.0f && xUT < 0.0f && abs( yUT ) < 33.0f );
        fiducialMask = fiducialMask || ( magSign * qop > 0.0f && xUT < 48.0f && xUT > 0.0f && abs( yUT ) < 33.0f );

        fiducialMask = fiducialMask || ( magSign * qop < 0.0f && txUT > 0.09f + 0.0003f * pt );
        fiducialMask = fiducialMask || ( magSign * qop > 0.0f && txUT < -0.09f - 0.0003f * pt );
      }

      // -- evaluate the linear discriminant and reject ghosts
      // -- the values only make sense if the final fit is performed
      simd::mask_v mvaMask = simd::mask_true();

      if ( m_finalFit ) {

        const simd::float_v fourHitDisc  = evaluateLinearDiscriminantSIMD<4>( {p, pt, finalParams[3]} );
        const simd::float_v threeHitDisc = evaluateLinearDiscriminantSIMD<3>( {p, pt, finalParams[3]} );

        simd::mask_v fourHitMask  = fourHitDisc > m_LD4Hits.value();
        simd::mask_v threeHitMask = threeHitDisc > m_LD3Hits.value();

        // -- only have 3 or 4 hit tracks
        mvaMask = ( fourHitTrack && fourHitMask ) || ( !fourHitTrack && threeHitMask );
      }

      simd::mask_v validTrackMask = !fiducialMask && pPTMask && loopMask && mvaMask;

      // ==========================================================================================

      const simd::int_v ancestor      = protoTracks.index<simd::int_v>( t );
      auto              indices       = select( validTrackMask, ancestor, 0 );
      auto              velo_ancestor = velozipped.gather( indices );
      const int         EndVelo       = 1;
      auto const        velo_scalar   = velozipped.with<SIMDWrapper::InstructionSet::Scalar>();

      auto currentsize = outputTracks.size();
      outputTracks.resize( currentsize + simd::popcount( validTrackMask ) );
      outputTracks.store<TracksTag::StateQoP>( currentsize, qop, validTrackMask );
      outputTracks.store<TracksTag::trackVP>( currentsize, indices, validTrackMask );
      outputTracks.store<TracksTag::nVPHits>( currentsize, velo_ancestor.nHits(), validTrackMask );
      outputTracks.store_StatePosDir( currentsize, velo_ancestor.StatePos( EndVelo ), velo_ancestor.StateDir( EndVelo ),
                                      validTrackMask );
      outputTracks.store_StateCovX( currentsize, velo_ancestor.StateCovX( EndVelo ), validTrackMask );
      // store velo index and lhcbids
      for ( auto idx = 0; idx < velo_ancestor.nHits().hmax( validTrackMask ); ++idx ) {
        outputTracks.store_vp_index( currentsize, idx, velo_ancestor.vp_index( idx ), validTrackMask );
        outputTracks.store_lhcbID( currentsize, idx, velo_ancestor.lhcbID( idx ), validTrackMask );
      }

      float txArray[simd::size];
      txUT.store( txArray );
      float xArray[simd::size];
      xUT.store( xArray );
      int nUTHits[simd::size] = {0};
      // -- This is needed to find the planeCode of the layer with the missing hit
      float sumLayArray[simd::size] = {};

      // -- from here on, go over each track individually to find and add the overlap hits
      // -- this is not particularly elegant...
      // -- As before, these are "pseudo layers", i.e. it is not guaranteed that if i > j, z[i] > z[j]
      for ( int iLayer = 0; iLayer < static_cast<int>( UTInfo::DetectorNumbers::TotalLayers ); ++iLayer ) {

        int trackIndex2 = 0;
        for ( unsigned int t2 = 0; t2 < simd::size; ++t2 ) {
          if ( !testbit( validTrackMask, t2 ) ) continue;
          auto const veloIdx = protoTracks.index<scalar::int_v>( t + t2 ).cast();

          const auto tscalar = t + t2;

          const bool goodHit = ( protoTracks.weight<scalar::float_v>( tscalar, iLayer ).cast() > 0.0001f );
          const auto hitIdx  = protoTracks.hitIndex<scalar::int_v>( tscalar, iLayer );
          const auto id      = protoTracks.id<scalar::int_v>( tscalar, iLayer );

          // -- Only add the hit, if it is not in an empty layer (that sounds like a tautology,
          // -- but given that one always has 4 hits, even if only 3 make sense, it is needed)
          // -- Only the last pseudo-layer can be an empty layer
          if ( goodHit ) {
            auto const idIdx = velo_scalar[veloIdx].nHits().cast() + nUTHits[t2];
            outputTracks.store_ut_index( currentsize + trackIndex2, nUTHits[t2], hitIdx );
            outputTracks.store_lhcbID( currentsize + trackIndex2, idIdx, id );
            nUTHits[t2] += 1;
          }
          // --
          // -----------------------------------------------------------------------------------
          // -- The idea of the following code is: In layers where we have found a hit, we search for
          // -- overlap hits.
          // -- In layers where no hit was found initially, we use the better parametrization of the final
          // -- track fit to pick up hits that were lost in the initial search
          // -----------------------------------------------------------------------------------
          const float zhit         = goodHit ? protoTracks.z<scalar::float_v>( tscalar, iLayer ).cast() : m_zMidUT;
          const float xhit         = goodHit ? protoTracks.x<scalar::float_v>( tscalar, iLayer ).cast() : xArray[t2];
          const int   hitContIndex = protoTracks.hitContIndex<scalar::int_v>( tscalar ).cast();

          // -- The total sum of all plane codes is: 0 + 1 + 2 + 3 = 6
          // -- We can therefore get the plane code of the last pseudo-layer
          // -- as: 6 - sumOfAllOtherPlaneCodes
          const int pC = goodHit ? planeCode( id.cast() ) : 6 - sumLayArray[t2];
          sumLayArray[t2] += pC;

          const float txUTS = txArray[t2];

          const int begin = hitsInLayers[hitContIndex].layerIndices[pC];
          const int end =
              ( pC == 3 ) ? hitsInLayers[hitContIndex].size : hitsInLayers[hitContIndex].layerIndices[pC + 1];
          for ( int index2 = begin; index2 < end; ++index2 ) {
            const float zohit = hitsInLayers[hitContIndex].zs[index2];
            if ( zohit == zhit ) continue;

            const float xohit   = hitsInLayers[hitContIndex].xs[index2];
            const float xextrap = xhit + txUTS * ( zohit - zhit );
            if ( xohit - xextrap < -m_overlapTol ) continue;
            if ( xohit - xextrap > m_overlapTol ) break;

            if ( nUTHits[t2] >= int( LHCb::Pr::Upstream::Tracks::MaxUTHits ) )
              continue; // get this number from PrUpstreamTracks!!!
            const scalar::int_v utidx = hitsInLayers[hitContIndex].indexs[index2];
            outputTracks.store_ut_index( currentsize + trackIndex2, nUTHits[t2], utidx );
            LHCb::LHCbID        oid( LHCb::UTChannelID( hitsInLayers[hitContIndex].channelIDs[index2] ) );
            const scalar::int_v lhcbid = bit_cast<int, unsigned int>( oid.lhcbID() );
            auto const          idIdx  = velo_scalar[veloIdx].nHits().cast() + nUTHits[t2];
            outputTracks.store_lhcbID( currentsize + trackIndex2, idIdx, lhcbid );
            nUTHits[t2] += 1;
            // only one overlap hit
            break; // this should ensure there are never more than 8 hits on the track
          }
          trackIndex2++;
        }
      }
      const simd::int_v n_uthits = nUTHits;
      outputTracks.store<TracksTag::nUTHits>( currentsize, n_uthits, validTrackMask );
    }
  }
} // namespace LHCb::Pr
