/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/MCParticle.h"
#include "Event/MCTrackInfo.h"
#include "Event/Track.h"
#include "GaudiAlg/GaudiTupleTool.h"
#include "Linker/LinkedTo.h"
#include "PrKernel/IPrDebugMatchTool.h"
#include <algorithm>
#include <limits>
#include <map>
#include <string>
#include <vector>

//-----------------------------------------------------------------------------
// Implementation file for class : PrDebugMatchToolNN
//
// 2017-02-17 : Sevda
//-----------------------------------------------------------------------------

/** @class PrDebugMatchToolNN PrDebugMatchToolNN.h
 *
 * Debug tool for Matching algorithm
 *
 * @author Sevda Esen
 * @date   2017-02-21
 *	initial implementation
 */

struct PrDebugMatchToolNN : public extends<GaudiTupleTool, IPrDebugMatchTool> {

  using base_class::base_class;

  int matchMCPart( const LHCb::Track& velo, const LHCb::Track& seed ) const override;

  void fillTuple( const LHCb::Track& velo, const LHCb::Track& seed, const std::vector<float>& vars ) const override;
};

// Declaration of the Tool Factory
DECLARE_COMPONENT( PrDebugMatchToolNN )

//=============================================================================
void PrDebugMatchToolNN::fillTuple( const LHCb::Track& velo, const LHCb::Track& seed,
                                    const std::vector<float>& vars ) const {

  int   found = matchMCPart( velo, seed );
  Tuple tuple = nTuple( "tuple", "tuple" );

  tuple->column( "quality", found ).ignore();

  unsigned int i = 0;
  for ( auto var : vars ) {
    tuple->column( "var" + std::to_string( i ), var ).ignore();
    i++;
  }

  tuple->write().ignore();
}

//=============================================================================
int PrDebugMatchToolNN::matchMCPart( const LHCb::Track& velo, const LHCb::Track& seed ) const {

  LinkedTo<LHCb::MCParticle, LHCb::Track> myLinkVelo( evtSvc(), msgSvc(), LHCb::TrackLocation::Velo );
  LinkedTo<LHCb::MCParticle, LHCb::Track> myLinkSeed( evtSvc(), msgSvc(), LHCb::TrackLocation::Seed );

  LHCb::MCParticle* mcPartV   = myLinkVelo.first( velo.key() );
  const auto        trackInfo = MCTrackInfo{*get<LHCb::MCProperty>( LHCb::MCPropertyLocation::TrackInfo )};

  int found = 0;
  while ( mcPartV ) {
    if ( !trackInfo.hasVeloAndT( mcPartV ) ) {
      mcPartV = myLinkVelo.next();
      continue;
    }

    LHCb::MCParticle* mcPartS = myLinkSeed.first( seed.key() );
    while ( mcPartS ) {
      if ( mcPartV == mcPartS ) {

        if ( 11 == abs( mcPartV->particleID().pid() ) ) {
          found = -1;
          break;
        } else {
          found = 1;
          break;
        }
      } else
        mcPartS = myLinkSeed.next();
    }
    if ( found )
      break;
    else
      mcPartV = myLinkVelo.next();
  }

  return found;
}
