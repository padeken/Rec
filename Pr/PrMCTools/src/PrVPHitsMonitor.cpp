/*****************************************************************************\
 * * (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
 * *                                                                             *
 * * This software is distributed under the terms of the GNU General Public      *
 * * Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
 * *                                                                             *
 * * In applying this licence, CERN does not waive the privileges and immunities *
 * * granted to it by virtue of its status as an Intergovernmental Organization  *
 * * or submit itself to any jurisdiction.                                       *
 * \*****************************************************************************/

#include "DetDesc/GenericConditionAccessorHolder.h"
#include "DetDesc/IConditionDerivationMgr.h"
#include "Event/LinksByKey.h"
#include "Event/MCHit.h"
#include "Event/PrVeloHits.h"
#include "Event/VPFullCluster.h"
#include "GaudiAlg/Consumer.h"
#include "GaudiAlg/GaudiAlgorithm.h"
#include "GaudiAlg/GaudiHistoAlg.h"
#include "GaudiKernel/Vector3DTypes.h"
#include "Kernel/LHCbID.h"
#include "VPDet/DeVP.h"
#include <map>

//-----------------------------------------------------------------------------
// Implementation file for class : PrVPHitsMonitor
//
// 2020-04: valeriia lukashenko
//-----------------------------------------------------------------------------
// @class PrVPHitsMonitor PrVPHitsMonitor.h
// 	    * class for plotting redisuals from VPHits to MCHits
//
// 	    * isGlobal - if true use global coordinates, if false use local coordinates
// 	    * performStudy  - if true plot residuals, residulas vs VPCluster size, residuals per cluster size, residuals
// vs track angle. 	   		      if false just plot residuals
// 	    * min is the left side of the residuals range
// 	    * max is the right side of the residuals range
// 	    * nbins is number of the bins for residuals
//
//          *  - example to use it :
//         from Configurables import PrVPHitsMonitor
//         GaudiSequencer("CheckPatSeq").Members+=[PrVPHitsMonitor(performStudy=True)];
//         HistogramPersistencySvc().OutputFile = "VPHitsMonitor.root"
//
// *  @author valeriia lukashenko
// *  @date   2020-04-23
//
namespace {
  using MCHits      = LHCb::MCHits;
  using MCParticles = LHCb::MCParticles;
  using MCHit       = LHCb::MCHit;
  using Hits        = LHCb::Pr::Velo::Hits;
  using simd        = SIMDWrapper::scalar::types;
  using VPFullClus  = LHCb::VPFullCluster;

  Gaudi::XYZVector getResidual( const Gaudi::XYZPoint& hit, const Gaudi::XYZPoint& mchit ) { return hit - mchit; }
} // namespace
class PrVPHitsMonitor final
    : public Gaudi::Functional::Consumer<void( const Hits&, const MCHits&, const MCParticles&, const LHCb::LinksByKey&,
                                               const std::vector<VPFullClus>&, const DeVP& ),
                                         LHCb::DetDesc::usesBaseAndConditions<GaudiHistoAlg, DeVP>> {
public:
  PrVPHitsMonitor( const std::string& name, ISvcLocator* pSvcLocator )
      : Consumer{name,
                 pSvcLocator,
                 {KeyValue{"VPHitsLocation", "Raw/VP/Hits"}, KeyValue{"VPMCHitsLocation", LHCb::MCHitLocation::VP},
                  KeyValue{"MCParticleLocation", LHCb::MCParticleLocation::Default},
                  KeyValue{"MCHitToClusterLinkLocation", "Link/Pr/LHCbID"},
                  KeyValue{"VPClusterLocation", LHCb::VPFullClusterLocation::Default},
                  KeyValue{"DeVP", LHCb::Det::VP::det_path}}} {}

  void operator()( const Hits&, const MCHits&, const LHCb::MCParticles&, const LHCb::LinksByKey&,
                   const std::vector<VPFullClus>&, const DeVP& ) const override;

private:
  Gaudi::Property<bool>   m_isGlobal{this, "isGlobal", true, "Boolean for choosing global coordinate system"};
  Gaudi::Property<bool>   m_performStudy{this, "performStudy", false,
                                       "Boolean for performing studies with cluster size and diraction of mcparticle"};
  Gaudi::Property<double> m_min{this, "min_bin", -0.5, "Double for left boundary of residuals range"};
  Gaudi::Property<double> m_max{this, "max_bin", 0.5, "Double for right boundary of residuals range"};
  Gaudi::Property<int>    m_num_bins{this, "num_bins", 1000, "Int for number of bins"};

  mutable Gaudi::Accumulators::MsgCounter<MSG::ERROR>   m_nomchit{this, "No MCHit is found in the plane of VPHit"};
  mutable Gaudi::Accumulators::MsgCounter<MSG::WARNING> m_nomcparticle{this, "No MCParticle is found for MCHit"};
};

DECLARE_COMPONENT( PrVPHitsMonitor )
void PrVPHitsMonitor::operator()( const Hits& VPHits, const MCHits& mcVPHits, const LHCb::MCParticles&,
                                  const LHCb::LinksByKey& links, const std::vector<VPFullClus>& clusters,
                                  const DeVP& det ) const {
  if ( !m_performStudy ) { // in global coordinates
    for ( int ind = 0; ind != VPHits.size(); ++ind ) {

      // get vp channel id
      const simd::int_v num           = VPHits.gather_ChannelId<simd::int_v>( simd::int_v( ind ) );
      const int         unwrapped_num = num.cast();

      // get vp hit position
      const auto            vec = VPHits.gather_pos( simd::int_v( ind ) );
      const Gaudi::XYZPoint VPhit( vec.x.cast(), vec.y.cast(), vec.z.cast() );

      // get MCParticles that have the same VPChannelID
      LHCb::MCHit const* MChit{nullptr};
      float              max_weight{0};

      links.applyToLinks( unwrapped_num,
                          [&max_weight, &MChit, &mcVPHits]( unsigned int, unsigned int mcHitKey, float weight ) {
                            if ( weight > max_weight ) MChit = mcVPHits[mcHitKey];
                          } );

      if ( !MChit ) continue;

      const auto residual = getResidual( VPhit, MChit->midPoint() ); // calcualte the residual

      plot1D( residual.X(), "residualsX;#Delta X[mm]; counts", m_min, m_max, m_num_bins );
      plot1D( residual.Y(), "residualsY;#Delta Y[mm]; counts", m_min, m_max, m_num_bins );
      plot1D( residual.Z(), "residualsZ;#Delta Z[mm]; counts", m_min, m_max, m_num_bins );
    } // loop over hits
  }   // if not perform study

  if ( m_performStudy ) {

    for ( auto& ch : clusters ) {

      const unsigned int    channelID = ch.channelID();
      const Gaudi::XYZPoint VPCluster( ch.x(), ch.y(), ch.z() );

      const auto  id   = LHCb::LHCbID{LHCb::VPChannelID( channelID )};
      const auto& sens = det.sensor( id.vpID() );

      LHCb::MCHit const* MChit{nullptr};
      float              max_weight{0};

      links.applyToLinks( channelID,
                          [&max_weight, &MChit, &mcVPHits]( unsigned int, unsigned int mcHitKey, float weight ) {
                            if ( weight > max_weight ) MChit = mcVPHits[mcHitKey];
                          } );

      if ( !MChit ) continue;

      const auto MCparticle = MChit->mcParticle();

      const auto mcparticle_p = MCparticle->p();
      const auto momentum     = MCparticle->momentum();

      if ( mcparticle_p < 2000 ) continue;

      const auto residual = getResidual( VPCluster, MChit->midPoint() );
      const auto residual_local =
          getResidual( sens.globalToLocal( VPCluster ), sens.globalToLocal( MChit->midPoint() ) );

      const auto pixels = ch.pixels();

      plot1D( pixels.size(), "cluster_size; cluster size; counts", -0.5, 10.5, 11 );

      if ( m_isGlobal ) {
        plot1D( residual.X(), "residualsX;#Delta X[mm]; counts", m_min, m_max, m_num_bins );
        plot1D( residual.Y(), "residualsY;#Delta Y[mm]; counts", m_min, m_max, m_num_bins );
        plot1D( residual.Z(), "residualsZ;#Delta Z[mm]; counts", m_min, m_max, m_num_bins );

        plot2D( residual.X(), pixels.size(), "residuals_X_VS_cluster_size", m_min, m_max, -0.5, 10.5, m_num_bins / 11.,
                11 );
        plot2D( residual.Y(), pixels.size(), "residuals_Y_VS_cluster_size", m_min, m_max, -0.5, 10.5, m_num_bins / 11.,
                11 );
        plot2D( residual.Z(), pixels.size(), "residuals_Z_VS_cluster_size", m_min, m_max, -0.5, 10.5, m_num_bins / 11.,
                11 );
        plot2D( residual.X(), momentum.X() / momentum.Z(), "residuals_X_VS_track_slope_momentum", m_min, m_max, -1, 1,
                m_num_bins / 10., m_num_bins / 10. );
        plot2D( residual.Y(), momentum.Y() / momentum.Z(), "residuals_Y_VS_track_slope_momentum", m_min, m_max, -1, 1,
                m_num_bins / 10., m_num_bins / 10. );
        plot1D( residual.X(), "residualsX_cluster_size_" + std::to_string( pixels.size() ) + "; #Delta X[mm]; counts",
                m_min, m_max, m_num_bins );
        plot1D( residual.Y(), "residualsY_cluster_size_" + std::to_string( pixels.size() ) + "; #Delta Y[mm]; counts",
                m_min, m_max, m_num_bins );
        plot1D( residual.Z(), "residualsZ_cluster_size_" + std::to_string( pixels.size() ) + "; #Delta Z[mm]; counts",
                m_min, m_max, m_num_bins );

      } else {
        plot1D( residual.X(), "residuals_local_X;#Delta X[mm]; counts", m_min, m_max, m_num_bins );
        plot1D( residual.Y(), "residuals_local_Y;#Delta Y[mm]; counts", m_min, m_max, m_num_bins );
        plot1D( residual.Z(), "residuals_local_Z;#Delta Z[mm], counts", m_min, m_max, m_num_bins );

        plot2D( residual_local.X(), pixels.size(), "residuals_local_X_VS_cluster_size", m_min, m_max, -0.5, 10.5,
                m_num_bins / 11., 11 );
        plot2D( residual_local.Y(), pixels.size(), "residuals_local_Y_VS_cluster_size", m_min, m_max, -0.5, 10.5,
                m_num_bins / 11., 11 );
        plot2D( residual_local.X(), momentum.X() / momentum.Z(), "residuals_local_X_VS_track_slope_momentum", m_min,
                m_max, -1, 1, m_num_bins / 10., m_num_bins / 10. );
        plot2D( residual_local.Y(), momentum.Y() / momentum.Z(), "residuals_local_Y_VS_track_slope_momentum", m_min,
                m_max, -1, 1, m_num_bins / 10., m_num_bins / 10. );
        plot1D( residual_local.X(),
                "residuals_local_X_cluster_size_" + std::to_string( pixels.size() ) + "; #Delta X [mm]; counts", m_min,
                m_max, m_num_bins );
        plot1D( residual_local.Y(),
                "residuals_local_Y_cluster_size_" + std::to_string( pixels.size() ) + "; #Delta Y [mm]; counts", m_min,
                m_max, m_num_bins );
      } // if in local
    }   // loop over clusters
  }     // if perform study
}
