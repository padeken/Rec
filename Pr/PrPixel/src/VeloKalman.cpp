/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include <vector>

// Gaudi
#include "GaudiAlg/Transformer.h"

// LHCb
#include "Event/StateParameters.h"
#include "Event/Track.h"

#include "Event/PrFittedForwardTracks.h"
#include "Event/PrLongTracks.h"
#include "Event/PrVeloHits.h"
#include "Event/PrVeloTracks.h"

#include "PrKernel/PrPixelFastKalman.h"

#include "VeloKalmanHelpers.h"

/**
 * Velo only Kalman fit
 *
 * @author Arthur Hennequin (CERN, LIP6)
 */
namespace LHCb::Pr::Velo {

  namespace TrackTag = LHCb::Pr::Fitted::Forward::Tag;

  class Kalman : public Gaudi::Functional::Transformer<Fitted::Forward::Tracks( const EventContext&, const Hits&,
                                                                                const Tracks&, const Long::Tracks& )> {
    using TracksVP  = Tracks;
    using TracksFT  = Long::Tracks;
    using TracksFit = Fitted::Forward::Tracks;
    using simd      = SIMDWrapper::avx256::types;
    using I         = simd::int_v;
    using F         = simd::float_v;

  public:
    Kalman( const std::string& name, ISvcLocator* pSvcLocator )
        : Transformer( name, pSvcLocator,
                       {KeyValue{"HitsLocation", "Raw/VP/Hits"}, KeyValue{"TracksVPLocation", "Rec/Track/VP"},
                        KeyValue{"TracksFTLocation", "Rec/Track/FT"}},
                       KeyValue{"OutputTracksLocation", "Rec/Track/Fit"} ) {}

    StatusCode initialize() override {
      StatusCode sc = Transformer::initialize();
      if ( sc.isFailure() ) return sc;
      return StatusCode::SUCCESS;
    };

    TracksFit operator()( const EventContext& evtCtx, const Hits& hits, const TracksVP& tracksVP,
                          const TracksFT& tracksFT ) const override {
      // Forward tracks and its fit are zipable as there is a one to one correspondence.
      TracksFit out{&tracksFT, tracksFT.zipIdentifier(), LHCb::getMemResource( evtCtx )};
      out.reserve( tracksFT.size() );
      m_nbTracksCounter += tracksFT.size();

      auto const fttracks = LHCb::Pr::make_zip( tracksFT );
      for ( auto const& track : fttracks ) {
        auto       loop_mask = track.loop_mask();
        auto const idxVP     = track.trackVP();
        auto const qop       = track.qOverP();

        auto [stateInfo, chi2, nDof] = fitBackwardWithMomentum( loop_mask, tracksVP, idxVP, qop, hits, 0 );

        auto outTrack = out.emplace_back( tracksFT.size() );

        outTrack.field<TrackTag::trackFT>().set( track.indices() );
        outTrack.field<TrackTag::StateQoP>().set( qop );
        outTrack.field<TrackTag::Chi2>().set( chi2 / F( nDof ) );
        outTrack.field<TrackTag::Chi2nDoF>().set( nDof );
        outTrack.field<TrackTag::StatePosition>().setPosition( stateInfo.pos().x, stateInfo.pos().y,
                                                               stateInfo.pos().z );
        outTrack.field<TrackTag::StatePosition>().setDirection( stateInfo.dir().x, stateInfo.dir().y );

        outTrack.field<TrackTag::StateCovX>( 0 ).set( stateInfo.covX().x );
        outTrack.field<TrackTag::StateCovX>( 1 ).set( stateInfo.covX().y );
        outTrack.field<TrackTag::StateCovX>( 2 ).set( stateInfo.covX().z );

        outTrack.field<TrackTag::StateCovY>( 0 ).set( stateInfo.covY().x );
        outTrack.field<TrackTag::StateCovY>( 1 ).set( stateInfo.covY().y );
        outTrack.field<TrackTag::StateCovY>( 2 ).set( stateInfo.covY().z );
      }

      return out;
    };

  private:
    PrPixel::SimplifiedKalmanFilter<F, I> trackFitter;

    mutable Gaudi::Accumulators::SummingCounter<> m_nbTracksCounter{this, "Nb of Produced Tracks"};
  };
} // namespace LHCb::Pr::Velo

DECLARE_COMPONENT_WITH_ID( LHCb::Pr::Velo::Kalman, "VeloKalman" )
