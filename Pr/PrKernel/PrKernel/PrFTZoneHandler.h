/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef PRFTZONEHANDLER_H
#define PRFTZONEHANDLER_H 1

// Include files
#include "DetDesc/ValidDataObject.h"
#include "FTDet/DeFTDetector.h"
#include "Kernel/DetectorSegment.h"
#include "PrKernel/PrFTInfo.h"
#include "PrKernel/PrHitZone.h"

/** @class PrFTZoneHandler PrFTZoneHandler.h
 * Handlers of zones, the object is stored in the detector event store as a condition
 * and each algorithms reads the object from there without calling the HitManagers (tools)
 * @author Renato Quagliani
 * @author Sebastien Ponce
 */

class PrFTZoneHandler final : public ValidDataObject {

public:
  PrFTZoneHandler( DeFTDetector const& ftDet ) {
    for ( auto station : ftDet.stations() ) {
      for ( auto layer : station->layers() ) {
        int id = 4 * ( station->stationID() - 1 ) + layer->layerID();

        DetectorSegment seg( 0, layer->globalZ(), layer->dxdy(), layer->dzdy(), 0., 0. );
        float           xmax = 0.5f * layer->sizeX();
        float           ymax = 0.5f * layer->sizeY();

        // The setGeometry defines the z at y=0, the dxDy and the dzDy, as well as the isX properties of the zone.
        // This is important, since these are used in the following.
        // They are set once for each zone in this method.
        this->MakeZone( 2 * id + 1, seg, -xmax, xmax, -25.f, ymax ); // Small overlap (25 mm) for stereo layers
        this->MakeZone( 2 * id, seg, -xmax, xmax, -ymax, 25.f );     // Small overlap (25 mm) for stereo layers
      }
    }
  }
  /// Standard constructor
  PrFTZoneHandler() = default;

  void MakeZone( unsigned int n, DetectorSegment& seg, float xMin, float xMax, float yMin, float yMax ) {
    m_zones[n].setZone( n, seg, xMin, xMax, yMin, yMax );
  }
  const PrHitZone& zone( unsigned int n ) const { return m_zones[n]; }

  template <PrHitZone::Side SIDE>
  static int getXZone( int layer ) {
    if constexpr ( SIDE == PrHitZone::Side::Upper ) {
      return PrFTInfo::xZonesUpper[layer];
    } else {
      return PrFTInfo::xZonesLower[layer];
    }
  }

  template <PrHitZone::Side SIDE>
  static int getUVZone( int layer ) {
    if constexpr ( SIDE == PrHitZone::Side::Upper ) {
      return PrFTInfo::uvZonesUpper[layer];
    } else {
      return PrFTInfo::uvZonesLower[layer];
    }
  }

  template <PrHitZone::Side SIDE>
  static int getTriangleZone( int layer ) {
    if constexpr ( SIDE == PrHitZone::Side::Upper ) {
      return PrFTInfo::uvZonesLower[layer];
    } else {
      return PrFTInfo::uvZonesUpper[layer];
    }
  }

private:
  // plain vector with indexing needed!
  std::array<PrHitZone, PrFTInfo::Numbers::NFTZones> m_zones;
};

#endif // PRFTZONEHANDLER_H
