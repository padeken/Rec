/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef PRKERNEL_PRUTMUTHITS_H
#define PRKERNEL_PRUTMUTHITS_H

#include "LHCbMath/SIMDWrapper.h"
#include "UTDAQ/UTInfo.h"

/** Mutable UT hit class for internal use in pattern recognition algorithms
 *
 *  @author Michel De Cian
 *  @date   2020-04-06
 */

namespace LHCb::Pr::UT {

  namespace Mut {

    struct Hits {

      // -- These hits only live inside the algorithm
      // -- so it should be ok to put them on the stack
      // -- need to handle the case of too many hits though
      constexpr static int max_hits = align_size( 256 );
      alignas( 64 ) std::array<float, max_hits> xs;
      alignas( 64 ) std::array<float, max_hits> zs;
      alignas( 64 ) std::array<float, max_hits> coss;
      alignas( 64 ) std::array<float, max_hits> sins;
      alignas( 64 ) std::array<float, max_hits> weights;
      alignas( 64 ) std::array<float, max_hits> projections;
      alignas( 64 ) std::array<int, max_hits> channelIDs;
      alignas( 64 ) std::array<int, max_hits> indexs;

      std::array<int, static_cast<int>( UTInfo::DetectorNumbers::TotalLayers )> layerIndices;

      std::size_t size{0};
      SOA_ACCESSOR( x, xs.data() )
      SOA_ACCESSOR( z, zs.data() )
      SOA_ACCESSOR( cos, coss.data() )
      // at some point one needs to calculate the sin, we'll see if calculating or storing it is faster
      SOA_ACCESSOR( sin, sins.data() )
      SOA_ACCESSOR( weight, weights.data() )
      SOA_ACCESSOR( projection, projections.data() )
      SOA_ACCESSOR( channelID, channelIDs.data() )
      SOA_ACCESSOR( index, indexs.data() )

      /// Retrieve the plane code
      template <typename T>
      T planeCode( int t ) const {
        T id      = channelID<T>( t );
        T station = ( id & static_cast<int>( UTInfo::MasksBits::StationMask ) ) >>
                    static_cast<int>( UTInfo::MasksBits::StationBits );
        T layer = ( id & static_cast<int>( UTInfo::MasksBits::LayerMask ) ) >>
                  static_cast<int>( UTInfo::MasksBits::LayerBits );

        return 2 * ( station - 1 ) + ( layer - 1 );
      }

      template <typename simd, typename MaskT>
      void copy_from( const Hits& hits, int from, MaskT mask ) {
        using I = typename simd::int_v;
        using F = typename simd::float_v;
        assert( from + simd::popcount( mask ) < max_hits );

        F( &hits.xs[from] ).compressstore( mask, &xs[size] );
        F( &hits.zs[from] ).compressstore( mask, &zs[size] );
        F( &hits.coss[from] ).compressstore( mask, &coss[size] );
        F( &hits.sins[from] ).compressstore( mask, &sins[size] );
        F( &hits.weights[from] ).compressstore( mask, &weights[size] );
        F( &hits.projections[from] ).compressstore( mask, &projections[size] );
        I( &hits.channelIDs[from] ).compressstore( mask, &channelIDs[size] );
        I( &hits.indexs[from] ).compressstore( mask, &indexs[size] );

        size += simd::popcount( mask );
      }
    };

  } // namespace Mut
} // namespace LHCb::Pr::UT
#endif
