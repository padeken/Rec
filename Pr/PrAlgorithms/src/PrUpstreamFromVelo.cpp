/*****************************************************************************\
* (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/PrUpstreamTracks.h"
#include "Event/PrVeloTracks.h"
#include "GaudiAlg/Transformer.h"
#include "GaudiKernel/SystemOfUnits.h"

namespace {
  using TracksVP    = LHCb::Pr::Velo::Tracks;
  using TracksUT    = LHCb::Pr::Upstream::Tracks;
  using Transformer = Gaudi::Functional::Transformer<TracksUT( TracksVP const& )>;
} // namespace

namespace Pr {
  /** @class UpstreamFromVelo PrUpstreamFromVelo.cpp
   *
   *  Converts a container of Velo tracks into a container of Upstream ones
   *  with some fixed pT value.
   */
  struct UpstreamFromVelo final : public Transformer {
    UpstreamFromVelo( const std::string& name, ISvcLocator* pSvcLocator )
        : Transformer( name, pSvcLocator, {"Input", ""}, {"Output", ""} ) {}

    TracksUT operator()( TracksVP const& inputTracks ) const override {
      using dType = SIMDWrapper::best::types;
      using I     = dType::int_v;
      using F     = dType::float_v;
      TracksUT outputTracks{&inputTracks};
      outputTracks.resize( inputTracks.size() );
      float      invAssumedPT{1.f / m_assumedPT};
      auto const velozipped = LHCb::Pr::make_zip( inputTracks );
      for ( auto const& track : velozipped ) {
        auto mask = track.loop_mask();
        auto cov  = track.StateCovX( 1 );
        auto pos  = track.StatePos( 1 );
        auto dir  = track.StateDir( 1 );

        // Assign q/p assuming q=+1 and pT is 'AssumedPT'
        auto    txy2 = dir.x * dir.x + dir.y * dir.y;
        F const qop  = invAssumedPT * sqrt( txy2 / ( 1 + txy2 ) );
        outputTracks.store<TracksTag::trackVP>( track.offset(), track.indices(), mask );
        outputTracks.store<TracksTag::nVPHits>( track.offset(), I{0}, mask );
        outputTracks.store<TracksTag::nUTHits>( track.offset(), I{0}, mask );
        outputTracks.store<TracksTag::StateQoP>( track.offset(), qop, mask );
        outputTracks.store_StatePosDir( track.offset(), pos, dir, mask );
        outputTracks.store_StateCovX( track.offset(), cov, mask );
        outputTracks.store_vp_index( track.offset(), 0, I{0}, mask );
        outputTracks.store_lhcbID( track.offset(), 0, I{0}, mask );
      }
      return outputTracks;
    }

    Gaudi::Property<float> m_assumedPT{this, "AssumedPT", 4.5 * Gaudi::Units::GeV};
  };
} // namespace Pr

DECLARE_COMPONENT_WITH_ID( Pr::UpstreamFromVelo, "PrUpstreamFromVelo" )
