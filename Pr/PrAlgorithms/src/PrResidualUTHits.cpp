/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files
#include "DetDesc/Condition.h"
#include "DetDesc/GenericConditionAccessorHolder.h"
#include "DetDesc/IConditionDerivationMgr.h"
#include "Event/ODIN.h"
#include "Event/PrLongTracks.h"
#include "Event/Track.h"
#include "Event/Track_v2.h"
#include "Gaudi/Accumulators.h"
#include "GaudiAlg/Transformer.h"
#include "GaudiKernel/IRegistry.h"
#include "PrKernel/PrHit.h"
#include "PrKernel/PrUTHitHandler.h"
#include "PrKernel/UTHit.h"
#include "PrKernel/UTHitHandler.h"
#include "PrKernel/UTHitInfo.h"
#include "UTDAQ/UTInfo.h"
#include "UTDet/DeUTDetector.h"
#include <Vc/Vc>
#include <vector>

#include "boost/container/small_vector.hpp"
#include "boost/container/static_vector.hpp"
#include <memory>

//-----------------------------------------------------------------------------
// class : PrResidualUTHits
// Store residual UTHits after other Algorithms, e.g. PrMatchNN/PrForward used
//
// 2020-04-21 : Peilian Li
//
//-----------------------------------------------------------------------------

class PrResidualUTHits
    : public Gaudi::Functional::Transformer<UT::HitHandler( const LHCb::Pr::Long::Tracks&, const UT::HitHandler& )> {

  using Tracks = LHCb::Pr::Long::Tracks;

public:
  StatusCode initialize() override;

  PrResidualUTHits( const std::string& name, ISvcLocator* pSvcLocator );

  UT::HitHandler operator()( const Tracks&, const UT::HitHandler& ) const override;

private:
  DeUTDetector* m_utDet = nullptr;
};

// Declaration of the Algorithm Factory
DECLARE_COMPONENT_WITH_ID( PrResidualUTHits, "PrResidualUTHits" )

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
PrResidualUTHits::PrResidualUTHits( const std::string& name, ISvcLocator* pSvcLocator )
    : Transformer( name, pSvcLocator, {KeyValue{"TracksLocation", ""}, KeyValue{"UTHitsLocation", ""}},
                   KeyValue{"UTHitsOutput", ""} ) {}

// initialisation
StatusCode PrResidualUTHits::initialize() {
  return GaudiAlgorithm::initialize().andThen( [&] { m_utDet = getDet<DeUTDetector>( DeUTDetLocation::UT ); } );
}
// Main execution
//=============================================================================
UT::HitHandler PrResidualUTHits::operator()( const Tracks& tracks, const UT::HitHandler& uthithandler ) const {

  UT::HitHandler tmp{};

  if ( tracks.size() == 0 ) {
    if ( UNLIKELY( msgLevel( MSG::DEBUG ) ) )
      debug() << "Track container '" << inputLocation<Tracks>() << "' is empty" << endmsg;
    return uthithandler;
  }

  std::vector<long unsigned int> usedUTHits{};
  usedUTHits.reserve( uthithandler.nbHits() );

  auto const longiter = LHCb::Pr::make_zip<SIMDWrapper::InstructionSet::Scalar>( tracks );
  for ( const auto& track : longiter ) {
    const auto ids = track.lhcbIDs();
    for ( auto id : ids ) {
      if ( !( id.isUT() ) ) continue;
      usedUTHits.emplace_back( id.utID().channelID() );
    }
  }

  for ( int iStation = 1; iStation <= static_cast<int>( UTInfo::DetectorNumbers::Stations ); ++iStation ) {
    for ( int iLayer = 1; iLayer <= static_cast<int>( UTInfo::DetectorNumbers::Layers ); ++iLayer ) {
      for ( int iRegion = 1; iRegion <= static_cast<int>( UTInfo::DetectorNumbers::Regions ); ++iRegion ) {
        for ( int iSector = 1; iSector <= static_cast<int>( UTInfo::DetectorNumbers::Sectors ); ++iSector ) {
          for ( auto& uthit : uthithandler.hits( iStation, iLayer, iRegion, iSector ) ) {
            bool used = std::any_of( usedUTHits.begin(), usedUTHits.end(),
                                     [utid = uthit.chanID().channelID()]( const auto& id ) { return utid == id; } );

            if ( used ) continue;
            const unsigned int fullChanIdx = UT::HitHandler::HitsInUT::idx( iStation, iLayer, iRegion, iSector );
            const auto*        aSector     = m_utDet->getSector( uthit.chanID() );
            tmp.AddHit( aSector, fullChanIdx, uthit.strip(), uthit.fracStrip(), uthit.chanID(), uthit.size(),
                        uthit.highThreshold() );
          }
        }
      }
    }
  }
  return tmp;
}
