/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files

// local
#include "PrMatchNN.h"
#include "Event/StateParameters.h"
#include <optional>
//-----------------------------------------------------------------------------
// Implementation file for class : PrMatchNN
//
// 2013-11-15 : Michel De Cian, migration to Upgrade
//
// 2007-02-07 : Olivier Callot
//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( PrMatchNN )

namespace SeedTag = LHCb::Pr::Seeding::Tag;

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
PrMatchNN::PrMatchNN( const std::string& name, ISvcLocator* pSvcLocator )
    : Transformer( name, pSvcLocator,
                   {KeyValue{"VeloInput", "Rec/Track/Velo"}, KeyValue{"SeedInput", "Rec/Track/Seed"}},
                   KeyValue{"MatchOutput", "Rec/Track/Match"} ) {}

//=============================================================================
// Initialization
//=============================================================================
StatusCode PrMatchNN::initialize() {
  return Transformer::initialize().andThen( [&] {
    // Input variable names for TMVA
    // they are only needed to make sure same variables are used in MVA
    // we can remove them once the algorithm is final
    std::array<std::string, 6> inputVars = {"var0", "var1", "var2", "var3", "var4", "var5"};
    m_MLPReader                          = std::make_unique<ReadMLPMatching>( inputVars );
  } );
}

//=============================================================================
// Main execution
//=============================================================================
LHCb::Pr::Long::Tracks PrMatchNN::operator()( const LHCb::Pr::Velo::Tracks&    velos,
                                              const LHCb::Pr::Seeding::Tracks& seeds ) const {

  std::array<float, 6> mLPReaderInput = {0.0, 0.0, 0.0, 0.0, 0.0, 0.0};

  LHCb::Pr::Long::Tracks noneresult( nullptr, nullptr, nullptr );

  if ( velos.size() == 0 || seeds.size() == 0 ) {
    if ( UNLIKELY( msgLevel( MSG::DEBUG ) ) ) {
      debug() << "Track container '" << inputLocation<LHCb::Pr::Seeding::Tracks>() << "' has size " << seeds.size()
              << endmsg;
      debug() << "Track container '" << inputLocation<LHCb::Pr::Velo::Tracks>() << "' has size " << velos.size()
              << endmsg;
    }

    LHCb::Pr::Long::Tracks noneresult( nullptr, nullptr, nullptr );
    return noneresult;
  }

  seedMLPPairs seedMLP;
  seedMLP.reserve( seeds.size() );
  MatchCandidates matches;

  // Scalar is needed below
  auto const veloiter = LHCb::Pr::make_zip<SIMDWrapper::InstructionSet::Scalar>( velos );
  for ( auto const& velo : veloiter ) {
    auto mlpCounterBuf  = m_tracksMLP.buffer();
    auto chi2CounterBuf = m_tracksChi2.buffer();

    const int EndVelo  = 1;
    auto      velo_pos = velo.StatePos( EndVelo );
    auto      velo_dir = velo.StateDir( EndVelo );

    const float posYApproxV = velo_pos.y.cast() + ( m_zMatchY - velo_pos.z.cast() ) * velo_dir.y.cast();

    const int EndT3 = 2;

    for ( auto const& s : seeds.scalar() ) {
      auto const seedidx    = s.offset();
      auto const seed_state = s.get<SeedTag::StatePositions>( EndT3 );
      const auto seed_pos   = Vec3<F>( seed_state.x(), seed_state.y(), seed_state.z() );
      const auto seed_dir   = Vec3<F>( seed_state.tx(), seed_state.ty(), 1.f );
      // TODO use seed_state directly

      const float posYApproxS = seed_state.y().cast() + ( m_zMatchY - seed_state.z().cast() ) * seed_state.ty().cast();
      if ( posYApproxS > posYApproxV + m_fastYTol ) continue;
      const float chi2 = getChi2Match( velo_pos, velo_dir, seed_pos, seed_dir, mLPReaderInput ).value_or( 9999.0 );

      if ( chi2 < m_maxChi2 ) {

        const float mlp = m_MLPReader->GetMvaValue( mLPReaderInput );
        mlpCounterBuf += mlp;
        chi2CounterBuf += chi2;

        if ( mlp > m_minNN ) { seedMLP.emplace_back( seedidx, mlp ); }
      }
    }

    std::sort( seedMLP.begin(), seedMLP.end(),
               [&]( std::pair<int, float> sP1, std::pair<int, float> sP2 ) { return sP1.second > sP2.second; } );

    for ( unsigned int sm = 0; sm != seedMLP.size(); sm++ ) {

      if ( seedMLP[0].second - seedMLP[sm].second > m_maxdDist ) break;
      matches.emplace_back( velo.offset(), seedMLP[sm].first );
    }

    seedMLP.clear();
  }

  auto outputTracks = makeTracks( velos, seeds, matches );

  if ( m_addUTHitsTool.isEnabled() ) {
    StatusCode sc = m_addUTHitsTool->addUTHits( outputTracks );
    if ( sc.isFailure() ) Warning( "adding UT clusters failed!", sc ).ignore();
  }

  m_tracksCount += outputTracks.size();
  return outputTracks;
}

//=============================================================================
std::optional<float> PrMatchNN::getChi2Match( const Vec3<F> vState_pos, const Vec3<F> vState_dir,
                                              const Vec3<F> sState_pos, const Vec3<F> sState_dir,
                                              std::array<float, 6>& mLPReaderInput ) const {

  const float tx2 = vState_dir.x.cast() * vState_dir.x.cast();
  const float ty2 = vState_dir.y.cast() * vState_dir.y.cast();

  const float dSlope = vState_dir.x.cast() - sState_dir.x.cast();
  if ( std::abs( dSlope ) > 1.5 ) return std::nullopt;

  const float dSlopeY = vState_dir.y.cast() - sState_dir.y.cast();
  if ( std::abs( dSlopeY ) > 0.15 ) return std::nullopt;

  const float zForX = m_zMagParams[0] + m_zMagParams[1] * std::abs( dSlope ) + m_zMagParams[2] * dSlope * dSlope +
                      m_zMagParams[3] * std::abs( sState_pos.x.cast() ) +
                      m_zMagParams[4] * vState_dir.x.cast() * vState_dir.x.cast();

  const float dxTol2      = m_dxTol * m_dxTol;
  const float dxTolSlope2 = m_dxTolSlope * m_dxTolSlope;

  const float xV = vState_pos.x.cast() + ( zForX - vState_pos.z.cast() ) * vState_dir.x.cast();
  // -- This is the function that calculates the 'bending' in y-direction
  // -- The parametrisation can be derived with the MatchFitParams package
  const float yV = vState_pos.y.cast() + ( m_zMatchY - vState_pos.z.cast() ) * vState_dir.y.cast() +
                   vState_dir.y.cast() * ( m_bendYParams[0] * dSlope * dSlope + m_bendYParams[1] * dSlopeY * dSlopeY );

  const float xS = sState_pos.x.cast() + ( zForX - sState_pos.z.cast() ) * sState_dir.x.cast();
  const float yS = sState_pos.y.cast() + ( m_zMatchY - sState_pos.z.cast() ) * sState_dir.y.cast();

  const float distX = xS - xV;
  if ( std::abs( distX ) > 400 ) return std::nullopt;
  const float distY = yS - yV;
  if ( std::abs( distX ) > 250 ) return std::nullopt;

  const float teta2 = tx2 + ty2;
  const float tolX  = dxTol2 + dSlope * dSlope * dxTolSlope2;
  const float tolY  = m_dyTol * m_dyTol + teta2 * m_dyTolSlope * m_dyTolSlope;

  float chi2 = ( tolX != 0 and tolY != 0 ? distX * distX / tolX + distY * distY / tolY : 9999. );

  chi2 += dSlopeY * dSlopeY * 10000 * 0.0625;

  if ( m_maxChi2 < chi2 ) return std::nullopt;

  mLPReaderInput[0] = chi2;
  mLPReaderInput[1] = teta2;
  mLPReaderInput[2] = std::abs( distX );
  mLPReaderInput[3] = std::abs( distY );
  mLPReaderInput[4] = std::abs( dSlope );
  mLPReaderInput[5] = std::abs( dSlopeY );

  return std::optional{chi2};
}

//=============================================================================
LHCb::Pr::Long::Tracks PrMatchNN::makeTracks( const LHCb::Pr::Velo::Tracks&    velos,
                                              const LHCb::Pr::Seeding::Tracks& seeds, MatchCandidates matches ) const {

  LHCb::Pr::Long::Tracks result( &velos, nullptr, &seeds, Zipping::generateZipIdentifier() );
  result.reserve( matches.size() );

  auto const seediter = seeds.scalar();
  auto const veloiter = LHCb::Pr::make_zip<SIMDWrapper::InstructionSet::Scalar>( velos );
  for ( const auto match : matches ) {

    auto currentsize = result.size();
    result.resize( currentsize + 1 );
    //== copy LHCbIDs
    auto const seed_track = seediter[match.sTr()];
    auto const n_fthits   = seed_track.nHits();
    auto const velo_track = veloiter[match.vTr()];
    auto const n_vphits   = velo_track.nHits();

    result.store<TracksTag::trackVP>( currentsize, I{match.vTr()} );
    result.store<TracksTag::trackUT>( currentsize, I{-1} );
    result.store<TracksTag::trackSeed>( currentsize, I{match.sTr()} );
    result.store<TracksTag::nVPHits>( currentsize, n_vphits );
    result.store<TracksTag::nUTHits>( currentsize, I{0} );
    result.store<TracksTag::nFTHits>( currentsize, n_fthits );
    for ( auto idx{0}; idx < n_vphits.cast(); ++idx ) {
      result.store_vp_index( currentsize, idx, velo_track.vp_index( idx ) );
      result.store_lhcbID( currentsize, idx, velo_track.lhcbID( idx ) );
    }

    for ( auto idx{0}; idx < n_fthits.cast(); ++idx ) {
      auto const id = n_vphits.cast() + idx;
      result.store_ft_index( currentsize, idx, seed_track.ft_index( idx ) );
      result.store_lhcbID( currentsize, id, seed_track.lhcbID( idx ) );
    }

    //== get Velo and T states at the usual pattern reco positions
    auto state_beam = getVeloState( velo_track, 0 );
    state_beam.setLocation( LHCb::State::Location::ClosestToBeam );

    auto state_endvelo = getVeloState( velo_track, 1 );
    state_endvelo.setLocation( LHCb::State::Location::EndVelo );

    auto state_endT = seed_track.getLHCbState( 2 );

    //== estimate q/p
    // bool const  cubicFit = seed.checkHistory( Track::History::PrSeeding );     //what to do about this?
    double     qOverP, sigmaQOverP;
    StatusCode sc = m_fastMomentumTool->calculate( &state_beam, &state_endT, qOverP, sigmaQOverP, true );

    if ( sc.isFailure() ) {
      Warning( "momentum determination failed!", sc ).ignore();
      // assume the Velo/T station standalone reco do something reasonable
      qOverP = -9999.; // what is a good nonsense value
    }

    // store end of VELO state
    const F qop = float( qOverP );
    result.store<TracksTag::StateQoP>( currentsize, qop );
    auto velopos = Vec3<F>( state_endvelo.x(), state_endvelo.y(), state_endvelo.z() );
    auto velodir = Vec3<F>( state_endvelo.tx(), state_endvelo.ty(), 1.f );
    result.store_StatePosDir( currentsize, 0, velopos, velodir );

    // store end of T state
    auto pos = Vec3<F>( state_endT.x(), state_endT.y(), state_endT.z() );
    auto dir = Vec3<F>( state_endT.tx(), state_endT.ty(), 1.f );
    result.store_StatePosDir( currentsize, 1, pos, dir );
  }
  // avoid FPEs
  if ( ( result.size() + simd::size ) > result.capacity() ) result.reserve( result.size() + simd::size );
  result.store<TracksTag::StateQoP>( result.size(), simd::float_v( 1.f ) );
  result.store_StatePosDir( result.size(), 0, Vec3<simd::float_v>( 1.f, 1.f, 1.f ),
                            Vec3<simd::float_v>( 1.f, 1.f, 1.f ) );
  result.store_StatePosDir( result.size(), 1, Vec3<simd::float_v>( 1.f, 1.f, 1.f ),
                            Vec3<simd::float_v>( 1.f, 1.f, 1.f ) );

  return result;
}
//=============================================================================
