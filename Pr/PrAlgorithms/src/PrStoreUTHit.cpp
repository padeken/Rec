/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "DetDesc/Condition.h"
#include "DetDesc/ConditionAccessorHolder.h"
#include "Event/RawBank.h"
#include "Event/RawEvent.h"
#include "GaudiAlg/Transformer.h"
#include "Kernel/IUTReadoutTool.h"
#include "Kernel/UTDAQBoard.h"
#include "Kernel/UTDecoder.h"
#include "Kernel/UTTell1Board.h"
#include "PrKernel/PrUTHitHandler.h"
#include "PrKernel/UTHitHandler.h"
#include "UTDet/DeUTDetector.h"
#include <memory>
#include <numeric>

namespace LHCb::Pr::UT {

  struct UTGeomCache {
    static constexpr int NSectorPerBoard = 6;
    static constexpr int NSectorAllBoard = 240 * 6;
    /** Faster access to sectors **/
    std::array<const DeUTSector*, NSectorAllBoard> sectors;

    struct FullChan {
      unsigned int idx{0};
      unsigned int chanID{0};
    };
    std::array<FullChan, NSectorAllBoard> fullchan;

    UTGeomCache( const DeUTDetector& utDet, const IUTReadoutTool& ro ) {
      const auto stripflip = ro.getstripflip();
      for ( unsigned int srcId = 0; srcId < ro.nBoard(); srcId++ ) {
        const auto aBoard = ro.findBySourceID( srcId ); // UTTell1Board*
        if ( aBoard.empty() ) continue;
        std::size_t i = 0;
        for ( const auto& sector : aBoard ) {

          auto* aSector = utDet.getSector( sector );
          // FIXME: it should not be possible to modify `aSector` at this point, but it is required
          //        to get the correct mapping of channel ids to geometry for older simulations
          if ( !aSector ) throw std::runtime_error{"DeUTDetector::getSector returned nullptr"};
          const_cast<DeUTSector*>( aSector )->setstripflip( stripflip );

          const unsigned int fullChanIdx =
              ::UT::HitHandler::HitsInUT::idx( sector.station(), sector.layer(), sector.detRegion(), sector.sector() );

          const std::size_t geomIdx = ro.TELLNumberToSourceID( srcId + 1 ) * UTGeomCache::NSectorPerBoard + i++;
          assert( geomIdx < NSectorAllBoard );
          assert( geomIdx < NSectorAllBoard );
          sectors[geomIdx]  = aSector;
          fullchan[geomIdx] = {fullChanIdx, (unsigned int)sector};
        }
      }
    }
  };

  template <typename HANDLER>
  using Transformer =
      Gaudi::Functional::Transformer<HANDLER( const EventContext&, const RawEvent&, const UTGeomCache& ),
                                     LHCb::DetDesc::usesConditions<UTGeomCache>>;

  template <typename HANDLER>
  class StoreHit : public Transformer<HANDLER> {
  public:
    using KeyValue = typename Transformer<HANDLER>::KeyValue;
    using Transformer<HANDLER>::inputLocation;

    StoreHit( const std::string& name, ISvcLocator* pSvcLocator )
        : Transformer<HANDLER>( name, pSvcLocator,
                                {KeyValue{"RawEventLocations", Gaudi::Functional::concat_alternatives(
                                                                   RawEventLocation::Tracker, RawEventLocation::Other,
                                                                   RawEventLocation::Default )},
                                 KeyValue{"GeomCache", "AlgorithmSpecific-" + name + "-UTGeomCache"}},
                                KeyValue{"UTHitsLocation", ::UT::Info::HitLocation} ) {}

    StatusCode initialize() override {
      return Transformer<HANDLER>::initialize().andThen( [&] {
        // TODO : alignment need the updateSvc for detector ( UT experts needed )
        this->addConditionDerivation( {DeUTDetLocation::UT}, this->template inputLocation<UTGeomCache>(),
                                      [this]( const DeUTDetector& utDet ) {
                                        return UTGeomCache{utDet, *m_readoutTool};
                                      } );
      } );
    }

    HANDLER operator()( const EventContext& evtCtx, const LHCb::RawEvent& rawEvt,
                        const UTGeomCache& cache ) const override {
      HANDLER hitHandler{LHCb::getMemResource( evtCtx )};
      if constexpr ( std::is_same_v<HANDLER, ::UT::HitHandler> ) hitHandler.reserve( 10000 );

      const auto tBanks = rawEvt.banks( RawBank::UT );
      for ( const auto& bank : tBanks ) {
        // make local decoder
        if ( bank->size() == 0 ) continue;

        auto decode = [&hitHandler, geomOffset = bank->sourceID() * UTGeomCache::NSectorPerBoard,
                       &cache]( auto decoder ) {
          for ( const auto& aWord : decoder.posRange() ) {
            const std::size_t geomIdx = geomOffset + ( aWord.channelID() / 512 );
            assert( geomIdx < cache.sectors.size() );
            assert( geomIdx < cache.fullchan.size() );

            const auto& aSector  = cache.sectors[geomIdx];
            const auto& fullChan = cache.fullchan[geomIdx];

            // FIXME: move functionality into channelID -- why is this _different_ from UTChennelID::strip() ??
            const std::size_t strip = ( aWord.channelID() & 0x1ff ) + 1;

            hitHandler.AddHit( aSector, fullChan.idx, strip, aWord.fracStripBits(),
                               UTChannelID{(int)( fullChan.chanID + strip )}, aWord.pseudoSizeBits(),
                               aWord.hasHighThreshold() );
          }
        };
        switch ( bank->version() ) {
        case UTDAQ::v5:
          decode( UTDecoder{*bank} );
          break;
        case UTDAQ::v4:
          decode( UTDecoder_v4{bank->data()} );
          break;
        default:
          throw std::runtime_error{"unknown version of the RawBank"}; /* OOPS: unknown format */
        };
      }

      m_nBanks += tBanks.size();

      if constexpr ( std::is_same_v<HANDLER, ::LHCb::Pr::UT::HitHandler> ) hitHandler.addPadding();

      return hitHandler;
    }

  private:
    mutable Gaudi::Accumulators::SummingCounter<> m_nBanks{this, "#banks"};
    ToolHandle<IUTReadoutTool>                    m_readoutTool{this, "ReadoutTool", "UTReadoutTool"};
  };
  // Declaration of the Algorithm Factory
  DECLARE_COMPONENT_WITH_ID( StoreHit<::UT::HitHandler>, "PrStoreUTHit" ) // scalar hits
  DECLARE_COMPONENT_WITH_ID( StoreHit<HitHandler>, "PrStorePrUTHits" )    // SoA hits

} // namespace LHCb::Pr::UT
