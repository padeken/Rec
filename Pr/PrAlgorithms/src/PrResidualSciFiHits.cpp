/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files
#include "Event/ODIN.h"
#include "Event/PrLongTracks.h"
#include "Event/Track.h"
#include "Event/Track_v2.h"
#include "Gaudi/Accumulators.h"
#include "GaudiAlg/Transformer.h"
#include "GaudiKernel/IRegistry.h"
#include "PrKernel/PrFTInfo.h"
#include "PrKernel/PrFTZoneHandler.h"
#include "PrKernel/PrHit.h"
#include "PrKernel/PrSciFiHits.h"
#include <Vc/Vc>
#include <array>
#include <vector>

#include "boost/container/small_vector.hpp"
#include "boost/container/static_vector.hpp"
#include "boost/dynamic_bitset.hpp"
#include <memory>

//-----------------------------------------------------------------------------
// class : PrResidualSciFiHits
// Store residual SciFiHits after other Algorithms, e.g. PrMatchNN or PrForwardTracking
// the input tracks and SciFiHits are in SOA structure
//
// 2020-04-02 : Peilian Li
//
//-----------------------------------------------------------------------------

namespace {
  using namespace SciFiHits;
}

class PrResidualSciFiHits
    : public Gaudi::Functional::Transformer<PrSciFiHits( const LHCb::Pr::Long::Tracks&, const PrSciFiHits& )> {
  using Tracks = LHCb::Pr::Long::Tracks;

public:
  PrResidualSciFiHits( const std::string& name, ISvcLocator* pSvcLocator );

  PrSciFiHits operator()( const Tracks&, const PrSciFiHits& ) const override;
};

// Declaration of the Algorithm Factory
DECLARE_COMPONENT_WITH_ID( PrResidualSciFiHits, "PrResidualSciFiHits" )

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
PrResidualSciFiHits::PrResidualSciFiHits( const std::string& name, ISvcLocator* pSvcLocator )
    : Transformer( name, pSvcLocator,
                   {KeyValue{"TracksLocation", ""}, KeyValue{"SciFiHitsLocation", PrFTInfo::SciFiHitsLocation}},
                   KeyValue{"SciFiHitsOutput", PrFTInfo::SciFiHitsLocation} ) {}

//=============================================================================
// Main execution
//=============================================================================
PrSciFiHits PrResidualSciFiHits::operator()( const Tracks& tracks, const PrSciFiHits& fthits ) const {

  PrSciFiHits tmp{};
  auto&       hitvec       = tmp._x;
  auto&       z0vec        = tmp._z0;
  auto&       yMinvec      = tmp._yMins;
  auto&       yMaxvec      = tmp._yMaxs;
  auto&       planeCodevec = tmp._planeCodes;
  auto&       IDvec        = tmp._IDs;
  auto&       werrvec      = tmp._w;
  auto&       dzDyvec      = tmp._dzDy;
  auto&       dxDyvec      = tmp._dxDy;
  auto&       zoneIndexes  = tmp.zoneIndexes;

  if ( tracks.size() == 0 ) {
    if ( UNLIKELY( msgLevel( MSG::DEBUG ) ) )
      debug() << "Track container '" << inputLocation<Tracks>() << "' is empty" << endmsg;
    return fthits;
  }

  const auto              nhits = fthits._IDs.size();
  boost::dynamic_bitset<> used{nhits, false};

  auto const longzipped = LHCb::Pr::make_zip<SIMDWrapper::InstructionSet::Scalar>( tracks );
  /// mark used SciFi Hits
  for ( const auto& track : longzipped ) {
    const int nfthits = track.nFTHits().cast();
    for ( int id = 0; id != nfthits; id++ ) {
      auto idx = track.ft_index( id ).cast();
      if ( idx >= 0 ) used[idx] = true;
    }
  }
  constexpr auto xu  = PrFTInfo::xZonesUpper;
  constexpr auto uvu = PrFTInfo::uvZonesUpper;

  constexpr auto xd       = PrFTInfo::xZonesLower;
  constexpr auto uvd      = PrFTInfo::uvZonesLower;
  constexpr auto hitzones = std::array<int, PrFTInfo::NFTZones>{
      xd[0], uvd[0], uvd[1], xd[1], xd[2], uvd[2], uvd[3], xd[3], xd[4], uvd[4], uvd[5], xd[5],
      xu[0], uvu[0], uvu[1], xu[1], xu[2], uvu[2], uvu[3], xu[3], xu[4], uvu[4], uvu[5], xu[5]};

  zoneIndexes[hitzones[0]] = hitvec.size();
  int j                    = 1;
  for ( unsigned int i = 0; i != fthits._IDs.size(); i++ ) { // loop whole SciFiHits container

    if ( used[i] ) continue;
    hitvec.emplace_back( fthits._x[i] );
    z0vec.emplace_back( fthits._z0[i] );
    yMinvec.emplace_back( fthits._yMins[i] );
    yMaxvec.emplace_back( fthits._yMaxs[i] );
    planeCodevec.emplace_back( fthits._planeCodes[i] );
    IDvec.emplace_back( fthits._IDs[i] );
    werrvec.emplace_back( fthits._w[i] );
    dzDyvec.emplace_back( fthits._dzDy[i] );
    dxDyvec.emplace_back( fthits._dxDy[i] );
    if ( j < 24 && fthits._IDs[i] == 0 && fthits._IDs[i + 1] == 0 ) {
      zoneIndexes[hitzones[j]] = hitvec.size();
      j++;
    }
  }
  zoneIndexes[PrFTInfo::NFTZones]     = zoneIndexes[xu[0]];
  zoneIndexes[PrFTInfo::NFTZones + 1] = hitvec.size();

  return tmp;
}
