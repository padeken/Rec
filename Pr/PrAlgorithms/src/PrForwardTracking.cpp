/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "DetDesc/Condition.h"
#include "DetDesc/ConditionAccessorHolder.h"
#include "Event/PrLongTracks.h"
#include "Event/StateParameters.h"
#include "Event/Track.h"
#include "Event/Track_v2.h"
#include "FTDet/DeFTDetector.h"
#include "GaudiAlg/GaudiTool.h"
#include "GaudiAlg/Transformer.h"
#include "GaudiKernel/AnyDataHandle.h"
#include "GaudiKernel/StdArrayAsProperty.h"
#include "GaudiKernel/extends.h"
#include "IPrAddUTHitsTool.h"
#include "Kernel/ILHCbMagnetSvc.h"
#include "LHCbMath/SIMDWrapper.h"
#include "LHCbMath/bit_cast.h"
#include "PrForwardTrack.h"
#include "PrKernel/PrFTInfo.h"
#include "PrKernel/PrFTZoneHandler.h"
#include "PrKernel/PrSciFiHits.h"
#include "PrKernel/PrSelection.h"
#include "PrLineFitter.h"
#include "PrPlaneCounter.h"
#include "boost/container/small_vector.hpp"
#include "boost/container/static_vector.hpp"
#include "range/v3/numeric/accumulate.hpp"
#include "range/v3/view.hpp"
#include "weights/TMVA_MLP_Forward1stLoop.h"
#include "weights/TMVA_MLP_Forward2ndLoop.h"
#include <algorithm>
#include <cassert>
#include <functional>
#include <limits>
#include <optional>

// *********************************************************************************
// ************************ Introduction to Forward Tracking **********************
// *********************************************************************************
//
//  A detailed introduction in Forward tracking (with real pictures!) can be
//  found here:
//  (2002) http://cds.cern.ch/record/684710/files/lhcb-2002-008.pdf
//  (2007) http://cds.cern.ch/record/1033584/files/lhcb-2007-015.pdf
//  (2014) http://cds.cern.ch/record/1641927/files/LHCb-PUB-2014-001.pdf
//
// *** Short Introduction in geometry:
//
// The SciFi Tracker Detector, or simple Fibre Tracker (FT) consits out of 3 stations.
// Each station consists out of 4 planes/layers. Thus there are in total 12 layers,
// in which a particle can leave a hit. The reasonable maximum number of hits a track
// can have is thus also 12 (sometimes 2 hits per layer are picked up).
//
// Each layer consists out of several Fibre mats. A fibre has a diameter of below a mm.(FIXME)
// Several fibres are glued alongside each other to form a mat.
// A Scintilating Fibre produces light, if a particle traverses. This light is then
// detected on the outside of the Fibre mat.
//
// Looking from the collision point, one (X-)layer looks like the following:
//
//                    y       6m
//                    ^  ||||||||||||| Upper side
//                    |  ||||||||||||| 2.5m
//                    |  |||||||||||||
//                   -|--||||||o||||||----> -x
//                       |||||||||||||
//                       ||||||||||||| Lower side
//                       ||||||||||||| 2.5m
//
// All fibres are aranged parallel to the y-axis. There are three different
// kinds of layers, denoted by X,U,V. The U/V layers are rotated with respect to
// the X-layers by +/- 5 degrees, to also get a handle of the y position of the
// particle. As due to the magnetic field particles are only deflected in
// x-direction, this configuration offers the best resolution.
// The layer structure in the FT is XUVX-XUVX-XUVX.
//
// The detector is divided into an upeer and a lower side (>/< y=0). As particles
// are only deflected in x direction there are only very(!) few particles that go
// from the lower to the upper side, or vice versa. The reconstruction algorithm
// can therefore be split into two independent steps: First track reconstruction
// for tracks in the upper side, and afterwards for tracks in the lower side.
//
// Due to construction issues this is NOT true for U/V layers. In these layers the
// complete(!) fibre modules are rotated, producing a zic-zac pattern at y=0, also
// called  "the triangles". Therefore for U/V layers it must be explicetly also
// searched for these hit on the "other side", if the track is close to y=0.
// Sketch (rotation exagerated!):
//                                          _.*
//     y ^   _.*                         _.*
//       | .*._      Upper side       _.*._
//       |     *._                 _.*     *._
//       |--------*._           _.*           *._----------------> x
//       |           *._     _.*                 *._     _.*
//                      *._.*       Lower side      *._.*
//
//
//
//
//
//       Zone ordering defined on PrKernel/PrFTInfo.h
//
//     y ^
//       |    1  3  5  7     9 11 13 15    17 19 21 23
//       |    |  |  |  |     |  |  |  |     |  |  |  |
//       |    x  u  v  x     x  u  v  x     x  u  v  x   <-- type of layer
//       |    |  |  |  |     |  |  |  |     |  |  |  |
//       |------------------------------------------------> z
//       |    |  |  |  |     |  |  |  |     |  |  |  |
//       |    |  |  |  |     |  |  |  |     |  |  |  |
//       |    0  2  4  6     8 10 12 14    16 18 20 22
//
//
// *** Short introduction in the Forward Tracking algorithm
//
// The track reconstruction is seperated into several steps:
//
// 1) Using only X-hits
//    1.1) Preselection: collectAllXHits()
//    1.2) Hough Transformation: xAtRef_SamePlaneHits()
//    1.3) Cluster search: selectXCandidates()
//    1.4) Linear and than Cubic Fit of X-Projection
// 2) Introducing U/V hits or also called stereo hits
//    2.1) Preselection: collectStereoHits
//    2.2) Cluster search: selectStereoHits
//    2.3) Fit Y-Projection
// 3) Using all (U+V+X) hits
//    3.1) Fitting X-Projection
//    3.2) calculating track quality with a Neural Net
//    3.3) final clone+ghost killing
//
// *****************************************************************

/** @class PrForwardTracking PrForwardTracking.cpp
 *
 *  \brief Main algorithm for the Forward tracking of the upgrade
 *  \brief The main work ins done in the extendTracks method
 *
 *
 *  @author Olivier Callot
 *  @date   2012-03-20
 *  @author Thomas Nikodem
 *  @date   2013-03-15
 *  @author Michel De Cian
 *  @date   2014-03-12 Changes with make code more standard and faster
 *  @author Sevda Esen
 *  @date   2015-02-13 additional search in the triangles by Marian Stahl
 *  @author Thomas Nikodem
 *  @date   2016-03-09 complete restructuring
 *  @author Olli Lupton
 *  @date   2018-11-07 Imported PrForwardTool into PrForwardTracking as a step towards making PrForwardTracking accept
 *                     selections
 *  @author André Günther
 *  @date 2019-12-03 adapted PrForwardTracking to new SoA hit class PrSciFiHits
 */

//-----------------------------------------------------------------------------

/** The following structs are left outside of the anonymous namespace
 * because there was a slowdown when including them!
 * It's unclear so far why this is the case.
 * See discussion here https://gitlab.cern.ch/lhcb/Rec/merge_requests/1815#note_3123847
 * Further understanding this probably requires digging into what the compiler/linker
 * is doing here.
 */

// Parameters passed around in functions
struct PrParameters {
  PrParameters( unsigned int minXHits_, float maxXWindow_, float maxXWindowSlope_, float maxXGap_,
                unsigned int minStereoHits_ )
      : minXHits{minXHits_}
      , maxXWindow{maxXWindow_}
      , maxXWindowSlope{maxXWindowSlope_}
      , maxXGap{maxXGap_}
      , minStereoHits{minStereoHits_} {}
  const unsigned int minXHits{};
  const float        maxXWindow{};
  const float        maxXWindowSlope{};
  const float        maxXGap{};
  unsigned int       minStereoHits{};
};

// Cache for ZoneHandler derived conditions
struct ZoneCache final {
  PrFTZoneHandler       handler;
  PrHitZone             firstZone, secondZone;
  std::array<float, 24> zoneZPos{std::numeric_limits<float>::signaling_NaN()};
  ZoneCache( const DeFTDetector& ftDet ) : handler( ftDet ) {
    for ( int i{0}; i < PrFTInfo::NFTLayers; ++i ) {
      zoneZPos[2 * i]     = handler.zone( 2 * i ).z();
      zoneZPos[2 * i + 1] = handler.zone( 2 * i + 1 ).z();
    }
    firstZone  = handler.zone( 0 );
    secondZone = handler.zone( 1 );
  }
};

namespace {
  using scalar = SIMDWrapper::scalar::types;
  using simd   = SIMDWrapper::avx256::types;
  using F      = scalar::float_v;
  using I      = scalar::int_v;
  using namespace SciFiHits;

  template <typename T>
  std::tuple<LHCb::Pr::Velo::Tracks const*, LHCb::Pr::Upstream::Tracks const*, LHCb::Pr::Seeding::Tracks const*>
  get_ancestors( T const& input_tracks ) {
    LHCb::Pr::Velo::Tracks const*     velo_ancestors     = nullptr;
    LHCb::Pr::Upstream::Tracks const* upstream_ancestors = nullptr;
    LHCb::Pr::Seeding::Tracks const*  seed_ancestors     = nullptr;
    if constexpr ( std::is_same_v<T, LHCb::Pr::Upstream::Tracks> ) {
      velo_ancestors     = input_tracks.getVeloAncestors();
      upstream_ancestors = &input_tracks;
    } else {
      velo_ancestors     = &input_tracks;
      upstream_ancestors = nullptr;
    }
    return {velo_ancestors, upstream_ancestors, seed_ancestors};
  }

  template <typename Range, typename Projection, typename Comparison, typename Value>
  auto best_element( Range&& range, Projection&& proj, Comparison&& cmp, Value best ) {
    using Element = std::pair<std::decay_t<Value>, std::add_const_t<ranges::range_value_type_t<Range>>*>;
    return ranges::accumulate(
        std::forward<Range>( range ), Element{std::move( best ), nullptr},
        [proj = std::forward<Projection>( proj ), cmp = std::forward<Comparison>( cmp )]( Element b, const auto& e ) {
          auto v = std::invoke( proj, e );
          if ( std::invoke( cmp, v, b.first ) ) {
            b.first  = std::move( v );
            b.second = &e;
          }
          return b;
        } );
  }

  // Input variable names for TMVA NeuralNet
  const std::vector<std::string> mlpInputVars{{"nPlanes"}, {"dSlope"}, {"dp"}, {"slope2"}, {"dby"}, {"dbx"}, {"day"}};

  constexpr auto  zMagnetParams  = std::array{5212.38f, 406.609f, -1102.35f, -498.039f};
  constexpr auto  xParams        = std::array{18.6195f, -5.55793f};
  constexpr auto  byParams       = std::array{-0.667996f};
  constexpr auto  cyParams       = std::array{-3.68424e-05f};
  constexpr auto  momentumParams = std::array{1.21014f, 0.637339f, -0.200292f, 0.632298f, 3.23793f, -27.0259f};
  constexpr float zReference     = 8520.f;

  constexpr auto NLayerBoundaries = PrFTInfo::NFTXLayers + 1;

  /** numberallXHitsAfterCuts controls size of internal x hit storage for Hough transform
   * it is taken as the maximum number of hits which can occur in the SciFi stations (~25000)
   * divided by two for each detector half and again by two because only half the layers are x.
   * With GEC this number might be much lower, however, the algorithm will crash if chosen too small.
   */
  constexpr std::size_t numberallXHitsAfterCuts{6250};

  // constants for the extrapolation polynomial from Velo to SciFi, used to determine minPT cut border in S3L0
  constexpr std::array<float, 8> toSciFiExtParams{4824.31956565f,  426.26974766f,   7071.08408876f, 12080.38364257f,
                                                  14077.79607408f, 13909.31561208f, 9315.34184959f, 3209.49021545f};

  inline simd::mask_v matchStereoHit( unsigned& xStartUV, const simd::float_v& xMinUV, const simd::float_v& xMaxUV,
                                      const PrSciFiHits& SciFiHits, const simd::mask_v& loopmask ) {
    auto x       = simd::float_v( SciFiHits.x( xStartUV ) );
    auto minMask = x > xMinUV;
    auto maxMask = x < xMaxUV;
    auto out     = minMask && maxMask;
    while ( !all( minMask || !loopmask ) ) {
      ++xStartUV;
      x       = simd::float_v( SciFiHits.x( xStartUV ) );
      minMask = x > xMinUV;
      maxMask = x < xMaxUV;
      out     = out || ( minMask && maxMask );
    }
    return out;
  }

  template <PrHitZone::Side SIDE>
  inline simd::mask_v matchStereoHitWithTriangle( unsigned& xStartUV, const simd::float_v& xMinUV,
                                                  const simd::float_v& xMaxUV, float yInZone, float tolerance,
                                                  const PrSciFiHits& SciFiHits, const simd::mask_v& loopmask ) {

    auto matchTriangle = [&]( auto& out, const auto& match ) -> void {
      if constexpr ( SIDE == PrHitZone::Side::Upper ) {
        if ( SciFiHits.yMax( xStartUV ) > yInZone - tolerance ) { out = out || match; }
      } else {
        if ( SciFiHits.yMin( xStartUV ) < yInZone + tolerance ) { out = out || match; }
      }
    };

    auto x       = simd::float_v( SciFiHits.x( xStartUV ) );
    auto minMask = x > xMinUV;
    auto maxMask = x < xMaxUV;
    auto match   = minMask && maxMask;
    auto out     = simd::mask_false();
    while ( !all( minMask || !loopmask ) ) {

      matchTriangle( out, match );

      ++xStartUV;
      x       = simd::float_v( SciFiHits.x( xStartUV ) );
      minMask = x > xMinUV;
      maxMask = x < xMaxUV;
      match   = ( minMask && maxMask );
    }
    matchTriangle( out, match );
    return out;
  }

  //=========================================================================
  //  Returns estimate of magnet kick position
  //=========================================================================
  float zMagnet( const PrForwardTrack<>& track ) {
    return ( zMagnetParams[0] + zMagnetParams[2] * track.seed().tx2 + zMagnetParams[3] * track.seed().ty2 );
  }
  //=========================================================================
  //  Returns the best momentum estimate
  //=========================================================================
  float calcqOverP( const PrForwardTrack<>& track, const float& magScaleFactor ) {
    constexpr float inv_gev = 1.0 / Gaudi::Units::GeV;
    float           qop     = inv_gev;
    if ( std::abs( magScaleFactor ) > 1e-6f ) {
      const float bx  = track.xSlope( zReference );
      const float bx2 = bx * bx;
      const auto  obs = {bx2, bx2 * bx2, bx * track.seed().tx, track.seed().ty2, track.seed().ty2 * track.seed().ty2};
      const float coef =
          std::inner_product( begin( obs ), end( obs ), std::next( begin( momentumParams ) ), momentumParams[0] );
      const float proj = std::sqrt( ( 1.f + track.seed().slope2 ) / ( 1.f + track.seed().tx2 ) );
      qop *= ( track.seed().tx - bx ) / ( coef * proj * magScaleFactor );
    }
    return qop;
  }
  namespace details {
    template <int first, int last, typename Iterators, typename Cmp = std::less<>>
    void inplace_merge( const Iterators& iters, const Cmp& cmp = Cmp() ) {
      static_assert( last >= first );
      if constexpr ( first == last || last == first + 1 ) return;
      constexpr int pivot = first + ( 1 + last - first ) / 2;
      inplace_merge<first, pivot>( iters, cmp );
      inplace_merge<pivot, last>( iters, cmp );
      std::inplace_merge( iters[first], iters[pivot], iters[last], cmp );
    }
  } // namespace details

  //=========================================================================
  //  Merge the layers of x hits for the hough transform
  //=========================================================================
  template <auto N, typename Container, typename Cmp = std::less<>>
  void mergeSortedRanges( Container& allXHits, LHCb::span<int, N> boundaries, const Cmp& cmp = Cmp() ) {
    static_assert( N > 1 );
    std::array<typename Container::iterator, N> offset;
    using std::begin;
    using std::end;
    std::transform( begin( boundaries ), end( boundaries ), begin( offset ),
                    [start = begin( allXHits )]( int b ) { return start + b; } );
    details::inplace_merge<0, N - 1>( offset, cmp );
  }

  template <typename Container>
  void setHitsUsed( unsigned first, unsigned last, const PrForwardTrack<>& track, const PrParameters& pars,
                    Container& allXHits ) {
    // Hits before it1 are not checked. Thus this method does not work perfect. However, it seems good enough :)
    assert( first <= last );
    const auto& xPars = track.getXParams();
    const float x1    = xPars.get( 0 ) + 2 * pars.maxXWindow;
    while ( first < last ) {
      first = [&]() {
        while ( !allXHits.isValid( first ) ) ++first;
        return first;
      }();
      if ( first >= last || allXHits.coords[first] > x1 ) return;
      // search hit in track
      const auto& ihits = track.ihits();
      auto        it    = std::find( ihits.begin(), ihits.end(), allXHits.getMainIndex( first ) );
      if ( it != ihits.end() ) allXHits.setInvalid( first ); // coord == max means the same as hit is deleted
      ++first;
    }
  }

  //=========================================================================
  //  Set the parameters of the track, from the (average) x at reference
  //=========================================================================
  void setTrackParameters( PrForwardTrack<>& track, const float xAtRef ) {

    float       dSlope    = ( track.seed().x( zReference ) - xAtRef ) / ( zReference - zMagnetParams[0] );
    const float zMagSlope = zMagnetParams[2] * track.seed().tx2 + zMagnetParams[3] * track.seed().ty2;
    const float zMag      = zMagnetParams[0] + zMagnetParams[1] * dSlope * dSlope + zMagSlope;
    assert( zMag != zReference && "zMag can not be equal to zReference" );
    const float xMag   = track.seed().x( zMag );
    const float slopeT = ( xAtRef - xMag ) / ( zReference - zMag );
    dSlope             = slopeT - track.seed().tx;
    const float dyCoef = dSlope * dSlope * track.seed().ty;

    track.setParams( {xAtRef, slopeT, 1.e-6f * xParams[0] * dSlope, 1.e-9f * xParams[1] * dSlope,
                      track.seed().y( zReference ), track.seed().ty + dyCoef * byParams[0], dyCoef * cyParams[0]} );
  }

  //=========================================================================
  //  Compute the x projection at the reference plane
  //=========================================================================

  // template <typename Container>
  inline void xAtRef_SamePlaneHits( const PrForwardTrack<>& track, unsigned start, float zHit,
                                    ModSciFiHits::ModPrSciFiHits& allXHits ) {

    const auto xFromVelo_Hit = track.seed().x( zHit );
    const auto zMagSlope     = ( zMagnetParams[2] * track.seed().tx2 + zMagnetParams[3] * track.seed().ty2 );
    const auto tx            = ( track.seed().tx );
    const auto dSlopeDivPart = 1.f / ( zHit - zMagnetParams[0] );
    const auto dz            = 1.e-3f * ( zHit - zReference );
    const auto dz2           = 1.e-6f * ( zHit - zReference ) * ( zHit - zReference );

    const auto allXHitsSize = allXHits.size();
    for ( auto i = start; i < allXHitsSize; i += simd::size ) {
      const simd::float_v xHits  = allXHits.coord<simd::float_v>( i );
      const simd::float_v dSlope = ( xFromVelo_Hit - xHits ) * dSlopeDivPart;
      const simd::float_v zMag   = zMagnetParams[0] + zMagnetParams[1] * dSlope * dSlope + zMagSlope;
      const simd::float_v dxCoef = dz2 * ( xParams[0] + dz * xParams[1] ) * dSlope;
      const simd::float_v xMag   = xFromVelo_Hit + tx * ( zMag - zHit );
      const simd::float_v ratio  = ( zReference - zMag ) / ( zHit - zMag );
      allXHits.store_coord( i, xMag + ratio * ( xHits + dxCoef - xMag ) );
    }
  }

  struct counting_inserter {
    int                count = 0;
    counting_inserter& operator++() { return *this; }
    counting_inserter& operator*() { return *this; }
    template <typename T>
    counting_inserter& operator=( const T& ) {
      ++count;
      return *this;
    }
  };

  void removeDuplicates( PrForwardTracks& tracks, std::vector<std::vector<LHCb::LHCbID>> const& ids,
                         float const deltaQuality ) {
    float const zEndT = StateParameters::ZEndT;
    for ( size_t i1 = 0, nTotal = tracks.size(); i1 < nTotal; ++i1 ) {
      auto& t1 = tracks[i1];
      if ( !t1.valid() ) continue;
      for ( size_t i2 = i1 + 1; i2 < nTotal; ++i2 ) {
        auto& t2 = tracks[i2];
        if ( !t2.valid() ) continue;
        if ( std::abs( t1.x( zEndT ) - t2.x( zEndT ) ) > 50.f )
          break; // The distance only gets larger, as the vectors are sorted
        if ( std::abs( t1.y( zEndT ) - t2.y( zEndT ) ) > 100.f ) continue;

        int const nCommon =
            std::set_intersection( ids[i1].begin(), ids[i1].end(), ids[i2].begin(), ids[i2].end(), counting_inserter{} )
                .count;

        if ( nCommon > .4f * ( ids[i1].size() + ids[i2].size() ) ) {
          float const delta_q = t2.quality() - t1.quality();
          if ( delta_q > deltaQuality ) {
            t2.setValid( false );
          } else if ( delta_q < -deltaQuality ) {
            t1.setValid( false );
          }
        }
      }
    }
  }

  std::vector<std::vector<LHCb::LHCbID>> extractAndSortLHCbIDs( const PrForwardTracks& tracks,
                                                                const PrSciFiHits&     SciFiHits ) {
    std::vector<std::vector<LHCb::LHCbID>> ids;
    ids.reserve( tracks.size() );
    for ( auto const& track : tracks ) {
      auto const& ihits = track.ihits();
      auto&       idVec = ids.emplace_back();
      idVec.reserve( ihits.size() );
      std::transform( ihits.begin(), ihits.end(), std::back_inserter( idVec ),
                      [&SciFiHits]( const unsigned& ihit ) { return LHCb::LHCbID( SciFiHits.ID( ihit ) ); } );
      std::sort( idVec.begin(), idVec.end() );
    }
    return ids;
  }

  class yShiftCalculator final {
  private:
    float m_t1{};
    float m_t2{};
    float m_magscf{};

  public:
    yShiftCalculator( VeloSeed const& seed, const float magscalefactor ) : m_magscf{magscalefactor} {
      const float ty  = seed.ty;
      const float ty3 = ty * ty * ty;
      const float tx  = seed.tx;
      const float tx2 = tx * tx;
      const float tx3 = tx * tx * tx;
      // parameters of the polynomial are obtained from MC toy studies
      // (see
      // https://indico.cern.ch/event/810764/contributions/3394473/attachments/1830241/2997521/Fast_Upgrade_Forward_Tracking.pdf)
      m_t1 = ( 1.95859013e+01f * ty + -1.69927519e+03f * tx2 * ty + 9.96117334e+02f * ty3 +
               6.18235545e+04f * tx2 * ty3 ); // doesn't depend on sign of qop
      m_t2 = ( 1.01414534e+00f + 1.96863566e+03f * tx * ty + 5.42636977e+03f * tx3 * ty +
               -1.23244167e+04f * tx * ty3 ); // need to multiply by a factor of sign of qop
    }
    inline const simd::float_v calcYCorrection( simd::float_v const& dSlope, unsigned layer ) const {
      constexpr float                c0      = 1.f / 0.2677f;
      constexpr std::array<float, 6> scaling = {0.62f, 0.63f, 0.83f, 0.85f, 1.00f, 1.03f};
      const simd::float_v            factor  = m_magscf * copysign( simd::float_v( 1.f ), dSlope );
      return ( m_t1 + m_t2 * factor ) * abs( dSlope ) * c0 * scaling[layer];
    }
  };
} // namespace

template <typename T>
class PrForwardTracking : public Gaudi::Functional::Transformer<LHCb::Pr::Long::Tracks( SciFiHits::PrSciFiHits const&,
                                                                                        T const&, ZoneCache const& ),
                                                                LHCb::DetDesc::usesConditions<ZoneCache>> {
public:
  using PrSciFiHits = SciFiHits::PrSciFiHits;
  using base_class_t =
      Gaudi::Functional::Transformer<LHCb::Pr::Long::Tracks( PrSciFiHits const&, T const&, ZoneCache const& ),
                                     LHCb::DetDesc::usesConditions<ZoneCache>>;
  using base_class_t::addConditionDerivation;
  using base_class_t::debug;
  using base_class_t::error;
  using base_class_t::info;
  using base_class_t::inputLocation;
  using base_class_t::msgLevel;

  //=============================================================================
  // Standard constructor, initializes variables
  //=============================================================================
  PrForwardTracking( const std::string& name, ISvcLocator* pSvcLocator )
      : base_class_t(
            name, pSvcLocator,
            std::array{typename base_class_t::KeyValue{"ForwardHitsLocation", PrFTInfo::SciFiHitsLocation},
                       typename base_class_t::KeyValue{"InputName", LHCb::TrackLocation::Velo},
                       typename base_class_t::KeyValue{"ZoneCache", "AlgorithmSpecific-" + name + "-ZoneCache"}},
            typename base_class_t::KeyValue{"OutputName", "Rec/Track/FTSoA"} )
      , m_MLPReader_1st{mlpInputVars}
      , m_MLPReader_2nd{mlpInputVars} {}

  //=============================================================================
  // Initialization
  //=============================================================================

  StatusCode initialize() override {
    auto sc = base_class_t::initialize();
    if ( sc.isFailure() ) return sc; // error printed already by GaudiAlgorithm

    if ( msgLevel( MSG::DEBUG ) ) debug() << "==> Initialize" << endmsg;
    // info()<<"......DEBUGS Initialize Forward BEGIN" <<endmsg;

    // Initialise stuff we imported from PrForwardTool

    if ( m_addUTHitsTool.name().empty() ) m_addUTHitsTool.disable();

    // check options
    if ( m_maxChi2StereoLinear <= m_maxChi2Stereo ) {
      error() << "Error: m_maxChi2StereoLinear must be chosen larger than m_maxChi2Stereo" << endmsg;
      return StatusCode::FAILURE;
    }

    // Zones cache
    this->template addConditionDerivation<ZoneCache( const DeFTDetector& )>(
        {DeFTDetectorLocation::Default}, this->template inputLocation<ZoneCache>() );

    m_magscalefactor = m_magFieldSvc->signedRelativeCurrent();

    return sc;
  }

  /// main call
  LHCb::Pr::Long::Tracks operator()( PrSciFiHits const&, T const&, ZoneCache const& ) const override final;

private:
  // Parameters for debugging
  // Gaudi::Property<int> m_wantedKey{this, "WantedKey", -100};
  // Gaudi::Property<int> m_veloKey{this, "VeloKey", -100};

  // Parameters that used to come from PrForwardTool
  Gaudi::Property<float> m_yTolUVSearch{this, "YToleranceUVsearch", 11. * Gaudi::Units::mm};
  Gaudi::Property<float> m_tolY{this, "TolY", 5. * Gaudi::Units::mm};
  Gaudi::Property<float> m_tolYSlope{this, "TolYSlope", 0.002 * Gaudi::Units::mm};
  Gaudi::Property<float> m_maxChi2LinearFit{this, "MaxChi2LinearFit", 100.};
  Gaudi::Property<float> m_maxChi2XProjection{this, "MaxChi2XProjection", 15.};
  Gaudi::Property<float> m_maxChi2PerDoF{this, "MaxChi2PerDoF", 7.};

  Gaudi::Property<float> m_tolYMag{this, "TolYMag", 10. * Gaudi::Units::mm};
  Gaudi::Property<float> m_tolYMagSlope{this, "TolYMagSlope", 0.015};
  Gaudi::Property<float> m_minYGap{this, "MinYGap", 0.4 * Gaudi::Units::mm};

  Gaudi::Property<unsigned int> m_minTotalHits{this, "MinTotalHits", 10};
  Gaudi::Property<float>        m_maxChi2StereoLinear{this, "MaxChi2StereoLinear", 60.};
  Gaudi::Property<float>        m_maxChi2Stereo{this, "MaxChi2Stereo", 4.5};

  // first loop Hough Cluster search
  Gaudi::Property<unsigned int> m_minXHits{this, "MinXHits", 5};
  Gaudi::Property<float>        m_maxXWindow{this, "MaxXWindow", 1.2 * Gaudi::Units::mm};
  Gaudi::Property<float>        m_maxXWindowSlope{this, "MaxXWindowSlope", 0.002 * Gaudi::Units::mm};
  Gaudi::Property<float>        m_maxXGap{this, "MaxXGap", 1.2 * Gaudi::Units::mm};
  Gaudi::Property<unsigned int> m_minSingleHits{this, "MinSingleHits", 2};

  // second loop Hough Cluster search
  Gaudi::Property<bool>         m_secondLoop{this, "SecondLoop", true};
  Gaudi::Property<unsigned int> m_minXHits2nd{this, "MinXHits2nd", 4};
  Gaudi::Property<float>        m_maxXWindow2nd{this, "MaxXWindow2nd", 1.5 * Gaudi::Units::mm};
  Gaudi::Property<float>        m_maxXWindowSlope2nd{this, "MaxXWindowSlope2nd", 0.002 * Gaudi::Units::mm};
  Gaudi::Property<float>        m_maxXGap2nd{this, "MaxXGap2nd", 0.5 * Gaudi::Units::mm};

  // stereo hit matching
  Gaudi::Property<float> m_tolYCollectX{this, "TolYCollectX", 3.5 * Gaudi::Units::mm};
  Gaudi::Property<float> m_tolYSlopeCollectX{this, "TolYSlopeCollectX", 0.001 * Gaudi::Units::mm};
  Gaudi::Property<float> m_tolYTriangleSearch{this, "TolYTriangleSearch", 20.f};

  // collectX search
  Gaudi::Property<float> m_minPT{this, "MinPT", 50.f * Gaudi::Units::MeV};
  Gaudi::Property<float> m_minP{this, "MinP", 1500.f * Gaudi::Units::MeV};

  // veloUT momentum estimate
  Gaudi::Property<bool>  m_useMomentumEstimate{this, "UseMomentumEstimate", false};
  Gaudi::Property<bool>  m_useWrongSignWindow{this, "UseWrongSignWindow", true};
  Gaudi::Property<float> m_wrongSignPT{this, "WrongSignPT", 2000. * Gaudi::Units::MeV};

  // Momentum guided search window switch
  Gaudi::Property<bool> m_useMomSearchWindow{this, "UseMomentumGuidedSearchWindow", false};
  // calculate an upper error boundary on the tracks x-prediction,
  Gaudi::Property<float> m_upperLimitOffset{this, "UpperLimitOffset", 100.f};
  Gaudi::Property<float> m_upperLimitSlope{this, "UpperLimitSlope", 2800.f};
  Gaudi::Property<float> m_upperLimitMax{this, "UpperLimitMax", 600.f};
  Gaudi::Property<float> m_upperLimitMin{this, "UpperLimitMin", 150.f};
  // same as above for the lower limit
  Gaudi::Property<float> m_lowerLimitOffset{this, "LowerLimitOffset", 50.f};
  Gaudi::Property<float> m_lowerLimitSlope{this, "LowerLimitSlope", 1400.f};
  Gaudi::Property<float> m_lowerLimitMax{this, "LowerLimitMax", 600.f};

  // preselection
  Gaudi::Property<bool>  m_preselection{this, "Preselection", false};
  Gaudi::Property<float> m_preselectionPT{this, "PreselectionPT", 400.f * Gaudi::Units::MeV};
  Gaudi::Property<float> m_preselectionP{this, "PreselectionP", 1000.f * Gaudi::Units::MeV};

  // Track Quality (Neural Net)
  Gaudi::Property<float> m_maxQuality{this, "MaxQuality", 0.9};
  Gaudi::Property<float> m_deltaQuality{this, "DeltaQuality", 0.1};

  using ErrorCounter = Gaudi::Accumulators::MsgCounter<MSG::ERROR>;

  mutable ErrorCounter m_maxTracksErr{this, "Number of tracks reached maximum!"};

  mutable Gaudi::Accumulators::SummingCounter<unsigned int> m_nbOutputTracksCounter{this, "Nb output tracks"};

  // -- MAIN METHOD
  PrForwardTracks extendTracks( T const& input_track, PrSciFiHits const& SciFiHits, ZoneCache const& zonecache ) const;

  // make full PrForward Track candidates
  template <typename Container>
  void selectFullCandidates( Container& trackCandidates, const PrSciFiHits& SciFiHits, PrParameters& pars,
                             ZoneCache const& zonecache ) const;

  // work wth with X-hits on x projection, side = top or bottom
  template <PrHitZone::Side SIDE, typename Container>
  void collectAllXHits( Container& allXHits, const PrForwardTrack<>& track, const PrSciFiHits& SciFiHits,
                        const PrParameters& pars, ZoneCache const& zonecache ) const;

  template <PrHitZone::Side SIDE, typename Container, typename OtherContainer>
  void selectXCandidates( OtherContainer& trackCandidates, const PrForwardTrack<>& seed, Container& allXHits,
                          const PrSciFiHits& SciFiHits, const PrParameters& pars, ZoneCache const& zonecache ) const;

  void fastLinearFit( PrForwardTrack<>& track, const PrParameters& pars, PrPlaneCounter& pc ) const;

  // also used in the end for "complete" fit
  bool fitXProjection( PrForwardTrack<>& track, const PrParameters& pars, PrPlaneCounter& pc ) const;

  template <PrHitZone::Side SIDE>
  bool addHitsOnEmptyXLayers( PrForwardTrack<>& track, bool refit, const PrSciFiHits& SciFiHits,
                              const PrParameters& pars, PrPlaneCounter& pc, ZoneCache const& zonecache ) const;

  // work with U/V hits on y projection
  template <typename Container>
  void collectStereoHits( PrForwardTrack<>& track, Container& stereoHits, const PrSciFiHits& SciFiHits,
                          ZoneCache const& zonecache ) const;

  template <typename Container>
  bool selectStereoHits( PrForwardTrack<>& track, const PrSciFiHits& SciFiHits, const Container& allStereoHits,
                         const PrParameters& pars, ZoneCache const& zonecache ) const;
  template <typename Container>
  bool fitYProjection( PrForwardTrack<>& track, Container& stereoHits, const PrParameters& pars,
                       PrPlaneCounter& pc ) const;

  template <typename Container>
  bool addHitsOnEmptyStereoLayers( PrForwardTrack<>& track, Container& stereoHits, const PrSciFiHits& SciFiHits,
                                   const PrParameters& pars, PrPlaneCounter& pc, ZoneCache const& zonecache ) const;

  std::tuple<const float, const float> calculateDx( const VeloSeed& seed ) const;

  // save good tracks
  template <typename Container>
  LHCb::Pr::Long::Tracks makeLHCbTracks( Container const& trackCandidates, std::vector<std::vector<LHCb::LHCbID>> ids,
                                         T const& ) const;

  // ====================================================================================
  // -- DEBUG HELPERS
  // ====================================================================================

  void printHit( const unsigned ihit, const PrSciFiHits* SciFiHits, const std::string title = "" ) const {
    debug() << "  " << title
            << format( "Plane%3d z0 %8.2f x0 %8.2f", SciFiHits->planeCode( ihit ), SciFiHits->z( ihit ),
                       SciFiHits->x( ihit ) );
    // if ( m_debugTool ) m_debugTool->printKey( debug(), LHCb::LHCbID( SciFiHits->ID( ihit ) ) );
    // if ( matchKey( SciFiHits->ID( ihit ) ) ) debug() << " ***";
    debug() << endmsg;
  }

  //=========================================================================
  //  Print the content of a track
  //=========================================================================
  void printTrack( const PrForwardTrack<>& track ) const {
    if ( !track.valid() ) {
      debug() << "Invalid Track " << endmsg;
      return;
    }
    if ( track.nDoF() > 0 ) {
      debug() << format( "   Track nDoF %3d   chi2/nDoF %7.3f quality %6.3f", int( track.getChi2nDof().get( 1 ) ),
                         track.chi2PerDoF(), track.quality() )
              << endmsg;
    } else {
      debug() << "   Track" << endmsg;
    }

    debug() << "hits:" << endmsg;
    const auto SciFiHits = track.getSciFiHits();
    for ( const auto& ihit : track.ihits() ) {
      debug() << format( "dist %7.3f dy %7.2f chi2 %7.2f ", track.distance( ihit ),
                         SciFiHits->dxDy( ihit ) != 0 ? track.distance( ihit ) / SciFiHits->dxDy( ihit )
                                                      : std::numeric_limits<float>::quiet_NaN(),
                         track.chi2( ihit ) );
      printHit( ihit, SciFiHits );
    }
  }

  // Tools
  ToolHandle<IPrAddUTHitsTool> m_addUTHitsTool{this, "AddUTHitsToolName", "PrAddUTHitsTool"};

  float                         m_magscalefactor{std::numeric_limits<float>::signaling_NaN()};
  ServiceHandle<ILHCbMagnetSvc> m_magFieldSvc{this, "MagneticFieldService", "MagneticFieldSvc"};

  // Neural Net The neural net readers are classes generated by TMVA. They are not threadsafe
  // and have been modified by hand. That will have to be done every time the MLP is retuned!
  // For as long as there are the hand tuned networks, no mutex here.
  ReadMLP_Forward1stLoop m_MLPReader_1st;
  ReadMLP_Forward2ndLoop m_MLPReader_2nd;
};

DECLARE_COMPONENT_WITH_ID( PrForwardTracking<LHCb::Pr::Upstream::Tracks>, "PrForwardTracking" )
DECLARE_COMPONENT_WITH_ID( PrForwardTracking<LHCb::Pr::Velo::Tracks>, "PrForwardTrackingVelo" )

//=============================================================================
// Main execution
//=============================================================================
template <typename T>
LHCb::Pr::Long::Tracks PrForwardTracking<T>::operator()( PrSciFiHits const& prSciFiHits, T const& input_tracks,
                                                         ZoneCache const& cache ) const {

  if ( msgLevel( MSG::DEBUG ) ) debug() << "==> Execute" << endmsg;
  // info()<<"......DEBUGS Forward BEGIN" <<endmsg;

  if ( UNLIKELY( input_tracks.size() == 0 ) ) {
    auto [velo_ancestors, upstream_ancestors, seed_ancestors] = get_ancestors( input_tracks );
    return {velo_ancestors, upstream_ancestors, seed_ancestors};
  }

  //============================================================
  //== Main processing: Extend selected tracks
  //============================================================

  // -- Loop over all Velo input tracks and try to find extension in the T-stations
  // -- This is the main 'work' of the forward tracking.
  auto candidates = extendTracks( input_tracks, prSciFiHits, cache );

  //============================================================
  //== Final processing: filtering of duplicates,...
  //============================================================

  // -- Sort the tracks according to their x-position of the state in the T-stations
  // -- in order to make the final loop faster.
  float const zEndT = StateParameters::ZEndT;
  std::sort( candidates.begin(), candidates.end(),
             [&zEndT]( auto const& track1, auto const& track2 ) { return track1.x( zEndT ) < track2.x( zEndT ); } );

  auto ids = extractAndSortLHCbIDs( candidates, prSciFiHits );
  removeDuplicates( candidates, ids, m_deltaQuality );
  auto outputTracks = makeLHCbTracks( candidates, std::move( ids ), input_tracks );

  m_nbOutputTracksCounter += outputTracks.size();
  return outputTracks;
}

//=========================================================================
//  Main method: Process a track
//=========================================================================
template <typename T>
PrForwardTracks PrForwardTracking<T>::extendTracks( T const& input_track, PrSciFiHits const& SciFiHits,
                                                    ZoneCache const& zonecache ) const {
  PrForwardTracks tracks;
  tracks.reserve( input_track.size() );
  PrForwardTracks result;
  result.reserve( input_track.size() ); // TODO: Reserve proper size, maybe find a fraction of forwarded tracks

  // in the worst(!) case preslection <<< HERE!>
  auto const inputtracks = LHCb::Pr::make_zip<SIMDWrapper::InstructionSet::Scalar>( input_track );
  for ( auto const& itrack : inputtracks ) {

    const auto [endv_qp, endv_pos, endv_dir] = [&]() {
      if constexpr ( std::is_same_v<T, LHCb::Pr::Upstream::Tracks> ) {
        Vec3<F> const endv_pos = itrack.StatePos();
        Vec3<F> const endv_dir = itrack.StateDir();
        return std::tuple{itrack.qOverP().cast(), endv_pos, endv_dir};
      } else {
        Vec3<F> const endv_pos = itrack.StatePos( 1 );
        Vec3<F> const endv_dir = itrack.StateDir( 1 );
        return std::tuple{1.0f, endv_pos, endv_dir};
      }
    }();

    // everything I need from the end velo state
    float const endv_x  = endv_pos.x.cast();
    float const endv_y  = endv_pos.y.cast();
    float const endv_tx = endv_dir.x.cast();
    float const endv_ty = endv_dir.y.cast();
    float const endv_z  = endv_pos.z.cast();

    if constexpr ( std::is_same_v<T, LHCb::Pr::Upstream::Tracks> ) {
      float const endv_tx2  = endv_tx * endv_tx;
      float const endv_ty2  = endv_ty * endv_ty;
      float const endv_txy2 = endv_tx2 + endv_ty2;
      float const endv_txy  = std::sqrt( endv_txy2 );
      float const endv_pq   = 1.f / endv_qp;
      float const endv_p    = std::abs( endv_pq );
      float const endv_pz   = endv_p / std::sqrt( 1.f + endv_txy2 );
      float const endv_pt   = endv_pz * endv_txy;

      if ( m_useMomentumEstimate ) {
        if ( endv_p < m_preselectionP ) continue;
        if ( m_preselection ) {
          if ( endv_pt < m_preselectionPT ) continue;
        }
      }
    }
    /** Check here if the straight line extrapolation in x and y of track hits the first station.
     * This has no influence on MC checked efficiencies which seems a bit counter-intuitive, but
     * it definitely improves ghost rate and throughput. A similar check is performed later also
     * for the other layers.
     */
    if ( const float xFirstStation = endv_x + endv_tx * ( zonecache.zoneZPos[0] - endv_z ),
         yFirstStation             = endv_y + endv_ty * ( zonecache.zoneZPos[0] - endv_z );
         !zonecache.firstZone.isInside( xFirstStation, yFirstStation ) &&
         !zonecache.secondZone.isInside( xFirstStation, yFirstStation ) ) {
      continue;
    }
    tracks.emplace_back( itrack.offset(), endv_x, endv_y, endv_z, endv_tx, endv_ty, endv_qp );
  }
  // First loop Hough Cluster search
  PrParameters pars{m_minXHits, m_maxXWindow, m_maxXWindowSlope, m_maxXGap, 4u};
  PrParameters pars2ndLoop{m_minXHits2nd, m_maxXWindow2nd, m_maxXWindowSlope2nd, m_maxXGap2nd, 4u};

  // reserve enough space for candidate tracks, usually N(cand)<10 and N(2ncand)<40
  PrForwardTracks trackCandidates;
  trackCandidates.reserve( 50 );
  PrForwardTracks trackCandidates2ndLoop;
  trackCandidates2ndLoop.reserve( 100 );

  ModSciFiHits::ModPrSciFiHits allXHitsUpper{numberallXHitsAfterCuts};
  ModSciFiHits::ModPrSciFiHits allXHitsLower{numberallXHitsAfterCuts};

  for ( const auto& track : tracks ) {

    allXHitsUpper.clear();
    allXHitsLower.clear();

    trackCandidates.clear();
    trackCandidates2ndLoop.clear();

    // calculate y of velo at zref to decide on which side to search
    const float yAtRef = track.seed().y( zReference );

    // -- it happens rarely that we have to collect in both halves
    if ( yAtRef > -5.f ) {
      collectAllXHits<PrHitZone::Side::Upper>( allXHitsUpper, track, SciFiHits, pars, zonecache );
      selectXCandidates<PrHitZone::Side::Upper>( trackCandidates, track, allXHitsUpper, SciFiHits, pars, zonecache );
    }
    if ( yAtRef < 5.f ) {
      collectAllXHits<PrHitZone::Side::Lower>( allXHitsLower, track, SciFiHits, pars, zonecache );
      selectXCandidates<PrHitZone::Side::Lower>( trackCandidates, track, allXHitsLower, SciFiHits, pars, zonecache );
    }

    // -- > Debug --------
    if ( msgLevel( MSG::DEBUG ) ) {
      debug() << endmsg << "=============== Selected " << trackCandidates.size() << " candidates in 1st loop." << endmsg
              << endmsg;
      int nValidTracks = 0;
      for ( const auto& trackcand : trackCandidates ) {
        if ( trackcand.valid() ) printTrack( trackcand );
        nValidTracks++;
      }
      debug() << "  valid tracks :   " << nValidTracks << endmsg;
    }
    // -- < Debug --------

    // Stereo hit search and full Fit
    selectFullCandidates( trackCandidates, SciFiHits, pars, zonecache );

    // erase tracks not valid
    trackCandidates.erase( std::remove_if( trackCandidates.begin(), trackCandidates.end(),
                                           []( const auto& track ) { return !track.valid(); } ),
                           trackCandidates.end() );
    // -- > Debug --------
    if ( msgLevel( MSG::DEBUG ) ) {
      debug() << "********** final list of 1st loop candidates ********" << endmsg;
      for ( const auto& trackcand : trackCandidates ) printTrack( trackcand );
    }
    // -- < Debug --------

    // check tracks, if no OK track is found start second Loop search
    bool ok = std::any_of( trackCandidates.begin(), trackCandidates.end(),
                           []( const auto& track ) { return track.ihits().size() > 10; } );

    if ( !ok && m_secondLoop ) {
      // Second loop Hough Cluster search
      if ( yAtRef > -5.f ) {
        selectXCandidates<PrHitZone::Side::Upper>( trackCandidates2ndLoop, track, allXHitsUpper, SciFiHits, pars2ndLoop,
                                                   zonecache );
      }
      if ( yAtRef < 5.f ) {
        selectXCandidates<PrHitZone::Side::Lower>( trackCandidates2ndLoop, track, allXHitsLower, SciFiHits, pars2ndLoop,
                                                   zonecache );
      }
      selectFullCandidates( trackCandidates2ndLoop, SciFiHits, pars2ndLoop, zonecache );
      std::copy_if( std::make_move_iterator( trackCandidates2ndLoop.begin() ),
                    std::make_move_iterator( trackCandidates2ndLoop.end() ), std::back_inserter( trackCandidates ),
                    []( const auto& track ) { return track.valid(); } );

      ok = !trackCandidates.empty();
    }

    // clone+ghost killing after merging
    if ( ok || !m_secondLoop ) {
      auto [minQuality, best] = best_element(
          trackCandidates, [&]( const auto& t ) { return t.quality() + m_deltaQuality; }, std::less<>{},
          m_maxQuality.value() );
      std::copy_if( std::make_move_iterator( trackCandidates.begin() ),
                    std::make_move_iterator( trackCandidates.end() ), std::back_inserter( result ),
                    [minQuality = minQuality]( const auto& t ) { return t.quality() <= minQuality; } );
    }
  }
  return result;
}

//=========================================================================
//  Create Full candidates out of xCandidates
//  Searching for stereo hits
//  Fit of all hits
//  save everything in track candidate folder
//=========================================================================
template <typename T>
template <typename Container>
void PrForwardTracking<T>::selectFullCandidates( Container& trackCandidates, const PrSciFiHits& SciFiHits,
                                                 PrParameters& pars, ZoneCache const& zonecache ) const {

  int                  nbOK = 0;
  std::array<float, 7> mlpInput{}; // zero-initialise out of paranoia; shouldn't be needed...

  for ( auto& cand : trackCandidates ) {
    if ( !cand.valid() ) continue;
    cand.setValid( false ); // set only true after track passed everything

    // at least 4 stereo hits OR  minTotalHits - found xHits (WATCH unsigned numbers!)
    pars.minStereoHits = 4u;

    if ( cand.ihits().size() + pars.minStereoHits < m_minTotalHits ) {
      pars.minStereoHits = m_minTotalHits - cand.ihits().size();
    }
    // search for hits in U/V layers

    /**
     * the number 32 comes from experience and scales with the number of stereo layers
     * and the parameters m_tolY and m_tolYSlope. In the current tuning it happens rarely
     * that there are more than 5 stereo hits per layer collected
     */
    boost::container::small_vector<ModSciFiHits::ModPrSciFiHit, 32> stereoHits;
    collectStereoHits( cand, stereoHits, SciFiHits, zonecache ); //, uvZoneEnd );

    if ( stereoHits.size() < pars.minStereoHits ) continue;
    std::sort( stereoHits.begin(), stereoHits.end(), ModSciFiHits::byCoord() );

    // select best U/V hits
    if ( !selectStereoHits( cand, SciFiHits, stereoHits, pars, zonecache ) ) continue;
    // reset pc to count ALL hits
    PrPlaneCounter pc;
    pc.set( cand.ihits(), SciFiHits );

    // make a fit of ALL hits
    if ( !fitXProjection( cand, pars, pc ) ) continue;

    // check in empty x layers for hits
    auto checked_empty =
        ( cand.y( zReference ) < 0.f )
            ? addHitsOnEmptyXLayers<PrHitZone::Side::Lower>( cand, true, SciFiHits, pars, pc, zonecache )
            : addHitsOnEmptyXLayers<PrHitZone::Side::Upper>( cand, true, SciFiHits, pars, pc, zonecache );

    if ( !checked_empty ) continue;

    // track has enough hits, calcualte quality and save if good enough
    if ( pc.nbDifferent() >= m_minTotalHits ) {

      const float qOverP = calcqOverP( cand, m_magscalefactor );

      // orig params before fitting , TODO faster if only calc once?? mem usage?
      const float xAtRef    = cand.x( zReference );
      float       dSlope    = ( cand.seed().x( zReference ) - xAtRef ) / ( zReference - zMagnetParams[0] );
      const float zMagSlope = zMagnetParams[2] * cand.seed().tx2 + zMagnetParams[3] * cand.seed().ty2;
      const float zMag      = zMagnetParams[0] + zMagnetParams[1] * dSlope * dSlope + zMagSlope;
      const float xMag      = cand.seed().x( zMag );
      const float slopeT    = ( xAtRef - xMag ) / ( zReference - zMag );
      dSlope                = slopeT - cand.seed().tx;
      const float dyCoef    = dSlope * dSlope * cand.seed().ty;

      float bx = slopeT;
      float ay = cand.seed().y( zReference );
      float by = cand.seed().ty + dyCoef * byParams[0];

      // ay,by,bx params
      const auto  yPars = cand.getYParams();
      const float ay1   = yPars.get( 0 );
      const float by1   = yPars.get( 1 );
      const auto  xPars = cand.getXParams();
      const float bx1   = xPars.get( 1 );

      mlpInput[0] = pc.nbDifferent();
      mlpInput[1] = qOverP;
      mlpInput[2] = ( std::abs( cand.seed().qOverP ) < 1e-9f || !m_useMomentumEstimate )
                        ? 0.f
                        : cand.seed().qOverP - qOverP; // input_tracks - scifi
      mlpInput[3] = cand.seed().slope2;
      mlpInput[4] = by - by1;
      mlpInput[5] = bx - bx1;
      mlpInput[6] = ay - ay1;

      /// WARNING: if the NN classes straight out of TMVA are used, put a mutex here!
      float quality = ( pars.minXHits > 4 ? m_MLPReader_1st.GetMvaValue( mlpInput )    // 1st loop NN
                                          : m_MLPReader_2nd.GetMvaValue( mlpInput ) ); // 2nd loop NN
      quality       = 1.f - quality;                                                   // backward compability

      if ( quality < m_maxQuality ) {
        cand.setValid( true );
        cand.setQuality( quality );
        cand.setQoP( qOverP );
        // -- > Debug --------
        if ( msgLevel( MSG::DEBUG ) ) {
          debug() << "*** Accepted as track " << nbOK << " ***" << endmsg;
          printTrack( cand );
        }
        // -- < Debug --------
        ++nbOK;
      }
    }
  }
}

//=========================================================================
//  Calculate the size of the search window in the reference plane for both
//  cases, right and wrong sign.
//=========================================================================
template <typename T>
std::tuple<const float, const float> PrForwardTracking<T>::calculateDx( const VeloSeed& seed ) const {

  /**  this factor picks up the correct sign. "Correct" is defined by the polarity which was used when
   *  determining the extrapolation parameters. It's chosen here such that it's consistent with the
   *  SciFiTrackForwarding.
   */
  const float factor = m_useMomSearchWindow ? std::copysign( 1.f, seed.qOverP ) * ( -m_magscalefactor ) : 1.f;

  // this is a term occurring in the polynomial from which the x prediction is deduced
  const float term1 =
      toSciFiExtParams[0] + seed.tx * ( -toSciFiExtParams[1] * factor + toSciFiExtParams[2] * seed.tx ) +
      seed.ty2 * ( toSciFiExtParams[3] + seed.tx * ( toSciFiExtParams[4] * factor + toSciFiExtParams[5] * seed.tx ) );

  // 1/p, given a minPT and minP cut and the tracks slopes
  const float minInvPGeVfromPt = factor * 1000.f / m_minPT * std::sqrt( seed.slope2 / ( 1.f + seed.slope2 ) );
  const float minInvPGeV       = factor * 1000.f / m_minP;

  // calculate size of window for given cut
  const float minPBorder =
      minInvPGeV * ( term1 + minInvPGeV * ( toSciFiExtParams[6] * seed.tx + toSciFiExtParams[7] * minInvPGeV ) );
  const float minPTBorder =
      minInvPGeVfromPt *
      ( term1 + minInvPGeVfromPt * ( toSciFiExtParams[6] * seed.tx + toSciFiExtParams[7] * minInvPGeVfromPt ) );

  // choose smallest window resulting from momentum requirements
  const float minMomentumBorder = std::min( minPBorder, minPTBorder );

  // window shutters for case without momentum estimate, i.e. velo tracks as input
  float dxMin = -minMomentumBorder;
  float dxMax = minMomentumBorder;

  if constexpr ( std::is_same_v<T, LHCb::Pr::Upstream::Tracks> ) {
    if ( m_useMomSearchWindow ) {
      const float qOpGeV = seed.qOverP * 1000.f * ( -m_magscalefactor );
      // correction for prediction of tracks x position with ut momentum estimate
      const float xExt = qOpGeV * ( term1 + qOpGeV * ( toSciFiExtParams[6] * seed.tx + toSciFiExtParams[7] * qOpGeV ) );

      // error boundaries depend on momentum!
      const float upperError = std::clamp( m_upperLimitOffset + m_upperLimitSlope * std::abs( qOpGeV ),
                                           m_upperLimitMin.value(), m_upperLimitMax.value() );
      // calculate a lower error boundary on the tracks x-prediction,
      const float lowerError =
          std::min( m_lowerLimitOffset + m_lowerLimitSlope * std::abs( qOpGeV ), m_lowerLimitMax.value() );

      // depending on the sign of q, set the lower and upper error in the right direction
      // on top of that make sure that the upper error isn't larger than the minPTBorder
      // and the lower error isn't bigger than xExt which means we don't look further than the straight line prediction
      dxMin = factor > 0 ? ( ( xExt < lowerError ) ? 0.f : ( xExt - lowerError ) )
                         : std::max( xExt - upperError, minMomentumBorder );

      dxMax = factor > 0 ? std::min( xExt + upperError, minMomentumBorder )
                         : ( ( xExt > -lowerError ) ? 0.f : ( xExt + lowerError ) );

      // handle tracks which are above the wrong sign PT threshold, they have the upper error boundary in both
      // directions
      if ( const float pt = std::abs( 1.f / seed.qOverP ) * std::sqrt( seed.slope2 / ( 1.f + seed.slope2 ) );
           pt > m_wrongSignPT && m_useWrongSignWindow ) {
        dxMin = -std::abs( xExt ) - upperError;
        dxMax = std::abs( xExt ) + upperError;
      }
    }
  }
  return {dxMin, dxMax};
}

//=========================================================================
//  Collect all X hits, within a window defined by the minimum Pt.
//  Better restrictions possible, if we use the momentum of the input track.
//  Ask for the presence of a stereo hit in the same biLayer compatible.
//  This reduces the efficiency. X-alone hits to be re-added later in the processing
//=========================================================================
template <typename T>
template <PrHitZone::Side SIDE, typename Container>
void PrForwardTracking<T>::collectAllXHits( Container& allXHits, const PrForwardTrack<>& track,
                                            const PrSciFiHits& SciFiHits, const PrParameters& pars,
                                            ZoneCache const& zonecache ) const {

  const VeloSeed& seed = track.seed();

  const float zMag      = zMagnet( track );
  const float zS3L0     = zonecache.zoneZPos[PrFTZoneHandler::getXZone<SIDE>( 4 )];
  const float dzInvS3L0 = 1.f / ( zS3L0 - zMag );
  // calculate search window shutter, they are signed, thus are both added to straight extrapolation
  const auto [dxMin, dxMax] = calculateDx( seed );

  boost::container::static_vector<int, NLayerBoundaries> iZoneEnd{0}; // 6 x planes
  const yShiftCalculator                                 yc{seed, m_magscalefactor};

  for ( unsigned int iZone = 0; iZone < PrFTInfo::NFTXLayers; iZone++ ) {

    const auto  xZoneNumber = PrFTZoneHandler::getXZone<SIDE>( iZone );
    const float zZone       = zonecache.zoneZPos[xZoneNumber];
    const float xInZone     = seed.x( zZone );
    const auto& zone        = zonecache.handler.zone( xZoneNumber );

    if ( !zone.isInside( xInZone, seed.y( zZone ) ) ) continue;

    float ratio = ( zZone - zMag ) * dzInvS3L0;

    const float xMin = xInZone + dxMin * ratio;
    const float xMax = xInZone + dxMax * ratio;

    // initialise indices
    const auto [xStart, xEnd] = SciFiHits.getZoneIndices( xZoneNumber );

    // -- Use search to find the lower bound of the range of x values
    // range of xHits of current window ([xMin, xMax]) on the current zone
    const unsigned start_window = SciFiHits.get_lower_bound_fast<0>( xStart, xEnd, xMin );
    const unsigned end_window   = SciFiHits.get_lower_bound_fast<0>( start_window, xEnd, xMax );

    if ( UNLIKELY( start_window == end_window ) ) continue; // nothing to do here

    // MatchStereoHits
    const auto  x0           = SciFiHits.x( start_window );
    const auto  uvZoneNumber = PrFTZoneHandler::getUVZone<SIDE>( iZone );
    const auto  zUVZone      = zonecache.zoneZPos[uvZoneNumber];
    const auto  invDz        = 1.f / ( zZone - zMag ); // needed in loop
    const auto  zRatio       = ( zUVZone - zMag ) * invDz;
    const auto& zoneUv       = zonecache.handler.zone( uvZoneNumber );
    const auto  uvDxDy       = zoneUv.dxDy();
    const auto  dx           = seed.y( zUVZone ) * uvDxDy; // x correction from rotation by stereo angle
    const auto  xCentral     = xInZone + dx;
    const auto  xPredUVProto = seed.x( zUVZone ) - xInZone * zRatio - dx;
    const auto  xPredUvStart = xPredUVProto + x0 * zRatio; // predicted hit in UV-layer
    const auto  yInZone      = seed.y( zZone );
    const auto  maxDxProto   = m_tolYCollectX + std::abs( yInZone ) * m_tolYSlopeCollectX;
    const auto  maxDxStart   = maxDxProto + std::abs( x0 - xCentral ) * m_tolYSlopeCollectX;
    const auto  xMinUVStart  = xPredUvStart - maxDxStart;

    // search first hit in UV search window
    const auto [uvStart, uvEnd] = SciFiHits.getZoneIndices( uvZoneNumber );
    unsigned startStereo        = SciFiHits.get_lower_bound_fast<0>( uvStart, uvEnd, xMinUVStart );
    // check if triangle search should be done
    const bool triangleSearch = std::abs( yInZone ) < m_tolYTriangleSearch;

    if ( UNLIKELY( startStereo == uvEnd && !triangleSearch ) ) continue;

    // in case of triangle search, we also have to search on the other side of the detector (OS)!
    unsigned startStereoOS{0};
    if ( triangleSearch ) {
      auto [uvStartOS, uvEndOS] = SciFiHits.getZoneIndices( PrFTZoneHandler::getTriangleZone<SIDE>( iZone ) );
      startStereoOS             = SciFiHits.get_lower_bound_fast<0>( uvStartOS, uvEndOS, xMinUVStart );
      if ( UNLIKELY( startStereoOS == uvEndOS && startStereo == uvEnd ) ) continue; // nothing to do
    }

    // the followiing code takes most of the time
    for ( unsigned i = start_window; i < end_window; i += simd::size ) {
      // SciFiHits vectors are properly padded
      const auto xVc     = simd::float_v( &SciFiHits._x[i] );
      const auto dSlp    = ( xVc - xInZone ) * invDz;
      const auto yCorrec = yc.calcYCorrection( dSlp, iZone );
      const auto xPredUv = xPredUVProto + xVc * zRatio + yCorrec * uvDxDy; // predicted hit in UV-layer
      const auto maxDx   = maxDxProto + abs( xVc - xCentral ) * m_tolYSlopeCollectX.value();
      const auto xMinUV  = xPredUv - maxDx;
      const auto xMaxUV  = xPredUv + maxDx;
      // mask flagging end of loop and current indices
      const auto loopmask = simd::loop_mask( i, end_window );
      const auto indices  = simd::indices( i );
      // vectorised stereo hit matching
      const auto match         = matchStereoHit( startStereo, xMinUV, xMaxUV, SciFiHits, loopmask );
      const auto triangleMatch = triangleSearch
                                     ? matchStereoHitWithTriangle<SIDE>( startStereoOS, xMinUV, xMaxUV, yInZone,
                                                                         m_yTolUVSearch, SciFiHits, loopmask )
                                     : simd::mask_false();
      const auto matchMask = ( match || triangleMatch ) && loopmask;
      // add to size only where entry of mask is true
      const auto addSize = simd::popcount( matchMask );
      // helper mask to store permutations
      const auto countMask   = simd::loop_mask( 0, addSize );
      const auto currentSize = allXHits.size();
      assert( currentSize + simd::size < SciFiHits::align_size( numberallXHitsAfterCuts ) &&
              "Reserved size is not large enough." );

      if ( UNLIKELY( allXHits.size() + addSize > numberallXHitsAfterCuts ) ) {
        error() << "Reach the size limit of allXHits container" << endmsg;
        break;
      }

      allXHits.compressstore_fulldex( currentSize, matchMask, indices );
      allXHits.compressstore_coord( currentSize, matchMask, xVc );
      allXHits.compressstore_permutation( currentSize, countMask, simd::indices() + currentSize );
      allXHits.updateSizeBy( addSize );
    }

    const unsigned start = iZoneEnd.back();
    const unsigned end   = allXHits.size();
    // keep track of zone indices
    iZoneEnd.push_back( end );
    // protect following projection function from using random toxic values after end
    allXHits.store_coord( end, simd::float_v( 1.e9f ) );
    if ( LIKELY( start != end ) ) { xAtRef_SamePlaneHits( track, start, zZone, allXHits ); }
  }

  const auto endSize = allXHits.size();
  auto       cmp     = [&allXHits]( const auto a, const auto b ) { return allXHits.coords[a] < allXHits.coords[b]; };

  if ( LIKELY( iZoneEnd.size() == NLayerBoundaries ) ) {
    mergeSortedRanges( allXHits.permutations, LHCb::make_span( iZoneEnd ).first<NLayerBoundaries>(), cmp );

  } else {
    // default sort
    // explicit range is needed because underlying std::vector does not keep track of 'actual' size with avx store
    auto& perm = allXHits.permutations;
    std::sort( perm.begin(), std::next( perm.begin(), endSize ), cmp );
  }
  // tmp object storing the sorted coords
  decltype( allXHits.coords ) tmp;
  tmp.reserve( endSize );
  for ( unsigned idx{0}; idx < endSize; ++idx ) {
    // TODO: can also use gather here
    tmp.push_back( allXHits.coords[allXHits.permutations[idx]] );
  }
  // move sorted coords to original container
  std::move( tmp.begin(), tmp.end(), allXHits.coords.begin() );

  // -- > Debug --------
  if ( msgLevel( MSG::DEBUG ) ) {
    const float xWindow = pars.maxXWindow + std::abs( seed.x( zReference ) ) * pars.maxXWindowSlope;
    debug() << "**** Processing Velo track  zone "
            << " Selected " << allXHits.size() << " hits, window size " << xWindow << endmsg;
  }
  // -- < Debug --------

  // pad one vector register size at the end to avoid any errors
  const auto lastIndex = simd::int_v( endSize - 1 );
  assert( endSize + simd::size < SciFiHits::align_size( numberallXHitsAfterCuts ) &&
          "Reserved size is not large enough for padding." );
  if ( endSize + simd::size < SciFiHits::align_size( numberallXHitsAfterCuts ) ) {
    allXHits.store_fulldex( endSize, lastIndex );
    allXHits.store_coord( endSize, simd::float_v( std::numeric_limits<float>::max() ) );
    allXHits.store_permutation( endSize, lastIndex );
  }
}

//=========================================================================
//  Select the zones in the allXHits array where we can have a track
//=========================================================================
template <typename T>
template <PrHitZone::Side SIDE, typename Container, typename OtherContainer>
void PrForwardTracking<T>::selectXCandidates( OtherContainer& trackCandidates, const PrForwardTrack<>& seed,
                                              Container& allXHits, const PrSciFiHits& SciFiHits,
                                              const PrParameters& pars, ZoneCache const& zonecache ) const {
  if ( UNLIKELY( allXHits.size() < pars.minXHits ) ) return;
  const float xStraight = seed.x( zReference );

  // Parameters for X-hit only fit, thus do not require stereo hits
  PrParameters xFitPars{pars};
  xFitPars.minStereoHits = 0;

  boost::container::small_vector<unsigned, PrFTInfo::NFTZones> coordToFit; // 24 Zones are a good number of coordsToFit
                                                                           // large enough ...
  PrLineFitter<decltype( coordToFit )> lineFitter;

  // otherHits can occur on any of the 12 layers and there are rarely more than 6 of these
  std::array<boost::container::small_vector<ModSciFiHits::ModPrSciFiHit, 6>, PrFTInfo::NFTLayers> otherHits;

  const unsigned idxEnd = allXHits.size();
  unsigned       idx1{0};

  while ( true ) {
    // find next unused Hits
    while ( idx1 + pars.minXHits - 1 < idxEnd && !allXHits.isValid( idx1 ) ) { ++idx1; }
    auto idx2 = idx1 + pars.minXHits; // it2 pointing at last+1 element, like list.end()
    while ( idx2 <= idxEnd && !allXHits.isValid( idx2 - 1 ) ) ++idx2;
    if ( idx2 > idxEnd ) break; // Minimum is after the end! this is the exit point

    // TODO: better xWindow calculation?? how to tune this???
    const float xWindow =
        pars.maxXWindow +
        ( std::abs( allXHits.coords[idx1] ) + std::abs( allXHits.coords[idx1] - xStraight ) ) * pars.maxXWindowSlope;

    if ( ( allXHits.coords[idx2 - 1] - allXHits.coords[idx1] ) > xWindow ) {
      ++idx1;
      continue;
    }
    // Cluster candidate found, now count planes
    PrPlaneCounter pc;
    for ( unsigned idx{idx1}; idx < idx2; ++idx ) {
      pc.addHit( allXHits.getMainIndex( idx ), allXHits.isValid( idx ), SciFiHits );
    }
    // Improve cluster (at the moment only add hits to the right)
    for ( auto idxLast = idx2 - 1; idx2 < idxEnd; ) {
      // if last/first hit isUsed, skip this
      if ( !allXHits.isValid( idx2 ) ) {
        ++idx2;
        continue;
      }
      // now  the first and last+1 hit exist and are not used!

      // Add next hit,
      // if there is only a small gap between the hits
      //    or inside window and plane is still empty
      if ( const auto mIdx2 = allXHits.getMainIndex( idx2 );
           ( allXHits.coords[idx2] < allXHits.coords[idxLast] + pars.maxXGap ) ||
           ( ( allXHits.coords[idx2] - allXHits.coords[idx1] < xWindow ) &&
             ( pc.nbInPlane( mIdx2, SciFiHits ) == 0 ) ) ) {
        pc.addHit( mIdx2, SciFiHits );
        idxLast = idx2;
        ++idx2;
        continue;
      }
      // Found nothing to improve
      break;
    }

    // if not enough different planes, start again from the very beginning with next right hit
    if ( pc.nbDifferent() < pars.minXHits ) {
      ++idx1;
      continue;
    }

    //====================================================================
    //  Now we have a (rather) clean candidate, do best hit selection
    //  Two possibilities:
    //  1) If there are enough planes with only one hit, do a straight
    //      line fit through these and select good matching others
    //  2) Do some magic
    //====================================================================
    coordToFit.clear();
    float              xAtRef   = 0.;
    const unsigned int nbSingle = pc.nbSingle();

    if ( nbSingle >= m_minSingleHits && nbSingle != pc.nbDifferent() ) {
      // 1) we have enough single planes (thus two) to make a straight line fit
      lineFitter.reset( zReference, &coordToFit );
      for ( auto& others : otherHits ) others.clear();

      // seperate single and double hits
      for ( auto idx{idx1}; idx < idx2; ++idx ) {
        if ( !allXHits.isValid( idx ) ) continue;
        const auto mIdx = allXHits.getMainIndex( idx );
        if ( pc.nbInPlane( mIdx, SciFiHits ) == 1 ) {
          lineFitter.addHit( mIdx, allXHits.coords[idx], SciFiHits );
        } else {
          const auto pcode = allXHits.planeCode( idx, SciFiHits );
          otherHits[pcode].emplace_back( mIdx, allXHits.coords[idx], pcode );
        }
      }

      lineFitter.solve();

      // select best other hits (only best other hit is enough!)
      for ( const auto& hits : otherHits ) {
        auto [chi2, best] = best_element(
            hits, [&]( const auto& hit ) { return lineFitter.chi2( hit, SciFiHits ); }, std::less<>{}, 1e9f );
        if ( !best ) continue;
        lineFitter.addHit( *best, best->coord(), SciFiHits );
        lineFitter.solve();
      }
      xAtRef = lineFitter.coordAtRef();

    } else {
      // 2) Try to find a small distance containing at least 5(4) different planes
      //    Most of the time do nothing

      const unsigned int nPlanes        = std::min( pc.nbDifferent(), uint{5} );
      auto               idxWindowStart = idx1;
      auto               idxWindowEnd   = idx1 + nPlanes; // pointing at last+1
      // Hit is used, go to next unused one
      while ( idxWindowEnd <= idx2 && !allXHits.isValid( idxWindowEnd - 1 ) ) ++idxWindowEnd;
      if ( idxWindowEnd > idx2 ) continue; // start from very beginning

      float minInterval = 1.e9f;
      auto  best        = idxWindowStart;
      auto  bestEnd     = idxWindowEnd;

      PrPlaneCounter lpc;
      for ( unsigned idx{idxWindowStart}; idx < idxWindowEnd; ++idx ) {
        lpc.addHit( allXHits.getMainIndex( idx ), allXHits.isValid( idx ), SciFiHits );
      }

      while ( idxWindowEnd <= idx2 ) {
        if ( lpc.nbDifferent() >= nPlanes ) {
          // have nPlanes, check x distance
          if ( const float dist = allXHits.coords[idxWindowEnd - 1] - allXHits.coords[idxWindowStart];
               dist < minInterval ) {
            minInterval = dist;
            best        = idxWindowStart;
            bestEnd     = idxWindowEnd;
          }
        } else {
          // too few planes, add one hit
          ++idxWindowEnd;
          while ( idxWindowEnd <= idx2 && !allXHits.isValid( idxWindowEnd - 1 ) ) ++idxWindowEnd;
          if ( idxWindowEnd > idx2 ) break;
          lpc.addHit( allXHits.getMainIndex( idxWindowEnd - 1 ), SciFiHits );
          continue;
        }
        // move on to the right
        lpc.removeHit( allXHits.getMainIndex( idxWindowStart ), SciFiHits );
        ++idxWindowStart;
        while ( idxWindowStart < idxWindowEnd && !allXHits.isValid( idxWindowStart ) ) ++idxWindowStart;
        // last hit guaranteed to be not used. Therefore there is always at least one hit to go to. No additional if
        // required.
      }

      // TODO: tune minInterval cut value
      if ( minInterval < 1.f ) {
        idx1 = best;
        idx2 = bestEnd;
      }

      // Fill coords and compute average x at reference
      for ( auto idx{idx1}; idx < idx2; ++idx ) {
        if ( allXHits.isValid( idx ) ) {
          coordToFit.emplace_back( allXHits.getMainIndex( idx ) );
          xAtRef += allXHits.coords[idx];
        }
      }
      xAtRef /= static_cast<float>( coordToFit.size() );
    }

    if ( msgLevel( MSG::DEBUG ) ) {
      debug() << "=== Coordinates to fit: " << coordToFit.size() << endmsg;
      for ( const auto& hit : coordToFit ) printHit( hit, &SciFiHits );
      debug() << endmsg;
    }

    //=== We have a candidate :)

    // overwriting is faster than resetting, attention: values which are not overwritten do not make sense!!
    pc.clear();
    // too difficult to keep track of add and delete, just do it again..
    pc.set( coordToFit, SciFiHits );
    // only unused(!) hits in coordToFit now

    bool             ok = pc.nbDifferent() > 3;
    PrForwardTrack<> track( seed );
    track.setSciFiHits( SciFiHits ); // TODO: this is a bit clumsy
    if ( ok ) {
      track.replaceHits( std::move( coordToFit ) );
      // -- > ------ DEBUG ------
      if ( msgLevel( MSG::DEBUG ) ) {
        debug() << "=== Replaced hits in track with hits to fit: " << coordToFit.size() << endmsg;
        printTrack( track );
        debug() << endmsg;
      }
      // -- < -------------------
      setTrackParameters( track, xAtRef );
      fastLinearFit( track, pars, pc );
      if ( pc.nbDifferent() < PrFTInfo::NFTXLayers ) {
        addHitsOnEmptyXLayers<SIDE>( track, false, SciFiHits, pars, pc, zonecache );
      }
      ok = pc.nbDifferent() > 3;
    }
    //== Fit and remove hits...
    if ( ok ) ok = fitXProjection( track, xFitPars, pc );
    if ( ok ) ok = track.chi2PerDoF() < m_maxChi2PerDoF;
    if ( ok && pc.nbDifferent() < PrFTInfo::NFTXLayers ) {
      ok = addHitsOnEmptyXLayers<SIDE>( track, true, SciFiHits, xFitPars, pc, zonecache );
    }
    if ( ok ) {
      // set ModPrHits used , challenge: we don't have the link any more!
      // Do we really need isUsed in Forward? We can otherwise speed up the search quite a lot!
      // --> we need it for the second loop
      // setHitsUsed( it1, itEnd, track, xFitPars );
      setHitsUsed( idx1, idxEnd, track, xFitPars, allXHits );
      trackCandidates.emplace_back( std::move( track ) );

      // -- > Debug --------
      if ( msgLevel( MSG::DEBUG ) ) {
        debug() << "=== Storing track candidate " << trackCandidates.size() << endmsg;
        printTrack( trackCandidates.back() );
        debug() << endmsg;
      }
      // -- < Debug --------
    }
    // next one
    ++idx1;
  }
}
//=========================================================================
//  Fit a linear form, remove the external worst as long as chi2 is big...
//=========================================================================
template <typename T>
void PrForwardTracking<T>::fastLinearFit( PrForwardTrack<>& track, const PrParameters& pars,
                                          PrPlaneCounter& pc ) const {
  bool       fit       = true;
  const auto SciFiHits = track.getSciFiHits();
  while ( fit ) {
    //== Fit a line
    float s0  = 0.;
    float sz  = 0.;
    float sz2 = 0.;
    float sd  = 0.;
    float sdz = 0.;

    for ( const auto& ihit : track.ihits() ) {
      const float zHit = SciFiHits->z( ihit );
      const float d    = SciFiHits->distanceXHit( ihit, track.x( zHit ) );
      const float w    = SciFiHits->w( ihit );
      const float z    = zHit - zReference;
      s0 += w;
      sz += w * z;
      sz2 += w * z * z;
      sd += w * d;
      sdz += w * d * z;
    }
    float den = ( sz * sz - s0 * sz2 );
    if ( !( std::abs( den ) > 1e-5f ) ) return;
    const float da = ( sdz * sz - sd * sz2 ) / den;
    const float db = ( sd * sz - s0 * sdz ) / den;
    track.addXParams<2>( {da, db} );
    fit = false;

    // -- > Debug --------
    if ( msgLevel( MSG::DEBUG ) ) {
      debug() << "Linear fit, current status : " << endmsg;
      printTrack( track );
    }
    // -- < Debug --------

    if ( track.ihits().size() < pars.minXHits ) return;

    auto       worst       = track.ihits().end();
    float      maxChi2     = 0.f;
    const bool notMultiple = pc.nbDifferent() == track.ihits().size();
    for ( auto itH = std::begin( track.ihits() ); std::end( track.ihits() ) != itH; ++itH ) {
      float chi2 = track.chi2XHit( *itH );
      if ( chi2 > maxChi2 && ( notMultiple || pc.nbInPlane( *itH, *SciFiHits ) > 1 ) ) {
        maxChi2 = chi2;
        worst   = itH;
      }
    }

    //== Remove grossly out hit, or worst in multiple layers

    if ( maxChi2 > m_maxChi2LinearFit || ( !notMultiple && maxChi2 > 4.f ) ) {

      /// -- > Debug --------
      if ( msgLevel( MSG::DEBUG ) ) {
        debug() << "Remove hit ";
        printHit( *worst, SciFiHits );
      }
      // -- < Debug --------

      pc.removeHit( *worst, *SciFiHits );
      // removing hit from track list, no need to keep track of isUsed
      std::iter_swap( worst, std::prev( track.ihits().end() ) ); // faster than just erase, order does not matter
      track.ihits().pop_back();
      fit = true;
    }
  }
}
//=========================================================================
//  Fit the X projection of a track, return OK if fit sucecssfull
//=========================================================================
template <typename T>
bool PrForwardTracking<T>::fitXProjection( PrForwardTrack<>& track, const PrParameters& pars,
                                           PrPlaneCounter& pc ) const {

  // -- > Debug --------
  if ( msgLevel( MSG::DEBUG ) ) {
    debug() << "  -- Entering fitXProjection with:" << endmsg;
    printTrack( track );
  }
  // -- < Debug --------

  if ( pc.nbDifferent() < pars.minXHits ) {

    // -- > Debug --------
    if ( msgLevel( MSG::DEBUG ) )
      debug() << "  == Not enough layers ( " << pc.nbDifferent() << " ) with hits" << endmsg;
    // -- < Debug --------

    return false;
  }

  bool       doFit     = true;
  const auto SciFiHits = track.getSciFiHits();
  while ( doFit ) {
    //== Fit a cubic
    float s0   = 0.f;
    float sz   = 0.f;
    float sz2  = 0.f;
    float sz3  = 0.f;
    float sz4  = 0.f;
    float sd   = 0.f;
    float sdz  = 0.f;
    float sdz2 = 0.f;

    for ( const auto& ihit : track.ihits() ) {
      float d = track.distance( ihit );
      float w = SciFiHits->w( ihit );
      float z = .001f * ( SciFiHits->z( ihit ) - zReference );
      s0 += w;
      sz += w * z;
      sz2 += w * z * z;
      sz3 += w * z * z * z;
      sz4 += w * z * z * z * z;
      sd += w * d;
      sdz += w * d * z;
      sdz2 += w * d * z * z;
    }
    const float b1  = sz * sz - s0 * sz2;
    const float c1  = sz2 * sz - s0 * sz3;
    const float d1  = sd * sz - s0 * sdz;
    const float b2  = sz2 * sz2 - sz * sz3;
    const float c2  = sz3 * sz2 - sz * sz4;
    const float d2  = sdz * sz2 - sz * sdz2;
    const float den = ( b1 * c2 - b2 * c1 );
    if ( !( std::abs( den ) > 1e-5f ) ) return false;
    const float db = ( d1 * c2 - d2 * c1 ) / den;
    const float dc = ( d2 * b1 - d1 * b2 ) / den;
    const float da = ( sd - db * sz - dc * sz2 ) / s0;
    track.addXParams<3>( {da, db * 1.e-3f, dc * 1.e-6f} );

    float maxChi2 = 0.f;
    float totChi2 = 0.f;
    // fitted 3 parameters
    int        nDoF        = -3;
    const bool notMultiple = pc.nbDifferent() == track.ihits().size();

    const auto itEnd = std::end( track.ihits() );
    auto       worst = itEnd;
    for ( auto itH = std::begin( track.ihits() ); itEnd != itH; ++itH ) {
      float chi2 = track.chi2( *itH );
      totChi2 += chi2;
      ++nDoF;
      if ( chi2 > maxChi2 && ( notMultiple || pc.nbInPlane( *itH, *SciFiHits ) > 1 ) ) {
        maxChi2 = chi2;
        worst   = itH;
      }
    }
    if ( nDoF < 1 ) return false;
    track.setChi2nDof( {totChi2, static_cast<float>( nDoF )} );

    // -- > Debug --------
    if ( msgLevel( MSG::DEBUG ) ) {
      debug() << "  -- In fitXProjection, maxChi2 = " << maxChi2 << " totCHi2/nDof " << totChi2 / nDoF << endmsg;
      printTrack( track );
    }
    // -- < Debug --------

    if ( worst == itEnd ) return true;
    doFit = false;
    if ( totChi2 > m_maxChi2PerDoF * nDoF || maxChi2 > m_maxChi2XProjection ) {
      pc.removeHit( *worst, *SciFiHits );                        // only valid "unused" hits in track.hits()
      std::iter_swap( worst, std::prev( track.ihits().end() ) ); // faster than just erase, order does not matter
      track.ihits().pop_back();

      if ( pc.nbDifferent() < pars.minXHits + pars.minStereoHits ) {

        // -- > Debug --------
        if ( msgLevel( MSG::DEBUG ) ) {
          debug() << "  == Not enough layers with hits" << endmsg;
          printTrack( track );
        }
        // -- < Debug --------

        return false;
      }
      doFit = true;
    }
  }

  // -- > Debug --------
  if ( msgLevel( MSG::DEBUG ) ) {
    debug() << "  -- End fitXProjection -- " << endmsg;
    printTrack( track );
  }
  // -- < Debug --------

  return true;
}

//=========================================================================
//  Fit the X projection of a track, return OK if fit sucecssfull
//=========================================================================
template <typename T>
template <typename Container>
bool PrForwardTracking<T>::fitYProjection( PrForwardTrack<>& track, Container& stereoHits, const PrParameters& pars,
                                           PrPlaneCounter& pc ) const {

  // -- > Debug --------
  if ( msgLevel( MSG::DEBUG ) ) {
    debug() << "  -- Entering fitYProjection with:" << endmsg;
    printTrack( track );
  }
  // -- < Debug --------

  if ( pc.nbDifferent() < pars.minStereoHits ) {
    // -- > Debug --------
    if ( msgLevel( MSG::DEBUG ) )
      debug() << "  == Not enough layers ( " << pc.nbDifferent() << " ) with hits" << endmsg;
    // -- < Debug --------
    return false;
  }

  float maxChi2  = 1.e9f;
  bool  parabola = false; // first linear than parabola

  //== Fit a line
  const float tolYMag =
      m_tolYMag + m_tolYMagSlope * std::abs( track.xStraight( zReference ) - track.seed().x( zReference ) );
  const float wMag = 1.f / ( tolYMag * tolYMag );

  const auto SciFiHits = track.getSciFiHits();
  bool       doFit     = true;
  while ( doFit ) {

    // Use position in magnet as constrain in fit
    // although bevause wMag is quite small only little influence...
    float       zMag  = zMagnet( track );
    const float dyMag = track.yStraight( zMag ) - track.seed().y( zMag );
    zMag -= zReference;
    float s0  = wMag;
    float sz  = wMag * zMag;
    float sz2 = wMag * zMag * zMag;
    float sd  = wMag * dyMag;
    float sdz = wMag * dyMag * zMag;

    auto itEnd = stereoHits.end();

    if ( parabola ) {
      float sz2m = 0.;
      float sz3  = 0.;
      float sz4  = 0.;
      float sdz2 = 0.;
      for ( const auto& ihit : stereoHits ) {
        const float d = -track.distance( ihit ) /
                        SciFiHits->dxDy( ihit ); // TODO: depending on alignment this could be calculated beforehand
        const float w = SciFiHits->w( ihit );
        const float z = SciFiHits->z( ihit ) - zReference;
        s0 += w;
        sz += w * z;
        sz2m += w * z * z;
        sz2 += w * z * z;
        sz3 += w * z * z * z;
        sz4 += w * z * z * z * z;
        sd += w * d;
        sdz += w * d * z;
        sdz2 += w * d * z * z;
      }
      const float b1  = sz * sz - s0 * sz2;
      const float c1  = sz2m * sz - s0 * sz3;
      const float d1  = sd * sz - s0 * sdz;
      const float b2  = sz2 * sz2m - sz * sz3;
      const float c2  = sz3 * sz2m - sz * sz4;
      const float d2  = sdz * sz2m - sz * sdz2;
      const float den = ( b1 * c2 - b2 * c1 );
      if ( !( std::abs( den ) > 1e-5f ) ) return false;
      const float db = ( d1 * c2 - d2 * c1 ) / den;
      const float dc = ( d2 * b1 - d1 * b2 ) / den;
      const float da = ( sd - db * sz - dc * sz2 ) / s0;
      track.addYParams<3>( {da, db, dc} );

      // -- > Debug --------
      if ( msgLevel( MSG::DEBUG ) ) {
        debug() << "fitYProjection Parabolic Fit da: " << da << " db: " << db << " dc: " << dc << endmsg;
      }
      // -- < Debug --------
    } else {
      for ( const auto& ihit : stereoHits ) {
        const float d = -track.distance( ihit ) / SciFiHits->dxDy( ihit ); // TODO: same as above
        const float w = SciFiHits->w( ihit );
        const float z = SciFiHits->z( ihit ) - zReference;
        s0 += w;
        sz += w * z;
        sz2 += w * z * z;
        sd += w * d;
        sdz += w * d * z;
      }
      const float den = ( s0 * sz2 - sz * sz );
      if ( !( std::abs( den ) > 1e-5f ) ) return false;
      const float da = ( sd * sz2 - sdz * sz ) / den;
      const float db = ( sdz * s0 - sd * sz ) / den;
      track.addYParams<2>( {da, db} );

      // -- > Debug --------
      if ( msgLevel( MSG::DEBUG ) ) { debug() << "fitYProjection Linear Fit da: " << da << " db: " << db << endmsg; }
      // -- < Debug --------
    } // fit end, now doing outlier removal

    auto worst = std::end( stereoHits );
    maxChi2    = 0.;
    for ( auto itH = std::begin( stereoHits ); itEnd != itH; ++itH ) {
      const float chi2 = track.chi2( *itH );
      if ( chi2 > maxChi2 ) {
        maxChi2 = chi2;
        worst   = itH;
      }
    }

    if ( maxChi2 < m_maxChi2StereoLinear && !parabola ) {
      parabola = true;
      maxChi2  = 1.e9f;
      continue;
    }

    if ( maxChi2 > m_maxChi2Stereo ) {
      pc.removeHit( *worst, *SciFiHits ); // stereo hits never 'used'
      if ( pc.nbDifferent() < pars.minStereoHits ) {

        // -- > Debug --------
        if ( msgLevel( MSG::DEBUG ) )
          debug() << "-- not enough different planes after removing worst: " << pc.nbDifferent() << " for "
                  << pars.minStereoHits << " --" << endmsg;
        // -- < Debug --------

        return false;
      }
      std::iter_swap( worst, std::prev( stereoHits.end() ) ); // faster than just erase, order does not matter
      stereoHits.pop_back();
      continue;
    }

    break;
  }

  return true;
}
//=========================================================================
//  Add hits on empty X layers, and refit if something was added
//=========================================================================
template <typename T>
template <PrHitZone::Side SIDE>
bool PrForwardTracking<T>::addHitsOnEmptyXLayers( PrForwardTrack<>& track, bool fullFit, const PrSciFiHits& SciFiHits,
                                                  const PrParameters& pars, PrPlaneCounter& pc,
                                                  ZoneCache const& zonecache ) const {

  // is there an empty plane? otherwise skip here!
  if ( pc.nbDifferent() > 11 ) return true;

  bool        added     = false;
  const auto& xPars     = track.getXParams();
  const float x1        = xPars.get( 0 );
  const float xStraight = track.seed().x( zReference );
  const float xWindow   = pars.maxXWindow + ( std::abs( x1 ) + std::abs( x1 - xStraight ) ) * pars.maxXWindowSlope;

  for ( unsigned int iZone = 0; iZone < PrFTInfo::NFTXLayers; ++iZone ) {
    const unsigned zoneNumber = PrFTZoneHandler::getXZone<SIDE>( iZone );
    if ( pc.nbInPlane( zoneNumber / 2 ) != 0 ) continue;

    auto [xStart, xEnd] = SciFiHits.getZoneIndices( zoneNumber );

    const float zZone = zonecache.zoneZPos[zoneNumber];
    const float xPred = track.x( zZone );

    // -- Use a search to find the lower bound of the range of x values
    const unsigned startwin = SciFiHits.get_lower_bound_fast<0>( xStart, xEnd, xPred - xWindow );
    unsigned       endwin   = startwin;
    auto           x        = SciFiHits.x( startwin );
    auto           maxX     = xPred + xWindow;
    auto           d        = SciFiHits.distanceXHit( startwin, xPred );
    auto           bestChi2{1.e9f};
    unsigned       best{0};
    while ( x <= maxX ) {
      d         = SciFiHits.distanceXHit( endwin, xPred );
      auto Chi2 = d * d * SciFiHits.w( endwin );
      if ( Chi2 < bestChi2 ) {
        bestChi2 = Chi2;
        best     = endwin;
      }
      ++endwin;
      x = SciFiHits.x( endwin );
    }
    if ( best ) {

      // -- > Debug --------
      if ( msgLevel( MSG::DEBUG ) ) {
        debug() << format( "AddHitOnEmptyXLayer:    chi2%8.2f", bestChi2 );
        printHit( best, &SciFiHits, " " );
      }
      // -- < Debug --------

      track.addHit( best );
      pc.addHit( best, SciFiHits );
      added = true;
    }
  }
  if ( !added ) return true;
  if ( fullFit ) { return fitXProjection( track, pars, pc ); }
  fastLinearFit( track, pars, pc );
  return true;
}
//=========================================================================
//  Add hits on empty stereo layers, and refit if something was added
//=========================================================================
template <typename T>
template <typename Container>
bool PrForwardTracking<T>::addHitsOnEmptyStereoLayers( PrForwardTrack<>& track, Container& stereoHits,
                                                       const PrSciFiHits& SciFiHits, const PrParameters& pars,
                                                       PrPlaneCounter& pc, ZoneCache const& zonecache ) const {
  // at this point pc is counting only stereo HITS!
  if ( pc.nbDifferent() > 5 ) return true;

  bool added = false;
  for ( const auto zoneNumber : PrFTInfo::stereoZones ) {
    if ( pc.nbInPlane( zoneNumber / 2 ) != 0 )
      continue; // there is already one hit, planeCode only goes until 12 -> divide by two

    float       zZone = zonecache.zoneZPos[zoneNumber];
    float       yZone = track.y( zZone );
    const auto& zone  = zonecache.handler.zone( zoneNumber );
    zZone             = zone.z( yZone ); // Correct for dzDy
    yZone             = track.y( zZone );
    const float xPred = track.x( zZone );

    const bool triangleSearch = std::abs( yZone ) < m_tolYTriangleSearch;
    if ( !triangleSearch && ( 2.f * float( ( ( zoneNumber % 2 ) == 0 ) ) - 1.f ) * yZone > 0.f ) continue;

    // only version without triangle search!
    const float dxTol = m_tolY + m_tolYSlope * ( std::abs( xPred - track.seed().x( zZone ) ) + std::abs( yZone ) );
    // -- Use a binary search to find the lower bound of the range of x values
    // -- This takes the y value into account

    const auto [uvStart, uvEnd] = SciFiHits.getZoneIndices( zoneNumber );
    auto minuvStart = SciFiHits.get_lower_bound_fast<0>( uvStart, uvEnd, -dxTol - yZone * zone.dxDy() + xPred );

    auto triangleOK = [&, yMax = yZone - m_yTolUVSearch, yMin = yZone + m_yTolUVSearch]( const unsigned& index ) {
      return ( yMax < SciFiHits.yMax( index ) ) && ( yMin > SciFiHits.yMin( index ) );
    };

    auto chi2 = [&]( const unsigned& index ) {
      const float d = SciFiHits.distance( index, xPred, yZone );
      return d * d * SciFiHits.w( index );
    };

    auto [bestChi2, best] = [&, xMax = xPred + dxTol]() {
      auto bestChi2{m_maxChi2Stereo.value()};
      auto best{0};
      auto i{minuvStart};
      auto xAtY = SciFiHits.x( minuvStart, yZone );
      while ( xAtY <= xMax ) {
        const float _chi2 = chi2( i );
        if ( ( !triangleSearch || triangleOK( i ) ) && _chi2 < bestChi2 ) {
          bestChi2 = _chi2;
          best     = i;
        }
        ++i;
        xAtY = SciFiHits.x( i, yZone );
      }
      return std::make_pair( bestChi2, best );
    }();

    if ( best ) {
      // -- > Debug --------
      if ( msgLevel( MSG::DEBUG ) ) {
        debug() << format( "AddHitOnEmptyStereoLayer:    chi2%8.2f", bestChi2 );
        printHit( best, &SciFiHits, " " );
        debug() << "zZone: " << zZone << " xpred: " << xPred << " dxTol " << dxTol << endmsg;
      }
      // -- < Debug --------
      stereoHits.push_back( best );
      pc.addHit( best, SciFiHits );
      added = true;
    }
  }
  if ( !added ) return true;
  return fitYProjection( track, stereoHits, pars, pc );
}

//=========================================================================
//  Collect all hits in the stereo planes compatible with the track
//=========================================================================
template <typename T>
template <typename Container>
void PrForwardTracking<T>::collectStereoHits( PrForwardTrack<>& track, Container& stereoHits,
                                              const PrSciFiHits& SciFiHits, ZoneCache const& zonecache ) const {

  // -- > Debug --------
  if ( msgLevel( MSG::DEBUG ) ) debug() << "== Collect stereo hits. wanted ones: " << endmsg;
  // -- < Debug --------

  for ( const auto& zoneNumber : PrFTInfo::stereoZones ) {
    float       zZone = zonecache.zoneZPos[zoneNumber];
    const float yZone = track.y( zZone );
    const auto& zone  = zonecache.handler.zone( zoneNumber ); // TODO: this can also be handled within the zonecache
    zZone             = zone.z( yZone );                      // Correct for dzDy
    const float xPred = track.x( zZone );

    const bool triangleSearch = std::abs( yZone ) < m_tolYTriangleSearch;
    if ( !triangleSearch && ( 2.f * float( ( zoneNumber % 2 ) == 0 ) - 1.f ) * yZone > 0.f ) continue;

    const float dxDySign = zone.dxDy() < 0 ? -1.f : 1.f;
    const float dxTol    = m_tolY + m_tolYSlope * ( std::abs( xPred - track.seed().x( zZone ) ) + std::abs( yZone ) );

    // -- Use a binary search to find the lower bound of the range of x values
    // -- This takes the y value into account
    const auto [uvStart, uvEnd] = SciFiHits.getZoneIndices( zoneNumber );
    auto minuvStart = SciFiHits.get_lower_bound_fast<0>( uvStart, uvEnd, -dxTol - yZone * zone.dxDy() + xPred );

    auto triangleOK = [&, yMax = yZone - m_yTolUVSearch, yMin = yZone + m_yTolUVSearch]( const unsigned& index ) {
      return ( yMax < SciFiHits.yMax( index ) ) && ( yMin > SciFiHits.yMin( index ) );
    };

    [&, xMax = xPred + dxTol]() {
      auto xAtY = SciFiHits.x( minuvStart, yZone );
      while ( xAtY <= xMax ) {
        if ( !triangleSearch || triangleOK( minuvStart ) ) {
          const float dx = xAtY - xPred;
          stereoHits.emplace_back( minuvStart, dx * dxDySign, SciFiHits.planeCode( minuvStart ) );
        }
        ++minuvStart;
        xAtY = SciFiHits.x( minuvStart, yZone );
      }
    }();
  }
}
//=========================================================================
//  Fit the stereo hits
//=========================================================================
template <typename T>
template <typename Container>
bool PrForwardTracking<T>::selectStereoHits( PrForwardTrack<>& track, const PrSciFiHits& SciFiHits,
                                             const Container& allStereoHits, const PrParameters& pars,
                                             ZoneCache const& zonecache ) const {
  // why do we rely on xRef? --> coord is NOT xRef for stereo HITS!

  boost::container::small_vector<unsigned, PrFTInfo::NFTZones> bestStereoHits;
  trackPars<3>                                                 originalYParams = track.getYParams();
  trackPars<3>                                                 bestYParams;
  float                                                        bestMeanDy    = 1e9f;
  const auto                                                   minStereoHits = pars.minStereoHits;

  if ( minStereoHits > allStereoHits.size() ) return false; // otherwise crash if minHits is too large

  auto beginRange = std::begin( allStereoHits ) - 1;
  auto endLoop    = std::end( allStereoHits );

  while ( std::distance( beginRange, endLoop ) > minStereoHits ) {
    ++beginRange;

    // -- > Debug --------
    if ( msgLevel( MSG::DEBUG ) ) {
      debug() << " Size of collected stereo hits: " << allStereoHits.size() << endmsg;
      debug() << " stereo start at ";
      printHit( beginRange->fulldex(), &SciFiHits );
    }
    // -- > Debug --------

    PrPlaneCounter pc; // counting now stereo hits
    auto           endRange = beginRange;
    float          sumCoord = 0.;
    while ( pc.nbDifferent() < minStereoHits || endRange->coord() - std::prev( endRange )->coord() < m_minYGap ) {
      pc.addHit( *endRange );
      sumCoord += endRange->coord();
      ++endRange;
      if ( endRange == allStereoHits.end() ) break;
    }

    // clean cluster
    while ( true ) {
      const float averageCoord = sumCoord / float( endRange - beginRange );

      // remove first if not single and furthest from mean
      if ( pc.nbInPlane( *beginRange ) > 1 && ( beginRange->coord() + std::prev( endRange )->coord() <
                                                2 * averageCoord ) ) { // tune this value has only little effect?!
        pc.removeHit( *beginRange );
        sumCoord -= ( beginRange++ )->coord();
        continue;
      }

      if ( endRange == allStereoHits.end() ) break; // already at end, cluster cannot be expanded anymore

      // add next, if it decreases the range size and is empty
      if ( pc.nbInPlane( *endRange ) == 0 && ( beginRange->coord() + endRange->coord() < 2 * averageCoord ) ) {
        pc.addHit( *endRange );
        sumCoord += ( endRange++ )->coord();
        continue;
      }

      break;
    }

    // -- > Debug --------
    if ( msgLevel( MSG::DEBUG ) ) {
      debug() << "Selected stereo range from " << endmsg;
      printHit( beginRange->fulldex(), &SciFiHits );
      printHit( ( endRange - 1 )->fulldex(), &SciFiHits );
    }
    // -- < Debug --------

    // Now we have a candidate, lets fit him

    track.setYParams( originalYParams );
    decltype( bestStereoHits ) trackStereoHits;
    std::transform( beginRange, endRange, std::back_inserter( trackStereoHits ),
                    []( const auto& hit ) { return hit.fulldex(); } );

    // fit Y Projection of track using stereo hits
    if ( !fitYProjection( track, trackStereoHits, pars, pc ) ) continue;

    if ( !addHitsOnEmptyStereoLayers( track, trackStereoHits, SciFiHits, pars, pc, zonecache ) ) continue;

    if ( trackStereoHits.size() < bestStereoHits.size() ) continue; // number of hits most important selection criteria!

    //== Calculate  dy chi2 /ndf
    float meanDy = ranges::accumulate( trackStereoHits, 0.f,
                                       [&track, &SciFiHits]( float m, const auto& ihit ) {
                                         const float d = track.distance( ihit ) /
                                                         SciFiHits.dxDy( ihit ); // TODO: here the same as in Y fits
                                         return m + d * d;
                                       } ) /
                   float( trackStereoHits.size() - 1 );

    if ( trackStereoHits.size() > bestStereoHits.size() || meanDy < bestMeanDy ) {
      // if same number of hits take smaller chi2
      // -- > Debug --------
      if ( msgLevel( MSG::DEBUG ) ) {
        debug() << "************ Store candidate, nStereo " << trackStereoHits.size() << " meanDy " << meanDy << endmsg;
      }
      // -- < Debug --------
      bestYParams    = track.getYParams();
      bestMeanDy     = meanDy;
      bestStereoHits = std::move( trackStereoHits );
    }
  }
  if ( !bestStereoHits.empty() ) {
    track.setYParams( bestYParams ); // only y params have been modified
    track.addHits( std::move( bestStereoHits ) );
    return true;
  }
  return false;
}

//=========================================================================
//  Convert the local track to the LHCb representation
//=========================================================================
template <typename T>
template <typename Container>
LHCb::Pr::Long::Tracks PrForwardTracking<T>::makeLHCbTracks( Container const&                       trackCandidates,
                                                             std::vector<std::vector<LHCb::LHCbID>> ids,
                                                             T const& input_tracks ) const {
  auto [velo_ancestors, upstream_ancestors, seed_ancestors] = get_ancestors( input_tracks );
  LHCb::Pr::Long::Tracks result( velo_ancestors, upstream_ancestors, seed_ancestors, Zipping::generateZipIdentifier() );
  result.reserve( input_tracks.size() );

  auto const inputtracks = LHCb::Pr::make_zip<SIMDWrapper::InstructionSet::Scalar>( input_tracks );
  for ( auto&& [cand, id] : Gaudi::Functional::details::zip::range( trackCandidates, ids ) ) {
    if ( !cand.valid() ) continue;
    int        uttrack = cand.track();
    auto const itrack  = inputtracks[uttrack];

    I    n_vphits    = 0;
    I    n_uthits    = 0;
    auto currentsize = result.size();
    result.resize( currentsize + 1 );
    result.store<TracksTag::trackSeed>( currentsize, I{-1} );
    if constexpr ( std::is_same_v<T, LHCb::Pr::Upstream::Tracks> ) {

      n_vphits = itrack.nVPHits();
      n_uthits = itrack.nUTHits();
      result.store<TracksTag::trackVP>( currentsize, itrack.trackVP() );
      result.store<TracksTag::trackUT>( currentsize, I{uttrack} );
      result.store<TracksTag::nVPHits>( currentsize, n_vphits );
      result.store<TracksTag::nUTHits>( currentsize, n_uthits );
      for ( auto idx{0}; idx < n_vphits.cast(); ++idx ) {
        result.store_vp_index( currentsize, idx, itrack.vp_index( idx ) );
        result.store_lhcbID( currentsize, idx, itrack.lhcbID( idx ) );
      }
      for ( auto idx{0}; idx < n_uthits.cast(); ++idx ) {
        result.store_ut_index( currentsize, idx, itrack.ut_index( idx ) );
        const auto uthit = n_vphits.cast() + idx;
        result.store_lhcbID( currentsize, uthit, itrack.lhcbID( uthit ) );
      }
    } else {
      n_vphits = itrack.nHits();
      result.store<TracksTag::trackVP>( currentsize, I{uttrack} );
      result.store<TracksTag::trackUT>( currentsize, I{-1} );
      result.store<TracksTag::nUTHits>( currentsize, I{0} );
      result.store<TracksTag::nVPHits>( currentsize, itrack.nHits() );

      for ( auto idx{0}; idx < n_vphits.cast(); ++idx ) {
        result.store_vp_index( currentsize, idx, itrack.vp_index( idx ) );
        result.store_lhcbID( currentsize, idx, itrack.lhcbID( idx ) );
      }
    }

    const auto qOverP = cand.getQoP();
    result.store<TracksTag::StateQoP>( currentsize, F{qOverP} );
    LHCb::State tState;
    float const z = StateParameters::ZEndT;
    tState.setLocation( LHCb::State::Location::AtT );
    tState.setState( cand.x( z ), cand.y( z ), z, cand.xSlope( z ), cand.ySlope( z ), qOverP );
    auto pos = Vec3<F>( tState.x(), tState.y(), tState.z() );
    auto dir = Vec3<F>( tState.tx(), tState.ty(), 1.f );
    result.store_StatePosDir( currentsize, 1, pos, dir );
    LHCb::State vState;
    vState.setState( cand.seed().x0, cand.seed().y0, cand.seed().z0, cand.seed().tx, cand.seed().ty, qOverP );
    auto velopos = Vec3<F>( vState.x(), vState.y(), vState.z() );
    auto velodir = Vec3<F>( vState.tx(), vState.ty(), 1.f );
    result.store_StatePosDir( currentsize, 0, velopos, velodir );

    //== hits indices, max_fthits=15, not sure if this number is reasonable.
    assert( id.size() <= LHCb::Pr::Long::Tracks::MaxFTHits &&
            "Container cannot store more than 15 SciFi hits per track" );

    const I n_fthits =
        ( id.size() < LHCb::Pr::Long::Tracks::MaxFTHits ) ? id.size() : ( LHCb::Pr::Long::Tracks::MaxFTHits );
    result.store<TracksTag::nFTHits>( currentsize, n_fthits );
    auto const& ihits = cand.ihits();
    for ( auto idx{0}; idx < n_fthits.cast(); ++idx ) {
      I const ftidx = bit_cast<int, unsigned int>( ihits[idx] );
      result.store_ft_index( currentsize, idx, ftidx );
      auto const hitidx = n_vphits.cast() + n_uthits.cast() + idx;
      I const    lhcbid = bit_cast<int, unsigned int>( id[idx].lhcbID() );
      result.store_lhcbID( currentsize, hitidx, lhcbid );
    }
    // -- > Debug --------
    if ( msgLevel( MSG::DEBUG ) ) debug() << "Store track  quality " << cand.quality() << endmsg;
    // -- < Debug --------
  } // next candidate
  /// avoid FPEs
  if ( ( result.size() + simd::size ) > result.capacity() ) result.reserve( result.size() + simd::size );
  result.store<TracksTag::StateQoP>( result.size(), simd::float_v( 1.f ) );
  result.store_StatePosDir( result.size(), 0, Vec3<simd::float_v>( 1.f, 1.f, 1.f ),
                            Vec3<simd::float_v>( 1.f, 1.f, 1.f ) );
  result.store_StatePosDir( result.size(), 1, Vec3<simd::float_v>( 1.f, 1.f, 1.f ),
                            Vec3<simd::float_v>( 1.f, 1.f, 1.f ) );

  //=== add UT hits
  if constexpr ( std::is_same_v<T, LHCb::Pr::Velo::Tracks> ) {
    if ( m_addUTHitsTool.isEnabled() ) {
      auto sc = m_addUTHitsTool->addUTHits( result );
      if ( sc.isFailure() ) info() << "adding UT clusters failed!" << endmsg;
    }
  }

  return result;
}
