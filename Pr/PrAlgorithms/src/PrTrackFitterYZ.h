/*******************************************************************************\
 * (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
 *                                                                             *
 * This software is distributed under the terms of the GNU General Public      *
 * Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
 *                                                                             *
 * In applying this licence, CERN does not waive the privileges and immunities *
 * granted to it by virtue of its status as an Intergovernmental Organization  *
 * or submit itself to any jurisdiction.                                       *
\*******************************************************************************/

#ifndef PRTRACKFITTERZY_H
#define PRTRACKFITTERZY_H 1

// Include files
#include "LHCbMath/MatVec.h"
#include "PrHybridSeedTrack.h"
#include "PrKernel/PrFTInfo.h"
#include <array>

/** @class PrTrackFitterYZ PrTrackFitterYZ.h
 *  Support class to perform fit of lines using solely hits in UV layers setting the parameters of
 *  the xz projection and fitting passing iterators of the hough-like cluster
 *  @author Renato Quagliani
 *  @date   2015-08-01
 *  @author Salvatore Aiola (salvatore.aiola@cern.ch)
 *  @date   2020-02-29
 */

class PrTrackFitterYZ final {
  static constexpr size_t N_MAX_HITS   = PrFTInfo::Numbers::NFTUVLayers;
  static constexpr float  ZREF         = Pr::Hybrid::zReference;
  static constexpr int    N_FIT_PARAMS = 2;

  using TrackType    = const Pr::Hybrid::SeedTrackX;
  using TrackTypeRef = TrackType&;
  using MatrixType   = LHCb::LinAlg::MatSym<float, N_FIT_PARAMS>;
  using VectorType   = LHCb::LinAlg::Vec<float, N_FIT_PARAMS>;

public:
  PrTrackFitterYZ()                         = delete;
  PrTrackFitterYZ( const PrTrackFitterYZ& ) = delete;
  PrTrackFitterYZ( PrTrackFitterYZ&& )      = delete;
  PrTrackFitterYZ& operator=( const PrTrackFitterYZ& ) = delete;
  PrTrackFitterYZ& operator=( PrTrackFitterYZ&& ) = delete;

  PrTrackFitterYZ( TrackType& track ) : m_track( track ) {}

  float       chi2PerDoF() const noexcept { return m_chi2PerDoF; }
  const auto& chi2Hits() const noexcept { return m_chi2Hits; }
  auto&       chi2Hits() noexcept { return m_chi2Hits; }
  float       ay() const noexcept { return m_ay; }
  float       by() const noexcept { return m_by; }

  // Fit hit positions with a straight line using Cramer's rule
  template <typename HitIteratorType>
  bool fit( HitIteratorType itBeg, HitIteratorType itEnd ) noexcept {
    VectorType rhs; // right-hand-side of the linear system (function of the x positions)
    MatrixType mat; // matrix of the linear system

    initialize_linear_system( itBeg, itEnd, mat, rhs );

    // Calculate determinant
    float det = mat( 0, 0 ) * mat( 1, 1 ) - mat( 0, 1 ) * mat( 1, 0 );
    if ( det == 0.f ) return false;

    // Solve linear system
    const float invDet = 1.f / det;
    m_ay               = ( rhs( 0 ) * mat( 1, 1 ) - mat( 0, 1 ) * rhs( 1 ) ) * invDet;
    m_by               = ( mat( 0, 0 ) * rhs( 1 ) - rhs( 0 ) * mat( 1, 0 ) ) * invDet;

    calculate_chi2( itBeg, itEnd );

    return true;
  }

private:
  template <typename HitIteratorType>
  void calculate_chi2( HitIteratorType itBeg, HitIteratorType itEnd ) noexcept {
    // Calculate chi2
    m_chi2PerDoF = 0.f;
    for ( auto [iterH, iterChi2] = std::tuple{itBeg, m_chi2Hits.begin()}; itEnd != iterH; ++iterH, ++iterChi2 ) {
      *iterChi2 = chi2hit( *( ( *iterH )->hit ) );
      m_chi2PerDoF += *iterChi2;
    }
    m_chi2PerDoF /= ( itEnd - itBeg - N_FIT_PARAMS );
  }

  template <typename HitIteratorType>
  void initialize_linear_system( HitIteratorType itBeg, HitIteratorType itEnd, MatrixType& mat,
                                 VectorType& rhs ) noexcept {
    mat.m.fill( 0.f );
    rhs.m.fill( 0.f );
    for ( auto iterH = itBeg; itEnd != iterH; ++iterH ) {
      const PrHit& hit    = *( ( *iterH )->hit );
      const float  dz     = hit.z() - ZREF;
      const float  dist   = distanceAt0( hit );
      const float  dxDy   = hit.dxDy();
      const float  wdxDy  = hit.w() * dxDy;
      const float  wdxDy2 = wdxDy * dxDy;
      mat( 0, 0 ) += wdxDy2;
      mat( 0, 1 ) += wdxDy2 * dz;
      mat( 1, 1 ) += wdxDy2 * dz * dz;
      rhs( 0 ) -= wdxDy * dist;
      rhs( 1 ) -= wdxDy * dz * dist;
    }
  }

  float y( float z ) const noexcept { return ( m_ay + m_by * ( z - ZREF ) ); }

  float chi2hit( const PrHit& hit ) const noexcept {
    float       erry = hit.w();
    const float dist = distance( hit );
    return dist * dist * erry;
  }

  float distance( const PrHit& hit ) const noexcept {
    const float z    = hit.z();
    const float yAtZ = y( z );
    return ( hit.x( yAtZ ) - m_track.x( z ) );
  }

  float distanceAt0( const PrHit& hit ) const noexcept {
    const float z = hit.z();
    return ( hit.x() - m_track.x( z ) );
  }

private:
  TrackTypeRef                  m_track;        // reference to the track candidate
  float                         m_chi2PerDoF{}; // chi2 per degrees of freedom
  float                         m_ay{};         // fit param ay
  float                         m_by{};         // fit param by
  std::array<float, N_MAX_HITS> m_chi2Hits{};   // chi2 contributions of each hit
};

#endif // PRLINEFITTERY_H
