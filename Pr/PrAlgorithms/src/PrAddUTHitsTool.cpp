/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/PrLongTracks.h"
#include "Event/State.h"
#include "Event/Track_v2.h"
#include "GaudiAlg/GaudiTool.h"
#include "GaudiKernel/DataObjectHandle.h"
#include "GaudiKernel/SystemOfUnits.h"
#include "IPrAddUTHitsTool.h" // Interface
#include "Kernel/ILHCbMagnetSvc.h"
#include "Kernel/LHCbID.h"
#include "Kernel/STLExtensions.h"
#include "LHCbMath/GeomFun.h"
#include "LHCbMath/SIMDWrapper.h"
#include "LHCbMath/bit_cast.h"
#include "PrKernel/PrMutUTHits.h"
#include "PrKernel/PrUTHitHandler.h"
#include "UTDAQ/UTDAQHelper.h"
#include "UTDAQ/UTInfo.h"
#include "boost/container/small_vector.hpp"
#include "vdt/log.h"
#include "vdt/sqrt.h"
#include <algorithm>
#include <array>
#include <numeric>
#include <optional>

//-----------------------------------------------------------------------------
// Implementation file for class : PrAddUTHitsTool
//
// 2016-05-11 : Michel De Cian
//
//-----------------------------------------------------------------------------
/*
 * @class PrAddUTHitsTool PrAddUTHitsTool.h
 *
 * \brief  Adds UT hits to long tracks, see note LHCb-INT-2010-20 for TT version
 *
 * Parameters:
 * - ZUTField: Z-Position of the kink for the state extrapolation
 * - ZMSPoint: Z-Position of the multiple scattering point
 * - UTParam: Parameter of the slope of the state extrapolation
 * - MaxChi2Tol: Offset of the chi2 cut
 * - MaxChi2Slope: Slope of the chi2 cut
 * - MaxChi2POffset: Momentum offest of the chi2 cut
 * - YTolSlope: Offest of the y-tolerance cut
 * - XTol: Offest of the x-window cut
 * - XTolSlope: Slope of the x-window cut
 * - MajAxProj: Major axis of the ellipse for the cut on the projection
 * - MinAxProj: Minor axis of the ellipse for the cut on the projection
 * - ZUTProj: Z-Position which state has to be closest to
 *
 *
 *  @author Michel De Cian
 *  @date   2016-05-11
 *
 */
namespace LHCb::Pr {

  using simd   = SIMDWrapper::best::types;
  using I      = simd::int_v;
  using F      = simd::float_v;
  using scalar = SIMDWrapper::scalar::types;
  using sI     = scalar::int_v;
  using sF     = scalar::float_v;

  constexpr static int max_tracks = align_size( 2048 );
  constexpr static int maxSectors = 9;

  struct MiniStates final {
    std::array<float, 3 * max_tracks> poss;
    std::array<float, 2 * max_tracks> dirs;
    std::array<float, max_tracks>     qops;
    std::array<float, max_tracks>     ps;
    std::array<int, max_tracks>       indexs;

    std::size_t size{0};

    SOA_ACCESSOR( x, &( poss[0] ) )
    SOA_ACCESSOR( y, &( poss[max_tracks] ) )
    SOA_ACCESSOR( z, &( poss[2 * max_tracks] ) )
    SOA_ACCESSOR( tx, &( dirs[0] ) )
    SOA_ACCESSOR( ty, &( dirs[max_tracks] ) )
    SOA_ACCESSOR( qop, qops.data() )
    SOA_ACCESSOR( p, ps.data() )
    SOA_ACCESSOR( index, indexs.data() )
    VEC3_SOA_ACCESSOR( pos, (float*)&( poss[0] ), (float*)&( poss[max_tracks] ), (float*)&( poss[2 * max_tracks] ) )
    VEC3_XY_SOA_ACCESSOR( dir, (float*)&( dirs[0] ), (float*)&( dirs[max_tracks] ), 1.0f )
  };
  struct Boundary final {

    std::array<int, maxSectors * max_tracks> sects;
    std::array<float, max_tracks>            xTols;
    std::array<int, max_tracks>              nPoss;

    std::size_t size{0};
    SOA_ACCESSOR_VAR( sect, &( sects[pos * max_tracks] ), int pos )
    SOA_ACCESSOR( xTol, xTols.data() )
    SOA_ACCESSOR( nPos, nPoss.data() )
  };

  class PrAddUTHitsTool : public extends<GaudiTool, IPrAddUTHitsTool> {

    using Tracks = LHCb::Pr::Long::Tracks;

  public:
    /// Standard constructor
    using extends::extends;

    StatusCode initialize() override;

    /** @brief Add UT clusters to matched tracks. This calls returnUTHits internally
        @param track Track to add the UT hits to
    */
    StatusCode addUTHits( Tracks& longtracks ) const override;

    /** Return UT hits without adding them.
        @param state State closest to UT for extrapolation (normally Velo state)
        @param ttHits Container to fill UT hits in
        @param finalChi2 internal chi2 of the UT hit adding
        @param p momentum estimate. If none given, the one from the state will be taken
    */

    // LHCb::Pr::UT::Mut::Hits returnUTHits( LHCb::State& state, float& finalChi2, float p = 0 ) const;

  private:
    StatusCode recomputeGeometry();

    DeUTDetector* m_utDet = nullptr;
    /// information about the different layers
    std::optional<LHCb::UTDAQ::GeomCache> m_geomcache;

    DataObjectReadHandle<LHCb::Pr::UT::HitHandler> m_HitHandler{this, "UTHitsLocation", "UT/PrUTHits"};

    Gaudi::Property<float> p_zUTField{this, "ZUTField", 1740. * Gaudi::Units::mm};
    Gaudi::Property<float> p_zMSPoint{this, "ZMSPoint", 400. * Gaudi::Units::mm};
    Gaudi::Property<float> p_utParam{this, "UTParam", 29.};
    Gaudi::Property<float> p_zUTProj{this, "ZUTProj", 2500. * Gaudi::Units::mm};
    Gaudi::Property<float> p_maxChi2Tol{this, "MaxChi2Tol", 2.0};
    Gaudi::Property<float> p_maxChi2Slope{this, "MaxChi2Slope", 25000.0};
    Gaudi::Property<float> p_maxChi2POffset{this, "MaxChi2POffset", 100.0};
    Gaudi::Property<float> p_yTolSlope{this, "YTolSlope", 20000.};
    Gaudi::Property<float> p_xTol{this, "XTol", 1.0};
    Gaudi::Property<float> p_xTolSlope{this, "XTolSlope", 30000.0};
    float                  m_invMajAxProj2 = 0.0;
    Gaudi::Property<float> p_majAxProj{
        this, "MajAxProj", 20.0 * Gaudi::Units::mm,
        [=]( auto& ) { this->m_invMajAxProj2 = 1 / ( this->p_majAxProj * this->p_majAxProj ); },
        Gaudi::Details::Property::ImmediatelyInvokeHandler{true}};
    Gaudi::Property<float> p_minAxProj{this, "MinAxProj", 2.0 * Gaudi::Units::mm};

    mutable Gaudi::Accumulators::SummingCounter<unsigned int> m_hitsAddedCounter{this, "#UT hits added"};
    mutable Gaudi::Accumulators::Counter<>                    m_tracksWithHitsCounter{this, "#tracks with hits added"};
    mutable Gaudi::Accumulators::MsgCounter<MSG::ERROR>       m_too_much_in_filtered{
        this, "Reached the maximum number of tracks in filteredStates!!"};
    mutable Gaudi::Accumulators::MsgCounter<MSG::ERROR> m_too_much_in_boundaries{
        this, "Reached the maximum number of tracks in Boundaries!!"};
    mutable Gaudi::Accumulators::MsgCounter<MSG::ERROR> m_too_much_in_MutHitsContainer{
        this, "Reached the maximum number of hits in MutUTHitsContainer!!"};

    ServiceHandle<ILHCbMagnetSvc> m_magFieldSvc{this, "MagneticField", "MagneticFieldSvc"};

    using BoundaryArray = std::array<LHCb::Pr::Boundary, static_cast<int>( UTInfo::DetectorNumbers::TotalLayers )>;

    BoundaryArray           findAllSectors( Tracks& tracks, MiniStates& filteredStates ) const;
    LHCb::Pr::UT::Mut::Hits returnUTHits( MiniStates& filteredStates, const BoundaryArray& compBoundsArray,
                                          std::size_t t ) const;
    bool                    selectHits( MiniStates& filteredStates, const BoundaryArray& compBoundsArray,
                                        LHCb::Pr::UT::Mut::Hits& hitsInLayers, std::size_t t ) const;
    void                    calculateChi2( float& chi2, const float& bestChi2, float& finalDist, const float& p,
                                           LHCb::Pr::UT::Mut::Hits& goodUT ) const;
    void                    printInfo( float dist, float chi2, const LHCb::Pr::UT::Mut::Hits& goodUT ) const;
  };
  // Declaration of the Algorithm Factory
  DECLARE_COMPONENT_WITH_ID( PrAddUTHitsTool, "PrAddUTHitsTool" )

  namespace {
    // -- bubble sort is slow, but we never have more than 9 elements (horizontally)
    // -- and can act on 8 elements at once vertically (with AVX)
    void bubbleSortSIMD(
        const int                                                                                      maxColsMaxRows,
        std::array<simd::int_v, maxSectors* static_cast<int>( UTInfo::DetectorNumbers::TotalLayers )>& helper,
        const int                                                                                      start ) {
      for ( int i = 0; i < maxColsMaxRows - 1; i++ ) {
        for ( int j = 0; j < maxColsMaxRows - i - 1; j++ ) {
          swap( helper[start + j] > helper[start + j + 1], helper[start + j], helper[start + j + 1] );
        }
      }
    }

    // remove duplicated sectors
    simd::int_v
    makeUniqueSIMD( std::array<simd::int_v, maxSectors* static_cast<int>( UTInfo::DetectorNumbers::TotalLayers )>& out,
                    int start, size_t len ) {
      simd::int_v pos  = start + 1;
      simd::int_v oldv = out[start];
      for ( size_t j = start + 1; j < start + len; ++j ) {
        simd::int_v  newv      = out[j];
        simd::mask_v blendMask = ( newv == oldv );
        for ( size_t k = j + 1; k < start + len; ++k ) { out[k - 1] = select( blendMask, out[k], out[k - 1] ); }
        oldv = newv;
        pos  = pos + select( blendMask, simd::int_v{0}, simd::int_v{1} );
      }
      return pos;
    }
  } // namespace

  using ROOT::Math::CholeskyDecomp;

  //=========================================================================
  //
  //=========================================================================
  StatusCode PrAddUTHitsTool::initialize() {
    return GaudiTool::initialize().andThen( [&] {
      m_utDet = getDet<DeUTDetector>( DeUTDetLocation::UT );
      // Make sure we precompute z positions/sizes of the layers/sectors
      registerCondition( m_utDet->geometry(), &PrAddUTHitsTool::recomputeGeometry );
    } );
  }

  StatusCode PrAddUTHitsTool::recomputeGeometry() {
    m_geomcache.emplace( *m_utDet );
    return StatusCode::SUCCESS;
  }

  //=========================================================================
  //  Add the UT hits on the track, only the ids.
  //=========================================================================
  StatusCode PrAddUTHitsTool::addUTHits( Tracks& tracks ) const {
    MiniStates filteredStates;
    auto       compBoundsArray = findAllSectors( tracks, filteredStates );
    auto const longtracks      = LHCb::Pr::make_zip<SIMDWrapper::InstructionSet::Scalar>( tracks );
    for ( auto t = 0; t < int( filteredStates.size ); t++ ) {
      auto myUTHits = returnUTHits( filteredStates, compBoundsArray, t );

      if ( ( myUTHits.size < 3 ) ) continue;
      assert( myUTHits.size <= LHCb::Pr::Upstream::Tracks::MaxUTHits &&
              "Container cannot store more than 8 UT hits per track" );

      auto const itr     = filteredStates.index<sI>( t ).cast();
      const int  nHits   = longtracks[itr].nHits().cast();
      const sI   nUTHits = bit_cast<int, unsigned int>( myUTHits.size );
      tracks.store<TracksTag::nUTHits>( itr, nUTHits );

      for ( auto i = 0; i < int( myUTHits.size ); i++ ) {
        // ----------------------------------
        if ( UNLIKELY( msgLevel( MSG::DEBUG ) ) )
          debug() << "--- Adding Hit in Layer: " << myUTHits.planeCode<sI>( i )
                  << " with projection: " << myUTHits.projections[i] << endmsg;
        // ----------------------------------
        // add ut hit indices and lhcbIDs to the long track
        const I      idxhit = myUTHits.indexs[i];
        LHCb::LHCbID lhcbid( LHCb::UTChannelID( myUTHits.channelIDs[i] ) );
        const sI     lhcbID = bit_cast<int, unsigned int>( lhcbid.lhcbID() );
        tracks.store_ut_index( itr, i, idxhit );
        tracks.store_lhcbID( itr, nHits + i, lhcbID );
      }
    }
    return StatusCode::SUCCESS;
  }

  ///=======================================================================
  //  find all sections
  ///=======================================================================
  PrAddUTHitsTool::BoundaryArray PrAddUTHitsTool::findAllSectors( LHCb::Pr::Long::Tracks& tracks,
                                                                  MiniStates&             filteredStates ) const {

    BoundaryArray compBoundsArray;
    filteredStates.size = 0;
    std::array<simd::int_v, static_cast<int>( UTInfo::DetectorNumbers::TotalLayers )> posArray;
    std::array<simd::int_v, maxSectors* static_cast<int>( UTInfo::DetectorNumbers::TotalLayers )>
                                                                              helperArray; // 4 layers x maximum 9 sectors
    std::array<int, static_cast<int>( UTInfo::DetectorNumbers::TotalLayers )> maxColsRows;

    //--- This now works with up to 9 sectors
    const float signedReCur = m_magFieldSvc->signedRelativeCurrent();
    auto const  longtracks  = LHCb::Pr::make_zip( tracks );
    for ( auto const& track : longtracks ) {
      auto        loopMask = track.loop_mask();
      simd::int_v nLayers{0};

      //---Define the tolerance parameters
      const F qoverp = track.qOverP();
      const F p      = abs( 1 / qoverp );
      const F yTol   = p_yTolSlope.value() / p;
      const F xTol   = p_xTol.value() + p_xTolSlope.value() / p;

      auto    pos     = track.StatePos( 0 );
      auto    dir     = track.StateDir( 0 );
      const F stateX  = pos.x;
      const F stateY  = pos.y;
      const F stateZ  = pos.z;
      const F stateTx = dir.x;
      const F stateTy = dir.y;

      const F bendParam = p_utParam.value() * -1 * signedReCur * qoverp;

      assert( m_geomcache.has_value() );
      for ( auto&& [layerIndex, layer] : LHCb::range::enumerate( m_geomcache->layers ) ) {

        const F zLayer   = layer.z;
        const F yPredLay = stateY + ( zLayer - stateZ ) * stateTy;
        const F xPredLay = stateX + ( zLayer - stateZ ) * stateTx + bendParam * ( zLayer - p_zUTField.value() );

        const simd::int_v regionBoundary1 = ( 2 * layer.nColsPerSide + 3 );
        const simd::int_v regionBoundary2 = ( 2 * layer.nColsPerSide - 5 );

        simd::int_v subcolmin{0};
        simd::int_v subcolmax{0};
        simd::int_v subrowmin{0};
        simd::int_v subrowmax{0};

        simd::mask_v mask = LHCb::UTDAQ::findSectors( layerIndex, xPredLay, yPredLay, xTol, yTol, layer, subcolmin,
                                                      subcolmax, subrowmin, subrowmax );

        const simd::mask_v gathermask = loopMask && mask;

        // -- Determine the maximum number of rows and columns we have to take into account
        // -- maximum 3
        const int maxCols = std::clamp( ( subcolmax - subcolmin ).hmax( gathermask ) + 1, 0, 3 );
        const int maxRows = std::clamp( ( subrowmax - subrowmin ).hmax( gathermask ) + 1, 0, 3 );

        maxColsRows[layerIndex] = maxCols * maxRows;

        int counter = 0;
        for ( int sc = 0; sc < maxCols; sc++ ) {
          simd::int_v realSC = min( subcolmax, subcolmin + sc );
          simd::int_v region = select( realSC > regionBoundary1, simd::int_v{1}, simd::int_v{0} ) +
                               select( realSC > regionBoundary2, simd::int_v{1}, simd::int_v{0} );

          for ( int sr = 0; sr < maxRows; sr++ ) {

            simd::int_v realSR = min( subrowmax, subrowmin + sr );
            simd::int_v sectorIndex =
                realSR + static_cast<int>( UTInfo::SectorNumbers::EffectiveSectorsPerColumn ) * realSC;

            // -- only gather when we are not outside the acceptance
            // -- if we are outside, fill 1 which is the lowest possible sector number
            // -- We need to fill a valid number, as one can have 3 layers with a correct sector
            // -- and one without a correct sector, in which case the track will not be masked off.
            // -- However, these cases should happen very rarely
            simd::int_v sect =
                ( layerIndex < 2 )
                    ? m_geomcache->sectorLUT.maskgather_station1<simd::int_v>( sectorIndex, gathermask, 1 )
                    : m_geomcache->sectorLUT.maskgather_station2<simd::int_v>( sectorIndex, gathermask, 1 );

            // -- ID is: sectorIndex (from LUT) + (layerIndex * 3 + region - 1 ) * 98
            // -- The regions are already calculated with a -1
            helperArray[maxSectors * layerIndex + counter] =
                sect +
                ( layerIndex * static_cast<int>( UTInfo::DetectorNumbers::Regions ) + region ) *
                    static_cast<int>( UTInfo::DetectorNumbers::Sectors ) -
                1;
            counter++;
          }
        }
        // sorting
        bubbleSortSIMD( maxCols * maxRows, helperArray, maxSectors * layerIndex );
        // uniquifying
        posArray[layerIndex] = makeUniqueSIMD( helperArray, maxSectors * layerIndex, maxCols * maxRows );
        // count the number of `valid` layers
        nLayers += select( mask, simd::int_v{1}, simd::int_v{0} );
      }
      //-- we need at least three layers
      const simd::mask_v compressMask = ( nLayers > 2 ) && loopMask;

      for ( auto&& [iLayer, compBoundsLayer] : LHCb::range::enumerate( compBoundsArray ) ) {
        int index = compBoundsLayer.size;
        if ( index + simd::popcount( compressMask ) > LHCb::Pr::max_tracks ) {
          ++m_too_much_in_boundaries;
          break;
        }
        for ( int iSector = 0; iSector < maxColsRows[iLayer]; ++iSector ) {
          compBoundsLayer.compressstore_sect<I>( index, iSector, compressMask,
                                                 helperArray[maxSectors * iLayer + iSector] );
        }
        compBoundsLayer.compressstore_xTol<F>( index, compressMask, xTol );
        compBoundsLayer.compressstore_nPos<I>( index, compressMask, posArray[iLayer] - maxSectors * iLayer );
        compBoundsLayer.size += simd::popcount( compressMask );
      }

      // -- Now need to compress the filtered states, such that they are
      // -- in sync with the sectors

      int stateidx = filteredStates.size;
      if ( stateidx + simd::popcount( compressMask ) > LHCb::Pr::max_tracks ) {
        ++m_too_much_in_filtered;
        break;
      }
      filteredStates.compressstore_pos<F>( stateidx, compressMask, pos );
      filteredStates.compressstore_dir<F>( stateidx, compressMask, dir );
      filteredStates.compressstore_qop<F>( stateidx, compressMask, qoverp );
      filteredStates.compressstore_p<F>( stateidx, compressMask, p );
      filteredStates.compressstore_index<I>( stateidx, compressMask, track.indices() );
      filteredStates.size += simd::popcount( compressMask );
    }

    return compBoundsArray;
  }

  //=========================================================================
  //  Return the TT hits
  //=========================================================================
  LHCb::Pr::UT::Mut::Hits PrAddUTHitsTool::returnUTHits( MiniStates&          filteredStates,
                                                         const BoundaryArray& compBoundsArray, std::size_t t ) const {
    LHCb::Pr::UT::Mut::Hits UTHits;
    if ( UNLIKELY( msgLevel( MSG::DEBUG ) ) ) debug() << "--- Entering returnUTHits ---" << endmsg;

    // -- Get the container with all the hits compatible with the track
    LHCb::Pr::UT::Mut::Hits hitsInLayers;
    hitsInLayers.size = 0;
    for ( auto& it : hitsInLayers.layerIndices ) it = -1;

    bool findHits = selectHits( filteredStates, compBoundsArray, hitsInLayers, t );

    // -- If less three layer or only two hits are selected, end algorithm
    if ( !findHits || int( hitsInLayers.size ) < 3 ) return UTHits;

    const auto p = filteredStates.p<sF>( t ).cast();

    float bestChi2 = p_maxChi2Tol.value() + p_maxChi2Slope.value() / ( p - p_maxChi2POffset.value() );

    // sort of hits in increasing projection
    std::vector<int> hitIdx;
    hitIdx.reserve( int( hitsInLayers.size ) );
    for ( int i = 0; i < int( hitsInLayers.size ); i++ ) hitIdx.emplace_back( i );
    std::sort( hitIdx.begin(), hitIdx.end(), [&hitsInLayers]( const int i, const int j ) {
      return hitsInLayers.projections[i] < hitsInLayers.projections[j];
    } );
    // remove duplicates if there is
    hitIdx.erase( std::unique( hitIdx.begin(), hitIdx.end(),
                               [&hitsInLayers]( const int i, const int j ) {
                                 return hitsInLayers.channelIDs[i] == hitsInLayers.channelIDs[j];
                               } ),
                  hitIdx.end() );

    // -- Loop over all hits and make "groups" of hits to form a candidate
    for ( auto itB = 0; itB + 2 < int( hitIdx.size() ); ++itB ) {
      const int   itBeg     = hitIdx[itB];
      const float firstProj = hitsInLayers.projections[itBeg];

      LHCb::Pr::UT::Mut::Hits goodUT;

      int                nbPlane = 0;
      std::array<int, 4> firedPlanes{};

      // -- If |firstProj| > m_majAxProj, the sqrt is ill defined
      float maxProj = firstProj;
      if ( fabs( firstProj ) < p_majAxProj.value() ) {
        // -- m_invMajAxProj2 = 1/(m_majAxProj*m_majAxProj), but it's faster like this
        maxProj = firstProj +
                  sqrt( p_minAxProj.value() * p_minAxProj.value() * ( 1 - firstProj * firstProj * m_invMajAxProj2 ) );
      }
      // TODO -- This means that there would be less than 3 hits, which does not work, so we can skip this right away
      if ( ( hitsInLayers.projections[hitIdx[itB + 2]] ) > maxProj ) continue;

      // -- Make "group" of hits which are within a certain distance to the first hit of the group
      for ( auto itE = itB; itE < int( hitIdx.size() ); itE++ ) {
        const int itEnd = hitIdx[itE];
        if ( hitsInLayers.projections[itEnd] > maxProj ) break;
        if ( 0 == firedPlanes[hitsInLayers.planeCode<sI>( itEnd ).cast()] ) {
          firedPlanes[hitsInLayers.planeCode<sI>( itEnd ).cast()] = 1; // -- Count number of fired planes
          ++nbPlane;
        }
        scalar::mask_v mask = 1;
        goodUT.copy_from<scalar>( hitsInLayers, itEnd, mask );
      }

      if ( 3 > nbPlane ) continue; // -- Need at least hits in 3 planes
      // -- group of hits has to be at least as large than best group at this stage
      if ( UTHits.size > goodUT.size ) continue;

      // ----------------------------------
      if ( UNLIKELY( msgLevel( MSG::DEBUG ) ) )
        debug() << "Start fit, first proj " << firstProj << " nbPlane " << nbPlane << " size " << goodUT.size << endmsg;
      // -- Set variables for the chi2 calculation
      float dist = 0;
      float chi2 = 1.e20;

      calculateChi2( chi2, bestChi2, dist, p, goodUT );

      // -- If this group has a better chi2 than all the others
      // -- and is at least as large as all the others, then make this group the new candidate
      if ( bestChi2 > chi2 && goodUT.size >= UTHits.size ) {

        UTHits.size = 0;
        // ----------------------------------
        if ( UNLIKELY( msgLevel( MSG::DEBUG ) ) ) printInfo( dist, chi2, goodUT );
        // ----------------------------------
        for ( auto i = 0; i < int( goodUT.size ); i += simd::size ) {
          auto loopmask = simd::loop_mask( i, goodUT.size );
          UTHits.copy_from<simd>( goodUT, i, loopmask );
        }
        bestChi2 = chi2;
      }
    }

    // -- Assign the final hit container and chi2 to the variables which are returned.
    if ( UTHits.size > 2 ) {
      m_hitsAddedCounter += UTHits.size;
      m_tracksWithHitsCounter++;
    }
    return UTHits;
  }
  //=========================================================================
  // Select the hits in a certain window
  //=========================================================================
  bool PrAddUTHitsTool::selectHits( MiniStates& filteredStates, const BoundaryArray& compBoundsArray,
                                    LHCb::Pr::UT::Mut::Hits& hitsInLayers, std::size_t t ) const {

    // -- Define the parameter that describes the bending
    // -- in principle the call m_magFieldSvc->signedRelativeCurrent() is not needed for every track...
    const float signedReCur = m_magFieldSvc->signedRelativeCurrent();
    hitsInLayers.size       = 0;

    // -- This is for some sanity checks later
    constexpr const int maxSectorsPerRegion = static_cast<int>( UTInfo::SectorNumbers::MaxSectorsPerRegion );
    constexpr const int maxLayer            = static_cast<int>( UTInfo::DetectorNumbers::TotalLayers );
    constexpr const int maxRegion           = static_cast<int>( UTInfo::DetectorNumbers::Regions );
    [[maybe_unused]] constexpr const int maxSectorNumber =
        maxSectorsPerRegion + ( ( maxLayer - 1 ) * maxRegion + ( maxRegion - 1 ) ) * maxSectorsPerRegion;

    const float stateX    = filteredStates.x<sF>( t ).cast();
    const float stateY    = filteredStates.y<sF>( t ).cast();
    const float stateZ    = filteredStates.z<sF>( t ).cast();
    const float stateTx   = filteredStates.tx<sF>( t ).cast();
    const float stateTy   = filteredStates.ty<sF>( t ).cast();
    const float p         = filteredStates.p<sF>( t ).cast();
    const float qop       = filteredStates.qop<sF>( t ).cast();
    const float bendParam = p_utParam.value() * -1.0 * signedReCur * qop;

    if ( UNLIKELY( msgLevel( MSG::DEBUG ) ) )
      debug() << "State z:  " << stateZ << " x " << stateX << " y " << stateY << " tx " << stateTx << " ty " << stateTy
              << " p " << p << endmsg;

    std::size_t               nSize   = 0;
    std::size_t               nLayers = 0;
    const LHCb::Pr::UT::Hits& myHits  = m_HitHandler.get()->hits();
    for ( auto&& [layerIndex, compBoundsLayer] : LHCb::range::enumerate( compBoundsArray ) ) {
      if ( ( layerIndex == 2 && nLayers == 0 ) || ( layerIndex == 3 && nLayers < 2 ) ) return false;

      // -- Define the tolerance parameters
      const F yTol = p_yTolSlope.value() / p;
      const F xTol = p_xTol.value() + p_xTolSlope.value() / p;

      const int                       nPos = compBoundsLayer.nPos<sI>( t ).cast();
      std::array<int, maxSectors + 1> sectors{0};
      for ( int i = 0; i < nPos; ++i ) {
        sectors[i] = std::min( maxSectorNumber - 1, std::max( compBoundsLayer.sect<sI>( t, i ).cast(), 0 ) );
      }

      for ( int j = 0; j < nPos; j++ ) {
        assert( ( sectors[j] > -1 ) && ( sectors[j] < maxSectorNumber ) && "sector number out of bound" );
        if ( ( sectors[j] < -1 ) || ( sectors[j] > maxSectorNumber ) ) {
          error() << "sector number out of bound, something wrong! Check!" << endmsg;
        }

        const std::pair<int, int>& temp       = m_HitHandler.get()->indices( sectors[j] );
        const std::pair<int, int>& temp2      = m_HitHandler.get()->indices( sectors[j + 1] );
        const int                  firstIndex = temp.first;
        const int                  shift =
            ( temp2.first == temp.second || ( temp.first == temp2.first && temp.second == temp2.second ) );
        const int lastIndex = ( shift == 1 && ( j + 1 ) < nPos ) ? temp2.second : temp.second;
        j += shift;
        if ( temp2.first == 0 && temp2.second == 0 && temp2.first != temp.second ) j += 1;
        for ( int i = firstIndex; i < lastIndex; i += simd::size ) {
          auto    loopMask = simd::loop_mask( i, lastIndex );
          const F yPred    = stateY + ( myHits.zAtYEq0<F>( i ) - stateZ ) * stateTy;

          const auto yMin  = min( myHits.yBegin<F>( i ), myHits.yEnd<F>( i ) );
          const auto yMax  = max( myHits.yBegin<F>( i ), myHits.yEnd<F>( i ) );
          const auto yy    = stateY + ( myHits.zAtYEq0<F>( i ) - stateZ ) * stateTy;
          auto       xx    = myHits.xAtYEq0<F>( i ) + yy * myHits.dxDy<F>( i );
          F          xPred = stateX + stateTx * ( myHits.zAtYEq0<F>( i ) - stateZ ) +
                    bendParam * ( myHits.zAtYEq0<F>( i ) - p_zUTField.value() );
          F absdx = abs( xx - xPred );

          if ( none( absdx < xTol ) ) continue;

          auto mask = ( yMin - yTol < yPred && yPred < yMax + yTol ) && ( absdx < xTol ) && loopMask;

          if ( none( mask ) ) continue;
          const F projDist = ( xPred - xx ) * ( p_zUTProj.value() - p_zMSPoint.value() ) /
                             ( myHits.zAtYEq0<F>( i ) - p_zMSPoint.value() );

          // save the selected hits
          auto index = hitsInLayers.size;
          if ( hitsInLayers.size + simd::popcount( mask ) > LHCb::Pr::UT::Mut::Hits::max_hits ) {
            ++m_too_much_in_MutHitsContainer;
            break;
          }
          hitsInLayers.compressstore_x( index, mask, xx );
          hitsInLayers.compressstore_z( index, mask, myHits.zAtYEq0<F>( i ) );
          hitsInLayers.compressstore_cos( index, mask, myHits.cos<F>( i ) );
          hitsInLayers.compressstore_sin( index, mask, myHits.cos<F>( i ) * -1.0f * myHits.dxDy<F>( i ) );
          hitsInLayers.compressstore_weight( index, mask, myHits.weight<F>( i ) );
          hitsInLayers.compressstore_projection( index, mask, projDist );
          hitsInLayers.compressstore_channelID( index, mask, myHits.channelID<I>( i ) );
          hitsInLayers.compressstore_index( index, mask, simd::indices( i ) );
          hitsInLayers.size += simd::popcount( mask );
        }
      }
      nLayers += int( nSize != hitsInLayers.size );
      hitsInLayers.layerIndices[layerIndex] = nSize;
      nSize                                 = hitsInLayers.size;
    }
    return nLayers > 2;
  }
  //=========================================================================
  // Calculate Chi2
  //=========================================================================
  void PrAddUTHitsTool::calculateChi2( float& chi2, const float& bestChi2, float& finalDist, const float& p,
                                       LHCb::Pr::UT::Mut::Hits& goodUT ) const {

    // -- Fit a straight line to the points and calculate the chi2 of the hits with respect to the fitted track

    float dist              = 0;
    chi2                    = 1.e20;
    const float xTol        = p_xTol.value() + p_xTolSlope.value() / p;
    const float fixedWeight = 9. / ( xTol * xTol );

    unsigned int       nHits         = goodUT.size;
    const unsigned int maxIterations = nHits;
    unsigned int       counter       = 0;

    // -- Loop until chi2 has a reasonable value or no more outliers can be removed to improve it
    // -- (with the counter as a sanity check to avoid infinite loops).

    unsigned int                nDoF = 0;
    std::array<unsigned int, 4> differentPlanes;
    differentPlanes.fill( 0 );
    float worstDiff = -1.0;
    float mat[6], rhs[3];

    mat[0] = fixedWeight; // -- Fix X = 0 with fixedWeight
    mat[1] = 0.;
    mat[2] = fixedWeight * ( p_zUTProj - p_zMSPoint ) *
             ( p_zUTProj - p_zMSPoint ); // -- Fix slope by point at multiple scattering point
    mat[3] = 0.;
    mat[4] = 0.;
    mat[5] = fixedWeight; // -- Fix Y = 0 with fixedWeight
    rhs[0] = 0.;
    rhs[1] = 0.;
    rhs[2] = 0.;

    for ( auto i = 0; i < int( goodUT.size ); i++ ) {
      const float w     = goodUT.weights[i];
      const float dz    = goodUT.zs[i] - p_zUTProj;
      const float t     = goodUT.sins[i];
      const float dist2 = goodUT.projections[i];
      mat[0] += w;
      mat[1] += w * dz;
      mat[2] += w * dz * dz;
      mat[3] += w * t;
      mat[4] += w * dz * t;
      mat[5] += w * t * t;
      rhs[0] += w * dist2;
      rhs[1] += w * dist2 * dz;
      rhs[2] += w * dist2 * t;

      if ( 0 == differentPlanes[goodUT.planeCode<sI>( i ).cast()]++ ) ++nDoF;
    }

    // -- Loop to remove outliers
    // -- Don't loop more often than number of hits in the selection
    // -- The counter protects infinite loops in very rare occasions.
    while ( chi2 > 1e10 && counter < maxIterations ) {

      worstDiff = -1.0;
      int worst = -1;

      // -- This is needed since 'CholeskyDecomp' overwrites rhs
      // -- which is needed later on
      const double saveRhs[3] = {rhs[0], rhs[1], rhs[2]};

      CholeskyDecomp<double, 3> decomp( mat );
      if ( UNLIKELY( !decomp ) ) {
        chi2 = 1e42;
        break;
      } else {
        decomp.Solve( rhs );
      }

      const double offset  = rhs[0];
      const double slope   = rhs[1];
      const double offsetY = rhs[2];

      rhs[0] = saveRhs[0];
      rhs[1] = saveRhs[1];
      rhs[2] = saveRhs[2];

      chi2 = fixedWeight * ( offset * offset + offsetY * offsetY +
                             ( p_zUTProj.value() - p_zMSPoint.value() ) * ( p_zUTProj.value() - p_zMSPoint.value() ) *
                                 slope * slope );

      for ( auto it = 0; it < int( goodUT.size ); it++ ) {
        const float w  = goodUT.weights[it];
        const float dz = goodUT.zs[it] - p_zUTProj;
        dist           = goodUT.projections[it] - offset - slope * dz - offsetY * goodUT.sins[it];
        if ( ( 1 < differentPlanes[goodUT.planeCode<sI>( it ).cast()] || nDoF == nHits ) &&
             worstDiff < w * dist * dist ) {
          worstDiff = w * dist * dist;
          worst     = it;
        }
        chi2 += w * dist * dist;
      }

      if ( nDoF != 0 ) chi2 /= nDoF;

      if ( UNLIKELY( msgLevel( MSG::DEBUG ) ) && worstDiff > 0. ) {
        info() << format( " chi2 %10.2f nDoF%2d wors %8.2f proj %6.2f offset %8.3f slope %10.6f offsetY %10.6f", chi2,
                          nDoF, worstDiff, goodUT.projections[worst], offset, slope, offsetY )
               << endmsg;
      }
      // -- Remove last point (outlier) if bad fit...or if nHits>8.
      if ( worstDiff > 0. && ( ( bestChi2 < chi2 && nHits > 3 ) || ( bestChi2 > chi2 && nHits > 8 ) ) ) {

        const double w     = goodUT.weights[worst];
        const double dz    = goodUT.zs[worst] - p_zUTProj;
        const double t     = goodUT.sins[worst];
        const double dist2 = goodUT.projections[worst];
        mat[0] -= w;
        mat[1] -= w * dz;
        mat[2] -= w * dz * dz;
        mat[3] -= w * t;
        mat[4] -= w * dz * t;
        mat[5] -= w * t * t;
        rhs[0] -= w * dist2;
        rhs[1] -= w * dist2 * dz;
        rhs[2] -= w * dist2 * t;

        if ( 1 == differentPlanes[goodUT.planeCode<sI>( worst ).cast()]-- ) --nDoF;
        // remove the worst hit
        goodUT.xs[worst]          = goodUT.xs[nHits - 1];
        goodUT.zs[worst]          = goodUT.zs[nHits - 1];
        goodUT.coss[worst]        = goodUT.coss[nHits - 1];
        goodUT.sins[worst]        = goodUT.sins[nHits - 1];
        goodUT.weights[worst]     = goodUT.weights[nHits - 1];
        goodUT.projections[worst] = goodUT.projections[nHits - 1];
        goodUT.channelIDs[worst]  = goodUT.channelIDs[nHits - 1];
        goodUT.indexs[worst]      = goodUT.indexs[nHits - 1];
        goodUT.size               = goodUT.size - 1;

        --nHits;
        chi2 = 1.e11; // S--Start new iteration
      }
      // -- Increase the sanity check counter
      ++counter;
    }

    finalDist = dist;
  }

  //=========================================================================
  // Print out info
  //=========================================================================
  void PrAddUTHitsTool::printInfo( float dist, float chi2, const LHCb::Pr::UT::Mut::Hits& goodUT ) const {

    // -- Print some information at the end
    info() << "*** Store this candidate, nbTT = " << goodUT.size << " chi2 " << chi2 << endmsg;
    for ( auto i = 0; i < int( goodUT.size ); i += simd::size ) {
      sF z     = goodUT.z<sF>( i );
      sF mPred = goodUT.x<sF>( i ) + dist;
      info() << goodUT.planeCode<sI>( i ) << format( " z%7.0f  pred %7.2f  diff %7.2f ", z, mPred, dist ) << endmsg;
    }
  }

} // namespace LHCb::Pr
