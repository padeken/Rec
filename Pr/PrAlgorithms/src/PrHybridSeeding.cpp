/*****************************************************************************\
 * (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
 *                                                                             *
 * This software is distributed under the terms of the GNU General Public      *
 * Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
 *                                                                             *
 * In applying this licence, CERN does not waive the privileges and immunities *
 * granted to it by virtue of its status as an Intergovernmental Organization  *
 * or submit itself to any jurisdiction.                                       *
 \*****************************************************************************/
#include "PrHybridSeeding.h"
#include "Event/FTCluster.h"
#include "Event/FTLiteCluster.h"
#include "Event/StateParameters.h"
#include "Event/Track.h"
#include "Kernel/STLExtensions.h" // ASSUME statement
#include "Math/CholeskyDecomp.h"
#include "PrTrackFitterXYZ.h"
#include "PrTrackFitterXZ.h"
#include "PrTrackFitterYZ.h"
#include <range/v3/algorithm/max.hpp>
#include <range/v3/view/transform.hpp>

//-----------------------------------------------------------------------------
// Implementation file for class : PrHybridSeeding
//
// @author Renato Quagliani (rquaglia@cern.ch)
// @author Louis Henry (louis.henry@cern.ch)
// @author Salvatore Aiola (salvatore.aiola@cern.ch)
// @date   2020-03-27
//-----------------------------------------------------------------------------

namespace {
  using namespace ranges;
  // Hit handling/ sorting etc...
  // Uses the reference to the PrHit
  struct compXreverse {
    bool operator()( float lv, float rv ) const { return lv > rv; }
    bool operator()( float lv, const ModPrHit& rhs ) const { return ( *this )( lv, rhs.x() ); }
    bool operator()( const ModPrHit& lhs, const ModPrHit& rhs ) const { return ( *this )( lhs.x(), rhs.x() ); }
  };
  // Uses the coord method for faster access
  struct compCoordX {
    bool operator()( float lv, float rv ) const { return lv < rv; }
    bool operator()( float lv, const ModPrHit& rhs ) const { return ( *this )( lv, rhs.coord ); }
    bool operator()( const ModPrHit& lhs, const ModPrHit& rhs ) const { return ( *this )( lhs.coord, rhs.coord ); }
  };
  struct compCoordXreverse {
    bool operator()( float lv, float rv ) const { return lv > rv; }
    bool operator()( const ModPrHit& lhs, const ModPrHit& rhs ) const { return ( *this )( lhs.coord, rhs.x() ); }
    bool operator()( float lv, const ModPrHit& rhs ) const { return ( *this )( lv, rhs.coord ); }
  };

  //---LoH: currently we cannot use coord as the flagging puts it to float::min -> breaks the logic of that function
  //---LoH: this function looks for the first hit whose x is smaller than the bound
  HitIter get_lowerBound_lin_reverse( const HitIter& low, const HitIter& high, float xMin ) {
    return std::find_if( std::reverse_iterator{high}, std::reverse_iterator{low},
                         [xMin, cmp = compXreverse{}]( const auto& i ) { return cmp( xMin, i ); } )
        .base();
  }
  // Version with a check (we could go to the end)
  void moveLowerBoundX_lin( HitIter& begin, const HitIter& end, float xMin ) {
    while ( begin != end ) {              // technically the first test is useless but it is a bit safer that way
      if ( xMin <= begin->coord ) return; // LoHCoord: will be safe once flag value is set to NaN
      ++begin;
    }
  }
  // Version without check: we know we cannot go to the end
  void moveLowerBoundX_lin( HitIter& begin, float xMin ) {
    while ( xMin > begin->coord ) ++begin; // LoHCoord: will not be safe once flag value is set to NaN.
  }

  // search upperBound from known position to another known position (linear time)
  HitIter get_upperBoundX_lin( const HitIter& begin, const HitIter& end,
                               float xMax ) { //---LoH: 280k times in 100 events
    return std::find_if( begin, end, [xMax, cmp = compCoordX{}]( const auto& i ) { return cmp( xMax, i ); } );
  }

  // Update the bounds (Hiterator Pairs) and old Min => new Min searching linearly around current boundary begin
  void LookAroundMin( IterPairs& bound, float& oMin, const float& nMin, const ModPrHitConstIter& eZone,
                      const ModPrHitConstIter& bZone ) {
    if ( nMin < oMin )                                                      //---LoH: 113k times in 100 events
      bound.begin = get_lowerBound_lin_reverse( bZone, bound.begin, nMin ); // THAT'S THE ONE OFFICER
    else                                                                    //---LoH: 279k times in 100 events
      moveLowerBoundX_lin( bound.begin, eZone, nMin );
    oMin = nMin;
  }

  IterPairs InitializeBB( const unsigned int& ZoneNumber, const PrFTHitHandler<ModPrHit>& FTHitHandler ) noexcept {
    auto r = FTHitHandler.hits( ZoneNumber );
    return {r.begin(), r.end()};
  }

} // namespace

// Clone removal namespace
namespace {
  template <typename T>
  bool areClones( const T& tr1, const T& tr2, const unsigned int& maxCommon ) {
    unsigned int nCommon = maxCommon;
    auto         itH1    = tr1.hits().begin();
    auto         itBeg2  = tr2.hits().begin();
    auto         itEnd1  = tr1.hits().end();
    auto         itEnd2  = tr2.hits().end();
    while ( nCommon != 0 && itH1 != itEnd1 ) {
      for ( auto itH2 = itBeg2; itH2 != itEnd2; ++itH2 )
        if ( itH1->hit->id() == itH2->hit->id() ) {
          --nCommon;
          break;
        }
      ++itH1;
    }
    return ( nCommon == 0 );
  }

  /** @brief Given two tracks it checks if they are clones. Clone threshold is defined by the
      amount of shared hits ( clones if nCommon > maxCommon)
  */
  template <typename T>
  bool removeWorstTrack( T& t1, T& t2 ) {
    bool ret = !T::LowerBySize( t1, t2 );
    if ( !ret )
      t2.setValid( false );
    else
      t1.setValid( false );
    return ret;
  }

  /** @brief Check if two tracks are passing through T1/T2/T3 at a distance less than "distance".
   *  @param tr1 First track in comparison
   *  @param tr2 Second track in comparison
   *  @param distance Distance to check if two tracks are close one to anohter
   *  @return bool Success, tracks are close enough
   */
  bool CloseEnough( const Pr::Hybrid::AbsSeedTrack& tr1, const Pr::Hybrid::AbsSeedTrack& tr2, const float& distance ) {
    return ( std::fabs( tr1.xT1() - tr2.xT1() ) < distance || std::fabs( tr1.ax() - tr2.ax() ) < distance ||
             std::fabs( tr1.xT3() - tr2.xT3() ) < distance );
  }

  // Removes the worst of two tracks
  template <typename CommonHits, typename T>
  struct removeClonesT {
    void operator()( unsigned int part, T& candidates, CommonHits commonHits = CommonHits() ) {
      constexpr float minDist = 2.f;
      //---LoH: it is surely faster to do by a simpler alg
      for ( auto itT1 = candidates[part].begin(); itT1 < candidates[part].end() - 1; ++itT1 ) {
        if ( !( *itT1 ).valid() ) continue;
        for ( auto itT2 = itT1 + 1; itT2 < candidates[part].end(); ++itT2 ) {
          if ( !( *itT2 ).valid() ) continue;
          if ( !CloseEnough( ( *itT1 ), ( *itT2 ), minDist ) ) continue;
          if ( areClones( ( *itT1 ), ( *itT2 ), commonHits( itT1->size(), itT2->size() ) ) ) {
            if ( removeWorstTrack( *itT1, *itT2 ) ) break;
          }
        }
      }
    }
  };

  auto removeClonesX = removeClonesT<PrHybridSeeding::CommonXHits, XCandidates>();
  auto removeClones  = removeClonesT<PrHybridSeeding::CommonXYHits, TrackCandidates>();

  // Remove tracks to recover
  void removeClonesRecover( unsigned int part, TrackToRecover& trackToRecover, XCandidates& xCandidates,
                            PrHybridSeeding::CommonRecoverHits commonHits = PrHybridSeeding::CommonRecoverHits() ) {
    constexpr float minDist = 5.f;
    for ( auto itT1 = trackToRecover[part].begin(); itT1 != trackToRecover[part].end(); ++itT1 ) {
      if ( !( *itT1 ).recovered() ) continue;
      for ( auto itT2 = itT1 + 1; itT2 != trackToRecover[part].end(); ++itT2 ) {
        if ( !( *itT2 ).recovered() ) continue;
        if ( !CloseEnough( ( *itT1 ), ( *itT2 ), minDist ) ) continue;
        if ( areClones( ( *itT1 ), ( *itT2 ), commonHits( itT1->size(), itT2->size() ) ) ) {
          if ( Pr::Hybrid::SeedTrackX::LowerBySize( *itT1, *itT2 ) )
            itT2->setRecovered( false );
          else {
            itT1->setRecovered( false );
            break;
          }
        }
      }
      if ( ( *itT1 ).recovered() ) { xCandidates[int( part )].push_back( ( *itT1 ) ); }
    }
  }

  inline size_t nUsed( Pr::Hybrid::SeedTrackHitsConstIter itBeg, Pr::Hybrid::SeedTrackHitsConstIter itEnd,
                       const PrFTHitHandler<ModPrHit>& hitHandler ) {
    auto condition = [&hitHandler]( const ModPrHit& h ) { return !hitHandler.hit( h.hitIndex ).isValid(); };
    return std::count_if( itBeg, itEnd, condition );
  }

  template <typename Range>
  static bool hasT1T2T3Track( const Range& hits ) noexcept {
    std::bitset<3> T{};
    for ( const auto& hit : hits ) {
      int planeBit = hit.pc() / 4;
      ASSUME( planeBit < 3 );
      T[planeBit] = true;
      if ( T.all() ) return true;
    }
    return false;
  }

} // namespace

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( PrHybridSeeding )
//=============================================================================
// Standard constructor, initializes variable
//=============================================================================

PrHybridSeeding::PrHybridSeeding( const std::string& name, ISvcLocator* pSvcLocator )
    : Transformer( name, pSvcLocator, KeyValue{"FTHitsLocation", PrFTInfo::FTHitsLocation},
                   KeyValue{"OutputName", LHCb::TrackLocation::Seed} ) {}

//=============================================================================
// Initialization
//=============================================================================
StatusCode PrHybridSeeding::initialize() {
  auto sc = Transformer::initialize();
  if ( sc.isFailure() ) return sc; // error printed already by GaudiAlgorithm

  if constexpr ( NCases > 3 ) {
    error() << "Algorithm does not support more than 3 Cases" << endmsg;
    return StatusCode::FAILURE;
  }
  if ( m_minTot[RecoverCase] < 9u ) {
    error() << "Algorithm does not support fewer than 9 hits in total (due to addStereo)" << endmsg;
    return StatusCode::FAILURE;
  }
  for ( unsigned int i = 0; i < NCases; ++i )
    if ( m_minTot[i] < 9 ) {
      error() << "Algorithm does not support fewer than 9 hits in total (due to addStereo)" << endmsg;
      return StatusCode::FAILURE;
    }

  // Zones cache, retrieved from the detector store
  registerCondition<PrHybridSeeding>( PrFTInfo::FTZonesLocation, m_zoneHandler );

  //---LoH: UV zones
  m_dxDy[0] = m_zoneHandler->zone( T1U ).dxDy(); // u-layers
  m_dxDy[1] = m_zoneHandler->zone( T1V ).dxDy(); // v-layers
  for ( auto layer : {T1X1, T1X2, T2X1, T2X2, T3X1, T3X2, T1U, T1V, T2U, T2V, T3U, T3V} )
    for ( auto part : {0, 1} ) {
      m_z[layer - part]         = m_zoneHandler->zone( layer - part ).z();
      m_planeCode[layer - part] = m_zoneHandler->zone( layer - part ).planeCode();
    }

  //---LoH: Precalculate y borders
  m_yMins[0][0] = m_yMin;
  m_yMins[0][1] = m_yMin_TrFix;
  m_yMins[1][0] = -m_yMax_TrFix;
  m_yMins[1][1] = -std::fabs( m_yMax );

  m_yMaxs[0][0] = std::fabs( m_yMax );
  m_yMaxs[0][1] = m_yMax_TrFix;
  m_yMaxs[1][0] = -m_yMin_TrFix;
  m_yMaxs[1][1] = -m_yMin; // positive

  // Create the m_minUV here
  m_minUV[0][0] = m_minUV6[0];
  m_minUV[0][1] = m_minUV5[0];
  m_minUV[0][2] = m_minUV4[0];
  m_minUV[1][0] = m_minUV6[1];
  m_minUV[1][1] = m_minUV5[1];
  m_minUV[1][2] = m_minUV4[1];
  m_minUV[2][0] = m_minUV6[2];
  m_minUV[2][1] = m_minUV5[2];
  m_minUV[2][2] = m_minUV4[2];
  m_minUV[3][0] = m_recover_minUV[0];
  m_minUV[3][1] = m_recover_minUV[1];
  m_minUV[3][2] = m_recover_minUV[2];

  return StatusCode::SUCCESS;
}

//=============================================================================
// Main execution
//=============================================================================
std::vector<PrHybridSeeding::Track> PrHybridSeeding::operator()( const PrFTHitHandler<PrHit>& FTHitHandler ) const {
  // Containers
  TrackCandidates trackCandidates; //---LoH: probably could be changed to array(static_vector)
  XCandidates     xCandidates;     //---LoH: probably could be changed to static_vector
  TrackToRecover  trackToRecover;  //---LoH: probably could be changed to static_vector

  trackCandidates[0].reserve( maxCandidates );
  trackCandidates[1].reserve( maxCandidates );
  xCandidates[0].reserve( maxXCandidates );
  xCandidates[1].reserve( maxXCandidates );
  trackToRecover[0].reserve( maxXCandidates );
  trackToRecover[1].reserve( maxXCandidates );

  //==========================================================
  // Hits are ready to be processed
  //==========================================================
  PrFTHitHandler<ModPrHit> hitHandler = makeHitHandler( FTHitHandler );
  //========================================================
  //------------------MAIN SEQUENCE IS HERE-----------------
  //========================================================
  auto main_loop = [&]( auto icase ) {
    //----- Loop through lower and upper half
    for ( unsigned int part = 0; 2 > part; ++part ) {
      xCandidates[part].clear(); // x candidates up cleaned every Case!
      findXProjections( part, icase, hitHandler, xCandidates );
      if ( m_removeClonesX ) {
        removeClonesX( part, xCandidates );
      } //---LoH: this probably should be encoded in the logic. Clone tracks are close to each other.
      addStereo<icase> addStereoCase( *this, trackToRecover );
      addStereoCase( part, hitHandler, trackCandidates, xCandidates );
      // Flag found Hits at the end of each single case ( exclude the latest one )
      if ( ( icase + 1 < NCases ) ) flagHits( icase, part, trackCandidates, hitHandler );
    }
  };
  static_for<NCases>( main_loop );

  // Recovering step
  if ( m_recover ) {
    xCandidates[0].clear();
    xCandidates[1].clear();
    RecoverTrack( hitHandler, trackCandidates, xCandidates, trackToRecover );
  }

  // Clone removal ( up/down )
  for ( unsigned int part = 0; part < 2; ++part ) { removeClones( part, trackCandidates ); }

  std::vector<Track> result;
  result.reserve( trackCandidates[0].size() + trackCandidates[1].size() );
  // Convert LHCb tracks
  for ( unsigned int part = 0; part < 2; ++part ) { makeLHCbTracks( result, part, trackCandidates ); }
  return result;
}

//---LoH: Can be put outside of the classx
PrFTHitHandler<ModPrHit> PrHybridSeeding::makeHitHandler( const PrFTHitHandler<PrHit>& FTHitHandler ) const noexcept {
  // Construct hit handler of ModPrHits
  // The track candidates will contain copies of the ModHits, but each will contain their
  // own index in the hit container, which can be used used to flag the original hits.
  ModPrHits hits;
  hits.reserve( FTHitHandler.hits().size() );
  size_t i = 0;
  for ( const auto& hit : FTHitHandler.hits().range() ) {
    hits.emplace_back( &hit, hit.x(), i );
    ++i;
  }
  return {std::move( hits ), FTHitHandler.hits().offsets()};
}

//===========================================================================================================================
// EVERYTHING RELATED TO UV-HIT ADDITION
//===========================================================================================================================

template <int CASE>
template <int C>
PrHybridSeeding::addStereo<CASE>::addStereo( const PrHybridSeeding& hybridSeeding,
                                             typename std::enable_if<C == RecoverCase>::type* )
    : addStereoBase<true>(), m_hybridSeeding( hybridSeeding ) {}

template <int CASE>
template <int C>
PrHybridSeeding::addStereo<CASE>::addStereo( const PrHybridSeeding& hybridSeeding, TrackToRecover& trackToRecover,
                                             typename std::enable_if<C != RecoverCase>::type* )
    : addStereoBase<false>( trackToRecover ), m_hybridSeeding( hybridSeeding ) {}

template <int CASE>
void PrHybridSeeding::addStereo<CASE>::operator()( unsigned int part, const PrFTHitHandler<ModPrHit>& FTHitHandler,
                                                   TrackCandidates& trackCandidates, XCandidates& xCandidates ) const
    noexcept {
  // Initialise bounds
  BoundariesUV                                       Bounds;
  BoundariesUV                                       borderZones;
  std::array<std::array<std::array<float, 2>, 3>, 2> xMinPrev;
  initializeUVBounds( FTHitHandler, Bounds, borderZones, xMinPrev );

  //---LoH: Loop on the xCandidates
  //---LoH: They are not sorted by xProje and their order can vary
  for ( auto&& itT : xCandidates[part] ) {
    if ( !itT.valid() ) continue;
    auto            hough = initializeHoughSearch( part, itT, borderZones, Bounds, xMinPrev );
    HoughCandidates houghCand;
    bool            hasAdded      = false;
    auto            lastCandidate = hough.search( houghCand.begin() );
    int             n_cand        = lastCandidate - houghCand.begin();
    if ( n_cand > 0 ) {
      // you have a minimal number of total hits to find on track dependent if standard 3 cases or from Recover routine
      hasAdded = createTracksFromHough( trackCandidates[part], itT, houghCand, n_cand );
    }
    if constexpr ( CASE != RecoverCase ) {
      if ( !hasAdded ) this->m_trackToRecover[part].push_back( itT );
    }
  }
}

//---LoH: Can be put outside of class
template <int CASE>
void PrHybridSeeding::addStereo<CASE>::initializeUVBounds(
    const PrFTHitHandler<ModPrHit>& FTHitHandler, BoundariesUV& Bounds, BoundariesUV& borderZones,
    std::array<std::array<std::array<float, 2>, 3>, 2>& xMinPrev ) const noexcept {
  for ( const unsigned int& layer : {T1U, T2U, T3U, T1V, T2V, T3V} ) {
    auto uv       = ( ( layer + 1 ) / 2 ) % 2;
    auto iStation = layer / 8;
    for ( unsigned int iPart = 0; iPart < 2; ++iPart ) {
      // Fill the UV bounds
      Bounds[uv][iStation][iPart]      = InitializeBB( layer - iPart, FTHitHandler );
      auto r                           = FTHitHandler.hits( layer - iPart );
      borderZones[uv][iStation][iPart] = {r.begin(), r.end()};
      xMinPrev[uv][iStation][iPart]    = std::numeric_limits<float>::lowest();
    }
  }
}

//---LoH: Can be put outside of class if takes the seeding as argument.
template <int CASE>
PrHybridSeeding::HoughSearch PrHybridSeeding::addStereo<CASE>::initializeHoughSearch(
    unsigned int part, const Pr::Hybrid::SeedTrackX& xProje, const BoundariesUV& borderZones, BoundariesUV& Bounds,
    std::array<std::array<std::array<float, 2>, 3>, 2>& xMinPrev ) const noexcept {

  auto calculate_factor = [this, shift = 0.f,
                           partSign = float( 1 - static_cast<int>( part ) * 2 )]( int layer ) -> float {
    auto UV = layer / 4 % 2;
    return partSign / ( ( m_hybridSeeding.m_z[layer] - shift ) * m_hybridSeeding.m_dxDy[UV] );
  };
  auto calculate_factors = [&]( auto... layers ) { return std::array{calculate_factor( layers )...}; };

  auto hough = HoughSearch{2,
                           -m_hybridSeeding.m_YSlopeBinWidth[CASE] / 2,
                           m_hybridSeeding.m_YSlopeBinWidth[CASE],
                           {calculate_factors( T1U, T1V, T2U, T2V, T3U, T3V )}};
  // u-layers
  for ( const unsigned int& layer : {T1U, T2U, T3U} )
    this->CollectLayerUV<0>( part, layer, xProje, borderZones[0], Bounds[0], xMinPrev[0], hough );
  // v-layers
  for ( const unsigned int& layer : {T1V, T2V, T3V} )
    this->CollectLayerUV<1>( part, layer, xProje, borderZones[1], Bounds[1], xMinPrev[1], hough );
  return hough;
}

template <int CASE>
template <int UV> // 0 if U, 1 if V
void PrHybridSeeding::addStereo<CASE>::CollectLayerUV( unsigned int part, unsigned int layer,
                                                       const Pr::Hybrid::SeedTrackX& xProje,
                                                       const BoundaryUV& borderZones, BoundaryUV& Bounds,
                                                       std::array<std::array<float, 2>, 3>& xMinPrev,
                                                       HoughSearch&                         hough ) const noexcept {
  static_assert( UV == 0 || UV == 1, "UV must be 0 (U) or 1 (V)!" );
  float zPlane = m_hybridSeeding.m_z[layer];
  float dxDy   = m_hybridSeeding.m_dxDy[UV]; // 0 if U, 1 if V
  float xPred  = xProje.x( zPlane );
  // Note: UV layers are 1, 3, 11, 13, 19, 21
  unsigned int iStation = layer / 8;
  unsigned int iLayer   = layer / 4;

  // Part 0
  std::array<float, 2> yMinMax{m_hybridSeeding.m_yMins[part][0], m_hybridSeeding.m_yMaxs[part][0]};
  // The 1 - UV expression will be optmized away by the compiler (const expression)
  std::array<float, 2> xMinMax{xPred - yMinMax[1 - UV] * dxDy, xPred - yMinMax[UV] * dxDy};
  //---LoH: in most cases (~70%), xMinMax is larger than xMinPrev. This is the same proportion between U and V
  LookAroundMin( Bounds[iStation][0], xMinPrev[iStation][0], xMinMax[0], borderZones[iStation][0].end,
                 borderZones[iStation][0].begin );
  //  Bounds[iStation][0].end = get_upperBoundX_lin( Bounds[iStation][0].begin, borderZones[iStation][0].end, xMinMax[1]
  //  ); for ( auto itH = Bounds[iStation][0].begin; itH != Bounds[iStation][0].end; ++itH )
  for ( Bounds[iStation][0].end = Bounds[iStation][0].begin; Bounds[iStation][0].end != borderZones[iStation][0].end;
        ++Bounds[iStation][0].end ) {
    if ( Bounds[iStation][0].end->coord > xMinMax[1] ) break;
    AddUVHit( iLayer, Bounds[iStation][0].end, xPred, hough );
  }
  // Part 1
  yMinMax = {m_hybridSeeding.m_yMins[part][1], m_hybridSeeding.m_yMaxs[part][1]};
  // The 1 - UV expression will be optmized away by the compiler (const expression)
  xMinMax = {xPred - yMinMax[1 - UV] * dxDy, xPred - yMinMax[UV] * dxDy};
  LookAroundMin( Bounds[iStation][1], xMinPrev[iStation][1], xMinMax[0], borderZones[iStation][1].end,
                 borderZones[iStation][1].begin );
  for ( Bounds[iStation][1].end = Bounds[iStation][1].begin; Bounds[iStation][1].end != borderZones[iStation][1].end;
        ++Bounds[iStation][1].end ) {
    if ( Bounds[iStation][1].end->coord > xMinMax[1] ) break;
    AddUVHit( iLayer, Bounds[iStation][1].end, xPred, hough );
  }
}

template <int CASE>
void PrHybridSeeding::addStereo<CASE>::AddUVHit( unsigned int layer, const ModPrHitConstIter& it, const float& xPred,
                                                 HoughSearch& hough ) const noexcept {
  if ( !it->isValid() ) return;
  float y = xPred - it->coord;
  hough.add( layer, y, &( *it ) );
}

template <int CASE>
void PrHybridSeeding::addStereo<CASE>::createFullTrack( std::vector<Pr::Hybrid::SeedTrack>& trackCandidates,
                                                        const Pr::Hybrid::SeedTrackX&       xCandidate,
                                                        HoughCandidateHitIter itBeg, HoughCandidateHitIter itEnd,
                                                        unsigned int& nTarget, float& chi2Target,
                                                        unsigned int& nValidCandidates ) const noexcept {
  constexpr int      LOW_NTOT_YSEL_TH = CASE == 2 ? 10 : 11;
  constexpr int      LOW_NTOT_TH      = 11;
  constexpr int      LOW_NUV_TH       = CASE == 2 ? 5 : 6;
  constexpr int      FITTER_XYZ_LOOPS = CASE == 0 ? 2 : 3;
  const unsigned int nXHits           = xCandidate.size();
  const unsigned int minUV            = m_hybridSeeding.m_minUV[CASE][6 - nXHits];

  unsigned int nUVHits = itEnd - itBeg;
  unsigned int nHits   = nXHits + nUVHits;

  if ( nHits < nTarget || nUVHits < minUV ) return;

  PrTrackFitterYZ fitterYZ( xCandidate );
  bool            fitY = fitterYZ.fit( itBeg, itEnd );
  if ( !fitY ) return;

  auto worstHitY     = removeWorstY( fitterYZ, itBeg, itEnd );
  bool canRemoveHitY = nHits > nTarget && nUVHits > m_hybridSeeding.m_minUV[CASE][6 - nXHits];
  while ( worstHitY != itEnd ) {
    if ( !canRemoveHitY ) return;
    --itEnd;
    std::copy( worstHitY + 1, itEnd + 1, worstHitY );
    *itEnd = nullptr;
    --nHits;
    --nUVHits;
    fitY = fitterYZ.fit( itBeg, itEnd );
    if ( !fitY ) return;
    worstHitY     = removeWorstY( fitterYZ, itBeg, itEnd );
    canRemoveHitY = nHits > nTarget && nUVHits > m_hybridSeeding.m_minUV[CASE][6 - nXHits];
  }

  if ( ( nUVHits >= LOW_NUV_TH && fitterYZ.chi2PerDoF() > m_hybridSeeding.m_maxChi2PerDofYHigh[CASE] ) ||
       ( nUVHits < LOW_NUV_TH && fitterYZ.chi2PerDoF() > m_hybridSeeding.m_maxChi2PerDofYLow[CASE] ) )
    return;

  Pr::Hybrid::SeedTrack trackCand( xCandidate );
  for ( auto hit = itBeg; hit != itEnd; ++hit ) trackCand.hits().push_back( *( *hit ) );
  trackCand.setYParam( fitterYZ.ay(), fitterYZ.by() );

  PrTrackFitterXYZ<FITTER_XYZ_LOOPS> fitterXYZ;
  bool                               fitXY = fitterXYZ.fit( trackCand, m_hybridSeeding.m_dRatioPar.value() );
  if ( !fitXY ) return;

  auto worstHit = removeWorstXY( trackCand );
  while ( worstHit != trackCand.hits().cend() ) {
    if ( worstHit->hit->isX() ) {
      if ( !canRemoveHitY ) return;
    } else {
      if ( !canRemoveHitY ) return;
      --nUVHits;
    }
    --nHits;
    trackCand.hits().erase( worstHit );
    fitXY = fitterXYZ.fit( trackCand, m_hybridSeeding.m_dRatioPar.value() );
    if ( !fitXY ) return;
    worstHit      = removeWorstXY( trackCand );
    canRemoveHitY = nHits > nTarget && nUVHits > m_hybridSeeding.m_minUV[CASE][6 - nHits + nUVHits];
  }

  if ( nHits > nTarget ) {
    if ( nHits >= LOW_NTOT_TH ) {
      if ( trackCand.chi2PerDoF() > m_hybridSeeding.m_maxChi2PerDofFullHigh[CASE] ) return;
    } else {
      if ( trackCand.chi2PerDoF() > m_hybridSeeding.m_maxChi2PerDofFullLow[CASE] ) return;
    }
  } else {
    if ( trackCand.chi2PerDoF() > chi2Target ) return;
  }

  float absY0   = std::fabs( trackCand.y0() );
  float absYref = std::fabs( trackCand.yRef() );
  if ( nHits < LOW_NTOT_YSEL_TH ) {
    if ( absY0 >= m_hybridSeeding.m_maxYAt0Low[CASE] || absYref >= m_hybridSeeding.m_maxYAtRefLow[CASE] ||
         absYref < m_hybridSeeding.m_minYAtRefLow[CASE] )
      return;
  } else {
    if ( absY0 >= m_hybridSeeding.m_maxYAt0High[CASE] || absYref >= m_hybridSeeding.m_maxYAtRefHigh[CASE] ||
         absYref < m_hybridSeeding.m_minYAtRefHigh[CASE] )
      return;
  }

  trackCand.setnXnY( nHits - nUVHits, nUVHits );
  trackCand.setXT1( trackCand.x( StateParameters::ZBegT ) );
  trackCand.setXT3( trackCand.x( StateParameters::ZEndT ) );

  if ( nValidCandidates == 0 ) {
    trackCandidates.push_back( trackCand );
  } else {
    trackCandidates.back() = trackCand;
  }
  ++nValidCandidates;
  nTarget    = trackCandidates.back().size();
  chi2Target = trackCandidates.back().chi2PerDoF();
}

template <int CASE>
bool PrHybridSeeding::addStereo<CASE>::createTracksFromHough( std::vector<Pr::Hybrid::SeedTrack>& trackCandidates,
                                                              const Pr::Hybrid::SeedTrackX&       xCandidate,
                                                              HoughCandidates& houghCand, size_t nHoughCand ) const
    noexcept {
  unsigned int nValidCandidates = 0;
  unsigned int nTarget          = m_hybridSeeding.m_minTot[CASE];
  float        chi2Target       = m_hybridSeeding.m_maxChi2PerDofFullLow[CASE];
  for ( size_t iCand = 0; iCand < nHoughCand; ++iCand ) {
    auto& hcand = houghCand[iCand];
    auto  itBeg = hcand.begin();
    auto  itEnd = hcand.end();
    while ( ( itEnd - itBeg ) > 4 && *( itEnd - 1 ) == nullptr ) --itEnd;
    if ( *( itEnd - 1 ) == nullptr ) continue;
    createFullTrack( trackCandidates, xCandidate, itBeg, itEnd, nTarget, chi2Target, nValidCandidates );
  }
  return ( nValidCandidates != 0 );
}

template <int CASE>
PrHybridSeeding::HoughCandidateHitIter
PrHybridSeeding::addStereo<CASE>::removeWorstY( const PrTrackFitterYZ& fitterYZ, HoughCandidateHitIter& hitBegin,
                                                HoughCandidateHitIter& hitEnd ) const noexcept {
  const auto worst_it =
      std::max_element( fitterYZ.chi2Hits().cbegin(), fitterYZ.chi2Hits().cbegin() + ( hitEnd - hitBegin ) );
  auto worst_pos = worst_it - fitterYZ.chi2Hits().cbegin();
  if ( fitterYZ.chi2PerDoF() < m_hybridSeeding.m_minChi2PerDofYRemove[CASE] &&
       fitterYZ.chi2Hits()[worst_pos] < m_hybridSeeding.m_minChi2HitYRemove[CASE] ) {
    return hitEnd;
  } else {
    return hitBegin + worst_pos;
  }
}

template <int CASE>
PrHybridSeeding::SeedTrackHitsConstIter
PrHybridSeeding::addStereo<CASE>::removeWorstXY( const Pr::Hybrid::SeedTrack& track ) const noexcept {
  const auto worst_it  = std::max_element( track.chi2Hits().cbegin(), track.chi2Hits().cbegin() + track.size() );
  auto       worst_pos = worst_it - track.chi2Hits().cbegin();
  if ( track.chi2PerDoF() < m_hybridSeeding.m_minChi2PerDofFullRemove[CASE] &&
       track.chi2Hits()[worst_pos] < m_hybridSeeding.m_minChi2HitFullRemove[CASE] ) {
    return track.hits().cend();
  } else {
    return track.hits().cbegin() + worst_pos;
  }
}

void PrHybridSeeding::RecoverTrack( PrFTHitHandler<ModPrHit>& FTHitHandler, TrackCandidates& trackCandidates,
                                    XCandidates& xCandidates, TrackToRecover& trackToRecover ) const noexcept {
  // Flagging all hits in the track candidates
  for ( int part = 0; part < 2; ++part ) {
    for ( auto&& itT1 : trackCandidates[part] ) {
      if ( !itT1.valid() ) continue;
      for ( auto&& hit : itT1.hits() ) { FTHitHandler.hit( hit.hitIndex ).setInvalid(); }
    }
  }

  for ( unsigned int part = 0; part < 2; ++part ) {
    for ( auto&& itT1 : trackToRecover[part] ) {
      int nUsed_threshold = m_nusedthreshold[6 - itT1.size()]; // LoH
      //---LoH: this should be asserted: size must be 4, 5 or 6
      //---LoH: this checks if we have enough valid hits.
      int nUsedHits = nUsed( itT1.hits().begin(), itT1.hits().end(), FTHitHandler );
      if ( nUsedHits < nUsed_threshold ) {
        if ( nUsedHits > 0 ) {
          while ( nUsedHits > 0 ) {
            auto it = std::remove_if( itT1.hits().begin(), itT1.hits().end(), [&FTHitHandler]( const ModPrHit& hit ) {
              return !FTHitHandler.hit( hit.hitIndex ).isValid();
            } );
            auto n  = std::distance( it, itT1.hits().end() );
            itT1.hits().erase( it, itT1.hits().end() );
            nUsedHits -= n;
          }
          if ( itT1.hits().size() < m_minXPlanes ) continue;
          if ( hasT1T2T3Track( itT1.hits() ) ) continue;
          PrTrackFitterXZ fitter;
          bool            fitX = fitter.fit( itT1 );
          if ( fitX ) {
            auto worstHitX = removeWorstX( itT1, 2 );
            if ( worstHitX == itT1.hits().end() ) { itT1.setRecovered( true ); }
          }
        } else {
          itT1.setRecovered( true );
        }
      }
    }
    removeClonesRecover( part, trackToRecover, xCandidates );
    addStereo<RecoverCase> addStereoReco( *this );
    addStereoReco( part, FTHitHandler, trackCandidates, xCandidates );
  }
}

void PrHybridSeeding::flagHits( unsigned int icase, unsigned int part, TrackCandidates& trackCandidates,
                                PrFTHitHandler<ModPrHit>& hitHandler ) const noexcept {
  // The hits in the track candidates are copies, but they each contain their own index
  // in the original hit container, which is used to flag the original hits.
  for ( auto& track : trackCandidates[part] ) {
    if ( !track.valid() ) continue;
    if ( ( track.size() == 12 ) || ( ( track.size() == 11 && track.chi2PerDoF() < m_MaxChi2Flag[icase] &&
                                       std::fabs( track.ax() - track.bx() * Pr::Hybrid::zReference +
                                                  track.cx() * m_ConstC ) < m_MaxX0Flag[icase] ) ) )
      for ( const auto& hit : track.hits() ) {
        if ( static_cast<int>( part ) == hit.hit->zone() % 2 ) continue; // hit.hit->zone() is signed int
        hitHandler.hit( hit.hitIndex ).setInvalid();
      }
  }
}

//=========================================================================
//  Convert to LHCb tracks
//=========================================================================
//---LoH: there are some things to understand, such as:
// - does the scaleFactor or the momentumScale change? If not, momentumScale/(-scaleFactor) is much better.
void PrHybridSeeding::makeLHCbTracks( std::vector<Track>& result, unsigned int part,
                                      const TrackCandidates& trackCandidates ) const noexcept {
  for ( const auto& track : trackCandidates[part] ) {
    if ( !track.valid() ) continue;
    //***** EXACT SAME IMPLEMENTATION OF PatSeedingTool *****//
    auto& out = result.emplace_back();
    out.setType( Track::Type::Ttrack );
    out.setHistory( Track::History::PrSeeding );
    out.setPatRecStatus( Track::PatRecStatus::PatRecIDs );
    for ( const auto& hit : track.hits() ) { out.addToLhcbIDs( hit.hit->id() ); }
    LHCb::State temp( Gaudi::TrackVector( track.ax(), track.yRef(), track.xSlope0(), //---LoH: can be simplified into
                                                                                     // ax(), ay()
                                          track.ySlope(), 0.f ),
                      Pr::Hybrid::zReference, LHCb::State::Location::AtT );
    double      qOverP, sigmaQOverP;
    const float scaleFactor = m_magFieldSvc->signedRelativeCurrent();
    if ( m_momentumTool->calculate( &temp, qOverP, sigmaQOverP, true ).isFailure() ) {
      if ( std::fabs( scaleFactor ) < 1.e-4f ) {
        qOverP = ( ( track.cx() < 0.f ) ? -1.f : 1.f ) * ( ( scaleFactor < 0.f ) ? -1.f : 1.f ) / Gaudi::Units::GeV;
        sigmaQOverP = 1.f / Gaudi::Units::MeV;
      } else {
        qOverP      = track.cx() * m_momentumScale / ( -1.f * scaleFactor );
        sigmaQOverP = 0.5f * qOverP;
      }
    }
    temp.setQOverP( qOverP );
    Gaudi::TrackSymMatrix& cov = temp.covariance();
    cov( 0, 0 )                = m_stateErrorX2;
    cov( 1, 1 )                = m_stateErrorY2;
    cov( 2, 2 )                = m_stateErrorTX2;
    cov( 3, 3 )                = m_stateErrorTY2;
    cov( 4, 4 )                = sigmaQOverP * sigmaQOverP;
    for ( const float z : m_zOutputs ) {
      temp.setX( track.x( z ) );
      temp.setY( track.y( z ) );
      temp.setZ( z );
      temp.setTx( track.xSlope( z ) );
      temp.setTy( track.ySlope() );
      out.addToStates( temp );
    }
    out.setChi2PerDoF( {track.chi2PerDoF(), static_cast<int>( track.hits().size() ) - 5} );
  }
}

//=================================================================================================

//=========================================================================
//  Finding the X projections. This is the most time-consuming part of the algorithm
//=========================================================================

void PrHybridSeeding::findXProjections( unsigned int part, unsigned int iCase,
                                        const PrFTHitHandler<ModPrHit>& FTHitHandler, XCandidates& xCandidates ) const
    noexcept {
  CaseGeomInfo xZones = {0.f, 0.f, {}, {}, {}, {}, {}, 0.f, 0.f, 0.f, 0.f, 0.f}; // zones, planeCodes, z, dz, dz2
  initializeXProjections( iCase, part, xZones );

  float slope = ( m_tolAtX0Cut[iCase] - m_tolX0SameSign[iCase] ) / ( m_x0Cut[iCase] - m_x0SlopeChange[iCase] );
  float slopeopp =
      ( m_tolAtx0CutOppSign[iCase] - m_tolX0OppSign[iCase] ) / ( m_x0Cut[iCase] - m_x0SlopeChange2[iCase] );
  float accTerm1 = slope * m_x0SlopeChange[iCase] - m_tolX0SameSign[iCase];
  float accTerm2 = slopeopp * m_x0SlopeChange2[iCase] - m_tolX0OppSign[iCase];

  //======================================================================================================================================================
  Boundaries Bounds;
  Boundaries tempBounds;
  //============Initialization
  std::array<HitIter, PrFTInfo::Numbers::NFTXLayers> begZones;
  std::array<HitIter, PrFTInfo::Numbers::NFTXLayers> endZones;
  std::array<float, PrFTInfo::Numbers::NFTXLayers>   lastHitX;
  // Always: first=0 last=1 middles=2,3 remains=4,5 (order of loops)
  for ( unsigned int i = 0; i < PrFTInfo::Numbers::NFTXLayers; ++i ) {
    Bounds[i]     = InitializeBB( xZones.zones[i], FTHitHandler );
    tempBounds[i] = InitializeBB( xZones.zones[i], FTHitHandler );
    auto r        = FTHitHandler.hits( xZones.zones[i] );
    begZones[i]   = r.begin();
    endZones[i]   = r.end();
    // Find the last hit that is not invalid
    for ( auto hit = endZones[i]; hit > begZones[i]; ) {
      --hit;
      if ( hit->isValid() ) {
        lastHitX[i] = ( hit )->coord;
        break;
      }
    }
  }
  for ( unsigned int i = 1; i < PrFTInfo::Numbers::NFTXLayers; ++i ) {
    Bounds[i].end     = Bounds[i].begin;
    tempBounds[i].end = tempBounds[i].begin;
  }
  ModPrHitConstIter Fhit, Lhit;
  // Used to cache the position to do a "look-around search"
  std::array<float, PrFTInfo::Numbers::NFTXLayers> xminPrev;
  xminPrev.fill( std::numeric_limits<float>::lowest() );

  std::array<SeedTrackParabolaHits, 2> parabolaSeedHits; // 0: layer T2x1, 1: layer T2x2, 2: extrapolated T2x2
  parabolaSeedHits[0].reserve( PrHybridSeeding::maxParabolaHits );
  parabolaSeedHits[1].reserve( PrHybridSeeding::maxParabolaHits );
  TwoHitCombination hitComb;
  bool  OK, first; //---LoH: good practice woul be to declare them in the loop but it slows a bit the program
  float tolHp = m_TolFirstLast[iCase];
  for ( ; Bounds[0].begin != Bounds[0].end; ++Bounds[0].begin ) // for a hit in first layer
  {
    Fhit = Bounds[0].begin;
    if ( !Fhit->isValid() ) continue;
    float xFirst    = Fhit->coord;
    float tx_inf    = xFirst * xZones.invZf;
    float xProjeInf = tx_inf * xZones.zLays[1];
    float maxXl     = xProjeInf + tx_inf * m_alphaCorrection[iCase] + tolHp;
    float minXl     = maxXl - 2 * tolHp;
    // Setting the last-layer bounds
    moveLowerBoundX_lin( Bounds[1].begin, endZones[1], minXl );               //---LoH: should be very small
    Bounds[1].end = get_upperBoundX_lin( Bounds[1].end, endZones[1], maxXl ); //---LoH: between 6 and 20 times roughly
    // Reinitialise the parabola seed bounds to the highest previous point
    tempBounds[2].begin = Bounds[2].begin;
    tempBounds[3].begin = Bounds[3].begin;
    first               = true;
    for ( Lhit = Bounds[1].begin; Lhit != Bounds[1].end; ++Lhit ) { // for a hit in last layer
      if ( !Lhit->isValid() ) continue;
      //---LoH: For 100 events, this is ran 1.6M times (number of updateXZCombination)
      float xLast = Lhit->coord;
      updateXZCombinationPars( iCase, xFirst, xLast, xZones, slope, slopeopp, accTerm1, accTerm2,
                               hitComb ); // New parameters
      parabolaSeedHits[0].clear();
      parabolaSeedHits[1].clear();
      // Look for parabola hits and update bounds
      float xProjectedCorrected = xZones.zLays[2] * hitComb.tx + hitComb.x0new; // Target
      float xMin                = xProjectedCorrected + hitComb.minPara;        // may add a 1.0 mm here?
      float xMax                = xProjectedCorrected + hitComb.maxPara;        // may add a 1.0 mm here?
      OK = findParabolaHits( 2, xMin, xMax, endZones, tempBounds, parabolaSeedHits[0], lastHitX[2] );
      xMin += hitComb.tx * xZones.t2Dist;
      xMax += hitComb.tx * xZones.t2Dist;
      OK += findParabolaHits( 3, xMin, xMax, endZones, tempBounds, parabolaSeedHits[1], lastHitX[3] );
      if ( first ) {
        Bounds[2].begin = tempBounds[2].begin;
        Bounds[3].begin = tempBounds[3].begin;
        first           = false;
      }
      if ( !OK ) continue;
      //---LoH: Technically it would be possible to ensure that the remaining hits are looked for in a sorted way
      //---LoH: If we have the first calculated position for a given 2-hit combination, say xRem[layer][0], all others
      // are
      //---LoH: xRem[layer][iParSeed] = xRem[layer][0] +
      //(x[iParSeed]-x[0])*xZones.delSeedCorr1*(-xZones.xRefCorr-xZones.txCorr*zBar-zBar^2(1+dzBar))
      //---LoH: which, simplified, says: xRem[layer][iParSeed] = xRem[layer][0] + (x[iParSeed]-x[0])*Corr[layer]
      //---LoH: with Corr[layer] =
      // xZones.delSeedCorr(1,2)*(xZones.xRefCorr+xZones.txCorr*xZones.dzLays[layer]+xZones.dz2Lays[layer]);
      //---LoH: We do not take this into account as we overwhelmingly have <3 parabola hits
      // First parabola
      unsigned int n0 = xCandidates[part].size();
      for ( unsigned int i = 0; i < parabolaSeedHits[0].size(); ++i )
        //	fillXhits0( iCase, part, Fhit, parabolaSeedHits[0][i], Lhit, xZones, hitComb, begZones, endZones,
        // xCandidates, 		    parabolaSeedHits[2], tempBounds, xminPrev, lastHitX );
        fillXhits0( iCase, part, Fhit, parabolaSeedHits[0][i], Lhit, xZones, hitComb, begZones, endZones, xCandidates,
                    tempBounds, xminPrev, lastHitX );
      //---LoH: do not even try to reconstruct a track if you already got one before.
      // It only happens 400 out of 960k times that we find a candidate in fillXhits0 AND fillXhits1
      // Adding these two lines replaces 3 real tracks out of 11k by ghosts.
      if ( n0 != xCandidates[part].size() ) {
        //---LoH: avoids 29k calls to the rest out of 100 events. ~ number of XZ candidates
        //---LoH: 27900 times it's 1 candidate, 700 times it's 2, 30 times it's 3
        continue;
      }
      //---LoH: Remove hits in T2x2 that were already built in T2x1 extension. Happens 42k times in 100 events.
      for ( unsigned int i = 0; i < parabolaSeedHits[1].size(); ++i ) {
        fillXhits1( iCase, part, Fhit, parabolaSeedHits[1][i], Lhit, xZones, hitComb, begZones, endZones, xCandidates,
                    tempBounds, xminPrev, lastHitX );
      }
    }
  } // end loop first zone
}

// Builds the parabola solver from solely z information
//---LoH: Can be put outside of the class if m_dRatio is externalised

// Fill hits due to parabola seeds in T2x1
//---LoH: Can be put outside of the class.
//---LoH: Can be templated
void PrHybridSeeding::fillXhits0( unsigned int iCase, unsigned int part, const ModPrHitConstIter& Fhit,
                                  const ModPrHit& Phit, const ModPrHitConstIter& Lhit, const CaseGeomInfo& xZones,
                                  const TwoHitCombination&                                  hitComb,
                                  const std::array<HitIter, PrFTInfo::Numbers::NFTXLayers>& begZones,
                                  const std::array<HitIter, PrFTInfo::Numbers::NFTXLayers>& endZones,
                                  XCandidates& xCandidates, Boundaries& Bounds,
                                  std::array<float, PrFTInfo::Numbers::NFTXLayers>&       xminPrev,
                                  const std::array<float, PrFTInfo::Numbers::NFTXLayers>& lastHitX ) const noexcept {
  //---LoH: called 712k times in 100 events
  SeedTrackHitsX xHits;
  float          c = ( ( xZones.zLays[2] * hitComb.tx + hitComb.x0 ) - Phit.coord ) * xZones.delSeedCorr1;
  float          b = hitComb.tx - c * xZones.txCorr;
  float          a = hitComb.xRef - c * xZones.xRefCorr;
  fillXhitParabola( iCase, 3, xHits, c * xZones.dz2Lays[3] + b * xZones.dzLays[3] + a, Bounds );
  fillXhitRemaining( iCase, 4, c * xZones.dz2Lays[4] + b * xZones.dzLays[4] + a, begZones, endZones, xHits, Bounds,
                     xminPrev, lastHitX ); //---Called 767k times in total
  if ( xHits.size() == 0 ) return;         //---LoH: ~650k times out of 750k
  // Add the last hit
  fillXhitRemaining( iCase, 5, c * xZones.dz2Lays[5] + b * xZones.dzLays[5] + a, begZones, endZones, xHits, Bounds,
                     xminPrev, lastHitX ); // Missing T3x
  if ( xHits.size() < 2 ) return;          //---LoH: ~50k times out of 750k
  xHits.push_back( *Fhit );
  xHits.push_back( Phit ); // T2x1
  xHits.push_back( *Lhit );
  createXTrack( iCase, part, xCandidates, xHits ); //---LoH: called 27k times
  return;
}

// Fill hits due to parabola seeds in T2x2
//---LoH: Can be put outside of the class.
//---LoH: Can be templated
void PrHybridSeeding::fillXhits1( unsigned int iCase, unsigned int part, const ModPrHitConstIter& Fhit,
                                  const ModPrHit& Phit, const ModPrHitConstIter& Lhit, const CaseGeomInfo& xZones,
                                  const TwoHitCombination&                                  hitComb,
                                  const std::array<HitIter, PrFTInfo::Numbers::NFTXLayers>& begZones,
                                  const std::array<HitIter, PrFTInfo::Numbers::NFTXLayers>& endZones,
                                  XCandidates& xCandidates, Boundaries& Bounds,
                                  std::array<float, PrFTInfo::Numbers::NFTXLayers>&       xminPrev,
                                  const std::array<float, PrFTInfo::Numbers::NFTXLayers>& lastHitX ) const noexcept {
  //---LoH: called 716k times
  SeedTrackHitsX xHits;
  float          c = ( ( xZones.zLays[3] * hitComb.tx + hitComb.x0 ) - Phit.coord ) * xZones.delSeedCorr2;
  float          b = hitComb.tx - c * xZones.txCorr;
  float          a = hitComb.xRef - c * xZones.xRefCorr;
  fillXhitRemaining( iCase, 4, c * xZones.dz2Lays[4] + b * xZones.dzLays[4] + a, begZones, endZones, xHits, Bounds,
                     xminPrev, lastHitX );
  if ( xHits.size() != 1 ) return;
  fillXhitRemaining( iCase, 5, c * xZones.dz2Lays[5] + b * xZones.dzLays[5] + a, begZones, endZones, xHits, Bounds,
                     xminPrev, lastHitX );
  if ( xHits.size() != 2 ) return;
  xHits.push_back( *Fhit );
  xHits.push_back( Phit ); // T2x2
  xHits.push_back( *Lhit );
  createXTrack( iCase, part, xCandidates, xHits );
}

void PrHybridSeeding::createXTrack( const unsigned int& iCase, const unsigned int& part, XCandidates& xCandidates,
                                    const SeedTrackHitsX& xHits ) const noexcept {
  //---LoH: called 27k times
  //---LoH: out of all the calls, less than 50% refused
  // Create the track
  Pr::Hybrid::SeedTrackX xCand( m_dRatio.value(), xHits ); // The X Candidate is created

  PrTrackFitterXZ fitter;
  bool            fitX = fitter.fit( xCand );
  if ( !fitX ) { return; }
  // Algorithm allows to go down to 4 hits (m_minXPlanes) only from 6 hits ( == maximum ) track refit by construction.
  auto worstHitX     = removeWorstX( xCand, iCase );
  bool canRemoveHitX = xCand.size() == PrFTInfo::Numbers::NFTXLayers;
  while ( worstHitX != xCand.hits().cend() ) {
    if ( !canRemoveHitX ) { return; }
    xCand.hits().erase( worstHitX );
    fitX = fitter.fit( xCand );
    if ( !fitX ) { return; }
    worstHitX     = removeWorstX( xCand, iCase );
    canRemoveHitX = xCand.size() > m_minXPlanes;
  }

  // Fit is successful and  make a x/z-candidate
  if ( ( ( xCand.chi2PerDoF() < m_maxChi2DoFX[iCase] ) ) ) {
    xCand.setXT1( xCand.x( StateParameters::ZBegT ) );
    xCand.setXT3( xCand.x( StateParameters::ZEndT ) );
    xCandidates[part].push_back( xCand );
  }
}

PrHybridSeeding::SeedTrackHitsConstIter PrHybridSeeding::removeWorstX( const Pr::Hybrid::SeedTrackX& track,
                                                                       int iCase ) const noexcept {
  const auto worst_it  = std::max_element( track.chi2Hits().cbegin(), track.chi2Hits().cbegin() + track.size() );
  auto       worst_pos = worst_it - track.chi2Hits().cbegin();
  if ( track.chi2Hits()[worst_pos] < m_maxChi2HitsX[iCase] ) {
    return track.hits().cend();
  } else {
    return track.hits().cbegin() + worst_pos;
  }
}

bool PrHybridSeeding::fillXhitParabola( const int& iCase, const int& iLayer, SeedTrackHitsX& xHits, const float& xAtZ,
                                        const std::array<IterPairs, PrFTInfo::Numbers::NFTXLayers>& Bounds ) const
    noexcept {
  //---LoH: called 712k times in 100 events
  // Prediction of the position with the cubic correction!
  float xMinAtZ = xAtZ - m_tolRemaining[iCase]; // * (1. + dz*invZref );
  // Parabola here is larger than tolerances in x0! ( 0.75 mm up / down when x0 ~0 )
  // may we want ot make the m_tolRemaining[iCase] ( x0 dependent too ?? )
  // here you are re-using the tolerance defined before from the linear extrapolation of 2-hit combo
  ModPrHitConstIter bestProj = Bounds[iLayer].end;
  float             bestDist = m_tolRemaining[iCase]; // 1 cm around predicted position!!
  float             tmpDist;
  for ( auto itH = Bounds[iLayer].begin; itH != Bounds[iLayer].end; ++itH ) {
    if ( itH->coord < xMinAtZ ) { continue; } // this condition relies on the fact that invalid hits are set to -inf
    tmpDist = std::fabs( itH->coord - xAtZ );
    if ( tmpDist < bestDist ) {
      bestDist = tmpDist;
      bestProj = itH;
    } else
      break;
  }
  // Can happen if there is only one hit in the loop.
  if ( bestProj != Bounds[iLayer].end ) {
    xHits.push_back( *bestProj );
    return true;
  }
  return false;
}

void PrHybridSeeding::fillXhitRemaining( const int& iCase, const int& iLayer, const float& xAtZ,
                                         const std::array<HitIter, PrFTInfo::Numbers::NFTXLayers>& begZones,
                                         const std::array<HitIter, PrFTInfo::Numbers::NFTXLayers>& endZones,
                                         SeedTrackHitsX&                                           xHits,
                                         std::array<IterPairs, PrFTInfo::Numbers::NFTXLayers>&     Bounds,
                                         std::array<float, PrFTInfo::Numbers::NFTXLayers>&         xminPrev,
                                         const std::array<float, PrFTInfo::Numbers::NFTXLayers>&   lastHitX ) const
    noexcept {
  //---LoH: called 1.66M times
  // Prediction of the position with the cubic correction!
  float xMinAtZ = xAtZ - m_tolRemaining[iCase];
  //---LoH: We reimplement LookAroundMin here with a catch: if (which is nearly always the case) there is no chance of
  // going out of bounds, use appropriate methods

  // Parabola here is larger than tolerances in x0! ( 0.75 mm up / down when x0 ~0 )
  // may we want ot make the m_tolRemaining[iCase] ( x0 dependent too ?? )
  // here you are re-using the tolerance defined before from the linear extrapolation of 2-hit combo

  // If all the hits are inside, you don't need to check for many things
  if ( xMinAtZ < xminPrev[iLayer] )
    Bounds[iLayer].begin = get_lowerBound_lin_reverse( begZones[iLayer], Bounds[iLayer].begin, xMinAtZ );
  else {
    if ( xAtZ + m_tolRemaining[iCase] < lastHitX[iLayer] )
      moveLowerBoundX_lin( Bounds[iLayer].begin, xMinAtZ );
    else
      moveLowerBoundX_lin( Bounds[iLayer].begin, endZones[iLayer], xMinAtZ );
  }
  xminPrev[iLayer]           = xMinAtZ;
  ModPrHitConstIter bestProj = endZones[iLayer];
  float             bestDist = m_tolRemaining[iCase];
  float             tmpDist;
  for ( Bounds[iLayer].end = Bounds[iLayer].begin; Bounds[iLayer].end < endZones[iLayer]; ++Bounds[iLayer].end ) {
    if ( Bounds[iLayer].end->coord < xMinAtZ ) { continue; }
    tmpDist = std::fabs( Bounds[iLayer].end->coord - xAtZ );
    if ( tmpDist < bestDist ) {
      bestDist = tmpDist;
      bestProj = Bounds[iLayer].end;
    } else
      break;
  }
  if ( bestProj != endZones[iLayer] ) { xHits.push_back( *bestProj ); }
  return;
}

void PrHybridSeeding::updateXZCombinationPars( const unsigned int& iCase, const float& xFirst, const float& xLast,
                                               const CaseGeomInfo& xZones, const float& slope, const float& slopeopp,
                                               const float& accTerm1, const float& accTerm2,
                                               TwoHitCombination& hitComb ) const noexcept {
  //---LoH: Called 3M times in 100 events
  hitComb.tx    = ( xLast - xFirst ) * xZones.invZlZf;              //---LoH: this is basically b
  hitComb.x0    = xFirst - hitComb.tx * xZones.zLays[0];            //---LoH: this carries all the momentum information
  hitComb.xRef  = hitComb.x0 + Pr::Hybrid::zReference * hitComb.tx; //---LoH: this is basically a
  hitComb.x0new = hitComb.x0 * ( 1.f + m_x0Corr[iCase] );
  if ( !m_parabolaSeedParabolicModel ) {
    if ( hitComb.x0 > 0.f ) {
      hitComb.minPara = hitComb.x0 > m_x0SlopeChange[iCase] ? -slope * hitComb.x0 + accTerm1 : -m_tolX0SameSign[iCase];
      hitComb.maxPara =
          hitComb.x0 > m_x0SlopeChange2[iCase] ? slopeopp * hitComb.x0 - accTerm2 : +m_tolX0OppSign[iCase];
    } else {
      hitComb.maxPara = hitComb.x0 < -m_x0SlopeChange[iCase] ? -slope * hitComb.x0 - accTerm1 : m_tolX0SameSign[iCase];
      hitComb.minPara =
          hitComb.x0 < -m_x0SlopeChange2[iCase] ? slopeopp * hitComb.x0 + accTerm2 : -m_tolX0OppSign[iCase];
    }
  } else {
    hitComb.maxPara =
        m_parPol0[iCase] + hitComb.x0new * m_parPol1[iCase] + hitComb.x0new * hitComb.x0new * m_parPol2[iCase];
    hitComb.minPara = hitComb.maxPara - 2 * hitComb.x0new * m_parPol1[iCase];
  }
}

void PrHybridSeeding::initializeXProjections( unsigned int iCase, unsigned int part, CaseGeomInfo& xZones ) const
    noexcept {
  unsigned int firstZoneId( 0 ), lastZoneId( 0 );
  if ( 0 == iCase ) {
    firstZoneId = T1X1 - part;
    lastZoneId  = T3X2 - part;
  } else if ( 1 == iCase ) {
    firstZoneId = T1X2 - part;
    lastZoneId  = T3X1 - part;
  } else if ( 2 == iCase ) {
    firstZoneId = T1X1 - part;
    lastZoneId  = T3X1 - part;
  } else if ( 3 == iCase ) {
    firstZoneId = T1X2 - part;
    lastZoneId  = T3X2 - part;
  }

  // Array[0] = first layer in T1 for 2-hit combo

  xZones.zones[0]     = firstZoneId;
  xZones.zLays[0]     = m_z[firstZoneId];
  xZones.dzLays[0]    = xZones.zLays[0] - Pr::Hybrid::zReference;
  xZones.dz2Lays[0]   = xZones.dzLays[0] * xZones.dzLays[0] * ( 1.f + m_dRatio * xZones.dzLays[0] );
  xZones.planeCode[0] = m_planeCode[firstZoneId];
  // Array[1] = last  layer in T3 for 2-hit combo
  xZones.zones[1]     = lastZoneId;
  xZones.zLays[1]     = m_z[lastZoneId];
  xZones.dzLays[1]    = xZones.zLays[1] - Pr::Hybrid::zReference;
  xZones.dz2Lays[1]   = xZones.dzLays[1] * xZones.dzLays[1] * ( 1.f + m_dRatio * xZones.dzLays[1] );
  xZones.planeCode[1] = m_planeCode[lastZoneId];

  // Array[2] = T2-1st x-layers
  xZones.zones[2]     = T2X1 - part;
  xZones.zLays[2]     = m_z[T2X1 - part];
  xZones.dzLays[2]    = xZones.zLays[2] - Pr::Hybrid::zReference;
  xZones.dz2Lays[2]   = xZones.dzLays[2] * xZones.dzLays[2] * ( 1.f + m_dRatio * xZones.dzLays[2] );
  xZones.planeCode[2] = m_planeCode[lastZoneId];
  // Array[3] = T2-2nd x-layers
  xZones.zones[3]     = T2X2 - part;
  xZones.zLays[3]     = m_z[T2X2 - part];
  xZones.dzLays[3]    = xZones.zLays[3] - Pr::Hybrid::zReference;
  xZones.dz2Lays[3]   = xZones.dzLays[3] * xZones.dzLays[3] * ( 1.f + m_dRatio * xZones.dzLays[3] );
  xZones.planeCode[3] = m_planeCode[T2X2 - part];

  unsigned int i = 4;
  // Add extra layer HERE, if needed!!!!
  for ( unsigned int xZoneId : {T1X1, T1X2, T3X1, T3X2} ) {
    xZoneId = xZoneId - part;
    if ( xZoneId != firstZoneId && xZoneId != lastZoneId ) {
      xZones.zones[i]     = xZoneId;
      xZones.zLays[i]     = m_z[xZoneId];
      xZones.dzLays[i]    = xZones.zLays[i] - Pr::Hybrid::zReference;
      xZones.dz2Lays[i]   = xZones.dzLays[i] * xZones.dzLays[i] * ( 1.f + m_dRatio * xZones.dzLays[i] );
      xZones.planeCode[i] = m_planeCode[xZoneId];
      ++i;
    }
  }
  xZones.invZf   = 1.f / xZones.zLays[0];
  xZones.invZlZf = 1.f / ( xZones.zLays[1] - xZones.zLays[0] );

  // Parabola z difference
  xZones.t2Dist = xZones.zLays[3] - xZones.zLays[2];

  // Correction factors
  // xInfCorr  = (zLast/zFirst) -1
  // x0        = Del_x_inf*xInfCorr
  //  xZones.xInfCorr = -1.f/((xZones.dzLays[1]/xZones.dzLays[0]) - 1.f);
  // tx_picked = b + c * txCorr (NEEDS TO BE CORRECTED AS WELL WITH D)
  //  xZones.txCorr         = (xZones.dz2Lays[1]-xZones.dz2Lays[0])/(xZones.zLays[1]-xZones.zLays[0]);
  xZones.txCorr = xZones.dzLays[1] + xZones.dzLays[0] +
                  m_dRatio * ( xZones.dzLays[1] * xZones.dzLays[1] + xZones.dzLays[0] * xZones.dzLays[0] +
                               xZones.dzLays[1] * xZones.dzLays[0] );
  // xRef      = a + c * xRefCorr
  xZones.xRefCorr =
      xZones.dzLays[0] * ( xZones.dzLays[0] - xZones.txCorr + m_dRatio * xZones.dzLays[0] * xZones.dzLays[0] );

  // DeltaSeed_X = c*delSeedCorrX
  xZones.delSeedCorr1 =
      1.f /
      ( ( ( ( xZones.zLays[2] - xZones.zLays[0] ) * xZones.invZlZf ) * ( xZones.dz2Lays[1] - xZones.dz2Lays[0] ) ) +
        ( xZones.dz2Lays[0] - xZones.dz2Lays[2] ) );
  xZones.delSeedCorr2 =
      1.f /
      ( ( ( ( xZones.zLays[3] - xZones.zLays[0] ) * xZones.invZlZf ) * ( xZones.dz2Lays[1] - xZones.dz2Lays[0] ) ) +
        ( xZones.dz2Lays[0] - xZones.dz2Lays[3] ) );
}

//---LoH: 40% of the findXProjection timing.
//---LoH: Can be put outside of the class.
bool PrHybridSeeding::findParabolaHits( const unsigned int& iLayer, const float& xMin, const float& xMax,
                                        const std::array<HitIter, PrFTInfo::Numbers::NFTXLayers>& endZones,
                                        Boundaries& Bounds, SeedTrackParabolaHits& parabolaSeedHits,
                                        const float& lastHitX ) const noexcept {
  //---LoH: Called 6M times in 100 events
  if ( xMin < lastHitX )
    moveLowerBoundX_lin( Bounds[iLayer].begin, xMin );
  else // there is no chance we find anything. In fact, it means we could stop the 2-hit search loop there: it won't get
       // better.
    return false;

  if ( xMax < lastHitX ) {
    for ( Bounds[iLayer].end = Bounds[iLayer].begin; /*No break test*/; ++Bounds[iLayer].end ) {
      if ( !Bounds[iLayer].end->isValid() ) continue;
      if ( Bounds[iLayer].end->coord > xMax ) break;
      parabolaSeedHits.push_back( *( Bounds[iLayer].end ) );
    }
  } else {
    for ( Bounds[iLayer].end = Bounds[iLayer].begin; Bounds[iLayer].end != endZones[iLayer]; ++Bounds[iLayer].end ) {
      if ( !Bounds[iLayer].end->isValid() ) continue;
      if ( Bounds[iLayer].end->coord > xMax ) break;
      parabolaSeedHits.push_back( *( Bounds[iLayer].end ) );
    }
  }
  return ( parabolaSeedHits.size() != 0 );
}
