/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include "Functors/Filter.h"
#include "Functors/with_functors.h"
#include "GaudiAlg/Transformer.h"
#include "Kernel/EventLocalUnique.h"

#include "SOAContainer/SOAContainer.h"
#include "SOAExtensions/ZipSelection.h"
#include "SelKernel/TrackZips.h"

#include <vector>

namespace Pr {

  namespace {
    template <typename T>
    struct TypeHelper {};

    /** Helper for compile time check if a type is a filtered-zip type
     */
    template <typename>
    struct must_be_wrapped : std::false_type {};

    // TODO don't want to write detail here...
    template <typename... T>
    struct must_be_wrapped<LHCb::Pr::detail::merged_t<T...>> : std::true_type {};

    template <typename T>
    inline constexpr bool must_be_wrapped_v = must_be_wrapped<T>::value;

    // Given the tag type T, TypeHelper tells us both the return type of the
    // functor (OutputType) and whether this return type is to be converted into
    // a non-owning view (WrapOutput), which means that we can deduce the output
    // type of the convert() function too, just using the tag type T. This is the
    // return type of operator() in the algorithm. We also need to deduce that
    // type with the leading bool stripped off, so that we can write down the
    // name of the functional framework base class that the algorithm inherits
    // from
    template <typename TagType>
    struct OutputHelper {
      using FilteredType               = Functors::filtered_t<TagType>;
      static constexpr bool WrapOutput = must_be_wrapped_v<FilteredType>;

      template <typename KeyValue>
      static auto names() {
        if constexpr ( WrapOutput ) {
          return std::array<KeyValue, 2>{{KeyValue{"Output", ""}, KeyValue{"OutputStorage", ""}}};
        } else {
          return KeyValue{"Output", ""};
        }
      }

      static auto convert( EventContext const& evtCtx, FilteredType&& filtered ) {
        if constexpr ( WrapOutput ) {
          // OutputType is some owning type that we don't want to use directly, but
          // which we need to keep alive so that we can use a non-owning view into
          // it. Achieve this by wrapping it up in a unique_ptr -- so the address
          // of the contained object is stable -- and returning that. We use this
          // special make_event_local_unique function to create a unique_ptr that is
          // allocated using the custom, event-local memory pool and deleted with a
          // custom, stateful, deleter.
          auto storage =
              LHCb::make_event_local_unique<FilteredType>( LHCb::getMemResource( evtCtx ), std::move( filtered ) );
          // Make a non-owning view, which will store the address of the object
          // hidden inside the unique_ptr.
          auto view = LHCb::Pr::make_zip( std::as_const( *storage.get() ) );
          // The point of the output wrapping is to make sure that the input
          // and output types are the same. Check that's the case.
          static_assert( std::is_same_v<decltype( view ), TagType> );
          return std::tuple{false, std::move( view ), std::move( storage )};
        } else {
          // Similarly if the output wrapping was not deemed necessary this
          // "should" mean that the filtered type already matched the input type.
          // Actually with the v2 zip machinery then `T` maps onto `std::tuple<T>`
          if constexpr ( std::is_same_v<FilteredType, std::tuple<TagType>> ) {
            return std::tuple{false, std::get<0>( std::move( filtered ) )};
          } else {
            static_assert( std::is_same_v<FilteredType, TagType> );
            return std::tuple{false, std::move( filtered )};
          }
        }
      }

      // this is std::tuple<bool, A[, B]>
      using AlgorithmOutput = std::invoke_result_t<decltype( convert ), EventContext const&, FilteredType>;

      // helper to strip off the bool from the type
      template <typename T>
      struct remove_first_type {};

      template <typename T, typename... Ts>
      struct remove_first_type<std::tuple<T, Ts...>> {
        using type = std::tuple<Ts...>;
      };

      // this is std::tuple<A[, B]>
      using DataTuple = typename remove_first_type<AlgorithmOutput>::type;
    };

    // Just shorthand for below
    template <typename T>
    using FilterTransform =
        Gaudi::Functional::MultiTransformerFilter<typename OutputHelper<T>::DataTuple( EventContext const&, T const& )>;

    template <typename T>
    using SOAFilterTransform = Gaudi::Functional::MultiTransformerFilter<std::tuple<typename TypeHelper<T>::OutputType>(
        typename TypeHelper<T>::AlgInputType, Zipping::ExportedSelection<> const& )>;

    template <typename T>
    struct FilterCut {
      constexpr static auto PropertyName = "Cut";
      constexpr static auto ExtraHeaders = LHCb::header_map_v<T>;
      using Signature                    = Functors::filtered_t<T>( T const& );
    };

    template <typename T>
    struct SOAFilterCut {
      constexpr static auto PropertyName = "Cut";
      constexpr static auto ExtraHeaders = TypeHelper<T>::ExtraHeaders;
      using InputType                    = typename TypeHelper<T>::InputType;
      using Signature                    = Functors::filtered_t<InputType>( InputType );
    };
  } // namespace

  /** @class Filter PrFilter.cpp
   *
   *  Filter<T> applies a selection to an input Selection<T> and returns a new Selection<T> object.
   *
   *  @tparam T The selected object type (e.g. Track, Particle, ...). By contruction this is not copied, as the
   *            input/output type Selection<T> is just a view of some other underlying storage.
   */
  template <typename T>
  class Filter final : public with_functors<FilterTransform<T>, FilterCut<T>> {
  public:
    using Base    = with_functors<FilterTransform<T>, FilterCut<T>>;
    using OHelper = OutputHelper<T>;

    Filter( const std::string& name, ISvcLocator* pSvcLocator )
        : Base( name, pSvcLocator, {"Input", ""}, OHelper::template names<typename Base::KeyValue>() ) {}

    // Return type is std::tuple<bool, A[, B]>
    typename OHelper::AlgorithmOutput operator()( EventContext const& evtCtx, T const& in ) const override {
      // Get the functor from the with_functors mixin
      auto const& pred = this->template getFunctor<FilterCut<T>>();

      // Apply the functor to get something we can return
      auto filtered = pred( in );
      // Add an extra conversion step if requested
      // We do this first so the constexpr logic in `convert` can normalise
      // things before we update the statistics and pass/fail flag. For
      // example, one zip implementation maps T -> T and the other maps
      // T -> std::tuple<T> when filtering.
      auto converted = OHelper::convert( evtCtx, std::move( filtered ) );
      // Get the pass/fail flag the control flow sees
      bool& filter_pass = std::get<0>( converted );
      // Get the output view, whose type should match `in`
      auto const& filtered_view = std::get<1>( converted );
      // For use in the control flow: did we select anything?
      filter_pass = !filtered_view.empty();
      // Update the statistics.
      m_cutEff += {filtered_view.size(), in.size()};
      return converted;
    }

  private:
    // Counter for recording cut retention statistics
    mutable Gaudi::Accumulators::BinomialCounter<> m_cutEff{this, "Cut selection efficiency"};
  };

  template <typename T>
  class SOAFilter final : public with_functors<SOAFilterTransform<T>, SOAFilterCut<T>> {
  public:
    using Base       = with_functors<SOAFilterTransform<T>, SOAFilterCut<T>>;
    using InputType  = typename TypeHelper<T>::AlgInputType;
    using OutputType = typename TypeHelper<T>::OutputType;
    using KeyValue   = typename SOAFilterTransform<T>::KeyValue;

    SOAFilter( const std::string& name, ISvcLocator* pSvcLocator )
        : Base( name, pSvcLocator, {KeyValue{"Input", ""}, KeyValue{"InputSelection", ""}}, KeyValue{"Output", ""} ) {}

    std::tuple<bool, OutputType> operator()( InputType in, Zipping::ExportedSelection<> const& in_sel ) const override {
      // Apply the input selection to our input data
      auto selected_input = Zipping::SelectionView{&in, in_sel};

      // Get the functor from the with_functors mixin
      auto const& pred = this->template getFunctor<SOAFilterCut<T>>();

      // Apply the functor to get the refined selection
      auto filtered = pred( selected_input );

      // Update the statistics.
      m_cutEff += {filtered.size(), in.size()};

      // For use in the control flow: did we select anything?
      auto filter_pass = !filtered.empty();

      return {filter_pass, std::move( filtered )};
    }

  private:
    // Counter for recording cut retention statistics
    mutable Gaudi::Accumulators::BinomialCounter<> m_cutEff{this, "Cut selection efficiency"};
  };

  ////////////////////////////////////////////////////////////
  /// TODO: replace all of the above with the code below:
  ////////////////////////////////////////////////////////////

  // Just shorthand for below
  namespace {
    template <typename... Ts>
    using ZipFilterTransform =
        Gaudi::Functional::MultiTransformerFilter<std::tuple<Ts...>( EventContext const&, Ts const&... )>;

    template <typename... Ts>
    struct ZipFilterCut {
      constexpr static auto PropertyName = "Cut";
      constexpr static auto ExtraHeaders = ( ... + LHCb::header_map_v<Ts> );
      using InputType = typename LHCb::v2::Event::Zip<SIMDWrapper::InstructionSet::Best, Ts const...>;
      using Signature = Functors::filtered_t<InputType>( InputType );
    };

    template <typename Base, typename... Inputs>
    struct IOHelper {
      static constexpr std::size_t NumInputs = sizeof...( Inputs );
      using KeyValue                         = typename Base::KeyValue;
      static auto InputNames() {
        std::array<KeyValue, NumInputs> ret;
        for ( auto i = 0ul; i < NumInputs; ++i ) { ret[i] = KeyValue{"Input" + std::to_string( i + 1 ), ""}; }
        return ret;
      }
      static auto OutputNames() {
        std::array<KeyValue, NumInputs> ret;
        for ( auto i = 0ul; i < NumInputs; ++i ) { ret[i] = KeyValue{"Output" + std::to_string( i + 1 ), ""}; }
        return ret;
      }
    };
  } // namespace

  template <typename... ContainerTypes>
  class ZipFilter final : public with_functors<ZipFilterTransform<ContainerTypes...>, ZipFilterCut<ContainerTypes...>> {
  public:
    using Base     = with_functors<ZipFilterTransform<ContainerTypes...>, ZipFilterCut<ContainerTypes...>>;
    using output_t = typename std::tuple<bool, ContainerTypes...>;

    ZipFilter( const std::string& name, ISvcLocator* pSvcLocator )
        : Base( name, pSvcLocator, IOHelper<Base, ContainerTypes...>::InputNames(),
                IOHelper<Base, ContainerTypes...>::OutputNames() ) {}

    output_t operator()( EventContext const&, ContainerTypes const&... in ) const override {
      // Apply the input selection to our input data
      auto zip = LHCb::v2::Event::make_zip( in... );

      // Get the functor from the with_functors mixin
      auto const& pred = this->template getFunctor<ZipFilterCut<ContainerTypes...>>();

      // Apply the functor to get the refined selection
      auto filtered = pred( zip );

      // Update the statistics.
      m_cutEff += {std::get<0>( filtered ).size(), zip.size()};

      // For use in the control flow: did we select anything?
      auto filter_pass = !std::get<0>( filtered ).empty();

      return std::apply( [&]( auto&&... args ) { return std::make_tuple( filter_pass, std::move( args )... ); },
                         filtered );
    }

  private:
    // Counter for recording cut retention statistics
    mutable Gaudi::Accumulators::BinomialCounter<> m_cutEff{this, "Cut selection efficiency"};
  };
} // namespace Pr
