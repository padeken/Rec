###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
################################################################################
# Package: LoKiTrack
################################################################################
gaudi_subdir(LoKiTrack_v2 v1r0)

gaudi_depends_on_subdirs(Event/TrackEvent
                         Phys/LoKiCore
                         Tr/TrackInterfaces)

find_package(Boost)
find_package(ROOT)
find_package(cppgsl)
include_directories(SYSTEM ${Boost_INCLUDE_DIRS} ${ROOT_INCLUDE_DIRS})

gaudi_add_library(LoKiTrack_v2Lib
                  src/*.cpp
                  PUBLIC_HEADERS LoKi_v2
                  INCLUDE_DIRS Tr/TrackInterfaces cppgsl
                  LINK_LIBRARIES TrackEvent LoKiCoreLib TrackKernel)

gaudi_add_module(LoKiTrack_v2
                 src/Components/*.cpp
                 INCLUDE_DIRS Tr/TrackInterfaces
                 LINK_LIBRARIES TrackEvent LoKiCoreLib LoKiTrack_v2Lib)

gaudi_add_dictionary(LoKiTrack_v2
                     dict/LoKiTrack_v2Dict.h
                     dict/LoKiTrack_v2.xml
                     INCLUDE_DIRS Tr/TrackInterfaces
                     LINK_LIBRARIES TrackEvent LoKiCoreLib LoKiTrack_v2Lib)

gaudi_install_python_modules()

gaudi_add_test(QMTest QMTEST)
