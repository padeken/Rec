/*****************************************************************************\
* (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration .         *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include <vector>

#include "GaudiAlg/FunctionalDetails.h"
#include "GaudiAlg/MergingTransformer.h"

#include "Event/MCParticle.h"
#include "Event/ProtoParticle.h"
#include "Relations/RelationWeighted1D.h"

/** @class MergeRelationsTables MergeRelationsTables.cpp
 *
 * @brief Merge a list of separate RelationsTable1DWeighted input tables to a single table.
 *
 * The merging logic assumes that there are no duplicate relations across all
 * of the input tables.
 *
 * @tparam To First template parameter of RelationWeighted1D.
 * @tparam From Second template parameter of RelationWeighted1D.
 * @tparam W Third template parameter of RelationWeighted1D.
 *
 */
template <typename To, typename From, typename W>
struct MergeRelationsTables
    : public Gaudi::Functional::MergingTransformer<
          LHCb::RelationWeighted1D<To, From, W>(
              const Gaudi::Functional::vector_of_const_<LHCb::RelationWeighted1D<To, From, W>>& ),
          Gaudi::Functional::Traits::BaseClass_t<FixTESPath<Gaudi::Algorithm>>> {

  using table_t  = LHCb::RelationWeighted1D<To, From, W>;
  using input_t  = const Gaudi::Functional::vector_of_const_<table_t>&;
  using output_t = table_t;
  using base_class =
      Gaudi::Functional::MergingTransformer<output_t( input_t ),
                                            Gaudi::Functional::Traits::BaseClass_t<FixTESPath<Gaudi::Algorithm>>>;

  MergeRelationsTables( const std::string& name, ISvcLocator* pSvcLocator )
      : base_class( name, pSvcLocator, {"InputRelationsTables", {}}, {"Output", ""} ){};

  output_t operator()( input_t tables ) const override {
    output_t merged;
    for ( const auto& table : tables ) {
      for ( const auto& relation : table.relations() ) {
        merged.relate( relation.from(), relation.to(), relation.weight() )
            .orElse( [&] { ++m_duplicate_relation; } )
            .ignore();
      }
    }
    return merged;
  }

private:
  mutable Gaudi::Accumulators::MsgCounter<MSG::ERROR> m_duplicate_relation{this, "Unexpected duplicate relation found"};
};

namespace {
  using MergeRelationsTablesPP2MCP = MergeRelationsTables<LHCb::ProtoParticle, LHCb::MCParticle, double>;
}

DECLARE_COMPONENT_WITH_ID( MergeRelationsTablesPP2MCP, "MergeRelationsTablesPP2MCP" )
