/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/ProtoParticle.h"
#include "GaudiAlg/GaudiAlgorithm.h"

/** @class ChargedProtoParticleRemovePIDInfo ChargedProtoParticleRemovePIDInfo.h
 *
 * Implementation file for algorithm ChargedProtoParticleRemovePIDInfo
 *  Remove the Muon and Rich PID information stored in the ProtoParticles
 *
 *  @author Dmitry Golubkov
 *  @date 13/03/2010
 */

class ChargedProtoParticleRemovePIDInfo final : public GaudiAlgorithm {

public:
  /// Standard constructor
  ChargedProtoParticleRemovePIDInfo( const std::string& name, ISvcLocator* pSvcLocator );

  StatusCode execute() override; ///< Algorithm execution

private:
  std::string m_protoPath;     ///< Location of the ProtoParticles in the TES
  bool        m_RemoveRichPID; ///< instruct algorith to remove the RichPID info from PP
  bool        m_RemoveMuonPID; ///< instruct algorith to remove the MuonPID info from PP
  bool        m_RemoveCombPID; ///< instruct algorith to remove the CombPID info from PP
};

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( ChargedProtoParticleRemovePIDInfo )

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
ChargedProtoParticleRemovePIDInfo::ChargedProtoParticleRemovePIDInfo( const std::string& name,
                                                                      ISvcLocator*       pSvcLocator )
    : GaudiAlgorithm( name, pSvcLocator ) {

  // context specific locations
  if ( context() == "HLT" || context() == "Hlt" ) {
    m_protoPath = LHCb::ProtoParticleLocation::HltCharged;
  } else {
    m_protoPath = LHCb::ProtoParticleLocation::Charged;
  }

  // ProtoParticles
  declareProperty( "ProtoParticleLocation", m_protoPath );
  declareProperty( "RemoveRichPID", m_RemoveRichPID = true, "remove Rich PID info from the ProtoParticles" );
  declareProperty( "RemoveMuonPID", m_RemoveMuonPID = true, "remove Muon PID info from the ProtoParticles" );
  declareProperty( "RemoveCombPID", m_RemoveCombPID = true, "remove Combined PID info from the ProtoParticles" );
}

//=============================================================================
// Main execution
//=============================================================================
StatusCode ChargedProtoParticleRemovePIDInfo::execute() {
  // ProtoParticle container
  auto* protos = getIfExists<LHCb::ProtoParticles>( m_protoPath );
  if ( !protos ) {
    return Warning( "No existing ProtoParticle container at " + m_protoPath + " thus do nothing.",
                    StatusCode::SUCCESS );
  }

  // Loop over proto particles
  for ( auto* proto : *protos ) {

    if ( msgLevel( MSG::VERBOSE ) ) verbose() << "Trying ProtoParticle " << proto->key() << endmsg;

    // Erase current RichPID information
    if ( m_RemoveRichPID ) proto->removeRichInfo();

    // Erase current MuonPID information
    if ( m_RemoveMuonPID ) proto->removeMuonInfo();

    // Erase current CombPID information
    if ( m_RemoveCombPID ) proto->removeCombinedInfo();

    // print full ProtoParticle content
    if ( msgLevel( MSG::VERBOSE ) ) verbose() << *proto << endmsg;
  }

  return StatusCode::SUCCESS;
}

//=============================================================================
