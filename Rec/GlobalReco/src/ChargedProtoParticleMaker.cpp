/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
//-----------------------------------------------------------------------------
/** @class ChargedProtoParticleMaker ChargedProtoParticleMaker.h
 *
 *  Algorithm to build charged ProtoParticles from charged Tracks.
 * Implementation file for algorithm ChargedProtoParticleMaker
 *
 * @author Chris Jones   Christopher.Rob.Jones@cern.ch
 * @date 28/08/2009
 */
//-----------------------------------------------------------------------------
#include "Event/ProtoParticle.h"
#include "Event/Track.h"
#include "GaudiAlg/GaudiAlgorithm.h"
#include "GaudiKernel/HashMap.h"
#include "Interfaces/IProtoParticleTool.h"
#include "TrackInterfaces/ITrackSelector.h"

class ChargedProtoParticleMaker final : public GaudiAlgorithm {

public:
  /// Standard constructor
  using GaudiAlgorithm::GaudiAlgorithm;

  StatusCode execute() override; ///< Algorithm execution

private: // data
  /// Location of the ProtoParticles in the TES
  Gaudi::Property<std::string> m_protoPath{this, "Output", LHCb::ProtoParticleLocation::Charged};

  /// Locations in TES of input Tracks
  Gaudi::Property<std::vector<std::string>> m_tracksPath{this, "Inputs", {LHCb::TrackLocation::Default}};

  /// Append new ProtoParticles
  Gaudi::Property<bool> m_append{this, "Append", false};

  /// Track selector type
  ToolHandle<ITrackSelector> m_trSel{this, "TrackSelector", "DelegatingTrackSelector/TrackSelector"};

  ToolHandleArray<LHCb::Rec::Interfaces::IProtoParticles> m_addInfo{this, "AddInfo", {}};
};

//=============================================================================
// Main execution
//=============================================================================
StatusCode ChargedProtoParticleMaker::execute() {
  // check if output data already exists
  auto* protos = getIfExists<LHCb::ProtoParticles>( m_protoPath );
  if ( protos ) {
    if ( !m_append.value() ) {
      Warning( "Existing ProtoParticle container at " + m_protoPath + " found -> Will replace.", StatusCode::SUCCESS,
               1 )
          .ignore();
      protos->clear();
    } else {
      Warning( "Existing ProtoParticle container at " + m_protoPath + " found -> Will conserve.", StatusCode::SUCCESS,
               1 )
          .ignore();
    }
  } else {
    // make new container and give to Gaudi
    protos = new LHCb::ProtoParticles();
    put( protos, m_protoPath );
  }

  // Loop over tracks container
  setFilterPassed( false );
  for ( const auto& loc : m_tracksPath ) {
    // Load the Track objects (mandatory - should be there for each event)
    const auto* tracks = getIfExists<LHCb::Tracks>( loc );
    if ( !tracks ) {
      Warning( "No Tracks at '" + loc + "'", StatusCode::SUCCESS ).ignore();
      continue;
    }

    setFilterPassed( true );
    if ( msgLevel( MSG::DEBUG ) )
      debug() << "Successfully loaded " << tracks->size() << " Tracks from " << loc << endmsg;

    int count = 0;
    // Loop over tracks
    for ( const auto* tk : *tracks ) {

      // Select tracks
      if ( msgLevel( MSG::VERBOSE ) ) verbose() << "Trying Track " << tk->key() << endmsg;
      if ( !m_trSel->accept( *tk ) ) continue;
      if ( msgLevel( MSG::VERBOSE ) ) {
        verbose() << " -> Track selected " << tk->key() << endmsg;
        verbose() << " -> Track type " << tk->type() << endmsg;
        verbose() << " -> Track flag " << tk->flag() << endmsg;
        verbose() << " -> Track charge " << tk->charge() << endmsg;
      }

      // Make a proto-particle
      auto* proto = new LHCb::ProtoParticle();

      if ( m_tracksPath.size() == 1 && !m_append.value() ) {
        // Insert into container, with same key as Track
        protos->insert( proto, tk->key() );
      } else {
        // If more than one Track container, cannot use Track key
        protos->insert( proto );
      }

      // Set track reference
      proto->setTrack( tk );

      // Add minimal track info
      proto->addInfo( LHCb::ProtoParticle::additionalInfo::TrackChi2PerDof, tk->chi2PerDoF() );
      proto->addInfo( LHCb::ProtoParticle::additionalInfo::TrackNumDof, tk->nDoF() );
      proto->addInfo( LHCb::ProtoParticle::additionalInfo::TrackHistory, tk->history() );
      proto->addInfo( LHCb::ProtoParticle::additionalInfo::TrackType, tk->type() );
      proto->addInfo( LHCb::ProtoParticle::additionalInfo::TrackP, tk->p() );
      proto->addInfo( LHCb::ProtoParticle::additionalInfo::TrackPt, tk->pt() );

      if ( msgLevel( MSG::VERBOSE ) ) { verbose() << " -> Created ProtoParticle : " << *proto << endmsg; }
      ++count;
    }
    counter( loc + " ==> " + m_protoPath ) += count;
  }
  for ( const auto& addInfo : m_addInfo ) ( *addInfo )( *protos ).ignore();
  // return
  return StatusCode::SUCCESS;
}

//=============================================================================

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( ChargedProtoParticleMaker )

//=============================================================================
