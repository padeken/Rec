#!/bin/bash
###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

export CASTORD=$CASTOR_HOME/ProtoParticlePIDtuples/MC12-Binc-nu2.5

export FILELIST="castor-list.txt"
rm -rf $FILELIST
touch $FILELIST

nsrm -r $CASTORD
nsmkdir $CASTORD

export i=0
for tuple in `find ~/gangadir/workspace/jonrob/LocalXML -name ProtoPIDANN.MC.tuples.root | perl -MList::Util=shuffle -e"print shuffle<>"`; do
 export CASTORF=${CASTORD}"/Reco14-"${i}".root"
 echo "Copying "$tuple" to castor as "$CASTORF
 echo $CASTORF >> $FILELIST
 rfcp $tuple $CASTORF
 i=`expr $i + 1`
done

echo "Copied "`cat $FILELIST | wc -l`" files to castor. Listed in '"$FILELIST"'"

exit 0
