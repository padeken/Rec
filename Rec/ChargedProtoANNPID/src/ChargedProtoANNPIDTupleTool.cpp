/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "ChargedProtoANNPIDToolBase.h"
#include "Event/MCParticle.h"
#include "MCInterfaces/IRichMCTruthTool.h"
#include "RecInterfaces/IChargedProtoANNPIDTupleTool.h"
#include <memory>
#include <unordered_map>

namespace {
  /// Access a variable from the RecSummary
  template <typename Parent>
  auto inRecSummary( LHCb::RecSummary::DataTypes info, const Parent* parent, double defValue = 0 ) {
    return [=]( const LHCb::ProtoParticle* ) -> double {
      return parent && parent->recSummary() ? parent->recSummary()->info( info, defValue ) : -999;
    };
  }
  const auto recSummaries = std::map<std::string_view, LHCb::RecSummary::DataTypes, std::less<>>{
      {"NumLongTracks", LHCb::RecSummary::DataTypes::nLongTracks},
      {"NumLongTracks", LHCb::RecSummary::DataTypes::nLongTracks},
      {"NumDownstreamTracks", LHCb::RecSummary::DataTypes::nDownstreamTracks},
      {"NumUpstreamTracks", LHCb::RecSummary::DataTypes::nUpstreamTracks},
      {"NumVeloTracks", LHCb::RecSummary::DataTypes::nVeloTracks},
      {"NumTTracks", LHCb::RecSummary::DataTypes::nTTracks},
      {"NumGhosts", LHCb::RecSummary::DataTypes::nGhosts},
      {"NumMuonTracks", LHCb::RecSummary::DataTypes::nMuonTracks},
      {"NumPVs", LHCb::RecSummary::DataTypes::nPVs},
      {"NumRich1Hits", LHCb::RecSummary::DataTypes::nRich1Hits},
      {"NumRich2Hits", LHCb::RecSummary::DataTypes::nRich2Hits},
      {"NumVeloClusters", LHCb::RecSummary::DataTypes::nVeloClusters},
      {"NumITClusters", LHCb::RecSummary::DataTypes::nITClusters},
      {"NumTTClusters", LHCb::RecSummary::DataTypes::nTTClusters},
      {"NumOTClusters", LHCb::RecSummary::DataTypes::nOTClusters},
      {"NumSPDHits", LHCb::RecSummary::DataTypes::nSPDhits},
      {"NumMuonCoordsS0", LHCb::RecSummary::DataTypes::nMuonCoordsS0},
      {"NumMuonCoordsS1", LHCb::RecSummary::DataTypes::nMuonCoordsS1},
      {"NumMuonCoordsS2", LHCb::RecSummary::DataTypes::nMuonCoordsS2},
      {"NumMuonCoordsS3", LHCb::RecSummary::DataTypes::nMuonCoordsS3},
      {"NumMuonCoordsS4", LHCb::RecSummary::DataTypes::nMuonCoordsS4}};
} // namespace

namespace ANNGlobalPID {

  /** @class ChargedProtoANNPIDTupleTool ChargedProtoANNPIDTupleTool.h
   *
   *  Tool to fill the ANN PID variables into a tuple
   *
   *  @author Chris Jones   Christopher.Rob.Jones@cern.ch
   *  @date   2011-02-04
   */

  class ChargedProtoANNPIDTupleTool final : public extends<ChargedProtoANNPIDToolBase, IChargedProtoANNPIDTupleTool> {

  public:
    /// Standard constructor
    ChargedProtoANNPIDTupleTool( const std::string& type, const std::string& name, const IInterface* parent );

    /// Algorithm initialization
    StatusCode initialize() override {
      return ChargedProtoANNPIDToolBase::initialize().andThen( [&] {
        // get tools
        m_truth = tool<Rich::MC::IMCTruthTool>( "Rich::MC::MCTruthTool", "MCTruth", this );

        // Get a vector of input accessor objects for the configured variables
        for ( const auto& i : m_variables ) { m_inputs.insert_or_assign( i, getInput( i ) ); }
      } );
    }

    /// Access on demand the RecSummary object
    const LHCb::RecSummary* recSummary() const {
      auto summary = m_recSumPath.getIfExists();
      if ( UNLIKELY( !summary ) ) ++m_no_recsum;
      return summary;
    }

    /// Access on demand the ODIN object
    const LHCb::ODIN* odin() const {
      auto odin = m_odinPath.getIfExists();
      if ( UNLIKELY( !odin ) ) ++m_no_odin;
      return odin;
    }

    Input getInput( const std::string& name ) const override {
      auto i = recSummaries.find( name );
      if ( i != recSummaries.end() ) return {name, inRecSummary( i->second, this )};

      // ODIN information
      if ( "RunNumber" == name ) { /// Access ODIN Run number
        return {"RunNumber", [parent = this]( const LHCb::ProtoParticle* ) {
                  const auto odin = parent->odin();
                  return odin ? odin->runNumber() : 0;
                }};
      } else if ( "EventNumber" == name ) { /// Access ODIN event number
        return {"EventNumber", [parent = this]( const LHCb::ProtoParticle* ) {
                  const auto odin = parent->odin();
                  return odin ? odin->eventNumber() : 0;
                }};
      }
      return ChargedProtoANNPIDToolBase::getInput( name );
    }

    /// Fill the tuple tool with information for the given ProtoParticle
    StatusCode fill( Tuples::Tuple& tuple, const LHCb::ProtoParticle* proto,
                     const LHCb::ParticleID pid = LHCb::ParticleID() ) const override;

  private:
    /// ProtoParticle variables as strings to add to the ntuple
    StringInputs m_variables;

    /// Use RICH tool to get MCParticle associations for Tracks (To avoid annoying Linkers)
    const Rich::MC::IMCTruthTool* m_truth = nullptr;

    /// variables to fill
    std::unordered_map<std::string, ANNGlobalPID::Input> m_inputs;

    /// Location in TES for RecSummary object
    DataObjectReadHandle<LHCb::RecSummary> m_recSumPath{this, "RecSummaryLocation", LHCb::RecSummaryLocation::Default};

    /// Location in TES for ODIN object
    DataObjectReadHandle<LHCb::ODIN> m_odinPath{this, "ODINLocation", LHCb::ODINLocation::Default};

    mutable Gaudi::Accumulators::MsgCounter<MSG::WARNING> m_no_recsum{this, "RecSummary missing"};
    mutable Gaudi::Accumulators::MsgCounter<MSG::WARNING> m_no_odin{this, "ODIN missing"};
    mutable Gaudi::Accumulators::MsgCounter<MSG::ERROR>   m_is_neutral{this, "ProtoParticle is neutral!"};
  };

} // namespace ANNGlobalPID

using namespace ANNGlobalPID;

// Declaration of the Tool Factory
DECLARE_COMPONENT( ChargedProtoANNPIDTupleTool )

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
ChargedProtoANNPIDTupleTool::ChargedProtoANNPIDTupleTool( const std::string& type, const std::string& name,
                                                          const IInterface* parent )
    : extends( type, name, parent ) {

  // Job options
  declareProperty(
      "Variables",
      m_variables = {
          // General event variables
          "NumProtoParticles", "NumCaloHypos", "NumLongTracks", "NumDownstreamTracks", "NumUpstreamTracks",
          "NumVeloTracks", "NumTTracks", "NumGhosts", "NumPVs", "NumSPDHits", "NumRich1Hits", "NumRich2Hits",
          "NumMuonTracks", "NumMuonCoordsS0", "NumMuonCoordsS1", "NumMuonCoordsS2", "NumMuonCoordsS3",
          "NumMuonCoordsS4", "NumVeloClusters", "NumITClusters", "NumTTClusters", "NumOTClusters", "RunNumber",
          "EventNumber",
          // Tracking
          "TrackP", "TrackPt", "TrackChi2PerDof", "TrackType", "TrackHistory", "TrackNumDof", "TrackLikelihood",
          "TrackGhostProbability", "TrackMatchChi2", "TrackFitMatchChi2", "TrackCloneDist", "TrackFitVeloChi2",
          "TrackFitVeloNDoF", "TrackFitTChi2", "TrackFitTNDoF", "TrackDOCA", "TrackVertexX", "TrackVertexY",
          "TrackVertexZ", "TrackRich1EntryX", "TrackRich1EntryY", "TrackRich1EntryZ", "TrackRich2EntryX",
          "TrackRich2EntryY", "TrackRich2EntryZ", "TrackRich1ExitX", "TrackRich1ExitY", "TrackRich1ExitZ",
          "TrackRich2ExitX", "TrackRich2ExitY", "TrackRich2ExitZ",
          // Combined DLLs
          "CombDLLe", "CombDLLmu", "CombDLLpi", "CombDLLk", "CombDLLp", "CombDLLd",
          // RICH
          "RichUsedAero", "RichUsedR1Gas", "RichUsedR2Gas", "RichAboveElThres", "RichAboveMuThres", "RichAbovePiThres",
          "RichAboveKaThres", "RichAbovePrThres", "RichAboveDeThres", "RichDLLe", "RichDLLmu", "RichDLLpi", "RichDLLk",
          "RichDLLp", "RichDLLd", "RichDLLbt",
          // MUON
          "InAccMuon", "MuonMuLL", "MuonBkgLL", "MuonIsMuon", "MuonIsLooseMuon", "MuonNShared", "MuonMVA1", "MuonMVA2",
          "MuonMVA3", "MuonMVA4", "MuonChi2Corr",
          // CALO
          "CaloEoverP",
          // ECAL
          "InAccEcal", "CaloChargedSpd", "CaloChargedPrs", "CaloChargedEcal", "CaloElectronMatch", "CaloTrMatch",
          "CaloEcalE", "CaloEcalChi2", "CaloClusChi2", "EcalPIDe", "EcalPIDmu", "CaloTrajectoryL",
          // HCAL
          "InAccHcal", "CaloHcalE", "HcalPIDe", "HcalPIDmu",
          // PRS
          "InAccPrs", "CaloPrsE", "PrsPIDe",
          // SPD
          "InAccSpd", "CaloSpdE",
          // BREM
          "InAccBrem", "CaloNeutralSpd", "CaloNeutralPrs", "CaloNeutralEcal", "CaloBremMatch", "CaloBremChi2",
          "BremPIDe",
          // VELO
          "VeloCharge"} );
}

//=============================================================================

StatusCode ChargedProtoANNPIDTupleTool::fill( Tuples::Tuple&             tuple, //
                                              const LHCb::ProtoParticle* proto, //
                                              const LHCb::ParticleID     pid ) const {
  bool sc = true;

  // Get track
  const auto* track = proto->track();
  if ( !track ) {
    ++m_is_neutral;
    return StatusCode::FAILURE;
  }

  // Loop over reconstruction variables
  for ( const auto& i : m_inputs ) {
    // get the variable and fill ntuple
    sc &= tuple->column( i.first, (float)i.second( proto ) );
  }

  // PID info
  sc &= tuple->column( "RecoPIDcode", pid.pid() );

  // MC variables

  // First get the MCParticle, if associated
  const auto* mcPart = m_truth->mcParticle( track );
  sc &= tuple->column( "HasMC", mcPart != nullptr );
  sc &= tuple->column( "MCParticleType", mcPart ? mcPart->particleID().pid() : 0 );
  sc &= tuple->column( "MCParticleP", mcPart ? mcPart->p() : -999 );
  sc &= tuple->column( "MCParticlePt", mcPart ? mcPart->pt() : -999 );
  sc &= tuple->column( "MCVirtualMass", mcPart ? mcPart->virtualMass() : -999 );

  // MC history flags
  bool fromB( false ), fromD( false );
  // Parent MC particle
  const auto*  mcParent = ( mcPart ? mcPart->mother() : nullptr );
  unsigned int iCount( 0 ); // protect against infinite loops
  while ( mcParent && ++iCount < 99999 ) {
    const auto& pid = mcParent->particleID();
    if ( pid.hasBottom() && mcParent->particleID().isHadron() ) { fromB = true; }
    if ( pid.hasCharm() && mcParent->particleID().isHadron() ) { fromD = true; }
    mcParent = mcParent->mother();
  }
  // Save MC parent info
  sc &= tuple->column( "MCFromB", fromB );
  sc &= tuple->column( "MCFromD", fromD );

  // Get info on the MC vertex type
  const auto* mcVert = ( mcPart ? mcPart->originVertex() : nullptr );
  sc &= tuple->column( "MCVertexType", mcVert ? (int)mcVert->type() : -999 );
  sc &= tuple->column( "MCVertexX", mcVert ? mcVert->position().x() : -999.0 );
  sc &= tuple->column( "MCVertexY", mcVert ? mcVert->position().y() : -999.0 );
  sc &= tuple->column( "MCVertexZ", mcVert ? mcVert->position().z() : -999.0 );

  // return
  return StatusCode{sc};
}
