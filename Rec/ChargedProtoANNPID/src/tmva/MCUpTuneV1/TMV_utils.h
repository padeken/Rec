/*****************************************************************************\
* (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include <algorithm>
#include <array>
#include <numeric>

//
//  utilities to (re)implement MVA generated neural nets
//
//  please note that this version does not require the number of observables
//  to be known at compile time i.e. the operator() of `Transformer`,
//  `Layers`, accept spans with a dynamic (i.e. only known at runtime) extent
//
//

namespace TMV::Utils {

  inline bool validateInput( LHCb::span<const std::string> in, LHCb::span<const std::string_view> expected,
                             std::string_view fClassName ) {
    // sanity checks
    if ( in.size() != expected.size() ) {
      std::cout << "Problem in class \"" << fClassName << "\": mismatch in number of input values: " << in.size()
                << " != " << expected.size() << std::endl;
      return false;
    }

    // validate input variables
    bool fStatusIsClean = true;
    for ( size_t ivar = 0; ivar < in.size(); ivar++ ) {
      if ( in[ivar] != expected[ivar] ) {
        std::cout << "Problem in class \"" << fClassName << "\": mismatch in input variable names" << std::endl
                  << " for variable [" << ivar << "]: " << in[ivar] << " != " << expected[ivar] << std::endl;
        fStatusIsClean = false;
      }
    }
    return fStatusIsClean;
  }

  template <auto N, auto M>
  class Transformer {
    struct Transform {
      double scale, offset;
    };
    std::array<std::array<Transform, M>, N> transform{};

  public:
    constexpr Transformer( std::array<std::array<double, M>, N> const& min,
                           std::array<std::array<double, M>, N> const& max ) {
      for ( unsigned i = 0; i < N; ++i )
        for ( unsigned j = 0; j < M; ++j ) {
          double s        = 2.0 / ( max[i][j] - min[i][j] );
          transform[i][j] = {s, min[i][j] * s + 1.};
        }
    }

    template <typename Range>
    constexpr std::array<double, M> operator()( unsigned i, const Range& r ) const {
      assert( i < N );
      assert( size( r ) == M );
      std::array<double, M> w{};
      std::transform( transform[i].begin(), transform[i].end(), r.begin(), w.begin(),
                      []( const auto& tr, auto d ) { return tr.scale * d - tr.offset; } );
      return w;
    }
  };

  template <auto N, auto M, typename F>
  class Layer {
    std::array<std::array<double, M>, N> fMatrix;
    F                                    f;

  public:
    constexpr Layer( std::array<std::array<double, M>, N> m, F f ) : fMatrix{std::move( m )}, f{std::move( f )} {}
    constexpr Layer( std::array<double, M> m, F f ) : fMatrix{{std::move( m )}}, f{std::move( f )} {
      static_assert( N == 1 );
    }

    template <typename Range>
    constexpr auto operator()( Range&& r ) const {
      assert( size( r ) == M - 1 );
      if constexpr ( N == 1 ) {
        return f( std::inner_product( r.begin(), r.end(), fMatrix[0].begin(), fMatrix[0].back() ) );
      } else {
        std::array<double, N> out{};
        std::transform( fMatrix.begin(), fMatrix.end(), out.begin(), [&]( const auto& w ) {
          return f( std::inner_product( r.begin(), r.end(), w.begin(), w.back() ) );
        } );
        return out;
      }
    }

    template <typename R>
    friend constexpr auto operator|( R&& r, const Layer& l ) {
      return l( std::forward<R>( r ) );
    }
  };

  template <auto M, typename F>
  Layer( std::array<double, M>, F ) -> Layer<1, M, F>;

} // namespace TMV::Utils
