/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "ChargedProtoANNPIDToolBase.h"
#include "Event/MCParticle.h"
#include "RecInterfaces/IChargedProtoANNPIDTool.h"
#include <memory>
#include <mutex>
#include <unordered_map>

namespace ANNGlobalPID {

  /** @class ChargedProtoANNPIDTool ChargedProtoANNPIDTool.h
   *
   *  Tool to provide the ANN PID variable for a given object
   *
   *  @author Chris Jones   Christopher.Rob.Jones@cern.ch
   *  @date   2014-06-27
   */

  class ChargedProtoANNPIDTool final : public extends<ChargedProtoANNPIDToolBase, IChargedProtoANNPIDTool> {

  public:
    /// Standard constructor
    ChargedProtoANNPIDTool( const std::string& type, const std::string& name, const IInterface* parent )
        : extends( type, name, parent ) {
      // Turn off Tuple printing during finalize
      setProperty( "NTuplePrint", false ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    }

  public:
    // Compute the ANNPID value
    IChargedProtoANNPIDTool::RetType annPID( const LHCb::ProtoParticle* proto, //
                                             const LHCb::ParticleID&    pid,   //
                                             const std::string&         annPIDTune ) const override;

  private:
    /// Access the NetConfig object for a given configuration
    const NetConfig* getANN( const std::string& trackType, //
                             const std::string& pidType,   //
                             const std::string& netVersion ) const;

  private:
    /// Networks for a given ANNPID configuration
    mutable std::unordered_map<std::string, std::unique_ptr<NetConfig>> m_annNets;

    /// mutex lock for updating m_annNets
    mutable std::mutex m_updateLock;
  };

} // namespace ANNGlobalPID

using namespace ANNGlobalPID;

// Declaration of the Tool Factory
DECLARE_COMPONENT( ChargedProtoANNPIDTool )

//=============================================================================

IChargedProtoANNPIDTool::RetType ChargedProtoANNPIDTool::annPID( const LHCb::ProtoParticle* proto,
                                                                 const LHCb::ParticleID&    pid,
                                                                 const std::string&         annPIDTune ) const {
  IChargedProtoANNPIDTool::RetType annPID;

  // are we charged ....
  if ( proto && proto->track() ) {

    // Get the track type
    const auto trackType = LHCb::Track::TypesToString( proto->track()->type() );

    // PID type
    /* clang-format off */
    const auto pidType =
        ( pid.abspid() == 11         ? "Electron" :
          pid.abspid() == 13         ? "Muon"     :
          pid.abspid() == 211        ? "Pion"     :
          pid.abspid() == 321        ? "Kaon"     :
          pid.abspid() == 2212       ? "Proton"   :
          pid.abspid() == 1000010020 ? "Deuteron" :
          pid.abspid() == 0          ? "Ghost"    :
                                       "UNDEFINED" );
    /* clang-format on */

    // Get the ANN for the given configuration
    const auto* ann = getANN( trackType, pidType, annPIDTune );

    // Get the value of the ANN PID
    if ( ann ) {
      annPID.status = true;
      annPID.value  = ( ann->passCuts( proto ) ? ann->netHelper()->getOutput( proto ) : -2 );
    }

    if ( msgLevel( MSG::DEBUG ) ) {
      debug() << annPIDTune << " " << trackType << " " << pidType << " Status=" << annPID.status
              << " Value=" << annPID.value << endmsg;
    }
  }

  // return the value
  return annPID;
}

//=============================================================================

const ChargedProtoANNPIDTool::NetConfig*                      //
ChargedProtoANNPIDTool::getANN( const std::string& trackType, //
                                const std::string& pidType,   //
                                const std::string& netVersion ) const {

  // mva to return
  const ChargedProtoANNPIDTool::NetConfig* mva{nullptr};

  // ANN key
  const auto key = trackType + pidType + netVersion;

  // operate under a mutex lock...
  std::lock_guard lock( m_updateLock );

  // do we already have a network for this config ?
  const auto iANN = m_annNets.find( key );

  // Need to create it ?
  if ( UNLIKELY( iANN == m_annNets.end() ) ) {
    // create a new net helper and insert
    auto [iter, ok] = m_annNets.emplace( key, std::make_unique<NetConfig>( trackType, pidType, netVersion, this ) );
    // is this MVA valid ?
    if ( UNLIKELY( !ok || !iter->second || !iter->second->isOK() ) ) {
      Warning( "Problem creating ANNPID network for " + trackType + " " + pidType + " " + netVersion ).ignore();
      m_annNets.erase( key );
    } else {
      // return newly created MVAA
      mva = iter->second.get();
    }
  } else {
    // return found MVA
    mva = iANN->second.get();
  }

  return mva;
}

//=============================================================================
