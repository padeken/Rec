/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "CaloFuture2CaloFuture.h"
#include "CaloFutureInterfaces/ICaloFutureHypo2CaloFuture.h" // Interface
#include "CaloFutureUtils/CaloFutureAlgUtils.h"
#include "CaloFutureUtils/CellNeighbour.h"
#include "Event/CaloDigits_v2.h"
#include "GaudiAlg/GaudiTool.h"
#include "GaudiKernel/Plane3DTypes.h"
#include "GaudiKernel/SystemOfUnits.h"
#include "GaudiKernel/Vector3DTypes.h"

/** @class CaloFutureHypo2CaloFuture CaloFutureHypo2CaloFuture.h
 *
 *
 *  @author Olivier Deschamps
 *  @date   2008-09-11
 */
class CaloFutureHypo2CaloFuture final : public extends<CaloFuture2CaloFuture, LHCb::Calo::Interfaces::IHypo2Calo> {
public:
  /// Standard constructor
  using extends::extends;

  // energy
  double energy( const LHCb::CaloCluster& fromCluster, CaloCellCode::CaloIndex toCalo ) const override;
  double energy( const LHCb::CaloHypo& fromHypo, CaloCellCode::CaloIndex toCalo ) const override;

  // multiplicity
  int multiplicity( const LHCb::CaloCluster& fromCluster, CaloCellCode::CaloIndex toCalo ) const override;
  int multiplicity( const LHCb::CaloHypo& fromHypo, CaloCellCode::CaloIndex toCalo ) const override;

private:
  const LHCb::Event::Calo::Digits& fetch_digits( CaloCellCode::CaloIndex toCalo ) const {
    return *m_handles[toCalo].get();
  }
  // cellIDs
  using CaloFuture2CaloFuture::cellIDs;
  std::vector<LHCb::Event::Calo::Digit> cellIDs( const LHCb::CaloCluster& fromCluster, CaloCellCode::CaloIndex toCalo,
                                                 const LHCb::Event::Calo::Digits& digits,
                                                 LHCb::CaloCellID                 lineID ) const;
  std::vector<LHCb::Event::Calo::Digit> cellIDs( const LHCb::CaloHypo& fromHypo, CaloCellCode::CaloIndex toCalo,
                                                 const LHCb::Event::Calo::Digits& digits ) const;

  Gaudi::Property<bool>                                m_seed{this, "Seed", true};
  Gaudi::Property<bool>                                m_neighb{this, "AddNeighbors", true};
  Gaudi::Property<bool>                                m_line{this, "PhotonLine", true};
  Gaudi::Property<bool>                                m_whole{this, "WholeCluster", false};
  Gaudi::Property<int>                                 m_status{this, "StatusMask", 0x0};
  Gaudi::Property<float>                               m_x{this, "xTolerance", 5. * Gaudi::Units::mm};
  Gaudi::Property<float>                               m_y{this, "yTolerance", 5. * Gaudi::Units::mm};
  Map<DataObjectReadHandle<LHCb::Event::Calo::Digits>> m_handles{
      std::forward_as_tuple( this, "EcalDigitsLocation", LHCb::CaloFutureAlgUtils::CaloFutureDigitLocation( "Ecal" ) ),
      std::forward_as_tuple( this, "HcalDigitsLocation", LHCb::CaloFutureAlgUtils::CaloFutureDigitLocation( "Hcal" ) )};
};

// Declaration of the Tool Factory
DECLARE_COMPONENT( CaloFutureHypo2CaloFuture )

double CaloFutureHypo2CaloFuture::energy( const LHCb::CaloCluster& fromCluster, CaloCellCode::CaloIndex toCalo ) const {
  const auto& output = cellIDs( fromCluster, toCalo, fetch_digits( toCalo ) );
  return std::accumulate( output.begin(), output.end(), 0., []( double e, const auto& d ) { return e + d.energy(); } );
}
double CaloFutureHypo2CaloFuture::energy( const LHCb::CaloHypo& fromHypo, CaloCellCode::CaloIndex toCalo ) const {
  const auto& output = cellIDs( fromHypo, toCalo, fetch_digits( toCalo ) );
  return std::accumulate( output.begin(), output.end(), 0., []( double e, const auto& d ) { return e + d.energy(); } );
}
int CaloFutureHypo2CaloFuture::multiplicity( const LHCb::CaloCluster& fromCluster,
                                             CaloCellCode::CaloIndex  toCalo ) const {
  return cellIDs( fromCluster, toCalo, fetch_digits( toCalo ) ).size();
}
int CaloFutureHypo2CaloFuture::multiplicity( const LHCb::CaloHypo& fromHypo, CaloCellCode::CaloIndex toCalo ) const {
  return cellIDs( fromHypo, toCalo, fetch_digits( toCalo ) ).size();
}

//=============================================================================
std::vector<LHCb::Event::Calo::Digit>
CaloFutureHypo2CaloFuture::cellIDs( const LHCb::CaloHypo& fromHypo, CaloCellCode::CaloIndex toCalo,
                                    const LHCb::Event::Calo::Digits& digits ) const {
  if ( UNLIKELY( msgLevel( MSG::DEBUG ) ) )
    debug() << "Matching CaloHypo to " << toCalo << " hypo energy = " << fromHypo.e() << endmsg;

  // get the cluster
  const LHCb::CaloCluster* cluster = LHCb::CaloFutureAlgUtils::ClusterFromHypo( fromHypo );

  if ( !cluster ) {
    Error( "No valid cluster!" ).ignore();
    return {};
  }

  LHCb::CaloCellID seedID   = cluster->seed();
  auto             fromCalo = seedID.calo();
  if ( UNLIKELY( msgLevel( MSG::DEBUG ) ) )
    debug() << "Cluster seed " << seedID << " " << m_det[fromCalo]->cellCenter( seedID ) << endmsg;

  if ( m_whole ) { return CaloFuture2CaloFuture::cellIDs( *cluster, toCalo, digits ); }

  auto lineID = LHCb::CaloCellID();
  if ( m_line && fromHypo.position() ) {
    lineID = intersect( toCalo, {fromHypo.position()->x(), fromHypo.position()->y(), fromHypo.position()->z()} );
  }
  return cellIDs( *cluster, toCalo, digits, lineID );
}

std::vector<LHCb::Event::Calo::Digit> CaloFutureHypo2CaloFuture::cellIDs( const LHCb::CaloCluster&         fromCluster,
                                                                          CaloCellCode::CaloIndex          toCalo,
                                                                          const LHCb::Event::Calo::Digits& digits,
                                                                          LHCb::CaloCellID lineID ) const {
  std::vector<LHCb::Event::Calo::Digit> output;
  LHCb::CaloCellID                      seedID   = fromCluster.seed();
  auto                                  fromCalo = seedID.calo();

  if ( UNLIKELY( msgLevel( MSG::DEBUG ) ) )
    debug() << "-----  cluster energy " << fromCluster.e() << " " << seedID << endmsg;
  CellNeighbour neighbour;
  neighbour.setDet( m_det[fromCalo] );

  // matching cluster
  for ( const LHCb::CaloClusterEntry& entry : fromCluster.entries() ) {
    LHCb::CaloCellID cellID = entry.digit()->cellID();
    if ( !( m_seed && entry.status().test( LHCb::CaloDigitStatus::Mask::SeedCell ) ) &&
         !( m_seed && m_neighb && neighbour( seedID, cellID ) != 0. ) &&
         entry.status().noneOf( LHCb::CaloDigitStatus::Status( m_status.value() ) ) && !( m_whole ) )
      continue;
    std::cout << ">>two " << entry.digit()->cellID() << '\n';
    output = cellIDs_( entry.digit()->cellID(), toCalo, digits, std::move( output ) );
    std::cout << ">>two\n";
    if ( UNLIKELY( msgLevel( MSG::DEBUG ) ) )
      debug() << toCalo << ":  digit is selected in front of the cluster : " << cellID << "/" << seedID << " "
              << output.size() << endmsg;
  }
  // photon line
  if ( m_line ) {
    auto point = Gaudi::XYZPoint();
    if ( !lineID ) {
      const auto& point = fromCluster.position();
      lineID            = intersect( toCalo, {point.x(), point.y(), point.z()} );
    }
    if ( lineID ) {
      assert( lineID.calo() == toCalo );
      output = addCell( lineID, digits, std::move( output ) );
      if ( UNLIKELY( msgLevel( MSG::DEBUG ) ) )
        debug() << toCalo << " : digit is selected in the photon line : " << lineID << "/" << seedID << " "
                << output.size() << endmsg;
      if ( m_neighb ) {
        for ( const auto& n : m_det[toCalo]->neighborCells( lineID ) ) {
          assert( n.calo() == toCalo );
          double                halfCell   = m_det[toCalo]->cellSize( n ) * 0.5;
          const Gaudi::XYZPoint cellCenter = m_det[toCalo]->cellCenter( n );
          if ( UNLIKELY( msgLevel( MSG::DEBUG ) ) )
            debug() << n << " Point : (" << point.X() << "," << point.Y() << " Cell :  ( " << cellCenter.X() << ","
                    << cellCenter.Y() << " size/2  : " << halfCell << " Tolerance : " << m_x << "/" << m_y << endmsg;
          if ( fabs( point.X() - cellCenter.X() ) < ( halfCell + m_x ) &&
               fabs( point.Y() - cellCenter.Y() ) < ( halfCell + m_y ) ) {
            output = addCell( n, digits, std::move( output ) );
            if ( UNLIKELY( msgLevel( MSG::DEBUG ) ) )
              debug() << toCalo << " : digit is selected in the photon line neighborhood : " << n << "/" << seedID
                      << " " << output.size() << endmsg;
          }
        }
      }
    }
  }
  return output;
}
