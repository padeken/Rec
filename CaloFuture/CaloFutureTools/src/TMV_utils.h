/*****************************************************************************\
* (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include "Kernel/STLExtensions.h"
#include <algorithm>
#include <array>
#include <numeric>
#include <stdexcept>
#include <string>

//
//  utilities to (re)implement MVA generated neural nets
//
//  please note that this version requires the number of observables
//  to be known at compile time i.e. the operator() of `Transformer`,
//  `Layers`, 'MVA' only accept spans with a compile time known extent.
//

namespace TMV::Utils {

  template <typename CName, typename... Names>
  class Validator {
    CName                fClassName;
    std::tuple<Names...> fNames;

  public:
    constexpr Validator( CName className, std::tuple<Names...> names )
        : fClassName{std::move( className )}, fNames{std::move( names )} {}

    void operator()( LHCb::span<const std::string_view> in ) const {
      auto cmp = [&]( auto lhs, auto rhs ) {
        bool r = ( lhs == rhs );
        if ( !r )
          std::cout << "Problem in class \"" << fClassName << "\": mismatch in input variable names\n"
                    << lhs << " != " << rhs << '\n';
        return r;
      };

      bool ok = ( in.size() == sizeof...( Names ) );
      if ( !ok ) {
        std::cout << "Problem in class \"" << fClassName << "\": expected " << sizeof...( Names )
                  << " variables, but got " << in.size() << std::endl;
      };
      ok = ok && std::apply( [&, i = begin( in )]( auto... n ) mutable { return ( cmp( *i++, n ) && ... ); }, fNames );
      if ( !ok ) throw std::runtime_error( std::string{"bad configuration for TMV "}.append( fClassName ) );
    }
  };

  template <auto N, auto M>
  class Transformer {
    struct Transform {
      double scale, offset;
    };
    std::array<std::array<Transform, M>, N> transform{};

  public:
    constexpr Transformer( std::array<std::array<double, M>, N> const& min,
                           std::array<std::array<double, M>, N> const& max ) {
      for ( unsigned i = 0; i < N; ++i )
        for ( unsigned j = 0; j < M; ++j ) {
          double s        = 2.0 / ( max[i][j] - min[i][j] );
          transform[i][j] = {s, min[i][j] * s + 1.};
        }
    }
    constexpr Transformer( std::array<double, M> const& min,
                           std::array<double, M> const& max ) /* C++20: requires( N==1 ) */
        : Transformer{std::array<std::array<double, M>, N>{min}, std::array<std::array<double, M>, N>{max}} {
      static_assert( N == 1 );
    }

    constexpr static auto extent() { return M; }

    constexpr std::array<double, M> operator()( unsigned i, LHCb::span<const double, M> r ) const {
      assert( i < N );
      std::array<double, M> w{};
      std::transform( transform[i].begin(), transform[i].end(), r.begin(), w.begin(),
                      []( const auto& tr, auto d ) { return tr.scale * d - tr.offset; } );
      return w;
    }
  };
  template <auto M>
  Transformer( std::array<double, M> const&, std::array<double, M> const& ) -> Transformer<1, M>;

  template <auto N, auto M, typename F>
  class Layer {
    std::array<std::array<double, M>, N> fMatrix;
    F                                    f;

  public:
    constexpr Layer( std::array<std::array<double, M>, N> m, F f ) : fMatrix{std::move( m )}, f{std::move( f )} {}
    constexpr Layer( std::array<double, M> m, F f ) /* C++20: requires( N==1 ) */
        : fMatrix{{std::move( m )}}, f{std::move( f )} {
      static_assert( N == 1 );
    }

    constexpr auto operator()( LHCb::span<const double, M - 1> r ) const {
      if constexpr ( N == 1 ) {
        return f( std::inner_product( r.begin(), r.end(), fMatrix[0].begin(), fMatrix[0].back() ) );
      } else {
        std::array<double, N> out{};
        std::transform( fMatrix.begin(), fMatrix.end(), out.begin(), [&]( const auto& w ) {
          return f( std::inner_product( r.begin(), r.end(), w.begin(), w.back() ) );
        } );
        return out;
      }
    }

    template <typename R>
    friend constexpr auto operator|( R&& r, const Layer& l ) {
      return l( std::forward<R>( r ) );
    }
  };

  template <auto M, typename F>
  Layer( std::array<double, M>, F ) -> Layer<1, M, F>;

  template <typename Validator, typename Transformation, typename... Layers>
  class MVA {
    Validator             validator;
    Transformation        transformer;
    std::tuple<Layers...> layers;
    size_t                sig;
    static constexpr auto N = Transformation::extent();

  public:
    constexpr MVA( Validator validator, Transformation transformer, size_t sig, Layers... layers )
        : validator{std::move( validator )}
        , transformer{std::move( transformer )}
        , layers{std::move( layers )...}
        , sig{sig} {}

    void validate( LHCb::span<const std::string_view> in ) const { validator( in ); }

    template <typename Float>
    constexpr auto operator()( LHCb::span<const Float, N> input ) const {
      return std::apply( [&]( auto&&... l ) { return ( transformer( sig, input ) | ... | l ); }, layers );
    }
  };

} // namespace TMV::Utils
