/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Class: ReadMLPMiddle

/* configuration options =====================================================

#GEN -*-*-*-*-*-*-*-*-*-*-*- general info -*-*-*-*-*-*-*-*-*-*-*-

Method         : MLP::MLP
TMVA Release   : 4.2.1         [262657]
ROOT Release   : 6.16/00       [397312]
Creator        : calvom
Date           : Tue Apr  9 15:01:05 2019
Host           : Linux lcgapp-centos7-x86-64-37.cern.ch 3.10.0-693.21.1.el7.x86_64 #1 SMP Wed Mar 7 19:03:37 UTC 2018
x86_64 x86_64 x86_64 GNU/Linux Dir            : /eos/home-c/calvom/GammaPi0Upgrade Training events: 11741 Analysis type
: [Classification]


#OPT -*-*-*-*-*-*-*-*-*-*-*-*- options -*-*-*-*-*-*-*-*-*-*-*-*-

# Set by User:
NCycles: "600" [Number of training cycles]
HiddenLayers: "N" [Specification of hidden layer architecture]
NeuronType: "tanh" [Neuron activation function type]
V: "False" [Verbose output (short form of "VerbosityLevel" below - overrides the latter one)]
VarTransform: "N" [List of variable transformations performed before training, e.g.,
"D_Background,P_Signal,G,N_AllClasses" for: "Decorrelation, PCA-transformation, Gaussianisation, Normalisation, each for
the given class of events ('AllClasses' denotes all events of all classes, if no class indication is given, 'All' is
assumed)"] H: "True" [Print method-specific help message] TestRate: "5" [Test for overtraining performed at each #th
epochs] UseRegulator: "False" [Use regulator to avoid over-training] # Default: RandomSeed: "1" [Random seed for initial
synapse weights (0 means unique seed for each run; default value '1')] EstimatorType: "CE" [MSE (Mean Square Estimator)
for Gaussian Likelihood or CE(Cross-Entropy) for Bernoulli Likelihood] NeuronInputType: "sum" [Neuron input function
type] VerbosityLevel: "Default" [Verbosity level] CreateMVAPdfs: "False" [Create PDFs for classifier outputs (signal and
background)] IgnoreNegWeightsInTraining: "False" [Events with negative weights are ignored in the training (but are
included for testing and performance evaluation)] TrainingMethod: "BP" [Train with Back-Propagation (BP), BFGS Algorithm
(BFGS), or Genetic Algorithm (GA - slower and worse)] LearningRate: "2.000000e-02" [ANN learning rate parameter]
DecayRate: "1.000000e-02" [Decay rate for learning parameter]
EpochMonitoring: "False" [Provide epoch-wise monitoring plots according to TestRate (caution: causes big ROOT output
file!)] Sampling: "1.000000e+00" [Only 'Sampling' (randomly selected) events are trained each epoch] SamplingEpoch:
"1.000000e+00" [Sampling is used for the first 'SamplingEpoch' epochs, afterwards, all events are taken for training]
SamplingImportance: "1.000000e+00" [ The sampling weights of events in epochs which successful (worse estimator than
before) are multiplied with SamplingImportance, else they are divided.] SamplingTraining: "True" [The training sample is
sampled] SamplingTesting: "False" [The testing sample is sampled] ResetStep: "50" [How often BFGS should reset history]
Tau: "3.000000e+00" [LineSearch "size step"]
BPMode: "sequential" [Back-propagation learning mode: sequential or batch]
BatchSize: "-1" [Batch size: number of events/batch, only set if in Batch Mode, -1 for BatchSize=number_of_events]
ConvergenceImprove: "1.000000e-30" [Minimum improvement which counts as improvement (<0 means automatic convergence
check is turned off)] ConvergenceTests: "-1" [Number of steps (without improvement) required for convergence (<0 means
automatic convergence check is turned off)] UpdateLimit: "10000" [Maximum times of regulator update] CalculateErrors:
"False" [Calculates inverse Hessian matrix at the end of the training to be able to calculate the uncertainties of an
MVA value] WeightRange: "1.000000e+00" [Take the events for the estimator calculations from small deviations from the
desired value to large deviations only over the weight range]
##


#VAR -*-*-*-*-*-*-*-*-*-*-*-* variables *-*-*-*-*-*-*-*-*-*-*-*-

NVar 6
isPhfr2                       isPhfr2                       isPhfr2                       isPhfr2 'F'
[372.859405518,12326.1298828] isPhfr2r4                     isPhfr2r4                     isPhfr2r4 isPhfr2r4 'F'
[0.240888416767,0.967787504196] abs(isPhasym)                 abs_isPhasym_                 abs(isPhasym) abs(isPhasym)
'F'    [0,0.843874514103] isPhkappa                     isPhkappa                     isPhkappa isPhkappa 'F'
[0,0.862615466118] isPhEseed                     isPhEseed                     isPhEseed                     isPhEseed
'F'    [0.246634334326,0.975922763348] isPhE2                        isPhE2                        isPhE2 isPhE2 'F'
[0.481120288372,1.80900859833] NSpec 0


============================================================================ */

#include "Kernel/STLExtensions.h"
#include "TMV_utils.h"
#include <array>
#include <cmath>
#include <string_view>

namespace Data::ReadMLPMiddle {
  namespace {
    constexpr auto ActivationFnc = []( double x ) {
      // fast hyperbolic tan approximation
      if ( x > 4.97 ) return 1.f;
      if ( x < -4.97 ) return -1.f;
      float x2 = x * x;
      float a  = x * ( 135135.0f + x2 * ( 17325.0f + x2 * ( 378.0f + x2 ) ) );
      float b  = 135135.0f + x2 * ( 62370.0f + x2 * ( 3150.0f + x2 * 28.0f ) );
      return a / b;
    };
    constexpr auto OutputActivationFnc = []( double x ) {
      // sigmoid
      return 1.0 / ( 1.0 + exp( -x ) );
    };

    // Normalization transformation
    constexpr auto fMin_1 = std::array{
        std::array{372.859405518, 0.240888416767, 0., 0., 0.246634334326, 0.491204738617},
        std::array{726.803344727, 0.345340937376, 4.61978415842e-05, 0.00523260282353, 0.261580228806, 0.481120288372},
        std::array{372.859405518, 0.240888416767, 0., 0., 0.246634334326, 0.481120288372}};

    constexpr auto fMax_1 = std::array{
        std::array{12326.1298828, 0.967787504196, 0.788577377796, 0.81840211153, 0.96903437376, 1.80105221272},
        std::array{10098.6914062, 0.936216890812, 0.843874514103, 0.862615466118, 0.975922763348, 1.80900859833},
        std::array{12326.1298828, 0.967787504196, 0.843874514103, 0.862615466118, 0.975922763348, 1.80900859833}};

    // build network structure
    // weight matrix from layer 0 to 1
    constexpr auto fWeightMatrix0to1 =
        std::array{std::array{-9.68953691945143, -2.10396456546594, -0.203574403316528, -0.289548506488541,
                              -0.632179560742408, 0.29860093288276, -7.81520691315301},
                   std::array{0.834111829013477, 1.57585587052325, -0.102795227888483, 0.171581795138297,
                              0.128318942109307, 4.35082872637589, 1.68957832864602},
                   std::array{0.564892370204254, -1.3062979327399, 3.26660818989507, -2.09228423314915,
                              -0.517092755596251, 1.53581114802376, 0.236007725366366},
                   std::array{1.90672241947199, -3.54880793282957, -0.156796991415895, 0.889580621798649,
                              0.347223491260298, 4.752176835059, 4.62311044659023},
                   std::array{-2.85596002795757, -2.766364902528, 0.923054388515109, -0.74130494478177,
                              -2.05884898859365, -0.644829911826026, 1.7852865678588},
                   std::array{-3.67351196151566, -3.47451579633598, 2.41131636843343, -0.883794331954949,
                              -0.789670341464348, -0.699420589495007, 1.40799243640316}};
    // weight matrix from layer 1 to 2
    constexpr auto fWeightMatrix1to2 =
        std::array{4.4857672489425,   1.74906609457621, -3.36363308221584, -1.78686535488863,
                   -2.14274782777877, 1.20042229459793, 2.13127487894641};

    // the training input variables
    constexpr auto validator = TMV::Utils::Validator{
        "ReadMLPMiddle", std::tuple{"isPhfr2", "isPhfr2r4", "abs(isPhasym)", "isPhkappa", "isPhEseed", "isPhE2"}};
    // Normalization transformation
    constexpr auto transformer = TMV::Utils::Transformer{fMin_1, fMax_1};
    constexpr auto l0To1       = TMV::Utils::Layer{fWeightMatrix0to1, ActivationFnc};
    constexpr auto l1To2       = TMV::Utils::Layer{fWeightMatrix1to2, OutputActivationFnc};
    constexpr auto MVA         = TMV::Utils::MVA{validator, transformer, 2, l0To1, l1To2};

  } // namespace
} // namespace Data::ReadMLPMiddle

struct ReadMLPMiddle final {

  // constructor
  ReadMLPMiddle( LHCb::span<const std::string_view, 6> theInputVars ) {
    Data::ReadMLPMiddle::MVA.validate( theInputVars );
  }

  // the classifier response
  // "inputValues" is a vector of input values in the same order as the
  // variables given to the constructor
  static constexpr double GetMvaValue( LHCb::span<const double, 6> inputValues ) {
    return Data::ReadMLPMiddle::MVA( inputValues );
  }
};
