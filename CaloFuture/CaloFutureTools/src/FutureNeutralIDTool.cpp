/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "CaloDet/DeCalorimeter.h"
#include "CaloFutureInterfaces/ICaloFutureHypoEstimator.h"
#include "CaloFutureUtils/CaloFutureAlgUtils.h"
#include "CaloFutureUtils/CaloMomentum.h"
#include "Event/CaloHypo.h"
#include "GaudiAlg/GaudiTool.h"
#include "INeutralIDTool.h"
#include "TMV_MLP_E.C"
#include "TMV_MLP_H.C"

/** @class neutralIDTool neutralIDTool.h
 *
 *
 *  @author Mostafa HOBALLAH
 *  @date   2013-07-25
 */
namespace LHCb::Calo {

  namespace {
    const ReadMLPE s_reader0{std::array<std::string_view, 3>{"ShowerShape", "E19", "trClmatch"}};
    const ReadMLPH s_reader1{std::array<std::string_view, 4>{"ShowerShape", "E19", "Hcal2Ecal", "trClmatch"}};
  } // namespace

  class NeutralIDTool final : public extends<GaudiTool, Interfaces::INeutralID> {
  public:
    /// Standard constructor
    using extends::extends;

    std::optional<double> isNotE( const LHCb::CaloHypo&                      hypo,
                                  const Interfaces::INeutralID::Observables& v ) const override {
      if ( Momentum( &hypo ).pt() <= m_minPt || !LHCb::CaloFutureAlgUtils::ClusterFromHypo( hypo, false ) )
        return std::nullopt;
      return isNotE( v );
    }
    std::optional<double> isNotH( const LHCb::CaloHypo&                      hypo,
                                  const Interfaces::INeutralID::Observables& v ) const override {
      if ( Momentum( &hypo ).pt() <= m_minPt || !LHCb::CaloFutureAlgUtils::ClusterFromHypo( hypo, false ) )
        return std::nullopt;
      return isNotH( v );
    }

    double isNotE( const Interfaces::INeutralID::Observables& v ) const override {
      return s_reader0.GetMvaValue( std::array{v.sprd, v.e19, v.clmatch} );
    }
    double isNotH( const Interfaces::INeutralID::Observables& v ) const override {
      return s_reader1.GetMvaValue( std::array{v.sprd, v.e19, v.hclecl, v.clmatch} );
    }

  private:
    Gaudi::Property<float> m_minPt{this, "MinPt", 75.};
  };

  // Declaration of the Tool Factory
  DECLARE_COMPONENT_WITH_ID( NeutralIDTool, "FutureNeutralIDTool" )

} // namespace LHCb::Calo
