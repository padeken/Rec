/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "CaloDet/DeCalorimeter.h"
#include "CaloFutureInterfaces/ICaloFutureElectron.h"
#include "CaloFutureInterfaces/ICaloFutureHypoEstimator.h" // Interface
#include "CaloFutureUtils/CaloFuture2Track.h"
#include "CaloFutureUtils/CaloFutureAlgUtils.h"
#include "CaloFutureUtils/CaloMomentum.h"
#include "CaloFutureUtils/ClusterFunctors.h"
#include "Event/CaloDataFunctor.h"
#include "Event/ProtoParticle.h"
#include "Event/Track.h"
#include "GaudiAlg/GaudiTool.h"
#include "IGammaPi0SeparationTool.h"
#include "INeutralIDTool.h"
#include "Relations/IRelationWeighted.h"
#include "Relations/IRelationWeighted2D.h"
#include "Relations/Relation1D.h"
#include "Relations/Relation2D.h"
#include "Relations/RelationWeighted2D.h"

//-----------------------------------------------------------------------------
// Implementation file for class : CaloFutureHypoEstimator
//
// 2010-08-18 : Olivier Deschamps
//-----------------------------------------------------------------------------

/** @class CaloFutureHypoEstimator CaloFutureHypoEstimator.h
 *
 *
 *  @author Olivier Deschamps
 *  @date   2010-08-18
 */

namespace LHCb::Calo {

  class HypoEstimator : public extends<GaudiTool, Interfaces::IHypoEstimator> {
  public:
    /// Standard constructor
    using extends::extends;

    StatusCode initialize() override;

    std::optional<double>            data( const LHCb::CaloHypo& hypo, Enum::DataType type ) const override;
    std::map<Enum::DataType, double> get_data( const LHCb::CaloHypo& hypo ) const override;

    Interfaces::IHypo2Calo* hypo2Calo() override { return m_toCaloFuture.get(); }

    const LHCb::Track* toTrack( const CaloHypo& hypo, Enum::MatchType match ) const override;

    StatusCode _setProperty( const std::string& p, const std::string& v ) override { return setProperty( p, v ); };

  private:
    using HypoTrTable2D    = LHCb::RelationWeighted2D<LHCb::CaloHypo, LHCb::Track, float>;
    using ClusterTrTable2D = LHCb::RelationWeighted2D<LHCb::CaloCellID, LHCb::Track, float>;
    using HypoEvalTable    = LHCb::Relation1D<LHCb::CaloHypo, float>;

    Gaudi::Property<bool> m_extrapol{this, "Extrapolation", true};
    Gaudi::Property<bool> m_seed{this, "AddSeed", false};
    Gaudi::Property<bool> m_neig{this, "AddNeighbors", false};

    DataObjectReadHandle<ClusterTrTable2D> m_clusterTrTable{
        this, "ClusterMatchLocation", LHCb::CaloFutureAlgUtils::CaloFutureIdLocation( "ClusterMatch" )};
    DataObjectReadHandle<HypoTrTable2D> m_electronTrTable{
        this, "ElectronMatchLocation", LHCb::CaloFutureAlgUtils::CaloFutureIdLocation( "ElectronMatch" )};
    DataObjectReadHandle<HypoTrTable2D> m_bremTrTable{this, "BremMatchLocation",
                                                      LHCb::CaloFutureAlgUtils::CaloFutureIdLocation( "BremMatch" )};

    Gaudi::Property<bool> m_skipC{this, "SkipChargedID", false};
    Gaudi::Property<bool> m_skipN{this, "SkipNeutralID", false};
    Gaudi::Property<bool> m_skipCl{this, "SkipClusterMatch", false};

    mutable Gaudi::Accumulators::Counter<> m_nEmptyCluster{this, "Empty cluster"};
    mutable Gaudi::Accumulators::Counter<> m_nSeedPointsToNull{this, "Seed points to NULL"};

    ToolHandle<Interfaces::IHypo2Calo>          m_toCaloFuture{this, "Hypo2Calo", "CaloFutureHypo2CaloFuture"};
    ToolHandle<Interfaces::IElectron>           m_electron{this, "Electron", "CaloFutureElectron"};
    ToolHandle<Interfaces::IGammaPi0Separation> m_GammaPi0{this, "Pi0Separation", "FutureGammaPi0SeparationTool"};
    ToolHandle<Interfaces::IGammaPi0Separation> m_GammaPi0XGB{this, "Pi0SeparationXGB", "FutureGammaPi0XGBoostTool"};
    ToolHandle<Interfaces::INeutralID>          m_neutralID{this, "NeutralID", "FutureNeutralIDTool"};

    DeCalorimeter* m_ecal = nullptr;
  };

  // Declaration of the Tool Factory
  DECLARE_COMPONENT_WITH_ID( HypoEstimator, "CaloFutureHypoEstimator" )

  StatusCode HypoEstimator::initialize() {
    StatusCode sc = GaudiTool::initialize(); // must be executed first

    m_toCaloFuture.retrieve().ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    auto& h2c = dynamic_cast<IProperty&>( *m_toCaloFuture );
    h2c.setProperty( "Seed", m_seed ? "true" : "false" ).ignore();
    h2c.setProperty( "PhotonLine", m_extrapol ? "true" : "false" ).ignore();
    h2c.setProperty( "AddNeighbors", m_neig ? "true" : "false" ).ignore();

    m_ecal = getDet<DeCalorimeter>( DeCalorimeterLocation::Ecal );

    return sc;
  }

  //=============================================================================

  // ------------
  std::optional<double> HypoEstimator::data( const LHCb::CaloHypo& hypo, LHCb::Calo::Enum::DataType type ) const {
    auto data = get_data( hypo );
    if ( auto it = data.find( type ); it != data.end() ) return it->second;
    return {};
  }

  // ------------ FROM HYPO
  std::map<Enum::DataType, double> HypoEstimator::get_data( const LHCb::CaloHypo& hypo ) const {
    std::map<Enum::DataType, double> data{};
    using namespace LHCb::Calo::Enum;

    LHCb::Calo::Momentum mom( &hypo );
    data[DataType::HypoE]  = mom.e();
    data[DataType::HypoEt] = mom.pt();
    data[DataType::HypoM] =
        mom.mass(); // for mergedPi0 hypothesis only (->for all clusters - toDo in CaloFutureMergedPi0Alg)

    // electron matching
    if ( !m_skipC ) {

      if ( const auto* tbl = m_electronTrTable.get(); tbl ) {
        if ( const auto range = tbl->relations( &hypo ); !range.empty() ) {
          data[DataType::ElectronMatch] = range.front().weight();
          if ( range.front() ) {
            LHCb::ProtoParticle dummy;
            dummy.setTrack( range.front() );
            dummy.addToCalo( &hypo );
            // CaloFutureElectron->caloTrajectory must be after addToCalo
            auto traj = m_electron->caloTrajectoryL( dummy, CaloPlane::ShowerMax );
            if ( traj ) data[DataType::TrajectoryL] = *traj;
          }
        }
      }

      // brem matching
      if ( const auto* tbl = m_bremTrTable.get(); tbl ) {
        if ( const auto range = tbl->relations( &hypo ); !range.empty() ) {
          data[DataType::BremMatch] = range.front().weight();
        }
      }
    }

    auto insert_if = [&]( DataType key, std::optional<double> val ) {
      if ( val ) data[key] = *val;
    };

    if ( auto obs = m_GammaPi0->observables( hypo ); obs ) {
      // gamma/pi0 separation
      insert_if( DataType::isPhoton, m_GammaPi0->isPhoton( obs.value() ) );
      // 1- Ecal variables :
      data[DataType::isPhotonEcl]   = obs->Ecl;
      data[DataType::isPhotonFr2]   = obs->fr2;
      data[DataType::isPhotonFr2r4] = obs->fr2r4;
      data[DataType::isPhotonAsym]  = obs->fasym;
      data[DataType::isPhotonKappa] = obs->fkappa;
      data[DataType::isPhotonEseed] = obs->Eseed;
      data[DataType::isPhotonE2]    = obs->E2;
    }

    insert_if( DataType::isPhotonXGB, m_GammaPi0XGB->isPhoton( hypo ) );
    // 1- Ecal variables :

    // link 2 cluster :
    if ( const LHCb::CaloCluster* cluster = LHCb::CaloFutureAlgUtils::ClusterFromHypo( hypo ); cluster ) {
      using namespace LHCb::Calo::Enum;

      if ( cluster->entries().empty() ) {
        ++m_nEmptyCluster;
        return {};
      }
      if ( auto iseed = LHCb::ClusterFunctors::locateDigit( cluster->entries().begin(), cluster->entries().end(),
                                                            LHCb::CaloDigitStatus::Mask::SeedCell );
           iseed != cluster->entries().end() ) {

        const LHCb::CaloDigit* seed = iseed->digit();
        if ( !seed ) {
          ++m_nSeedPointsToNull;
          return {};
        }

        double eEcal             = cluster->e();
        data[DataType::ClusterE] = eEcal;
        //
        double          cSize      = m_ecal->cellSize( cluster->seed() );
        Gaudi::XYZPoint cCenter    = m_ecal->cellCenter( cluster->seed() );
        double          asX        = ( cluster->position().x() - cCenter.x() ) / cSize;
        double          asY        = ( cluster->position().y() - cCenter.y() ) / cSize;
        data[DataType::ClusterAsX] = asX;
        data[DataType::ClusterAsY] = asY;

        data[DataType::E1]     = seed->e();
        auto   it              = data.find( DataType::HypoE );
        double eHypo           = ( it != data.end() ) ? it->second : 0;
        data[DataType::E1Hypo] = eHypo > 0. ? ( seed->e() ) / eHypo : -1.;
        LHCb::CaloCellID sid   = seed->cellID();
        data[DataType::CellID] = sid.all();
        data[DataType::Spread] = cluster->position().spread()( 1, 1 ) + cluster->position().spread()( 0, 0 );
        // E4
        std::array<double, 4> e4s       = {0, 0, 0, 0};
        double                e9        = 0.;
        double                ee9       = 0.; // full cluster energy without fraction applied
        double                e2        = 0.;
        bool                  hasShared = false;
        int                   code      = 0.;
        int                   mult      = 0.;
        int                   nsat      = 0; // number of saturated cells
        for ( const auto& ie : cluster->entries() ) {
          const LHCb::CaloDigit* dig = ie.digit();
          if ( !dig ) continue;
          double ecel = dig->e() * ie.fraction();
          if ( ie.status().anyOf(
                   {LHCb::CaloDigitStatus::Mask::UseForEnergy, LHCb::CaloDigitStatus::Mask::UseForCovariance} ) &&
               m_ecal->isSaturated( dig->e(), dig->cellID() ) )
            nsat++;
          LHCb::CaloCellID id = dig->cellID();
          if ( id.area() != sid.area() || abs( (int)id.col() - (int)sid.col() ) > 1 ||
               abs( (int)id.row() - (int)sid.row() ) > 1 )
            continue;
          if ( id.col() <= sid.col() && id.row() >= sid.row() ) e4s[0] += ecel;
          if ( id.col() >= sid.col() && id.row() >= sid.row() ) e4s[1] += ecel;
          if ( id.col() >= sid.col() && id.row() <= sid.row() ) e4s[2] += ecel;
          if ( id.col() <= sid.col() && id.row() <= sid.row() ) e4s[3] += ecel;
          e9 += ecel;
          // new info
          ee9 += dig->e();
          mult++;
          if ( ie.status().test( LHCb::CaloDigitStatus::Mask::SharedCell ) ) hasShared = true;
          if ( !( id == sid ) && ecel > e2 ) {
            e2     = ecel;
            int dc = (int)id.col() - (int)sid.col() + 1;
            int dr = (int)id.row() - (int)sid.row() + 1;
            code   = 3 * dr + dc;
          }
        }
        data[DataType::Saturation] = nsat;
        double e4max =
            std::accumulate( e4s.begin(), e4s.end(), 0., []( double mx, const auto& i ) { return std::max( mx, i ); } );

        code = mult * 10 + code;
        if ( hasShared ) code *= -1;
        data[DataType::ClusterCode] = code;
        data[DataType::ClusterFrac] = ( e9 > 0. ? e9 / ee9 : -1 );
        data[DataType::E4]          = e4max;
        data[DataType::E9]          = e9;
        data[DataType::E49]         = ( e9 > 0. ? e4max / e9 : 0. );
        data[DataType::E19]         = ( e9 > 0. ? seed->e() / e9 : -1. );

        double eHcal              = m_toCaloFuture->energy( *cluster, CaloCellCode::CaloIndex::HcalCalo );
        data[DataType::ToHcalE]   = eHcal;
        data[DataType::Hcal2Ecal] = ( eEcal > 0 ) ? eHcal / eEcal : 0.;
      }

      // cluster-match chi2
      if ( !m_skipCl ) {
        const LHCb::CaloCluster* clus = cluster;
        // special trick for split cluster : use full cluster matching
        if ( hypo.hypothesis() == LHCb::CaloHypo::Hypothesis::PhotonFromMergedPi0 )
          clus = LHCb::CaloFutureAlgUtils::ClusterFromHypo( hypo, false ); // get the main cluster
        if ( const auto* ctable = m_clusterTrTable.get(); ctable ) {
          if ( const auto& range = ctable->relations( clus->seed() ); !range.empty() ) {
            data[DataType::ClusterMatch] = range.front().weight();
          }
        }
      }
    } else {
      counter( "no cluster" ) += 1;
    }

    // Estimator MUST be at the end (after all inputs are loaded)

    auto get_ = [&]( LHCb::Calo::Enum::DataType type ) {
      auto d = data.find( type );
      return d != data.end() ? d->second : 0.;
    };

// FIXME: C++20: remove pragmas
// designated initializers are part of C99 (so both clang and gcc support them)
// and C++20, but when using C++17, they generate a '-Wpedantic' warning.
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wpedantic"

    auto obs = LHCb::Calo::Interfaces::INeutralID::Observables{.clmatch = get_( DataType::ClusterMatch ),
                                                               .e19     = get_( DataType::E19 ),
                                                               .hclecl  = get_( DataType::Hcal2Ecal ),
                                                               .sprd    = get_( DataType::Spread )};
#pragma GCC diagnostic pop

    insert_if( DataType::isNotH, m_neutralID->isNotH( hypo, obs ) );
    insert_if( DataType::isNotE, m_neutralID->isNotE( hypo, obs ) );
    return data;
  }

  const LHCb::Track* HypoEstimator::toTrack( const CaloHypo& hypo, Enum::MatchType match ) const {
    using namespace LHCb::Calo::Enum;
    switch ( match ) {
    case MatchType::ClusterMatch: {
      if ( const auto* tbl = m_clusterTrTable.get(); tbl ) {
        const LHCb::CaloCluster* cluster = LHCb::CaloFutureAlgUtils::ClusterFromHypo(
            hypo, hypo.hypothesis() != LHCb::CaloHypo::Hypothesis::PhotonFromMergedPi0 );
        if ( const auto& range = tbl->relations( cluster->seed() ); !range.empty() ) { return range.front(); }
      }
      break;
    }
    case MatchType::ElectronMatch: {
      if ( const auto* tbl = m_electronTrTable.get(); tbl ) {
        if ( const auto range = tbl->relations( &hypo ); !range.empty() ) { return range.front(); }
      }
      break;
    }
    case MatchType::BremMatch: {
      if ( const auto* tbl = m_bremTrTable.get(); tbl ) {
        if ( const auto range = tbl->relations( &hypo ); !range.empty() ) { return range.front(); }
      }
      break;
    }
    }
    return nullptr;
  }
} // namespace LHCb::Calo
