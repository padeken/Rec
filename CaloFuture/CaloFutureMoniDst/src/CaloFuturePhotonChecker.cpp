/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "CaloDet/DeCalorimeter.h"
#include "CaloFutureMoniAlg.h"
#include "CaloFutureMoniUtils.h"
#include "CaloFutureUtils/CaloFuture2Track.h"
#include "CaloFutureUtils/CaloMomentum.h"
#include "CaloFutureUtils/ClusterFunctors.h"
#include "Event/CaloHypo.h"
#include "Event/MCParticle.h"
#include "Event/MCVertex.h"
#include "Event/Track.h"
#include "GaudiAlg/Consumer.h"
#include "GaudiKernel/Plane3DTypes.h"
#include "GaudiKernel/Point3DTypes.h"
#include "GaudiKernel/Vector3DTypes.h"
#include "Kernel/IParticlePropertySvc.h"
#include "Kernel/ParticleProperty.h"
#include "LHCbMath/Line.h"
#include "Relations/Relation.h"
#include "Relations/RelationWeighted.h"
#include "fmt/format.h"
#include <algorithm>

// Aliases
using Input   = LHCb::CaloHypo::Container;
using MCPs    = LHCb::MCParticles;
using Table   = Relations::RelationWeighted<LHCb::CaloCluster, LHCb::Track, float>;
using MCTable = Relations::RelationWeighted<LHCb::CaloCluster, LHCb::MCParticle, float>;
using IDTable = Relations::Relation<LHCb::CaloHypo, float>;
using Line    = Gaudi::Math::Line<Gaudi::XYZPoint, Gaudi::XYZVector>;

// =============================================================================

/** @class CaloFuturePhotonChecker CaloFuturePhotonChecker.h
 *
 *  Photon Selection Monitoring
 *  (LHCb 2004-03)
 *
 *  @author Frederic Machefert frederic.machefert@in2p3.fr
 *  @date   2004-15-04
 */

class CaloFuturePhotonChecker final
    : public Gaudi::Functional::Consumer<void( const Input&, const MCPs&, const Table&, const MCTable&,
                                               const IDTable& ),
                                         Gaudi::Functional::Traits::BaseClass_t<CaloFutureMoniAlg>> {
public:
  // GaudiAlgorithm
  StatusCode initialize() override;
  StatusCode finalize() override;

  // Gaudi::Functional
  void operator()( const Input&, const MCPs&, const Table&, const MCTable&, const IDTable& ) const override;

  CaloFuturePhotonChecker( const std::string& name, ISvcLocator* pSvc );

protected:
  std::vector<AIDA::IHistogram2D*> defHisto( unsigned int, double, double, unsigned int, std::string_view );

  bool isMergedPi0( const LHCb::MCParticle* ) const;

private:
  // FutureCounters
  mutable unsigned int m_nEvents     = 0;
  mutable unsigned int m_nCandidates = 0;
  mutable unsigned int m_nPhotons    = 0;
  mutable unsigned int m_nMCPhotons  = 0;
  mutable unsigned int m_nWrongs     = 0;

  // more counters
  mutable std::vector<double> m_mc_g;
  mutable std::vector<double> m_rec_bkg;
  mutable std::vector<double> m_rec_sig;
  mutable double              m_lh_mcg;
  mutable std::vector<double> m_lh_recsig;
  mutable std::vector<double> m_lh_recbkg;
  mutable double              m_lh_mcg_conv;
  mutable std::vector<double> m_lh_recsig_conv;
  mutable std::vector<double> m_lh_recsig_spd;
  mutable std::vector<double> m_lh_recbkg_spd;
  mutable double              m_lh_mcg_noconv;
  mutable std::vector<double> m_lh_recsig_noconv;
  mutable std::vector<double> m_lh_recsig_nospd;
  mutable std::vector<double> m_lh_recbkg_nospd;

  // Detector Information
  DeCalorimeter* m_ecal = nullptr;
  DeCalorimeter* m_spd  = nullptr;
  DeCalorimeter* m_prs  = nullptr;

  Gaudi::Plane3D m_ecalPlane;
  Gaudi::Plane3D m_prsPlane;
  Gaudi::Plane3D m_spdPlane;

  double m_zConv = 0.;

  // Tools
  std::string m_IDTableName = LHCb::CaloFutureIdLocation::PhotonID;

  // Particle Properties
  std::string      m_gammaName{"gamma"};
  LHCb::ParticleID m_gammaID{0};
  std::string      m_pi0Name{"pi0"};
  LHCb::ParticleID m_pi0ID{0};

  // histogramming related variables
  Gaudi::Property<bool>                m_pdf{this, "Pdf", false};
  Gaudi::Property<std::vector<double>> m_prsbin{this, "EPrsBin", {50, 0., 200.}};
  Gaudi::Property<std::vector<double>> m_chi2bin{this, "Chi2Bin", {26, 0., 104.}};
  Gaudi::Property<std::vector<double>> m_seedbin{this, "SeedBin", {50, 0., 1.}};

  // Signal/background definitions
  Gaudi::Property<float> m_etmin{this, "Etmin", 200. * Gaudi::Units::MeV};
  Gaudi::Property<float> m_dr{this, "Dr", -1.};
  Gaudi::Property<float> m_dz{this, "Dz", -1.};
  Gaudi::Property<float> m_de{this, "DE", 0.25};
  Gaudi::Property<float> m_mergedDist{this, "MergedDist", 1.5};

  // histograms

  AIDA::IHistogram1D* m_lhSig    = nullptr;
  AIDA::IHistogram1D* m_lhSigSpd = nullptr;
  AIDA::IHistogram1D* m_lhBkg    = nullptr;
  AIDA::IHistogram1D* m_lhBkgSpd = nullptr;

  Gaudi::Property<int> m_nbinlh{this, "LhNBin", 20};
  AIDA::IHistogram2D*  m_effpur       = nullptr;
  AIDA::IHistogram2D*  m_effpur_spd   = nullptr;
  AIDA::IHistogram2D*  m_effpur_nospd = nullptr;

  Gaudi::Property<int>   m_nbinpt{this, "PtNBin", 20};
  Gaudi::Property<float> m_lhcut{this, "LhCut", 0.3};
  Gaudi::Property<float> m_ptmin{this, "PtMinHisto", 0. * Gaudi::Units::MeV};
  Gaudi::Property<float> m_ptmax{this, "PtMaxHisto", 10. * Gaudi::Units::GeV};
  AIDA::IHistogram1D*    m_efficiency = nullptr;
  AIDA::IHistogram1D*    m_purity     = nullptr;

  std::vector<AIDA::IHistogram2D*> m_signalEPrs2D;
  std::vector<AIDA::IHistogram2D*> m_backgrEPrs2D;
  std::vector<AIDA::IHistogram2D*> m_signalChi22D;
  std::vector<AIDA::IHistogram2D*> m_backgrChi22D;
  std::vector<AIDA::IHistogram2D*> m_signalSeed2D;
  std::vector<AIDA::IHistogram2D*> m_backgrSeed2D;

  std::vector<AIDA::IHistogram2D*> m_signalEPrsSpd2D;
  std::vector<AIDA::IHistogram2D*> m_backgrEPrsSpd2D;
  std::vector<AIDA::IHistogram2D*> m_signalChi2Spd2D;
  std::vector<AIDA::IHistogram2D*> m_backgrChi2Spd2D;
  std::vector<AIDA::IHistogram2D*> m_signalSeedSpd2D;
  std::vector<AIDA::IHistogram2D*> m_backgrSeedSpd2D;
};

DECLARE_COMPONENT( CaloFuturePhotonChecker )

// =============================================================================

namespace {
  // math transform
  constexpr double transform( double e ) {
    return ( e < 1.e-10 ) ? 0. : std::max( {log( 1.35914 * e ) - 6.21461, 5.5} );
  }
} // namespace

// =============================================================================
/** @file
 *
 *  Implementation file for class CaloFuturePhotonChecker
 *  Photon Selection Monitoring
 *  (LHCb 2004-03)
 *
 *  @author Frederic Machefert frederic.machefert@in2p3.fr
 *  @date   2004-15-04
 */
// =============================================================================

CaloFuturePhotonChecker::CaloFuturePhotonChecker( const std::string& name, ISvcLocator* pSvcLocator )
    : Consumer( name, pSvcLocator,
                {KeyValue{"Input", LHCb::CaloHypoLocation::Photons}, // Legacy naming
                 KeyValue{"InputMCPs", LHCb::MCParticleLocation::Default},
                 KeyValue{"CC2TrTableName", LHCb::CaloFutureIdLocation::ClusterMatch},           // Legacy naming
                 KeyValue{"CC2MCPTableName", "Relations/" + LHCb::CaloClusterLocation::Default}, // Legacy naming
                 KeyValue{"InputIDTable", LHCb::CaloFutureIdLocation::PhotonID}} ) {}

// =============================================================================

StatusCode CaloFuturePhotonChecker::initialize() {
  StatusCode sc = Consumer::initialize();
  if ( sc.isFailure() ) return sc;

  //----- locate particle property service
  LHCb::IParticlePropertySvc* ppSvc = svc<LHCb::IParticlePropertySvc>( "LHCb::ParticlePropertySvc", true );
  if ( !ppSvc ) return Error( "Could not locate LHCb::ParticlePropertySvc!" );

  const auto ppg = ppSvc->find( m_gammaName );
  if ( !ppg ) {
    error() << "Could not locate particle ' " << m_gammaName << " '" << endmsg;
    return StatusCode::FAILURE;
  }
  m_gammaID = ppg->pid();

  const auto ppp = ppSvc->find( m_pi0Name );
  if ( !ppp ) {
    error() << "Could not locate particle ' " << m_pi0Name << " '" << endmsg;
    return StatusCode::FAILURE;
  }
  m_pi0ID = ppp->pid();

  info() << "Photon/Pi0 particle properties locatlized." << endmsg;

  //----- Detector recovery

  m_ecal = getDet<DeCalorimeter>( DeCalorimeterLocation::Ecal );
  m_spd  = getDetIfExists<DeCalorimeter>( DeCalorimeterLocation::Spd );
  m_prs  = getDetIfExists<DeCalorimeter>( DeCalorimeterLocation::Prs );
  if ( !m_spd || !m_prs ) return Error( "DeCalorimeter does not exist for Prs/Spd" );

  m_ecalPlane = m_ecal->plane( CaloPlane::ShowerMax );
  m_spdPlane  = m_spd->plane( CaloPlane::Middle );
  m_prsPlane  = m_prs->plane( CaloPlane::Middle );

  const auto spdFront = m_spd->plane( CaloPlane::Front );
  const auto normal   = spdFront.Normal();
  m_zConv             = -spdFront.HesseDistance() / normal.Z();
  if ( UNLIKELY( msgLevel( MSG::DEBUG ) ) )
    debug() << "z conversion definition is set to SPD front : " << m_zConv << endmsg;

  //----- Tool recovery

  //----- Check data consistency
  if ( m_prsbin.size() != 3 ) { return Error( "Wrong Binning Parameter (EPrs)" ); }
  if ( m_chi2bin.size() != 3 ) { return Error( "Wrong Binning Parameter (Chi2Tk)" ); }
  if ( m_seedbin.size() != 3 ) { return Error( "Wrong Binning Parameter (ESeed)" ); }

  // Reset Monitoring parameter
  for ( int i = 0; i < m_nbinpt; ++i ) {
    m_mc_g.push_back( 0 );
    m_rec_bkg.push_back( 0 );
    m_rec_sig.push_back( 0 );
  }

  m_lh_mcg        = 0;
  m_lh_mcg_conv   = 0;
  m_lh_mcg_noconv = 0;
  for ( int i = 0; i < m_nbinlh; ++i ) {
    m_lh_recsig.push_back( 0 );
    m_lh_recbkg.push_back( 0 );
    m_lh_recsig_conv.push_back( 0 );
    m_lh_recsig_spd.push_back( 0 );
    m_lh_recbkg_spd.push_back( 0 );
    m_lh_recsig_noconv.push_back( 0 );
    m_lh_recsig_nospd.push_back( 0 );
    m_lh_recbkg_nospd.push_back( 0 );
  }

  // Likelihood Output for Signal / Background
  m_lhSig    = book1D( "Lh_Sig_noSpd", "likelihood Signal - no SPD", 0., 1., 50 );
  m_lhSigSpd = book1D( "Lh_Sig_Spd", "likelihood Signal - SPD hit", 0., 1., 50 );
  m_lhBkg    = book1D( "Lh_Bkg_noSpd", "likelihood Background - no SPD", 0., 1., 50 );
  m_lhBkgSpd = book1D( "Lh_Bkg_Spd", "likelihood Background - SPD hit", 0., 1., 50 );

  // Efficiency / Purity versus Pt
  m_efficiency = book1D( "Efficiency", "Photon Selection Efficiency vs Pt", m_ptmin, m_ptmax, m_nbinpt );
  m_purity     = book1D( "Purity", "Photon Selection Purity vs Pt", m_ptmin, m_ptmax, m_nbinpt );

  m_effpur       = book2D( "Eff_Pur", "Efficiency vs Purity - Lh cut", 0., 1., 100, 0., 1., 100 );
  m_effpur_spd   = book2D( "Eff_Pur_Spd", "Efficiency vs Purity - Lh cut - no Conv. sample", 0., 1., 100, 0., 1., 100 );
  m_effpur_nospd = book2D( "Eff_Pur_noSpd", "Efficiency vs Purity - Lh cut - Conv. sample", 0., 1., 100, 0., 1., 100 );

  // Probability Density Functions Definitions
  if ( m_pdf ) {
    m_signalEPrs2D = defHisto( int( m_prsbin[0] ), m_prsbin[1], m_prsbin[2], 10, std::string( "Signal_Prs_noSpdHit" ) );
    m_signalChi22D =
        defHisto( int( m_chi2bin[0] ), m_chi2bin[1], m_chi2bin[2], 20, std::string( "Signal_Chi2Tk_noSpdHit" ) );
    m_signalSeed2D =
        defHisto( int( m_seedbin[0] ), m_seedbin[1], m_seedbin[2], 30, std::string( "Signal_ESeed_noSpdHit" ) );

    m_signalEPrsSpd2D =
        defHisto( int( m_prsbin[0] ), m_prsbin[1], m_prsbin[2], 15, std::string( "Signal_Prs_SpdHit" ) );
    m_signalChi2Spd2D =
        defHisto( int( m_chi2bin[0] ), m_chi2bin[1], m_chi2bin[2], 25, std::string( "Signal_Chi2Tk_SpdHit" ) );
    m_signalSeedSpd2D =
        defHisto( int( m_seedbin[0] ), m_seedbin[1], m_seedbin[2], 35, std::string( "Signal_ESeed_SpdHit" ) );

    m_backgrEPrs2D =
        defHisto( int( m_prsbin[0] ), m_prsbin[1], m_prsbin[2], 110, std::string( "Background_Prs_noSpdHit" ) );
    m_backgrChi22D =
        defHisto( int( m_chi2bin[0] ), m_chi2bin[1], m_chi2bin[2], 120, std::string( "Background_Chi2Tk_noSpdHit" ) );
    m_backgrSeed2D =
        defHisto( int( m_seedbin[0] ), m_seedbin[1], m_seedbin[2], 130, std::string( "Background ESeed_noSpdHit" ) );

    m_backgrEPrsSpd2D =
        defHisto( int( m_prsbin[0] ), m_prsbin[1], m_prsbin[2], 115, std::string( "Background_Prs_SpdHit" ) );
    m_backgrChi2Spd2D =
        defHisto( int( m_chi2bin[0] ), m_chi2bin[1], m_chi2bin[2], 125, std::string( "Background_Chi2Tk_SpdHit" ) );
    m_backgrSeedSpd2D =
        defHisto( int( m_seedbin[0] ), m_seedbin[1], m_seedbin[2], 135, std::string( "Background_ESeed_SpdHit" ) );
  }

  if ( m_split ) {
    Warning( "No area spliting allowed for CaloFuturePhotonChecker" ).ignore();
    m_split = false;
  }
  return StatusCode::SUCCESS;
}
// ============================================================================

// ============================================================================
/** standard algorithm finalization
 *  @see CaloFutureAlgorithm
 *  @see     Algorithm
 *  @see    IAlgorithm
 *  @return status code
 */
// ============================================================================

StatusCode CaloFuturePhotonChecker::finalize() {

  if ( UNLIKELY( msgLevel( MSG::DEBUG ) ) ) debug() << "==> Finalize" << endmsg;

  info() << "************* Photon Monitoring *****************" << endmsg;
  info() << "Number of Events Analyzed : " << m_nEvents << endmsg;
  info()
      //<< "MCPhotons (Signal def.) in sample: "<<m_nMCPhotons<<" - "
      << m_nPhotons << " Photons (Signal def.) out of " << m_nCandidates << " hypos processed."
      << " - " << m_nWrongs << " hypos rejected." << endmsg;
  info() << endmsg;
  info() << "     Et(GeV)        | Efficiency |   Purity   " << endmsg;
  info() << "----------------------------------------------" << endmsg;
  for ( int i = 0; i < m_nbinpt; ++i ) {
    double pt  = m_ptmin + double( i ) * ( m_ptmax - m_ptmin ) / double( m_nbinpt );
    double eff = ( m_mc_g[i] > 0 ) ? double( m_rec_sig[i] ) / double( m_mc_g[i] ) : 0.;
    double pur = ( m_rec_sig[i] + m_rec_bkg[i] > 0 ) ? m_rec_sig[i] / ( m_rec_sig[i] + m_rec_bkg[i] ) : 0.;
    info() << fmt::format( " [ {:5.2} - {:5.2} ]  |    {:4.2}    |    {:4.2}    ", pt / 1000.,
                           ( pt + ( m_ptmax - m_ptmin ) / double( m_nbinpt ) ) / 1000., eff, pur )
           << endmsg;

    fill( m_efficiency, pt, eff );
    fill( m_purity, pt, pur );
  }
  info() << endmsg;
  info() << "  L>= |   Total   |   No Conv |    Conv   " << endmsg;
  info() << "      | Eff   Pur | Eff   Pur | Eff   Pur " << endmsg;
  info() << " -----------------------------------------" << endmsg;
  for ( int i = 0; i < m_nbinlh; ++i ) {
    double eff       = ( m_lh_mcg > 0 ) ? m_lh_recsig[i] / m_lh_mcg : 0.;
    double effnoconv = ( m_lh_mcg_noconv > 0 ) ? m_lh_recsig_noconv[i] / m_lh_mcg_noconv : 0.;
    double effconv   = ( m_lh_mcg_conv > 0 ) ? m_lh_recsig_conv[i] / m_lh_mcg_conv : 0.;
    double pur = ( m_lh_recsig[i] + m_lh_recbkg[i] > 0 ) ? m_lh_recsig[i] / ( m_lh_recsig[i] + m_lh_recbkg[i] ) : 0.;
    double purnoconv = ( m_lh_recsig_nospd[i] + m_lh_recbkg_nospd[i] > 0 )
                           ? m_lh_recsig_nospd[i] / ( m_lh_recsig_nospd[i] + m_lh_recbkg_nospd[i] )
                           : 0.;
    double purconv = ( m_lh_recsig_spd[i] + m_lh_recbkg_spd[i] > 0 )
                         ? m_lh_recsig_spd[i] / ( m_lh_recsig_spd[i] + m_lh_recbkg_spd[i] )
                         : 0.;

    fill( m_effpur, pur, eff, 1. );
    fill( m_effpur_nospd, purnoconv, effnoconv, 1. );
    fill( m_effpur_spd, purconv, effconv, 1. );

    info() << fmt::format( " {:3.2} | {:3.2} {:3.2} | {:3.2} {:3.2} | {:3.2} {:3.2}", double( i ) / double( m_nbinlh ),
                           eff, pur, effnoconv, purnoconv, effconv, purconv )
           << endmsg;
  }
  info() << "*************************************************" << endmsg;

  return Consumer::finalize();
}
// ============================================================================

// ============================================================================
/** standard algorithm execution
 *  @see CaloFutureAlgorithm
 *  @see     Algorithm
 *  @see    IAlgorithm
 *  @return status code
 */
// ============================================================================

void CaloFuturePhotonChecker::operator()( const Input& hypos, const MCPs& mcParts, const Table& table,
                                          const MCTable& gtable, const IDTable& idTable ) const {

  // increment number of events
  m_nEvents++;

  if ( UNLIKELY( msgLevel( MSG::DEBUG ) ) )
    debug() << "MC Particles extracted from event : " << mcParts.size() << endmsg;

  // Loop over each mc particle
  for ( const auto& part : mcParts ) {

    if ( !( m_gammaID == part->particleID() ) ) continue; // select MC-gamma
    if ( part->momentum().pz() < 0. ) continue;           // Pz Acceptance
    if ( part->momentum().pt() < m_etmin ) continue;      // Et acceptance

    // Origin vertex
    const auto vertex = part->originVertex();
    if ( vertex == nullptr ) continue;                                         // ask for a vertex
    if ( ( m_dz > 0 ) && ( fabs( vertex->position().Z() ) > m_dz ) ) continue; // origin vertex acceptance ... in z
    if ( ( m_dr > 0 ) && ( vertex->position().Rho() > m_dr ) ) continue;       // ... and in (x,y)

    // Ecal acceptance
    Line                  line( vertex->position(), part->momentum().Vect() );
    const auto            cross = intersection( line, m_ecalPlane );
    const Gaudi::XYZPoint hit( cross );
    if ( UNLIKELY( msgLevel( MSG::DEBUG ) ) )
      debug() << "MC part momentum " << part->momentum() << " crosses Ecal Plane at point " << cross
              << " -> cellID : " << m_ecal->Cell( hit ) << endmsg;
    if ( !m_ecal->valid( m_ecal->Cell( hit ) ) ) continue; // Ecal acceptance.

    // Conversion
    Gaudi::XYZPoint decay( 0., 0., 1. * Gaudi::Units::km );
    const auto      decays = part->endVertices();
    for ( const auto& vertex : decays ) {
      if ( vertex->position().z() < decay.Z() ) decay = vertex->position();
    }

    if ( UNLIKELY( msgLevel( MSG::DEBUG ) ) ) debug() << "MC gamma endVertex.z() " << decay.Z() << endmsg;

    // belong to a merged pi0 ?
    if ( isMergedPi0( part ) ) {
      if ( UNLIKELY( msgLevel( MSG::DEBUG ) ) ) debug() << "Merged Pi0 photons removed from Signal sample" << endmsg;
      continue;
    }
    m_nMCPhotons++;

    int ibin = int( m_nbinpt * ( part->momentum().pt() - m_ptmin ) / ( m_ptmax - m_ptmin ) );
    if ( ibin >= 0 && ibin < m_nbinpt ) {
      // efficiency / purity versus pt
      m_mc_g[ibin]++;
      // efficiency / purity versus likelihood
      m_lh_mcg++;
      if ( decay.Z() > m_zConv ) {
        m_lh_mcg_noconv++;
        if ( UNLIKELY( msgLevel( MSG::DEBUG ) ) ) debug() << " Not converted " << m_zConv << endmsg;
      } else {
        m_lh_mcg_conv++;
        if ( UNLIKELY( msgLevel( MSG::DEBUG ) ) ) debug() << " converted " << m_zConv << endmsg;
      }
    }
  } // end loop over mcp

  if ( UNLIKELY( msgLevel( MSG::DEBUG ) ) )
    debug() << " MC part all/no-conv/conv" << m_lh_mcg << "/" << m_lh_mcg_noconv << "/" << m_lh_mcg_conv << endmsg;

  // loop over hypos
  for ( const auto& hypo : hypos ) {

    // skip nulls
    if ( hypo == nullptr ) {
      if ( UNLIKELY( msgLevel( MSG::DEBUG ) ) ) debug() << "empty CaloHypo : skipping" << endmsg;
      continue;
    }

    LHCb::Calo::Momentum momentum( hypo );
    m_nCandidates++;

    // Transverse Momentum
    if ( UNLIKELY( msgLevel( MSG::DEBUG ) ) )
      debug() << "==> Processing new CaloHypo : Et=" << momentum.momentum().pt() << endmsg;
    if ( momentum.momentum().pt() < m_etmin ) continue;
    if ( hypo->clusters().size() != 1 ) {
      warning() << "Photon Hypothesis : number of clusters!=1 ..." << endmsg;
      continue;
    }

    const auto cluster = hypo->clusters().front();
    if ( !cluster ) {
      Warning( " *CaloCluster* points to NULL " ).ignore();
      continue;
    }

    const auto entries = cluster->entries();
    if ( entries.empty() ) {
      Warning( " *CaloCluster* empty " ).ignore();
      continue;
    }

    const auto iseed =
        LHCb::ClusterFunctors::locateDigit( entries.begin(), entries.end(), LHCb::CaloDigitStatus::Mask::SeedCell );
    if ( iseed == entries.end() ) {
      Warning( " *SeedCell* not found " ).ignore();
      continue;
    }

    const auto seed = iseed->digit();
    if ( !seed ) {
      Warning( " SeedCell *Digit* points to NULL! " ).ignore();
      continue;
    }

    // seed cell area
    const auto m_area = seed->cellID().area();

    // Energy
    const double energy = momentum.momentum().e();
    if ( UNLIKELY( msgLevel( MSG::DEBUG ) ) ) debug() << "- Energy [MeV]=" << energy << endmsg;

    // Chi2
    const auto   range = table.relations( cluster );
    const double chi2  = range.empty() ? 1.e+6 : range.front().weight();
    if ( UNLIKELY( msgLevel( MSG::DEBUG ) ) ) debug() << " - Chi2        =" << chi2 << endmsg;

    // Cell seed
    const double eSeed = energy > 0. ? ( seed->e() ) / energy : -1.;
    if ( UNLIKELY( msgLevel( MSG::DEBUG ) ) ) debug() << " - Seed Energy =" << eSeed << endmsg;

    // Spd hit and Prs deposit
    Line       line( Gaudi::XYZPoint( 0., 0., 0. ), momentum.momentum().Vect() );
    const auto spdPoint = intersection( line, m_spdPlane );
    const auto prsPoint = intersection( line, m_prsPlane );
    const auto cellSpd  = m_spd->Cell( spdPoint );
    const auto cellPrs  = m_prs->Cell( prsPoint );

    double eSpd = 0.;
    double ePrs = 0.;

    // Get CaloFutureCell Deposits in the SPD and PRS
    if ( cellSpd ) {
      for ( const auto& digit : hypo->digits() ) {
        if ( digit->cellID() == cellSpd ) { eSpd = digit->e(); }
      }
      if ( UNLIKELY( msgLevel( MSG::DEBUG ) ) ) debug() << " - SPD " << cellSpd << " Energy  =" << eSpd << endmsg;
    }

    if ( cellPrs ) {
      for ( const auto& digit : hypo->digits() ) {
        if ( digit->cellID() == cellPrs ) { ePrs = digit->e(); }
      }
      if ( UNLIKELY( msgLevel( MSG::DEBUG ) ) ) debug() << " - PRS " << cellPrs << " Energy  =" << ePrs << endmsg;
    }

    // // ***
    // double likelihood = -1.;
    // if( NULL != idTable ){
    //   const auto idRange = idTable->relations( hypo ) ;
    //   if( !idRange.empty() ) likelihood = idRange.front().to();
    // }
    const auto   idRange    = idTable.relations( hypo );
    const double likelihood = idRange.empty() ? -1. : idRange.front().to();
    if ( likelihood < 0. ) { m_nWrongs++; }

    // MCTruth Information
    Gaudi::XYZPoint decay( 0., 0., 1. * Gaudi::Units::km );
    double          wmax     = -1.e6;
    double          dr       = 1.e+6;
    double          dz       = 1.e+6;
    double          de       = 1.e+6;
    bool            isSignal = false;
    bool            isPhoton = false;
    bool            isMerged = false;

    for ( const auto& mc : gtable.relations( cluster ) ) {
      const auto mcpart = mc.to();
      if ( mcpart == 0 ) continue;
      if ( UNLIKELY( msgLevel( MSG::DEBUG ) ) )
        debug() << "mctruth : --> pid=" << mcpart->particleID().pid() << " weight=" << mc.weight() << endmsg;
      if ( !( m_gammaID == mcpart->particleID() ) ) continue;
      if ( mc.weight() < wmax ) continue;
      wmax = mc.weight();

      if ( UNLIKELY( msgLevel( MSG::DEBUG ) ) ) {
        debug() << "A MC-gamma matches the hypo " << endmsg;
        debug() << " Energy :  " << mcpart->momentum().e() << endmsg;
      }

      isPhoton          = true;
      const auto vertex = mcpart->originVertex();
      if ( vertex == 0 ) {
        warning() << "MC-gamma has no origin vertex !" << endmsg;
        continue;
      }

      // selection
      dr = vertex->position().Rho();
      dz = vertex->position().z();
      de = fabs( energy - mcpart->momentum().e() ) / energy;
      if ( UNLIKELY( msgLevel( MSG::DEBUG ) ) )
        debug() << "Gamma parameters : dr=" << dr << " - dz=" << dz << " - de=" << de << endmsg;

      for ( const auto& v : mcpart->endVertices() ) {
        if ( v->position().z() < decay.Z() ) { decay = v->position(); }
      }

      // Check against truth, register if it's indeed from Pi0
      if ( isMergedPi0( mcpart ) ) isMerged = true;
    }

    // Collect good signal
    if ( de < m_de && ( ( m_dr < 0 ) || ( dr < m_dr ) ) && ( ( m_dz < 0 ) || ( dz < m_dz ) ) && isPhoton &&
         !isMerged ) {
      m_nPhotons++;
      isSignal = true;
      if ( UNLIKELY( msgLevel( MSG::DEBUG ) ) ) debug() << "Candidate is Signal according to MC" << endmsg;
    } else {
      if ( UNLIKELY( msgLevel( MSG::DEBUG ) ) )
        debug() << "Candidate is Background according to MC (photon, merged)" << isPhoton << " " << isMerged << endmsg;
    }

    // Efficiency and purity versus pt
    if ( likelihood > m_lhcut ) {
      int ibin = int( m_nbinpt * ( momentum.momentum().pt() - m_ptmin ) / ( m_ptmax - m_ptmin ) );
      if ( ibin >= 0 && ibin < m_nbinpt ) {
        if ( isSignal ) {
          m_rec_sig[ibin]++;
        } else {
          m_rec_bkg[ibin]++;
        }
      }
    }

    // Efficiency and purity versus likelihood
    int lhbin = int( likelihood * double( m_nbinlh ) );
    for ( int l = 0; l < lhbin; l++ ) {
      if ( isSignal ) {
        m_lh_recsig[l]++;
        if ( eSpd > 1. ) {
          m_lh_recsig_spd[l]++;
        } else {
          m_lh_recsig_nospd[l]++;
        }
        if ( decay.Z() > m_zConv ) {
          m_lh_recsig_noconv[l]++;
        } else {
          m_lh_recsig_conv[l]++;
        }
      } else {
        m_lh_recbkg[l]++;
        if ( eSpd > 1. ) {
          m_lh_recbkg_spd[l]++;
        } else {
          m_lh_recbkg_nospd[l]++;
        }
      }
    }

    // Fill General Monitoring histograms
    if ( isSignal ) {
      if ( eSpd > 1. ) {
        fill( m_lhSigSpd, likelihood, 1. );
      } else {
        fill( m_lhSig, likelihood, 1. );
      }
    } else {
      if ( eSpd > 1. ) {
        fill( m_lhBkgSpd, likelihood, 1. );
      } else {
        fill( m_lhBkg, likelihood, 1. );
      }
    }

    if ( m_pdf ) {
      double eTransf = transform( energy );
      if ( isSignal ) {
        if ( eSpd > 1. ) {
          fill( m_signalEPrsSpd2D[m_area], ePrs, eTransf, 1. );
          fill( m_signalChi2Spd2D[m_area], chi2, eTransf, 1. );
          fill( m_signalSeedSpd2D[m_area], eSeed, eTransf, 1. );
        } else {
          fill( m_signalEPrs2D[m_area], ePrs, eTransf, 1. );
          fill( m_signalChi22D[m_area], chi2, eTransf, 1. );
          fill( m_signalSeed2D[m_area], eSeed, eTransf, 1. );
        }
      } else {
        if ( eSpd > 1. ) {
          fill( m_backgrEPrsSpd2D[m_area], ePrs, eTransf, 1. );
          fill( m_backgrChi2Spd2D[m_area], chi2, eTransf, 1. );
          fill( m_backgrSeedSpd2D[m_area], eSeed, eTransf, 1. );
        } else {
          fill( m_backgrEPrs2D[m_area], ePrs, eTransf, 1. );
          fill( m_backgrChi22D[m_area], chi2, eTransf, 1. );
          fill( m_backgrSeed2D[m_area], eSeed, eTransf, 1. );
        }
      }
    }
  } // End loop over hypo
}

// =============================================================================

std::vector<AIDA::IHistogram2D*> CaloFuturePhotonChecker::defHisto( unsigned int bin, double xmin, double xmax,
                                                                    unsigned int nhisto, std::string_view hname ) {
  std::vector<AIDA::IHistogram2D*> histoList;
  for ( unsigned int area = 0; area < 3; ++area ) {
    auto histoname = fmt::format( "{}_{}", hname, nhisto + area );
    if ( msgLevel( MSG::DEBUG ) ) debug() << "booking Histo ..." << histoname << endmsg;
    histoList.push_back( book2D( histoname, histoname, (int)( xmin ), (int)( xmax ), bin, 0., 6., 6 ) );
  }
  return histoList;
}

// =============================================================================

// Return True if the given mcparticle
bool CaloFuturePhotonChecker::isMergedPi0( const LHCb::MCParticle* mcpart ) const {
  bool       isMerged = false;
  const auto mother   = mcpart->mother();
  if ( mother != nullptr ) {
    if ( m_pi0ID == mother->particleID() ) {
      const auto decayPi0 = mcpart->originVertex();
      const auto products = decayPi0->products();
      if ( products.size() == 2 ) {
        for ( const auto& pi0daughter : products ) {
          if ( mcpart == pi0daughter ) { continue; }
          const Line   line1( decayPi0->position(), mcpart->momentum().Vect() );
          const Line   line2( decayPi0->position(), pi0daughter->momentum().Vect() );
          const auto   hit1     = intersection( line1, m_ecalPlane );
          const auto   hit2     = intersection( line2, m_ecalPlane );
          const auto   distance = ( hit1 - hit2 ).R();
          const double param    = m_mergedDist *
                               ( m_ecal->cellSize( m_ecal->Cell( hit1 ) ) + m_ecal->cellSize( m_ecal->Cell( hit2 ) ) ) /
                               2.;
          if ( distance < param ) {
            isMerged = true;
            if ( UNLIKELY( msgLevel( MSG::DEBUG ) ) )
              debug() << "Pi0->Merged Photon :  distance=" << distance << "  < Criteria=" << param << " mm" << endmsg;
          }
        }
      }
    }
  }
  return isMerged;
}
