/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "CaloFutureInterfaces/ICaloFuture2MCTool.h"
#include "CaloFutureInterfaces/ICaloFutureHypoEstimator.h"
#include "CaloFutureMoniUtils.h"
#include "CaloFutureUtils/CaloFutureAlgUtils.h"
#include "CaloFutureUtils/CaloMomentum.h"
#include "Event/CaloHypo.h"
#include "Event/MCParticle.h"
#include "Event/ODIN.h"
#include "Event/RecVertex.h"
#include "GaudiAlg/Consumer.h"
#include "GaudiAlg/GaudiTupleAlg.h"
#include "GaudiKernel/IEventTimeDecoder.h"
#include "IFutureCounterLevel.h"

namespace {
  /// hack to allow for tools with non-const interfaces...
  template <typename IFace>
  IFace* fixup( const ToolHandle<IFace>& iface ) {
    return &const_cast<IFace&>( *iface );
  }
} // namespace
// =============================================================================

// List of Consumers dependencies
namespace {
  using ODIN     = LHCb::ODIN;
  using Hypos    = LHCb::CaloHypo::Container;
  using Tracks   = LHCb::Tracks;
  using Vertices = LHCb::RecVertices;
} // namespace

// ============================================================================

class CaloFutureHypoNtp final
    : public Gaudi::Functional::Consumer<void( const ODIN&, const Hypos&, const Tracks&, const Vertices& ),
                                         Gaudi::Functional::Traits::BaseClass_t<GaudiTupleAlg>> {
public:
  CaloFutureHypoNtp( const std::string& name, ISvcLocator* pSvcLocator );
  StatusCode initialize() override;
  void       operator()( const ODIN&, const Hypos&, const Tracks&, const Vertices& ) const override;

private:
  ToolHandle<ICaloFuture2MCTool>                     m_2MC         = {"CaloFuture2MCTool", this};
  ToolHandle<IFutureCounterLevel>                    m_counterStat = {"FutureCounterLevel"};
  ToolHandle<LHCb::Calo::Interfaces::IHypoEstimator> m_estimator   = {"CaloFutureHypoEstimator", this};

  Gaudi::Property<bool> m_extrapol{this, "Extrapolation", true};
  Gaudi::Property<bool> m_seed{this, "AddSeed", false};
  Gaudi::Property<bool> m_neig{this, "AddNeighbors", false};

  Gaudi::Property<std::pair<double, double>> m_et{this, "RangePt", {100., 15000.}};
  Gaudi::Property<std::pair<double, double>> m_e{this, "RangeE", {0., 5000000}};

  Gaudi::Property<std::vector<std::string>> m_hypos{this, "Hypos", {"Electrons", "Photons", "MergedPi0s"}};

  Gaudi::Property<bool> m_tupling{this, "Tupling", true};
  Gaudi::Property<bool> m_checker{this, "CheckerMode", false};
  Gaudi::Property<bool> m_print{this, "Printout", false};
  Gaudi::Property<bool> m_stat{this, "Statistics", true};
  Gaudi::Property<int>  m_mcID{this, "MCID", -99999999};

  mutable Gaudi::Accumulators::Counter<> m_nEventsInTuple{this, "Events in tuple"};
};

DECLARE_COMPONENT( CaloFutureHypoNtp )

// =============================================================================

CaloFutureHypoNtp::CaloFutureHypoNtp( const std::string& name, ISvcLocator* pSvcLocator )
    : Consumer( name, pSvcLocator,
                {KeyValue{"InputODIN", LHCb::ODINLocation::Default}, KeyValue{"Locations", ""},
                 KeyValue{"TrackLoc", LHCb::TrackLocation::Default},
                 KeyValue{"VertexLoc", LHCb::RecVertexLocation::Primary}} ) {}

// =============================================================================

StatusCode CaloFutureHypoNtp::initialize() {
  StatusCode sc = Consumer::initialize();
  if ( sc.isFailure() ) return sc;
  // retrieve tools
  if ( !( m_2MC.retrieve() && m_counterStat.retrieve() && m_estimator.retrieve() ) ) {
    error() << "Unable to retrive one of the ToolHandles" << endmsg;
    return StatusCode::FAILURE;
  }

  // Configure tool
  auto& h2c = dynamic_cast<IProperty&>( *m_estimator->hypo2Calo() );
  h2c.setProperty( "Seed", m_seed ? "true" : "false" ).ignore();
  h2c.setProperty( "PhotonLine", m_extrapol ? "true" : "false" ).ignore();
  h2c.setProperty( "AddNeighbors", m_neig ? "true" : "false" ).ignore();

  // Configure input locations
  using namespace LHCb::CaloHypoLocation;
  // get the user-override input
  auto                     loc = inputLocation<2>(); // <-- Careful with the index, 'Location' is 3rd.
  std::vector<std::string> m_locs{};
  if ( !loc.empty() ) m_locs.push_back( loc );
  // get the respective input from Hypos
  for ( const auto& h : m_hypos ) {
    if ( h == "Photons" )
      m_locs.push_back( Photons );
    else if ( h == "Electrons" )
      m_locs.push_back( Electrons );
    else if ( h == "MergedPi0s" )
      m_locs.push_back( MergedPi0s );
    else if ( h == "SplitPhotons" )
      m_locs.push_back( SplitPhotons );
  }
  // Finally, update handle
  updateHandleLocation( *this, "Locations", boost::algorithm::join( m_locs, "&" ) );

  return StatusCode::SUCCESS;
}

// =============================================================================
// standard execution method
// =============================================================================

void CaloFutureHypoNtp::operator()( const ODIN& odin, const Hypos& hypos, const Tracks& tracks,
                                    const Vertices& verts ) const {

  using namespace LHCb::Calo::Enum;

  // declare tuple
  Tuple ntp = ( !m_tupling ) ? Tuple{nullptr} : nTuple( 500, "HypoNtp", CLID_ColumnWiseTuple );

  // Collect global info (in pre-Functional implementation, some are optionals)
  const auto run    = odin.runNumber();
  const auto evt    = (double)odin.eventNumber();
  const auto tty    = odin.triggerType();
  const auto nTrack = tracks.size();
  const auto nVert  = verts.size();

  // loop over hypo containers
  bool ok = true;
  for ( const auto& hypo : hypos ) {
    // hypo string
    std::ostringstream type( "" );
    type << hypo->hypothesis();
    std::string hypothesis = type.str();

    // filtering hypo
    if ( !inRange( m_et, fixup( m_estimator )->data( *hypo, DataType::HypoEt ).value_or( 0. ) ) ) continue;
    if ( !inRange( m_e, fixup( m_estimator )->data( *hypo, DataType::HypoE ).value_or( 0. ) ) ) continue;

    // MC-associated filtering
    if ( m_checker ) {
      const auto mcp = fixup( m_2MC )->from( hypo )->bestMC();
      if ( !mcp ) continue;
      if ( m_mcID >= 0 && (int)mcp->particleID().abspid() != m_mcID ) continue;
    }

    // PrintOut
    if ( m_print ) {
      info() << "+++ Run/Evt " << run << "/" << evt << endmsg;
      info() << " === hypothesis " << hypo->hypothesis() << "(" << inputLocation<2>() << ")" << endmsg;
      if ( m_checker ) fixup( m_2MC )->from( hypo )->descriptor();
    }

    // DataTypes statistics
    for ( int j = 0; j < count<DataType>(); ++j ) {
      auto       i   = DataType( j );
      const auto val = fixup( m_estimator )->data( *hypo, i ).value_or( 0. );
      if ( m_stat && m_counterStat->isQuiet() ) counter( std::string{toString( i )} + " for " + hypothesis ) += val;
      if ( m_tupling ) ok &= ntp->column( toString( i ), val );
      if ( m_print ) info() << "   --> " << toString( i ) << " : " << val << endmsg;
    }

    // Tupling
    if ( m_tupling ) {

      // hypothesis
      ok &= ntp->column( "hypothesis", hypo->hypothesis() );

      // kinematics
      const auto cluster = LHCb::CaloFutureAlgUtils::ClusterFromHypo( *hypo );
      ok &= ntp->column( "ClusterR", position3d( cluster ) );
      ok &= ntp->column( "HypoR", position3d( hypo ) );
      ok &= ntp->column( "HypoP", momentum( hypo ) );

      // matched tracks
      for ( auto match : {MatchType::ClusterMatch, MatchType::ElectronMatch, MatchType::BremMatch} ) {
        const auto track = fixup( m_estimator )->toTrack( *hypo, match );
        ok &= ntp->column( std::string{toString( match )} + "TrackP", momentum( track ) );
      }

      // odin info
      ok &= ntp->column( "Run", run );
      ok &= ntp->column( "Event", evt );
      ok &= ntp->column( "Triggertype", tty );
      ok &= ntp->column( "Nvertices", nVert );
      ok &= ntp->column( "NTracks", nTrack );

      // Checker Mode (MC info)
      if ( m_checker ) {
        int        id      = -999;
        double     weight  = -999;
        double     quality = -999;
        const auto mcp     = fixup( m_2MC )->from( hypo )->bestMC();
        if ( mcp ) {
          id      = mcp->particleID().pid();
          weight  = fixup( m_2MC )->from( hypo )->weight( mcp );
          quality = fixup( m_2MC )->from( hypo )->quality( mcp );
        }
        ok &= ntp->column( "MCid", id );
        ok &= ntp->column( "MCw", weight );
        ok &= ntp->column( "MCq", quality );
      }
      ok &= ntp->write();
    }
  }

  // Finally, report
  if ( ok ) {
    if ( m_counterStat->isQuiet() ) ++m_nEventsInTuple;
  } else {
    Warning( "Error with ntupling", StatusCode::SUCCESS ).ignore();
  }
  return;
}
