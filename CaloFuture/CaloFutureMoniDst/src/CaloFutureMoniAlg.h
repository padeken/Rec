/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef CALOFUTUREMONIDST_CALOFUTUREMONIALG_H
#define CALOFUTUREMONIDST_CALOFUTUREMONIALG_H 1
// ============================================================================
// Include files
// ============================================================================
// from Gaudi
// ============================================================================

#include "AIDA/IAxis.h"
#include "AIDA/IHistogram1D.h"
#include "AIDA/IHistogram2D.h"
#include "CaloFutureUtils/CaloFuture2Dview.h"
#include "CaloFutureUtils/CaloFutureAlgUtils.h"
#include "GaudiAlg/GaudiHistoAlg.h"
#include "GaudiKernel/HashMap.h"
#include "GaudiUtils/Aida2ROOT.h"
#include "IFutureCounterLevel.h"
#include "Kernel/CaloCellID.h"
#include "TROOT.h"

// ============================================================================
// AIDA
// ============================================================================
#include "AIDA/IHistogram1D.h"

// @class CaloFutureMoniAlg CaloFutureMoniAlg.h
//
//   @see GaudiHistoAlg
//   @see GaudiAlgorithm
//   @see      Algorithm
//   @see     IAlgorithm

class CaloFutureMoniAlg : public CaloFuture2Dview {

public:
  // Standard constructor
  //   @param   name        algorithm name
  //   @param   pSvcLocator pointer to service locator
  CaloFutureMoniAlg( const std::string& name, ISvcLocator* pSvcLocator );
  StatusCode initialize() override;

  // address/location/name in Transient Store of detector data
  const std::string& detData() const { return m_detData; }

  // booking histogram
  void initCounters() const;
  void count( LHCb::CaloCellID id = LHCb::CaloCellID() ) const;
  void fillFutureCounters( std::string ) const;
  void hBook1( const std::string, const std::string, const double low = 0, const double high = 100,
               const unsigned long bins = 100 );
  void h1binLabel( const std::string hid, int bin, std::string label ) const;
  void hBook2( const std::string hid, const std::string titl, const double lowx = 0, const double highx = 100,
               const unsigned long binsx = 100, const double lowy = 0, const double highy = 100,
               const unsigned long binsy = 100 );

  // fill histogram
  void hFill1( std::string hid, double value, double w = 1. ) const;
  void hFill2( std::string hid, double x, double y, double w = 1. ) const;
  void hFill1( LHCb::CaloCellID cellID, std::string hid, double value, double w = 1. ) const;
  void hFill2( LHCb::CaloCellID cellID, std::string hid, double x, double y, double w = 1. ) const;

private:
  Gaudi::Property<std::string> m_detData{this, "Detector", "Ecal",
                                         "address/location/name in Transient Store of detector data"};

protected:
  ToolHandle<IFutureCounterLevel> m_counterStat{"FutureCounterLevel"};

  // Histogram Map
  GaudiUtils::HashMap<std::string, AIDA::IHistogram1D*> m_h1;
  GaudiUtils::HashMap<std::string, AIDA::IHistogram2D*> m_h2;

  // Properties
  Gaudi::Property<float> m_energyMin{this, "HistoEnergyMin", 0.};
  Gaudi::Property<float> m_etMin{this, "HistoEtMin", 0.};
  Gaudi::Property<float> m_massMin{this, "HistoMassMin", 0.};
  Gaudi::Property<float> m_xMin{this, "HistoXMin", -4 * Gaudi::Units::meter};
  Gaudi::Property<float> m_yMin{this, "HistoYMin", -4 * Gaudi::Units::meter};
  Gaudi::Property<float> m_multMin{this, "HistoMultiplicityMin", 0.};
  Gaudi::Property<float> m_sizeMin{this, "HistoSizeMin", 0.};

  Gaudi::Property<float> m_energyMax{this, "HistoEnergyMax", 250. * Gaudi::Units::GeV};
  Gaudi::Property<float> m_etMax{this, "HistoEtMax", 15. * Gaudi::Units::GeV};
  Gaudi::Property<float> m_massMax{this, "HistoMassMax", 250. * Gaudi::Units::MeV};
  Gaudi::Property<float> m_xMax{this, "HistoXMax", +4 * Gaudi::Units::meter};
  Gaudi::Property<float> m_yMax{this, "HistoYMax", +4 * Gaudi::Units::meter};
  Gaudi::Property<float> m_multMax{this, "HistoMultiplicityMax", 2000.};
  Gaudi::Property<float> m_sizeMax{this, "HistoSizeMax", 25.};

  Gaudi::Property<int> m_energyBin{this, "HistoEnergyBin", 100};
  Gaudi::Property<int> m_etBin{this, "HistoEtBin", 100};
  Gaudi::Property<int> m_massBin{this, "HistoMassBin", 100};
  Gaudi::Property<int> m_xBin{this, "HistoXBin", 50};
  Gaudi::Property<int> m_yBin{this, "HistoYBin", 50};
  Gaudi::Property<int> m_multBin{this, "HistoMultiplicityBin", 100};
  Gaudi::Property<int> m_sizeBin{this, "HistoSizeBin", 25};

  Gaudi::Property<float> m_eFilter{this, "EnergyFilter", -100.};
  Gaudi::Property<float> m_etFilter{this, "EtFilter", -100.};
  Gaudi::Property<float> m_massFilterMin{this, "MassWindowMin", -9999999.};
  Gaudi::Property<float> m_massFilterMax{this, "MassWindowMax", +9999999.};

  Gaudi::Property<bool> m_sat{this, "SaturationBin1D", true};
  Gaudi::Property<bool> m_sat2D{this, "SaturationBin2D", false};
  Gaudi::Property<bool> m_print{this, "PrintOut", false};
  Gaudi::Property<bool> m_splitSides{this, "SplitSides", false};

  Gaudi::Property<std::vector<std::string>> m_histoList{this, "histoList", {"All"}};
  Gaudi::Property<std::vector<std::string>> m_removeHisto{this, "removeFromHistoList", {}};
  Gaudi::Property<std::vector<std::string>> m_areas{
      this, "listOfAreas", {"Outer", "Middle", "Inner"}, "list of areas to be split"};

  const unsigned int m_nAreas = 1 << ( CaloCellCode::BitsArea + 1 );

  // For const operator()
  mutable unsigned int              m_count = 0;
  mutable std::vector<unsigned int> m_mcount;
  mutable std::vector<unsigned int> m_scount;

  bool doHisto( const std::string histo ) const;
  bool validArea( const std::string area ) const;
};
#endif // CALOFUTUREMONIDST_CALOFUTUREMONIALG_H
