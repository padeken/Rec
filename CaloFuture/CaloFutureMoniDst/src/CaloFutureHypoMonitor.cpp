/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Includes
#include "CaloDet/DeCalorimeter.h"
#include "CaloFutureMoniAlg.h"
#include "CaloFutureUtils/CaloFutureAlgUtils.h"
#include "CaloFutureUtils/CaloMomentum.h"
#include "Event/CaloDataFunctor.h"
#include "Event/CaloHypos_v2.h"
#include "GaudiAlg/Consumer.h"
#include "boost/algorithm/string/predicate.hpp"

// =============================================================================

/** @class CaloFutureHypoMonitor CaloFutureHypoMonitor.cpp
 *
 *  The algorithm for trivial monitoring of "CaloHypo" container
 *  The algorithm produces 10 histograms:
 *  <ol>
 *  <li> @p CaloHypo multiplicity                           </li>
 *  <li> @p CaloHypo energy distribution                    </li>
 *  <li> @p CaloHypo transverse momentum distribution       </li>
 *  <li> @p CaloHypo mass distribution                      </li>
 *  <li> @p CaloHypo x distribution                         </li>
 *  <li> @p CaloHypo y distribution                         </li>
 *  <li> multiplicity of     @p CaloCluster per @p CaloHypo </li>
 *  <li> CaloHypo x vs y distribution                       </li>
 *  </ol>
 *  Histograms reside in the directory @p /stat/"Name" , where
 *  @ "Name" is the name of the algorithm
 *
 *  @see CaloFutureAlgorithm
 *  @see     Algorithm
 *  @see    IAlgorithm
 *
 *  @author Vanya Belyaev Ivan.Belyaev@itep.ru
 *  @date   02/11/2001
 */

using Input = LHCb::Event::Calo::Hypotheses;

class CaloFutureHypoMonitor final
    : public Gaudi::Functional::Consumer<void( const Input&, const LHCb::Event::Calo::Clusters& ),
                                         Gaudi::Functional::Traits::BaseClass_t<CaloFutureMoniAlg>> {
public:
  CaloFutureHypoMonitor( const std::string& name, ISvcLocator* pSvcLocator );
  StatusCode initialize() override;
  void       operator()( const Input&, const LHCb::Event::Calo::Clusters& ) const override;

private:
  Gaudi::Property<int>   m_clusBin{this, "NClusterBin", 5};
  Gaudi::Property<float> m_clusMax{this, "NClusterMax", 5.};
  Gaudi::Property<float> m_clusMin{this, "NClusterMin", 0.};
};

DECLARE_COMPONENT( CaloFutureHypoMonitor )

// =============================================================================

CaloFutureHypoMonitor::CaloFutureHypoMonitor( const std::string& name, ISvcLocator* pSvcLocator )
    : Consumer( name, pSvcLocator,
                {
                    KeyValue{"Input", LHCb::CaloFutureAlgUtils::CaloFutureHypoLocation( name )},
                    KeyValue{"InputClusters", LHCb::CaloFutureAlgUtils::CaloFutureClusterLocation( name )},
                } ) {
  m_multMax = 250;
  m_multBin = 50;
}

// =============================================================================

/// standard algorithm initialization
StatusCode CaloFutureHypoMonitor::initialize() {
  return Consumer::initialize().andThen( [&] {
    hBook1( "1", "# of Hypos    " + inputLocation(), m_multMin, m_multMax, m_multBin );
    hBook1( "2", "Hypo Energy   " + inputLocation(), m_energyMin, m_energyMax, m_energyBin );
    hBook1( "3", "Hypo Pt       " + inputLocation(), m_etMin, m_etMax, m_etBin );
    if ( boost::algorithm::contains( inputLocation(), "MergedPi0s" ) )
      hBook1( "4", "Hypo Mass     " + inputLocation(), m_massMin, m_massMax, m_massBin );
    hBook1( "5", "Hypo X        " + inputLocation(), m_xMin, m_xMax, m_xBin );
    hBook1( "6", "Hypo Y        " + inputLocation(), m_yMin, m_yMax, m_yBin );
    hBook1( "7", "Clusters/Hypo " + inputLocation(), m_clusMin, m_clusMax, m_clusBin );
    hBook2( "10", "Hypo barycenter position x vs y   " + inputLocation(), m_xMin, m_xMax, m_xBin, m_yMin, m_yMax,
            m_yBin );
    hBook2( "11", "Energy-weighted hypo barycenter position x vs y " + inputLocation(), m_xMin, m_xMax, m_xBin, m_yMin,
            m_yMax, m_yBin );
    hBook1( "14", "#Hypo/#Cluster" + inputLocation(), 0., 1., 100 );
  } );
}

// =============================================================================
// standard execution method
// =============================================================================

void CaloFutureHypoMonitor::operator()( const Input& hypos, const LHCb::Event::Calo::Clusters& clusters ) const {
  // produce histos ?
  if ( !produceHistos() ) return;

  // check data
  if ( hypos.empty() ) {
    if ( UNLIKELY( msgLevel( MSG::DEBUG ) ) ) debug() << "Empty hypos found in" << inputLocation() << endmsg;
    return;
  }

  initCounters();

  // Start looping over hypo
  for ( const auto& hypo : hypos ) {
    LHCb::Calo::Momentum momentum( hypo.clusters() );
    const double         e    = momentum.e();
    const double         et   = momentum.pt();
    const double         mass = momentum.momentum().mass();
    if ( e < m_eFilter ) continue;
    if ( et < m_etFilter ) continue;
    if ( mass < m_massFilterMin || mass > m_massFilterMax ) continue;
    auto id = hypo.cellID();

    // Start filling histo
    count( id );
    hFill1( id, "2", e );
    hFill1( id, "3", et );
    if ( boost::algorithm::contains( inputLocation(), "MergedPi0s" ) ) hFill1( id, "4", mass );

    const auto& position = hypo.position();
    hFill1( id, "5", position.x() );
    hFill1( id, "6", position.y() );
    hFill2( id, "10", position.x(), position.y() );
    hFill2( id, "11", position.x(), position.y(), e );

    hFill1( id, "7", hypo.size() );

    if ( !( id == LHCb::CaloCellID() ) ) {
      if ( doHisto( "12" ) ) fillCaloFuture2D( "12", id, 1., "Hypo position 2Dview " + inputLocation() );
      if ( doHisto( "13" ) ) fillCaloFuture2D( "13", id, e, "Hypo Energy 2Dview " + inputLocation() );
    }
  }
  // fill multiplicity histogram
  fillFutureCounters( "1" );

  // cluster fraction (no area-splittable so far)
  const int    nClus = clusters.size();
  const double frac  = nClus > 0 ? (double)m_count / (double)nClus : 0.;
  hFill1( "14", frac );
}
