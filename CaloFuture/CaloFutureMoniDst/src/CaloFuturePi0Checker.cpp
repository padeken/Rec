/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Includes
#include "GaudiAlg/Consumer.h"
// #include "Relations/IRelationWeighted.h"
#include "CaloDet/DeCalorimeter.h"
#include "CaloFutureMoniAlg.h"
#include "CaloFutureUtils/CaloMomentum.h"
#include "CaloFutureUtils/ClusterFunctors.h"
#include "Event/CaloHypos_v2.h"
#include "Event/MCParticle.h"
#include "Kernel/IParticlePropertySvc.h"
#include "Kernel/ParticleProperty.h"
#include "Relations/RelationWeighted.h"
#include "Relations/RelationWeighted1D.h"

// ============================================================================

/** @class CaloFuturePi0Checker CaloFuturePi0Checker.cpp
 *
 *  Simple MC pi0 monitoring algorithm
 *  It produces 2 histograms
 *  <ol>
 *  <li> "Raw" mass distribution of 2 photons </li>
 *  <li> Mass distribution of 2 photons after Pt cut for each photon </li>
 *  <li> Mass distribution of 2 photons after Pt cut for combination </li>
 *  </ol>
 *
 *  @see CaloFutureAlgorithm
 *  @see     Algorithm
 *  @see    IAlgorithm
 *
 *  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
 *  @date   02/11/2001
 */

using Input  = LHCb::RelationWeighted1D<LHCb::CaloCellID, LHCb::MCParticle, float>;
using Inputs = LHCb::Event::Calo::Hypotheses;

class CaloFuturePi0Checker final
    : public Gaudi::Functional::Consumer<void( const Input&, const Inputs& ),
                                         Gaudi::Functional::Traits::BaseClass_t<CaloFutureMoniAlg>> {
public:
  StatusCode initialize() override;
  void       operator()( const Input&, const Inputs& ) const override;

  CaloFuturePi0Checker( const std::string& name, ISvcLocator* pSvcLocator );

private:
  LHCb::ClusterFunctors::ClusterFromCaloFuture m_calo = CaloCellCode::CaloIndex::EcalCalo;

  LHCb::ParticleID m_pi0ID{0};

  Gaudi::Property<float> m_cut{this, "Cut", float( 50 * Gaudi::Units::perCent ), "photon purity cut"};

  Gaudi::Property<std::string> m_pi0Name{this, "Ancestor", "pi0"};
};

// =============================================================================

DECLARE_COMPONENT( CaloFuturePi0Checker )

// =============================================================================

CaloFuturePi0Checker::CaloFuturePi0Checker( const std::string& name, ISvcLocator* pSvcLocator )
    : Consumer( name, pSvcLocator,
                {KeyValue{"Input", "Relations/" + LHCb::CaloClusterLocation::Default},
                 KeyValue{"Inputs", LHCb::CaloHypoLocation::Photons}} ) {
  // set the appropriate default value for detector data
  setProperty( "Detector", DeCalorimeterLocation::Ecal ).ignore();
}

// =============================================================================

StatusCode CaloFuturePi0Checker::initialize() {
  StatusCode sc = Consumer::initialize(); // must be executed first
  if ( sc.isFailure() ) return sc;        // error already printedby GaudiAlgorithm

  // re-initialize the Ecal cluster selector
  m_calo = {CaloCellCode::CaloNumFromName( detData() )};

  // locate particle eproperty service
  auto ppS = svc<LHCb::IParticlePropertySvc>( "LHCb::ParticlePropertySvc", true );
  if ( !ppS ) return StatusCode::FAILURE;

  const auto pp = ppS->find( m_pi0Name );
  if ( !pp ) return Error( "Could not locate particle '" + m_pi0Name + "'" );
  m_pi0ID = pp->pid();

  hBook1( "1", "Gamma-Gamma mass       ", 0, 1, 500 );
  hBook1( "2", "Gamma-Gamma mass (MCpi0 match)", 0, 1, 500 );
  if ( m_split ) {
    Warning( "No area spliting allowed for CaloFuturePi0Checker" ).ignore();
    m_split = false;
  }
  return StatusCode::SUCCESS;
}

// =============================================================================

void CaloFuturePi0Checker::operator()( const Input& table, const Inputs& photons ) const {

  using namespace LHCb::ClusterFunctors;

  // loop over the first photon
  for ( auto g1 = photons.begin(); g1 != photons.end(); ++g1 ) {
    LHCb::Calo::Momentum momentum1( g1->clusters() );

    // get Ecal cluster
    const auto clusters1 = g1->clusters();
    const auto cluster1 =
        ( 1 == clusters1.size() ) ? clusters1.begin() : std::find_if( clusters1.begin(), clusters1.end(), m_calo );
    if ( clusters1.end() == cluster1 ) continue;

    // get all MCtruth information for this cluster
    const float cut1   = (float)( ( *cluster1 )->e() * m_cut );
    const auto  range1 = table.relations( ( *cluster1 )->seed(), cut1, true );

    // loop over the second photon
    for ( auto g2 = std::next( g1 ); g2 != photons.end(); ++g2 ) {
      LHCb::Calo::Momentum momentum2( g2->clusters() );

      // get Ecal cluster
      const auto clusters2 = g2->clusters();
      auto       cluster2 =
          ( 1 == clusters2.size() ) ? clusters2.begin() : std::find_if( clusters2.begin(), clusters2.end(), m_calo );
      if ( clusters2.end() == cluster2 ) continue;

      // get all MCtruth information for this cluster
      const float cut2   = (float)( cluster2->e() * m_cut );
      const auto  range2 = table.relations( cluster2->seed(), cut2, true );

      // double loop for search the common ancestor
      LHCb::MCParticle* pi0 = nullptr;
      for ( auto mc1 = range1.begin(); ( !pi0 ) && ( range1.end() != mc1 ); ++mc1 ) {
        if ( !mc1->to() ) continue;
        for ( auto mc2 = range2.begin(); ( !pi0 ) && ( range2.end() != mc2 ); ++mc2 ) {
          if ( mc1->to() != mc2->to() ) continue; // common ancestor?
          if ( m_pi0ID == mc1->to()->particleID() ) pi0 = mc1->to();
        } // end of second MC loop
      }   // end of first MC loop

      const double mass = ( momentum1.momentum() + momentum2.momentum() ).mass();
      hFill1( "1", mass / Gaudi::Units::GeV );
      if ( !pi0 ) continue;
      hFill1( "2", mass / Gaudi::Units::GeV );
    } // end of loop over second photon
  }   // end of loop over first photon
}
