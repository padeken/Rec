/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files
#include "CaloFutureEnergyForTrack.h"

namespace LHCb::Calo {
  // ============================================================================
  // initialize the tool
  // ============================================================================

  StatusCode EnergyForTrack::initialize() {
    StatusCode sc = TrackTool::initialize();
    if ( sc.isFailure() ) { return sc; }
    std::string det    = detectorName();
    const long  caloID = CaloCellCode::CaloNumFromName( det.c_str() );
    switch ( caloID ) {
    case CaloCellCode::CaloIndex::EcalCalo:
      // ECAL
      m_location = State::Location::ECalShowerMax;
      m_planes   = {calo()->plane( CaloPlane::Front ), calo()->plane( CaloPlane::ShowerMax ),
                  calo()->plane( CaloPlane::Middle ), calo()->plane( CaloPlane::Back )};
      break;
    case CaloCellCode::CaloIndex::HcalCalo:
      // HCAL
      m_location = State::Location::MidHCal;
      m_planes   = {calo()->plane( CaloPlane::Front ), calo()->plane( CaloPlane::Middle ),
                  calo()->plane( CaloPlane::Back )};
      break;
    default:
      return Error( "Invald calorimeter TYPE! '" + detectorName() + "'" );
    }
    //
    if ( msgLevel( MSG::DEBUG ) ) { info() << "State Location is set to '" << m_location << "'" << endmsg; }
    //
    if ( m_planes.empty() ) { return Error( "Empty list of Calo-planes" ); }
    //
    return StatusCode::SUCCESS;
  }

  std::optional<CaloCellID::Set> EnergyForTrack::collect( const Line& line ) const {
    CaloCellID::Set cells;
    // get the interesection points of the line with the planes
    std::vector<Gaudi::XYZPoint> points;
    points.reserve( m_planes.size() + m_morePlanes );
    // get the intersection points
    for ( const auto& plane : m_planes ) {
      double          mu = 0;
      Gaudi::XYZPoint point;
      Gaudi::Math::intersection( line, plane, point, mu );
      points.push_back( point );
    }
    // add some additional points (if requested)
    if ( 2 <= points.size() && 0 < m_morePlanes ) {
      // the same line but in another reparameterization:
      const auto   l2  = Line( points.front(), points.back() - points.front() );
      const double dmu = 1.0 / ( 1.0 + m_morePlanes );
      for ( unsigned short i = 0; i < m_morePlanes; ++i ) { points.push_back( l2.position( dmu * ( i + 1.0 ) ) ); }
    }
    // get all touched calorimeter cells: convert points to cells
    for ( const auto& point : points ) {
      // get the cell !
      const auto callParam = calo()->Cell_( point );
      /// skip invalid cells
      if ( !callParam || !callParam->valid() ) { continue; }
      // collect all valid cells
      cells.insert( callParam->cellID() );
    }

    // add neighbours
    if ( 0 < m_addNeighbours ) { CaloFutureFunctors::neighbours( cells, m_addNeighbours, calo() ); }

    return cells;
  }
  std::optional<CaloCellID::Set> EnergyForTrack::collect( const Track& track ) const {
    // get the correct state
    auto trkstate = TrackTool::state( track, m_location );
    if ( !trkstate ) {
      // propagate it!
      State      tmpstate;
      StatusCode sc = TrackTool::propagate( track, m_planes.front(), tmpstate );
      if ( sc.isFailure() ) {
        Error( "process(): failure from propagate", sc ).ignore();
        return std::nullopt;
      }
      trkstate = &tmpstate;
    }
    // get the line from the state:
    return collect( TrackTool::line( *trkstate ) );
  }
  // ============================================================================
  // The main processing method
  // ============================================================================
  std::optional<double> EnergyForTrack::process( const Track& track, const Event::Calo::Digits& digits ) const {
    auto ids = collect( track );
    if ( !ids ) return std::nullopt;
    return std::accumulate( std::begin( *ids ), std::end( *ids ), 0., [&]( double e, CaloCellID id ) {
      if ( auto digit = digits( id ); digit ) e += digit->energy();
      return e;
    } );
  }
} // namespace LHCb::Calo
