/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files
#include "InCaloFutureAcceptance.h"
#include "CaloDet/DeCalorimeter.h"
#include "Event/Track.h"
#include "GaudiKernel/Plane3DTypes.h"
#include "GaudiKernel/Point3DTypes.h"
#include "GaudiKernel/Vector3DTypes.h"
#include "Kernel/CaloCellCode.h"

// ============================================================================
/** @file
 *  Implementation file for class InCaloFutureAcceptance
 *  @see InAccpetance
 *
 *  @author Victor EGORYCHEV Victor.Egorychev@cern.ch
 *  @author Vanya  BELYAEV    ibelyaev@phsycis.syr.edu
 *  @date   2006-05-28
 */
// ============================================================================
namespace LHCb::Calo {

  // ============================================================================
  // initialization @see IAlgTool
  // ============================================================================

  StatusCode InAcceptance::initialize() {
    StatusCode sc = TrackTool::initialize();
    if ( sc.isFailure() ) { return sc; }
    // check the detector
    if ( calo() == nullptr ) { return Error( "Invalid Detector" ); }
    // select the plane
    switch ( CaloCellCode::CaloNumFromName( detectorName() ) ) {
    case CaloCellCode::CaloIndex::EcalCalo:
      m_loc   = State::Location::ECalShowerMax;
      m_plane = calo()->plane( CaloPlane::ShowerMax );
      break; // BREAK
    case CaloCellCode::CaloIndex::HcalCalo:
      m_loc   = State::Location::MidHCal;
      m_plane = calo()->plane( CaloPlane::Middle );
      break; // BREAK
    default:
      return Error( "Invalid calorimeter TYPE! '" + detectorName() + "'" );
    }
    //
    if ( msgLevel( MSG::DEBUG ) ) { info() << "State to be used for aceptance check is '" << m_loc << "'" << endmsg; }
    return StatusCode::SUCCESS;
  }

  // ============================================================================
  // check the track is in acceptance of the given calorimeter
  // ============================================================================

  bool InAcceptance::inAcceptance( const Track* track ) const {
    // find the appropriate state
    auto state_ptr = TrackTool::state( *track, m_loc );
    if ( state_ptr == nullptr ) {
      LHCb::State state;
      // if there is no proper state - add it into the track!
      StatusCode sc = propagate( *track, plane(), state );
      if ( sc.isFailure() ) {
        Error( "failure from propagate()", sc ).ignore();
        return false; // RETURN
      }
      state.setLocation( m_loc );
      const_cast<Track*>( track )->addToStates( state );
      return ok( state.position() );
    } else {
      // check the point
      return ok( state_ptr->position() );
    }
  }

  DECLARE_COMPONENT_WITH_ID( InAcceptance, "InCaloFutureAcceptance" )
} // namespace LHCb::Calo

// ============================================================================
