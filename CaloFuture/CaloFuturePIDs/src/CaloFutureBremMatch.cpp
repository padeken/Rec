/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files
#include "CaloFutureInterfaces/CaloFutureMatch2D.h"
#include "CaloFutureInterfaces/ICaloFutureTrackMatch.h"
#include "CaloFutureTrackMatch.h"
#include "CaloMatchUtils.h"
#include "Event/CaloPosition.h"
#include "Event/Track.h"
#include "GaudiKernel/GaudiException.h"
#include "Linear.h"
#include <tuple>
#include <variant>

// ============================================================================
/** @file
 *  Implementation file for class CaloFutureBremMatch
 *  @date 2006-05-29
 *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
 */
// ============================================================================

namespace LHCb::Calo {
  class BremMatch final : public extends<TrackMatch, Interfaces::ITrackMatch> {
  public:
    BremMatch( const std::string& type, const std::string& name, const IInterface* parent );

    /// initialize the tool
    StatusCode initialize() override;

    /** the main matching method
     *  @see ICaloFutureTrackMatch
     *  @param calo_obj "calorimeter" object (position)
     *  @param track_obj tracking object (track)
     *  @param detector_plane detector's plane - hase to be initialized in calling method
     *  @param match_state - matching state
     */
    Interfaces::ITrackMatch::MatchResults
    match( const CaloPosition& calo_obj, const Track& track_obj,
           const Interfaces::ITrackMatch::MatchResults& old_match_results ) const override;

  private:
    Gaudi::Plane3D m_showerMax;
  };
  DECLARE_COMPONENT_WITH_ID( BremMatch, "CaloFutureBremMatch" )

  BremMatch::BremMatch( const std::string& type, const std::string& name, const IInterface* parent )
      : extends( type, name, parent ) {
    setProperty( "Calorimeter", DeCalorimeterLocation::Ecal ).ignore();
    setProperty( "Tolerance", "10" ).ignore();           // 10 millimeters
    setProperty( "zForFastExtrapolator", "0" ).ignore(); // 0  meters
  }

  StatusCode BremMatch::initialize() {
    return TrackMatch::initialize().andThen( [&] { m_showerMax = calo()->plane( CaloPlane::ShowerMax ); } );
  }

  Interfaces::ITrackMatch::MatchResults
  BremMatch::match( const CaloPosition& calo_obj, const Track& track_obj,
                    const Interfaces::ITrackMatch::MatchResults& old_match_results ) const {

    Interfaces::ITrackMatch::MatchResults match_result;
    match_result.plane = old_match_results.plane;
    match_result.state = old_match_results.state;

    Match2D calo_match;

    if ( old_match_results.is_new_calo_obj ) {
      calo_match = getBremMatch2D( calo_obj );
      if ( !calo_match ) {
        if ( msgLevel( MSG::WARNING ) ) { warning() << "match(): Error from fill(2D) -- " << endmsg; }
        /** According to old code all calls for this one calo objecte will be canceled.
           We send here signal: stop calling match for this calo object and load next one
        **/
        match_result.skip_this_calo = true;
        return match_result;
      }

      // find the proper plane in the detector
      const CaloPosition::Center& par = calo_obj.center();
      const Gaudi::XYZPoint       point( par( 0 ), par( 1 ), calo_obj.z() );
      if ( tolerance() < match_result.plane.Distance( point ) ) { match_result.plane = calo()->plane( point ); }
    } else {
      calo_match = std::get<Match2D>( old_match_results.matrix );
    }

    match_result.matrix = calo_match;

    // get the correct state (at TT)
    const State* ptr_state = TrackTool::state( track_obj, State::Location::AtTT );
    if ( !ptr_state ) { ptr_state = TrackTool::state( track_obj, State::Location::EndRich1 ); }
    if ( !ptr_state ) { ptr_state = TrackTool::state( track_obj, State::Location::BegRich1 ); }
    if ( !ptr_state ) { ptr_state = TrackTool::state( track_obj, State::Location::EndVelo ); }

    // no appropriate state is found
    if ( !ptr_state ) {
      // get the closest state to some artificial value
      ptr_state = &( track_obj.closestState( 2.0 * Gaudi::Units::meter ) );
      // allowed z ?
      if ( ptr_state->z() > 4.0 * Gaudi::Units::meter ) {
        if ( msgLevel( MSG::DEBUG ) ) { warning() << "match(): No appropriate states are found " << endmsg; }
        return match_result;
      }
    }

    // use the linear extrapolator
    StatusCode sc = Utils::Linear::propagate( *ptr_state, match_result.plane, match_result.state );
    if ( sc.isFailure() ) {
      if ( msgLevel( MSG::WARNING ) ) { warning() << "match(): failure from propagate (1) " << endmsg; }
      return match_result;
    }

    auto track_match = getMatch2D( match_result.state );
    if ( !track_match ) {
      if ( msgLevel( MSG::WARNING ) ) { warning() << "match(): error for fill(2D)" << endmsg; }
      /** According to old code this only means that matching failed so we cant calculate in proper way a chi2 value
       **/
      return match_result;
    }

    // make a real evaluation
    match_result.chi2 = chi2( calo_match, track_match );
    return match_result;
  }
} // namespace LHCb::Calo
