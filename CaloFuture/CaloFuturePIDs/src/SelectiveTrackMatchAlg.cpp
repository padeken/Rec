/*****************************************************************************\
 * (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
 *                                                                             *
 * This software is distributed under the terms of the GNU General Public      *
 * Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
 *                                                                             *
 * In applying this licence, CERN does not waive the privileges and immunities *
 * granted to it by virtue of its status as an Intergovernmental Organization  *
 * or submit itself to any jurisdiction.                                       *
 \*****************************************************************************/

#include "CaloDet/DeCalorimeter.h"
#include "CaloFutureInterfaces/CaloFutureMatch2D.h"
#include "CaloFutureUtils/CaloFutureAlgUtils.h"
#include "DetDesc/ConditionAccessorHolder.h"
#include "Event/CaloClusters_v2.h"
#include "Event/Track.h"
#include "Gaudi/Accumulators.h"
#include "GaudiAlg/Transformer.h"
#include "LHCbMath/GeomFun.h"
#include "LHCbMath/Line.h"
#include "Relations/Relation1D.h"
#include "Relations/RelationWeighted2D.h"
#include "SelectiveMatchUtils.h"
#include "TrackInterfaces/ITrackExtrapolator.h"
#include "TrackKernel/TrackFunctors.h"

/** @class SelectiveTrackMatchAlg SelectiveTrackMatchAlg.h
 *
 *  Matches tracks with local clusters in and around calo cell
 *  corresponding to track extrapolation.
 *  Sufficient since resolution is about cell size / sqrt(12).
 *
 *  @date   2020-09
 *  @author Maarten VAN VEGHEL
 */

// ============================================================================
/** @file
 *
 *  Implementation file for class SelectiveTrackMatchAlg
 *  It takes inspiration from TrackMatchAlg and ClassifyPhotonElectron
 *
 *  @date   2020-09
 *  @author Maarten VAN VEGHEL
 */

namespace LHCb::Calo {

  using CaloObjects     = LHCb::Event::Calo::Clusters;
  using TrackMatchTable = LHCb::RelationWeighted2D<CaloCellID, Track, float>;
  using Filter          = LHCb::Relation1D<LHCb::Track, bool>;

  using namespace LHCb::Calo::SelectiveMatchUtils;

  // main class
  class SelectiveTrackMatchAlg
      : public Gaudi::Functional::Transformer<TrackMatchTable( const DeCalorimeter&, const CaloObjects&,
                                                               const Track::Range&, const Filter&,
                                                               const cellSizeCovariances& ),
                                              DetDesc::usesConditions<DeCalorimeter, cellSizeCovariances>> {
  public:
    // standard constructor
    SelectiveTrackMatchAlg( const std::string& name, ISvcLocator* pSvc );

    // initialize
    StatusCode initialize() override;

    // main function/operator
    TrackMatchTable operator()( const DeCalorimeter&, const CaloObjects&, const Track::Range&, const Filter&,
                                const cellSizeCovariances& ) const override;

  private:
    // properties
    Gaudi::Property<std::vector<int>> m_type{
        this,
        "TrackTypes",
        {LHCb::Track::Types::Long, LHCb::Track::Types::Downstream, LHCb::Track::Types::Ttrack},
        "Types of input tracks"};
    int                   m_nmaxelements;
    Gaudi::Property<int>  m_nsquares{this,
                                    "nNeighborSquares",
                                    1,
                                    [this]( auto& ) { m_nmaxelements = std::pow( 2 * m_nsquares + 1, 2 ); },
                                    Gaudi::Details::Property::ImmediatelyInvokeHandler{true},
                                    "Number of squares of cells around central cell to scan for clusters"};
    Gaudi::Property<bool> m_usespread{this, "useSpread", false, "Use spread or cellsize/sqrt(12) for uncertainties"};
    Gaudi::Property<int>  m_caloplane{this, "CaloPlane", CaloPlane::Middle,
                                     "Plane at the calorimeter of where to extrapolate to (middle is for hadrons)"};

    Gaudi::Property<float> m_zmin_linear{this, "zMinLinear", 10.0 * Gaudi::Units::m,
                                         "Minimum distance in z where we can linearly extrapolate to Calo"};
    Gaudi::Property<float> m_zmin_para{this, "zMinParabolic", 7.5 * Gaudi::Units::m,
                                       "Minimum distance in z where we can parabolicaly extrapolate to Calo"};

    Gaudi::Property<int>   m_tablesize{this, "TableSize", 5000, "Size of track-cluster table"};
    Gaudi::Property<float> m_threshold{this, "MaxChi2Threshold", 10000., "Maximum allowed chi2 value"};
    Gaudi::Property<float> m_tolerance{this, "ExtrapolationTolerance", 1. * Gaudi::Units::mm,
                                       "Tolerance for track extrapolation to calo plane"};

    // functions
    bool propagateToCalo( State& state, const Track& track, const Gaudi::Math::Plane3D& plane ) const;

    // tools
    ToolHandle<ITrackExtrapolator> m_extrapolator_para{this, "ParabolicExtrapolator",
                                                       "TrackParabolicExtrapolator/FastExtrapolator"};
    ToolHandle<ITrackExtrapolator> m_extrapolator_ruku{this, "Extrapolator",
                                                       "TrackRungeKuttaExtrapolator/Extrapolator"};

    // statistics
    mutable Gaudi::Accumulators::StatCounter<>          m_nMatchFailure{this, "#match failure"};
    mutable Gaudi::Accumulators::StatCounter<>          m_nLinks{this, "#links in table"};
    mutable Gaudi::Accumulators::StatCounter<>          m_nOverflow{this, "#above threshold"};
    mutable Gaudi::Accumulators::StatCounter<float>     m_chi2{this, "average chi2"};
    mutable Gaudi::Accumulators::MsgCounter<MSG::ERROR> m_not_unique{this, "Failed to generate index"};
  };

  DECLARE_COMPONENT_WITH_ID( SelectiveTrackMatchAlg, "SelectiveTrackMatchAlg" )

  // ============================= IMPLEMENTATION ===============================

  // ============================================================================
  /*  Standard constructor
   *  @param name algorithm name
   *  @param pSvc service locator
   */
  // ============================================================================

  SelectiveTrackMatchAlg::SelectiveTrackMatchAlg( const std::string& name, ISvcLocator* pSvc )
      : Transformer( name, pSvc,
                     // Inputs
                     {KeyValue( "Detector", {CaloFutureAlgUtils::DeCaloFutureLocation( "Ecal" )} ),
                      KeyValue( "InputClusters", {CaloFutureAlgUtils::CaloFutureClusterLocation( "Ecal" )} ),
                      KeyValue( "InputTracks", {CaloFutureAlgUtils::TrackLocations().front()} ),
                      KeyValue( "Filter", {CaloFutureAlgUtils::CaloFutureIdLocation( "InEcal" )} ),
                      KeyValue( "cellSizeCovariances", {"AlgorithmSpecific-" + name + "-cellsizecovariances"} )},
                     // Outputs
                     {KeyValue( "Output", {CaloFutureAlgUtils::CaloFutureIdLocation( "ClusterMatch" )} )} ) {}

  // ============================================================================
  //   Initialization of algorithm / tool
  // ============================================================================
  StatusCode SelectiveTrackMatchAlg::initialize() {
    auto sc = Transformer::initialize().andThen( [&] {
      addConditionDerivation<cellSizeCovariances( const DeCalorimeter& )>(
          {CaloFutureAlgUtils::DeCaloFutureLocation( "Ecal" )}, inputLocation<cellSizeCovariances>() );
    } );
    return sc;
  }

  // ============================================================================
  //   Algorithm execution
  // ============================================================================
  TrackMatchTable SelectiveTrackMatchAlg::operator()( const DeCalorimeter& calo, const CaloObjects& caloobjects,
                                                      const Track::Range& tracks, const Filter& filter,
                                                      const cellSizeCovariances& cellsizecovs ) const {
    // declare output
    TrackMatchTable table( m_tablesize );

    // obtain the tracks to be looped over by checking and filtering
    LHCb::Track::ConstVector good_tracks;
    good_tracks.reserve( 2000 );
    std::copy_if( tracks.begin(), tracks.end(), std::back_inserter( good_tracks ), [&]( const auto& track ) {
      if ( m_type.value().end() == std::find( m_type.value().begin(), m_type.value().end(), track->type() ) ) {
        return false;
      }
      if ( track->checkFlag( LHCb::Track::Flags::Backward ) ) { return false; }
      const auto& r = filter.relations( track );
      return !r.empty() && r.front().to();
    } );

    // obtain cluster indeces
    auto caloindex = caloobjects.index();
    if ( !caloindex ) {
      ++m_not_unique;
      return table;
    }

    // some info that can be loaded before loop(s)
    const auto plane_at_calo = calo.plane( m_caloplane );
    const auto other_planes  = std::array{calo.plane( CaloPlane::Front ), calo.plane( CaloPlane::Back )};
    const auto cscovs        = cellsizecovs();

    // declare what is needed in the loop
    CaloCellID::Vector cellids;
    cellids.reserve( m_nmaxelements );
    std::vector<CaloObjects::const_reference> found_caloobjs;
    found_caloobjs.reserve( m_nmaxelements );

    LHCb::State     stateAtCalo;
    Match2D::Vector track_pos, calo_pos;
    Match2D::Matrix track_cov, comb_cov;

    // main loop over tracks
    for ( const auto& track : good_tracks ) {
      // get state at calo plane
      if ( !propagateToCalo( stateAtCalo, *track, plane_at_calo ) ) continue;

      // get closest cell to track extrapolation
      const auto closestcell = getClosestCellID( calo, stateAtCalo.position(), stateAtCalo.slopes(), other_planes );
      if ( !closestcell ) continue;

      // check if there is a calo object and otherwise look at neighboring cells
      found_caloobjs.clear();
      cellids.clear();
      const auto firstcaloobj = caloindex.find( closestcell );
      if ( firstcaloobj != caloindex.end() ) {
        found_caloobjs.push_back( *firstcaloobj );
      } else {
        cellids.push_back( closestcell );
        if ( !getNeighborCellIDs( cellids, calo, m_nsquares ) ) continue;
        // loop over local calo objects via cellids
        for ( const LHCb::CaloCellID cellid : cellids ) {
          const auto caloobj = caloindex.find( cellid );
          // if cellid does not have a cluster associated to it, continue
          if ( caloobj == caloindex.end() ) continue;
          // otherwise add
          found_caloobjs.push_back( *caloobj );
        }
        // quit if we didnt find calo objs
        if ( found_caloobjs.empty() ) continue;
      }

      // obtain relevant info for matching from track
      track_pos = {stateAtCalo.x(), stateAtCalo.y()};
      track_cov = stateAtCalo.errPosition().Sub<Gaudi::SymMatrix2x2>( 0, 0 );

      // loop over local calo objects and calculate chi2
      for ( const auto caloobj : found_caloobjs ) {
        // obtain calo obj info
        calo_pos = {caloobj->position().x(), caloobj->position().y()};
        comb_cov = ( m_usespread ? caloobj->spread() : cscovs[caloobj->cellID().area()] ) + track_cov;

        // calculate chi2
        if ( !comb_cov.Invert() ) {
          m_nMatchFailure += 1;
          continue;
        }
        auto chi2 = ROOT::Math::Similarity( calo_pos - track_pos, comb_cov );

        // check if it has proper value
        if ( m_threshold < chi2 ) {
          m_nOverflow += 1;
          continue;
        }

        // only now push proper result
        table.i_push( caloobj->cellID(), track, chi2 );
      }
    }
    // sort table (needed downstream of code!)
    table.i_sort();

    // monitor statistics: number of links and average chi2
    const auto& links  = table.i_relations();
    const auto  nLinks = links.size();
    m_nLinks += nLinks;
    accumulate( m_chi2, links, [nLinks]( const auto& l ) { return l.weight() / nLinks; } );

    return table;
  }

  // ============================================================================
  //   Helper member functions
  // ============================================================================

  bool SelectiveTrackMatchAlg::propagateToCalo( State& state, const Track& track, const Gaudi::Plane3D& plane ) const {
    // obtain closest state to calo plane (tilt is small enough to ignore in closest state search)
    state = closestState( track, -plane.HesseDistance() );
    // check how far, and decide which extrapolation is good enough
    if ( state.z() > m_zmin_linear ) {
      // quick linear extrapolation assuming only tilt of y-axis
      auto denom = 1 + plane.B() * state.ty();
      if ( denom == 0. ) return false;
      auto zintersect = ( -plane.HesseDistance() - plane.B() * ( state.y() - state.ty() * state.z() ) ) / denom;
      state.linearTransportTo( zintersect );
      return true;
    }
    // in case one is beyond the magnet, parabolic is good enough w.r.t.
    // precision due to cell size, otherwise still use RK
    return ( state.z() > m_zmin_para ? m_extrapolator_para : m_extrapolator_ruku )
        ->propagate( state, plane, m_tolerance )
        .isSuccess();
  }
} // namespace LHCb::Calo
