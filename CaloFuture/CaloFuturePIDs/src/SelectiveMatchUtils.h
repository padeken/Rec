/*****************************************************************************\
 * (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
 *                                                                             *
 * This software is distributed under the terms of the GNU General Public      *
 * Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
 *                                                                             *
 * In applying this licence, CERN does not waive the privileges and immunities *
 * granted to it by virtue of its status as an Intergovernmental Organization  *
 * or submit itself to any jurisdiction.                                       *
 \*****************************************************************************/
#ifndef SELECTIVEMATCHUTILS_H
#define SELECTIVEMATCHUTILS_H

#include "CaloDet/DeCalorimeter.h"
#include "CaloDet/DeSubCalorimeter.h"
#include "CaloDet/DeSubSubCalorimeter.h"
#include "CaloFutureInterfaces/CaloFutureMatch2D.h"
#include "CaloFutureInterfaces/CaloFutureMatch3D.h"
#include "CaloFutureUtils/CaloFutureNeighbours.h"
#include "LHCbMath/GeomFun.h"
#include "LHCbMath/Line.h"
#include <array>

/** @file
 *
 *  Implementation file for helper functions and classes for
 *  track - calo object matching algorithms using selective cell ID
 *  search in calo
 *
 *  @date   2020-09
 *  @author Maarten VAN VEGHEL
 */

namespace LHCb::Calo {

  namespace SelectiveMatchUtils {

    // simplified covariances based on cell size for outer, middle and inner region
    class cellSizeCovariances {
    private:
      std::array<double, 3>          m_cellsizes = {0., 0., 0.};
      std::array<Match2D::Matrix, 3> m_covariances;

    public:
      // constructor
      cellSizeCovariances( DeCalorimeter const& calo ) {
        // get cell sizes
        unsigned nFilledCellSizes = 0;
        for ( const auto subcalo : calo.subCalos() ) {
          for ( const auto subsubcalo : subcalo->subSubCalos() ) {
            unsigned area = subsubcalo->area();
            if ( m_cellsizes[area] == 0. ) {
              m_cellsizes[area] = subsubcalo->cellSize();
              ++nFilledCellSizes;
            }
            if ( nFilledCellSizes == 3 ) break;
          }
          if ( nFilledCellSizes == 3 ) break;
        }
        // convert them to covariance matrices
        Match2D::Matrix identity = ROOT::Math::SMatrixIdentity();
        m_covariances            = std::array<Match2D::Matrix, 3>{( std::pow( m_cellsizes[0], 2 ) / 12. ) * identity,
                                                       ( std::pow( m_cellsizes[1], 2 ) / 12. ) * identity,
                                                       ( std::pow( m_cellsizes[2], 2 ) / 12. ) * identity};
      }

      // access (operator) to covariances
      const std::array<Match2D::Matrix, 3>& operator()() const { return m_covariances; }
    };

    // to obtain closest cell id (or not) to track extrapolation
    inline CaloCellID getClosestCellID( const DeCalorimeter& calo, const Gaudi::XYZPoint& point,
                                        const Gaudi::XYZVector& slopes, LHCb::span<const Gaudi::Plane3D> planes ) {
      // is the point associated to a valid cell?
      auto centercellparam = calo.Cell_( point );
      // if not, scan along slope of track if there is maybe one around?
      if ( !centercellparam || !centercellparam->valid() ) {
        Gaudi::XYZPoint other_point_at_calo;
        double          mu   = 0.0;
        const auto&     line = Gaudi::Math::Line<Gaudi::XYZPoint, Gaudi::XYZVector>( point, slopes );
        for ( const auto& plane : planes ) {
          if ( !Gaudi::Math::intersection( line, plane, other_point_at_calo, mu ) ) { continue; }
          centercellparam = calo.Cell_( other_point_at_calo );
          if ( centercellparam && centercellparam->valid() ) { break; }
        }
        if ( !centercellparam || !centercellparam->valid() ) { return {}; }
      }
      // return valid cell
      return centercellparam->cellID();
    }

    // to obtain calo cell IDs around track extrapolation to calo
    inline bool getNeighborCellIDs( CaloCellID::Vector& cellids, const DeCalorimeter& calo,
                                    const unsigned int nSquares ) {
      // assume there is just one center cell in vector
      if ( cellids.size() != 1 ) return false;
      // add neighbours in squares around center cell
      switch ( nSquares ) {
      case 0:
        return true;
      case 1: {
        const auto& neighbors = calo.neighborCells( cellids[0] );
        cellids.insert( cellids.end(), neighbors.begin(), neighbors.end() );
        return true;
      }
      default: // add even more if needed
        return CaloFutureFunctors::neighbours( cellids, nSquares, &calo );
      }
    }

    // to get calo area of track position
    inline int getAreaForTrack( const Gaudi::XYZPoint& pointatcalo, const DeCalorimeter& calo ) {
      auto area = calo.Area( pointatcalo );
      if ( area == CaloCellCode::CaloArea::UndefinedArea ) {
        area = ( fabs( pointatcalo.x() ) < 2. * Gaudi::Units::m && fabs( pointatcalo.y() ) < 2. * Gaudi::Units::m )
                   ? CaloCellCode::CaloArea::Inner
                   : CaloCellCode::CaloArea::Outer;
      }
      return area;
    }

  } // namespace SelectiveMatchUtils

} // namespace LHCb::Calo
#endif
