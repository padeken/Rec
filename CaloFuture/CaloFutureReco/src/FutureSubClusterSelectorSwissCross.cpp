/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "CaloFutureUtils/ClusterFunctors.h"
#include "CellSwissCross.h"
#include "FutureSubClusterSelectorBase.h"

// ============================================================================
/** @file FutureSubClusterSelectorSwissCross.cpp
 *
 *  Implementation file for class : FutureSubClusterSelectorSwissCross
 *
 *  @author Vanya Belyaev Ivan.Belyaev@itep.ru
 *  @date 07/11/2001
 */
// ============================================================================
namespace LHCb::Calo {

  class SubClusterSelectorSwissCross : public SubClusterSelectorBase {
  public:
    using SubClusterSelectorBase::SubClusterSelectorBase;

    // ============================================================================
    /** standard initiliazation
     *  @return status code
     */
    // ============================================================================
    StatusCode initialize() override {
      /// initliaze the base class
      StatusCode sc = SubClusterSelectorBase::initialize();
      if ( sc.isFailure() ) return sc;
      if ( det() ) {
        m_matrix.setDet( det() );
      } else {
        return Error( "DeCalorimeter* ponts to NULL!" );
      }
      return sc;
    }

    // ============================================================================
    /** The main processing method
     *  @param cluster pointer to CaloCluster object to be processed
     *  @return status code
     */
    // ============================================================================
    StatusCode tag( Event::Calo::Clusters::Entries entries ) const override {
      return Functor::Cluster::tagTheSubCluster( entries, m_matrix, modify(), mask(),
                                                 DigitStatus::Mask::ModifiedBySwissCrossTagger );
    }

    // ============================================================================
    /** The main processing method (untag)
     *  @param cluster pointer to CaloCluster object to be processed
     *  @return status code
     */
    // ============================================================================
    StatusCode untag( Event::Calo::Clusters::Entries entries ) const override {
      return Functor::Cluster::untagTheSubCluster( entries, m_matrix, DigitStatus::Mask::ModifiedBySwissCrossTagger );
    }

  private:
    CellSwissCross m_matrix;
  };

  DECLARE_COMPONENT( SubClusterSelectorSwissCross )

} // namespace LHCb::Calo
// ============================================================================
// The End
// ============================================================================
