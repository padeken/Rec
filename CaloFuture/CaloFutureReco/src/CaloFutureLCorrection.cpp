/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "CaloDet/DeCalorimeter.h"
#include "CaloFutureCorrectionBase.h"
#include "Gaudi/Accumulators.h"
#include "GaudiKernel/SystemOfUnits.h"
#include "ICaloFutureHypoTool.h"
#include <cmath>
#include <string>

/** @class CaloFutureLCorrection CaloFutureLCorrection.h
 *
 *
 *  @author Deschamps Olivier
 *  @date   2003-03-10
 *  revised 2010

 *  Adam Szabelski
 *  date 2019-10-15
 */

class CaloFutureLCorrection : public extends<CaloFutureCorrectionBase, LHCb::Calo::Interfaces::IProcessHypos> {
public:
  CaloFutureLCorrection( const std::string& type, const std::string& name, const IInterface* parent );
  using LHCb::Calo::Interfaces::IProcessHypos::process;
  StatusCode process( LHCb::Event::Calo::Hypotheses::Type, LHCb::Event::Calo::Clusters::Range ) const override;

private:
  Gaudi::XYZPoint m_origin = {0, 0, 0};

  template <typename Point>
  std::array<double, 4> compute( const Point& pos, LHCb::CaloCellID cellID, double energy ) const {
    int area = cellID.area();
    assert( area >= 0 && area <= 2 ); // TODO: comment assert out
    // Account for the tilt
    const auto   plane  = m_det->plane( CaloPlane::Front ); // Ecal Front-Plane
    const auto   normal = plane.Normal();
    const double Hesse  = plane.HesseDistance();

    const double xg = pos.x() - m_origin.X();
    const double yg = pos.y() - m_origin.Y();
    const double z0 = ( -Hesse - normal.X() * pos.x() - normal.Y() * pos.y() ) / normal.Z();
    double       zg = z0 - m_origin.Z();

    // hardcoded inner offset (+7.5 mm)
    if ( area == 2 ) { zg += 7.5; }

    // Uncorrected angle
    double xy_offset  = std::sqrt( xg * xg + yg * yg );
    double xyz_offset = std::sqrt( xg * xg + yg * yg + zg * zg );
    double tan_theta  = xy_offset / zg;

    // Corrected angle

    double gamma0 = getCorrection( CaloFutureCorrection::gamma0, cellID ).value_or( 1 );
    double delta0 = getCorrection( CaloFutureCorrection::delta0, cellID ).value_or( 1 );

    // NB: gammaP(ePrs = 0) != 0, deltaP(ePrs = 0) != 0 and depend on cellID.area()
    // get gammaP and deltaP parameters (depending on cellID.area() for each cluster
    double gammaP = getCorrection( CaloFutureCorrection::gammaP, cellID, 0. ).value_or( 1 );
    double deltaP = getCorrection( CaloFutureCorrection::deltaP, cellID, 0. ).value_or( 1 );
    double g      = gamma0 - gammaP;
    double d      = delta0 + deltaP;

    double     tg_fps    = ( energy > 0.0 ? g * LHCb::Math::fast_log( energy / Gaudi::Units::GeV ) + d : 0.0 );
    double     temp      = ( 1. + tg_fps / xyz_offset );
    double     cos_theta = temp / std::sqrt( tan_theta * tan_theta + temp * temp );
    const auto dz_fps    = cos_theta * tg_fps;
    return {g, d, z0, dz_fps};
  }
};

DECLARE_COMPONENT( CaloFutureLCorrection )

CaloFutureLCorrection::CaloFutureLCorrection( const std::string& type, const std::string& name,
                                              const IInterface* parent )
    : extends( type, name, parent ) {
  // define conditionName
  const std::string uName( LHCb::CaloFutureAlgUtils::toUpper( name ) );
  if ( uName.find( "ELECTRON" ) != std::string::npos ) {
    setProperty( "ConditionName", "Conditions/Reco/Calo/ElectronLCorrection" ).ignore();
  } else if ( uName.find( "MERGED" ) != std::string::npos || uName.find( "SPLITPHOTON" ) != std::string::npos ) {
    setProperty( "ConditionName", "Conditions/Reco/Calo/SplitPhotonLCorrection" ).ignore();
  } else if ( uName.find( "PHOTON" ) ) {
    setProperty( "ConditionName", "Conditions/Reco/Calo/PhotonLCorrection" ).ignore();
  }
}

StatusCode CaloFutureLCorrection::process( LHCb::Event::Calo::Hypotheses::Type hypo,
                                           LHCb::Event::Calo::Clusters::Range  clusters ) const {
  // check the Hypo
  const auto h = std::find( m_hypos.begin(), m_hypos.end(), hypo );
  if ( m_hypos.end() == h ) {
    Error( "Invalid hypothesis -> no correction applied", StatusCode::SUCCESS ).ignore();
    return StatusCode::FAILURE;
  }

  for ( auto&& cluster : clusters ) {

    // No correction for negative energy :
    if ( cluster.e() < 0. ) { continue; }

    auto [g, d, z0, dz_fps] = compute( cluster.position(), cluster.seed(), cluster.e() );

    // Recompute Z position and fill CaloPosition
    cluster.position().SetZ( z0 + dz_fps );
  }
  return StatusCode::SUCCESS;
}
