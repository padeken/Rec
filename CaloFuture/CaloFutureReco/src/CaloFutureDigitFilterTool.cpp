/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "CaloDet/DeCalorimeter.h"
#include "CaloFutureUtils/CaloFutureAlgUtils.h"
#include "Event/RecVertex.h"
#include "GaudiAlg/GaudiTool.h"
#include "ICaloFutureDigitFilterTool.h" // Interface
#include "Kernel/CaloCellID.h"
//-----------------------------------------------------------------------------
// Implementation file for class : CaloFutureDigitFilterTool
//
// 2010-12-13 : Olivier Deschamps
//-----------------------------------------------------------------------------
namespace Gaudi::Parsers {
  StatusCode parse( std::map<CaloCellCode::CaloIndex, int>& out, const std::string& s ) {
    out.clear();
    std::map<std::string, int> m;
    return parse( m, s ).andThen( [&] {
      std::transform( m.begin(), m.end(), std::insert_iterator( out, out.end() ), []( const auto& p ) {
        if ( p.first == "Default" ) return std::pair{CaloCellCode::CaloIndex::Undefined, p.second};
        auto k = CaloCellCode::caloNum( p.first );
        if ( k < 0 ) throw std::invalid_argument{std::string{"unknown Calo: "} + p.first};
        return std::pair{k, p.second};
      } );
    } );
  }
} // namespace Gaudi::Parsers

/** @class CaloFutureDigitFilterTool CaloFutureDigitFilterTool.h
 *
 *
 *  @author Olivier Deschamps
 *  @date   2010-12-13
 */
class CaloFutureDigitFilterTool final : public extends<GaudiTool, ICaloFutureDigitFilterTool> {
public:
  /// Standard constructor
  CaloFutureDigitFilterTool( const std::string& type, const std::string& name, const IInterface* parent );

  StatusCode initialize() override; ///< Algorithm initialization

  int method( CaloCellCode::CaloIndex det ) const override {
    if ( !m_calo || det != m_calo->index() ) { throw std::invalid_argument( "Wrong Calorimeter for this instance" ); }
    return m_scalingMethod;
  }
  double offset( LHCb::CaloCellID id, Pileup_t pileup ) const override;
  int    i_pileup() const override;

private:
  int        getScale() const;
  StatusCode caloUpdate(); /// Triggered by calo updates
  bool       setDet( const CaloCellCode::CaloIndex det );
  double     getOffset( LHCb::CaloCellID id, int scale ) const;

  using Offsets = std::map<LHCb::CaloCellID, double>;
  const Offsets& offsets() const noexcept { return *m_offsets; }
  const Offsets& offsetsRMS() const noexcept { return *m_offsetsRMS; }
  struct CondMaps {
    Offsets offsets, offsetsRMS;
  };
  const CondMaps& createMaps( DeCalorimeter* calo, bool regUpdate = true );
  void            setMaps( const CondMaps& maps ) {
    m_offsets    = &maps.offsets;
    m_offsetsRMS = &maps.offsetsRMS;
  }

private:
  Gaudi::Property<int>    m_scalingMethod{this, "ScalingMethod", 0}; // 0 : SpdMult ; 1 = nPV  (+10 for clusters)
  Gaudi::Property<bool>   m_useCondDB{this, "UseCondDB", true};
  Gaudi::Property<int>    m_scalingBin{this, "ScalingBin", 50};
  Gaudi::Property<double> m_scalingMin{this, "ScalingMin", 150.};
  DataObjectReadHandle<LHCb::RecVertices>  m_vertLoc{this, "PrimaryVertices", LHCb::RecVertexLocation::Primary};
  Gaudi::Property<CaloCellCode::CaloIndex> m_which{this, "Calorimeter",
                                                   CaloCellCode::CaloIndex::EcalCalo}; // TODO: add handler to veto
                                                                                       // changes after ::initialize...
  Gaudi::Property<std::map<CaloCellCode::CaloIndex, int>> m_maskMap{
      this, "MaskMap", {std::pair{CaloCellCode::CaloIndex::Undefined, CaloCellQuality::OfflineMask}}};

  CondMaps              m_ecalMaps;
  CondMaps              m_hcalMaps;
  static const CondMaps m_nullMaps;

  std::map<DeCalorimeter*, std::unique_ptr<CondMaps>> m_offsetMap;
  using OffsetsPtr        = Offsets const*;
  OffsetsPtr m_offsets    = nullptr;
  OffsetsPtr m_offsetsRMS = nullptr;

  DeCalorimeter* m_calo = nullptr;
};

// Declaration of the Tool Factory
DECLARE_COMPONENT( CaloFutureDigitFilterTool )

const CaloFutureDigitFilterTool::CondMaps CaloFutureDigitFilterTool::m_nullMaps = {};

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
CaloFutureDigitFilterTool::CaloFutureDigitFilterTool( const std::string& type, const std::string& name,
                                                      const IInterface* parent )
    : extends( type, name, parent ) {
  declareProperty( "EcalOffset", m_ecalMaps.offsets );
  declareProperty( "HcalOffset", m_hcalMaps.offsets );
  declareProperty( "EcalOffsetRMS", m_ecalMaps.offsetsRMS );
  declareProperty( "HcalOffsetRMS", m_hcalMaps.offsetsRMS );
}
//=============================================================================

StatusCode CaloFutureDigitFilterTool::initialize() {
  return extends::initialize().andThen( [&] {
    // check
    for ( const auto& i : m_maskMap ) {
      if ( ( i.second & CaloCellQuality::OfflineMask ) == 0 )
        warning() << "OfflineMask flag is disabled for " << i.first << " - Are you sure ?" << endmsg;
    }
    setDet( m_which );
  } );
}

//-----------------------
StatusCode CaloFutureDigitFilterTool::caloUpdate() {
  // loop over all current maps and recreate
  for ( auto& maps : m_offsetMap ) createMaps( maps.first, false );
  const auto offsets = m_offsetMap.find( m_calo );
  setMaps( offsets != m_offsetMap.end() ? *( offsets->second ) : createMaps( m_calo ) );
  return StatusCode::SUCCESS;
}

//-----------------------
const CaloFutureDigitFilterTool::CondMaps& CaloFutureDigitFilterTool::createMaps( DeCalorimeter* calo,
                                                                                  bool           regUpdate ) {
  // Map new set of maps for this calo
  m_offsetMap[calo] = std::make_unique<CondMaps>();
  auto* maps        = m_offsetMap[calo].get();
  // fill the maps from the CaloFuture DetElem
  for ( const auto& c : calo->cellParams() ) {
    const auto id = c.cellID();
    if ( !calo->valid( id ) || id.isPin() ) continue;
    maps->offsets[id]    = c.pileUpOffset();
    maps->offsetsRMS[id] = c.pileUpOffsetRMS();
  }
  // if first time, register callback for future updates
  if ( regUpdate ) { updMgrSvc()->registerCondition( this, calo, &CaloFutureDigitFilterTool::caloUpdate ); }
  // return the new maps
  return *maps;
}

//-----------------------
bool CaloFutureDigitFilterTool::setDet( CaloCellCode::CaloIndex det ) {
  if ( m_calo && m_calo->index() == det ) return true;
  m_calo = getDet<DeCalorimeter>( LHCb::CaloFutureAlgUtils::DeCaloFutureLocation( det ) );
  if ( !m_calo ) return false;
  assert( m_calo->index() == det );
  auto it = m_maskMap.find( det );
  if ( it == m_maskMap.end() ) it = m_maskMap.find( CaloCellCode::CaloIndex::Undefined );
  if ( m_useCondDB.value() ) {
    m_scalingMethod = m_calo->pileUpSubstractionMethod();
    m_scalingMin    = m_calo->pileUpSubstractionMin();
    m_scalingBin    = m_calo->pileUpSubstractionBin();
    if ( m_scalingMethod < 0 ) {
      setMaps( m_nullMaps );
    } else {
      auto caloOffs = m_offsetMap.find( m_calo );
      setMaps( caloOffs != m_offsetMap.end() ? *( caloOffs->second ) : createMaps( m_calo ) );
    }
  } else {
    setMaps( det == CaloCellCode::CaloIndex::EcalCalo
                 ? m_ecalMaps
                 : det == CaloCellCode::CaloIndex::HcalCalo ? m_hcalMaps : m_nullMaps );
  }
  return true;
}

//-----------------------
double CaloFutureDigitFilterTool::getOffset( LHCb::CaloCellID id, int scale ) const {
  if ( 0 == scale ) return 0.;
  if ( scale <= m_scalingMin ) return 0.;
  const auto& table = offsets();
  const auto  it    = table.find( id );
  if ( it == table.end() ) return 0.;
  const auto& ref = it->second;
  //  overlap probabilty (approximate)
  constexpr double ncells = 6016.;
  const double     aa     = 4 * scale / ncells;
  double           rscale =
      ( aa < 1 ? 0.5 * ncells * ( 1. - std::sqrt( 1. - aa ) ) : ( double( ncells * scale ) / ( ncells - scale ) ) );
  return ref * ( rscale - m_scalingMin ) / m_scalingBin;
}

//-----------------------
int CaloFutureDigitFilterTool::i_pileup() const {
  if ( m_scalingMethod == 1 || m_scalingMethod == 11 ) {
    if ( const LHCb::RecVertices* verts = m_vertLoc.getIfExists(); verts ) return verts->size();
  }
  return 0;
}

double CaloFutureDigitFilterTool::offset( LHCb::CaloCellID id, Pileup_t pileup ) const {
  if ( !m_calo || id.calo() != m_calo->index() ) {
    throw std::invalid_argument( "Wrong Calorimeter for this instance" );
  }
  return getOffset( id, pileup );
}
