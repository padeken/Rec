/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "CaloFutureCorrectionBase.h"
#include "Event/ProtoParticle.h"

namespace Gaudi::Parsers {
  StatusCode parse( std::vector<LHCb::Event::Calo::Hypotheses::Type>& r, const std::string& s ) {
    std::vector<std::string> sv;
    return parse( sv, s ).orThrow( "Oops", "Oops!" ).andThen( [&] {
      for ( const auto& i : sv ) {
        LHCb::Event::Calo::Hypotheses::Type t;
        parse( t, i ).andThen( [&] { r.push_back( t ); } ).orThrow( "Oops", "Oops!" ).ignore();
      }
    } );
  }
} // namespace Gaudi::Parsers

//-----------------------------------------------------------------------------
// Implementation file for class : CaloFutureCorrectionBase
//
// 2010-05-07 : Olivier Deschamps
//-----------------------------------------------------------------------------
namespace CaloFutureCorrection {
  namespace {
    const auto typeName = std::array<std::string, nT>{"alphaG",
                                                      "alphaE",
                                                      "alphaB",
                                                      "alphaX",
                                                      "alphaY",
                                                      "RESERVED(alphaP)",
                                                      "RESERVED(beta)",
                                                      "RESERVED(betaP)",
                                                      "RESERVED(betaPR)",
                                                      "RESERVED(betaC)",
                                                      "RESERVED(betaCP)",
                                                      "RESERVED(betaCPR)", // E-corrections
                                                      "RESERVED(globalC)",
                                                      "globalT",
                                                      "offsetT",
                                                      "offset",
                                                      "RESERVED(offsetC)",
                                                      "ClusterCovariance",
                                                      "gamma0",
                                                      "delta0",
                                                      "gammaP",
                                                      "deltaP", // L-Corrections
                                                      "shapeX",
                                                      "shapeY",
                                                      "residual",
                                                      "residualX",
                                                      "residualY",
                                                      "asymP",
                                                      "asymM",
                                                      "angularX",
                                                      "angularY", // S-Corrections
                                                      "profile",
                                                      "RESERVED(profileC)", // Profile shape
                                                      "EnergyMask",
                                                      "PositionMask",
                                                      "Unknown"};
  }
  const std::string& toString( Type t ) { return typeName.at( t ); }
} // namespace CaloFutureCorrection

CaloFutureCorrectionBase::CorrectionResult CaloFutureCorrectionBase::Polynomial::correction( double var ) const {
  double cor = pars.front();
  double v   = var;
  for ( auto i = std::next( pars.begin() ); i != pars.end(); ++i ) {
    cor += ( *i ) * v;
    v *= var;
  }
  double derivative = 0.;
  v                 = 1.;
  int cnt           = 1;
  for ( auto i = std::next( pars.begin() ); i != pars.end(); ++i, ++cnt ) {
    derivative += cnt * ( *i ) * v;
    v *= var;
  }
  return {cor, derivative};
}

CaloFutureCorrectionBase::CorrectionResult CaloFutureCorrectionBase::InversePolynomial::correction( double var ) const {
  auto cor = Polynomial::correction( var );
  if ( cor.value != 0 ) cor.value = 1. / cor.value;
  cor.derivative *= -cor.value * cor.value;
  return cor;
}

CaloFutureCorrectionBase::CorrectionResult CaloFutureCorrectionBase::ExpPolynomial::correction( double var ) const {
  auto cor = Polynomial::correction( var );
  if ( cor.value != 0 ) { cor.value = ( var == 0. ? cached : LHCb::Math::fast_exp( cor.value ) ); }
  cor.derivative *= cor.value;
  return cor;
}

CaloFutureCorrectionBase::CorrectionResult
CaloFutureCorrectionBase::ReciprocalPolynomial::correction( double var ) const {
  if ( var == 0 ) return {pars[0], 0.}; // CHECKME
  auto cor = Polynomial::correction( 1.0 / var );
  cor.derivative *= -1.0 / ( var * var );
  return cor;
}

CaloFutureCorrectionBase::CorrectionResult CaloFutureCorrectionBase::Sigmoid::correction( double var ) const {
  auto mytanh = []( auto x ) {
    const auto y = LHCb::Math::fast_exp( -2 * x );
    return ( 1 - y ) / ( 1 + y );
  };
  auto mytanh_val = mytanh( c * ( var + d ) );
  return {a + b * mytanh_val, b * c * ( 1 - mytanh_val * mytanh_val )};
}

CaloFutureCorrectionBase::CorrectionResult CaloFutureCorrectionBase::Sshape::correction( double var ) const {
  if ( b > 0 ) {
    double arg  = cache * var / delta;
    auto   sapo = std::sqrt( arg * arg + 1 );
    return {b * LHCb::Math::fast_log( arg + sapo ), b / delta * cache / sapo};
  }
  return {0, 0}; // CHECKME
}

CaloFutureCorrectionBase::CorrectionResult CaloFutureCorrectionBase::Sinusoidal::correction( double var ) const {
  constexpr double twopi = 2 * M_PI;
  double           sin_val, cos_val;
  LHCb::Math::fast_sincos( twopi * var, sin_val, cos_val );
  return {A * sin_val, A * twopi * cos_val};
}

CaloFutureCorrectionBase::CorrectionResult CaloFutureCorrectionBase::ShowerProfile::correction( double var ) const {
  if ( var > 0.5 ) {
    auto tmp1 = pars[0] * LHCb::Math::fast_exp( -pars[1] * var );
    auto tmp2 = pars[2] * LHCb::Math::fast_exp( -pars[3] * var );
    auto tmp3 = pars[4] * LHCb::Math::fast_exp( -pars[5] * var );
    return {tmp1 + tmp2 + tmp3, -pars[1] * tmp1 - pars[3] * tmp2 - pars[5] * tmp3};
  } else {
    auto tmp1 = pars[6] * LHCb::Math::fast_exp( -pars[7] * var );
    auto tmp2 = pars[8] * LHCb::Math::fast_exp( -pars[9] * var );
    return {2 - tmp1 - tmp2, pars[7] * tmp1 + pars[9] * tmp2};
  }
}

DECLARE_COMPONENT( CaloFutureCorrectionBase )

CaloFutureCorrectionBase::CaloFutureCorrectionBase( const std::string& type, const std::string& name,
                                                    const IInterface* parent )
    : GaudiTool( type, name, parent ) {
  declareInterface<CaloFutureCorrectionBase>( this );
}

StatusCode CaloFutureCorrectionBase::initialize() {
  StatusCode sc = GaudiTool::initialize();
  if ( sc.isFailure() ) return sc;

  if ( UNLIKELY( msgLevel( MSG::DEBUG ) ) ) debug() << "==> Initialize" << endmsg;

  if ( UNLIKELY( msgLevel( MSG::DEBUG ) ) ) debug() << "Condition name : " << m_conditionName << endmsg;
  // locate and set and configure the Detector
  m_det = getDet<DeCalorimeter>( m_detData );
  if ( !m_det ) { return StatusCode::FAILURE; }
  m_calo = {m_det->index()};

  if ( m_hypos.empty() ) return Error( "Empty vector of allowed Calorimeter Hypotheses!" );

  // debug printout of all allowed hypos
  if ( UNLIKELY( msgLevel( MSG::DEBUG ) ) ) {
    debug() << " List of allowed hypotheses : " << endmsg;
    for ( const auto& h : m_hypos ) { debug() << " -->" << h << endmsg; }
    for ( const auto& c : m_corrections ) { debug() << "Accepted corrections :  '" << c << "'" << endmsg; }
  }

  return setConditionParams( m_conditionName );
}

StatusCode CaloFutureCorrectionBase::finalize() {
  if ( UNLIKELY( msgLevel( MSG::DEBUG ) ) ) debug() << "==> Finalize" << endmsg;

  if ( m_corrections.size() > 1 || *( m_corrections.begin() ) != "All" ) {
    for ( const auto& c : m_corrections ) { info() << "Accepted corrections :  '" << c << "'" << endmsg; }
  }
  if ( m_corrections.empty() ) warning() << "All corrections have been disabled for " << name() << endmsg;

  if ( !m_cond )
    warning() << " Applied corrections configured via options for  " << name() << endmsg;
  else if ( UNLIKELY( msgLevel( MSG::DEBUG ) ) )
    debug() << " Applied corrections configured via condDB  ('" << m_conditionName << "') for " << name() << endmsg;

  if ( UNLIKELY( msgLevel( MSG::DEBUG ) ) ) {
    for ( auto itype = 0; itype < static_cast<int>( CaloFutureCorrection::nT ); ++itype ) {
      for ( auto area = 0; area < 3; ++area ) {
        debug() << " o  '" << static_cast<CaloFutureCorrection::Type>( itype ) << "'  correction as a '"
                << correctionName( m_params[itype][area] ) << "' function" << endmsg;
      }
    }
  }

  m_hypos.clear();

  return GaudiTool::finalize(); // must be called after all other actions
}

//=============================================================================
StatusCode CaloFutureCorrectionBase::setDBParams() {
  if ( UNLIKELY( msgLevel( MSG::DEBUG ) ) )
    debug() << "Get params from CondDB condition = " << m_conditionName << endmsg;
  registerCondition( m_conditionName, m_cond, &CaloFutureCorrectionBase::updParams );
  return runUpdate();
}
// ============================================================================
StatusCode CaloFutureCorrectionBase::setOptParams() {
  if ( UNLIKELY( msgLevel( MSG::DEBUG ) ) ) debug() << "Get params from options " << endmsg;
  if ( m_optParams.empty() && m_conditionName != "none" ) {
    info() << "No default options parameters defined" << endmsg;
    return StatusCode::SUCCESS;
  }
  for ( const auto& [name, parVec] : m_optParams ) {
    auto type = accept( name );
    if ( !type ) continue;
    if ( parVec.size() < 2 ) {
      error() << " # of parameters is insufficient!!" << endmsg;
      continue;
    }
    constructParams( m_params[*type], parVec );
    if ( UNLIKELY( msgLevel( MSG::DEBUG ) ) )
      debug() << " o Will apply correction '" << *type << "' as a '" << correctionName( m_params[*type].front() )
              << "' function" << endmsg;
  }
  return StatusCode::SUCCESS;
}
// ============================================================================
StatusCode CaloFutureCorrectionBase::updParams() {
  if ( UNLIKELY( msgLevel( MSG::DEBUG ) ) ) debug() << "updParams() called" << endmsg;
  if ( !m_cond ) return Error( "Condition points to NULL", StatusCode::FAILURE );

  for ( const auto& paramName : m_cond->paramNames() ) {
    auto type = accept( paramName );
    if ( !type || !m_cond->exists( paramName ) ) continue;
    const auto& parVec = m_cond->param<std::vector<double>>( paramName );
    if ( parVec.size() < 2 ) {
      error() << " # of parameters is insufficient!!" << endmsg;
      continue;
    }
    constructParams( m_params[*type], parVec );
    if ( UNLIKELY( msgLevel( MSG::DEBUG ) ) ) {
      debug() << " o Will apply correction '" << *type << "' as a '" << correctionName( m_params[*type].front() )
              << "' function" << endmsg;
    }
  }
  return StatusCode::SUCCESS;
}

void CaloFutureCorrectionBase::constructParams( std::array<Function, area_size>& fun,
                                                LHCb::span<const double>         params ) {
  const auto func = static_cast<CaloFutureCorrection::Function>( std::lround( params[0] ) );
  const auto dim  = static_cast<int>( std::lround( params[1] ) );
  if ( func == CaloFutureCorrection::GlobalParamList ) {
    if ( dim + 2 != static_cast<int>( params.size() ) ) {
      warning() << "o Size of DB parameter vector does not match the expectation for the DB specified type." << endmsg;
    }
    for ( auto& i : fun ) i = ParamVector{{std::next( params.begin(), 2 ), params.end()}};
  } else {
    if ( 3 * dim + 2 != static_cast<int>( params.size() ) ) {
      warning() << "o Size of DB parameter vector does not match the expectation for the DB specified type." << endmsg;
    }
    for ( unsigned int area = 0; area != area_size; ++area ) {
      // transpose the parameters into the right order...
      std::vector<double> p;
      p.reserve( dim );
      int pos = 2 + area;
      for ( int i = 0; i < dim; ++i ) {
        p.push_back( params[pos] );
        pos += 3;
      }

      switch ( func ) {
      case CaloFutureCorrection::Polynomial:
        if ( !p.empty() ) {
          fun[area] = Polynomial{{p.begin(), p.end()}};
        } else {
          error() << "Inconsistent # of parameters for " << func << endmsg;
        }
        break;
      case CaloFutureCorrection::InversPolynomial:
        if ( !p.empty() ) {
          fun[area] = CaloFutureCorrectionBase::InversePolynomial{p};
        } else {
          error() << "Inconsistent # of parameters for " << func << endmsg;
        }
        break;
      case CaloFutureCorrection::ExpPolynomial:
        if ( !p.empty() ) {
          fun[area] = CaloFutureCorrectionBase::ExpPolynomial{p};
        } else {
          error() << "Inconsistent # of parameters for " << func << endmsg;
        }
        break;
      case CaloFutureCorrection::ReciprocalPolynomial:
        if ( !p.empty() ) {
          fun[area] = CaloFutureCorrectionBase::ReciprocalPolynomial{p};
        } else {
          error() << "Inconsistent # of parameters for " << func << endmsg;
        }
        break;
      case CaloFutureCorrection::Sigmoid:
        if ( p.size() == 4 ) {
          fun[area] = CaloFutureCorrectionBase::Sigmoid{LHCb::make_span( p ).first<4>()};
        } else {
          error() << "Inconsistent # of parameters for sigmoid" << endmsg;
        }
        break;
      case CaloFutureCorrection::Sshape:
        if ( p.size() == 1 ) {
          fun[area] = CaloFutureCorrectionBase::Sshape{p[0]};
        } else {
          error() << "Inconsistent # of parameters for Sshape" << endmsg;
        }
        break;
      case CaloFutureCorrection::SshapeMod:
        if ( p.size() == 1 ) {
          fun[area] = CaloFutureCorrectionBase::SshapeMod{p[0]};
        } else {
          error() << "Inconsistent # of parameters for SshapeMod" << endmsg;
        }
        break;
      case CaloFutureCorrection::ShowerProfile:
        if ( p.size() == 10 ) {
          fun[area] = CaloFutureCorrectionBase::ShowerProfile{LHCb::make_span( p ).first<10>()};
        } else {
          error() << "Inconsistent # of parameters for ShowerProfile" << endmsg;
        }
        break;
      case CaloFutureCorrection::Sinusoidal:
        if ( p.size() == 1 ) {
          fun[area] = CaloFutureCorrectionBase::Sinusoidal{p[0]};
        } else {
          error() << "Inconsistent # of parameters for Sinusoidal" << endmsg;
        }
        break;
      case CaloFutureCorrection::ParamList:
        assert( !p.empty() );
        fun[area] = CaloFutureCorrectionBase::ParamVector{{p.begin(), p.end()}};
        break;
      default:
        error() << "got unknown function" << endmsg;
        break;
      }
    }
  }
}

std::optional<CaloFutureCorrectionBase::CorrectionResult>
CaloFutureCorrectionBase::getCorrectionAndDerivative( const CaloFutureCorrection::Type type, const LHCb::CaloCellID id,
                                                      const double var ) const {
  return std::visit(
      Gaudi::overload( [&]( const auto& data ) -> std::optional<CorrectionResult> { return data.correction( var ); },
                       []( const ParamVector& ) -> std::optional<CorrectionResult> {
                         return std::nullopt; // FIXME: throw exception instead? (and drop the use of optional)
                       } ),
      m_params[type][id.area()] );
}

std::optional<double> CaloFutureCorrectionBase::getCorrection( const CaloFutureCorrection::Type type,
                                                               const LHCb::CaloCellID id, const double var ) const {
  return std::visit(
      Gaudi::overload( [&]( const auto& data ) -> std::optional<double> { return data.correction( var ).value; },
                       []( const ParamVector& ) -> std::optional<double> {
                         return std::nullopt; // FIXME: throw exception instead? (and drop the use of optional)
                       } ),
      m_params[type][id.area()] );
}

StatusCode CaloFutureCorrectionBase::setConditionParams( const std::string& cond,
                                                         bool force ) { // force = true : forcing access via condDB only
  if ( cond != m_conditionName ) m_conditionName = cond;

  // get parameters from options  :
  if ( !m_useCondDB && !force ) return setOptParams();

  // get from DB if exists :
  if ( !existDet<DataObject>( m_conditionName ) ) {
    if ( force ) {
      if ( m_conditionName != "none" )
        info() << "Condition '" << m_conditionName.value() << "' has not been found " << endmsg;
      return StatusCode::SUCCESS;
    } else {
      if ( UNLIKELY( msgLevel( MSG::DEBUG ) ) )
        debug() << " Condition '" << m_conditionName.value() << "' has not found -- try options parameters !" << endmsg;
      return setOptParams();
    }
  }
  return setDBParams();
}
