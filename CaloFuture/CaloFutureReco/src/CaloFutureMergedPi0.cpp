/*****************************************************************************\
* (c) Copyright 2018 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
// Include files
// ============================================================================
#include "CaloDet/DeCalorimeter.h"
#include "CaloFutureUtils/CaloFutureAlgUtils.h"
#include "CaloFutureUtils/CaloMomentum.h"
#include "DetDesc/ConditionAccessorHolder.h"
#include "Event/CaloClusters_v2.h"
#include "Event/CaloDataFunctor.h"
#include "Event/CaloHypos_v2.h"
#include "Event/CellID.h"
#include "FutureSubClusterSelectorTool.h"
#include "GaudiAlg/Transformer.h"
#include "GaudiKernel/SystemOfUnits.h"
#include "ICaloFutureClusterTool.h"
#include "ICaloFutureHypoTool.h"
#include "ICaloFutureShowerOverlapTool.h"
#include "Kernel/CaloCellID.h"
#include <algorithm>
#include <cmath>
#include <numeric>
#include <string>
#include <vector>
// ============================================================================

/** @class MergedPi0 CaloFutureMergedPi0.h
 *
 *  Merged pi0 reconstruction with iterative Method
 *
 * NEW IMPLEMENTATION
 *
 *  @author Olivier Deschamps
 *  @date   05/05/2014
 */

namespace {

  bool isNeighbor( LHCb::CaloCellID id0, LHCb::CaloCellID id1 ) {
    if ( !id0 || !id1 ) return false;
    if ( std::abs( int( id0.row() ) - int( id1.row() ) ) > 1 ) return false;
    if ( std::abs( int( id0.col() ) - int( id1.col() ) ) > 1 ) return false;
    return true;
  }

  struct ClusterTriplet {
    LHCb::Event::Calo::Clusters::const_reference merged;
    LHCb::Event::Calo::Clusters::reference       photon1;
    LHCb::Event::Calo::Clusters::reference       photon2;
  };

} // namespace

namespace LHCb::Calo::Algorithm {
  using namespace LHCb::Event::Calo;

  class MergedPi0 : public Gaudi::Functional::MultiTransformer<std::tuple<Hypotheses, Clusters>( const Clusters&,
                                                                                                 const DeCalorimeter& ),
                                                               LHCb::DetDesc::usesConditions<DeCalorimeter>> {

  public:
    MergedPi0( const std::string& name, ISvcLocator* svcloc );

    std::tuple<Hypotheses, Clusters> operator()( const Clusters&, const DeCalorimeter& ) const override;

  private:
    template <typename Entries, typename FloatE, typename FloatZ>
    decltype( auto ) addSubCluster( Clusters& clusters, LHCb::CaloCellID seed, LHCb::CaloCellID seed2, FloatE seede,
                                    FloatE seed2e, Entries&& entries, FloatZ z, const DeCalorimeter& det ) const {

      constexpr auto used   = LHCb::CaloDigitStatus::Status{LHCb::CaloDigitStatus::Mask::UseForEnergy,
                                                          LHCb::CaloDigitStatus::Mask::UseForPosition,
                                                          LHCb::CaloDigitStatus::Mask::UseForCovariance};
      constexpr auto seed_s = LHCb::CaloDigitStatus::Status{LHCb::CaloDigitStatus::Mask::SeedCell,
                                                            LHCb::CaloDigitStatus::Mask::LocalMaximum} |
                              used;

      int hwm = clusters.totalNumberOfEntries();
      for ( const auto& e : entries ) {
        // -- tag 3x3 area for energy and position
        if ( isNeighbor( seed, e.cellID() ) ) {
          // set initial weights
          auto weight = ( e.cellID() == seed2 ? 0
                                              : ( e.cellID() != seed && isNeighbor( seed2, e.cellID() ) )
                                                    ? ( seede / ( seede + seed2e ) * e.fraction() )
                                                    : e.fraction() );
          clusters.emplace_back( e.cellID(), e.energy(), weight, seed == e.cellID() ? seed_s : used );
        }
      }
      // --  apply position tagger (possibly replacing the 3x3 already set)
      // --  needed to apply hypo S/L-corrections with correct parameters internally
      // --  keep the 3x3 energy tag for the time being (to be applied after the overlap subtraction)
      m_tagger->tagPosition( seed, clusters.range_of_entries( hwm ) ).orElse( [&] { ++m_tag_failed; } ).ignore();

      auto r = LHCb::CaloDataFunctor::calculateClusterEXY( clusters.range_of_entries( hwm ), &det );
      return clusters.emplace_back( seed, Clusters::Type::Area3x3, {hwm}, r->Etot, {r->x, r->y, z} );
    }

    Gaudi::Property<float> m_etCut{this, "EtCut", 1500 * Gaudi::Units::MeV}; // minimal ET of clusters to consider
    Gaudi::Property<float> m_minET{this, "SplitPhotonMinET", 0.}; // minimal ET of resulting split-clusters and photons
    Gaudi::Property<int>   m_iter{this, "MaxIterations", 25,
                                "Max iterations in ShowerOverlapTool. If negative, stop when tolerance is "
                                "reached."}; // forwarded to ShowerOverlapTool
    Gaudi::Property<bool>  m_applyLSCorr{this, "ApplyLSCorr", true};

    ToolHandleArray<Interfaces::IProcessHypos> m_gTools{this, "PhotonTools", {}};

    ToolHandle<Interfaces::IShowerOverlap> m_oTool{"CaloFutureShowerOverlapTool/SplitPhotonShowerOverlap", this};
    ToolHandle<SubClusterSelectorTool>     m_tagger{"FutureSubClusterSelectorTool/EcalClusterTag", this};
    ToolHandle<Interfaces::IClusterTool>   m_spread{"FutureClusterSpreadTool/EcalSpread", this};
    ToolHandle<Interfaces::IClusterTool>   m_cov{"FutureClusterCovarianceMatrixTool/EcalCovariance", this};

    mutable Gaudi::Accumulators::BinomialCounter<> m_cntNo2ndSeed{this, "Cluster without 2nd seed found"};
    mutable Gaudi::Accumulators::BinomialCounter<> m_cntFailsToTagECluster1{this, "Fails to tag(E) cluster (1)"};
    mutable Gaudi::Accumulators::BinomialCounter<> m_cntFailsToTagECluster2{this, "Fails to tag(E) cluster (2)"};
    mutable Gaudi::Accumulators::BinomialCounter<> m_cntFailsToSetCovariance{this, "Fails to set covariance"};
    mutable Gaudi::Accumulators::BinomialCounter<> m_cntFailsToSetSpread{this, "Fails to set spread"};

    mutable Gaudi::Accumulators::AveragingCounter<> m_cntPi0sSize{this, "clusters => mergedPi0s"};
    mutable Gaudi::Accumulators::AveragingCounter<> m_cntSplitClustersSize{this, "clusters => splitClusters"};

    mutable Gaudi::Accumulators::MsgCounter<MSG::ERROR>   m_g1_error{this, "Error from 'Tool' for g1"};
    mutable Gaudi::Accumulators::MsgCounter<MSG::WARNING> m_tag_failed{
        this, "SplitCluster tagging failed - keep the initial 3x3 tagging"};

    mutable Gaudi::Accumulators::StatCounter<> m_counterPhotonsDeltaX{this, "Photon Delta(X)"};
    mutable Gaudi::Accumulators::StatCounter<> m_counterPhotonsDeltaY{this, "Photon Delta(Y)"};
    mutable Gaudi::Accumulators::StatCounter<> m_counterPhotonsDeltaZ{this, "Photon Delta(Z)"};
    mutable Gaudi::Accumulators::StatCounter<> m_counterPhotonsDeltaE{this, "Photon Delta(E)"};
    mutable Gaudi::Accumulators::StatCounter<> m_counterPhotonsCorrectedEnergy{this, "Corrected energy"};
  };
  DECLARE_COMPONENT_WITH_ID( MergedPi0, "CaloFutureMergedPi0" )

  // ============================================================================
  /** @file CaloFutureMergedPi0.cpp
   *
   *  Implementation file for class : CaloFutureMergedPi0
   *
   *  @author Olivier Deschamps
   *  @date 05/05/2014
   *
   *  New implementation of CaloFutureMergedPi0 algorithm
   *
   */
  // ============================================================================

  MergedPi0::MergedPi0( const std::string& name, ISvcLocator* svcloc )
      : MultiTransformer{
            name,
            svcloc,
            // Input
            {KeyValue{"InputData", LHCb::CaloFutureAlgUtils::CaloFutureClusterLocation( "Ecal" )},
             KeyValue{"Detector", DeCalorimeterLocation::Ecal}},
            // Output
            {KeyValue{"MergedPi0s", LHCb::CaloFutureAlgUtils::CaloFutureHypoLocation( "MergedPi0s" )},
             KeyValue{"SplitClusters", LHCb::CaloFutureAlgUtils::CaloFutureClusterLocation( "SplitClusters" )}}} {}

  // ============================================================================

  std::tuple<Hypotheses, Clusters> MergedPi0::operator()( const Clusters& clusters, const DeCalorimeter& det ) const {

    auto result       = std::tuple<Hypotheses, Clusters>{};
    auto& [pi0s, out] = result;
    pi0s.reserve( clusters.size() );
    auto splitClusters = Clusters{};
    splitClusters.reserveForClusters( 2 * clusters.size() );
    boost::container::small_vector<ClusterTriplet, 1024> triplets;

    // - setup the estimator of cluster transverse energy
    const auto eT = LHCb::CaloDataFunctor::EnergyTransverse{&det};

    auto zNominal = det.toGlobal( Gaudi::XYZPoint() ).z() + det.zOffset();

    // define entry status

    // book counter buffers
    auto cntNo2ndSeed           = m_cntNo2ndSeed.buffer();
    auto cntFailsToTagECluster1 = m_cntFailsToTagECluster1.buffer();
    auto cntFailsToTagECluster2 = m_cntFailsToTagECluster2.buffer();

    // ============ loop over all clusters ==============
    for ( auto&& cluster : clusters ) {
      if ( 0 < m_etCut && m_etCut > eT( &cluster ) ) continue;

      // -- remove small clusters :
      if ( cluster.entries().size() < 2 ) continue;

      // -- locate cluster Seed
      LHCb::CaloCellID seed    = cluster.seed();
      const auto&      entries = cluster.entries();
      auto             dig1 =
          std::find_if( entries.begin(), entries.end(), [seed]( const auto& e ) { return e.cellID() == seed; } );
      assert( dig1 != entries.end() );
      auto seede = dig1->energy();

      // -- locate seed2
      // 2nd seed should must have a positive energy !
      using Float = decltype( entries.front().energy() );
      auto [seed2, _, seed2e] =
          std::accumulate( entries.begin(), entries.end(), std::tuple{LHCb::CaloCellID{}, Float{0}, Float{0}},
                           [&]( auto ee, const auto& entry ) {
                             if ( entry.cellID() == seed ) return ee;
                             auto ecel = entry.energy() * entry.fraction();
                             return ( ecel > std::get<1>( ee ) && ecel < seede && isNeighbor( seed, entry.cellID() ) )
                                        ? std::tuple{entry.cellID(), ecel, entry.energy()}
                                        : ee;
                           } );
      assert( seed2 != seed );

      cntNo2ndSeed += !seed2;
      if ( !seed2 ) continue;

      // -- create and fill sub-clusters
      auto cl1 = addSubCluster( splitClusters, seed, seed2, seede, seed2e, entries, zNominal, det );
      auto cl2 = addSubCluster( splitClusters, seed2, seed, seed2e, seede, entries, zNominal, det );

      // == apply the mergedPi0 tool : subtract shower overlap
      m_oTool->process( cl1, cl2, m_iter, LHCb::Calo::Interfaces::IShowerOverlap::propagateInitialWeights{true},
                        LHCb::Calo::Interfaces::IShowerOverlap::applyCorrections{m_applyLSCorr.value()} );

      // skip negative energy "clusters"
      if ( LHCb::Calo::Momentum( cl1 ).pt() <= m_minET || LHCb::Calo::Momentum( cl2 ).pt() <= m_minET ) {
        splitClusters.erase( std::next( splitClusters.begin(), splitClusters.size() - 1 ) );
        splitClusters.erase( std::next( splitClusters.begin(), splitClusters.size() - 1 ) );
        continue;
      }

      triplets.push_back( {cluster, cl1, cl2} ); // C++20: use emplace_back when aggregate initialization is possible in
                                                 // emplace_back...

      // == APPLY CLUSTER TOOLS : Energy tagger,  covariance & spread (position tagger already applied):
      // to reduce branching, unconditionally increment all counter buffers
      // cluster energy tag
      cntFailsToTagECluster1 += m_tagger->tagEnergy( cl1.seed(), cl1.entries() ).isFailure();
      cntFailsToTagECluster2 += m_tagger->tagEnergy( cl2.seed(), cl2.entries() ).isFailure();
    }

    //-- apply cluster covariance and spread tools...
    m_cntFailsToSetCovariance += ( *m_cov )( splitClusters ).isFailure();
    m_cntFailsToSetSpread += ( *m_spread )( splitClusters ).isFailure();

    //-- make a snapshot of the clusters prior to E/S/L corrections so the converters can use them to
    //   make backwards compatible CaloHypos... (and, bit expensive for just that, for monitoring the
    //   result of the E/S/L corrections)
    out = splitClusters;

    //--  Apply hypo tools : E/S/L-corrections
    for ( const auto& [i, t] : LHCb::range::enumerate<1>( m_gTools ) ) {
      if ( msgLevel( MSG::DEBUG ) ) debug() << " apply SplitPhoton tool " << i << "/" << m_gTools.size() << endmsg;
      if ( !t ) continue;
      t->process( Hypotheses::Type::PhotonFromMergedPi0, splitClusters ).orElse( [&] { ++m_g1_error; } ).ignore();
    }

    auto update_counters = [dx = m_counterPhotonsDeltaX.buffer(), dy = m_counterPhotonsDeltaY.buffer(),
                            dz = m_counterPhotonsDeltaZ.buffer(), dE = m_counterPhotonsDeltaE.buffer(),
                            E             = m_counterPhotonsCorrectedEnergy.buffer(),
                            ref_container = std::cref( out )]( const auto& c ) mutable {
      const auto& ref = c.rebind( ref_container.get() );
      dx += c.position().x() - ref.position().x();
      dy += c.position().y() - ref.position().y();
      dz += c.position().z() - ref.position().z();
      dE += c.energy() - ref.energy();
      E += c.energy();
    };

    for ( auto triplet : triplets ) {
      if ( LHCb::Calo::Momentum( triplet.photon1 ).pt() < m_minET ||
           LHCb::Calo::Momentum( triplet.photon2 ).pt() < m_minET )
        continue;
      pi0s.emplace_back( Hypotheses::Type::Pi0Merged, triplet.merged.cellID(),
                         {triplet.merged, triplet.photon1, triplet.photon2} );

      // monitor calibration applied to clusters
      update_counters( triplet.photon1 );
      update_counters( triplet.photon2 );
    }

    // ===== process stat counters
    m_cntPi0sSize += pi0s.size();
    m_cntSplitClustersSize += splitClusters.size();

    return result;
  }

} // namespace LHCb::Calo::Algorithm
