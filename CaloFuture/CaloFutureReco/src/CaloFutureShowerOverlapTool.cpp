/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files
// from Gaudi
#include "CaloDet/DeCalorimeter.h"
#include "CaloFutureCorrectionBase.h"
#include "CaloFutureUtils/CaloFutureAlgUtils.h"
#include "Event/CaloClusters_v2.h"
#include "Event/CaloDataFunctor.h"
#include "Event/CaloHypos_v2.h"
#include "Gaudi/Accumulators.h"
#include "GaudiAlg/GaudiTool.h"
#include "ICaloFutureHypoTool.h"
#include "ICaloFutureShowerOverlapTool.h" // Interface
#include "boost/container/flat_map.hpp"
#include <functional>
#include <utility>

/** @class CaloFutureShowerOverlapTool CaloFutureShowerOverlapTool.h
 *
 *
 *  @author Olivier Deschamps
 *  @date   2014-06-02
 */

namespace {
  std::string correctionType( const std::string& name ) {
    const std::string uName( LHCb::CaloFutureAlgUtils::toUpper( name ) );
    if ( uName.find( "MERGED" ) != std::string::npos || uName.find( "SPLITPHOTON" ) != std::string::npos )
      return "SplitPhoton";
    return "Photon";
  }

  Gaudi::LorentzVector to_p4( Gaudi::XYZPoint loc, double e ) {
    auto dir = ( loc - Gaudi::XYZPoint{0, 0, 0} ).Unit();
    return {e * dir.x(), e * dir.y(), e * dir.z(), e};
  }
} // namespace

namespace LHCb::Calo::Tools {

  class ShowerOverlap final : public extends<GaudiTool, Interfaces::IShowerOverlap> {

  public:
    using extends::extends;

    StatusCode initialize() override;

    // if niter < 0, stop if the tolerance criteria is satisfied (see definition),
    // perform maximum -niter iterations
    void process( Event::Calo::Clusters::reference c1, Event::Calo::Clusters::reference c2, int niter = 10,
                  propagateInitialWeights = propagateInitialWeights{false},
                  applyCorrections        = applyCorrections{true} ) const override;

  private:
    void evaluate( LHCb::Event::Calo::Clusters::reference c, bool hypoCorrection = true ) const;

    Gaudi::Property<unsigned int>         m_minSize{this, "ClusterMinSize", 2};
    Gaudi::Property<std::string>          m_detLoc{this, "Detector", DeCalorimeterLocation::Ecal};
    Gaudi::Property<std::string>          m_pcond{this, "Profile", "Conditions/Reco/Calo/PhotonShowerProfile"};
    ToolHandle<Interfaces::IProcessHypos> m_stool{this, "CaloFutureSCorrection",
                                                  "CaloFutureSCorrection/" + correctionType( name() ) + "SCorrection"};
    ToolHandle<LHCb::Calo::Interfaces::IProcessHypos> m_ltool{
        this, "CaloFutureLCorrection", "CaloFutureLCorrection/" + correctionType( name() ) + "LCorrection"};
    ToolHandle<CaloFutureCorrectionBase>   m_shape{this, "CaloFutureCorrectionBase",
                                                 "CaloFutureCorrectionBase/ShowerProfile"};
    const DeCalorimeter*                   m_det = nullptr;
    mutable Gaudi::Accumulators::Counter<> m_skippedSize{this, "Overlap correction skipped due to cluster size"};
    mutable Gaudi::Accumulators::Counter<> m_skippedEnergy{this, "Overlap correction skipped due to cluster energy"};
    mutable Gaudi::Accumulators::Counter<> m_positionFailed{this, "Cluster position failed"};

    mutable Gaudi::Accumulators::MsgCounter<MSG::ERROR> m_lcorr_error{this, "LCorrection could not be evaluated"};
    mutable Gaudi::Accumulators::MsgCounter<MSG::ERROR> m_scorr_error{this, "SCorrection could not be evaluated"};
  };

  namespace {

    using WeightMap = boost::container::flat_map<CaloCellID, double>;

    double showerFraction( const CaloFutureCorrectionBase& shape, const double d3d, const unsigned int area ) {
      CaloCellID cellID( CaloCellCode::CaloIndex::EcalCalo, area, 0, 0 ); // fake cell
      return std::clamp( shape.getCorrection( CaloFutureCorrection::profile, cellID, d3d ).value_or( 0. ), 0., 1. );
    }

    double fraction( const CaloFutureCorrectionBase& shape, const DeCalorimeter& det,
                     Event::Calo::Clusters::const_reference cluster, CaloCellID cellID, double ed, int area ) {

      double size = det.cellSize( cellID );
      double xc   = cluster.position().x();
      double yc   = cluster.position().y();
      double zc   = cluster.position().z();
      double xd   = det.cellX( cellID );
      double yd   = det.cellY( cellID );
      double zd   = ( xc * xc + yc * yc + zc * zc - xc * xd - yc * yd ) / zc;
      double d3d =
          std::sqrt( ( xd - xc ) * ( xd - xc ) + ( yd - yc ) * ( yd - yc ) + ( zd - zc ) * ( zd - zc ) ) / size;
      double f  = showerFraction( shape, d3d, area );
      double ec = f * cluster.e();
      return ( ed > ec ) ? ( ed - ec ) / ed : 0.;
    }

    template <typename Map, typename Predicate>
    void erase_if( Map& map, Predicate&& predicate ) {
      auto i = map.begin();
      while ( i != map.end() ) {
        if ( std::invoke( predicate, std::as_const( *i ) ) )
          i = map.erase( i );
        else
          ++i;
      }
    }

    template <typename Map>
    Map makeWeights( Event::Calo::Clusters::const_reference cl1, Event::Calo::Clusters::const_reference cl2 ) {
      Map weights;
      weights.reserve( cl1.entries().size() + cl2.entries().size() );
      for ( const auto& i1 : cl1.entries() ) {
        [[maybe_unused]] auto [it, inserted] = weights.try_emplace( i1.cellID(), i1.fraction() );
        assert( inserted );
      }
      for ( const auto& i2 : cl2.entries() ) {
        auto [it, inserted] = weights.try_emplace( i2.cellID(), i2.fraction() );
        if ( !inserted ) it->second += i2.fraction();
      }
      // check
      erase_if( weights, []( const auto& p ) { return p.second == 1.; } );
      return weights;
    }

    template <typename GetWeight, typename GetFraction, typename Evaluate>
    void subtract( LHCb::Event::Calo::Clusters::reference cl1, LHCb::Event::Calo::Clusters::reference cl2,
                   const GetWeight& getWeight, const GetFraction& getFraction, const Evaluate& evaluate ) {

      auto skip = []( auto status ) {
        return status.noneOf( {CaloDigitStatus::Mask::UseForEnergy, CaloDigitStatus::Mask::UseForPosition} );
      };

      // cluster1  -> cluster2 spillover
      for ( auto& i2 : cl2.entries() ) {
        if ( skip( i2.status() ) ) continue;
        i2.setFraction( getFraction( cl1, i2, 1 ) * getWeight( i2.cellID() ) );
      }

      // re-evaluate cluster2 accordingly
      evaluate( cl2 );
      if ( cl2.e() < 0 ) return; // skip negative energy "clusters"

      // cluster2  -> cluster1 spillover
      for ( auto& i1 : cl1.entries() ) {
        if ( skip( i1.status() ) ) continue;
        double initialWeight = getWeight( i1.cellID() );
        i1.setFraction( getFraction( cl2, i1, 2 ) * initialWeight );
        constexpr double eps = 1.e-4;
        // normalize the sum of partial weights in case of  shared cells
        for ( auto& i2 : cl2.entries() ) {
          if ( !( i2.cellID() == i1.cellID() ) || skip( i2.status() ) ) continue;
          double f1  = i1.fraction();
          double f2  = i2.fraction();
          double sum = f1 + f2;
          if ( std::abs( sum - initialWeight ) > eps ) {
            if ( sum < initialWeight && f2 == 0 ) {
              i2.setFraction( initialWeight - f1 );
            } else if ( sum < initialWeight && f1 == 0 ) {
              i1.setFraction( initialWeight - f2 );
            } else {
              i1.setFraction( initialWeight * f1 / ( f1 + f2 ) );
              i2.setFraction( initialWeight * f2 / ( f1 + f2 ) );
            }
          }
        }
      }

      // reevaluate  cluster1 & 2 accordingly
      evaluate( cl1 );
      evaluate( cl2 );
    }
  } // namespace

  //=============================================================================
  // Initializer
  //=============================================================================

  StatusCode ShowerOverlap::initialize() {
    return GaudiTool::initialize()
        .andThen( [&] { m_det = getDet<DeCalorimeter>( m_detLoc ); } )
        .andThen( [&] { return m_shape.retrieve(); } )
        .andThen( [&] { return m_shape->setConditionParams( m_pcond, true ); } );
  }

  //=============================================================================

  void ShowerOverlap::process( Event::Calo::Clusters::reference cl1, Event::Calo::Clusters::reference cl2,
                               const int niter, propagateInitialWeights propagateInitialWeights,
                               applyCorrections applyCorrections ) const {

    if ( cl1.entries().size() < m_minSize || cl2.entries().size() < m_minSize ) {
      ++m_skippedSize;
      return; // skip small clusters
    }

    // settings for stopping criterias
    // typical energy resolution (LHCb note 2003 091, page 5): sigma_E/E = (10.2+-0.3)%/sqrt(E) convoluted with
    // (1.6+-0.1)%
    auto resolution_e = []( double e ) {
      constexpr double part2_squared       = 1.6 * 1.6;
      constexpr auto   part1_squared       = 10.2 * 10.2 * 1000.0;
      const auto       relative_resolution = sqrt( part1_squared / e + part2_squared );
      return relative_resolution / 100.0 * e;
    };
    // typical transversal resolution (LHCb note 2003 091, page 5): 1.5mm, 3mm and 7mm for the inner, middle and outer
    // part of Ecal, respectively (∼ 4%, 5% and 6% of the cell size)
    constexpr double resolution_transversal_all[3] = {7.0, 3.0, 1.5};
    // distance in transversal plane
    auto distance = []( const double x1, const double x2, const double y1, const double y2 ) {
      const auto dx = x2 - x1;
      const auto dy = y2 - y1;
      return sqrt( dx * dx + dy * dy );
    };
    // tolerance (as fraction of typical resolution)
    constexpr double tolerance = 0.01; // very safe, 1% of typical resolution
    // store energies and cluster positions at each iteration to determine if the algorithm could be stopped
    double last_e_cl1 = cl1.e();
    double last_e_cl2 = cl2.e();
    double last_x_cl1 = cl1.position().x();
    double last_x_cl2 = cl2.position().x();
    double last_y_cl1 = cl1.position().y();
    double last_y_cl2 = cl2.position().y();
    // currently do not check z: seems that it changes sizably only at 1st iteration, and can't find typical resolution
    // values
    // double last_z_cl1 = cl1.position().z();
    // double last_z_cl2 = cl2.position().z();
    // temporary stuff
    // static int niter_tot = 0;
    // static int ncalls = 0;

    // 0 - evaluate parameters (optionally applying photon hypo corrections for the position)
    evaluate( cl1, bool( applyCorrections ) );
    evaluate( cl2, bool( applyCorrections ) );

    if ( cl1.e() <= 0. || cl2.e() <= 0 ) {
      ++m_skippedEnergy;
      return;
    }

    if ( msgLevel( MSG::VERBOSE ) ) {
      verbose() << " ======== Shower Overlap =======" << endmsg;
      verbose() << " CL1/CL2 : " << cl1.e() << " " << cl2.e() << " " << cl1.e() + cl2.e() << endmsg;
      verbose() << " seed    : " << cl1.seed() << " " << cl2.seed() << endmsg;
      verbose() << " area    : " << cl1.seed().area() << " " << cl2.seed().area() << endmsg;
      verbose() << " position  : " << cl1.position() << " / " << cl2.position() << endmsg;
      verbose() << " >> E  : " << cl1.e() << " / " << cl2.e() << endmsg;
    }

    const auto evaluate_ = [this, applyCorrections]( auto& cluster ) {
      return this->evaluate( cluster, bool( applyCorrections ) );
    };
    const auto weight_ =
        [weights = ( propagateInitialWeights ? makeWeights<WeightMap>( cl1, cl2 ) : WeightMap{} )]( CaloCellID id ) {
          if ( weights.empty() ) return 1.;
          auto it = weights.find( id );
          return it != weights.end() ? it->second : 1.;
        };
    const auto fraction_ = [shape = std::cref( *m_shape ), det = std::cref( *m_det ), a1 = cl1.seed().area(),
                            a2 = cl2.seed().area()]( LHCb::Event::Calo::Clusters::const_reference c, const auto& entry,
                                                     int flag ) {
      return fraction( shape, det, c, entry.cellID(), entry.energy(), flag == 1 ? a1 : a2 );
    };
    // 1 - determine the energy fractions of each entry
    // if niter < 0, stop if the tolerance criteria is satisfied (see definition),
    // perform maximum -niter iterations
    for ( int iter = 0; iter < abs( niter ); ++iter ) {

      subtract( cl1, cl2, weight_, fraction_, evaluate_ );

      if ( msgLevel( MSG::VERBOSE ) ) {
        verbose() << " ------ iter = " << iter << endmsg;
        verbose() << " >> CL1/CL2 : " << cl1.e() << " " << cl2.e() << "  " << cl1.e() + cl2.e() << endmsg;
        verbose() << " >> position  : " << cl1.position() << " / " << cl2.position() << endmsg;
        verbose() << " >> E  : " << cl1.e() << " / " << cl2.e() << endmsg;
        auto m = ( to_p4( cl1.position(), cl1.e() ) + to_p4( cl2.position(), cl2.e() ) ).mass();
        verbose() << " >> Mass : " << m << endmsg;
      }

      if ( niter < 0 ) {
        if ( distance( cl1.position().x(), last_x_cl1, cl1.position().y(), last_y_cl1 ) <
                 ( tolerance * resolution_transversal_all[cl1.seed().area()] ) &&
             distance( cl2.position().x(), last_x_cl2, cl2.position().y(), last_y_cl2 ) <
                 ( tolerance * resolution_transversal_all[cl2.seed().area()] ) &&
             fabs( cl1.e() - last_e_cl1 ) < ( tolerance * resolution_e( cl1.e() ) ) &&
             fabs( cl2.e() - last_e_cl2 ) < ( tolerance * resolution_e( cl2.e() ) ) ) {
          // niter_tot += iter + 1;
          // ncalls++;
          // always() << "stopped after iteration " << iter << " (average " << 1.0 * niter_tot / ncalls <<  ")" <<
          // endmsg;
          break;
        }
        // else if (iter == (abs(niter) - 1))
        //{
        // niter_tot += abs(niter);
        // always() << "stopped after iteration " << iter << " (average " << 1.0 * niter_tot / ncalls <<  ")" << endmsg;
        // ncalls++;
        //}
        last_e_cl1 = cl1.e();
        last_e_cl2 = cl2.e();
        last_x_cl1 = cl1.position().x();
        last_x_cl2 = cl2.position().x();
        last_y_cl1 = cl1.position().y();
        last_y_cl2 = cl2.position().y();
        // last_z_cl1 = cl1.position().z();
        // last_z_cl2 = cl2.position().z();
      }
    }
    // 3 - reset cluster-like parameters
    evaluate( cl1, false );
    evaluate( cl2, false );
  }

  void ShowerOverlap::evaluate( Event::Calo::Clusters::reference cluster, bool hypoCorrection ) const {

    // 0 - reset z-position of cluster
    LHCb::ClusterFunctors::ZPosition zPosition( m_det );
    cluster.position().SetZ( zPosition( cluster ) );

    // 1 - 3x3 energy and energy-weighted barycenter
    auto ret = LHCb::Calo::Functor::calculateClusterEXY( cluster.entries(), m_det );
    if ( ret ) {
      cluster.energy()   = ret->Etot;
      cluster.position() = {ret->x, ret->y, cluster.position().Z()};
    } else {
      ++m_positionFailed;
    }

    if ( cluster.e() < 0 ) return; // skip correction for negative energy "clusters"
    if ( !hypoCorrection ) return; // do not apply 'photon' hypo correction

    // 2 - apply 'photon hypothesis' corrections

    // Apply transversal corrections and then Apply longitudinal correction
    m_stool->process( LHCb::Event::Calo::Hypotheses::Type::Photon, cluster )
        .orElse( [&] { ++m_scorr_error; } )
        .andThen( [&] { return m_ltool->process( LHCb::Event::Calo::Hypotheses::Type::Photon, cluster ); } )
        .orElse( [&] { ++m_lcorr_error; } )
        .ignore();
  }
} // namespace LHCb::Calo::Tools

// Declaration of the Tool Factory
DECLARE_COMPONENT_WITH_ID( LHCb::Calo::Tools::ShowerOverlap, "CaloFutureShowerOverlapTool" )
