/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "FutureSubClusterSelectorTool.h"
#include "fmt/format.h"

//-----------------------------------------------------------------------------
// Implementation file for class : FutureSubClusterSelectorTool
//
// 2014-06-20 : Olivier Deschamps
//-----------------------------------------------------------------------------
namespace LHCb::Calo {
  // Declaration of the Tool Factory
  DECLARE_COMPONENT_WITH_ID( SubClusterSelectorTool, "FutureSubClusterSelectorTool" )

  //=============================================================================
  // Standard constructor, initializes variables
  //=============================================================================
  SubClusterSelectorTool::SubClusterSelectorTool( const std::string& type, const std::string& name,
                                                  const IInterface* parent )
      : GaudiTool( type, name, parent ) {
    declareInterface<SubClusterSelectorTool>( this );

    // define calo-dependent property
    m_det       = LHCb::CaloFutureAlgUtils::DeCaloFutureLocation( name );
    m_condition = fmt::format( "Conditions/Reco/Calo/{}ClusterMasks",
                               toString( LHCb::CaloFutureAlgUtils::CaloIndexFromAlg( name ) ) );

    // inherit properties from parents
    if ( parent ) {
      if ( const IProperty* prop = dynamic_cast<const IProperty*>( parent ); prop ) {
        if ( prop->hasProperty( "EnergyTags" ) )
          setProperty( prop->getProperty( "EnergyTags" ) ).orThrow( "oops", "oops" );
        if ( prop->hasProperty( "PositionTags" ) )
          setProperty( prop->getProperty( "PositionTags" ) ).orThrow( "oops", "oops" );
      }
    }

    // apply local default setting if not defined by parent
    if ( m_taggerE.empty() ) m_taggerE = std::vector<std::string>( 1, "useDB" );
    if ( m_taggerP.empty() ) m_taggerP = std::vector<std::string>( 1, "useDB" );
  }

  //=============================================================================

  StatusCode SubClusterSelectorTool::initialize() {
    StatusCode sc = GaudiTool::initialize(); // must be executed first
    if ( sc.isFailure() ) return sc;         // error printed already by GaudiTool

    // get detector element
    m_detector = getDet<DeCalorimeter>( m_det );
    if ( !m_detector ) return Error( "Cannot access DetectorElement at '" + m_det, StatusCode::FAILURE );

    // setup DB accessor tool
    m_dbAccessor = tool<CaloFutureCorrectionBase>( "CaloFutureCorrectionBase", "DBAccessor", this );
    sc           = m_dbAccessor->setConditionParams( m_condition,
                                           true ); // force access via DB - if not exist will return empty params
    if ( sc.isFailure() ) return sc;
    m_DBtaggerE = std::vector<std::string>( m_detector->numberOfAreas(), "unset" );
    m_DBtaggerP = std::vector<std::string>( m_detector->numberOfAreas(), "unset" );
    m_sourceE   = " (none)";
    m_sourceP   = " (none)";

    // always set options parameters first
    sc = getParamsFromOptions();
    if ( sc.isFailure() ) return sc;

    // then possibly update from DB
    updateParamsFromDB().ignore();

    updMgrSvc()->registerCondition( this, m_condition.value(), &SubClusterSelectorTool::updateParamsFromDB );

    info() << " Energy   mask : " << m_DBtaggerE << m_sourceE << endmsg;
    info() << " Position mask : " << m_DBtaggerP << m_sourceP << endmsg;

    return sc;
  }

  StatusCode SubClusterSelectorTool::updateParamsFromDB() {

    using namespace CaloFutureClusterMask;
    unsigned int narea = m_detector->numberOfAreas();
    for ( unsigned int area = 0; area < narea; ++area ) {
      LHCb::CaloCellID id = LHCb::CaloCellID( m_detector->index(), area, 0, 0 );

      if ( m_taggerE[area] == "useDB" ) {
        m_sourceE = " (from DB) ";
        Mask maskE =
            (Mask)m_dbAccessor->getParameter( CaloFutureCorrection::EnergyMask, 0, id ).value_or( 0. ); // default is
                                                                                                        // 3x3 when no
                                                                                                        // DB
        std::string nameE = maskName[maskE];
        if ( nameE != m_DBtaggerE[area] ) { // DB has changed ! update !
          std::string taggerE =
              ( m_clusterTaggers.find( nameE ) != m_clusterTaggers.end() ) ? m_clusterTaggers[nameE] : "";
          if ( !taggerE.empty() ) {
            Interfaces::ISubClusterTag* tE =
                tool<Interfaces::ISubClusterTag>( taggerE, id.areaName() + "EnergyTagger", this );
            tE->setMask( m_energyStatus );
            m_tagE.push_back( tE );
            m_DBtaggerE[area] = nameE;
          } else
            ++m_energy_update_failed;
        }
      }

      if ( m_taggerP[area] == "useDB" ) {
        m_sourceP = " (from DB) ";
        Mask maskP =
            (Mask)m_dbAccessor->getParameter( CaloFutureCorrection::PositionMask, 0, id ).value_or( 0. ); // default is
                                                                                                          // 3x3 when no
                                                                                                          // DB
        std::string nameP = maskName[maskP];
        if ( nameP != m_DBtaggerP[area] ) { // DB has changed ! update !
          std::string taggerP =
              ( m_clusterTaggers.find( nameP ) != m_clusterTaggers.end() ) ? m_clusterTaggers[nameP] : "";
          if ( !taggerP.empty() ) {
            Interfaces::ISubClusterTag* tP =
                tool<Interfaces::ISubClusterTag>( taggerP, id.areaName() + "PositionTagger", this );
            tP->setMask( m_positionStatus );
            m_tagP.push_back( tP );
            m_DBtaggerP[area] = nameP;
          } else
            ++m_position_update_failed;
        }
      }
    }
    return StatusCode::SUCCESS;
  }

  StatusCode SubClusterSelectorTool::getParamsFromOptions() {

    unsigned int narea = m_detector->numberOfAreas();

    // extend tagger per area when needed
    if ( m_taggerE.size() == 1 ) m_taggerE = std::vector<std::string>( narea, m_taggerE[0] );
    if ( m_taggerP.size() == 1 ) m_taggerP = std::vector<std::string>( narea, m_taggerP[0] );
    if ( m_taggerE.size() != m_detector->numberOfAreas() || m_taggerP.size() != m_detector->numberOfAreas() )
      return Error( "You must define the tagger for each calo area" );

    using namespace CaloFutureClusterMask;
    // == Define Energy tagger per area
    m_DBtaggerE = std::vector<std::string>( narea, "unset" );
    m_DBtaggerP = std::vector<std::string>( narea, "unset" );

    for ( unsigned int area = 0; area < narea; ++area ) {
      std::string areaName = LHCb::CaloCellID( m_detector->index(), area, 0, 0 ).areaName();

      std::string nameE = m_taggerE[area];
      if ( nameE != "useDB" ) {
        m_sourceE = " (from options) ";
        std::string taggerE =
            ( m_clusterTaggers.find( nameE ) != m_clusterTaggers.end() ) ? m_clusterTaggers[nameE] : "";
        if ( taggerE == "" )
          return Error( "Cannot find a  '" + nameE + "' tagger - You must select or define a known tagging method",
                        StatusCode::FAILURE );
        Interfaces::ISubClusterTag* tE = tool<Interfaces::ISubClusterTag>( taggerE, areaName + "EnergyTagger", this );
        tE->setMask( m_energyStatus );
        m_tagE.push_back( tE );
        m_DBtaggerE[area] = nameE;
      }

      std::string nameP = m_taggerP[area];
      if ( nameP != "useDB" ) {
        m_sourceP = " (from options) ";
        std::string taggerP =
            ( m_clusterTaggers.find( nameP ) != m_clusterTaggers.end() ) ? m_clusterTaggers[nameP] : "";
        if ( taggerP.empty() )
          return Error( "Cannot find a  '" + nameP + "' tagger - You must select or define a known tagging method",
                        StatusCode::FAILURE );
        Interfaces::ISubClusterTag* tP = tool<Interfaces::ISubClusterTag>( taggerP, areaName + "PositionTagger", this );
        tP->setMask( m_positionStatus );
        m_tagP.push_back( tP );
        m_DBtaggerP[area] = nameP;
      }
    }

    return StatusCode::SUCCESS;
  }
} // namespace LHCb::Calo
