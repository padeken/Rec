/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
//-----------------------------------------------------------------------------
/** @file RegionID.h
 *
 *  Header file for track find class Tf::RegionID
 *
 *  @author S. Hansmann-Menzemer, W. Hulsbergen, C. Jones, K. Rinnert
 *  @date   2007-05-30
 */
//-----------------------------------------------------------------------------

#ifndef TFKERNEL_REGIONID_H
#define TFKERNEL_REGIONID_H 1

#include "Kernel/OTChannelID.h"
#include "Kernel/STChannelID.h"
#include "Kernel/UTChannelID.h"
#include "Kernel/VeloChannelID.h"
#include "VeloDet/DeVeloSensor.h"

namespace Tf {

  /** @class RegionID TfKernel/RegionID.h
   *
   *  Simple class to store detector region information and definitions
   *  (station, layer, region etc.)
   *
   *  @author S. Hansmann-Menzemer, W. Hulsbergen, C. Jones, K. Rinnert
   *  @date   2007-05-30
   */
  class RegionID {

  public:
    /** @enum DetType TfKernel/RegionID.h
     *
     *  Enumerated type for detector type
     *
     *  @author S. Hansmann-Menzemer, W. Hulsbergen, C. Jones, K. Rinnert
     *  @date   2007-05-30
     */
    enum DetType {
      UT      = 5, ///< UT sensor
      UNKNOWN = 6  ///< Unknown type
    };

  public:
    /** Default constructor */
    RegionID() : m_dettype( UNKNOWN ), m_station( 0 ), m_layer( 0 ), m_region( 0 ) {}

    /** Constructor from an UTChannelID
     *  @param[in] id The UT channel identifier for the region
     */
    RegionID( const LHCb::UTChannelID& id );

    /** Returns the detector type for this region ID
     *  XXX???XXX Would it make more sense for this method to return a DetType enum ? */
    inline unsigned char type() const { return m_dettype; }

    /** Returns the station number */
    inline unsigned char station() const { return m_station; }

    /** Returns the layer number */
    inline unsigned char layer() const { return m_layer; }

    /** Returns the region number */
    inline unsigned char region() const { return m_region; }

    /** Returns the 'velohalf' number
     *  This is internally the same as the layer.  But a half is not a layer.
     *  Hence this accessor to make client code more readable,*/
    inline unsigned char veloHalf() const { return m_layer; }

    /** Return the 'zone' number
     *  This is internally the same as the region. But the VELO detector
     *  element calls this a zone.  Hence this accessor to make client
     *  code more consistent and readable. */
    inline unsigned char zone() const { return m_region; }

    struct UTIndex {
      enum {
        kMinStation = 0,  ///< Minimum valid station number for UT
        kMaxStation = 1,  ///< Maximum valid station number for UT
        kNStations  = 2,  ///< Number of UT stations
        MinLayer    = 0,  ///< Minimum valid layer number for a UT station
        kMaxLayer   = 1,  ///< Maximum valid layer number for a UT station
        kNLayers    = 2,  ///< Number of UT layers within a station
        kMinRegion  = 0,  ///< Minimum valid region number for a UT layer
        kMaxRegion  = 11, ///< Maximum valid region number for a UT layer
        kNRegions   = 12  ///< Number of UT regions within a layer
      };
    };

    /** Comparision operator for RegionID objects
     *  @param[in] rhs RHS of comparision == operator
     *  @return The result of the comparison
     *  @retval TRUE  The regions are the same
     *  @retval FALSE The regions are different
     */
    bool operator==( const RegionID& rhs ) const {
      return ( this->type() == rhs.type() && this->station() == rhs.station() && this->layer() == rhs.layer() &&
               this->region() == rhs.region() );
    }

  private:
    unsigned char m_dettype; ///< The detector type
    unsigned char m_station; ///< The station number (all indices start numbering from 0)
    unsigned char m_layer;   ///< The layer number within a station (also encodes VELO half)
    unsigned char m_region;  ///< The region number within a layer (also encodes zone on a VELO sensor in the global
                             ///< frame)
  };

  inline RegionID::RegionID( const LHCb::UTChannelID& id )
      : m_dettype( UT )
      , m_station( id.station() - 1 )
      , m_layer( id.layer() - 1 )
      ,
      // XXX???XXX Would be nice to document the reasoning behind this 'magic conversion' somewhere ?
      m_region( ( id.detRegion() - 1 ) * 4 + ( id.sector() - 1 ) % 4 ) {}

} // namespace Tf

#endif // TFKERNEL_REGIONID_H
