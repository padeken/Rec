/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef INDEXEDHITCONTAINER1D_H
#define INDEXEDHITCONTAINER1D_H 1

#include <algorithm>
#include <array>
#include <assert.h>
#include <numeric>
#include <vector>

#include "MultiIndexedHitContainer.h"

template <size_t nSub, typename Hit>
using IndexedHitContainer1D = MultiIndexedHitContainer<Hit, nSub>;

#endif
