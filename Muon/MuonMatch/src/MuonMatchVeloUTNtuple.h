/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef MUONMATCHVELOUTNTUPLE_H
#define MUONMATCHVELOUTNTUPLE_H 1

#include "GaudiAlg/Consumer.h"
#include "GaudiKernel/StdArrayAsProperty.h"
#include <GaudiAlg/GaudiTupleAlg.h>

#include <iostream>
#include <sstream>
#include <string>

#include "Associators/InputLinks.h"
#include "Associators/Location.h"
#include "Event/IntLink.h"
#include "Event/MCHit.h"
#include "Event/MCMuonDigit.h"
#include "Event/MCParticle.h"
#include "Event/MuonDigit.h"
#include "Event/ODIN.h"
#include "Event/RecVertex_v2.h"
#include "Event/State.h"
#include "Event/Track.h"
#include "Kernel/ILHCbMagnetSvc.h"
#include "LHCbMath/GeomFun.h"
#include "LHCbMath/Line.h"
#include "LHCbMath/SIMDWrapper.h"
#include "LHCbMath/Vec3.h"
#include "Linker/LinkerWithKey.h"
#include "MCInterfaces/IMCReconstructible.h"
#include "MuonDAQ/CommonMuonHit.h"
#include "MuonDAQ/MuonHitContainer.h"

#include "TMatrixD.h"
#include "TVectorD.h"

using Track  = LHCb::Event::v2::Track;
using Tracks = std::vector<Track>;
using Vertex = LHCb::Event::v2::RecVertex;
// using Vertices = std::vector<Vertex>;
using Vertices = LHCb::Event::v2::RecVertices;

class MuonMatchVeloUTNtuple final
    : public Gaudi::Functional::Consumer<
          void( const LHCb::ODIN& odin, const Vertices& pvs, const Tracks& uttracks, const Tracks& mmtracks,
                const MuonHitContainer& hit_handler, const LHCb::MCParticles& mctracks, const LHCb::MCHits& mcmuonhits,
                const LHCb::LinksByKey& mcparticleLinks, const LHCb::LinksByKey& mcdigits ),
          Gaudi::Functional::Traits::BaseClass_t<GaudiTupleAlg>> {
public:
  MuonMatchVeloUTNtuple( const std::string& name, ISvcLocator* pSvcLocator );

  virtual StatusCode initialize() override;
  virtual StatusCode finalize() override;
  virtual void       operator()( const LHCb::ODIN&, const Vertices& pvs, const Tracks& uttracks, const Tracks& mmtracks,
                           const MuonHitContainer&, const LHCb::MCParticles&, const LHCb::MCHits&,
                           const LHCb::LinksByKey&, const LHCb::LinksByKey& ) const override;

private:
  Gaudi::Property<bool> m_onlyMCMuons{this, "onlyMCMuons", false};

  void makeVeloUTNtuple( const LHCb::ODIN& odin, const Vertices& pvs, const Tracks& uttracks, const Tracks& mmtracks,
                         const MuonHitContainer& muonhits, const LHCb::MCParticles& mcparticles,
                         const LHCb::MCHits& mcmuonhits, const LHCb::LinksByKey& mcparticleLinks,
                         const LHCb::LinksByKey& mcmuonLinks ) const;

  std::optional<LHCb::MCParticle*> isDecayInFlightToMuon( const LHCb::MCParticle* mcparticle ) const;
  float                            impactParameter( const Track& track, const Vertices& vertex ) const;

  ILHCbMagnetSvc*     m_fieldSvc        = nullptr;
  IMCReconstructible* m_reconstructible = nullptr;
  DeMuonDetector*     m_muonDetector    = nullptr;

  float m_za = +5.331 * Gaudi::Units::m;
  float m_zb = -0.958 * Gaudi::Units::m;
};

#endif
