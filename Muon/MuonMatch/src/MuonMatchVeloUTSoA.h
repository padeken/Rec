/*****************************************************************************\
 * (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
 *                                                                             *
 * This software is distributed under the terms of the GNU General Public      *
 * Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
 *                                                                             *
 * In applying this licence, CERN does not waive the privileges and immunities *
 * granted to it by virtue of its status as an Intergovernmental Organization  *
 * or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef MUONMATCHVELOUTSOA_H
#define MUONMATCHVELOUTSOA_H 1

// STL
#include <algorithm>
#include <numeric>
#include <string>
#include <tuple>
#include <utility>
// from Gaudi
#include "GaudiAlg/Transformer.h"
#include "GaudiKernel/StdArrayAsProperty.h"
#include "GaudiKernel/SystemOfUnits.h"
// from LHCb
#include "Event/PrUpstreamTracks.h"
#include "Event/State.h"
#include "Event/Track.h"
#include "Kernel/ILHCbMagnetSvc.h"
#include "LHCbMath/GeomFun.h"
#include "LHCbMath/Line.h"
#include "LHCbMath/SIMDWrapper.h"
#include "LHCbMath/Vec3.h"
// from MuonDAQ
#include "MuonDAQ/MuonHitContainer.h"
// from MuonDet
#include "MuonDet/DeMuonDetector.h"
// from MuonMatch
#include "MuonMatch/Utils.h"

using window_bounds = std::tuple<float, float, float, float>;
using UTTracks      = LHCb::Pr::Upstream::Tracks;

class MuonMatchVeloUTSoA
    : public Gaudi::Functional::Transformer<UTTracks( const EventContext&, const UTTracks&, const MuonHitContainer& )> {

public:
  MuonMatchVeloUTSoA( const std::string& name, ISvcLocator* pSvcLocator )
      : Transformer(
            name, pSvcLocator,
            {KeyValue{"InputTracks", "Rec/Track/UT"}, KeyValue{"InputMuonHits", MuonHitContainerLocation::Default}},
            KeyValue{"OutputTracks", LHCb::TrackLocation::Match} ) {}

  StatusCode initialize() override;

  UTTracks operator()( const EventContext& evtCtx, const UTTracks& seeds,
                       const MuonHitContainer& hit_cont ) const override;

private:
  Gaudi::Property<bool> m_setQOverP{this, "SetQOverP", false}; // update q/p with the information from the muon chambers
  Gaudi::Property<float> m_maxChi2DoF{this, "MaxChi2DoF", 60};
  Gaudi::Property<bool>  m_fitY{this, "FitY", false};

  /// Magnetic field service
  ILHCbMagnetSvc* m_fieldSvc = nullptr;

  /// Counter for the number of input muon hits
  mutable Gaudi::Accumulators::AveragingCounter<> m_hitCount{this, "#hits"};
  /// Counter for the number of input seeds
  mutable Gaudi::Accumulators::AveragingCounter<> m_seedCount{this, "#seeds"};
  /// Counter for the matched tracks
  mutable Gaudi::Accumulators::AveragingCounter<> m_matchCount{this, "#matched"};

  // Parameters for calcutating the
  float m_kickOffset = 338.92 * Gaudi::Units::MeV;
  float m_kickScale  = 1218.62 * Gaudi::Units::MeV;

  // Parameters for calculating the magnet focal plane
  float m_za = +5.331 * Gaudi::Units::m;
  float m_zb = -0.958 * Gaudi::Units::m;

  // Size of the search windows for each muon station
  std::array<std::pair<float, float>, sizeof( MuonChamber )> m_window = {
      std::pair<float, float>{500 * Gaudi::Units::mm, 400 * Gaudi::Units::mm},
      std::pair<float, float>{600 * Gaudi::Units::mm, 500 * Gaudi::Units::mm},
      std::pair<float, float>{700 * Gaudi::Units::mm, 600 * Gaudi::Units::mm},
      std::pair<float, float>{800 * Gaudi::Units::mm, 700 * Gaudi::Units::mm}};

  // Momentum bounds defining the track types
  std::array<float, 3> m_momentumBounds = {2.5 * Gaudi::Units::GeV, 6 * Gaudi::Units::GeV,
                                           10 * Gaudi::Units::GeV}; // 3, 6 and 10 GeV/c for offline muons

  bool matchByTrackType( State& state, const MuonHitContainer& hit_cont,
                         LHCb::Allocators::MemoryResource* memResource ) const;

  template <MuonChamber... Last>
  bool match( State& state, const MuonHitContainer& hit_cont, MuonChamber first, MuonChamberSeq<Last...>&& last,
              LHCb::Allocators::MemoryResource* memResource ) const;

  std::optional<Hit> findHit( MuonChamber much, const State& state, const Hit& magnet_hit,
                              const MuonHitContainer& hit_cont, const float slope ) const;

  TrackType getTrackTypeFromMomentum( float p ) const;

  Hit getMagnetFocalPlane( const State& state ) const;

  window_bounds getFirstStationWindow( const State& state, const CommonMuonStation& station, const Hit& hit ) const;

  window_bounds getNextStationWindow( const State& state, const CommonMuonStation& station, const Hit& magnet_hit,
                                      const float slope ) const;

  float getChangeInXSlope( const State& state ) const;

  float extrapolateYInStraightLine( const State& state, float z ) const;

  std::tuple<float, float> fitHits( const Hits& hits, const Hit& magnetHit, const State& state ) const;

  template <class Container>
  std::tuple<float, float, unsigned int> fitLinear( const Container& points ) const;

  template <class Container>
  float chi2( const Container& points ) const;

  float getUpdatedMomentum( float dtx ) const;
};
//=============================================================================
//

//=============================================================================

#endif // MUONMATCHVELOUTSOA_H
