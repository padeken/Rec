/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef IMVATOOL_H_
#define IMVATOOL_H_

#include "GaudiKernel/IAlgTool.h"
#include "Kernel/STLExtensions.h"
#include "MuonDAQ/CommonMuonHit.h"

#include "Event/Track.h"

static const InterfaceID IID_IMVATool( "IMVATool", 1, 0 );

class IMVATool : virtual public IAlgTool {
public:
  struct MVAResponses {
    float CatBoost;
  };
  virtual bool              getRunCatBoostBatch() const noexcept = 0;
  virtual size_t            getNFeatures() const noexcept        = 0;
  static const InterfaceID& interfaceID() { return IID_IMVATool; }
  virtual auto              calcBDT( const float, const LHCb::State&, const CommonConstMuonHits& ) const noexcept
      -> MVAResponses                                                                          = 0;
  virtual void compute_features( const float trackP, const LHCb::State& state, const CommonConstMuonHits& hits,
                                 LHCb::span<float> result ) const                              = 0;
  virtual std::vector<double> calcBDTBatch( const std::vector<float> features ) const noexcept = 0;
};

#endif // IMVATOOL_H_
