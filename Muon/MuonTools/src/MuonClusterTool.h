/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef MUONCLUSTERTOOL_H
#define MUONCLUSTERTOOL_H 1

// Include files
// from Gaudi
#include "Event/MuonCluster.h"
#include "Event/MuonCoord.h"
#include "GaudiAlg/GaudiTool.h"
#include "MuonDet/DeMuonDetector.h"
#include "MuonInterfaces/IMuonClusterTool.h" // Interface

/** @class MuonClusterTool MuonClusterTool.h
 *
 *
 *  @author Alessia Satta
 *  @date   2010-01-15
 */
class MuonClusterTool : public extends<GaudiTool, IMuonClusterTool> {
public:
  /// Standard constructor
  MuonClusterTool( const std::string& type, const std::string& name, const IInterface* parent );

  StatusCode initialize() override;
  StatusCode doCluster( const std::string& Input, const std::string& Output ) override;

private:
  StatusCode initClusterVector( LHCb::MuonCoords* coords );
  void       mergeCluster();
  void       mergeStation( int i );
  void       ClearMemory();
  void       SaveOutput( const std::string& output );

  bool                                            detectCluster( LHCb::MuonCluster* one, LHCb::MuonCluster* two );
  bool                                            isIncluded( int station, LHCb::MuonCluster* cluster );
  std::vector<std::pair<LHCb::MuonCluster*, int>> m_inputclust[5];
  std::vector<LHCb::MuonCluster*>                 m_finalclust[5];
  DeMuonDetector*                                 m_muonDetector = nullptr;
};
#endif // MUONCLUSTERTOOL_H
