###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
################################################################################
# Package: MuonTrackMonitor
################################################################################
gaudi_subdir(MuonTrackMonitor v2r10)

gaudi_depends_on_subdirs(Det/MuonDet
                         Event/HltEvent
                         Event/LinkerEvent
                         Event/MCEvent
                         Event/RecEvent
                         Event/TrackEvent
                         Tr/TrackInterfaces
                         Tr/TrackKernel
                         GaudiAlg
                         Tf/TrackSys
                         Tr/TrackFitter
                         Tr/TrackTools)

find_package(AIDA)

find_package(Boost)
find_package(ROOT)
include_directories(SYSTEM ${Boost_INCLUDE_DIRS} ${ROOT_INCLUDE_DIRS})

gaudi_add_module(MuonTrackMonitor
                 src/*.cpp
                 INCLUDE_DIRS AIDA Tr/TrackInterfaces Tr/TrackKernel
                 LINK_LIBRARIES MuonDetLib HltEvent LinkerEvent MCEvent RecEvent TrackEvent TrackKernel GaudiAlgLib)

gaudi_install_python_modules()

